require File.dirname(__FILE__) + '/../spec_helper'

describe NoteType do
  fixtures :note_types
  before do
#    NoteType.create(:name => "Alert - Sent", :status => "system")
#    NoteType.create(:name => "Sales Appraisal", :status => "contact_note")
#    NoteType.create(:name => "Rental Appraisal", :status => "contact_note")
  end
  
  it "can get first note type name" do
    ev = NoteType.find(:first)
    ev.name.should include("Alert - Sent")
  end

  it "can get last note type name" do
    ev = NoteType.find(:last)
    ev.name.should include("Alert - Created")
  end
end