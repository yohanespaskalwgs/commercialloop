#!/usr/bin/env ruby
#env = ['development', 'production']
RAILS_ENV = 'development'#env.include?("#{ARGV[0]}") ? ARGV[0] : 'production'

require File.dirname(__FILE__) + '/../config/environment' 
puts "CRAKEN ENV: #{RAILS_ENV}"

def inactivate_office_whose_trial_date_is_over
  puts "[#{Time.now}] - STARTING office status scheduler..."
  offices = Office.find_by_sql("SELECT * FROM `subscriptions` , `offices` WHERE offices.status = 'active' AND offices.id = subscriptions.office_id")
  offices.each do |office|
		sub = office.subscription
		sp = office.subscription_plan
    if Time.now >= sp['trial_end_date'] && sub.status == 'pending'
      office.update_attribute(:status, 'inactive')
      puts "[#{Time.now}] - #{office.name} has expired today...."
    end
  end
  puts "[#{Time.now}] - COMPLETED "
end

def update_office_whose_subscription_is_canceled
  puts "[#{Time.now}] - STARTING update offices status scheduler..."
  subscriptions = Subscription.all
  subscriptions.each do |subscription|
		office = subscription.office
    if !subscription.canceled_at.blank? && subscription.status == 'canceled' && Date.now >= subscription.ends_on
      puts "[#{Time.now}] - #{office.name} no longer active today...."
      office.update_attribute(:status, "inactive")
    end 
    
  end
  puts "[#{Time.now}] - COMPLETED "
end


inactivate_office_whose_trial_date_is_over
update_office_whose_subscription_is_canceled

