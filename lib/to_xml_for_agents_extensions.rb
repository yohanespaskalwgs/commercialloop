module WillPaginateHelpers
  include ActiveSupport::CoreExtensions::Array::Conversions
  mattr_accessor :param_request_uri
  mattr_accessor :param_request_host

  def to_xml_with_collection_type(options = {})
    empty? ? returning(self.clone) { |c| c.instance_eval {|i| def empty?; false; end } } : self
    require 'builder'
    xml = Builder::XmlMarkup.new(:indent => 2)
    xml.instruct!
    xml.agents do
      self.each do |agent_user|
        xml << agent_user.to_xml(:root =>"agent",:skip_instruct => true,:except => [:crypted_password, :salt, :deleted_at, :remember_token, :remember_token_expires_at, :activation_code])
        unless agent_user.blank?
          xml << agent_user.testimonials.to_xml(:skip_types => true, :skip_instruct => true, :root => "testimonial", :only => [:id, :name,:content])
        end
        unless agent_user.landscape_image.blank?
          xml.landscape_images do
            agent_user.landscape_image.each_with_index do |landscape_image, i|
              xml.landscape_image(:position => i) do
                xml.tag!(:large, agent_user.landscape_image[i].public_filename)
                xml.tag!(:medium, agent_user.landscape_image[i].public_filename(:medium)) rescue ''
                xml.tag!(:thumb, agent_user.landscape_image[i].public_filename(:thumb)) rescue ''
              end
            end
          end
        else
          xml.landscape_images do
          end
        end
        unless agent_user.portrait_image.blank?
          xml.portrait_images do
            agent_user.portrait_image.each_with_index do |portrait_image, i|
              xml.portrait_image(:position => i) do
                xml.tag!(:large, agent_user.portrait_image[i].public_filename)
                xml.tag!(:medium, agent_user.portrait_image[i].public_filename(:medium)) rescue ''
                xml.tag!(:thumb, agent_user.portrait_image[i].public_filename(:thumb)) rescue ''
              end
            end
          end
        else
          xml.portrait_images do
          end
        end

        unless agent_user.vcard.blank?
          xml.vcard do
            xml.tag!(:title, agent_user.vcard.filename)
            xml.tag!(:path, translate_url(WillPaginateHelpers.param_request_host)+agent_user.vcard.public_filename)
          end
        else
          xml.vcard do
          end
        end
      end
    end

  end
  alias_method_chain :to_xml, :collection_type
  
  def translate_url(text)
    return 'http://' + text.gsub('http://', '')
  end
end
WillPaginate::Collection.send(:include, WillPaginateHelpers)
