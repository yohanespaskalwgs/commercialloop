class OfficeApiScheduler < Struct.new(:office_id)  
  include Jobs::ScheduledJob

  run_every 3.minutes

  def perform
    office = Office.find_by_id(office_id.to_i)
    if !office.blank?
      if (office.processed == 0 || office.processed.blank?) && office.count_api_sent.to_i < 3
        office.send_office_api
        office.count_api_sent = office.count_api_sent.to_i + 1

        #could not save with method save active record
        sql = ActiveRecord::Base.connection()
        sql.update "UPDATE offices SET count_api_sent = #{office.count_api_sent} WHERE id = #{office.id}"
        sql.commit_db_transaction

      else
        #delete delayed_job
        remove_delayed_jobs(office_id.to_i)
      end
    else
      remove_delayed_jobs(office_id.to_i)
    end
  end

  def remove_delayed_jobs(off_id)
    jobs = Delayed::Job.all
    id_jobs = []
    jobs.each do |job|
      handler = job.handler.split
      id_jobs << job.id if handler[handler.size-2].include?("office_id") && off_id.to_i == handler.last.to_i
    end
    unless id_jobs.blank?
      Delayed::Job.destroy(id_jobs)
    end
  end

end
