module NamedParam
  def to_param 
    "#{id}-#{name.gsub(/[^a-z1-9]+/i, '-')}"
  end
end  
