class ExportImportImageCsv < Struct.new(:property_id, :no, :path, :type)
  def perform
    property = Property.find(property_id)
    if property
      require 'action_controller/test_process.rb'
      if type == "Image"
        image_path = "#{RAILS_ROOT}/public/images/tmp/image_tmp#{property.id}_#{no}.jpg"
        if system("curl #{path} -o '#{image_path}'")
          mimetype = `file -ib #{image_path}`.gsub(/\n/,"")
          if mimetype
            if mimetype == "image/gif" || mimetype == "image/jpeg"
              image = Image.new(:uploaded_data => ActionController::TestUploadedFile.new(image_path, mimetype))
              image.attachable = property
              image.updated_at = Time.now
              image.save
              ActiveRecord::Base.connection.execute("UPDATE properties SET image_exist = 1 WHERE id = #{property.id}")
            end
            system("rm -f #{image_path}")
          end
        end
      end

      if type == "Floorplan"
        image_path = "#{RAILS_ROOT}/public/images/tmp/floorplan_tmp#{property.id}_#{no}.jpg"
        if system("curl #{path} -o '#{image_path}'")
          mimetype = `file -ib #{image_path}`.gsub(/\n/,"")
          if mimetype
            if mimetype == "image/gif" || mimetype == "image/jpeg"
              plan = Floorplan.new(:uploaded_data => ActionController::TestUploadedFile.new(image_path, mimetype))
              plan.updated_at = Time.now
              plan.attachable = property
              plan.save
              ActiveRecord::Base.connection.execute("UPDATE properties SET floorplan_exist = 1 WHERE id = #{property.id}")
            end
            system("rm -f #{image_path}")
          end
        end
      end
    end
  end
end
