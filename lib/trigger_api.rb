class TriggerApi < Struct.new(:type, :id, :conditions)
  def perform
    if type == "Property"
      properties = Property.find_by_sql("SELECT * FROM properties WHERE #{conditions}")
      unless properties.blank?
        properties.each do |property|
          property.update_attribute(:updated_at,Time.now)
        end
      end
    end
    if type == "PropertyDomain"
      unless id.blank?
       domain_id = id
       # Send Current & Under Offer Listings
       properties = Property.find_by_sql("SELECT * FROM properties WHERE (status = 1 or status = 5) #{conditions}")
       unless properties.blank?
        properties.each do |property|
          property.send_property_domain(domain_id)
        end
       end

       # Send Non Current & Under Offer Listings, for special condition
        domain = Domain.find_by_sql("SELECT * FROM domains WHERE id = #{domain_id}").first
       unless domain.blank?
        if domain.portal_export == true
          spec_properties = Property.find_by_sql("SELECT * FROM properties WHERE status NOT IN (1,5,6) #{conditions}")
          unless spec_properties.blank?
            spec_properties.each do |property|
              send_api = false
              next if domain.name.blank?
              property_portal_exports = property.property_portal_exports
              unless property_portal_exports.blank?
               property_portal_exports.each do |px|
                 next if px.portal.blank?
                 next if px.uncheck == true
                 portal = px.portal
                 next if portal.ftp_url.blank?
                 portal_url = portal.ftp_url.downcase.gsub('http://', '')
                 portal_url = portal_url.gsub('www.', '')
                  if domain.name.downcase.include?(portal_url)
                     if domain.name.downcase.include?("propertypoint.com.au")
                       next if property.state.blank?
                       send_api = true if property.state.strip == "TAS"

                     elsif domain.name.downcase.include?("land.com.au")
                       send_api = true if property.type.to_s == "ProjectSale" || property.property_type.to_s == "Land"

                     elsif domain.name.downcase.include?("getrealty.com.au")
                       next if property.state.blank?
                       send_api = true if property.state.strip == "ACT"

                     elsif domain.name.downcase.include?("virginhomes.com.au")
                       include_feature = property.features.find(:all,:conditions => "`name` LIKE '%newly built property%' OR `name` LIKE '%new developer listing%'") unless property.features.blank?
                       send_api = true if !include_feature.blank? || property.type.to_s == "ProjectSale"

                     elsif domain.name.downcase.include?("propertyinvestor.com.au")
                        send_api = true if property.type.to_s == "ResidentialSale" || property.type.to_s == "ProjectSale" || property.type.to_s == "NewDevelopment"
                     end

                     if send_api == true
                       property.send_property_domain(domain_id)
                       break
                     end
                   end
                   end
                 end
               end
           end
         end
        end
       end
    end
    if type == "User"
      users = AgentUser.find_by_sql("SELECT * FROM users WHERE #{conditions}")
      unless users.blank?
        users.each do |user|
          user.update_attribute(:updated_at,Time.now)
        end
      end
    end      
  end
end
