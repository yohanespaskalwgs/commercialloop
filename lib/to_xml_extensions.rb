module WillPaginateHelpers
  include ActiveSupport::CoreExtensions::Array::Conversions
  mattr_accessor :param_request_uri
  mattr_accessor :param_request_host
  mattr_accessor :param_mobile_service

  def to_xml_with_collection_type(options = {})
    empty? ? returning(self.clone) { |c| c.instance_eval {|i| def empty?; false; end } } : self
    url = WillPaginateHelpers.param_request_uri
    unless url.scan("office_id").blank?
      unless url.scan("page").blank?
        url = url.split("&")
        url =  url[2].blank? ? "#{url[0]}&page=" : "#{url[0]}&#{url[1]}&#{url[2].split("=")[0]}="
        flag = true
      else
        url = url + "&page="
      end
    else
      unless url.scan("page").blank?
        url = url.split("&")
        url = "#{url[0]}&#{url[1].split("=")[0]}="
        flag = false
      else
        url = url + "&page="
      end
    end

    if WillPaginateHelpers.param_mobile_service.blank?

      unless url.scan("agents").blank?
        #agents
        require 'builder'
        xml = Builder::XmlMarkup.new(:indent => 2)
        xml.instruct!
        xml.agents do
          if (!url.scan("developer_id").blank? or !url.scan("office_id").blank?) and !["NilClass"].include?(self.first.class.name.to_s)
            xml.pagination("xmlns:xlink" => "http://www.w3.org/1999/xlink" ) do
              unless current_page == 1
                xml.tag!(:pagination, {"xlink:type" => "simple","rel" => "previous_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page - 1).to_s : url + (current_page - 1).to_s}"},current_page - 1)
              end
              xml.tag!(:current_page, {"rel" => "current_page"}, current_page)
              unless current_page == (total_entries).ceil
                xml.tag!(:pagination, {"xlink:type" => "simple","rel" => "next_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page + 1).to_s : url + (current_page + 1).to_s}" },current_page + 1)
              end
              xml.tag!(:per_page, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[per_page.class.name]}, per_page)
              xml.tag!(:total_entries, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]}, total_entries)
              xml.tag!(:total_pages, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]},flag == true  ? (total_entries/50.to_f).ceil : (total_entries/10.to_f).ceil )
            end.sub(%{record_type="array"}, %{record_type="collection"})

          end
          self.each do |agent_user|
            agent_user.description = Iconv.new('UTF-8//IGNORE', 'UTF-8').iconv(agent_user.description) unless agent_user.blank?
            xml << agent_user.to_xml(:root =>"agent",:skip_instruct => true,:except => [:crypted_password, :salt, :deleted_at, :remember_token, :remember_token_expires_at, :activation_code])
            unless agent_user.blank?
              xml << agent_user.testimonials.to_xml(:skip_types => true, :skip_instruct => true, :root => "testimonial", :only => [:id, :name,:content])
            end
            
            unless agent_user.landscape_image.blank?
              xml.landscape_images do
                if agent_user.landscape_image.length == 2
                  agent_user.landscape_image.each_with_index do |landscape_image, i|
                    xml.landscape_image(:position => landscape_image.position) do
                      xml <<  agent_user.landscape_image[i].public_filename
                    end
                  end
                else
                  agent_user.landscape_image.each_with_index do |landscape_image, i|
                    if landscape_image.position.to_i == 1
                      xml.landscape_image(:position => landscape_image.position) do
                        xml <<  agent_user.landscape_image[i].public_filename
                      end
                      xml.landscape_image(:position => 2) do
                        xml << translate_url(WillPaginateHelpers.param_request_host).to_s + "/images/landscape2_default_avatar.jpg"
                      end
                    else
                      xml.landscape_image(:position => 1) do
                        xml << translate_url(WillPaginateHelpers.param_request_host).to_s + "/images/landscape1_default_avatar.jpg"
                      end
                      xml.landscape_image(:position => landscape_image.position) do
                        xml <<  agent_user.landscape_image[i].public_filename
                      end
                    end
                  end
                end
              end
            else
              xml.landscape_images do
                xml.landscape_image(:position => 1) do
                  xml << translate_url(WillPaginateHelpers.param_request_host).to_s + "/images/landscape1_default_avatar.jpg"
                end
                xml.landscape_image(:position => 2) do
                  xml << translate_url(WillPaginateHelpers.param_request_host).to_s + "/images/landscape2_default_avatar.jpg"
                end
              end
            end
            unless agent_user.portrait_image.blank?
              if agent_user.portrait_image.length == 2
                xml.portrait_images do
                  agent_user.portrait_image.each_with_index do |portrait_image, i|
                    xml.portrait_image(:position => portrait_image.position) do
                      xml <<  agent_user.portrait_image[i].public_filename
                    end
                  end
                end
              else
                xml.portrait_images do
                  agent_user.portrait_image.each_with_index do |portrait_image, i|
                    if portrait_image.position.to_i == 1
                      xml.portrait_image(:position => portrait_image.position) do
                        xml <<  agent_user.portrait_image[i].public_filename
                      end
                      xml.portrait_image(:position => 2) do
                        xml <<  translate_url(WillPaginateHelpers.param_request_host).to_s + "/images/portrait2_default_avatar.jpg"
                      end
                    else
                      xml.portrait_image(:position => 1) do
                        xml <<  translate_url(WillPaginateHelpers.param_request_host).to_s + "/images/portrait1_default_avatar.jpg"
                      end
                      xml.portrait_image(:position => portrait_image.position) do
                        xml <<  agent_user.portrait_image[i].public_filename
                      end
                    end
                  end
                end
              end
            else
              xml.portrait_images do
                xml.portrait_image(:position => 1) do
                  xml <<  translate_url(WillPaginateHelpers.param_request_host).to_s + "/images/portrait1_default_avatar.jpg"
                end
                xml.portrait_image(:position => 2) do
                  xml <<  translate_url(WillPaginateHelpers.param_request_host).to_s + "/images/portrait2_default_avatar.jpg"
                end
              end
            end

            unless agent_user.vcard.blank?
              xml.vcard do
                xml.tag!(:title, agent_user.vcard.filename)
                xml.tag!(:path, translate_url(WillPaginateHelpers.param_request_host).to_s + agent_user.vcard.public_filename)
              end
            else
              xml.vcard do
              end
            end
          end
        end

      else
        #properties
        require 'builder'
        xml = Builder::XmlMarkup.new(:indent => 2)
        xml.instruct!
        if ["ResidentialSale","ResidentialLease","Commercial","HolidayLease","ProjectSale","BusinessSale"].include?(self.first.class.name.to_s)
          xml.properties do
            xml.pagination("xmlns:xlink" => "http://www.w3.org/1999/xlink" ) do
              unless current_page == 1
                xml.tag!(:pagination, {"xlink:type" => "simple","rel" => "previous_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page - 1).to_s : url + (current_page - 1).to_s}"},current_page - 1)
              end
              xml.tag!(:current_page, {"rel" => "current_page"}, current_page)
              unless current_page == (total_entries).ceil
                xml.tag!(:pagination, {"xlink:type" => "simple","rel" => "next_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page + 1).to_s : url + (current_page + 1).to_s}" },current_page + 1)
              end
              xml.tag!(:per_page, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[per_page.class.name]}, per_page)
              xml.tag!(:total_entries, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]}, total_entries)
              xml.tag!(:total_pages, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]},flag == true  ? (total_entries/50.to_f).ceil : (total_entries/10.to_f).ceil )
            end.sub(%{record_type="array"}, %{record_type="collection"})

            self.each do |property|
              property.description = Iconv.new('UTF-8//IGNORE', 'UTF-8').iconv(property.description) unless property.blank?
              if property.status == 2 or property.status == 3
                begin
                  the_detail = property.detail.attributes
                  the_detail.delete_if {|x,y|['auction_date','auction_time'].include?(x)}
                rescue Exception => ex
                  the_detail = nil
                end
              else
                begin
                  the_detail = property.detail
                rescue Exception => ex
                  the_detail = nil
                end
              end
              xml.property do
                xml.tag!(:id, property.id)
                xml.tag!(:unique_id, property.property_id)
                xml.tag!(:xml_id, property.xml_id)
                xml.tag!(:third_contact_id, property.third_contact_id)
                xml.tag!(:headline, property.headline)
                xml.tag!(:description, property.description)
                xml.tag!(:country, property.country)
                xml.tag!(:deal_type, property.deal_type)
                xml.tag!(:display_address, property.display_address)
                xml.tag!(:display_price, property.display_price)
                xml.tag!(:display_price_text, property.display_price_text)
                xml.tag!(:latitude, property.latitude)
                xml.tag!(:longitude, property.longitude)
                xml.tag!(:price, property.price)
                xml.tag!(:to_price, property.to_price)
                xml.tag!(:price_period, "Weekly") if %w( Commercial ResidentialLease ).include?(property.type)
                xml.tag!(:property_type, property.property_type)
                xml.tag!(:save_status, property.save_status)
                xml.tag!(:state, property.state)
                xml.tag!(:status, property.status_name)
                xml.tag!(:street_number, property.street_number)
                xml.tag!(:street, property.street)
                xml.tag!(:suburb, property.suburb)
                xml.tag!(:town_village, property.town_village)
                xml.tag!(:unit_number, property.unit_number)
                xml.tag!(:vendor_email, property.vendor_email)
                xml.tag!(:vendor_first_name, property.vendor_first_name)
                xml.tag!(:vendor_last_name, property.vendor_last_name)
                xml.tag!(:vendor_phone, property.vendor_phone)
                xml.tag!(:zipcode, property.zipcode)
                xml.tag!(:sold_on, property.sold_on)
                xml.tag!(:sold_price, property.sold_price)
                xml.tag!(:leased_on, property.leased_on)
                xml.tag!(:leased_price, property.leased_price)
                xml.tag!(:agent_id, property.agent_id)
                xml.tag!(:office_id, property.office_id)
                xml.tag!(:type, property.type)
                xml.tag!(:show_price, property.show_price)
                unless the_detail.blank?
                  case property.type
                  when "ResidentialSale"
                    xml << the_detail.to_xml(:skip_types => true, :skip_instruct => true, :root => "detail")
                  when "ProjectSale"
                    xml << the_detail.to_xml(:skip_types => true, :skip_instruct => true, :root => "detail")
                  when "HolidayLease"
                    xml << the_detail.to_xml(:skip_types => true, :skip_instruct => true, :root => "detail")
                    xml << property.rental_seasons.to_xml(:skip_types => true, :skip_instruct => true, :root => 'rental_seasons', :except => [:id, :holiday_lease_id])
                  else
                    xml << the_detail.to_xml(:skip_types => true, :skip_instruct => true, :root => "detail")
                  end
                end
                unless property.features.blank?
                  xml << property.features.to_xml(:skip_types => true, :skip_instruct => true, :root => "features", :only => [:name])
                end
                unless property.opentimes.blank?
                  xml << property.opentimes.to_xml(:skip_types => true, :skip_instruct => true, :root => "opentimes", :only => [:date], :methods => [:starttime, :endtime]) unless property.status == 2 or property.status == 3
                end
                unless property.primary_contact.blank?
                  xml << property.primary_contact.to_xml(:skip_types => true, :skip_instruct => true, :root => "primary_contact", :only => [:id, :first_name, :last_name])
                end
                unless property.secondary_contact.blank?
                  xml << property.secondary_contact.to_xml(:skip_types => true, :skip_instruct => true, :root => "secondary_contact", :only => [:id, :first_name, :last_name]) unless property.secondary_contact_id.blank?
                end
                #              unless property.images.blank?
                xml.photos do
                  property.images.each_with_index do |photo, i|
                    xml.photo(:position => i) do
                      xml.tag!(:large, photo.public_filename)
                      xml.tag!(:medium, photo.public_filename(:medium)) rescue ''
                      xml.tag!(:thumb, photo.public_filename(:thumb)) rescue ''
                      xml.tag!(:original, photo.public_filename(:original)) rescue ''
                    end
                  end
                end
                #              end

                #              unless property.floorplans.blank?
                xml.floorplans do
                  property.floorplans.each_with_index do |floorplan, i|
                    xml.floorplan(:position => i) do
                      xml.tag!(:large, floorplan.public_filename)
                      xml.tag!(:medium, floorplan.public_filename(:medium)) rescue ''
                      xml.tag!(:thumb, floorplan.public_filename(:thumb)) rescue ''
                      xml.tag!(:original, floorplan.public_filename(:original)) rescue ''
                    end
                  end
                end
                #              end

                #              unless property.brochure.blank?
                xml.brochures do
                  for i in 0..3 do
                    brochure = {}
                    brochure['title'] = property.brochure[i].blank? ? '' : property.brochure[i].title
                    brochure['public_filename'] = property.brochure[i].blank? ? '' : property.brochure[i].public_filename
                    xml.brochure(:position => i) do
                      xml.tag!(:title, brochure['title'])
                      xml.tag!(:large, brochure['public_filename'])
                    end
                  end
                end

                xml.tag!(:audio, property.mp3.blank? ? nil : translate_url(WillPaginateHelpers.param_request_host).to_s + property.mp3.public_filename.to_s )

                xml.property_videos do
                  unless property.property_videos.blank?
                    property.property_videos.each_with_index do |video, i|
                      xml.videos do
                        xml.tag!(:title, video.title)
                        xml.tag!(:youtube_id, video.youtube_id)
                        xml.tag!(:embed_code, video.embed_code)
                        xml.tag!(:status, video.status)
                      end
                    end
                  end
                end
                #              end
                unless property.extra.blank?
                  the_extra = property.extra.attributes
                  the_extra.delete_if {|x,y|['created_at', 'updated_at', 'property_id', 'id', 'office_id'].include?(x)}
                  xml << the_extra.to_xml(:skip_types => true, :skip_instruct => true, :root => "extra")
                end

                xml.tag!(:updated_at, property.updated_at)
                xml.tag!(:created_at, property.created_at)
              end
            end
          end
        else
          if ["AgentUser","Office","NilClass"].include?(self.first.class.name.to_s)

            self.to_xml_without_collection_type(options) do |data|
              data.pagination("xmlns:xlink" => "http://www.w3.org/1999/xlink" ) do
                unless current_page == 1
                  data.tag!(:pagination, {"xlink:type" => "simple","rel" => "previous_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page - 1).to_s : url + (current_page - 1).to_s}"},current_page - 1)
                end
                data.tag!(:current_page, {"rel" => "current_page"}, current_page)
                unless current_page == (total_entries/10.to_f).ceil
                  data.tag!(:pagination, {"xlink:type" => "simple","rel" => "next_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page + 1).to_s : url + (current_page + 1).to_s}" },current_page + 1)
                end
                data.tag!(:per_page, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[per_page.class.name]}, per_page)
                data.tag!(:total_entries, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]}, total_entries)
                data.tag!(:total_pages, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]},(total_entries/10.to_f).ceil )
              end.sub(%{record_type="array"}, %{record_type="collection"})
            end
            require 'builder'
            xml = Builder::XmlMarkup.new(:indent => 2)
            xml.instruct!
            xml.offices do
              xml.pagination("xmlns:xlink" => "http://www.w3.org/1999/xlink" ) do
                unless current_page == 1
                  xml.tag!(:pagination, {"xlink:type" => "simple","rel" => "previous_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page - 1).to_s : url + (current_page - 1).to_s}"},current_page - 1)
                end
                xml.tag!(:current_page, {"rel" => "current_page"}, current_page)
                unless current_page == (total_entries).ceil
                  xml.tag!(:pagination, {"xlink:type" => "simple","rel" => "next_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page + 1).to_s : url + (current_page + 1).to_s}" },current_page + 1)
                end
                xml.tag!(:per_page, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[per_page.class.name]}, per_page)
                xml.tag!(:total_entries, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]}, total_entries)
                xml.tag!(:total_pages, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]},flag == true  ? (total_entries/50.to_f).ceil : (total_entries/10.to_f).ceil )
              end.sub(%{record_type="array"}, %{record_type="collection"})
              self.each do |office|
                xml.office do
                  xml.tag!("agent-id", office.agent_id)
                  xml.tag!("code", office.code)
                  xml.tag!("country", office.country)
                  xml.tag!("created-at", office.created_at)
                  xml.tag!("deleted-at", office.deleted_at)
                  xml.tag!("developer-contact-id", office.developer_contact_id)
                  xml.tag!("email", office.email)
                  xml.tag!("fax-number", office.fax_number)
                  xml.tag!("head", office.head)
                  xml.tag!("id", office.id)
                  xml.tag!("latitude", office.latitude)
                  xml.tag!("longitude", office.longitude)
                  xml.tag!("name", office.name)
                  xml.tag!("office-contact", office.office_contact)
                  xml.tag!("office-paypal-account", office.office_paypal_account)
                  xml.tag!("phone-number", office.phone_number)
                  xml.tag!("property-draft-for-new", office.property_draft_for_new)
                  xml.tag!("property-draft-for-updated", office.property_draft_for_updated)
                  xml.tag!("property_listing_cost", office.property_listing_cost)
                  xml.tag!("property_owner_access", office.property_owner_access)
                  xml.tag!("property-payment-required", office.property_payment_required)
                  xml.tag!("property-url", office.property_url)
                  xml.tag!("property-with-owner-details", office.property_with_owner_details)
                  xml.tag!("state", office.state)
                  xml.tag!("status", office.status)
                  xml.tag!("street", office.street)
                  xml.tag!("street-number", office.street_number)
                  xml.tag!("street-type", office.street_type)
                  xml.tag!("suburb", office.suburb)
                  xml.tag!("support-contact-id", office.support_contact_id)
                  xml.tag!("unit-number", office.unit_number)
                  xml.tag!("updated-at", office.updated_at)
                  xml.tag!("upgrade-contact-id", office.upgrade_contact_id)
                  xml.tag!("url", office.url)
                  xml.tag!("zipcode", office.zipcode)
                  xml.tag!("notes_for_property", office.notes_for_property)
                  if(!url.scan("developer_id").blank?)
                    xml.tag!("logo1", office.logos.first.blank? ? nil : office.logos.first.public_filename)
                    xml.tag!("logo2", office.logos.second.blank? ? nil : office.logos.second.public_filename)
                  end
                  xml.domains do
                    if !office.domains.blank?
                      office.domains.each do |domain|
                        xml.tag!(:name, domain.name)
                      end
                    else
                      xml.tag!(:name, '')
                    end
                  end
                end
              end
            end


          end
        end
      end

    else

      unless WillPaginateHelpers.param_mobile_service.blank?
        WillPaginateHelpers.param_mobile_service = nil
        require 'builder'
        xml = Builder::XmlMarkup.new(:indent => 2)
        xml.instruct!
        xml.properties do
          xml.pagination("xmlns:xlink" => "http://www.w3.org/1999/xlink" ) do
            unless current_page == 1
              xml.tag!(:pagination, {"xlink:type" => "simple","rel" => "previous_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page - 1).to_s : url + (current_page - 1).to_s}"},current_page - 1)
            end
            xml.tag!(:current_page, {"rel" => "current_page"}, current_page)
            unless current_page == (total_entries).ceil
              xml.tag!(:next_page, {"xlink:type" => "simple","rel" => "next_page","xlink:href" =>"#{url.scan("page").blank? ? url +"&page="+ (current_page + 1).to_s : url + (current_page + 1).to_s}" },current_page + 1)
            end
            xml.tag!(:per_page, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[per_page.class.name]}, per_page)
            xml.tag!(:total_entries, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]}, total_entries)
            xml.tag!(:total_pages, {:record_type => ActiveSupport::CoreExtensions::Hash::Conversions::XML_TYPE_NAMES[total_entries.class.name]},flag == true  ? (total_entries/50.to_f).ceil : (total_entries/10.to_f).ceil )
          end.sub(%{record_type="array"}, %{record_type="collection"})

          self.each do |property|
            next if property.blank?
            property.description = Iconv.new('UTF-8//IGNORE', 'UTF-8').iconv(property.description)
            if property.status == 2 or property.status == 3
              begin
                the_detail = property.detail.attributes
                the_detail.delete_if {|x,y|['auction_date','auction_time'].include?(x)}
              rescue Exception => ex
                the_detail = nil
              end
            else
              begin
                the_detail = property.detail
              rescue Exception => ex
                the_detail = nil
              end
            end
            xml.property do
              xml.tag!(:id_property, property.id)
              xml.tag!(:suburb, property.suburb)
              xml.tag!(:headline, property.headline)
              xml.tag!(:description, property.description)
              xml.tag!(:latitude, property.latitude)
              xml.tag!(:longitude, property.longitude)
              xml.tag!(:status, Property::STATUS_NAME[property.status.to_i - 1])
              xml.tag!(:price, property.price)
              xml.tag!(:display_price, property.display_price)
              xml.tag!(:display_price_text, property.display_price_text)
              if (property.class.to_s != "BusinessSale" && property.class.to_s != "Commercial")
                xml.tag!(:bedrooms, property.detail.bedrooms)
                xml.tag!(:bathrooms, property.detail.bathrooms)
                xml.tag!(:carspaces, property.detail.carport_spaces)
              else
                xml.tag!(:bedrooms, nil)
                xml.tag!(:bathrooms, nil)
                xml.tag!(:carspaces, nil)
              end
              xml.tag!(:property_type, property.property_type)
              xml.tag!(:address, property.display_street.blank? ? "" : property.display_street.to_s)
              xml.tag!(:type, property.type)
              xml.tag!(:deal_type, property.deal_type)
              xml.tag!(:sell_type, property.lease? ? "Lease" : "Sale")
              if property.images.blank?
                xml.tag!(:photo, nil)
              else
                xml.tag!(:photo, "#{property.images.first.public_filename(:thumb)}")
              end
              xml.tag!(:updated_at, property.updated_at)
            end
          end
        end
      end

    end

  end
  alias_method_chain :to_xml, :collection_type

  def translate_url(text)
    return 'http://' + text.gsub('http://', '') unless text.blank?
  end
end
WillPaginate::Collection.send(:include, WillPaginateHelpers)
