class FlashUploaderSenderApi < Struct.new(:property_id, :office_id, :current_user_id, :type)
  def perform
    office = Office.find(office_id)
    property = Property.find(property_id)
    curr_user = User.find_by_id(current_user_id)
    if office.edit_alert && curr_user.allow_property_owner_access?
      dont_send = true 
    end
    if property
      property.updated_at = Time.now
      property.status = 6 if curr_user.allow_property_owner_access?
      property.image_exist = true if type == "Image"
      property.floorplan_exist = true if type == "Plan"
      property.save(false)
    end
    if office
      office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles").each do |user|
        UserMailer.deliver_media_notification_for_level1(user, curr_user, office, property, type) if curr_user.allow_property_owner_access? and office.property_owner_access? and dont_send.blank?
      end
    end
  end
end
