class AgentUserApiScheduler < Struct.new(:host, :agent_user_id)
  include Jobs::ScheduledJob

  run_every 90.seconds

  def perform
    agent_user = AgentUser.find_by_sql("SELECT * FROM users WHERE (`processed` = 0 OR `processed` IS NULL) AND `count_api_sent` < 3 AND type LIKE 'AgentUser' AND id = #{agent_user_id.to_i}").first
    unless agent_user.blank?
      unless agent_user.office.agent.blank?
        GLOBAL_ATTR["Host-#{agent_user.office_id}"] = host.blank? ? "#{agent_user.office.agent.developer.entry}.commercialloopcrm.com.au" : host
      else
        agent = Agent.find_by_sql("SELECT * FROM agents WHERE id = #{agent_user.office.agent_id.to_i}").first
        new_host = "#{agent.developer.entry}.commercialloopcrm.com.au"
        GLOBAL_ATTR["Host-#{agent_user.office_id}"] = host.blank? ? new_host : host
      end
      if !agent_user.blank?
          agent_user.send_agent_user_api
          agent_user.count_api_sent = agent_user.count_api_sent.to_i + 1
          sql = ActiveRecord::Base.connection()
          sql.update "UPDATE users SET count_api_sent = #{agent_user.count_api_sent} WHERE id = #{agent_user.id}"
          sql.commit_db_transaction
      else
        remove_delayed_jobs(agent_user_id.to_i)
      end
    else
      remove_delayed_jobs(agent_user_id.to_i)
    end
  end

  def remove_delayed_jobs(au_id)
    jobs = Delayed::Job.all
    id_jobs = []
    unless jobs.blank?
      jobs.each do |job|
        handler = job.handler.split
        id_jobs << job.id if handler[handler.size-2].include?("agent_user_id") && au_id.to_i == handler.last.to_i
      end
    end
    Delayed::Job.destroy(id_jobs) unless id_jobs.blank?
  end
end
