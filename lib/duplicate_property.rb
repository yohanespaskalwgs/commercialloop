class DuplicateProperty < Struct.new(:parent_property_id, :new_property_id)
  def perform
      require 'open-uri'
      parent_property = Property.find_by_id(parent_property_id)
      new_property = Property.find_by_id(new_property_id)
      unless parent_property.images.blank?
        #========Copying Image========
        parent_property.images.each{ |src|
          begin
            fname = "#{new_property_id}-#{src.filename}"
            open("tmp/cache/#{fname}", 'wb') do |file|
              file << open(src.public_filename).read
            end
            img = new_property.images.new(:uploaded_data => File.new("tmp/cache/#{fname}"))
            img.save!
            File.delete("tmp/cache/#{fname}")
          rescue Exception => ex
            Log.create({:message => "Duplicate img err: #{ex.inspect}"})
          end
        }
        unless new_property.images.blank?
          new_property.images.each do |img|
            ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{img.position} WHERE parent_id = #{img.id}")
          end
        end
      end
      unless parent_property.floorplans.blank?
        #========Copying Floorplan========
        parent_property.floorplans.each{ |fsrc|
          begin
            fname = "#{new_property_id}-#{fsrc.filename}"
            open("tmp/cache/#{fname}", 'wb') do |file|
              file << open(fsrc.public_filename).read
            end
            flr = new_property.floorplans.new(:uploaded_data => File.new("tmp/cache/#{fname}"))
            flr.save!
            File.delete("tmp/cache/#{fname}")
          rescue
          end
        }
        unless new_property.floorplans.blank?
          new_property.floorplans.each do |img|
            ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{img.position} WHERE parent_id = #{img.id}")
          end
        end
      end
      unless parent_property.brochure.blank?
        #========Copying Brochure========
        parent_property.brochure.each{ |brc_src|
          begin
            fname = "#{new_property_id}-#{brc_src.filename}"
            open("tmp/cache/#{fname}", 'wb') do |file|
              file << open(brc_src.public_filename).read
            end
            brc = new_property.brochure.new(:uploaded_data => File.new("tmp/cache/#{fname}"))
            brc.save!
            File.delete("tmp/cache/#{fname}")
          rescue
          end
        }
      end
      unless parent_property.mp3.blank?
        #========Copying Mp3 ========
        begin
          audio = Mp3.new(:uploaded_data => File.new(parent_property.mp3.full_filename))
          audio.attachable = new_property
          audio.save!
        rescue
        end
      end
  end
end
