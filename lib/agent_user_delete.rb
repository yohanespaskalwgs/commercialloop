class AgentUserDelete < Struct.new(:host, :agent_user_id)  
  def perform
    agent_user = AgentUser.find_by_sql("SELECT * FROM users WHERE type LIKE 'AgentUser' AND id = #{agent_user_id.to_i}").first
    GLOBAL_ATTR["Host-#{agent_user.office_id}"] = host.blank? ? "#{agent_user.office.agent.developer.entry}.commercialloopcrm.com.au" : host
    agent_user.send_agent_user_notification
  end
end
