
desc "touch restart.txt, DEBUG=true for ruby-debug"
task :restart do
  system("touch tmp/restart.txt")
  # make sure to delete debug.txt if not in debug mode
  if File.exists?(File.join(RAILS_ROOT,'tmp', 'debug.txt'))
    File.delete(File.join(RAILS_ROOT,'tmp', 'debug.txt'))
  end
  system("touch tmp/debug.txt") if ENV["DEBUG"] == 'true'
  
end

