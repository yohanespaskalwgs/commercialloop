namespace :reminder do

  desc "send email notification for lease expiry reminder"
  task :whn_check_for_lease_reminder => :environment do
    tenancies = TenancyRecord.find(:all, :include => [:agent_contact], :conditions => "is_notified = '0' AND agent_contact_id IS NOT NULL AND is_deleted = '0' AND reminder_alert <= '#{Date.today.strftime('%Y-%m-%d')}'")
    tenancies.each do |tenancy|
      contact = tenancy.agent_contact
      TenancyRecord.transaction do
        if tenancy.reminder_alert <= Date.today.strftime('%Y-%m-%d')
          UserMailer.deliver_lease_reminder(contact,tenancy)
          ActiveRecord::Base.connection.execute("UPDATE tenancy_records SET is_notified=1 WHERE ID=#{tenancy.id}")
        end                
      end
    end
  end
  
end