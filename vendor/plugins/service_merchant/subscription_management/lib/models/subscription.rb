class Subscription < ActiveRecord::Base
  default_scope :order => 'subscriptions.created_at DESC'


  has_many :subscription_profiles, :dependent => :destroy
  has_many :recurring_payment_profiles, :through => :subscription_profiles, :dependent => :destroy
  has_many :subscription_histories

  belongs_to :developer, :class_name => "Developer", :foreign_key => :account_id
	belongs_to :office # @author: Budhi

  named_scope :active, :conditions => {:status => "ok"}
  named_scope :inactive, :conditions => {:status => ["ok","pending","canceled"]},  :order => "updated_at DESC"
  named_scope :pending, :conditions => {:status => "pending"},  :order => "updated_at DESC"
  named_scope :canceled, :conditions => {:status => "canceled"}, :order => "updated_at DESC"

  extend ActiveSupport::Memoizable
  
  
  def profile_id
    recurring_payment_profile.gateway_reference
  end

  def recurring_payment_profile
    recurring_payment_profiles.first
  end
  memoize :recurring_payment_profile
    
  # Returns billing amount (sum of net amount + taxes) 
  def billing_amount
    self.net_amount + self.taxes_amount
  end

  # Recalculate price and taxes
  # 
  # subscription_config should be initialized SubscriptionManagement instance
  def recalc_price(subscription_config)
    total_tax = subscription_config.all_taxes[self.taxes_id]["taxes"].inject(0){|sum,item| sum + item["rate"]} # sum all applicable taxes
    self.net_amount = self.quantity * subscription_config.all_tariff_plans[self.tariff_plan_id]["price"]
    self.taxes_amount = self.net_amount * total_tax
  end

  def update_quantity(qty, subscription_config)
    self.quantity = qty
    self.recalc_price(subscription_config)
    self.save
  end

  protected
  
 
  
end
