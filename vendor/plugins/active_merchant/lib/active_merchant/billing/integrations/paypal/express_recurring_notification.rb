require 'net/http'

module ActiveMerchant #:nodoc:
  module Billing #:nodoc:
    module Integrations #:nodoc:
      module Paypal
        # Parser and handler for incoming Instant payment notifications from paypal. 
        # The Example shows a typical handler in a rails application. Note that this
        # is an example, please read the Paypal API documentation for all the details
        # on creating a safe payment controller.
        #
        # Example
        #  
        #   class BackendController < ApplicationController
        #     include ActiveMerchant::Billing::Integrations
        #
        #     def paypal_ipn
        #       notify = Paypal::ExpressRecurringNotification.new(request.raw_post)
        #   
        #       order = Order.find(notify.item_id)
        #     
        #       if notify.acknowledge 
        #   
        #         rescue => e
        #           order.status        = 'failed'      
        #           raise
        #         ensure
        #           order.save
        #         end
        #       end
        #   
        #       render :nothing
        #     end
        #   end
        class ExpressRecurringNotification < ActiveMerchant::Billing::Integrations::Paypal::Notification

          # Status of transaction. List of possible values:
          # "Active"
          # "Cancelled"
          def status
            params['profile_status']
          end
          
          def profile_id
            params["recurring_payment_id"]
          end          

          def product_name
            params["product_name"]
          end
           
          [:payer_email, :payer_id, :amount, :currency_code, :profile_status, :txn_type].each do |method|
             define_method method do
                params[method.to_s]
             end
          end
          
        end
      end
    end
  end
end
