require 'active_record'
require 'view_extensions'

module Adocca
  module Acts
    module ActsAsCached

      class CacheObserver

        # class_name:  The name of the class whose namespace we'll invalidate
        # id_column:   The column of the monitored object that contains the id
        #              of the object we'll invalidate
        # type_column: For polymorphic associations, this is the column of the
        #              monitored object that contains the class name of the
        #              associated object. E.g. for trigs, "triggable_type"

        def initialize(class_name, id_column, type_column = nil)
          @class_name, @id_column, @type_column = class_name, id_column, type_column
        end

        def update(method_name, object)
          if method_name == :after_save || method_name == :after_destroy
            # in case of polymorphic associations, make sure the monitored object
            # is attached to the right type
            if @type_column.nil? || object[@type_column] == @class_name
              namespace = "#{@class_name}:#{object[@id_column]}"
              #puts 'Invalidating: ' + namespace + ' for change on object ' + object.class.name
              RAILS_DEFAULT_LOGGER.debug "ActsAsCached, Invalidating namespace: #{namespace}"
              CACHE.invalidate_namespace(namespace)
            end
          end
          true
        end
      end

      def self.append_features(base)
        super
        base.extend(ClassMethods)
      end

      module ClassMethods
        def acts_as_cached(options = {})
          if assocs = options[:depends]
            assocs = [ assocs ] if assocs.kind_of? Symbol

            assocs.each do |assoc|
              unless reflection = reflect_on_association(assoc)
                raise "Association not found"
              end

              # For trough reflections, we need to observe both the
              # intermediate model (e.g. Tagging for tags) and the
              # and the associated (Tag for tags)
              # For now, only the through reflection since it's pain
              # in the ASS to do this correctly. This works for tags :)
              if through = reflection.through_reflection
                reflection = through
              end

              if as = reflection.options[:as]
                type_column = "#{as}_type"
              else
                type_column = nil
              end

              reflection.klass.add_observer CacheObserver.new(self.name, reflection.primary_key_name, type_column)
            end
          end

          self.add_observer CacheObserver.new(self.name, self.primary_key.to_sym)
        end
      end
    end
  end
end

ActiveRecord::Base.send(:include, Adocca::Acts::ActsAsCached)
