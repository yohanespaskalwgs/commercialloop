require 'set'

ActionView::Base.class_eval do
  class RenderOptions

    def initialize(*init_values)
      @set = Set.new(init_values)
    end

    def <<(key)
      @set << key
    end

    def length
      @set.length
    end

    alias_method :add, :<<

    def method_missing(name)
      if name.to_s =~ /(\w+)\?/
        return @set.include?($1.to_sym)
      else
        super
      end
    end

    def to_key
      @set.to_a.collect{ |sym| sym.to_s }.sort.join(':')
    end

  end

  # Will render and cache a partial
  # If the object has been removed or doesn't exists nothing will be rendered

  def render_object(object, view, options={})
    options[:object] = object
    do_render_object(view, options)
  end

 
  def render_object_klass_id(klass, id, view, options = {})
    options[:klass] = klass
    options[:id] = id
    do_render_object(view, options)
  end

  private
    

  def do_render_object(view, options={})

    klass, id = 
      if options[:object]
        [options[:object].class, options[:object].id]
      else
        [options[:klass], options[:id]]
      end
    
    unless klass && id
      raise "options[:object] or options[:id] and options[:klass] must be specified"
    end
    
    id = id.to_i

    logger.info "render_cached: class: #{klass.name} :id #{id} view: #{view} options #{options.inspect}"


    # XXX validate that object is an acts_as_cached  FIXME

    namespace = "#{klass.name}:#{id}"
    if render_options = options[:options]
      key = "#{view}:#{render_options.to_key}"
    else
      render_options = RenderOptions.new
      key = view
    end

    fragment_proc = Proc.new do
      partial = options[:partial]
      class_lower = klass.name.underscore
      unless partial
        if directory = options[:directory]
          partial = "#{directory}/#{class_lower}_#{view}"
        else
          partial = "#{class_lower}_#{view}"
        end
      end
      begin 
        
        object = view_extensions_fetch_object(options)
        render :partial => partial, :locals => { class_lower.to_sym => object, :options => render_options }
      rescue ActiveRecord::RecordNotFound => e
        ''
      end
    end

    unless options[:cache] == false
      fragment = cache_value([namespace, key], options[:expiry] || 0, &fragment_proc) 
    else
      fragment = fragment_proc.call
    end

    if transformer = options[:transform]
      
      begin 
        object = view_extensions_fetch_object(options)

        case transformer
        when Symbol
          return send(transformer, object, fragment)
        when Proc
          return transformer.call(object, fragment)
        else
          raise TypeError, "Transformer must be either symbol or proc"
        end
      rescue ActiveRecord::RecordNotFound => e
        ''
      end
    else
      return fragment
    end

  end
  
  def view_extensions_fetch_object(options)
    if options[:object]
      options[:object]
    else
      options[:klass].find(options[:id].to_i)
    end
  end

end

