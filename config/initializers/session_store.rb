# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :session_key => '_commercial_loop_crm_session',
  :secret      => 'ce3a9dc06e73fa64ca6bee5747815b1736d430bd5f606d74b14bec72c07f5f767e2e9517898dfaa5a901ed8d8d5748a2751348e8a8f2bf5bdb9fa28ecf8c923f'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
