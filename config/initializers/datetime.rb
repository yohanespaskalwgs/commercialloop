ActiveSupport::CoreExtensions::Time::Conversions::DATE_FORMATS.merge!(
   :detail => "%Y-%m-%d %H:%M",
   :brief => "%e %B, %Y",
   :short => "%d-%m-%y",
   :db_format => "%Y-%m-%d %H:%M:%S"
)