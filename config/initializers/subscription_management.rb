SubscriptionManager = SubscriptionManagement.new(
          :tariff_plans_config  => Rails.root.join("config" , 'subscription_plans.yml'),
          :taxes_config         => Rails.root.join("config", "taxes.yml"),
          :gateways_config      => Rails.root.join("config", "payment_gateways.yml"),
          :gateway => :paypal_express_recurring
          )