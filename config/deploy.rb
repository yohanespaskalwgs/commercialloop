set :default_stage, "commercialloopcrm"
require "capistrano/ext/multistage"


set :application, "commercial_loop_crm"
set :repository,  "git@gitlab.kiranatama.net:222/commercialloop_zoo_property_clone_.git"

default_run_options[:pty] = true
set :scm, "git"

# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
set :deploy_to, "/var/www/#{application}"

# Thinking Sphinx
namespace :thinking_sphinx do
  task :configure, :roles => [:app] do
    run "cd #{current_path}; rake ts:config RAILS_ENV=production"
  end
  task :index, :roles => [:app] do
    run "cd #{current_path}; rake ts:in RAILS_ENV=production"
  end
  task :start, :roles => [:app] do
    run "cd #{current_path}; rake ts:start RAILS_ENV=production"
  end
  task :stop, :roles => [:app] do
    run "cd #{current_path}; rake ts:stop RAILS_ENV=production"
  end
  task :restart, :roles => [:app] do
    run "cd #{current_path}; rake ts:restart RAILS_ENV=production"
  end
end

# Thinking Sphinx typing shortcuts
namespace :ts do
  task :configure, :roles => [:app] do
    run "cd #{current_path}; rake ts:config RAILS_ENV=production"
  end
  task :in, :roles => [:app] do
    run "cd #{current_path}; rake ts:in RAILS_ENV=production"
  end
  task :start, :roles => [:app] do
    run "cd #{current_path}; rake ts:start RAILS_ENV=production"
  end
  task :stop, :roles => [:app] do
    run "cd #{current_path}; rake ts:stop RAILS_ENV=production"
  end
  task :restart, :roles => [:app] do
    run "cd #{current_path}; rake ts:restart RAILS_ENV=production"
  end
end

# Make symlink for for app
after "deploy:symlink", "app:symlink", "deploy:update_crontab"
namespace :app do
  task :symlink do
    run "cd #{deploy_to}/current/config && rm -f database.yml && ln -s #{shared_path}/config/database.yml"
    run "cd #{deploy_to}/current/config && ln -s #{shared_path}/config/production.sphinx.conf"
    run "cd #{deploy_to}/current/config && rm -f payment_gateways.yml && mv payment_gateways.production.yml payment_gateways.yml"
    run "cd #{deploy_to}/current/public && rm -rf attachments && ln -s #{shared_path}/public/attachments"
  end

  task :after_symlink do
    run "ln -nfs #{shared_path}/public/xml_file #{current_path}/public/xml_file"
  end
end

namespace :deploy do
  task :before_update do
    # Stop Thinking Sphinx before the update so it finds its configuration file.
    ts.stop
  end

  task :after_update do
    symlink_sphinx_indexes
    ts.in
    ts.start
  end

  desc "Link up Sphinx's indexes."
  task :symlink_sphinx_indexes, :roles => [:app] do
    run "ln -nfs #{shared_path}/db/sphinx #{current_path}/db/sphinx"
  end

  desc "Update the crontab file"
  task :update_crontab, :roles => :db do
    run "cd #{release_path} && whenever --update-crontab #{application}"
  end
end