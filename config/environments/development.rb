# Settings specified here will take precedence over those in config/environment.rb

# In the development environment your application's code is reloaded on
# every request.  This slows down response time but is perfect for development
# since you don't have to restart the webserver when you make code changes.
config.cache_classes = false

# Log error messages when you accidentally call methods on nil.
config.whiny_nils = true

# Show full error reports and disable caching
config.action_controller.consider_all_requests_local = true
config.action_view.debug_rjs                         = true
config.action_controller.perform_caching             = true

# Don't care if the mailer can't send
config.action_mailer.raise_delivery_errors = true

config.action_mailer.perform_deliveries = true
config.action_mailer.delivery_method = :test
config.log_level = :debug


DOMAIN_NAMES    = %w(zoopdev.com vhost.com)
DOMAIN_NAME     = 'zoopdev.com'
BASE_URL        = 'http://localhost:3000'

if `hostname`.strip == "rexchung-laptop" # localzoo.com
  GOOGLE_MAPS_KEY = "ABQIAAAAICnTwmaKUgI2xwMDuGzXrhRcOHNFMVcEGcFHsTf0nCibsUIzlBQQ0DmALHoWoZCsvNrc9tiLpiG6vg"
else
  GOOGLE_MAPS_KEY = 'ABQIAAAARl4Fnv3RDN8f-zFyA8lGxBQGg63fVThnK5Ye4HVbxW7E3djQLBQhCXZnZBUeVLDvCB1grFTbYgy-yg'
end


Paperclip.options[:image_magick_path] = "/opt/local/bin"


if File.exists?(File.join(RAILS_ROOT,'tmp', 'debug.txt'))
  require 'ruby-debug'
  Debugger.wait_connection = true
  Debugger.start_remote
  File.delete(File.join(RAILS_ROOT,'tmp', 'debug.txt'))
end


# also config in payment_gateways.yml for servicemerchant
config.after_initialize do
  ActiveMerchant::Billing::Base.mode = :test
  paypal_options = {
    #:login => "rex_1239802854_biz_api1.rorcraft.com",
    #:password => "1239802866",
    #:signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31AVB8-9Wp3USt468hAz8mrf7xJepR "
    :login => "sel__1255597104_biz_api1.yahoo.com",
    :password => "1255597115",
    :signature => "Awsr.tgJlF4hQJCs02xOeXE5GZq7A71Y01ihMRinfsgHZ.Eeb1VuER9Z"
  }

  ::EXPRESS_GATEWAY = ActiveMerchant::Billing::PaypalExpressRecurringGateway.new( paypal_options )
end

PAYPAL_TIMEZONE = 'Etc/GMT-7'

