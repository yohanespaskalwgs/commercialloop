set :user, "deploy"
set :password, "r0rcr4ft"

set :scm_username, "railsdeploy"

role :app, "commercialloopcrm.com.au"
role :web, "commercialloopcrm.com.au"
role :db,  "commercialloopcrm.com.au", :primary => true