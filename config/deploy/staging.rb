set :user, "rails2"
set :password, "railsonruby"

role :app, "zooproperty.rorcraft.com"
role :web, "zooproperty.rorcraft.com"
role :db,  "zooproperty.rorcraft.com", :primary => true

namespace :deploy do

  desc "Restart mongrel"
  task :restart, :roles => :app do
    run "cd #{deploy_to}/current && cat tmp/pids/server.pid | xargs kill -9"
    run "cd #{deploy_to}/current && ./script/server -e production -p 1230 -d"
    run "cd #{deploy_to}/current && ./script/ferret_server -e production stop && ./script/ferret_server -e production start"
  end

  desc "Start mongrel"
  task :start, :roles => :app do
    run "cd #{deploy_to}/current && /home/rails2/usr/local/bin/ruby script/server -e production -p 1230 -d"
  end

  desc "Stop mongrel"
  task :stop, :roles => :app do
    run "cd #{deploy_to}/current && cat tmp/pids/server.pid | xargs kill -9"
  end

end

after "deploy:symlink", "app:symlink"
namespace :app do
  task :symlink do
    run "rm -rf #{deploy_to}/current/tmp; ln -s #{deploy_to}/shared/tmp #{deploy_to}/current/tmp"
    run "rm #{deploy_to}/current/config/database.yml; ln -s #{deploy_to}/shared/database.yml #{deploy_to}/current/config/database.yml"
    run "rm #{deploy_to}/current/config/ferret_server.yml; ln -s #{deploy_to}/shared/ferret_server.yml #{deploy_to}/current/config/ferret_server.yml"
    run "rm #{deploy_to}/current/config/mongrel_cluster.yml; ln -s #{deploy_to}/shared/mongrel_cluster.yml #{deploy_to}/current/config/mongrel_cluster.yml"
    run "chmod 766 #{deploy_to}/current/script/ferret_server"
  end
end