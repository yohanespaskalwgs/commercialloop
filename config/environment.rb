# Be sure to restart your server when you modify this file

# Uncomment below to force Rails into production mode when
# you don't control web/app server and can't set it the proper way
# ENV['RAILS_ENV'] ||= 'production'

# Specifies gem version of Rails to use when vendor/rails is not present
#RAILS_GEM_VERSION = '2.3.2' unless defined? RAILS_GEM_VERSION
RAILS_GEM_VERSION = '2.3.18' unless defined? RAILS_GEM_VERSION

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # -- all .rb files in that directory are automatically loaded.
  # See Rails::Configuration for more options.

  # Skip frameworks you're not going to use. To use Rails without a database
  # you must remove the Active Record framework.
  # config.frameworks -= [ :active_record, :active_resource, :action_mailer ]

  # Specify gems that this application depends on.
  # They can then be installed with "rake gems:install" on new installations.
  # You have to specify the :lib option for libraries, where the Gem name (sqlite3-ruby) differs from the file itself (sqlite3)
  # config.gem "bj"
  # config.gem "hpricot", :version => '0.6', :source => "http://code.whytheluckystiff.net"
  # config.gem "sqlite3-ruby", :lib => "sqlite3"
  config.gem "aws-s3", :lib => "aws/s3"
  require 'scheduler'
  require 'redis'
  #  require "savon"
  require 'bluecloth'
  require 'sanitize'
  require 'smtp_tls'
  #require "#{Rails.root}/vendor/plugins/xero_gateway/lib/xero_gateway.rb"

  #  require "#{Rails.root}/vendor/plugins/memcachedb-client-0.0.2/lib/memcachedb" if not defined?(MemCacheDb)
  config.gem "geokit"
  config.gem "rcov", :lib => false
  config.gem "crafterm-comma", :lib => "comma"
  config.gem "acts_as_cached", :version => "0.1.2"
  config.gem "hobosupport", :lib => "hobosupport"
  config.gem 'delayed_job', :source => 'http://rubygems.org'
  config.gem 'daemons'
  config.gem "fastercsv"
  config.gem 'rdoc'

  #  config.gem 'javan-whenever', :lib => false, :source => 'http://gems.github.com'

  # config.gem "activemerchant", :lib => "active_merchant", :version => "1.3.2"

  # Only load the plugins named here, in the order given. By default, all plugins
  # in vendor/plugins are loaded in alphabetical order.
  # :all can be used as a placeholder for all plugins not explicitly named
  # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

  # Add additional load paths for your own custom dirs
  config.load_paths += Dir["#{Rails.root}/app/models/[_a-z]*"]
  GLOBAL_ATTR = {}
  # Force all environments to use the same logger level
  # (by default production uses :info, the others :debug)
  # config.log_level = :debug

  # Make Time.zone default to the specified zone, and make Active Record store time values
  # in the database in UTC, and return them converted to the specified local zone.
  # Run "rake -D time" for a list of tasks for finding time zone names. Comment line to use default local time.
  config.time_zone = 'Sydney'

  # The internationalization framework can be changed to have another default locale (standard is :en) or more load paths.
  # All files from config/locales/*.rb,yml are added automatically.
  # config.i18n.load_path << Dir[File.join(RAILS_ROOT, 'my', 'locales', '*.{rb,yml}')]
  # config.i18n.default_locale = :de

  # Your secret key for verifying cookie session data integrity.
  # If you change this key, all old sessions will become invalid!
  # Make sure the secret is at least 30 characters and all random,
  # no regular words or you'll be exposed to dictionary attacks.
  config.action_controller.session = {
    :session_key => '_commercial_loop_crm_session_key',
    :secret      => '273cb28519e69c4f0fee7839b9b5c9a2d056fee0a649e9ff83831bfc9b8a8367d2628a49c7b45c8b61e8fec02659df2c77'
  }

  # Use the database for sessions instead of the cookie-based default,
  # which shouldn't be used to store highly confidential information
  # (create the session table with "rake db:sessions:create")

  # caching
  config.action_controller.session_store = :active_record_store
  config.action_mailer.raise_delivery_errors = true
  config.action_controller.perform_caching = true

  # Mailer
  config.action_mailer.perform_deliveries = true
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :enable_starttls_auto => true,
    :address              => "smtp.gmail.com",
    :port                 => 587,
    :domain               => 'commercialloopcrm.com.au',
    :user_name            => 'commercialloopcrm2@gmail.com',
    :password             => 'kiranatama1234',
    :authentication       => 'plain'}
  config.action_mailer.default_url_options = { :host => 'commercialloopcrm.com.au' }
  # Use the memory store - this store has no options
  # Use the file store with a custom storage path (if the directory doesn’t already exist it will be created)

  # Use the memcached store with default options (localhost, TCP port 11211)
  # Use the memcached store with an options hash
  #config.cache_store = :mem_cache_store
  config.cache_store = :file_store, "public/page_cache/"
  # Use SQL instead of Active Record's schema dumper when creating the test database.
  # This is necessary if your schema can't be completely dumped by the schema dumper,
  # like if you have constraints or database-specific column types
  # config.active_record.schema_format = :sql

  # Activate observers that should always be running
  # Please note that observers generated using script/generate observer need to have an _observer suffix
  # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

  config.after_initialize do
    Gibberish.load_languages! rescue nil #if you don't rescue, you'll crash during migrations
  end

end

# require "#{Rails.root}/vendor/plugins/redis-session-store/lib/redis-session-store.rb"

Workling::Remote.dispatcher = Workling::Remote::Runners::StarlingRunner.new
memcache = YAML.load(File.open("#{RAILS_ROOT}/config/memcached.yml"))

memcache_options = {
  :c_threshold => memcache[RAILS_ENV]['c_threshold'],
  :compression => memcache[RAILS_ENV]['compression'],
  :debug => memcache[RAILS_ENV]['debug'],
  :namespace => memcache[RAILS_ENV]['namespace'],
  :readonly => memcache[RAILS_ENV]['readonly'],
  :urlencode => memcache[RAILS_ENV]['urlencode']
}
#CACHE = Redis.new(:host => '184.73.51.243', :port => 6379)

CACHE = MemCache.new memcache_options
CACHE.servers = memcache[RAILS_ENV]['servers']
#CACHE = MemCacheDb.new :servers=>'localhost:21201'
#ActionController::Base.session_store = RedisSessionStore
ActionController::Base.session_options[:expires] = 1800
ActionController::Base.session_options[:cache] = CACHE
require 'activerecord'
Time.zone = "Sydney"
class Time
  def now
    Time.zone.now
  end
end
ActiveRecord::Base.time_zone_aware_attributes = true
ActiveRecord::Base.default_timezone = 'Sydney'
ExceptionNotifier.exception_recipients = %w(rio.dermawan@kiranatama.com arif.qodari@wgs.co.id winarti@kiranatama.com)#%w(budhi.a@kiranatama.com ari.p@kiranatama.com james.k.s@kiranatama.com ryan@agentpoint.com log@commercialloopcrm.com.au)
ExceptionNotifier.sender_address = %w("Application Error"<error.notifier@commercialloopcrm.com.au>)


