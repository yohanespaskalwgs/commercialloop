jQuery(document).ready(function(){
 
    });
 
jQuery(function() {
    jQuery('#submit_geocode').click(function(){
        set_map();
        jQuery('#overlay').hide();
    });
    jQuery('#overlay').click(function(){
        set_map();
        jQuery('#overlay').hide();
    });
});

function set_map(){
    var mapOptions = {
        center: new google.maps.LatLng(-34.397, 150.644),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_div"),
        mapOptions);
    if(jQuery('#property_display_address_1').attr("checked") == true){
        address = jQuery('#property_unit_number').val() + ' ' + jQuery('#property_street_number').val() + ' ' + jQuery('#property_street').val().replace("'", "\'") + ',' + jQuery('#property_suburb').val() + ',' + jQuery('#property_zipcode').val() + ',' + jQuery('#property_state').val() + ',' + jQuery('#property_country').val();
    }else{
        address = jQuery('#property_suburb').val() + ',' + jQuery('#property_state').val() + ',' + jQuery('#property_country').val();
    }
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({
        'address':address
    },

    function(result, status){
        if (status == google.maps.GeocoderStatus.OK){
            map.setCenter(result[0].geometry.location);

            var marker = new google.maps.Marker({
                map: map,
                position: result[0].geometry.location
            });
            jQuery('#property_latitude').val(map.getCenter().lat());
            jQuery('#property_longitude').val(map.getCenter().lng());
            jQuery('#coordinate_latitude').text(map.getCenter().lat());
            jQuery('#coordinate_longitude').text(map.getCenter().lng());
            map.setZoom(15);
        }else{
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
    return false;
}

function search_by_address(country){
    var mapOptions = {
        center: new google.maps.LatLng(-34.397, 150.644),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_div"),
        mapOptions);
    var address = validate_address();
    if(address[0]){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address':address[1] + " "+country
        },
        function(result, status){
            if (status == google.maps.GeocoderStatus.OK){
                map.setCenter(result[0].geometry.location);
                jQuery('#property_latitude').val(map.getCenter().lat());
                jQuery('#property_longitude').val(map.getCenter().lng());
                jQuery('#coordinate_latitude').text(map.getCenter().lat());
                jQuery('#coordinate_longitude').text(map.getCenter().lng());
                var marker = new google.maps.Marker({
                    map: map,
                    position: result[0].geometry.location
                });
                map.setZoom(15);
            }else{
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }
    return false;
}

function setup_address_info(address){
    var info = address.toString().split(",");
    var address_info = new String();
    $.each(info,function(i){
        if(parseInt(i) == 0){
            address_info = "<b style='font-size:15px;'>"+info[i]+"</b>";
        }else{

            address_info += "<span style='font-weight:lighter'>"+info[i]+"</span>";
        };
    })
    $("#highlight_address").html(address_info);
}

function validate_address(){
    var address = [true,null]
    var temp = [$("#property_unit_number").val(),$("#property_street_number").val(), $("#property_street").val(), $("#property_state").val(), $("#property_town_village").val(), $("#property_suburb").val(), $("#property_zipcode").val() ]
    var tempObject = {
        property_unit_number : $("#property_unit_number").val(),
        property_street_number : $("#property_street_number").val(),
        property_street: $("#property_street").val(),
        property_state : $("#property_state").val(),
        property_town_village : $("#property_town_village").val() ,
        property_suburb : $("#property_suburb").val(),
        property_zipcode : $("#property_zipcode").val()
    };
    $.each(tempObject, function(id,value){
        var flag = true;
        if(id == "property_town_village"){
            flag = false;
        }
        if(id == "property_unit_number"){
            flag = false;
        }
        if(flag){
            if(value == ""){
                address = [false,null];
                $("#"+id).attr("style", "background-color:red")
                $("#"+id).bind("keyup",function(){
                    $(this).attr("style","")
                });
            };
        }
    });
    if(address[0]){
        address = [true,temp.toString()];
    };
    return address;
};
 
function setup_property_address(result){
    if(result.Placemark[0].AddressDetails.Country.AdministrativeArea != undefined){
        if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea != undefined){
            if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality != undefined){
                if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['Thoroughfare'] != undefined){
                    var street = result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['Thoroughfare']['ThoroughfareName'].trim().split(" ");
                    var unit_number = street[0].trim().split("/")[0];
                    var street_number = street[0].trim().split("/")[1];
                    jQuery("#street_address").val(result.Placemark[0].address);
                    jQuery('#property_street').val(street[1]);
                    if(unit_number != undefined){
                        jQuery('#property_unit_number').val(unit_number);
                    };
                    if(street_number != undefined){
                        jQuery('#property_street_number').val(street_number);
                    };
                };
                jQuery('#property_town_village').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea['SubAdministrativeAreaName'])
                jQuery('#lb_town_village').text(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea['SubAdministrativeAreaName'])

                if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['LocalityName'] != undefined){
                    jQuery('#property_suburb_suggest_input').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['LocalityName'])
                    jQuery('#property_suburb_suggest_hidden').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['LocalityName'])
                    jQuery('#property_suburb').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['LocalityName'])
                };
                if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['PostalCode'] != undefined){
                    jQuery('#lb_zip_code').text(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['PostalCode']['PostalCodeNumber']);
                    jQuery('#property_zipcode').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality['PostalCode']['PostalCodeNumber']);
                };
            };
        }else{
            if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality != undefined){
                if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['Thoroughfare'] != undefined){
                    var street = result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['Thoroughfare']['ThoroughfareName'].trim().split(" ");
                    var unit_number = street[0].trim().split("/")[0];
                    var street_number = street[0].trim().split("/")[1];
                    jQuery("#street_address").val(result.Placemark[0].address);
                    jQuery('#property_street').val(street[1]);
                    if(unit_number != undefined){
                        jQuery('#property_unit_number').val(unit_number);
                    };
                    if(street_number != undefined){
                        jQuery('#property_street_number').val(street_number);
                    };
                };
                if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['LocalityName'] != undefined){
                    jQuery('#property_suburb_suggest_input').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['LocalityName'])
                    jQuery('#property_suburb_suggest_hidden').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['LocalityName'])
                    jQuery('#property_suburb').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['LocalityName'])
                };
                if(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['PostalCode'] != undefined){
                    jQuery('#lb_zip_code').text(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['PostalCode']['PostalCodeNumber']);
                    jQuery('#property_zipcode').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea.Locality['PostalCode']['PostalCodeNumber']);
                };
            };
        }
       
        if(result.Placemark[0].AddressDetails.Country.AdministrativeArea['AdministrativeAreaName'] != undefined){
            jQuery('#lb_state').text(result.Placemark[0].AddressDetails.Country.AdministrativeArea['AdministrativeAreaName']);
            jQuery('#property_state').val(result.Placemark[0].AddressDetails.Country.AdministrativeArea['AdministrativeAreaName']);
        };
    };
}


function init_map(map_center, map_zoom) {
    var mapOptions = {
        center: new google.maps.LatLng(-34.397, 150.644),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_div"),
        mapOptions);
    var latlng = new google.maps.LatLng(map_center[0],map_center[1],true)
    if(map_center[0] != null && map_center[1] != null){
        map.setCenter(latlng);
        var check_country = $("#property_country").val();
        if(check_country == "Australia" || check_country == "New Zealand"){
        }else{
            var geocoder = new google.maps.Geocoder();
                geocoder.geocode(latlng, function showAddress(result){
                var valid = Mapifies.SearchCode(result.Status.code);
                try {
                    setup_address_info(result.Placemark[0].address)
                } catch (exception) {
                    alert(valid.message);
                }
            })
        };
        jQuery('#property_latitude').val(latlng.lat());
        jQuery('#property_longitude').val(latlng.lng());
        jQuery('#coordinate_longitude').text(latlng.lng());
        jQuery('#coordinate_latitude').text(latlng.lat());
    }
    map.setZoom(map_zoom);
    var marker = new google.maps.Marker({
        map: map,
        position: latlng
    });
    jQuery("#overlay").hide();
}
