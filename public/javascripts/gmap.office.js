jQuery(function(){

  var aus_center = [-25, 133];
  var map_center;
  var map_zoom = 4;
  if(jQuery('#office_latitude').val() != "") {
    map_center = [jQuery('#office_latitude').val(), jQuery('#office_longitude').val()]
    map_zoom = 15;
  }else{
    map_center = aus_center
  }

  
 jQuery('#map_div').jmap('init', {'mapType':'map',
    'mapCenter': map_center,
    'mapZoom': map_zoom,
    'mapControl':'large'
  },
  function(map, element, options){
    window.gmap = map;//how to get the map object properly from jmap??    
    GEvent.addListener(map, "click", function(overlay, latlng, overlaylatlng){
      if(latlng != undefined ){
        jQuery('#office_latitude').val(latlng.lat());
        jQuery('#office_longitude').val(latlng.lng());
        jQuery('#coordinate_longitude').text(latlng.lng());
        jQuery('#coordinate_latitude').text(latlng.lat());
        map.clearOverlays();
        jQuery('#map_div').jmap('AddMarker',{
          'pointLatLng': [latlng.lat(), latlng.lng()],
          'pointHTML': 'Here I am!',
          'centerMap': true
          });
      };
    });
  });
  
  jQuery('#map_div').jmap('AddMarker',{
      'pointLatLng': map_center,
      'centerMap': true
  });
  
  
  jQuery('#submit_geocode').click(function(){
      address = jQuery('#office_unit_number').val() + ' ' + jQuery('#office_street_number').val() + ' ' + jQuery('#office_street').val() + ',' + jQuery('#office_suburb').val() + ',' + jQuery('#property_zipcode').val() + jQuery('#office_state').val() + ',' + jQuery('#office_country').val();
      jQuery("#map_div").jmap('SearchAddress',{
          'query': address,
          'returnType': 'getLocations'
        },function(result, options){
          var valid = Mapifies.SearchCode(result.Status.code);
          if(valid.success){
            window.gmap.clearOverlays();
            jQuery('#map_div').jmap('AddMarker',{
                'pointLatLng': [   result.Placemark[0].Point.coordinates[1], result.Placemark[0].Point.coordinates[0]],
                'pointHTML':  result.Placemark[0].address,
                'centerMap': true
            }, function(marker, options){
                jQuery('#coordinate_latitude').text(marker.getLatLng().lat());
                jQuery('#coordinate_longitude').text(marker.getLatLng().lng());
                jQuery('#office_latitude').val(marker.getLatLng().lat());
                jQuery('#office_longitude').val(marker.getLatLng().lng());
                jQuery('#map_div').jmap('MoveTo', {'mapType':'map','mapZoom': 15, 'mapCenter': [marker.getLatLng().lat(), marker.getLatLng().lng()] });
                });                
          }else{
            alert(valid.message);
          }
      });
        return false;
  });


});
