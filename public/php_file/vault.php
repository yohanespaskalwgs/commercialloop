<?php
  //http://commercialloopcrm.com.au/php_file/vault.php
  require_once('fpdf/fpdf.php');
  require_once('fpdi/fpdi.php');

  $source_file = base64_decode($_GET['file']);
  $hour = $_GET['status'];
  if($hour == ''){$hour = 2;}
  // initiate FPDI
  $pdf = new FPDI();
  $pdf->AddPage(); 
  $total_pages_src_pdf = $pdf->setSourceFile($source_file);

  for( $i=1; $i<=$total_pages_src_pdf;  $i++ ) {
    if( $i>1 ) {
        $pdf->AddPage();
        $pdf->setSourceFile($source_file);
    }
    $pdf->useTemplate($pdf->importPage($i), 0, 0);
  }

  $javascript = '
   var LastDay = '.(integer)date("d").';
   var LastMonth = '.(integer)date("m").';
   var LastYear = '.date("Y").';
   var serverDate = new Date();
   LastMonth = LastMonth - 1;
   serverDate.setFullYear(LastYear, LastMonth, LastDay);

   var hour = '.$hour.';
   var diff = hour * 60;
   var today = new Date();
   var myDate = new Date(serverDate.getTime() + diff*60000);
   if (myDate < today)
   {
     app.alert("This files has expired.",3);
     this.closeDoc(1);
   }';
  
  $pdf->IncludeJS($javascript);
  $pdf->Output('vault_file'.date("Ymdhis").'.pdf', 'D');
?>
