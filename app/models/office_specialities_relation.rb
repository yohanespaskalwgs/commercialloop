class OfficeSpecialitiesRelation < ActiveRecord::Base
  belongs_to :office
  belongs_to :office_speciality
end
