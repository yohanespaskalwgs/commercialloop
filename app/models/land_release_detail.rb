class LandReleaseDetail < ActiveRecord::Base
  belongs_to :land_release
  validates_presence_of :release_name
  include DetailHelper

  def get_property_id
    return self.land_release_id
  end
end
