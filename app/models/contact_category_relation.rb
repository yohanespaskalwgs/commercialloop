class ContactCategoryRelation < ActiveRecord::Base
  belongs_to :agent_contact
  belongs_to :category_contact
end
