include PropertiesHelper

class UserMailer < ActionMailer::Base
  def signup_notification(user)
    setup_email(user)
    @subject    += 'Welcome to Commercial Loop!'
    @body[:url]  = "http://#{user.developer.entry}.commercialloopcrm.com.au/login"
  end

  def debug_notification(data)
    @recipients  = "rio.dermawan@kiranatama.com"
    @from        = "Admin <noreply@commercialloopcrm.com.au>"
    @subject     = "Debug Notification #{data}"
    @sent_on     = Time.now
    @body[:debug_subdomain]     = data
  end

  def send_api_notification(data)
    @recipients  = "rio.dermawan@kiranatama.com"
    @from        = "Admin <noreply@commercialloopcrm.com.au>"
    @subject     = "Send Api Notification"
    @sent_on     = Time.now
    @body[:data]     = data
  end

  def send_log(data)
    @recipients  = "rio.dermawan@kiranatama.com"
    @content_type = "text/html"
    @from        = "Log <noreply@commercialloopcrm.com.au>"
    @subject     = "Agentpoint Log"
    @sent_on     = Time.now
    @body[:data]     = data
  end

  def activation(user)
    setup_email(user)
    @subject    += 'Your account has been activated!'
    @body[:url]  = "http://#{user.developer.entry}.commercialloopcrm.com.au/login"
  end

  def agent_user_creation(user, office)
    #setup_email(user, office.full_name)
    #@content_type = "text/html"
    @recipients  = "#{user.email}"
    #@bcc         = "hello@commercialloopcrm.com.au"
    @from        = "#{user.developer.name} <noreply@commercialloopcrm.com.au>"
    @subject     = "#{user.office.name} - Account Creation"
    @sent_on     = Time.now
    @body[:user] = user
    default_url_options[:host] = user.site_url.sub('http://', '')
    @body[:url] = user.site_url
    @body[:office] = office
  end

  def agent_user_notification(user, office, agent_user)
    @recipients  = "#{user.email}"
    @from        = "#{agent_user.developer.name} <noreply@commercialloopcrm.com.au>"
    @subject     = "Headline: New User Registration"
    @sent_on     = Time.now
    @body[:user] = user
    @body[:agent_user] = agent_user
    @body[:office] = office
  end

  def office_user_notification(user, office, agent_user)
    @recipients  = "#{user.email}"
    @from        = "#{agent_user.developer.name} <hello@commercialloopcrm.com.au>"
    @subject     = "New Office's User Registration"
    @sent_on     = Time.now
    @body[:user] = user
    @body[:agent_user] = agent_user
    @body[:office] = office
  end

  def office_user_creation(user, office)
    setup_email(user, office.full_name)
    @content_type = "text/html"
    default_url_options[:host] = user.site_url.sub('http://', '')
    @body[:url] = user.site_url
    @body[:office] = office
  end

  def reset_notification(user)
    setup_email(user)
    @subject    += 'Your Commercial Loop Password'
    @body[:url]  = "http://#{user.developer.entry}.commercialloopcrm.com.au/reset/#{user.reset_code}"
  end

  def signup_member_notification(user)
    setup_email(user)
    @subject    += 'Welcome to Commercial Loop!'
    @body[:url]  = "http://#{user.developer.entry}.commercialloopcrm.com.au/login"
  end

  def subscriptions(office)
    @recipients  = "budhi.a@kiranatama.com"
    @from        = "Test Email <bugeh_oce@yahoo.com>"
    @subject     = "Notification Office Trial Days"
    @sent_on     = Time.now
    @body[:office] = office
  end

  def listing_notification(user, office, property)
    @recipients  = "#{user.email}"
    @bcc         = "hello@commercialloopcrm.com.au"
    @from        = "Admin <noreply@commercialloopcrm.com.au>"
    @subject     = "Listing Added/Updated"
    @sent_on     = Time.now
    @body[:user] = user
    default_url_options[:host] = user.site_url.sub('http://', '')
    @body[:url] = user.site_url
    @body[:office] = office
    @body[:property] = property
  end

  def listing_notification_for_level1(user_level1, user, office, property)
    @recipients  = "#{user_level1.email}"
    @from        = "Admin <noreply@commercialloopcrm.com.au>"
    @subject     = "Listing Added/Updated"
    @sent_on     = Time.now
    @body[:user_level1] = user_level1
    @body[:user] = user
    default_url_options[:host] = user.site_url.sub('http://', '')
    @body[:url] = user.site_url
    @body[:office] = office
    @body[:property] = property
  end

  def media_notification_for_level1(user_level1, user, office, property, media)
    @recipients  = "#{user_level1.email}"
    @from        = "Admin <noreply@commercialloopcrm.com.au>"
    @subject     = "#{media} Media Added/Updated"
    @media       = media.humanize
    @sent_on     = Time.now
    @body[:user_level1] = user_level1
    @body[:user] = user
    default_url_options[:host] = user.site_url.sub('http://', '')
    @body[:url] = user.site_url
    @body[:office] = office
    @body[:property] = property
  end

  def portal_activated_for_level1(user_level1, portal, autosubcribed)
    @recipients  = "#{user_level1.email}"
    @from        = "Admin <noreply@commercialloopcrm.com.au>"
    @subject     = "#{portal} enabled"
    @media       = portal.humanize
    @sent_on     = Time.now
    @body[:user_level1] = user_level1
    @body[:portal] = portal
    @body[:autosubcribed] = autosubcribed
  end

  def lease_reminder(contact,tenancy)
    @recipients = "#{contact.email}"
    @cc         = "ryan@agentpoint.com"
    @subject    = "Lease Expiry Reminder"
    @sent_on    = Time.now
    @agent_contact = contact
    @tenancy    = tenancy
    @from       = "Commercial Loop <comlpcrm@gmail.com>"
    @content_type = "text/html"
  end

  def email_configuration(email,agent_contact,agent_contact_email,current_property,agent,office,property,main_image,thumb_image1,thumb_image2,header_image,footer_image,side_image,host)
    @main_image = main_image
    @thumb_image1 = thumb_image1
    @thumb_image2 = thumb_image2
    @header_image = header_image
    @footer_image = footer_image
    @side_image = side_image
    @host = host
    @agent = agent
    @office = office
    @property = property
    @current_property = current_property
    @email = email
    @agent_contact = agent_contact
    @send_to = agent_contact_email
    @recipients = agent_contact_email
    @from       = "#{email.sender_name} <#{email.sender_email}>"
    @subject    = "#{email.title}"
    @bcc        = "#{email.send_bcc}"
    @content_type = "text/html"
  end

  def email_configuration_search(email,agent_contact_email,agent_contact,agent,office,properties,header_image,footer_image,side_image,host)
    @header_image = header_image
    @footer_image = footer_image
    @side_image = side_image
    @host = host
    @properties = properties
    @email = email
    @agent = agent
    @office = office
    @agent_contact = agent_contact
    @send_to = agent_contact_email
    @recipients = agent_contact_email
    @from = "#{email.sender_name} <#{email.sender_email}>"
    @subject = "#{email.title}"
    @bcc = "#{email.send_bcc}"
    @content_type = "text/html"
  end

  protected
  def setup_email(user, sender = "Commercial Loop")
    @recipients  = "#{user.email}"
    #@bcc         = "hello@commercialloopcrm.com.au"
    @from        = "#{sender} <noreply@commercialloopcrm.com.au>"
    @subject     = "[Commercial Loop] "
    @sent_on     = Time.now
    @body[:user] = user
  end
end
