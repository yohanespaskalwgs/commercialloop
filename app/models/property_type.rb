# == Schema Info
# Schema version: 20090614051807
#
# Table name: property_types
#
#  id       :integer(4)      not null, primary key
#  category :string(255)
#  name     :string(255)

class PropertyType < ActiveRecord::Base
  validates_presence_of :category, :name
  validates_uniqueness_of :name, :scope => :category

  has_many :contact_property_type_relations
  has_many :company_property_type_relations
  has_many :country_property_type_relationships

  %w(Residential Commercial Business Holiday).each do |cat|
    named_scope cat.downcase, :conditions => {:category => cat}
  end
end
