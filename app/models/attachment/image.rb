# == Schema Info
# Schema version: 20090331151240
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime
class Image < Attachment
  attr_accessor :dimension, :source

  FLAG = [16,11]
  AVATAR = [150,150]
  BROCHURE = [780,80]
  #  LOGO1 = [160,30]
  #  LOGO2 = [150,75]
  #  LOGO3 = [250,125]
  LOGO1 = [200,150]
  LOGO2 = [200,200]
  PHOTO = [800, 600]

  # TODO: need to separate property photos, floorplans from image attachments
  has_attachment :storage => :file_system,
    :path_prefix => 'public/attachments/images',
    :content_type => :image,
    :resize_to => :check_dimension,
    :thumbnails => {:thumb => :check_thumb, :medium => :check_medium, :original => :check_original},
    :processor => :Rmagick

  # BUGBUG: I kept getting attachment_fu Size is not included in the list error, so remove it for now
  #validates_as_attachment
  attr_accessible :order, :description, :position
  attr_accessor :no_resize

  acts_as_list :scope => 'attachable_id = #{attachable_id} AND attachable_type = \'Property\' AND parent_id IS NULL'#, :column => "order"
  before_validation :attach_thumbnail
  include AttachmentHelper

  def add_watermark(fmark)
    begin
      path_ori = self.full_filename(:original).to_s
      path = self.full_filename.to_s
      path_med = self.full_filename(:medium).to_s
      path_thumb = self.full_filename(:thumb).to_s

      self.filename = self.filename.gsub(".","_watermarked.")
      ori_watermarked = self.full_filename(:original).to_s
      path_watermarked = self.full_filename.to_s

      image_file_ori = Magick::Image::read(path_ori).first
      image_file_ori = image_file_ori.watermark(Magick::Image.read(fmark).first, 0.25, 1.0, Magick::SouthEastGravity)
      image_file_ori.write(ori_watermarked)

      image_file = Magick::Image::read(path).first
      image_file = image_file.watermark(Magick::Image.read(fmark).first, 0.25, 1.0, Magick::SouthEastGravity)
      image_file.write(path_watermarked)

      image_file.resize_to_fit!(400)
      image_file.write(self.full_filename(:medium).to_s)

      image_file.resize_to_fit!(200)
      image_file.write(self.full_filename(:thumb).to_s)

      ori_wm_img = Image.find(:first, :conditions => "`parent_id`= #{self.id} AND `thumbnail` = 'original'")
      med_wm_img = Image.find(:first, :conditions => "`parent_id`= #{self.id} AND `thumbnail` = 'medium'")
      thumb_wm_img = Image.find(:first, :conditions => "`parent_id`= #{self.id} AND `thumbnail` = 'thumb'")

      ori_wm_img.filename = ori_wm_img.filename.gsub("_original","_watermarked_original")
      med_wm_img.filename = med_wm_img.filename.gsub("_medium","_watermarked_medium")
      thumb_wm_img.filename = thumb_wm_img.filename.gsub("_thumb","_watermarked_thumb")

      ActiveRecord::Base.connection.execute("UPDATE attachments SET filename='#{self.filename}' WHERE id = #{self.id}")
      ActiveRecord::Base.connection.execute("UPDATE attachments SET filename='#{ori_wm_img.filename}' WHERE `parent_id`= #{self.id} AND `thumbnail` = 'original'")
      ActiveRecord::Base.connection.execute("UPDATE attachments SET filename='#{med_wm_img.filename}' WHERE `parent_id`= #{self.id} AND `thumbnail` = 'medium'")
      ActiveRecord::Base.connection.execute("UPDATE attachments SET filename='#{thumb_wm_img.filename}' WHERE `parent_id`= #{self.id} AND `thumbnail` = 'thumb'")

      self.moved_to_s3 = false
      ori_wm_img.moved_to_s3 = false
      med_wm_img.moved_to_s3 = false
      thumb_wm_img.moved_to_s3 = false

      self.upload_to_s3
      ori_wm_img.upload_to_s3
      med_wm_img.upload_to_s3
      thumb_wm_img.upload_to_s3

      File.delete(path)
      File.delete(path_ori)
      File.delete(path_med)
      File.delete(path_thumb)

    rescue Exception => ex
      Log.create({:message => "failed create watermark #{ex.inspect}"})
      return false
    end
  end

  def validate_photos
    begin
      a = Magick::Image::read(self.temp_path.to_s).first
      errors.add_to_base("You must choose a file to upload") and return false unless self.filename
      errors.add_to_base("#{self.filename}") and return false unless /(jpeg|jpg)/.match(a.format.to_s.downcase)
      return true
    rescue Exception => ex
      errors.add_to_base("#{self.filename}") and return false
    end
  end

  def validate_logo
    begin
      a = Magick::Image::read(self.temp_path.to_s).first
      if a.colorspace == Magick::CMYKColorspace
        return true
      else
        return false
      end
    rescue Exception => ex
      return true
    end
  end

  def check_dimension
    w,h = width,height
    with_image do |img|
      w, h = img.columns, img.rows
    end

    GLOBAL_ATTR["image_dimension"] = [w,h]
    unless self.no_resize
      unless source == 'team_member'
        d = Array.new(2)
        d[0]  = 800
        d[1]  = (h/(w/800.to_f))
        return d
      else
        ori = Array.new(2)
        if w > 600
          ori[0]  = 600
          ori[1]  = (600*h)/w
        else
          ori[0]  = w
          ori[1]  = h
        end
        return ori
      end
    else
      return GLOBAL_ATTR["image_dimension"]
    end
  end

  def check_thumb
    thumb = Array.new(2)
    w,h = width,height
    with_image do |img|
      w, h = img.columns, img.rows
      img.strip!
    end

    thumb = Array.new(2)
    thumb[0]  = 200
    thumb[1]  = (h/(w/200.to_f))
    return thumb
  end

  def check_medium
    medium = Array.new(2)
    w,h = width,height
    with_image do |img|
      w, h = img.columns, img.rows
      img.strip!
    end
    medium[0]  = 400
    medium[1]  = (h/(w/400.to_f))
    return medium
  end

  def check_original
    original = Array.new(2)
    original[0]  = GLOBAL_ATTR["image_dimension"][0]
    original[1]  = GLOBAL_ATTR["image_dimension"][1]
    return original
  end

  def after_save
#    self.upload_to_s3
  end

  def upload_to_s3(temp_file)
    s3_filename = Digest::SHA1.hexdigest(full_filename.gsub(%r(^#{Regexp.escape(base_path)}), ''))
    img = self
    if !img.blank?
      if img.moved_to_s3 == false
        begin
          AWS::S3::DEFAULT_HOST.replace "s3-ap-southeast-2.amazonaws.com"
          AWS::S3::Base.establish_connection!(
             :access_key_id     => 'AKIAIXGPCECXGHB56OLQ',
             :secret_access_key => 'dQRmYVF4HSa8Nh8WKKvQw2gc7Z5WALS8uQ52/TUX'
          )
          AWS::S3::S3Object.store(
            s3_filename,
            open(temp_file),
            "img.commercialloopcrm.com.au",
            :access => :public_read,
            :content_type => img.content_type
          )
          sql = ActiveRecord::Base.connection()
          sql.insert "UPDATE attachments SET moved_to_s3 = 1 WHERE id = #{img.id}"
          sql.commit_db_transaction
#          ActiveRecord::Base.connection.execute("UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}")
          return true
        rescue Exception => ex #Errno::ENOENT
          Log.create({:message => "Image model UPLOAD ERROR. full_filename:#{full_filename}, base_path:#{base_path}. ERROR:#{ex.inspect}"})
          return false
#          return BASE_URL + full_filename.gsub(%r(^#{Regexp.escape(base_path)}), '')
        end
      end
    end
  end

  private

  def attach_thumbnail
    self.attachable = (self.parent.attachable if attribute_present?('thumbnail')) unless self.parent.blank?
  end

end
