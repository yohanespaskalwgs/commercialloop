# == Schema Info
# Schema version: 20090331151240
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime

class PortraitImage < Image
  attr_accessor :the_position, :break_after_save_thumb, :break_after_save_medium, :old_images
  def self.width; 100; end
  def self.height; 125; end
  def dimension; [100,125]; end

  def before_save
    @the_position = self.position
  end

  def before_create
    self.position = @the_position if @the_position
  end

  def after_save
    return if @old_images.blank?
    unless break_after_save_thumb
      self.thumbnails.first.update_attributes(:position => self.position, :break_after_save_thumb => true) if self.thumbnails.first
    end
    unless break_after_save_medium
      self.thumbnails.last.update_attributes(:position => self.position, :break_after_save_medium => true) if self.thumbnails.last
    end
  end

  def after_create
   last_portrait = PortraitImage.find(:all, :conditions => "attachable_id = #{self.attachable.id} AND attachable_type='User' AND position = #{self.position} AND id <> #{self.id}")
   unless last_portrait.blank?
     begin
      last_portrait.map{|pt| ActiveRecord::Base.connection.execute("DELETE FROM `attachments` WHERE `id` = #{pt.id} OR `parent_id` = #{pt.id}")}
     rescue
     end
   end
  end

  def upload_to_s3(temp_file)
    s3_filename = Digest::SHA1.hexdigest(full_filename.gsub(%r(^#{Regexp.escape(base_path)}), ''))
    img = self
    if !img.blank?
      if img.moved_to_s3 == false
        begin
          AWS::S3::DEFAULT_HOST.replace "s3-ap-southeast-2.amazonaws.com"
          AWS::S3::Base.establish_connection!(
             :access_key_id     => 'AKIAIXGPCECXGHB56OLQ',
             :secret_access_key => 'dQRmYVF4HSa8Nh8WKKvQw2gc7Z5WALS8uQ52/TUX'
          )
          AWS::S3::S3Object.store(
            s3_filename,
            open(temp_file),
            "img.commercialloopcrm.com.au",
            :access => :public_read,
            :content_type => img.content_type
          )
          ActiveRecord::Base.connection.execute("UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}")
          return true
        rescue Exception => ex #Errno::ENOENT
          Log.create({:message => "Portrait Image model UPLOAD ERROR. full_filename:#{full_filename}, base_path:#{base_path}. ERROR:#{ex.inspect}"})
          return false
#          sleep(15)
#          if try.blank? || try < 10
#            try = try.to_i + 1
#            self.upload_to_s3(try)
#          end
#          return BASE_URL + full_filename.gsub(%r(^#{Regexp.escape(base_path)}), '')
        end
      end
    end
  end

end
