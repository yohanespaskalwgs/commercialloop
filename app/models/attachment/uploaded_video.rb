class UploadedVideo < Attachment
    has_attachment  :storage => :file_system,
                  :path_prefix => 'public/attachments/uploaded_videos',
                  :content_type => [ 'video/mp4', 'video/mpeg' ]
    attr_accessible :order, :description, :position
#    validates_as_attachment
    include AttachmentHelper
end
