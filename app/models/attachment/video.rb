# == Schema Info
# Schema version: 20090331151240
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime

class Video < Attachment

  def before_create
    self.attachable_type = 'Property'
  end

  def public_filename
    self.filename
  end

  def after_save
    property = self.attachable
    property.updated_at = self.updated_at
    if property.save
#      self.upload_to_s3
    end
  end

  def upload_to_s3
    s3_filename = Digest::SHA1.hexdigest(full_filename.gsub(%r(^#{Regexp.escape(base_path)}), ''))
    img = self
    if !img.blank?
      if img.moved_to_s3 == false
        begin
          AWS::S3::DEFAULT_HOST.replace "s3-ap-southeast-2.amazonaws.com"
          AWS::S3::Base.establish_connection!(
             :access_key_id     => 'AKIAIXGPCECXGHB56OLQ',
             :secret_access_key => 'dQRmYVF4HSa8Nh8WKKvQw2gc7Z5WALS8uQ52/TUX'
          )
          AWS::S3::S3Object.store(
            s3_filename,
            open(full_filename),
            "img.commercialloopcrm.com.au",
            :access => :public_read,
            :content_type => img.content_type
          )
          ActiveRecord::Base.connection.execute("UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}")
        rescue Exception => ex #Errno::ENOENT
          return BASE_URL + full_filename.gsub(%r(^#{Regexp.escape(base_path)}), '')
        end
      end
    end
  end

  #def before_save
    #begin
    #f = File.open(filename, 'r')
    #self.content_type = f.content_type
    #self.size = f.size
    #f.close
    #rescue => e
#raise e.inspect
      #errors.add_to_base("Cant read youtube file you specified. Please ensure the video url is valid<br /> #Error is follow: <br />#{e.inspect}")
      #return false
    #end
  #end

end
