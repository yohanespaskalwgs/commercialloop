# == Schema Info
# Schema version: 20090331151240
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime

class Mp3 < Attachment

  has_attachment  :storage => :file_system,
                  :path_prefix => 'public/attachments/mp3s',
                  :content_type => [ 'application/mp3', 'application/x-mp3', 'audio/mpeg', 'audio/mp3', 'audio/mpg' ],
                  :max_size => 100.megabytes
  validates_as_attachment
  include AttachmentHelper

  def after_save
    property = self.attachable
    property.updated_at = self.updated_at
    property.save
  end
end
