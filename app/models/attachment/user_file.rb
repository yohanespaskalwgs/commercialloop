# == Schema Info
# Schema version: 20090331151240
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime

class UserFile < Attachment
  attr_accessible :uploaded_data, :category, :description, :attachable_id, :attachable_type, :private, :parent_id

  has_attachment :storage => :file_system,
                 :path_prefix => 'public/attachments/files',
                 :max_size => 100.megabyte
  validates_as_attachment

  validates_presence_of :category
  include AttachmentHelper

  def after_save
#    self.upload_to_s3
  end

  def upload_to_s3
    thumbnail = self.thumbnail
    s3_filename = Digest::SHA1.hexdigest(full_filename(thumbnail).gsub(%r(^#{Regexp.escape(base_path)}), ''))
    if thumbnail.blank?
      img = self
    else
      img = self.thumbnails.find(:first, :conditions => "thumbnail = '#{thumbnail.to_s}'")
    end
    if !img.blank?
      if img.moved_to_s3 == false
        begin
          AWS::S3::DEFAULT_HOST.replace "s3-ap-southeast-2.amazonaws.com"
          AWS::S3::Base.establish_connection!(
             :access_key_id     => 'AKIAIXGPCECXGHB56OLQ',
             :secret_access_key => 'dQRmYVF4HSa8Nh8WKKvQw2gc7Z5WALS8uQ52/TUX'
          )
          AWS::S3::S3Object.store(
            s3_filename,
            open(full_filename(thumbnail)),
            "img.commercialloopcrm.com.au",
            :access => :public_read,
            :content_type => img.content_type
          )
          ActiveRecord::Base.connection.execute("UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}")
        rescue Exception => ex #Errno::ENOENT
          return BASE_URL + full_filename(thumbnail).gsub(%r(^#{Regexp.escape(base_path)}), '')
        end
      end
    end
  end


end
