class Brochure < Attachment
  has_attachment :storage => :file_system,
    :path_prefix => 'public/attachments/brochures',
    :processor => :Rmagick,
    :max_size => 5.megabytes
  validates_as_attachment
  include AttachmentHelper
  attr_accessible :order, :description, :position
  acts_as_list :scope => 'attachable_id = #{attachable_id} AND attachable_type = \'Property\' AND parent_id IS NULL'#, :column => "order"

  def validate_brochures
    begin
      a = Magick::Image::read(self.temp_path.to_s).first
      errors.add_to_base("You must choose a file to upload") and return false unless self.filename
      errors.add_to_base("#{self.filename}, you can only upload brochure with PDF format") and return false unless /(pdf)/.match(a.format.to_s.downcase)
      return true
    rescue Exception => ex
      errors.add_to_base("#{self.filename}, you can only upload brochure with PDF format") and return false
    end
  end

  def self.theme_count
    return 9
  end

  def self.theme_limit(name)
    case name.to_s.downcase
    when "theme1"
      return 4
    when "theme2"
      return 4
    when "theme3"
      return 4
    when "theme4"
      return 4
    when "theme5"
      return 3
    when "theme6"
      return 3
    when "theme7"
      return 3
    when "theme8"
      return 4
    when "theme9"
      return 1
    else
      return 0
    end
  end

  def self.theme_info(name)
    case name.to_s.downcase
    when "theme1"
      return {:main => {:result_width => 780, :result_height=> 350, :dot_width => 780, :dot_height=> 350, :pre_width=> 195, :pre_height => 87},
              :thumb => {:result_width => 258, :result_height=> 130, :dot_width => 258, :dot_height=> 130, :pre_width=> 258, :pre_height => 130}}
    when "theme2"
      return {:main => {:result_width => 780, :result_height=> 430, :dot_width => 780, :dot_height=> 430, :pre_width=> 195, :pre_height => 108},
              :thumb => {:result_width => 258, :result_height=> 130, :dot_width => 232, :dot_height=> 130, :pre_width=> 258, :pre_height => 130}}
    when "theme3"
      return {:main => {:result_width => 780, :result_height=> 350, :dot_width => 780, :dot_height=> 350, :pre_width=> 195, :pre_height => 87},
              :thumb => {:result_width => 258, :result_height=> 130, :dot_width => 258, :dot_height=> 130, :pre_width=> 258, :pre_height => 130}}
    when "theme4"
      return {:main => {:result_width => 780, :result_height=> 430, :dot_width => 780, :dot_height=> 430, :pre_width=> 195, :pre_height => 108},
              :thumb => {:result_width => 258, :result_height=> 130, :dot_width => 232, :dot_height=> 130, :pre_width=> 258, :pre_height => 130}}
    when "theme5"
      return {:main => {:result_width => 780, :result_height=> 430, :dot_width => 780, :dot_height=> 430, :pre_width=> 195, :pre_height => 108},
              :thumb => {:result_width => 300, :result_height=> 295, :dot_width => 300, :dot_height=> 295, :pre_width=> 198, :pre_height => 167}}
    when "theme6"
      return {:main => {:result_width => 780, :result_height=> 350, :dot_width => 780, :dot_height=> 350, :pre_width=> 195, :pre_height => 87},
              :thumb => {:result_width => 388, :result_height=> 220, :dot_width => 388, :dot_height=> 220, :pre_width=> 258, :pre_height => 147}}
    when "theme7"
      return {:main => {:result_width => 780, :result_height=> 350, :dot_width => 780, :dot_height=> 350, :pre_width=> 195, :pre_height => 87},
              :thumb => {:result_width => 388, :result_height=> 220, :dot_width => 388, :dot_height=> 220, :pre_width=> 232, :pre_height => 147}}
    when "theme8"
      return {:main => {:result_width => 780, :result_height=> 490, :dot_width => 780, :dot_height=> 490, :pre_width=> 195, :pre_height => 123},
              :thumb => {:result_width => 258, :result_height=> 194, :dot_width => 258, :dot_height=> 194, :pre_width=> 258, :pre_height => 194}}
    when "theme9"
      return {:main => {:result_width => 780, :result_height=> 490, :dot_width => 780, :dot_height=> 490, :pre_width=> 195, :pre_height => 123},
              :thumb => {:result_width => 258, :result_height=> 194, :dot_width => 258, :dot_height=> 194, :pre_width=> 258, :pre_height => 194}}
    end
  end


  def self.theme_editor(name)
    case name.to_s.downcase
    when "theme1"
      return "text_editor1"
    when "theme2"
      return "text_editor1"
    when "theme3"
      return "text_editor2"
    when "theme4"
      return "text_editor2"
    when "theme5"
      return "text_editor3"
    when "theme6"
      return "text_editor2"
    when "theme7"
      return "text_editor1"
    when "theme8"
      return "text_editor4"
    else
      return "text_editor1"
    end
  end

  def self.editor_limit(name)
    case name.to_s.downcase
    when "theme1"
      return 420
    when "theme2"
      return 420
    when "theme3"
      return 430
    when "theme4"
      return 430
    when "theme5"
      return 565
    when "theme6"
      return 420
    when "theme7"
      return 425
    when "theme8"
      return 225
    when "theme9"
      return 325
    else
      return 420
    end
  end

  def upload_to_s3(temp_file)
    s3_filename = Digest::SHA1.hexdigest(full_filename.gsub(%r(^#{Regexp.escape(base_path)}), ''))
    img = self
    if !img.blank?
      if img.moved_to_s3 == false
        begin
          AWS::S3::DEFAULT_HOST.replace "s3-ap-southeast-2.amazonaws.com"
          AWS::S3::Base.establish_connection!(
             :access_key_id     => 'AKIAIXGPCECXGHB56OLQ',
             :secret_access_key => 'dQRmYVF4HSa8Nh8WKKvQw2gc7Z5WALS8uQ52/TUX'
          )
          AWS::S3::S3Object.store(
            s3_filename,
            open(temp_file),
            "img.commercialloopcrm.com.au",
            :access => :public_read,
            :content_type => img.content_type
          )
          sql = ActiveRecord::Base.connection()
          sql.insert "UPDATE attachments SET moved_to_s3 = 1 WHERE id = #{img.id}"
          sql.commit_db_transaction
      ActiveRecord::Base.connection.execute("UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}")
          return true
        rescue Exception => ex #Errno::ENOENT
          Log.create({:message => "Image model UPLOAD ERROR. full_filename:#{full_filename}, base_path:#{base_path}. ERROR:#{ex.inspect}"})
          return false
      return BASE_URL + full_filename.gsub(%r(^#{Regexp.escape(base_path)}), '')
        end
      end
    end
  end

end
