class VaultFile < Attachment
  has_attachment :storage => :file_system,
    :path_prefix => 'public/attachments/vault_files',
    :processor => :Rmagick,
    :max_size => 50.megabytes
  validates_as_attachment
  include AttachmentHelper
  attr_accessible :order, :description, :position
  acts_as_list :scope => 'attachable_id = #{attachable_id} AND attachable_type = \'Property\' AND parent_id IS NULL'#, :column => "order"

  def validate_brochures
    begin
      a = Magick::Image::read(self.temp_path.to_s).first
      errors.add_to_base("You must choose a file to upload") and return false unless self.filename
      errors.add_to_base("#{self.filename}, you can only upload brochure with PDF format") and return false unless /(pdf)/.match(a.format.to_s.downcase)
      return true
    rescue Exception => ex
      errors.add_to_base("#{self.filename}, you can only upload brochure with PDF format") and return false
    end
  end

  def after_save
#    self.upload_to_s3
  end

  def upload_to_s3
    s3_filename = Digest::SHA1.hexdigest(full_filename.gsub(%r(^#{Regexp.escape(base_path)}), ''))
    img = self
    if !img.blank?
      if img.moved_to_s3 == false
        begin
          AWS::S3::DEFAULT_HOST.replace "s3-ap-southeast-2.amazonaws.com"
          AWS::S3::Base.establish_connection!(
             :access_key_id     => 'AKIAIXGPCECXGHB56OLQ',
             :secret_access_key => 'dQRmYVF4HSa8Nh8WKKvQw2gc7Z5WALS8uQ52/TUX'
          )
          AWS::S3::S3Object.store(
            s3_filename,
            open(full_filename),
            "img.commercialloopcrm.com.au",
            :access => :public_read,
            :content_type => img.content_type
          )
          ActiveRecord::Base.connection.execute("UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}")
        rescue Exception => ex #Errno::ENOENT
          return BASE_URL + full_filename.gsub(%r(^#{Regexp.escape(base_path)}), '')
        end
      end
    end
  end


end
