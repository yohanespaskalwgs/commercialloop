# == Schema Info
# Schema version: 20090614051807
#
# Table name: users
#
#  id                        :integer(4)      not null, primary key
#  developer_id              :integer(4)
#  office_id                 :integer(4)
#  activation_code           :string(40)
#  code                      :string(255)
#  crypted_password          :string(40)
#  description               :text
#  email                     :string(100)
#  fax                       :string(255)
#  first_name                :string(50)      default("")
#  im_service                :string(255)
#  im_username               :string(255)
#  last_name                 :string(50)      default("")
#  login                     :string(40)
#  mobile                    :string(255)
#  phone                     :string(255)
#  position                  :integer(4)
#  remember_token            :string(40)
#  salt                      :string(40)
#  type                      :string(255)
#  activated_at              :datetime
#  created_at                :datetime
#  deleted_at                :datetime
#  remember_token_expires_at :datetime
#  updated_at                :datetime

# == Schema Info
# Schema version: 20090424025319
#
# Table name: users
#
#  id                        :integer(4)      not null, primary key
#  developer_id              :integer(4)
#  office_id                 :integer(4)
#  activation_code           :string(40)
#  code                      :string(255)
#  crypted_password          :string(40)
#  description               :text
#  email                     :string(100)
#  fax                       :string(255)
#  first_name                :string(50)      default("")
#  im_service                :string(255)
#  im_username               :string(255)
#  last_name                 :string(50)      default("")
#  login                     :string(40)
#  mobile                    :string(255)
#  phone                     :string(255)
#  remember_token            :string(40)
#  salt                      :string(40)
#  type                      :string(255)
#  activated_at              :datetime
#  created_at                :datetime
#  deleted_at                :datetime
#  remember_token_expires_at :datetime
#  updated_at                :datetime
require 'digest/sha1'

class User < ActiveRecord::Base
  include Authentication
  include Authentication::ByPassword
  include Authentication::ByCookieToken

  # HACK HACK HACK -- how to do attr_accessible from here?
  # prevents a user from submitting a crafted form that bypasses activation
  # anything else you want your user to change should be added here.
  attr_accessor :conjunctional_office, :conjunctional_rea_agent_id, :conjunctional_user
  attr_accessible  :first_name, :last_name, :email, :login, :mobile, :phone, :fax, :description, :creator, :group,
    :password, :password_confirmation, :office_id, :developer_id, :code, :reset_code, :role, :active_help_status, :suburb, :conjunctional_office, :conjunctional_rea_agent_id, :conjunctional_id , :conjunctional_user,
    :share_stock_on_admin_account, :share_stock_with_all_offices, :share_database_requirement_on_admin_account, :share_database_requirement_with_all_offices

  HUMANIZED_ATTRIBUTES = {
    :login => "Username"
  }

  def self.human_attribute_name(attr)
    HUMANIZED_ATTRIBUTES[attr.to_sym] || super
  end

  #validates_format_of       :first_name,  :with => Authentication.name_regex,  :message => Authentication.bad_name_message, :allow_nil => true
  #validates_length_of       :first_name,  :maximum => 100
  #validates_format_of       :last_name,   :with => Authentication.name_regex,  :message => Authentication.bad_name_message, :allow_nil => true
  #validates_length_of      :last_name,   :maximum => 100
  validates_presence_of     :email, :first_name, :last_name
  validates_length_of       :email,    :within => 6..100 #r@a.wk
  #validates_uniqueness_of   :email
  validates_format_of       :email,    :with => Authentication.email_regex, :message => Authentication.bad_email_message
  validates_presence_of     :login, :message =>"can't be blank"
  validates_uniqueness_of  :login,       :scope => :developer_id
  validates_length_of       :login,       :within => 3..100, :message =>"is too short (minimum is 3 characters)"
  #validates_presence_of :phone
  #validates_presence_of :phone , :if => proc{|x| list ||= x.roles.collect(&:name);list.include?("LEVEL 4") && !x.full_access?}
  #validates_format_of      :login,       :with => Authentication.login_regex, :message => Authentication.bad_login_message

  #validates_presence_of :agreement,
  #  :message => "doesn't checked, please check terms and agreements"

  before_create :make_activation_code

  has_one :avatar, :class_name => 'Image', :as => :attachable
  has_many :files, :class_name => 'UserFile', :as => :attachable
  has_and_belongs_to_many :roles
  has_and_belongs_to_many :active_help_pages

  has_many :message_recipients
  has_many :messages, :through => :message_recipients
  has_many :email_configurations, :dependent => :destroy
  has_many :email_configuration_searches, :dependent => :destroy
  named_scope :admin, :joins => "LEFT JOIN `roles_users` ON `users`.id = `roles_users`.user_id LEFT JOIN `roles` ON `roles_users`.role_id = `roles`.id", :conditions => ["roles.name = 'admin'"]
  named_scope :admin_full_right, :joins => "LEFT JOIN `roles_users` ON `users`.id = `roles_users`.user_id LEFT JOIN `roles` ON `roles_users`.role_id = `roles`.id", :conditions => ["roles.name = 'admin'"]
  acts_as_paranoid

  DISK_QUOTA = 10 #gigabytes
  def disk_usage
    files.sum('size')
  end

  def toggle_activate_help!
    self.active_help_status = !self.active_help_status
    self.save
    ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=#{self.active_help_status} WHERE user_id = #{self.id}")
  end

  def check_user_page_status(page_id)
    ActiveHelpPage.find_by_sql("SELECT status FROM active_help_pages_users WHERE active_help_page_id = #{page_id} AND user_id = #{self.id}")
  end

  def close_per_page(page_id)
    ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=0 WHERE user_id = #{self.id} AND active_help_page_id = #{page_id}")
  end

  def full_name
    fullname = "#{first_name} #{last_name}"
    fullname.blank? ? login : fullname
  end

  # has_role? simply needs to return true or false whether a user has a role or not.
  # It may be a good idea to have "admin" roles return true always
  def has_role?(role_in_question)
    return true if AdminUser === self # AdminUser is like god
    return true if self.class.name == role_in_question
    @_list ||= self.roles.collect(&:name)
    @_list.include?(role_in_question.to_s)
  end

  def toggle_admin
    r = Role.find_by_name 'admin'
    roles.collect(&:name).include?('admin') ? roles.delete(r) : (roles << r)
    @_list = nil
  end

  # Activates the user in the database.
  def activate!
    @activated = true
    self.activated_at = Time.now.utc
    self.activation_code = nil
    save(false)
  end

  # Returns true if the user has just been activated.
  def recently_activated?
    @activated
  end

  def active?
    # the existence of an activation code means they have not activated yet
    activation_code.nil?
  end

  def create_reset_code
    @reset = true
    self.attributes = {:reset_code => Digest::SHA1.hexdigest( Time.now.to_s.split(//).sort_by {rand}.join )}
    save(false)
  end

  def recently_reset?
    @reset
  end

  def delete_reset_code
    self.attributes = {:reset_code => nil}
    save(false)
  end

  # Authenticates a user by their login name and unencrypted password.  Returns the user or nil.
  #
  # uff.  this is really an authorization, not authentication routine.
  # We really need a Dispatch Chain here or something.
  # This will also let us return a human error message.
  #
  def self.authenticate(login, password)
    return nil if login.blank? || password.blank?
    u = find :first, :conditions => ['login = ? and activated_at IS NOT NULL', login] # need to get the salt
    u && u.authenticated?(password) ? u : nil
  end

  def login=(value)
    write_attribute :login, (value ? value.downcase : nil)
  end

  def email=(value)
    write_attribute :email, (value ? value.downcase : nil)
  end

  def site_url
    DOMAIN_NAME
  end

  def include_accessible?(contact_id)
    ac = AgentContact.find(contact_id)
    old_ids = ac.contact_accessible_relations.map(&:accessible_by)
    old_ids.include?(self.id)
  end

  def include_accessible_company?(company_id)
    ac = AgentCompany.find(company_id)
    old_ids = ac.company_accessible_relations.map(&:accessible_by)
    old_ids.include?(self.id)
  end

  protected

  def make_activation_code
    self.activation_code = self.class.make_token
  end

end
