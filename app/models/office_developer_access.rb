# == Schema Info
# Schema version: 20090614051807
#
# Table name: office_developer_accesses
#
#  id                :integer(4)      not null, primary key
#  developer_user_id :integer(4)
#  office_id         :integer(4)

class OfficeDeveloperAccess < ActiveRecord::Base
  belongs_to :office
  belongs_to :developer_user
end
