# == Schema Info
# Schema version: 20090614051807
#
# Table name: features
#
#  id            :integer(4)      not null, primary key
#  office_id     :integer(4)
#  name          :string(255)
#  property_type :string(255)
#  type          :string(255)

class Feature < ActiveRecord::Base
  has_and_belongs_to_many :properties

  validates_presence_of :name, :property_type
  #Ticket 212 - allow the same name for the feature
  #validates_uniqueness_of :name, :scope => [:type, :property_type], :case_sensitive => false

  named_scope :external, :conditions => {:type => 'ExternalFeature'}, :order =>"name ASC"
  named_scope :general, :conditions => {:type => 'GeneralFeature'}, :order =>"name ASC"
  named_scope :internal, :conditions => {:type => 'InternalFeature'}, :order =>"name ASC"
  named_scope :lifestyle, :conditions => {:type => 'LifestyleFeature'}, :order =>"name ASC"
  named_scope :location, :conditions => {:type => 'LocationFeature'}, :order =>"name ASC"
  named_scope :security, :conditions => {:type => 'SecurityFeature'}, :order =>"name ASC"
  named_scope :user_defined, :conditions => {:type => 'UserDefinedFeature'}, :order =>"name ASC"

  named_scope :residential_lease, :conditions => {:property_type => 'ResidentialLease'}
  named_scope :residential_sale, :conditions => {:property_type => 'ResidentialSale'}
  named_scope :commercial, :conditions => {:property_type => 'Commercial'}
  named_scope :commercial_building, :conditions => {:property_type => 'CommercialBuilding'}
  named_scope :holiday_lease, :conditions => {:property_type => 'HolidayLease'}
  named_scope :project_sale, :conditions => {:property_type => 'ProjectSale'}
  named_scope :business_sale, :conditions => {:property_type => 'BusinessSale'}
  named_scope :new_development, :conditions => {:property_type => 'NewDevelopment'}
  named_scope :land_release, :conditions => {:property_type => 'LandRelease'}
  named_scope :general_sale, :conditions => {:property_type => 'GeneralSale'}
  named_scope :livestock_sale, :conditions => {:property_type => 'LivestockSale'}
  named_scope :clearing_sales_event, :conditions => {:property_type => 'ClearingSalesEvent'}
  named_scope :clearing_sale, :conditions => {:property_type => 'ClearingSale'}

  ALL_PRE_DEFINED = %w(InternalFeature ExternalFeature SecurityFeature GeneralFeature LocationFeature LifestyleFeature)
end
