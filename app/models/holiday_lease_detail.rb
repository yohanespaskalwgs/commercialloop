# == Schema Info
# Schema version: 20090614051807
#
# Table name: holiday_lease_details
#
#  id                       :integer(4)      not null, primary key
#  holiday_lease_id         :integer(4)
#  additional_notes         :text
#  bathrooms                :integer(4)      default(0)
#  bedrooms                 :integer(4)      default(0)
#  bond                     :decimal(16, 3)
#  carport_spaces           :integer(4)      default(0)
#  cleaning_fee             :decimal(16, 3)
#  energy_efficiency_rating :string(255)
#  energy_star_rating       :string(255)
#  floor_area               :decimal(16, 3)
#  garage_spaces            :integer(4)      default(0)
#  high_season_price        :decimal(16, 3)
#  land_area                :decimal(16, 3)
#  latitude                 :decimal(12, 7)
#  longitude                :decimal(12, 7)
#  max_persons              :integer(4)
#  mid_season_price         :decimal(16, 3)
#  number_of_floors         :integer(4)
#  off_street_spaces        :integer(4)      default(0)
#  peak_season_price        :decimal(16, 3)
#  virtual_tour             :string(255)
#  year_built               :integer(4)

class HolidayLeaseDetail < ActiveRecord::Base
  #attr_accessor :mid_season_period, :high_season_period, :peak_season_period

  belongs_to :holiday_lease
  belongs_to :property, :foreign_key => "holiday_lease_id"
  attr_accessor :property_type
  attr_accessor :country

  validates_presence_of :bedrooms, :unless =>:land_and_studio?
  validates_presence_of :bathrooms , :unless =>:land?
  validates_numericality_of :number_of_floors, :greater_than => 0, :only_integer => true, :allow_nil => true
  validates_numericality_of :bond, :cleaning_fee, :mid_season_price, :high_season_price, :peak_season_price, :greater_than => 0, :allow_nil => true
  validates_numericality_of :year_built, :only_integer => true, :greater_than => 999, :less_than_or_equal_to => Time.now.year, :allow_nil => true

  #before_save :formalize

  include DetailHelper

  #  def mid_season_period
  #    @mid_season_period || price_per_period
  #  end
  #
  #  def high_season_period
  #    @high_season_period || price_per_period
  #  end
  #
  #  def peak_season_period
  #    @peak_season_period || price_per_period
  #  end

  def get_property_id
    return self.holiday_lease_id
  end

  def land?
    if holiday_lease_id
      p = Property.find_by_id(holiday_lease_id)
      unless p.blank?
        (p.property_type == "Land" || p.property_type == "Rural" && p.country == "Australia") || (p.property_type == "Section" && p.country == "New Zealand")
      end
    else
      (property_type == "Land" || property_type == "Rural" && country == "Australia") || (property_type == "Section" && country == "New Zealand")
    end
  end

  def land_and_studio?
    if holiday_lease_id
      p = Property.find_by_id(holiday_lease_id)
      unless p.blank?
        p.property_type = property_type if property_type == "Studio"
        ((p.property_type == "Studio" || p.property_type == "Land" || p.property_type == "Rural") && p.country == "Australia") || (p.property_type == "Section" && p.country == "New Zealand")
      end
    else
      ((property_type == "Studio" || property_type == "Land" || property_type == "Rural") && country == "Australia") || (property_type == "Section" && country == "New Zealand")
    end
  end

  private
=begin
  def formalize
    unless mid_season_price.blank?
      self.mid_season_price = formalize_number mid_season_price, mid_season_period
      self.mid_season_period = "Weekly"
    end
    unless high_season_price.blank?
      self.high_season_price = formalize_number high_season_price, high_season_period
      self.high_season_period = "Weekly"
    end
    unless peak_season_price.blank?
      self.peak_season_price = formalize_number peak_season_price, peak_season_period
      self.peak_season_period = "Weekly"
    end
  end

  def formalize_number(number, period)
    case period
    when 'Nightly' then number*7.0
    when 'Weekly' then number
    when 'Monthly' then (number*12.0)/52.0
    else number
    end
  end
=end
end
