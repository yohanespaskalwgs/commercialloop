# == Schema Info
# Schema version: 20090614051807
#
# Table name: users
#
#  id                        :integer(4)      not null, primary key
#  developer_id              :integer(4)
#  office_id                 :integer(4)
#  activation_code           :string(40)
#  code                      :string(255)
#  crypted_password          :string(40)
#  description               :text
#  email                     :string(100)
#  fax                       :string(255)
#  first_name                :string(50)      default("")
#  im_service                :string(255)
#  im_username               :string(255)
#  last_name                 :string(50)      default("")
#  login                     :string(40)
#  mobile                    :string(255)
#  phone                     :string(255)
#  position                  :integer(4)
#  remember_token            :string(40)
#  salt                      :string(40)
#  type                      :string(255)
#  activated_at              :datetime
#  created_at                :datetime
#  deleted_at                :datetime
#  remember_token_expires_at :datetime
#  updated_at                :datetime

class AdminUser < User
  attr_accessible :im_username, :im_service, :access_level

  def all_messages
    AdminMessages.all
  end

  def unread_messages_count
    AdminMessage.admin_unread.count
  end

  include RoleLevelSystem

  def full_access?
    true
  end

  def time_zone
    "Sydney"
  end

  protected

  def has_level?(level)
    roles = self.roles.collect(&:name)
    roles.include?(level)
  end
end
