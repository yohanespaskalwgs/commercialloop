class NewDevelopmentDetail < ActiveRecord::Base
  belongs_to :new_development
  validates_presence_of :development_name
  include DetailHelper

  def get_property_id
    return self.new_development_id
  end
end
