class PropertyNote < ActiveRecord::Base
  attr_accessor :agent_id, :office_id, :duplicate
  belongs_to :property
  belongs_to :agent_contact
  belongs_to :agent_user
  belongs_to :note_type, :class_name => 'NoteType', :foreign_key => "note_type_id"
  belongs_to :heard_from
  validates_presence_of :note_type_id
  validates_presence_of :agent_contact_id, :message => " error, Contact must include a First Name and Last Name. Please try again"
  attr_accessor :skip_enquiry
  after_create :add_viewer, :create_property_enquiry_note

  def self.build_sorting(sort,dir)
    case sort
    when 'date'
      return 'property_notes.date '+dir
    when 'note_type'
      return 'note_types.name '+dir
    when 'heard'
      return 'heard_froms.name '+dir
    when 'first_name'
      return 'agent_contacts.first_name '+dir
    when 'last_name'
      return 'agent_contacts.last_name '+dir
    when 'note'
      return 'property_notes.note '+dir
    when 'updated_at'
      return 'property_notes.updated_at '+dir
    end
  end


  def self.property_viewers(property_id, condition)
    property_viewers = []
    property_viewer_types = PropertyViewerType.find(:all, :conditions => condition)
    property_viewer_types.map{|viewer_type|
      viewers = PropertyViewer.find(:all, :conditions =>
        ["`property_id`=? and `property_viewer_type_id`= ?", property_id, viewer_type.id])

      case viewer_type.name
      when "Open Inspection Attendees"
        note_type_id = 5
      when "Contract Provided"
        note_type_id = 16
      when "Offers Submitted"
        note_type_id = 7
      when "Auction Attendees"
        note_type_id = 15
      when "Property Enquiries"
        note_type_id = 14
      else
        id = 0
      end

      property_viewers << {
        :note_id => note_type_id,
        :type => viewer_type.name,
        :start_week => self.start_week,
        :end_week => self.end_week,
        :start_month => self.start_month,
        :end_month => self.end_month,
        :week => self.count_viewer_by_date(property_id, viewer_type.id, self.start_week, self.end_week),
        :month => self.count_viewer_by_date(property_id, viewer_type.id, self.start_month, self.end_month),
        :total => viewers.size
      }
    }
    return property_viewers
  end

  def self.count_viewer_by_date(property_id, type_id, start_date, end_date)
    viewers = PropertyViewer.find(:all, :conditions =>
        ["`property_id`=? and `property_viewer_type_id`= ? and (`date` >= ? and `date` <= ?)", property_id, type_id , start_date, end_date])
    return viewers.size
  end

  def self.property_enquires(property_id)
    property_enquires = []
    property_enquire_types = HeardFrom.find(:all, :conditions => ["`status` = 'heard_property'"])
    property_enquire_types.map{|enquire|
      enquires = PropertyNote.find(:all, :conditions => ["`note_type_id` = 14 and `property_id`=? and `heard_from_id`= ?", property_id, enquire.id])
      property_enquires << {
        :id => enquire.id,
        :type => enquire.name,
        :start_week => self.start_week,
        :end_week => self.end_week,
        :start_month => self.start_month,
        :end_month => self.end_month,
        :week => self.count_enquires_by_date(property_id, enquire.id, self.start_week, self.end_week),
        :month => self.count_enquires_by_date(property_id, enquire.id, self.start_month, self.end_month),
        :total => enquires.size
      }
    }
    return property_enquires
  end

  def self.count_enquires_by_date(property_id, type_id, start_date, end_date)
   PropertyNote.find(:all, :conditions => " `note_type_id` = 14 and `property_id`=#{property_id} and (`note_date` >= '#{start_date}' and `note_date` <= '#{end_date}' ) and `heard_from_id` #{type_id == "unindicated" ? 'IS NULL' : '= ' + type_id.to_s}").length
  end

  def add_viewer
     #Attend Auction
     PropertyViewer.create({:property_id => self.property_id, :property_viewer_type_id => 10 ,:date => Time.now}) if self.note_type_id == 15
     PropertyViewer.create({:property_id => self.property_id, :property_viewer_type_id => 8 ,:date => Time.now}) if self.contract == 1
     #Property Enquiry
     PropertyViewer.create({:property_id => self.property_id, :property_viewer_type_id => 11 ,:date => Time.now}) if self.note_type_id == 14
  end

  def self.start_week
    now = Time.now
    position = start_day = end_day = 0

    days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday","Sunday" ]
    days.each_with_index{|c,x| position = x + 1 if c.to_s == now.strftime("%A") }
    start_day = position - 1
    start_date = start_day.days.ago
    return start_date.strftime("%Y-%m-%d")
  end

  def self.end_week
    now = Time.now
    position = start_day = end_day = 0

    days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday","Sunday" ]
    days.each_with_index{|c,x| position = x + 1 if c.to_s == now.strftime("%A") }
    end_day = 7 - position
    end_date = end_day.days.from_now
    return end_date.strftime("%Y-%m-%d")
  end

  def self.start_month
    now = Time.now
    start_date = Date.new( now.year, now.month, 1 )
    return start_date.strftime("%Y-%m-%d")
  end

  def self.end_month
    now = Time.now
    if now.month == 12
      end_date = Date.new( now.year + 1, 1, 1 )-1
    else
      end_date = Date.new( now.year, now.month + 1, 1 )-1
    end
    return end_date.strftime("%Y-%m-%d")
  end

  def create_property_enquiry_note
    return unless self.skip_enquiry.blank?
    property_contact_enquiry = PropertyContactEnquiry.find(:first, :conditions => ["`property_id` = ? and `agent_contact_id` = ? ", self.property_id, self.agent_contact_id])
    unless property_contact_enquiry
     PropertyContactEnquiry.create(:property_id => self.property_id, :agent_contact_id => self.agent_contact_id)
     enquiry_note = {:property_id => self.property_id, :agent_contact_id => self.agent_contact_id, :agent_user_id => self.agent_user_id,
            :note_type_id => 14,:address => self.address, :description => "Property Enquiry", :note_date => Time.now, :heard_from_id => self.heard_from_id, :duplicate => 0}
     property_enquiry_note = PropertyNote.new(enquiry_note)
     property_enquiry_note.save
     self.agent_contact.create_note(self.property_id, "Property Enquiry", 6, self.agent_user_id)
    end
  end

  def self.unindicated_source(property_id)
    enquires = PropertyNote.find(:all, :conditions => ["`note_type_id` = 14 and `property_id`=? and `heard_from_id` IS NULL", property_id])
    return {
      :id => "NULL",
      :type => "Not Indicated",
      :start_week => self.start_week,
      :end_week => self.end_week,
      :start_month => self.start_month,
      :end_month => self.end_month,
      :week => self.count_enquires_by_date(property_id, "unindicated", self.start_week, self.end_week),
      :month => self.count_enquires_by_date(property_id, "unindicated", self.start_month, self.end_month),
      :total => enquires.size
    }
  end
end
