class TownCountry < ActiveRecord::Base
  has_many :suburbs, :dependent => :destroy
  belongs_to :country
  belongs_to :state

  validates_presence_of :country_id, :state_id, :name

  def self.find_all_by_state_name(state)
    self.find(:all, :joins => [:state], :conditions => ["states.name = ?", state], :order => "town_countries.name ASC")
  end
end
