class ContactNote < ActiveRecord::Base
  belongs_to :agent_contact
  belongs_to :agent_user
  belongs_to :note_type, :class_name => 'NoteType', :foreign_key => "note_type_id"
  validates_presence_of :note_type_id

  def create_offer
    Offer.create(:property_id => self.property_id, :agent_contact_id => self.agent_contact_id, :date => self.note_date, :detail => self.description, :contact_note_id => self.id, :duplicate => 0) if self.note_type_id == 7 && self.property_id != nil
  end
end
