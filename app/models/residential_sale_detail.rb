# == Schema Info
# Schema version: 20090614051807
#
# Table name: residential_sale_details
#
#  id                       :integer(4)      not null, primary key
#  residential_sale_id      :integer(4)
#  auction_date             :date
#  auction_place            :string(255)
#  auction_price            :decimal(16, 3)
#  auction_time             :time
#  bathrooms                :integer(4)      default(0)
#  bedrooms                 :integer(4)      default(0)
#  carport_spaces           :integer(4)      default(0)
#  condo_strata_fee         :decimal(16, 3)
#  energy_efficiency_rating :string(255)
#  energy_star_rating       :string(255)
#  estimate_rental_return   :decimal(16, 3)
#  floor_area               :decimal(16, 3)
#  forthcoming_auction      :boolean(1)
#  garage_spaces            :integer(4)      default(0)
#  land_area                :decimal(16, 3)
#  latitude                 :decimal(12, 7)
#  longitude                :decimal(12, 7)
#  number_of_floors         :integer(4)
#  off_street_spaces        :integer(4)      default(0)
#  tax_rate                 :decimal(16, 3)
#  virtual_tour             :string(255)
#  year_built               :integer(4)

class ResidentialSaleDetail < ActiveRecord::Base
  #attr_accessor :tax_rate_period, :condo_strata_fee_period, :estimate_rental_return_period
  attr_accessor :property_type, :skip_validation
  attr_accessor :country

  belongs_to :residential_sale
  belongs_to :property, :foreign_key => "residential_sale_id"

  validates_presence_of :bedrooms, :unless => :land_and_studio?
  validates_presence_of :bathrooms , :unless =>:land?
  validates_presence_of :tax_rate_period, :unless => lambda { |r| r.tax_rate.blank? } #, :on => :create
  validates_presence_of :condo_strata_fee_period, :unless => lambda {|r| r.condo_strata_fee.blank? } #, :on => :create
  validates_presence_of :water_rate_period, :unless => lambda { |r| r.water_rate.blank? }
  validates_presence_of :estimate_rental_return_period, :unless => lambda {|r| r.estimate_rental_return.blank? } #, :on => :create
  #  validates_numericality_of :number_of_floors, :greater_than => 0, :only_integer => true
  validates_numericality_of :year_built, :only_integer => true, :greater_than => 999, :less_than_or_equal_to => Time.now.year, :allow_nil => true

  #before_save :formalize

  include DetailHelper
=begin
  def tax_rate_period
    @tax_rate_period || period
  end

  def condo_strata_fee_period
    @condo_strata_fee_period || period
  end

  def estimate_rental_return_period
    @estimate_rental_return_period || period
  end
=end
  def land?
    if residential_sale_id
      p = Property.find_by_id(residential_sale_id)
      unless p.blank?
        (p.property_type == "Land" || p.property_type == "Rural" && p.country == "Australia") || (p.property_type == "Section" && p.country == "New Zealand")
      end
    else
      (property_type == "Land" || property_type == "Rural" && country == "Australia") || (property_type == "Section" && country == "New Zealand")
    end
  end

  def land_and_studio?
    if residential_sale_id
      p = Property.find_by_id(residential_sale_id)
      unless p.blank?
        p.property_type = property_type if property_type == "Studio"
        ((p.property_type == "Studio" || p.property_type == "Land" || p.property_type == "Rural") && p.country == "Australia") || (p.property_type == "Section" && p.country == "New Zealand")
      end
    else
      ((property_type == "Studio" || property_type == "Land" || property_type == "Rural") && country == "Australia") || (property_type == "Section" && country == "New Zealand")
    end
  end

  def get_property_id
    return self.residential_sale_id
  end

  private

  def formalize
    self.tax_rate =  formalize_number tax_rate, tax_rate_period
    self.water_rate =  formalize_number water_rate, water_rate_period
    self.condo_strata_fee =  formalize_number condo_strata_fee, condo_strata_fee_period
    self.estimate_rental_return =  formalize_number estimate_rental_return, estimate_rental_return_period
  end

  def formalize_number(number, period)
    case period
    when 'Per Week' then number*12.0
    when 'Per Month' then number*3.0
    when 'Per Quarter' then number
    when 'Per Year' then number/4.0
    when 'Per Decade' then number/40.0
    else number
    end
  end

end
