class LivestockSaleDetail < ActiveRecord::Base
  attr_accessor :property_type, :skip_validation
  attr_accessor :country

  belongs_to :livestock_sale_type
  belongs_to :livestock_type
  belongs_to :livestock_sex
  belongs_to :livestock_pregnancy
  belongs_to :livestock_frame
  belongs_to :livestock_condition
  belongs_to :livestock_quality
  belongs_to :livestock_horn
  belongs_to :livestock_tick_status

  belongs_to :livestock_sale
  belongs_to :property, :foreign_key => "livestock_sale_id"


  include DetailHelper


  def get_property_id
    return self.livestock_sale_id
  end
end
