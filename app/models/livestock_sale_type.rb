class LivestockSaleType < ActiveRecord::Base
  has_many :livestock_sale_details


  # Class Method

  def self.auction_type_id
    find_by_name('auction').id rescue nil;
  end
end
