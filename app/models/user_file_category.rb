# == Schema Info
# Schema version: 20090614051807
#
# Table name: user_file_categories
#
#  id        :integer(4)      not null, primary key
#  office_id :integer(4)
#  name      :string(255)

class UserFileCategory < ActiveRecord::Base
  belongs_to :office

  validates_uniqueness_of :name, :scope => :office_id
end
