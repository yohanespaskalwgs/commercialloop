class LivestockType < ActiveRecord::Base
  has_many :livestock_sale_detail
  has_many :child_types, :class_name => "LivestockType", :foreign_key => :parent_id

  belongs_to :parent_type, :class_name => "LivestockType", :foreign_key => :parent_id

  named_scope :child_types, :conditions => ["parent_id IS NOT NULL"]

  # Class method

  def self.parent_types
    find_all_by_parent_id(nil)
  end

  def self.build_select_options
    array = []
    array << ["Please select", nil]

    parent_types.each do |p|
      array << [ p.name, p.id ]

      children = p.child_types
      children.each do |c|
        array << [ "--#{c.name}", c.id ]
      end
    end

    return array
  end
end
