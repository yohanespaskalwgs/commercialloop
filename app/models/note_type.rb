class NoteType < ActiveRecord::Base
  has_many :contact_notes
  has_many :company_notes
  belongs_to :agent_user
  belongs_to :creator, :class_name => 'AgentUser', :foreign_key => "creator_id"
  validates_presence_of :name
end
