# == Schema Info
# Schema version: 20090614051807
#
# Table name: suburbs
#
#  id         :integer(4)      not null, primary key
#  country_id :integer(4)
#  state_id   :integer(4)
#  name       :string(255)
#  postcode   :string(10)
class Suburb < ActiveRecord::Base
  define_index do
    indexes :name
  end

  belongs_to :country
  belongs_to :state
  belongs_to :town_country

  validates_presence_of :name

  named_delegate :code, :to => :country
  named_delegate :name, :to => :state
  named_delegate :name, :to => :town_country

#  comma do
#    country_code
#    state_name
#    town_country_name
#    name
#    postcode
#  end

  def self.set_post_code(post_code)
    if post_code.size == 4
      post_code = [0].push(post_code).to_s
    end
    if post_code.size == 3
      post_code = [0,0].push(post_code).to_s
    end
    return post_code
  end

  def self.find_all_by_region_name(region)
    self.find(:all, :joins => [:town_country], :conditions => ["town_countries.name = ?", region])
  end

end
