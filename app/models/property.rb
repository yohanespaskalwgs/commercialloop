# == Schema Info
# Schema version: 20090614051807
#
# Table name: properties
#
#  id                   :integer(4)      not null, primary key
#  agent_id             :integer(4)
#  agent_user_id        :integer(4)
#  office_id            :integer(4)
#  primary_contact_id   :integer(4)
#  property_id          :string(255)
#  secondary_contact_id :integer(4)
#  country              :string(255)
#  deal_type            :string(255)
#  description          :text
#  display_address      :boolean(1)      default(TRUE)
#  display_price        :boolean(1)
#  display_price_text   :string(255)
#  headline             :string(255)
#  latitude             :string(50)
#  longitude            :string(50)
#  price                :decimal`(16, 3)
#  price2               :decimal(16, 3)
#  price2_include_tax   :boolean(1)
#  property_type        :string(255)
#  save_status          :string(255)
#  state                :string(255)
#  status               :integer(4)
#  street               :string(255)
#  street_number        :string(255)
#  suburb               :string(255)
#  town_village         :string(255)
#  type                 :string(255)
#  unit_number          :string(255)
#  vendor_email         :string(255)
#  vendor_first_name    :string(255)
#  vendor_last_name     :string(255)
#  vendor_phone         :string(255)
#  zipcode              :string(30)
#  created_at           :datetime
#  deleted_at           :datetime
#  updated_at           :datetime

class Property < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  attr_accessor :skip_validation, :amount, :date, :subdomain, :image_api, :floorplan_api, :brochure_api, :opentime_api, :contact_name,
    :home_number, :work_number, :mobile_number, :team_member_api, :send_image_api

  PER_PAGE = 10

  LEASE = 'Lease'
  SALE = 'Sale'
  BOTH = 'Both'

  MAX_PHOTO = 30
  MAX_PLAN  = 5
  MAX_BROCHURE = 10
  MAX_VAULT = 30
  MAX_MP3 = 1
  MAX_VIDEO = 1

  DRAFT = 'Draft'
  SUBMITTED = 'Submitted'

  #Payment Status

  COMPLETED = 'Completed'

  has_and_belongs_to_many :features


  composed_of(:address, :class_name => 'Address',
    :mapping =>
      [
      [:unit_number, :unit_number]  ,
      [:street_number, :street_number]  ,
      [:street, :street]  ,
      [:suburb, :suburb]  ,
      [:state, :state]   ,
      [:country, :country]
    ])

  belongs_to :agent_user
  has_many :attachments, :dependent => :destroy
  belongs_to :office
  belongs_to :agent
  belongs_to :primary_contact, :class_name => 'AgentUser'
  belongs_to :secondary_contact, :class_name => 'AgentUser'
  belongs_to :third_contact, :class_name => 'AgentUser'
  belongs_to :fourth_contact, :class_name => 'AgentUser'
  belongs_to :agent_contact
  belongs_to :external_agent
  belongs_to :external_agent_contact, :class_name => 'ExternalAgentContact'
  belongs_to :agent_company
  belongs_to :parent_listing, :class_name => "Property", :foreign_key => :parent_listing_id

  has_many :child_listing, :class_name => "LivestockType", :foreign_key => :parent_listing_id
  has_many :match_properties, :dependent => :destroy
  has_many :match_contacts, :dependent => :destroy
  has_many :email_configurations, :dependent => :destroy
  has_many :searches
  has_many :campaigns, :conditions => ['current = false']
  has_many :images, :dependent => :destroy, :as => 'attachable', :order => "position asc", :conditions => { :parent_id => nil }
  has_many :floorplans, :dependent => :destroy, :as => 'attachable', :order => "position asc", :conditions => { :parent_id => nil }
  has_many :videos, :dependent => :destroy, :as => 'attachable'
  has_many :sales_records, :dependent => :destroy
  has_many :tenancy_records, :dependent => :destroy
  has_one :on_holder, :dependent => :destroy
  has_many :residential_sale_details, :foreign_key => "residential_sale_id"
  has_many :residential_lease_details, :foreign_key => "residential_lease_id"
  has_many :commercial_details, :foreign_key => "commercial_id"
  has_many :holiday_lease_details, :foreign_key => "holiday_lease_id"
  has_many :general_sale_details, :foreign_key => "general_sale_id"
  has_many :livestock_sale_details, :foreign_key => "livestock_sale_id"
  has_many :clearing_sales_event_details, :foreign_key => "clearing_sales_event_id"
  has_many :clearing_sale_details, :foreign_key => "clearing_sale_id"
  has_many :brochure, :dependent => :destroy, :as => 'attachable'
  has_many :brochure_images, :dependent => :destroy, :as => :attachable, :order => "position asc"
  has_many :vault_files, :dependent => :destroy, :as => 'attachable'
  has_many :opentimes, :dependent => :destroy
  has_many :past_opentimes, :dependent => :destroy
  has_many :offers, :dependent => :destroy
  has_many :vault_logs
  has_many :extra_details
  has_many :extra_options
  has_many :export_errors
  has_many :export_reports
  has_many :property_viewers, :dependent => :destroy
  has_many :property_portal_exports, :dependent => :destroy
  has_many :property_domain_exports, :dependent => :destroy
  has_many :property_videos, :dependent => :destroy
  has_many :property_contact_enquires, :dependent => :destroy
  has_many :property_company_enquires, :dependent => :destroy
  has_many :property_domains
  has_many :exports_properties
  has_many :tasks, :dependent => :destroy

  has_one :property_domain_project, :dependent => :destroy
  has_one :uploaded_video, :dependent => :destroy, :as => 'attachable'
  has_one :mp3, :dependent => :destroy, :as => 'attachable'
  has_one :property_logo, :dependent => :destroy, :as => 'attachable'
  has_one :current_campaign, :class_name => 'Campaign', :conditions => ['current = true']
  has_one :purchaser
  has_one :extra
  has_one :vault


  # http://api.rubyonrails.org/classes/ActiveRecord/Validations/ClassMethods.html#M001893
  #validates_inclusion_of :display_address, :in => [false, true]
  validates_associated  :office, :agent
  validates_presence_of :primary_contact_id, :unless => proc{|x|["CommercialBuilding", "ClearingSalesEvent", "GeneralSale", "LivestockSale", "ClearingSale"].include? x.type or ["Prospect"].include? x.ownership}
  #  validates_presence_of :description, :unless => proc{|x|["LandRelease", "CommercialBuilding"].include? x.type}
  validates_presence_of :property_type, :unless => (proc{|x|(["CommercialBuilding"].include? x.type)})
  validates_presence_of :country, :state, :suburb, :unless => proc{|x|["LandRelease", "CommercialBuilding", "ClearingSalesEvent", "GeneralSale", "LivestockSale", "ClearingSale"].include? x.type}
  validates_presence_of :street_number, :street
  validates_presence_of :zipcode, :unless => (proc{|x|["ClearingSalesEvent", "GeneralSale", "LivestockSale", "ClearingSale"].include? x.type || x.country == "New Zealand" })
  validates_numericality_of :price, :greater_than => 0, :unless => proc{|x|["NewDevelopment", "Commercial", "ClearingSalesEvent", "GeneralSale", "LivestockSale", "ClearingSale", "CommercialBuilding"].include? x.type}
  validates_numericality_of :price, :greater_than => 5000, :if => :commercial_sale_deal?
  validates_presence_of :price, :unless => (proc{|x|(["ClearingSalesEvent", "GeneralSale", "LivestockSale", "ClearingSale", "CommercialBuilding"].include? x.type) || (x.deal_type == "Lease")})
  validates_presence_of :status, :if => proc{|x|x.type.to_s != "CommercialBuilding"}
  validates_presence_of :headline, :description
  validate :save_vendor_detail, :if => proc{|x|x.skip_validation != true}
  #  validate :tenancy_validation

  before_validation           :set_office, :set_agent
  before_validation_on_create :set_default_status

  before_save :set_geocode
  after_save  :trigger_superapi,:use_spawn, :send_vendor_detail, :check_create_update_time
  #  after_update :send_api_notification
  attr_accessor :role_6_mode
  acts_as_paranoid


  extend ActiveSupport::Memoizable

  define_index do
    indexes id
    indexes [unit_number, street_number, street ], :as => :display_street
    indexes office_id
    indexes suburb
    indexes state
    indexes country
    indexes unit_number
    indexes street_number
    indexes street
    where ['deleted_at is NULL']
    has updated_at, primary_contact_id
    set_property :delta => WorklingDelta
  end


  TYPES = [['Residential Sale', 'ResidentialSale'], ['Residential Lease', 'ResidentialLease'], ['Commercial Sale/Lease', 'Commercial'], ['Building', 'CommercialBuilding'],['Holiday Lease', 'HolidayLease'], ['Project Sale', 'ProjectSale'], ['Business Sale', 'BusinessSale'], ['New Development', 'NewDevelopment'], ['Land Release', 'LandRelease'], ['General Sale', 'GeneralSale'], ['Livestock Sale', 'LivestockSale'], ['Clearing Sales Event', 'ClearingSalesEvent'], ['Clearing Sale', 'ClearingSale']]

  STATUS_NAME = ['available', 'sold', 'leased', 'withdrawn', 'underoffer', 'draft', 'off_market', 'on_hold']
  STATUS = { :available => 1, :sold => 2, :leased => 3, :withdrawn => 4, :underoffer => 5 , :draft => 6, :off_market => 7, :on_hold => 8}

  METRIC_MONTHLY = 0
  METRIC_WEEKLY = 1
  METRIC_QARTERLY = 2
  METRIC_YEARLY = 3

  DONT_DISPLAY_PRICE = 0
  DISPLAY_PRICE      = 1
  DISPLAY_PRICE_TEXT = 2

  #before_save :convert_metrics_for_residential_lease,:if =>lambda{|p|p.type == "ResidentialLease"}
  #before_save :convert_metrics_for_holiday_lease,:if =>lambda{|p|p.type == "HolidayLease"}
  #before_save :convert_metrics,:if =>lambda{|p|p.type != "ResidentialLease"}
  delegate :display_street, :to  => :address

  named_scope :submitted, :conditions => ["save_status <> ?", DRAFT]
  named_scope :draft, :conditions => ["save_status = ?", DRAFT]
  named_scope :status_draft, :conditions => ["status = ?", STATUS[:draft]]
  named_scope :available, :conditions => ["status = ?", STATUS[:available]]
  named_scope :sold, :conditions => ["status = ?", STATUS[:sold]]
  named_scope :leased, :conditions => ["status = ?", STATUS[:leased]]
  named_scope :soldleased, :conditions => ["status = ? or status = ?", STATUS[:sold], STATUS[:leased]]
  named_scope :withdrawn, :conditions => ["status = ?", STATUS[:withdrawn]]
  named_scope :underoffer, :conditions => ["status = ?", STATUS[:underoffer]]
  named_scope :on_hold, :conditions => ["status = ?", STATUS[:on_hold]]
  named_scope :off_market, :conditions => ["status = ?", STATUS[:off_market]]

  named_scope :residential_sale, :conditions => ["properties.type = ?", "ResidentialSale"]
  named_scope :residential_lease, :conditions => ["properties.type = ?", "ResidentialLease"]
  named_scope :commercial, :conditions => ["properties.type = ?", "Commercial"]
  named_scope :commercial_building, :conditions => ["properties.type = ?", "CommercialBuilding"]
  named_scope :holiday_lease, :conditions => ["properties.type = ?", "HolidayLease"]
  named_scope :project_sale, :conditions => ["properties.type = ?", "ProjectSale"]
  named_scope :business_sale, :conditions => ["properties.type = ?", "BusinessSale"]
  named_scope :new_development, :conditions => ["properties.type = ?", "NewDevelopment"]
  named_scope :land_release, :conditions => ["properties.type = ?", "LandRelease"]
  named_scope :general_sale, :conditions => ["properties.type = ?", "GeneralSale"]
  named_scope :livestock_sale, :conditions => ["properties.type = ?", "LivestockSale"]
  named_scope :clearing_sales_event, :conditions => ["properties.type = ?", "ClearingSalesEvent"]
  named_scope :clearing_sale, :conditions => ["properties.type = ?", "ClearingSale"]

  named_scope :residential_building_parent_listing, :conditions => ["properties.type = ? AND is_have_children_listing = ?", "NewDevelopment", "1"]
  named_scope :land_release_parent_listing, :conditions => ["properties.type = ? AND is_have_children_listing = ?", "LandRelease", "1"]
  named_scope :commercial_building_parent_listing, :conditions => ["properties.type = ? AND is_have_children_listing = ?", "CommercialBuilding", "1"]
  named_scope :general_sale_parent_listing, :conditions => ["properties.type = ? AND is_have_children_listing = ?", "GeneralSale", "1"]
  named_scope :livestock_sale_parent_listing, :conditions => ["properties.type = ? AND is_have_children_listing = ?", "LivestockSale", "1"]
  named_scope :clearing_sales_event_parent_listing, :conditions => ["properties.type = ? AND is_have_children_listing = ?", "ClearingSalesEvent", "1"]

  named_scope :by_keyword, (lambda do |search_keyword|
      {:conditions => ["properties.type LIKE ? OR properties.suburb LIKE ? OR properties.headline LIKE ? OR properties.number_of_suites LIKE ? OR properties.size_from LIKE ? OR properties.size_to LIKE ? OR properties.country LIKE ? OR properties.state LIKE ? OR properties.town_village LIKE ? OR properties.street LIKE ? OR properties.street_number LIKE ? OR properties.unit_number LIKE ?", '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%']}
    end)

  def save_vendor_detail
    unless agent_contact_id.blank?
    contact = AgentContact.find(agent_contact_id)
      if ["Sale", "Lease", "Both"].include?(deal_type)
        contact_category = contact.contact_category_relations.find(:first, :conditions => ["category_contact_id = ?", 2])
        if contact_category.blank?
          errors.add(:agent_contact_id, "must be a Property Owner!")
        end
      end
    end
  end

  def is_lease?
    rv = false
    if self.deal_type == "Lease" && self.type.to_s == "Commercial"
      rv = true
    end
    rv
  end

  def is_parent_residential_building?
    rv = ''
    parent = Property.find(self.parent_listing_id)
    property_type = parent.type.to_s
    if property_type == "NewDevelopment"
      rv = true
    else
      rv = false
    end
    rv
  end

  def get_parent_building_suburb
    suburb = ''
    parent = Property.find(self.parent_listing_id)
    suburb = parent.suburb
    return suburb
  end

  def self.available_children_listing_count(x)
    available = Property.find_all_by_parent_listing_id(x.id)
    return available
  end

  def self.basic_search(search_keyword, start, limit, page, agent_id, office_id, order,sort,dir)
    page = start.to_i / limit.to_i + 1
    unless sort == 'sqm' || sort == 'I/E'
      Property.paginate(:all, :include =>[:primary_contact], :conditions => ["(properties.property_type LIKE ? OR properties.type LIKE ? OR properties.suburb LIKE ? OR properties.headline LIKE ? OR properties.number_of_suites LIKE ? OR properties.size_from LIKE ? OR properties.size_to LIKE ? OR properties.country LIKE ? OR properties.state LIKE ? OR properties.town_village LIKE ? OR properties.street LIKE ? OR properties.street_number LIKE ? OR properties.unit_number LIKE ? ) AND properties.agent_id = ? AND properties.office_id = ?", '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', '%'+search_keyword+'%', agent_id, office_id],:select => "properties.id,properties.agent_id,properties.office_id,properties.headline,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,users.first_name,users.last_name,properties.size_from,properties.size_to,properties.number_of_suites,properties.detail.floor_area", :order => order, :page =>page, :per_page => limit.to_i)
    else
      details = []
      property_ids = nil
      order_by = dir

      [[NewDevelopmentDetail, "new_development_id"], [LandReleaseDetail, "land_release_id"], [CommercialBuildingDetail, "commercial_building_id"], [ResidentialLeaseDetail, "residential_lease_id"], [ResidentialSaleDetail, "residential_sale_id"], [CommercialDetail, "commercial_id"], [ProjectSaleDetail, "project_sale_id"], [HolidayLeaseDetail, "holiday_lease_id"], [BusinessSaleDetail, "business_sale_id"], [GeneralSale, "general_sale_id"], [LivestockSale, "livestock_sale_id"], [ClearingSalesEvent, "clearing_sales_event_id"], [ClearingSale, "clearing_sale"]].each do |detail|
        if sort == 'sqm'
          detail.first.find(:all, :select => "floor_area, #{detail.last}", :order => "floor_area #{order_by}").each{|data| details << [(!data.floor_area.nil? && data.floor_area != "" ? data.floor_area : "-1"), data.get_property_id]}
        elsif sort == 'I/E'
          detail.first.find(:all, :select => "internal_area, #{detail.last}", :order => "internal_area #{order_by}").each{|data| details << [(!data.internal_area.nil? && data.internal_area != "" ? data.internal_area : "-1"), data.get_property_id]}
        end
      end

      if order_by == "ASC"
        property_ids = details.sort{|x, y| y.first.to_f <=> x.first.to_f}.collect{|x| x.last}
      else
        property_ids = details.sort{|x, y| x.first.to_f <=> y.first.to_f}.collect{|x| x.last}
      end
      Property.paginate(:include => [:primary_contact], :conditions => "id IN (#{property_ids.join(",")}) AND properties.agent_id = '#{agent_id}' AND properties.office_id = '#{office_id}' AND deleted_at IS NULL AND properties.property_type LIKE '%#{search_keyword}%' OR properties.type LIKE '%#{search_keyword}%' OR properties.suburb LIKE '%#{search_keyword}%' OR properties.headline LIKE '%#{search_keyword}%' OR properties.number_of_suites LIKE '%#{search_keyword}%' OR properties.size_from LIKE '%#{search_keyword}%' OR properties.size_to LIKE '%#{search_keyword}%' OR properties.country LIKE '%#{search_keyword}%' OR properties.state LIKE '%#{search_keyword}%' OR properties.town_village LIKE '%#{search_keyword}%' OR properties.street LIKE '%#{search_keyword}%' OR properties.street_number LIKE '%#{search_keyword}%' OR properties.unit_number LIKE '%#{search_keyword}%'", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
    end
  end

  def self.advanced_search(listing_type,property_type,business_type,business_category,ownership,price_from,price_to,deal_type,availiability,suites,range_from,range_to,type_commercial,type,residential_building,list_status_commercial,commercial_type,list_status_residential,state,region,suburb,agent_internal,network_agent,agent_external,yield_from,yield_to,building_from,building_to,land_from,land_to,price_from_commercial,price_to_commercial,price_from_residential,price_to_residential,annual_rental_from,annual_rental_to,property_owner,floor_from,floor_to,office_from,office_to,warehouse_from,warehouse_to,rent_from,rent_to,lease_from,lease_to,tenant,search_keyword_advanced,start,limit,page,agent_id,office_id,order,sort,dir)
    cond = "properties.agent_id = '#{agent_id}' AND properties.office_id = '#{office_id}'"
    unless residential_building.blank?
      if suburb.blank?
        cond += " AND properties.type = 'NewDevelopment'"
      else
        if listing_type == "Residential"
          cond += " AND properties.type IN ('ResidentialSale', 'ResidentialLease', 'NewDevelopment')" unless listing_type.blank?
        else
          cond += " AND properties.type LIKE '%#{listing_type}%'" unless listing_type.blank?
        end
      end
    else
      if listing_type == "Residential"
        cond += " AND properties.type IN ('ResidentialSale', 'ResidentialLease', 'NewDevelopment')" unless listing_type.blank?
      else
        cond += " AND properties.type LIKE '%#{listing_type}%'" unless listing_type.blank?
      end
    end
    cond += " AND (" unless property_type.blank? || business_type.blank? || business_category.blank? || price_from.blank? || price_to.blank? || availiability.blank? || suites.blank? || range_from.blank? || range_to.blank? || type.blank? || list_status_commercial.blank? || commercial_type.blank? || list_status_residential.blank? || state.blank? || region.blank? || suburb.blank? || agent_internal.blank? || network_agent.blank? || agent_external.blank? || yield_from.blank? || yield_to.blank?
    cond += " AND properties.type = '#{property_type}'" unless property_type.blank?
    cond += " AND properties.property_type = '#{business_type}'" unless business_type.blank?
    cond += " AND properties.price = '#{price_from}'" unless price_from.blank?
    cond += " AND properties.to_price = '#{price_to}'" unless price_to.blank?
    cond += " AND properties.deal_type = '#{deal_type}'" unless deal_type.blank?
    cond += " AND properties.number_of_suites = '#{suites}'" unless suites.blank?
    cond += " AND properties.size_from = '#{range_from}'" unless range_from.blank?
    cond += " AND properties.size_to = '#{range_to}'" unless range_to.blank?
    cond += " AND properties.id IN (SELECT `properties`.id FROM `business_sale_details` INNER JOIN `properties` ON `business_sale_details`.business_sale_id = `properties`.id WHERE `business_sale_details`.category = '#{business_category}')" unless business_category.blank?
    if business_type.blank?
      cond += " AND properties.ownership = '#{ownership}'" unless ownership.blank?
    end
    if ["Commercial", "Residential", "NewDevelopment", "ResidentialSale", "ResidentialLease", "CommercialBuilding", "ProjectSale", "HolidayLease", "BusinessSale", "LandRelease"].include?(listing_type)
      if type_commercial.blank?
        cond += " AND properties.property_type IN (#{type.map(&:inspect).join(', ')})" unless type.blank?
      else
        cond += " AND properties.property_type IN (#{type_commercial.map(&:inspect).join(', ')})" unless type_commercial.blank?
      end
      cond += " AND properties.suburb IN (#{residential_building.map(&:inspect).join(', ')})" unless residential_building.blank?
      if listing_type == "Commercial"
        # cond += " AND properties.type = '#{commercial_type}'" unless commercial_type.blank?
        cond += " AND properties.deal_type = '#{list_status_commercial}'" unless list_status_commercial.blank?
      elsif listing_type == "Residential"
        cond += " AND properties.deal_type = '#{list_status_residential}'" unless list_status_residential.blank?
      end
      cond += " AND properties.status = '#{availiability}'" unless availiability.blank?
      cond += " AND properties.state = '#{state}'" unless state.blank?
      unless region.blank?
        region_split = region.split(",")
        region_split.map_with_index{|x, i| region_split.delete_at(i) if x == ""  }
        region_val = region_split.map{|x| "\'#{x}\'"}
        region_fix = region_val.join(',')
        cond += " AND properties.town_village IN (#{region_fix})"
        #cond += " AND properties.town_village = '#{region}'" unless region.blank? || region == 'Please select'
      end
      if residential_building.blank?
        suburb_split = suburb.split(",")
        suburb_split.map_with_index{|x, i| suburb_split.delete_at(i) if x == ""  }
        suburb_val = suburb_split.map{|x| "\'#{x}\'"}
        suburb_fix = suburb_val.join(',')
        cond += " AND properties.suburb IN (#{suburb_fix})" unless suburb.blank?
        #cond += " AND properties.suburb = '#{suburb}'" unless suburb.blank? || suburb == 'Please select'
      end
      cond += " AND properties.agent_user_id = '#{agent_internal}'" unless agent_internal.blank?
      cond += " AND ((properties.secondary_contact_id = '#{network_agent}' AND properties.secondary_contact_type = 'Network') OR (properties.third_contact_id = '#{network_agent}' AND properties.third_contact_type = 'Network') OR (properties.fourth_contact_id = '#{network_agent}' AND properties.fourth_contact_type = 'Network'))" unless network_agent.blank?
      cond += " AND properties.external_agent_contact_id = '#{agent_external}'" unless agent_external.blank?
      if !yield_from.blank? || !yield_to.blank? || !building_from.blank? || !building_to.blank? || !land_from.blank? || !land_to.blank? || !price_from_commercial.blank? || !price_to_commercial.blank? || !price_from_residential.blank? || !price_to_residential.blank? || !annual_rental_from.blank? || !annual_rental_to.blank? || !floor_from.blank? || !floor_to.blank? || !office_from.blank? || !office_to.blank? || !warehouse_from.blank? || !warehouse_to.blank? || !rent_from.blank? || !rent_to.blank? || !lease_from.blank? || !lease_to.blank? || !tenant.blank?
        if ["Commercial", "CommercialBuilding"].include?(listing_type)
          cond += " AND properties.id IN (SELECT `properties`.id FROM `commercial_details` INNER JOIN `properties` ON `commercial_details`.commercial_id = `properties`.id"
        elsif ["Residential", "ResidentialSale"].include?(listing_type)
          cond += " AND properties.id IN (SELECT `properties`.id FROM `residential_sale_details` INNER JOIN `properties` ON `residential_sale_details`.residential_sale_id = `properties`.id"
        elsif listing_type == "ResidentialLease"
          cond += " AND properties.id IN (SELECT `properties`.id FROM `residential_lease_details` INNER JOIN `properties` ON `residential_lease_details`.residential_lease_id = `properties`.id"
        elsif listing_type == "NewDevelopment"
          cond += " AND properties.id IN (SELECT `properties`.id FROM `new_development_details` INNER JOIN `properties` ON `new_development_details`.new_development_id = `properties`.id"
        elsif listing_type == "LandRelease"
          cond += " AND properties.id IN (SELECT `properties`.id FROM `land_release_details` INNER JOIN `properties` ON `land_release_details`.land_release_id = `properties`.id"
        elsif listing_type == "ProjectSale"
          cond += " AND properties.id IN (SELECT `properties`.id FROM `project_sale_details` INNER JOIN `properties` ON `project_sale_details`.project_sale_id = `properties`.id"
        elsif listing_type == "HolidayLease"
          cond += " AND properties.id IN (SELECT `properties`.id FROM `holiday_lease_details` INNER JOIN `properties` ON `holiday_lease_details`.new_development_id = `properties`.id"
        elsif listing_type == "BusinessSale"
          cond += " AND properties.id IN (SELECT `properties`.id FROM `business_sale_details` INNER JOIN `properties` ON `business_sale_details`.business_sale_id = `properties`.id"
          # cond += " AND `business_sale_details`.category = '#{business_category}'" unless business_category.blank? || business_category == 'Please select'
        end
        cond += " WHERE"

        if !yield_from.blank?
          if !yield_to.blank?
            cond += " '#{yield_from}' <= `rental_yield` AND `rental_yield` <= '#{yield_to}'"
          else
            cond += " `rental_yield` >= '#{yield_from}'"
          end
          is_rental_not_empty = true
        else
          if !yield_to.blank?
            cond += " `rental_yield` <= '#{yield_to}'"
            is_rental_not_empty = true
          end
        end

        if !building_from.blank?
          if !building_to.blank?
            if is_rental_not_empty == true
              cond += " AND"
            end
            cond += " `area_range` >= '#{building_from}' AND `area_range_to` <= '#{building_to}'"
          else
            if is_rental_not_empty == true
              cond += " AND"
            end
            cond += " `area_range` >= '#{building_from}'"
          end
          is_area_not_empty = true
        else
          if !building_to.blank?
            cond += " `area_range_to` <= '#{building_to}'"
            is_area_not_empty = true
          end
        end

        if !land_from.blank?
          if !land_to.blank?
            if is_rental_not_empty == true || is_area_not_empty == true
              cond += " AND"
            end
            cond += " `land_area` >= '#{land_from}' AND `land_area` <= '#{land_to}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true
              cond += " AND"
            end
            cond += " `land_area` >= '#{land_from}'"
          end
          is_land_area_not_empty = true
        else
          if !land_to.blank?
            cond += " `land_area` <= '#{land_to}'"
            is_land_area_not_empty = true
          end
        end

        if !price_from_commercial.blank?
          if !price_to_commercial.blank?
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true
              cond += " AND"
            end
            cond += " `sale_price_from` >= '#{price_from_commercial}' AND `sale_price_to` <= '#{price_to_commercial}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true
              cond += " AND"
            end
            cond += " `sale_price_from` >= '#{price_from_commercial}'"
          end
          is_price_not_empty = true
        else
          if !price_from_commercial.blank?
            cond += " `sale_price_to` <= '#{price_to_commercial}'"
            is_price_not_empty = true
          end
        end

        if !price_from_residential.blank?
          if !price_to_residential.blank?
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true
              cond += " AND"
            end
            cond += " `price` >= '#{price_from_residential}' AND `to_price` <= '#{price_to_residential}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true
              cond += " AND"
            end
            cond += " `price` >= '#{price_from_residential}'"
          end
          is_price_not_empty = true
        else
          if !price_from_residential.blank?
            cond += " `to_price` <= '#{price_to_residential}'"
            is_price_not_empty = true
          end
        end

        if !annual_rental_from.blank?
          if !annual_rental_to.blank?
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true
              cond += " AND"
            end
            cond += " `rental_price` >= '#{annual_rental_from}' AND `rental_price_to` <= '#{annual_rental_to}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true
              cond += " AND"
            end
            cond += " `rental_price` >= '#{annual_rental_from}'"
          end
          is_annual_rental_not_empty = true
        else
          if !annual_rental_from.blank?
            cond += " `rental_price_to` <= '#{annual_rental_to}'"
            is_annual_rental_not_empty = true
          end
        end

        if !floor_from.blank?
          if !floor_to.blank?
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true
              cond += " AND"
            end
            cond += " `floor_area` >= '#{floor_from}' AND `floor_area` <= '#{floor_to}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true
              cond += " AND"
            end
            cond += " `floor_area` >= '#{floor_from}'"
          end
          is_floor_not_empty = true
        else
          if !floor_from.blank?
            cond += " `floor_area` <= '#{floor_to}'"
            is_floor_not_empty = true
          end
        end

        if !office_from.blank?
          if !office_to.blank?
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true
              cond += " AND"
            end
            cond += " `office_area` >= '#{office_from}' AND `office_area` <= '#{office_to}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true
              cond += " AND"
            end
            cond += " `office_area` >= '#{office_from}'"
          end
          is_office_not_empty = true
        else
          if !office_from.blank?
            cond += " `office_area` <= '#{office_to}'"
            is_office_not_empty = true
          end
        end

        if !warehouse_from.blank?
          if !warehouse_to.blank?
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true || is_office_not_empty == true
              cond += " AND"
            end
            cond += " `warehouse_area` >= '#{warehouse_from}' AND `warehouse_area` <= '#{warehouse_to}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true || is_office_not_empty == true
              cond += " AND"
            end
            cond += " `warehouse_area` >= '#{warehouse_from}'"
          end
          is_warehouse_not_empty = true
        else
          if !warehouse_from.blank?
            cond += " `warehouse_area` <= '#{warehouse_to}'"
            is_warehouse_not_empty = true
          end
        end

        if !rent_from.blank?
          if !rent_to.blank?
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true || is_office_not_empty == true || is_warehouse_not_empty == true
              cond += " AND"
            end
            cond += " `current_rent` >= '#{rent_from}' AND `current_rent` <= '#{rent_to}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true || is_office_not_empty == true || is_warehouse_not_empty == true
              cond += " AND"
            end
            cond += " `current_rent` >= '#{rent_from}'"
          end
          is_current_rent_not_empty = true
        else
          if !rent_from.blank?
            cond += " `current_rent` <= '#{rent_to}'"
            is_current_rent_not_empty = true
          end
        end

        if !lease_from.blank?
          if !lease_to.blank?
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true || is_office_not_empty == true || is_warehouse_not_empty == true || is_current_rent_not_empty == true
              cond += " AND"
            end
            cond += " `lease_commencement` = '#{lease_from}' AND `lease_end` = '#{lease_to}'"
          else
            if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true || is_office_not_empty == true || is_warehouse_not_empty == true || is_current_rent_not_empty == true
              cond += " AND"
            end
            cond += " `lease_commencement` = '#{lease_from}'"
          end
          is_lease_end_not_empty = true
        else
          if !lease_from.blank?
            cond += " `lease_end` = '#{lease_to}'"
            is_lease_end_not_empty = true
          end
        end

        if !tenant.blank?
          if is_rental_not_empty == true || is_area_not_empty == true || is_land_area_not_empty == true || is_price_not_empty == true || is_annual_rental_not_empty == true || is_floor_not_empty == true || is_office_not_empty == true || is_warehouse_not_empty == true || is_current_rent_not_empty == true || is_current_rent_not_empty == true || is_lease_end_not_empty == true
            cond += " AND" unless tenant.blank?
          end
          cond += " properties.id IN (SELECT `properties`.id FROM `tenancy_records` INNER JOIN `properties` ON `tenancy_records`.property_id = `properties`.id WHERE `tenancy_records`.agent_contact_id = '#{tenant}')" unless tenant.blank?
        end
        cond += ")"
      end
    end
    cond += ")" unless property_type.blank? || business_type.blank? || business_category.blank? || price_from.blank? || price_to.blank? || availiability.blank? || suites.blank? || range_from.blank? || range_to.blank? || type.blank? || list_status.blank? || state.blank? || region.blank? || suburb.blank? || agent_internal.blank? || network_agent.blank? || agent_external.blank? || yield_from.blank? || yield_to.blank?
    cond += " AND `properties`.agent_contact_id IN (SELECT `properties`.agent_contact_id FROM `agent_contacts` INNER JOIN `properties` ON `agent_contacts`.id = `properties`.agent_contact_id WHERE `agent_contacts`.category_contact_id = 2 AND `properties`.agent_contact_id = '#{property_owner}')" unless property_owner.blank?
    cond += " AND (properties.property_type LIKE '%#{search_keyword_advanced}%' OR properties.suburb LIKE '%#{search_keyword_advanced}%' OR properties.headline LIKE '%#{search_keyword_advanced}%' OR properties.state LIKE '%#{search_keyword_advanced}%' OR properties.number_of_suites LIKE '%#{search_keyword_advanced}%' OR properties.size_from LIKE '%#{search_keyword_advanced}%' OR properties.size_to LIKE '%#{search_keyword_advanced}%' OR properties.country LIKE '%#{search_keyword_advanced}%' OR properties.town_village LIKE '%#{search_keyword_advanced}%' OR properties.street LIKE '%#{search_keyword_advanced}%' OR properties.street_number LIKE '%#{search_keyword_advanced}%' OR properties.unit_number LIKE '%#{search_keyword_advanced}%') OR properties.primary_contact_id = (SELECT `agent_users`.id FROM `properties` INNER JOIN `agent_users` ON `properties` WHERE `agent_users`.id = `properties`.primary_contact_id AND `agent_users`.first_name LIKE '%#{search_keyword_advanced}%' OR `agent_users`.last_name LIKE '%#{search_keyword_advanced}%')" unless search_keyword_advanced.blank?

    page = start.to_i / limit.to_i + 1
    unless %w(I/E sqm).include?(sort) #modified by winarti@kiranatama.com since this code sort == 'sqm' || 'I/E' contain deprecated warning. Please contact me if any errors
      Property.paginate(:all, :include => [:primary_contact], :conditions => cond, :page => page, :per_page => limit.to_i, :order => order)
    else
      details = []
      property_ids = nil
      order_by = dir

      [[NewDevelopmentDetail, "new_development_id"], [LandReleaseDetail, "land_release_id"], [CommercialBuildingDetail, "commercial_building_id"], [ResidentialLeaseDetail, "residential_lease_id"], [ResidentialSaleDetail, "residential_sale_id"], [CommercialDetail, "commercial_id"], [ProjectSaleDetail, "project_sale_id"], [HolidayLeaseDetail, "holiday_lease_id"], [BusinessSaleDetail, "business_sale_id"], [GeneralSale, "general_sale_id"], [LivestockSale, "livestock_sale_id"], [ClearingSalesEvent, "clearing_sales_event_id"], [ClearingSale, "clearing_sale"]].each do |detail|
        if sort == 'sqm'
          detail.first.find(:all, :select => "floor_area, #{detail.last}", :order => "floor_area #{order_by}").each{|data| details << [(!data.floor_area.nil? && data.floor_area != "" ? data.floor_area : "-1"), data.get_property_id]}
        elsif sort == 'I/E'
          detail.first.find(:all, :select => "internal_area, #{detail.last}", :order => "internal_area #{order_by}").each{|data| details << [(!data.internal_area.nil? && data.internal_area != "" ? data.internal_area : "-1"), data.get_property_id]}
        end
      end

      if order_by == "ASC"
        property_ids = details.sort{|x, y| y.first.to_f <=> x.first.to_f}.collect{|x| x.last}
      else
        property_ids = details.sort{|x, y| x.first.to_f <=> y.first.to_f}.collect{|x| x.last}
      end
      #      Property.paginate(:include => [:primary_contact], :conditions => "id IN (#{property_ids.join(",")}) AND #{cond}", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
      Property.paginate(:all, :include =>[:primary_contact], :conditions => cond, :page =>page, :per_page => limit.to_i, :order => order)
    end
  end

  def send_api_notification
    #self.use_spawn unless self.deleted_at.blank?
  end

  def send_vendor_detail
    if [225, 244, 443, 654, 1025, 243].include?(self.office_id)
      self.agent_user.update_attribute(:updated_at, Time.now) unless self.agent_user.blank?
    end
  end

  def check_create_update_time
    if created_at <= Time.now && updated_at < created_at
      old_updated = updated_at
      Log.create(:message => "Updated_at Invalid, was : #{old_updated.inspect} fixed to: #{Time.now}, for property : #{id}")
      ActiveRecord::Base.connection.execute("UPDATE properties SET updated_at = '#{Time.now.strftime('%F %T')}' WHERE id = #{self.id}")
    end
    #    if created_at > Time.now
    #      ActiveRecord::Base.connection.execute("UPDATE properties SET created_at = '#{Time.now.strftime('%F %T')}' WHERE id = #{self.id}")
    #    end
  end

  def use_spawn
    #synchronize domain before send
    PropertyDomainExport.synchronize_domain_export(self)

    return if self.team_member_api == true
    return if self.status == 6
    return if self.agent.blank?
    return if self.office.blank?
    return if self.office.http_api_enable != true

    counter = Time.zone.now.to_i
    GLOBAL_ATTR["Host-#{self.office_id}"] = GLOBAL_ATTR["Host-#{self.office_id}"].blank? ? "#{self.agent.developer.entry}.commercialloopcrm.com.au" : GLOBAL_ATTR["Host-#{self.office_id}"]
    property_domains = self.property_domain_exports.find(:all, :conditions => "send_api=1")
    is_http_api = self.office.http_api_enable
    is_office_active = self.office.status == 'active' ? true : false
    return if property_domains.blank? || (is_http_api == false || is_http_api.blank?) || is_office_active == false
    data = self.property_parser
    property_domains.each do |property_domain|
      ipn_url = nil
      domain_id = nil
      property_domain_domain = property_domain.domain

      next if property_domain_domain.blank?
      if property_domain_domain.portal_export == true
        next if property_domain_domain.name.blank?
        property_portal_exports = self.property_portal_exports
        unless property_portal_exports.blank?
          property_portal_exports.each do |px|
            portal = px.portal
            next if portal.blank?
            next if px.uncheck == true
            next if portal.ftp_url.blank?
            portal_url = portal.ftp_url.downcase.gsub('http://', '')
            portal_url = portal_url.gsub('www.', '')
            if property_domain_domain.name.downcase.include?(portal_url)
              domain_id = property_domain.domain_id
              if property_domain_domain.name.downcase.include?("propertypoint.com.au")
                next if self.state.blank?
                if self.state.strip == "TAS"
                  ipn_url = portal.ftp_directory
                  break
                end
              elsif property_domain_domain.name.downcase.include?("land.com.au")
                if self.type.to_s == "ProjectSale" || self.property_type.to_s == "Land"
                  ipn_url = portal.ftp_directory
                  break
                end
              elsif property_domain_domain.name.downcase.include?("getrealty.com.au")
                next if self.state.blank?
                if self.state.strip == "ACT"
                  ipn_url = portal.ftp_directory
                  break
                end
              elsif property_domain_domain.name.downcase.include?("virginhomes.com.au")
                include_feature = self.features.find(:all,:conditions => "`name` LIKE '%newly built property%' OR `name` LIKE '%new developer listing%'") unless self.features.blank?
                if !include_feature.blank? || self.type.to_s == "ProjectSale"
                  ipn_url = portal.ftp_directory
                  break
                end
              elsif property_domain_domain.name.downcase.include?("propertyinvestor.com.au")
                if self.type.to_s == "ResidentialSale" || self.type.to_s == "ProjectSale" || self.type.to_s == "NewDevelopment"
                  ipn_url = portal.ftp_directory
                  break
                end
              else
                ipn_url = portal.ftp_directory
                break
              end
            end
          end
        end
      else
        next if property_domain_domain.ipn.blank?
        ipn_url = property_domain_domain.ipn
        domain_id = property_domain.domain_id
      end
      next if ipn_url.blank?

      spawn(:kill => true) do
        ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE property_id = #{self.id} And domain_id = #{domain_id}")
        PropertyDomain.create({:property_id => self.id, :domain_id => domain_id, :count_sent => 0, :cron_checked => 0, :sender_uid => counter})
        self.check_before_send(data, domain_id, counter)
        GC.start
      end
    end
  end

  def trigger_domain(domain_id)
    counter = Time.zone.now.to_i
    ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE property_id = #{self.id} And domain_id = #{domain_id}")
    PropertyDomain.create({:property_id => self.id, :domain_id => domain_id, :count_sent => 0, :cron_checked => 0, :sender_uid => counter})
    ActiveRecord::Base.connection.execute("UPDATE properties SET updated_at = '#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}' WHERE id=#{self.id}")
    self.check_before_send("", domain_id, counter)
  end

  def send_property_domain(domain_id)
    counter = Time.zone.now.to_i
    GLOBAL_ATTR["Host-#{self.office_id}"] = GLOBAL_ATTR["Host-#{self.office_id}"].blank? ? "#{self.agent.developer.entry}.commercialloopcrm.com.au" : GLOBAL_ATTR["Host-#{self.office_id}"]
    spawn(:kill => true) do
      # delete cron check record
      ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE property_id = #{self.id} And domain_id = #{domain_id}")
      # create record for new update
      PropertyDomain.create({:property_id => self.id, :domain_id => domain_id, :count_sent => 0, :cron_checked => 0, :sender_uid => counter})
      self.check_before_send("", domain_id, counter)
    end
  end

  def check_before_send(data, domain_id, counter)
    property_domain = PropertyDomain.find_by_sql("SELECT * FROM property_domains WHERE cron_checked = 0 And domain_id = #{domain_id} And property_id = #{self.id} And sender_uid = #{counter}").first
    if !property_domain.blank?
      ActiveRecord::Base.connection.execute("UPDATE property_domains SET updated_at = '#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}' WHERE id=#{property_domain.id}")
      self.send_property_api(data, domain_id, counter)
      return false
    else
      return true
    end
  end

  def send_property_api(data, domain_id, counter)
    return if domain_id.blank?
    return if counter.blank?
    pd = PropertyDomain.find_by_sql("SELECT * FROM property_domains WHERE cron_checked = 0 And domain_id = #{domain_id} And property_id = #{self.id} And sender_uid = #{counter}").first
    domain = Domain.find_by_sql("SELECT * FROM domains WHERE id = #{pd.domain_id}").first
    return if domain.blank?
    ipn_url = domain.ipn
    token = Digest::SHA1.hexdigest("#{Time.now}")[8..50]
    GLOBAL_ATTR["#{self.office_id}-#{self.agent_id}-#{self.id}"] = token
    token_counter = "#{token}_#{counter}_#{domain_id}"
    call_back_url = GLOBAL_ATTR["Host-#{self.office_id}"].blank? ? "commercialloopcrm.com.au" : GLOBAL_ATTR["Host-#{self.office_id}"]
    parsed_data = (data.blank? ? self.property_parser : data)
    unless parsed_data.blank?
      ActiveRecord::Base.connection.execute("UPDATE property_domains SET count_sent = (count_sent + 1) WHERE id=#{pd.id}")
      ActiveRecord::Base.connection.execute("INSERT INTO `api_requests`(`url`,`post_data`,`created`) VALUES ('#{ipn_url}?update_property_#{self.id}', '#{parsed_data}&callback_url=http://#{call_back_url}/agents/#{self.agent_id}/offices/#{self.office_id}/properties/#{self.id}/property_notification?token=#{token_counter}&type=property', '#{Time.now.to_formatted_s(:db_format)}')")
    end
  end

  def property_parser
    begin
      property = self.attributes
      encoded_property = {}
      property.each do |prop|
        if prop.last.is_a?(String)
          encoded_property.merge!(prop.first => Base64.encode64(prop.last))
        else
          encoded_property.merge!(prop.first => prop.last)
        end
      end

      rental_seasons = self.is_a?(HolidayLease) ? self.rental_seasons : nil
      rent_seasons = {}
      unless rental_seasons.blank?
        rental_seasons.each_with_index do |rent, i|
          rent_seasons.merge!(i.to_s => rent.attributes)
        end
      end

      property_detail = self.detail.attributes.delete_if { |x,y| ["residential_sale_id","residential_lease_id","business_sale_id","commercial_id","commercial_building_id","project_sale_id","holiday_lease_id", "new_development_id","land_release_id", "general_sale_id", "livestock_sale_id", "clearing_sales_event_id", "clearing_sale_id", "id"].include?(x)  }
      property_detail["bedrooms"] = (property_detail["bedrooms"].blank? ? "" : property_detail["bedrooms"] + 0.5) if property_detail["half_bedroom"] == true
      property_detail["bathrooms"] = (property_detail["bathrooms"].blank? ? "" : property_detail["bathrooms"] + 0.5) if property_detail["half_bathroom"] == true
      encoded_property_detail ={}
      property_detail.each do |prop_detail|
        if prop_detail.first == "land_area" || prop_detail.first == "floor_area"
          encoded_property_detail.merge!(prop_detail.first => prop_detail.last)
        else
          if prop_detail.last.is_a?(String)
            encoded_property_detail.merge!(prop_detail.first => Base64.encode64(prop_detail.last))
          else
            encoded_property_detail.merge!(prop_detail.first => prop_detail.last)
          end
        end
      end

      if self.is_a?(ProjectSale)
        prd = Property.find_by_id(self.detail.project_development_id)
        encoded_property_detail.merge!("project_development_name" => (Base64.encode64(prd.detail.development_name) unless prd.blank?))
        pdt = ProjectDesignType.find_by_id(self.detail.project_design_type_id)
        encoded_property_detail.merge!("project_design_type_name" => (Base64.encode64(pdt.name) unless pdt.blank?))
        ps = ProjectStyle.find_by_id(self.detail.project_style_id)
        encoded_property_detail.merge!("project_style_name" => (Base64.encode64(ps.name) unless ps.blank?))
        ndc = NewDevelopmentCategory.find_by_id(self.detail.new_development_category_id)
        encoded_property_detail.merge!("new_development_category_name" => (Base64.encode64(ndc.name) unless ndc.blank?))
      end

      encoded_extra ={}
      extra = Extra.find(:first, :conditions => "`property_id` = #{self.id}")
      unless extra.blank?
        extra_attr = extra.attributes
        extra_attr.each do |ext|
          if ext.last.is_a?(String)
            encoded_extra.merge!(ext.first => Base64.encode64(ext.last))
          else
            encoded_extra.merge!(ext.first => ext.last)
          end
        end
      end

      encoded_extra_details = {}
      extra_details = ExtraDetail.find(:all, :conditions => "`property_id` = #{self.id}")
      unless extra_details.blank?
        extra_details.map{|x| x.attributes}.each_with_index do |optd, i|
          optd.keys.each{|d|optd[d] = Base64.encode64(optd[d]) if optd[d].is_a?(String)}
          encoded_extra_details.merge!(i.to_s => optd)
        end
      end

      encoded_extra_detail_names = {}
      unless self.office.extra_detail_names.blank?
        self.office.extra_detail_names.map{|x| x.attributes}.each_with_index do |optdn, i|
          optdn.keys.each{|dn|optdn[dn] = Base64.encode64(optdn[dn]) if optdn[dn].is_a?(String)}
          encoded_extra_detail_names.merge!(i.to_s => optdn)
        end
      end

      encoded_extra_options = {}
      unless self.office.extra_options.blank?
        self.office.extra_options.map{|x| x.attributes}.each_with_index do |opteo, i|
          opteo.keys.each{|eo|opteo[eo] = Base64.encode64(opteo[eo]) if opteo[eo].is_a?(String)}
          encoded_extra_options.merge!(i.to_s => opteo)
        end
      end

      encoded_newdev_categories = {}
      newcat_property_id = self.id
      (newcat_property_id = property_detail["project_development_id"] unless property_detail["project_development_id"].blank? ) if self.type == "ProjectSale"
      new_cat = NewDevelopmentCategory.find(:all, :conditions => "property_id = #{newcat_property_id}")
      unless new_cat.blank?
        new_cat.map{|x| x.attributes}.each_with_index do |optnc, i|
          optnc.keys.each{|nc|optnc[nc] = Base64.encode64(optnc[nc]) if optnc[nc].is_a?(String)}
          encoded_newdev_categories.merge!(i.to_s => optnc)
        end
      end

      users = {}
      features = {}
      opentimes = {}

      unless self.primary_contact.blank?
        primary_attr = self.primary_contact.attributes
        encoded_primary = {}
        primary_attr.each do |prim_user|
          if prim_user.last.is_a?(String)
            encoded_primary.merge!(prim_user.first => Base64.encode64(prim_user.last))
          else
            encoded_primary.merge!(prim_user.first => prim_user.last)
          end
        end

        unless encoded_primary.blank?
          encoded_primary.merge!({ "landscape_images"=>{"1"=>( self.primary_contact.landscape_image.find_by_position(1).blank? ? "" : self.primary_contact.landscape_image.find_by_position(1).real_filename ),"2"=>( self.primary_contact.landscape_image.find_by_position(2).blank? ? "" : self.primary_contact.landscape_image.find_by_position(2).real_filename )},"portrait_images"=>{"1"=>( self.primary_contact.portrait_image.find_by_position(1).blank? ? "" : self.primary_contact.portrait_image.find_by_position(1).real_filename ),"2"=>( self.primary_contact.portrait_image.find_by_position(2).blank? ? "" : self.primary_contact.portrait_image.find_by_position(2).real_filename )}})
          encoded_primary.merge!({ "landscape_images_s3"=>{"1"=>( self.primary_contact.landscape_image.find_by_position(1).blank? ? "" : self.primary_contact.landscape_image.find_by_position(1).public_filename ),"2"=>( self.primary_contact.landscape_image.find_by_position(2).blank? ? "" : self.primary_contact.landscape_image.find_by_position(2).public_filename )},"portrait_images_s3"=>{"1"=>( self.primary_contact.portrait_image.find_by_position(1).blank? ? "" : self.primary_contact.portrait_image.find_by_position(1).public_filename ),"2"=>( self.primary_contact.portrait_image.find_by_position(2).blank? ? "" : self.primary_contact.portrait_image.find_by_position(2).public_filename )}})
          users.merge!({"primary" => encoded_primary})
        end
      end

      unless self.secondary_contact.blank?
        secondary_attr = self.secondary_contact.attributes
        encoded_secondary = {}
        secondary_attr.each do |sec_user|
          if sec_user.first == "conjunctional_office_name" || sec_user.first == "conjunction_rea_agent_id"
            data_tm = ""
            if !self.secondary_contact.conjunctional_id.blank?
              cn = Conjunctional.find_by_id(self.secondary_contact.conjunctional_id)
              unless cn.blank?
                if sec_user.first == "conjunctional_office_name"
                  data_tm = cn.conjunctional_office_name
                else
                  data_tm = cn.conjunctional_rea_agent_id
                end
              end
            else
              data_tm = sec_user.last
            end
            encoded_secondary.merge!(sec_user.first => Base64.encode64(data_tm.to_s))
          else
            if sec_user.last.is_a?(String)
              encoded_secondary.merge!(sec_user.first => Base64.encode64(sec_user.last))
            else
              encoded_secondary.merge!(sec_user.first => sec_user.last)
            end
          end
        end
        unless encoded_secondary.blank?
          encoded_secondary.merge!({ "landscape_images"=>{"1"=>( self.secondary_contact.landscape_image.find_by_position(1).blank? ? "" : self.secondary_contact.landscape_image.find_by_position(1).real_filename ),"2"=>( self.secondary_contact.landscape_image.find_by_position(2).blank? ? "" : self.secondary_contact.landscape_image.find_by_position(2).real_filename )},"portrait_images"=>{"1"=>( self.secondary_contact.portrait_image.find_by_position(1).blank? ? "" : self.secondary_contact.portrait_image.find_by_position(1).real_filename ),"2"=>( self.secondary_contact.portrait_image.find_by_position(2).blank? ? "" : self.secondary_contact.portrait_image.find_by_position(2).real_filename )}})
          encoded_secondary.merge!({ "landscape_images_s3"=>{"1"=>( self.secondary_contact.landscape_image.find_by_position(1).blank? ? "" : self.secondary_contact.landscape_image.find_by_position(1).public_filename ),"2"=>( self.secondary_contact.landscape_image.find_by_position(2).blank? ? "" : self.secondary_contact.landscape_image.find_by_position(2).public_filename )},"portrait_images_s3"=>{"1"=>( self.secondary_contact.portrait_image.find_by_position(1).blank? ? "" : self.secondary_contact.portrait_image.find_by_position(1).public_filename ),"2"=>( self.secondary_contact.portrait_image.find_by_position(2).blank? ? "" : self.secondary_contact.portrait_image.find_by_position(2).public_filename )}})
          users.merge!({"secondary" => encoded_secondary})
        end
      end

      unless self.third_contact.blank?
        third_attr = self.third_contact.attributes
        encoded_third = {}
        third_attr.each do |third_user|
          if third_user.first == "conjunctional_office_name" || third_user.first == "conjunction_rea_agent_id"
            data_tm = ""
            if !self.third_contact.conjunctional_id.blank?
              cn = Conjunctional.find_by_id(self.third_contact.conjunctional_id)
              unless cn.blank?
                if third_user.first == "conjunctional_office_name"
                  data_tm = cn.conjunctional_office_name
                else
                  data_tm = cn.conjunctional_rea_agent_id
                end
              end
            else
              data_tm = third_user.last
            end
            encoded_third.merge!(third_user.first => Base64.encode64(data_tm.to_s))
          else
            if third_user.last.is_a?(String)
              encoded_third.merge!(third_user.first => Base64.encode64(third_user.last))
            else
              encoded_third.merge!(third_user.first => third_user.last)
            end
          end
        end

        unless encoded_third.blank?
          encoded_third.merge!("landscape_images"=>{"1"=>( self.third_contact.landscape_image.find_by_position(1).blank? ? "" : self.third_contact.landscape_image.find_by_position(1).real_filename ),"2"=>( self.third_contact.landscape_image.find_by_position(2).blank? ? "" : self.third_contact.landscape_image.find_by_position(2).real_filename )},"portrait_images"=>{"1"=>( self.third_contact.portrait_image.find_by_position(1).blank? ? "" : self.third_contact.portrait_image.find_by_position(1).real_filename ),"2"=>( self.third_contact.portrait_image.find_by_position(2).blank? ? "" : self.third_contact.portrait_image.find_by_position(2).real_filename )})
          encoded_third.merge!("landscape_images_s3"=>{"1"=>( self.third_contact.landscape_image.find_by_position(1).blank? ? "" : self.third_contact.landscape_image.find_by_position(1).public_filename ),"2"=>( self.third_contact.landscape_image.find_by_position(2).blank? ? "" : self.third_contact.landscape_image.find_by_position(2).public_filename )},"portrait_images_s3"=>{"1"=>( self.third_contact.portrait_image.find_by_position(1).blank? ? "" : self.third_contact.portrait_image.find_by_position(1).public_filename ),"2"=>( self.third_contact.portrait_image.find_by_position(2).blank? ? "" : self.third_contact.portrait_image.find_by_position(2).public_filename )})
          users.merge!({"third" => encoded_third})
        end
      end

      unless self.fourth_contact.blank?
        fourth_attr = self.fourth_contact.attributes
        encoded_fourth = {}
        fourth_attr.each do |fourth_user|
          if fourth_user.first == "conjunctional_office_name" || fourth_user.first == "conjunction_rea_agent_id"
            data_tm = ""
            if !self.fourth_contact.conjunctional_id.blank?
              cn = Conjunctional.find_by_id(self.fourth_contact.conjunctional_id)
              unless cn.blank?
                if fourth_user.first == "conjunctional_office_name"
                  data_tm = cn.conjunctional_office_name
                else
                  data_tm = cn.conjunctional_rea_agent_id
                end
              end
            else
              data_tm = fourth_user.last
            end
            encoded_fourth.merge!(fourth_user.first => Base64.encode64(data_tm.to_s))
          else
            if fourth_user.last.is_a?(String)
              encoded_fourth.merge!(fourth_user.first => Base64.encode64(fourth_user.last))
            else
              encoded_fourth.merge!(fourth_user.first => fourth_user.last)
            end
          end
        end

        unless encoded_fourth.blank?
          encoded_fourth.merge!("landscape_images"=>{"1"=>( self.fourth_contact.landscape_image.find_by_position(1).blank? ? "" : self.fourth_contact.landscape_image.find_by_position(1).real_filename ),"2"=>( self.fourth_contact.landscape_image.find_by_position(2).blank? ? "" : self.fourth_contact.landscape_image.find_by_position(2).real_filename )},"portrait_images"=>{"1"=>( self.fourth_contact.portrait_image.find_by_position(1).blank? ? "" : self.fourth_contact.portrait_image.find_by_position(1).real_filename ),"2"=>( self.fourth_contact.portrait_image.find_by_position(2).blank? ? "" : self.fourth_contact.portrait_image.find_by_position(2).real_filename )})
          encoded_fourth.merge!("landscape_images_s3"=>{"1"=>( self.fourth_contact.landscape_image.find_by_position(1).blank? ? "" : self.fourth_contact.landscape_image.find_by_position(1).public_filename ),"2"=>( self.fourth_contact.landscape_image.find_by_position(2).blank? ? "" : self.fourth_contact.landscape_image.find_by_position(2).public_filename )},"portrait_images_s3"=>{"1"=>( self.fourth_contact.portrait_image.find_by_position(1).blank? ? "" : self.fourth_contact.portrait_image.find_by_position(1).public_filename ),"2"=>( self.fourth_contact.portrait_image.find_by_position(2).blank? ? "" : self.fourth_contact.portrait_image.find_by_position(2).public_filename )})
          users.merge!({"fourth" => encoded_fourth})
        end
      end

      self.opentimes.each_with_index{|o,i|
        opentimes = opentimes.merge!(i.to_s => {"date"=> o.date, "property_id"=> o.property_id, "id"=> o.id, "start_time"=> o.start_time.strftime("%H:%M"), "end_time"=> o.end_time.strftime("%H:%M")})
      } unless self.opentimes.blank?

      encoded_images = {}
      unless self.images.blank?
        self.images.map{|x| x.attributes}.each_with_index do |img, i|
          img.keys.each{|t| img[t] = Base64.encode64(img[t]) if img[t].is_a?(String)}
          im = self.images.find_by_id(img["id"].to_i)
          img.merge!({"image_s3_url" => im.public_filename, "image_s3_url_thumb" => im.public_filename(:thumb), "image_s3_url_medium" => im.public_filename(:medium), "image_s3_url_original" => im.public_filename(:original)}) unless im.blank?
          encoded_images.merge!(i.to_s => img)
        end
      end

      encoded_brochures = {}
      unless self.brochure.blank?
        self.brochure.find(:all, :conditions => "`send_to_api` IS NULL OR `send_to_api` = 1").map{|x| x.attributes}.each_with_index do |bro, i|
          bro.keys.each{|t|bro[t] = Base64.encode64(bro[t]) if bro[t].is_a?(String)}
          br = self.brochure.find_by_id(bro["id"].to_i)
          bro.merge!({"brochure_s3_url" => br.public_filename}) unless br.blank?
          encoded_brochures.merge!(i.to_s => bro)
        end
      end

      encoded_floorplans = {}
      unless self.floorplans.blank?
        self.floorplans.map{|x| x.attributes}.each_with_index do |fp, i|
          fp.keys.each{|t|fp[t] = Base64.encode64(fp[t]) if fp[t].is_a?(String)}
          fpm = self.floorplans.find_by_id(fp["id"].to_i)
          fp.merge!({"image_s3_url" => fpm.public_filename, "image_s3_url_thumb" => fpm.public_filename(:thumb), "image_s3_url_medium" => fpm.public_filename(:medium), "image_s3_url_original" => fpm.public_filename(:original)}) unless fpm.blank?
          encoded_floorplans.merge!(i.to_s => fp)
        end
      end

      self.features.each_with_index { |f,i| features.merge!({i.to_s => Base64.encode64(f.name)}) } unless self.features.blank?

      encoded_videos = {}
      unless self.property_videos.blank?
        self.property_videos.map{|x| x.attributes}.each_with_index do |vd, i|
          vd.keys.each{|t| vd[t] = Base64.encode64(vd[t]) if vd[t].is_a?(String)}
          encoded_videos.merge!(i.to_s => vd)
        end
      end

      unless self.mp3.blank?
        audio_o = self.mp3.attributes
        audio_o.merge!({"audio_s3_url" => self.mp3.public_filename})
      end
      audio = self.mp3.blank? ? nil : {"0" => audio_o}

      unless self.property_logo.blank?
        logo_o = self.property_logo.attributes
        logo_o.merge!({"logo_s3_url" => self.property_logo.public_filename})
      end
      logo = self.property_logo.blank? ? nil : {"0" => logo_o}

      unless self.uploaded_video.blank?
        uploaded_video = self.uploaded_video.attributes
        uploaded_video.merge!({"uploaded_video_s3_url" => self.uploaded_video.public_filename})
      end

      encoded_testimonials = {}
      cn = ContactNote.find(:all, :conditions => "`note_type_id`= 11 And `property_id` = #{self.id}")
      cn.each_with_index{|note, i| encoded_testimonials.merge!(i.to_s => {"first_name" => Base64.encode64(note.agent_contact.first_name), "last_name" => Base64.encode64(note.agent_contact.last_name), "contact_name" => Base64.encode64(note.agent_contact.full_name), "date" => note.note_date, "note" => Base64.encode64(note.description)})} unless cn.blank?

      ["updated_at","created_at","deleted_at"].each do |date|
        property[date] = property[date].to_formatted_s(:db_format) unless property[date].blank?
        property_detail[date] = property_detail[date].to_formatted_s(:db_format) unless property_detail[date].blank?
      end

      data = "property=#{encoded_property.to_json}&details=#{encoded_property_detail.to_json}&rental_seasons=#{rent_seasons.to_json}&contacts=#{users.to_json}&brochures=#{encoded_brochures.to_json}&opentimes=#{opentimes.to_json}&images=#{encoded_images.to_json}&floorplans=#{encoded_floorplans.to_json}&features=#{features.to_json}&videos=#{encoded_videos.to_json}&audio=#{audio.to_json}&logo=#{logo.to_json}&uploaded_video=#{uploaded_video.to_json}&extra=#{encoded_extra.to_json}&encoded_extra_details=#{encoded_extra_details.to_json}&extra_detail_names=#{encoded_extra_detail_names.to_json}&extra_options=#{encoded_extra_options.to_json}&new_development_categories=#{encoded_newdev_categories.to_json}&testimonials=#{encoded_testimonials.to_json}"
      GC.start
      return data
    rescue Exception => ex
      Log.create(:message => "property ---> #{self.errors.inspect} ::: ---> errors : #{ex.inspect} ")
      return ""
    end
  end

  def self.check_color(x)
    photo =  x.image_exist ?  "/images/icons/icon_photo.gif" :  "/images/icons/icon_photo_grey.gif"
    plan = x.floorplan_exist  ?  "/images/icons/icon_plan.gif" :  "/images/icons/icon_plan_grey.gif"
    open = x.opentimes.blank? ? "/images/icons/icon_date_grey.gif" : "/images/icons/icon_date.gif"
    payment =  x.payment_status == "Completed" ? "/images/icons/icon_dollar_grey.gif" : "/images/icons/icon_dollar_red.gif"
    deal_type = ""

    unless x.deal_type.blank?
      if x.deal_type == "Sale"
        deal_type = "/images/icons/icon_com_sale.png"
      elsif x.deal_type == "Lease"
        deal_type = "/images/icons/icon_com_lease.png"
      else
        deal_type = "/images/icons/icon_com_both.png"
      end
    end
    return [x.id,photo,plan,open,payment,x.office.property_payment_required,x.office.office_paypal_account,x.office.property_listing_cost,x.property_type,x.office.office_currency_code,x.type,deal_type,x.is_have_children_listing,x.status,Property::STATUS_NAME[x.status.to_i-1], x.payment_status.to_s.downcase,x.on_hold_by, x.on_hold_by_user_type]
  end

  def self.check_tenancy_record(x)
    record = ""

    unless x.status.blank?
      record = x.tenancy_record.blank? ? "" : x.tenancy_record.id
    end
    return [x.id, record]
  end

  def self.check_transaction_record(x)
    record = ""

    unless x.status.blank?
      record = x.sales_records.blank? ? "" : x.sales_record.id
    end
    return [x.id, record]
  end

  def self.check_price(x)
    price = x.price
    price = x.detail.current_rent if x.type.to_s == "Commercial" && x.deal_type == "Lease"
    return price
  end

  def self.check_relationship(x)
    relationship = ''
    if x.type.to_s == 'Commercial'
      if ['Both', 'Sale'].include?(x.deal_type)
        relationship = 'Property Owner'
      else
        relationship = 'Tenant'
      end
    end
    return relationship
  end

  def self.get_contact(x)
    contact = ''
    if ["ResidentialSale", "ResidentialLease", "ProjectSale", "NewDevelopment", "LandRelease", "BusinessSale", "HolidayLease"].include?(x.type.to_s)
      unless x.purchaser.blank?
        unless x.purchaser.agent_contact.blank?
          contact = x.purchaser.agent_contact
        end
      end
    elsif x.type.to_s == "Commercial"
      if ["Sale", "Both"].include?(x.deal_type.to_s)
        if x.valid_transactions
          unless x.sales_records.valid.blank?
            unless x.sales_records.valid.first.agent_contact.blank?
              contact = x.sales_records.valid.first.agent_contact
            end
          end
        end
      else
        if x.valid_tenancies
          unless x.tenancy_records.valid.blank?
            unless x.tenancy_records.valid.first.agent_contact.blank?
              contact = x.tenancy_records.valid.first.agent_contact
            end
          end
        end
      end
    end
    id = contact.blank? ? "" : (contact.check_valid? == false ? "" : contact.id)
    email = contact.blank? ? "" : (contact.email.blank? ? "" : contact.email)
    full_name = contact.blank? ? "" : (contact.full_name.blank? ? (contact.contact_data_alternative.blank? ? "" : contact.contact_data_alternative) : contact.full_name)
    company_name = contact.blank? ? "" : (contact.agent_company.blank? ? "" : contact.agent_company.company)
    return [id, full_name, email, company_name, contact]
  end

  def self.abbrv_properties(name)
    case name.to_s.downcase
    when "residentialsale"
      return "ResSale"
    when "residentiallease"
      return "ResLease"
    when "commercial"
      return "Commercial"
    when "commercialbuilding"
      return "Building"
    when "businesssale"
      return "BusSale"
    when "projectsale"
      return "HouseLand"
    when "holidaylease"
      return "HolLease"
    when "newdevelopment"
      return "ResBuilding"
    when "landrelease"
      return "LandRelease"
    when "generalsale"
      return "GeneralSale"
    when "livestocksale"
      return "LivestockSale"
    when "clearingsalesevent"
      return "ClearingSalesEvent"
    when "clearingsale"
      return "ClearingSale"
    end
  end

  def self.build_sorting(sort,dir)
    case sort
    when 'id'
      return 'properties.id '+dir
    when 'status'
      return 'properties.status '+dir
    when 'listing'
      return 'properties.type '+dir
    when 'property_type'
      return 'properties.property_type '+dir
    when 'address'
      return 'properties.unit_number, properties.street_number , properties.street '+dir
    when 'type'
      return 'properties.property_type '+dir
    when 'suburb'
      return 'properties.suburb '+dir
    when 'price'
      return 'properties.price '+dir
    when 'assigned_to'
      return 'users.first_name  '+dir
    when 'updated_at'
      return 'properties.updated_at '+dir
      # when 'suites'
      #   return 'properties.number_of_suites '+dir
    when 'suite'
      return 'CAST(properties.unit_number AS DECIMAL(10,2)) '+dir
    when 'floor'
      return 'properties.detail.number_of_floors '+dir
    when 'level'
      return 'commercial_details.number_of_floors '+dir
    when 'tenancy'
      return 'tenancy_records.contact '+dir
    when 'start_date'
      return 'tenancy_records.start_date '+dir
    when 'end_date'
      return 'tenancy_records.end_date '+dir
    when 'storage'
      return 'residential_sale_details.storage '+dir
    when 'sold_price'
      return 'purchasers.amount '+dir
    when 'sold_date'
      return 'purchasers.date '+dir
    when 'purchaser'
      return 'agent_contacts.first_name '+dir
    when 'property_type'
      return 'properties.property_type '+dir
    end
  end

  def valid_transactions
    rv = false
    unless self.sales_records.blank?
      valid = self.sales_records.find(:first, :conditions => "is_deleted = '0'")
      self.sales_records.blank? ? rv = false : (valid.blank? ? rv = false : rv = true)
    else
      rv = false
    end
    rv
  end

  def valid_tenancies
    rv = false
    unless self.tenancy_records.blank?
      valid = self.tenancy_records.find(:first, :conditions => "is_deleted = '0'")
      self.tenancy_records.blank? ? rv = false : (valid.blank? ? rv = false : rv = true)
    else
      rv = false
    end
    rv
  end

  def is_have_transaction?
    rv = false
    self.sales_records.blank? ? rv = false : rv = true
    rv
  end

  def is_have_tenancy?
    rv = false
    self.tenancy_records.blank? ? rv = false : rv = true
    rv
  end

  def self.sort_by_customize(sort,controller,dir,page,limit,agent,office,property)
    details = []
    property_ids = nil
    order_by = dir

    [[NewDevelopmentDetail, "new_development_id"], [LandReleaseDetail, "land_release_id"], [CommercialBuildingDetail, "commercial_building_id"], [ResidentialLeaseDetail, "residential_lease_id"], [ResidentialSaleDetail, "residential_sale_id"], [CommercialDetail, "commercial_id"], [ProjectSaleDetail, "project_sale_id"], [HolidayLeaseDetail, "holiday_lease_id"], [BusinessSaleDetail, "business_sale_id"]].each do |detail|
      if sort == 'sqm'
        detail.first.find(:all, :select => "floor_area, #{detail.last}", :order => "floor_area #{order_by}").each{|data| details << [(!data.floor_area.nil? && data.floor_area != "" ? data.floor_area : "-1"), data.get_property_id]}
      elsif sort == 'bedroom'
        detail.first.find(:all, :select => "bedrooms, #{detail.last}", :order => "bedrooms #{order_by}").each{|data| details << [(!data.bedrooms.nil? && data.bedrooms != "" ? data.bedrooms : "-1"), data.get_property_id]}
      elsif sort == 'bathroom'
        detail.first.find(:all, :select => "bathrooms, #{detail.last}", :order => "bathrooms #{order_by}").each{|data| details << [(!data.bathrooms.nil? && data.bathrooms != "" ? data.bathrooms : "-1"), data.get_property_id]}
      elsif sort == 'carspace'
        detail.first.find(:all, :select => "carport_spaces, #{detail.last}", :order => "carport_spaces #{order_by}").each{|data| details << [(!data.carport_spaces.nil? && data.carport_spaces != "" ? data.carport_spaces : "-1"), data.get_property_id]}
      elsif sort == 'I/E'
        detail.first.find(:all, :select => "internal_area, #{detail.last}", :order => "internal_area #{order_by}").each{|data| details << [(!data.internal_area.nil? && data.internal_area != "" ? data.internal_area : "-1"), data.get_property_id]}
      end
    end

    if order_by == "ASC"
      property_ids = details.sort{|x, y| y.first.to_f <=> x.first.to_f}.collect{|x| x.last}
    else
      property_ids = details.sort{|x, y| x.first.to_f <=> y.first.to_f}.collect{|x| x.last}
    end

    if controller == "properties" || controller == "search_results"
      Property.paginate(:include => [:primary_contact], :conditions => "id IN (#{property_ids.join(",")}) AND agent_id = '#{agent}' AND office_id = '#{office}' AND deleted_at IS NULL", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
    elsif controller == "residential_building_children_listings" || controller == "commercial_building_children_listings"
      Property.paginate(:include => [:primary_contact], :conditions => "id IN (#{property_ids.join(",")}) AND agent_id = '#{agent}' AND office_id = '#{office}' AND deleted_at IS NULL AND parent_listing_id = '#{property}'", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
    elsif controller == "available_children_listings"
      Property.paginate(:include => [:primary_contact], :conditions => "id IN (#{property_ids.join(",")}) AND agent_id = '#{agent}' AND office_id = '#{office}' AND deleted_at IS NULL AND parent_listing_id = '#{property}' AND status = '1'", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
    elsif controller == "sold_children_listings"
      Property.paginate(:include => [:primary_contact], :conditions => "id IN (#{property_ids.join(",")}) AND agent_id = '#{agent}' AND office_id = '#{office}' AND deleted_at IS NULL AND parent_listing_id = '#{property}' AND status = '2'", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
    elsif controller == "children_listings"
      Property.paginate(:include => [:primary_contact], :conditions => "id IN (#{property_ids.join(",")}) AND agent_id = '#{agent}' AND office_id = '#{office}' AND deleted_at IS NULL AND parent_listing_id = '#{property}'", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
    end
  end

  def self.sort_by_one_similiar(page,sort,dir,limit,keyword,size_from,size_to,agent,office)
    details = []
    property_ids = nil
    order_by = dir

    [[NewDevelopmentDetail, "new_development_id"], [LandReleaseDetail, "land_release_id"], [CommercialBuildingDetail, "commercial_building_id"], [ResidentialLeaseDetail, "residential_lease_id"], [ResidentialSaleDetail, "residential_sale_id"], [CommercialDetail, "commercial_id"], [ProjectSaleDetail, "project_sale_id"], [HolidayLeaseDetail, "holiday_lease_id"], [BusinessSaleDetail, "business_sale_id"]].each do |detail|
      detail.first.find(:all, :select => "floor_area, #{detail.last}", :order => "floor_area #{order_by}").each{|data| details << [(!data.floor_area.nil? && data.floor_area != "" ? data.floor_area : "-1"), data.get_property_id]}
    end

    if order_by == "ASC"
      property_ids = details.sort{|x, y| y.first.to_f <=> x.first.to_f}.collect{|x| x.last}
    else
      property_ids = details.sort{|x, y| x.first.to_f <=> y.first.to_f}.collect{|x| x.last}
    end

    if sort == 'sqm'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.id IN (#{property_ids.join(",")}) AND properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.type = '#{keyword.type}' AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country = '#{keyword.country}' AND properties.state = '#{keyword.state}' AND properties.town_village = '#{keyword.town_village}' AND properties.id != '#{keyword.id}'", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
    elsif sort == 'id'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.type = '#{keyword.type}' AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country = '#{keyword.country}' AND properties.state = '#{keyword.state}' AND properties.town_village = '#{keyword.town_village}' AND properties.id != '#{keyword.id}'", :order => "properties.id #{dir}", :page => page, :per_page => limit)
    elsif sort == 'type'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.type = '#{keyword.type}' AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country = '#{keyword.country}' AND properties.state = '#{keyword.state}' AND properties.town_village = '#{keyword.town_village}' AND properties.id != '#{keyword.id}'", :order => "properties.type #{dir}", :page => page, :per_page => limit)
    elsif sort == 'suburb'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.type = '#{keyword.type}' AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country = '#{keyword.country}' AND properties.state = '#{keyword.state}' AND properties.town_village = '#{keyword.town_village}' AND properties.id != '#{keyword.id}'", :order => "properties.suburb #{dir}", :page => page, :per_page => limit)
    elsif sort == 'address'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.type = '#{keyword.type}' AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country = '#{keyword.country}' AND properties.state = '#{keyword.state}' AND properties.town_village = '#{keyword.town_village}' AND properties.id != '#{keyword.id}'", :order => "properties.unit_number, properties.street_number , properties.street  #{dir}", :page => page, :per_page => limit)
    elsif sort == 'suite'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.type = '#{keyword.type}' AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country = '#{keyword.country}' AND properties.state = '#{keyword.state}' AND properties.town_village = '#{keyword.town_village}' AND properties.id != '#{keyword.id}'", :order => "CAST(properties.unit_number AS DECIMAL(10,2))  #{dir}", :page => page, :per_page => limit)
    end
  end

  def self.sort_by_more_similiar(keyword_ids,keyword_types,size_from,size_to,keyword_countries,keyword_states,keyword_town_villages,agent,office,page,sort,dir,limit)
    details = []
    property_ids = nil
    order_by = dir

    [[NewDevelopmentDetail, "new_development_id"], [LandReleaseDetail, "land_release_id"], [CommercialBuildingDetail, "commercial_building_id"], [ResidentialLeaseDetail, "residential_lease_id"], [ResidentialSaleDetail, "residential_sale_id"], [CommercialDetail, "commercial_id"], [ProjectSaleDetail, "project_sale_id"], [HolidayLeaseDetail, "holiday_lease_id"], [BusinessSaleDetail, "business_sale_id"]].each do |detail|
      detail.first.find(:all, :select => "floor_area, #{detail.last}", :order => "floor_area #{order_by}").each{|data| details << [(!data.floor_area.nil? && data.floor_area != "" ? data.floor_area : "-1"), data.get_property_id]}
    end

    if order_by == "ASC"
      property_ids = details.sort{|x, y| y.first.to_f <=> x.first.to_f}.collect{|x| x.last}
    else
      property_ids = details.sort{|x, y| x.first.to_f <=> y.first.to_f}.collect{|x| x.last}
    end

    if sort == 'sqm'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.id IN (#{property_ids.join(",")}) AND properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.id NOT IN (#{keyword_ids}) AND properties.type REGEXP (#{keyword_types}) AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country REGEXP (#{keyword_countries}) AND properties.state REGEXP (#{keyword_states}) AND properties.town_village REGEXP (#{keyword_town_villages})", :order => "FIELD(id, #{property_ids.join(",")})", :page => page, :per_page => limit)
    elsif sort == 'id'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.id NOT IN (#{keyword_ids}) AND properties.type REGEXP (#{keyword_types}) AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country REGEXP (#{keyword_countries}) AND properties.state REGEXP (#{keyword_states}) AND properties.town_village REGEXP (#{keyword_town_villages})", :order => "properties.id #{dir}", :page => page, :per_page => limit)
    elsif sort == 'type'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.id NOT IN (#{keyword_ids}) AND properties.type REGEXP (#{keyword_types}) AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country REGEXP (#{keyword_countries}) AND properties.state REGEXP (#{keyword_states}) AND properties.town_village REGEXP (#{keyword_town_villages})", :order => "properties.type #{dir}", :page => page, :per_page => limit)
    elsif sort == 'suburb'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.id NOT IN (#{keyword_ids}) AND properties.type REGEXP (#{keyword_types}) AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country REGEXP (#{keyword_countries}) AND properties.state REGEXP (#{keyword_states}) AND properties.town_village REGEXP (#{keyword_town_villages})", :order => "properties.suburb #{dir}", :page => page, :per_page => limit)
    elsif sort == 'address'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.id NOT IN (#{keyword_ids}) AND properties.type REGEXP (#{keyword_types}) AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country REGEXP (#{keyword_countries}) AND properties.state REGEXP (#{keyword_states}) AND properties.town_village REGEXP (#{keyword_town_villages})", :order => "properties.unit_number, properties.street_number , properties.street #{dir}", :page => page, :per_page => limit)
    elsif sort == 'suite'
      Property.paginate(:include => [:primary_contact], :conditions => "properties.agent_id = '#{agent.id}' AND properties.office_id = '#{office.id}' AND properties.deleted_at IS NULL AND properties.id NOT IN (#{keyword_ids}) AND properties.type REGEXP (#{keyword_types}) AND properties.size_from <= '#{size_from}' AND properties.size_to >= '#{size_to}' AND properties.country REGEXP (#{keyword_countries}) AND properties.state REGEXP (#{keyword_states}) AND properties.town_village REGEXP (#{keyword_town_villages})", :order => "properties.unit_number #{dir}", :page => page, :per_page => limit)
    end
  end

  def self.valid_type?(_type)
    Property::TYPES.map{|s| s[1].downcase }.include?(_type.to_s.downcase)
  end

  def self.valid_status?(_status)
    Property::STATUS_NAME.include?(_status.to_s)
  end

  def self.types_select_data
    _types = Property::TYPES.dup
    _types.unshift(["All Listing Types", ''] )
  end

  def self.current_statuses
    [Property::STATUS[:available],Property::STATUS[:hidden],Property::STATUS[:contact]]
  end

  def self.quick_search(query, condition, with)
    #search query , :conditions => condition, :with => {:updated_at => 3.months.ago..Time.now}, :star => true
    search query , :conditions => condition, :with => with, :star => true, :per_page => 200
  end

  def self.status_name(x)
    [x.id,Property::STATUS_NAME[x.status.to_i-1], x.payment_status.to_s.downcase]
  end

  def self.check_and_display_status_name(x)

    if x.office.property_payment_required
      if x.payment_status.to_s.downcase != "completed"
        #status will always as draft as long as the payment is not completed by the level4. the updated_at value can not be changed because we don't want the properties table keep changes whenever user look at the listing.
        GLOBAL_ATTR[:status] = true
        GLOBAL_ATTR[:time] = x.updated_at
        x.update_attribute("status", 6)
        GLOBAL_ATTR[:status] = false
      end
    end
    [x.id,Property::STATUS_NAME[x.status.to_i-1], x.payment_status.to_s.downcase]
  end

  def status_name
    Property::STATUS_NAME[status.to_i-1]
  end

  def with_tenancy?
    rv = ''
    if self.type.to_s == "Commercial" && self.deal_type.to_s == "Lease" && self.valid_tenancies == true
      rv = true
    else
      rv = false
    end
    rv
  end

  def lease?
    [ResidentialLease, HolidayLease, Commercial].include? self.class
  end

  def sale?
    ["Sale"].include? self.deal_type
  end

  def contact
    @contact = Contact.new :country => country, :state => state, :suburb => suburb, :street => street, :street_number => street_number, :unit_number => unit_number
  end
  memoize :contact

  def contact=(ct)
    self.country = ct.country
    self.state = ct.state
    self.suburb = ct.suburb
    self.street_number = ct.street_number
    self.street = ct.street
    self.unit_number = ct.unit_number
  end

  def display_price
    dp = read_attribute(:display_price)
    dp.nil? ? Property::DONT_DISPLAY_PRICE : dp
  end

  def display_street
    "#{unit_number.blank? ? '' : unit_number + '/'}#{street_number} #{street}"
  end

  def size_range
    if self.size_from.blank?
      ''
    elsif self.size_to.blank?
      ''
    else
      "#{self.size_from} -" " #{self.size_to}"
    end

  end

  def set_geocode
    return if latitude || longitude
    loc = GeoKit::Geocoders::MultiGeocoder.geocode(address.to_s.gsub('blank', ''))
    # errors.add(:address, "Could not Geocode address") if !loc.success
    self.latitude, self.longitude = loc.lat, loc.lng if loc.success
  end

  def tenancy_validation
    prop_types = ['Land','Development','Leisure','Farmland'] #Validate tenancy except for these property types
    if self.agent.developer.entry == "knightfrank"
      if self.type == "Commercial" && (self.deal_type == "Sale" || self.deal_type == "Both") && !prop_types.include?(self.property_type)
        if self.detail.present? && self.detail.current_leased.blank?
          errors.add("", "Tenancy can't be blank")
          false
        else
          true
        end
      end
    end
  end

  def set_agent
    if agent_id.blank?
      self.agent = self.agent_user.agent
    end
  end

  def set_office
    if office_id.blank?
      self.office = self.agent_user.office
    end
  end

  def set_default_status
    self.status = self.class::STATUS[:available] if status.blank?
  end

  def lease_deal?
    deal_type == LEASE
  end

  def formalize_number(number, period)
    case period
    when 'Nightly' then number*7.0
    when 'Weekly' then number
    when 'Monthly' then (number*12.0)/52.0
    else number
    end
  end

  def sale_deal?
    deal_type == SALE or deal_type == BOTH
  end

  def strict_sale_deal?
    deal_type == SALE
  end

  def commercial_sale_deal?
    (deal_type == SALE or deal_type == BOTH) and type == "Commercial"
  end

  def add_property_portal(ids, status = nil)
    ids ||= []
    to_delete = []
    old_ids = property_portal_exports.map(&:portal_id)
    new_ids = ids.map { |i| i.to_i }
    to_add = new_ids - old_ids

    if status.nil?
      tmp_to_delete = old_ids - new_ids
      tmp_to_delete.each do |id|
        portal_autosubcribe = PortalAutosubcribe.find_by_portal_id_and_office_id(id,self.office_id)
        if portal_autosubcribe.blank?
          to_delete << id
        else
          ActiveRecord::Base.connection.execute("UPDATE `property_portal_exports` SET `uncheck` = 1 WHERE property_id=#{self.id} And portal_id = #{id}")
        end
      end
    end

    PropertyPortalExport.transaction do
      if status.nil?
        property_portal_exports.all(:conditions => { :portal_id => to_delete}).each(&:destroy)
      end
      new_ids.each do |id|
        check_exist = property_portal_exports.find_by_portal_id(id)
        ActiveRecord::Base.connection.execute("UPDATE `property_portal_exports` SET `uncheck` = 0 WHERE property_id=#{self.id} And portal_id = #{id}") unless check_exist.blank?
      end
      to_add.each do |id|
        property_portal_exports.create(:portal_id => id.to_i)
      end
    end
  end

  def update_contact_detail
    unless self.agent_contact.blank?
      self.agent_contact.first_name = self.vendor_first_name
      self.agent_contact.last_name = self.vendor_last_name
      self.agent_contact.email = self.vendor_email
      self.agent_contact.contact_number = self.vendor_phone
      self.agent_contact.home_number = self.home_number
      self.agent_contact.work_number = self.work_number
      self.agent_contact.mobile_number = self.mobile_number
      self.agent_contact.save
    end
  end

  def update_external_agent_detail
    unless self.external_agent_contact.blank?
      self.external_agent_contact.first_name = self.external_agent_first_name
      self.external_agent_contact.last_name = self.external_agent_last_name
      self.external_agent_contact.email = self.external_agent_email
      self.external_agent_contact.contact_number = self.external_agent_phone
      self.external_agent_contact.home_number = self.external_agent_home_number
      self.external_agent_contact.work_number = self.external_agent_work_number
      self.external_agent_contact.mobile_number = self.external_agent_mobile_number
      self.external_agent_contact.save
    end
  end

  def parse_duplication(expected_type)
    arr = nil
    case self.type
    when "ResidentialSale"
      case expected_type
      when "ResidentialLease"
        arr = ["living_area","living_area_metric","method_of_sale", "closing_date","water_rate", "water_rate_period","tax_rate", "tax_rate_period", "condo_strata_fee", "condo_strata_fee_period", "estimate_rental_return", "estimate_rental_return_period", "forthcoming_auction", "auction_place", "residential_sale_id", "auction_price", "auction_date", "auction_time","new_construction","capital_growth","rental_yield", "storage", "balcony_area", "internal_area"]
      when "HolidayLease"
        arr = ["living_area","living_area_metric","method_of_sale", "closing_date","water_rate", "water_rate_period","tax_rate", "estimate_rental_return", "tax_rate_period", "condo_strata_fee", "condo_strata_fee_period", "forthcoming_auction", "auction_place", "residential_sale_id", "auction_price", "auction_date", "auction_time", "estimate_rental_return_period","new_construction","capital_growth","rental_yield", "storage", "balcony_area", "internal_area"]
      when "ProjectSale"
        arr = ["living_area","living_area_metric","method_of_sale", "closing_date","year_built", "water_rate", "water_rate_period","tax_rate", "tax_rate_period", "condo_strata_fee", "condo_strata_fee_period", "number_of_floors", "forthcoming_auction", "auction_place", "residential_sale_id", "auction_price", "auction_date", "auction_time","new_construction","capital_growth","rental_yield", "storage", "balcony_area", "internal_area"]
      end
    when "ResidentialLease"
      case expected_type
      when "ResidentialSale"
        arr = ["bond", "date_available", "residential_lease_id", "fee_terms"]
      when "HolidayLease"
        arr = ["bond", "date_available", "residential_lease_id", "property_url", "fee_terms"]
      when "ProjectSale"
        arr = ["year_built", "number_of_floors", "bond", "date_available", "residential_lease_id", "fee_terms"]
      end
    when "HolidayLease"
      case expected_type
      when "ResidentialSale"
        arr = ["additional_notes", "year_built", "max_persons", "mid_season_price", "high_season_price", "peak_season_price", "bond", "cleaning_fee", "holiday_lease_id", "mid_season_period", "high_season_period", "peak_season_period", "ext_link_1", "ext_link_2", "unavailable_date", "price_period"]
      when "ResidentialLease"
        arr = ["additional_notes", "max_persons", "mid_season_price", "high_season_price", "peak_season_price", "cleaning_fee", "holiday_lease_id", "mid_season_period", "high_season_period", "peak_season_period", "unavailable_date", "price_period"]
      when "ProjectSale"
        arr = ["additional_notes", "year_built", "max_persons", "energy_efficiency_rating", "land_area", "floor_area", "number_of_floors", "mid_season_price", "high_season_price", "peak_season_price", "bond", "cleaning_fee", "holiday_lease_id", "mid_season_period", "high_season_period", "peak_season_period", "unavailable_date", "price_period"]
      end
    when "ProjectSale"
      case expected_type
      when "ResidentialSale"
        arr = ["category", "date_of_completion", "porch_terrace_area", "porch_terrace_area_metric", "garage_area", "land_width", "land_depth", "garage_area_metric", "land_width_metric", "land_depth_metric", "development_name", "design_type", "style", "project_development_id", "project_design_type_id", "project_style_id", "duplication", "project_sale_id", "new_development_category_id"]
      when "ResidentialLease"
        arr = ["category", "date_of_completion", "estimate_rental_return", "estimate_rental_return_period", "porch_terrace_area", "porch_terrace_area_metric", "garage_area", "garage_area_metric", "land_width", "land_depth", "land_width_metric", "land_depth_metric", "development_name", "design_type", "style", "duplication", "project_sale_id", "project_development_id", "project_design_type_id", "project_style_id", "new_development_category_id"]
      when "HolidayLease"
        arr = ["category", "date_of_completion", "estimate_rental_return", "estimate_rental_return_period", "porch_terrace_area", "porch_terrace_area_metric", "garage_area", "garage_area_metric", "land_width", "land_depth", "land_width_metric", "land_depth_metric", "development_name", "design_type", "style", "project_development_id", "project_design_type_id", "project_style_id", "duplication", "project_sale_id", "new_development_category_id"]
      end
    end
    return arr
  end

  def self.create_import_contact(param)
    category_id = ["residential_lease", "commercial", "holiday_lease"].include?(param[:listing_type]) ? 5 : 2
    unless param[:agent_contact_id].blank?
      agent_contact= AgentContact.find_by_id(param[:agent_contact_id])
      agent_contact.update_category_contact(category_id) unless agent_contact.blank?
      return param[:agent_contact_id]
    else
      team = []
      team << param[:primary_contact_id] unless param[:primary_contact_id].blank?
      team << param[:secondary_contact_id] unless param[:secondary_contact_id].blank?

      contact = {:category_contact_id => category_id, :agent_id => param[:agent_id], :office_id => param[:office_id],
        :first_name => param[:vendor_first_name], :last_name => param[:vendor_last_name],
        :email => param[:vendor_email], :contact_number => param[:vendor_phone],
        :home_number => param[:home_number], :work_number => param[:work_number],
        :mobile_number => param[:mobile_number]
      }

      agent_contact = AgentContact.new(contact)
      if agent_contact.save
        agent_contact.add_categories([category_id])
        agent_contact.add_assigns(team)
        agent_contact.add_access(team)
        agent_contact.use_spawn_for_boom
        agent_contact.use_spawn_for_md
        return agent_contact.id
      else
        return nil
      end
    end
  end

  def trigger_superapi
    ActiveRecord::Base.connection.execute("UPDATE superapi_offices SET next_listproperties = UNIX_TIMESTAMP() + 15 WHERE office_id = #{self.office_id}")
  end

  def aliasize
    rv = ''
    if self.type.to_s == "NewDevelopment"
      rv = "Residential Building"
    elsif self.type.to_s == "ProjectSale"
      rv = "House & Land"
    else
      rv = self.type.to_s.titleize
    end
    rv
  end

  def total_area
    rv = 0
    if self.detail
      if self.type.to_s == "Commercial" || self.type.to_s == "CommercialBuilding"
        rv = rv + self.detail.land_area.to_i if self.detail.land_area
        rv = rv + self.detail.floor_area.to_i if self.detail.floor_area
        rv = rv + self.detail.office_area.to_i if self.detail.office_area
        rv = rv + self.detail.warehouse_area.to_i if self.detail.warehouse_area
        rv = rv + self.detail.retail_area.to_i if self.detail.retail_area
        rv = rv + self.detail.other_area.to_i if self.detail.other_area
      elsif self.type.to_s == "ResidentialSale"
        rv = rv + self.detail.land_area.to_i if self.detail.land_area
        rv = rv + self.detail.floor_area.to_i if self.detail.floor_area
        rv = rv + self.detail.internal_area.to_i if self.detail.internal_area
        rv = rv + self.detail.balcony_area.to_i if self.detail.balcony_area
      elsif self.type.to_s == "ResidentialLease" || self.type.to_s == "BusinessSale" || self.type.to_s == "HolidayLease"
        rv = rv + self.detail.land_area.to_i if self.detail.land_area
        rv = rv + self.detail.floor_area.to_i if self.detail.floor_area
      elsif self.type.to_s == "ProjectSale"
        rv = rv + self.detail.floor_area.to_i if self.detail.floor_area
        rv = rv + self.detail.porch_terrace_area.to_i if self.detail.porch_terrace_area
        rv = rv + self.detail.garage_area.to_i if self.detail.garage_area
        rv = rv + self.detail.land_area.to_i if self.detail.land_area
      elsif self.type.to_s == "NewDevelopment" || self.type.to_s == "LandRelease"
        rv = rv + self.size_to
      end
    end
    rv
  end

  def currently_tenanted?
    rv = false
  if self.status == 3
    if self.tenancy_records
      tenant = self.tenancy_records.valid.first
      if tenant
        if tenant.end_date && tenant.end_date != "0000-00-00"
          if tenant.end_date.to_date > Date.today
            rv = true
          end
        end
      end
    end
   end
  end

  def lease_term
    rv = "-"
    if self.tenancy_records
      if self.tenancy_records.valid
        rv = self.tenancy_records.valid.first.lease_term if self.tenancy_records.valid.first
      end
    end
    rv
  end

  def full_address
    self.unit_number + ", " + self.street_number + ", " + self.street + ", " + self.suburb + ", " + self.state + ", " + self.country
  end

  def address_fix
    rv = ""
    unless self.unit_number.blank?
      rv = self.unit_number + "/"
    end
    unless self.street_number.blank?
      rv = rv + self.street_number
    end
    unless self.street.blank?
      rv = rv + " " + self.street
    end
    unless self.suburb.blank?
      rv = rv + ", " + self.suburb
    end
    unless self.state.blank?
      rv = rv + " " + self.state
    end
    unless self.country.blank?
      rv = rv + " " + self.country
    end
    rv
  end

  def short_address
    address = ""

    if self.unit_number.present?
      address = self.unit_number.strip + "/"
    end

    if self.street_number.present?
      address = address + self.street_number.strip
    end

    if self.street.present?
      address = address + " " + self.street.strip
    end

    if self.suburb.present?
      address = address + ", " + self.suburb.strip
    end

    if self.state.present?
      address = address + " " + self.state.strip
    end

    return address
  end

  def show_price
    if self.display_price.to_i == 1 && self.price.present?
      return property_currency(self.price)
    elsif self.display_price.to_i == 2 && self.display_price_text.present?
      return self.display_price_text
    end
  end

end
