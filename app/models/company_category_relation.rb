class CompanyCategoryRelation < ActiveRecord::Base
  belongs_to :agent_company
  belongs_to :category_company
end
