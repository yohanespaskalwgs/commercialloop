# == Schema Info
# Schema version: 20090614051807
#
# Table name: project_sale_details
#
#  id                       :integer(4)      not null, primary key
#  project_sale_id          :integer(4)
#  bathrooms                :integer(4)      default(0)
#  bedrooms                 :integer(4)      default(0)
#  carport_spaces           :integer(4)      default(0)
#  category                 :string(255)
#  date_of_completion       :date
#  design_type              :string(255)
#  development_name         :string(255)
#  duplication              :integer(4)
#  energy_efficiency_rating :string(255)
#  energy_star_rating       :string(255)
#  estimate_rental_return   :decimal(16, 3)
#  floor_area               :decimal(16, 3)
#  garage_area              :decimal(16, 3)
#  garage_spaces            :integer(4)      default(0)
#  land_depth              :decimal(16, 3)
#  land_width              :decimal(16, 3)
#  land_area                :decimal(16, 3)
#  latitude                 :decimal(12, 7)
#  longitude                :decimal(12, 7)
#  off_street_spaces        :integer(4)      default(0)
#  porch_terrace_area       :decimal(16, 3)
#  style                    :string(255)
#  virtual_tour             :string(255)

class ProjectSaleDetail < ActiveRecord::Base
  attr_accessor :property_type
  #attr_accessor :estimate_rental_return_period, :land_width_metric, :land_depth_metric
  #attr_writer :porch_terrace_area_metric, :garage_area_metric

  belongs_to :project_sale

  validates_presence_of :bedrooms, :bathrooms , :unless =>:land?
  validates_presence_of :category
  validates_numericality_of :porch_terrace_area, :land_width, :land_depth, :estimate_rental_return, :greater_than => 0, :allow_nil => true

  validates_presence_of :porch_terrace_area_metric, :unless => lambda {|r| r.porch_terrace_area.blank?}, :on => :create
  validates_presence_of :garage_area_metric, :unless => lambda {|r| r.garage_area.blank?}, :on => :create
  validates_presence_of :estimate_rental_return_period, :unless => lambda { |r| r.estimate_rental_return.blank? }, :on => :create
  validates_presence_of :land_width_metric, :unless => lambda {|r| r.land_width.blank? }, :on => :create
  validates_presence_of :land_depth_metric, :unless => lambda {|r| r.land_depth.blank? }, :on => :create

  #before_save :formalize

  include DetailHelper


=begin
  def porch_terrace_area_metric
    @porch_terrace_area_metric || area_metric
  end

  def garage_area_metric
    @garage_area_metric || area_metric
  end

  def land_width_metric
    @land_width_metric ||length_metric
  end

  def land_depth_metric
    @land_depth_metric || length_metric
  end

  def estimate_rental_return_period
    @estimate_rental_return_period || period
  end
=end

  def get_property_id
    return self.project_sale_id
  end

  def total_area
    (area_standarize(floor_area_metric, floor_area) + area_standarize(porch_terrace_area_metric, porch_terrace_area) + area_standarize(garage_area_metric, garage_area)).to_i
  end

  def land?
    if project_sale_id
      p = Property.find_by_id(project_sale_id)
      unless p.blank?
        p.property_type == "Land" || p.property_type == "Rural"
      end
    else
      property_type == "Land" || property_type == "Rural"
    end
  end

  private

  def formalize
    self.estimate_rental_return = formalize_number estimate_rental_return, estimate_rental_return_period
    self.land_width = formalize_length land_width, land_width_metric
    self.land_depth = formalize_length land_depth, land_depth_metric
  end

  def formalize_number(number, period)
    case period
    when 'Per Week' then number*4.0
    when 'Per Month' then number
    when 'Per Year' then number/12.0
    when 'Per Decade' then number/120.0
    else number
    end
  end

  def formalize_length(length, metric)
    case metric
    when 'Metres' then length
    when 'Feet' then length*0.3048
    when 'Yards' then length*0.9144
    else length
    end
  end

  def area_translate
    self.floor_area = area_standarize floor_area_metric, floor_area
    self.land_area = area_standarize land_area_metric, land_area
    self.porch_terrace_area = area_standarize porch_terrace_area_metric, porch_terrace_area
    self.garage_area = area_standarize garage_area_metric, garage_area
  end

end
