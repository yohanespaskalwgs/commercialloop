# == Schema Info
# Schema version: 20090614051807
#
# Table name: campaigns
#
#  id          :integer(4)      not null, primary key
#  property_id :integer(4)
#  current     :boolean(1)
#  end_date    :date
#  start_date  :date
#  created_at  :datetime
#  updated_at  :datetime

class Campaign < ActiveRecord::Base
  belongs_to :property
end
