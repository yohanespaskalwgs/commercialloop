class MatchContact < ActiveRecord::Base
  belongs_to :property
  belongs_to :agent_contact
  attr_accessible :property_id, :agent_contact_id
  validates_uniqueness_of :agent_contact_id, :scope => [:property_id]
end
