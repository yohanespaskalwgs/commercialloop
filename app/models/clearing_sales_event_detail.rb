class ClearingSalesEventDetail < ActiveRecord::Base
  attr_accessor :property_type, :skip_validation
  attr_accessor :country

  belongs_to :clearing_sales_event
  belongs_to :property, :foreign_key => "clearing_sales_event_id"


  include DetailHelper


  def get_property_id
    return self.clearing_sales_event_id
  end

end
