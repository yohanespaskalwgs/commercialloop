class IndustryCategory < ActiveRecord::Base
  has_many :agent_contacts
  has_many :agent_companies
end
