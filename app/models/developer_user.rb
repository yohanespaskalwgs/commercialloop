# == Schema Info
# Schema version: 20090614051807
#
# Table name: users
#
#  id                        :integer(4)      not null, primary key
#  developer_id              :integer(4)
#  office_id                 :integer(4)
#  activation_code           :string(40)
#  code                      :string(255)
#  crypted_password          :string(40)
#  description               :text
#  email                     :string(100)
#  fax                       :string(255)
#  first_name                :string(50)      default("")
#  im_service                :string(255)
#  im_username               :string(255)
#  last_name                 :string(50)      default("")
#  login                     :string(40)
#  mobile                    :string(255)
#  phone                     :string(255)
#  position                  :integer(4)
#  remember_token            :string(40)
#  salt                      :string(40)
#  type                      :string(255)
#  activated_at              :datetime
#  created_at                :datetime
#  deleted_at                :datetime
#  remember_token_expires_at :datetime
#  updated_at                :datetime

class DeveloperUser < User
  belongs_to :developer
  attr_accessible :access_level, :assign_to_all_ticket

  validates_presence_of :first_name, :last_name


  has_many :agent_developer_accesses
  has_many :office_developer_accesses

  has_many :unread_message_recipients, :class_name => "MessageRecipient", :foreign_key => 'user_id',
    :conditions => { :unread => true }

  has_many :client_messages, :through => :message_recipients, :source => :message,
    :conditions => ["messages.type <> ?", 'AdminMessage']

  has_many :unread_client_messages, :through => :unread_message_recipients, :source => :message,
    :conditions => ["messages.type <> ?", 'AdminMessage']

  has_many :admin_messages, :through => :message_recipients, :source => :message,
    :conditions => ["messages.type = ?", 'AdminMessage']

  has_many :unread_admin_messages, :through => :unread_message_recipients, :source => :message,
    :conditions => ["messages.type = ?", 'AdminMessage']

  delegate :site_url, :to => :developer
  delegate :time_zone , :to => :developer

  delegate :country , :to => :developer
  delegate :state, :to => :developer


  include RoleLevelSystem

  def unread_messages_count
    unread_message_recipients.count
  end

  def unread_client_messages_count
    unread_client_messages.count
  end

  def unread_admin_messages_count
    unread_admin_messages.count
  end

end
