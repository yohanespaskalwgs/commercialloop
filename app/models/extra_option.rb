class ExtraOption < ActiveRecord::Base
  belongs_to :property
  belongs_to :extra_detail_name, :foreign_key => :extra_detail_name_id
  validates_presence_of :option_title
end
