class ContactPropertyTypeRelation < ActiveRecord::Base
  belongs_to :agent_contact
  belongs_to :property_type
end
