class GeneralSaleDetail < ActiveRecord::Base
  attr_accessor :property_type, :skip_validation
  attr_accessor :country

  belongs_to :general_sale
  belongs_to :property, :foreign_key => "general_sale_id"


  include DetailHelper


  def get_property_id
    return self.general_sale_id
  end

end
