# == Schema Info
# Schema version: 20090614051807
#
# Table name: opentimes
#
#  id          :integer(4)      not null, primary key
#  property_id :integer(4)
#  date        :date
#  end_time    :time
#  start_time  :time

class Opentime < ActiveRecord::Base
  belongs_to :property

  validates_presence_of :date
  validates_presence_of :start_time
  validates_presence_of :end_time
  validate :end_greater_than_start
  attr_accessor :number_of_weeks, :start_hour, :start_minute, :end_hour, :end_minute
  after_create :create_past_opentime

  def starttime
    self.start_time.to_s(:time)
  end

  def endtime
    self.end_time.to_s(:time)
  end

  def end_greater_than_start
    if !self.start_time.nil? and !self.end_time.nil?
      if self.start_time >= self.end_time
        errors.add("", "End time should be greater than start time")
        false
      else
        true
      end
    end
  end

  def create_past_opentime
    PastOpentime.create({:opentime_id => self.id, :property_id => self.property_id, :date => self.date, :start_time => self.start_time, :end_time => self.end_time, :total_attended => 0})
  end
end
