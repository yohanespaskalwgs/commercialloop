# == Schema Info
# Schema version: 20090614051807
#
# Table name: users
#
#  id                        :integer(4)      not null, primary key
#  developer_id              :integer(4)
#  office_id                 :integer(4)
#  activation_code           :string(40)
#  code                      :string(255)
#  crypted_password          :string(40)
#  description               :text
#  email                     :string(100)
#  fax                       :string(255)
#  first_name                :string(50)      default("")
#  im_service                :string(255)
#  im_username               :string(255)
#  last_name                 :string(50)      default("")
#  login                     :string(40)
#  mobile                    :string(255)
#  phone                     :string(255)
#  position                  :integer(4)
#  remember_token            :string(40)
#  salt                      :string(40)
#  type                      :string(255)
#  activated_at              :datetime
#  created_at                :datetime
#  deleted_at                :datetime
#  remember_token_expires_at :datetime
#  updated_at                :datetime

require 'digest/sha1'
class AgentUser < User
  has_one :vcard, :as => :attachable
  has_one :mp3, :as => :attachable
  has_many :industry_sub_categories
  has_many :landscape_image, :as => :attachable, :order => "position asc"
  has_many :portrait_image, :as => :attachable, :order => "position asc"
  has_many :testimonials
  has_many :properties
  has_many :agent_contacts
  has_many :agent_companies
  has_many :contact_assigned_relations
  has_many :contact_accessible_relations
  has_many :company_assigned_relations
  has_many :company_accessible_relations
  has_many :category_contacts
  has_many :category_companies
  has_many :company_contacts
  has_many :group_contacts
  has_many :group_companies
  has_many :heard_froms
  has_many :note_types
  has_many :user_groups
  has_many :tasks, :dependent => :destroy

  belongs_to :office
  belongs_to :developer
  belongs_to :conjunctional

  delegate :agent, :to => :office
  delegate :site_url, :to => :developer
  delegate :time_zone, :to => :developer

  has_many :unread_message_recipients, :class_name => "MessageRecipient", :foreign_key => 'user_id',
    :conditions => { :unread => true }

  has_many :unread_messages, :through => :unread_message_recipients, :source => :message

  def unread_messages_count
    unread_messages.count
  end

  validates_uniqueness_of :code, :scope => :office_id
  validates_presence_of :office_id, :first_name, :last_name
  validate :validate_conjunctional, :unless => proc{|x|x.conjunctional_user.blank?}
  validate :code_length

  before_validation_on_create :generate_sequential_code
  before_save :set_login
  after_update :send_deteled_notif_later #, :send_agent_user_notification
  after_save :trigger_superapi, :use_spawn
  before_validation_on_create :assign_developer

  GROUP = [['Executive', 'Executive'], ['Sales Agent', 'Sales Agent'], ['Property Manager', 'Property Manager'], ['Admin', 'Admin'], ['Industrial', 'Industrial'], ['Office Leasing', 'Office Leasing'], ['Office Sales', 'Office Sales'], ['Retail', 'Retail'], ['Project Marketing', 'Project Marketing'], ['PMBC', 'PMBC'], ['Other', 'Other']]

  include RoleLevelSystem

  def send_deteled_notif_later
    if !self.deleted_at.blank?
      #self.send_later(:send_agent_user_notification, self)
      Delayed::Job.enqueue(AgentUserDelete.new(GLOBAL_ATTR["Host-#{self.office_id}"], self.id), 3)
    end
  end

  def send_agent_user_notification
    Log.create(:message => "The Team member does not have url :: #{self.inspect}") and return if GLOBAL_ATTR["Host-#{self.office_id}"].blank?
    domains = self.office.domains
    return if domains.blank?
    domains.each do |domain|
      next if domain.ipn.blank?
      begin
        team_member = self.attributes
        encoded_team_member = {}
        team_member.each do |tm|
          if tm.last.is_a?(String)
            encoded_team_member.merge!(tm.first => Base64.encode64(tm.last))
          else
            encoded_team_member.merge!(tm.first => tm.last)
          end
        end

        encoded_testimonials = {}
        unless self.testimonials.blank?
          self.testimonials.map{|x| x.attributes}.each_with_index do |testi, i|
            testi.keys.each do |t|
              if testi[t].is_a?(String)
                testi[t] = Base64.encode64(testi[t])
              end
            end
            encoded_testimonials.merge!(i.to_s => testi)
          end
        end

        audio = self.mp3.blank? ? nil : self.mp3.attributes
        img_1 = img_2 = img_portrait_1 = img_portrait_2 = nil
        landscapes = self.landscape_image
        unless landscapes.blank?
          landscapes.each do |img|
            if img.position == 1
              img_1 = img
            else
              img_2 = img
            end
          end
        end

        portraits = self.portrait_image
        unless portraits.blank?
          portraits.each do |img_portrait|
            if img_portrait.position == 1
              img_portrait_1 = img_portrait
            else
              img_portrait_2 = img_portrait
            end
          end
        end
        encoded_team_member.merge!({ "landscape_images"=>{"1"=>( img_1.blank? ? "" : img_1.real_filename ),"2"=>( img_2.blank? ? "" : img_2.real_filename )},"portrait_images"=>{"1"=>( img_portrait_1.blank? ? "" : img_portrait_1.real_filename ),"2"=>( img_portrait_2.blank? ? "" : img_portrait_2.real_filename )}}).merge!("testimonials" => encoded_testimonials).merge!("vcard" => (self.vcard.blank? ? "" : BASE_URL + self.vcard.public_filename)).merge!("audio" => audio)
#        encoded_team_member.merge!({ "landscape_images"=>{"1"=>( self.landscape_image.first.blank? ? "" : self.landscape_image.first.real_filename ),"2"=>( self.landscape_image.second.blank? ? "" : self.landscape_image.second.real_filename )},"portrait_images"=>{"1"=>( self.portrait_image.first.blank? ? "" : self.portrait_image.first.real_filename ),"2"=>( self.portrait_image.second.blank? ? "" : self.portrait_image.second.real_filename )}}).merge!("testimonials" => encoded_testimonials).merge!("vcard" => (self.vcard.blank? ? "" : BASE_URL + self.vcard.real_filename)).merge!("audio" => audio)

        token = Digest::SHA1.hexdigest("#{Time.now}")[8..50]
        GLOBAL_ATTR["#{self.office_id}-#{self.developer_id}-#{self.id}"] = token

        call_back_url = GLOBAL_ATTR["Host-#{self.office_id}"].blank? ? "commercialloopcrm.com.au" : GLOBAL_ATTR["Host-#{self.office_id}"]
        system("curl -i -X POST -d 'contacts=#{encoded_team_member.to_json}&callback_url=http://#{call_back_url}/agents/#{self.agent.id}/offices/#{self.office_id}/agent_users/#{self.id}/team_notification?token=#{token}&type=team_member&status=delete' #{domain.ipn}")

      rescue Exception => ex
        Log.create(:message => "team member deleted ---> #{self.inspect} :: ---> errors when deleting team member: #{ex.inspect}")
      end
    end
  end

  def use_spawn
    #return unless self.deleted_at.blank?

    if self.count_api_sent.to_i > 0
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE users SET count_api_sent = 0 WHERE id = #{self.id}"
      sql.commit_db_transaction
    end

    if self.office.http_api_enable == true
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE users SET processed = 0, cron_checked = 0 WHERE id = #{self.id}"
      sql.commit_db_transaction
    end

    spawn(:kill => true) do
      self.check_before_send
    end

#    spawn(:kill => true) do
#     sleep 10
#     first_shot = second_shot = third_shot = true
#     first_shot = self.check_before_send
#
#     if first_shot == false
#       sleep 60
#       second_shot = self.check_before_send
#     end
#
#     if second_shot == false
#       sleep 60
#       third_shot = self.check_before_send
#     end
#    end
  end

  def check_before_send
    agent_user = AgentUser.find_by_sql("SELECT * FROM users WHERE (`processed` = 0 OR `processed` IS NULL) AND (`count_api_sent` < 3 OR `count_api_sent` IS NULL) AND type LIKE 'AgentUser' AND id = #{self.id.to_i}").first
    unless agent_user.blank?
      unless agent_user.office.agent.blank?
        GLOBAL_ATTR["Host-#{agent_user.office_id}"] = GLOBAL_ATTR["Host-#{self.office_id}"].blank? ? "#{agent_user.office.agent.developer.entry}.commercialloopcrm.com.au" : GLOBAL_ATTR["Host-#{self.office_id}"]
      else
        agent = Agent.find_by_sql("SELECT * FROM agents WHERE id = #{agent_user.office.agent_id.to_i}").first
        new_host = "#{agent.developer.entry}.commercialloopcrm.com.au"
        GLOBAL_ATTR["Host-#{agent_user.office_id}"] = GLOBAL_ATTR["Host-#{self.office_id}"].blank? ? new_host : GLOBAL_ATTR["Host-#{self.office_id}"]
      end
      tmp = agent_user.count_api_sent.to_i + 1
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE users SET count_api_sent = #{tmp} WHERE id = #{agent_user.id}"
      sql.commit_db_transaction
      agent_user.send_agent_user_api
      return false
    else
      return true
    end
  end

  def send_agent_user_api
    domains = self.office.domains
    is_http_api = self.office.http_api_enable
    is_office_active = self.office.status == 'active' ? true : false
    return if domains.blank? || (is_http_api == false || is_http_api.blank?) || is_office_active == false
    domains.each do |domain|
      next if domain.ipn.blank?
      begin
        team_member = self.attributes
        encoded_team_member = {}
        team_member.each do |tm|
          if tm.first == "conjunctional_office_name" || tm.first == "conjunction_rea_agent_id"
            data_tm = ""
            if !self.conjunctional_id.blank?
              cn = Conjunctional.find_by_id(self.conjunctional_id)
              unless cn.blank?
                if tm.first == "conjunctional_office_name"
                  data_tm = cn.conjunctional_office_name
                else
                  data_tm = cn.conjunctional_rea_agent_id
                end
              end
            else
              data_tm = tm.last
            end
            encoded_team_member.merge!(tm.first => Base64.encode64(data_tm.to_s))
          else
            if tm.last.is_a?(String)
              encoded_team_member.merge!(tm.first => Base64.encode64(tm.last))
            else
              encoded_team_member.merge!(tm.first => tm.last)
            end
          end
        end

        encoded_testimonials = {}
        unless self.testimonials.blank?
          self.testimonials.map{|x| x.attributes}.each_with_index do |testi, i|
            testi.keys.each do |t|
              if testi[t].is_a?(String)
                testi[t] = Base64.encode64(testi[t])
              end
            end
            testi["testi_images"] = self.testimonials[i].testi_image.first.public_filename rescue ''
#            testi["testi_images"] = self.testimonials[i].testi_image.first.real_filename rescue ''
#            testi["testi_images_s3"] = self.testimonials[i].testi_image.first.public_filename rescue ''
            encoded_testimonials.merge!(i.to_s => testi)
          end
        end

        audio = self.mp3.blank? ? nil : self.mp3.attributes
        img_1 = img_2 = img_portrait_1 = img_portrait_2 = nil

        landscapes = self.landscape_image
        unless landscapes.blank?
          landscapes.each do |img|
            if img.position == 1
              img_1 = img
            else
              img_2 = img
            end
          end
        end

        portraits = self.portrait_image
        unless portraits.blank?
          portraits.each do |img_portrait|
            if img_portrait.position == 1
              img_portrait_1 = img_portrait
            else
              img_portrait_2 = img_portrait
            end
          end
        end

        encoded_groups = {}
        unless self.user_groups.blank?
          self.user_groups.map{|x| x.attributes}.each_with_index do |grp, i|
            grp.keys.each{|t| grp[t] = Base64.encode64(grp[t]) if grp[t].is_a?(String)}
            encoded_groups.merge!(i.to_s => grp)
          end
        end

        encoded_team_member.merge!({
            "landscape_images"=>{"1"=>( img_1.blank? ? "" : img_1.real_filename ),"2"=>( img_2.blank? ? "" : img_2.real_filename )},
            "portrait_images"=>{"1"=>( img_portrait_1.blank? ? "" : img_portrait_1.real_filename ),"2"=>( img_portrait_2.blank? ? "" : img_portrait_2.real_filename )}}).merge!({
            "landscape_images_s3"=>{"1"=>( img_1.blank? ? "" : img_1.public_filename ),"2"=>( img_2.blank? ? "" : img_2.public_filename )},
            "portrait_images_s3"=>{"1"=>( img_portrait_1.blank? ? "" : img_portrait_1.public_filename ),"2"=>( img_portrait_2.blank? ? "" : img_portrait_2.public_filename )}}).merge!(
            "testimonials" => encoded_testimonials).merge!(
            "vcard" => (self.vcard.blank? ? "" : BASE_URL + self.vcard.public_filename)).merge!(
            "audio" => audio).merge!(
            "user_groups" => encoded_groups)
#       encoded_team_member.merge!({ "landscape_images"=>{"1"=>( self.landscape_image.find_by_position(1).blank? ? "" : self.landscape_image.find_by_position(1).real_filename ),"2"=>( self.landscape_image.find_by_position(2).blank? ? "" : self.landscape_image.find_by_position(2).real_filename )},"portrait_images"=>{"1"=>( self.portrait_image.find_by_position(1).blank? ? "" : self.portrait_image.find_by_position(1).real_filename ),"2"=>( self.portrait_image.find_by_position(2).blank? ? "" : self.portrait_image.find_by_position(2).real_filename )}}).merge!("testimonials" => encoded_testimonials).merge!("vcard" => (self.vcard.blank? ? "" : BASE_URL + self.vcard.real_filename)).merge!("audio" => audio)

        token = Digest::SHA1.hexdigest("#{Time.now}")[8..50]
        GLOBAL_ATTR["#{self.office_id}-#{self.developer_id}-#{self.id}"] = token

        #system("curl -i -X POST -d 'contacts=#{encoded_team_member.to_json}&callback_url=http://#{GLOBAL_ATTR["Host-#{self.office_id}"]}/agents/#{self.agent.id}/offices/#{self.office_id}/agent_users/#{self.id}/team_notification?token=#{token}&type=team_member' #{domain.ipn}")
        ActiveRecord::Base.connection.execute("INSERT INTO `api_requests`(`url`,`post_data`,`created`) VALUES ('#{domain.ipn}', 'contacts=#{encoded_team_member.to_json}&callback_url=http://#{GLOBAL_ATTR["Host-#{self.office_id}"]}/agents/#{self.agent.id}/offices/#{self.office_id}/agent_users/#{self.id}/team_notification?token=#{token}&type=team_member', '#{Time.now.to_formatted_s(:db_format)}')")
        #update api_is_sent field
        self.api_is_sent = true
      rescue Exception => ex
        Log.create(:message => "team member ---> #{self.inspect} :: ---> errors : #{ex.inspect}")
        self.api_is_sent = false
      end
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE users SET api_is_sent = #{self.api_is_sent} WHERE id = #{self.id}"
      sql.commit_db_transaction
    end
  end

  def pretty_login
    self[:login]
  end

  def login
    self[:login]
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def full_code
    "#{office.full_code}-#{code}"
  end

  def title
    full_access? ? 'Director' : 'Member'
  end


  def generate_username_and_password
    generate_username
    generate_password
  end

  def generate_username
    self.login = "#{self.first_name}#{self.last_name}"
    v = 1
    while self.developer.agent_users.find_by_login(self.login)
      self.login += v.to_s
      v += 1
    end
  end

  def generate_password
    self.password = self.password_confirmation = Digest::SHA1.hexdigest("login pass#{Time.now.to_s + rand.to_s}word")[0..7]
  end

  def generate_sequential_code
    return if (office_id.blank? || !code.blank?)
    v = office.agent_users.count
    self.code = ("%04d" % v)
    while AgentUser.find_by_office_id_and_code(office_id, code)
      v += 1
      self.code = ("%04d" % v)
    end
  end

  def trigger_superapi
    ActiveRecord::Base.connection.execute("UPDATE superapi_offices SET next_listcontacts = UNIX_TIMESTAMP() + 15 WHERE office_id = #{self.office_id}")
  end

  private

  def set_login
    self.login = pretty_login
  end

  def assign_developer
    if self.office && !self.developer_id
      self.developer_id = self.office.agent.developer_id
    end
  end

  def code_length
    unless code.blank?
      if code.size == 4 || code.size == 3
        true
      else
        errors.add("code", "agent user must have a 4 digits code")
        false
      end
    else
      errors.add("code", "agent user must have a 4 digits code")
      false
    end
  end

  def validate_conjunctional
    if conjunctional_id.blank?
      errors.add(:conjunctional_office, " must be selected")
      false
    else
      true
    end
  end
end
