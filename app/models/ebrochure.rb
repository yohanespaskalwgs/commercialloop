class Ebrochure < ActiveRecord::Base
  has_many :ecampaign_tracks
  has_many :ecampaign_clicks
  belongs_to :property

  def self.show_ebrochure(conditions)
    sql = [" SELECT `e`.id,`e`.property_id,`e`.title,`e`.created_at,`e`.sent_at, (SELECT count(r.id) FROM `ecampaign_tracks` r WHERE `r`.ebrochure_id = e.id) as recipient "]
    sql[0] += ", (SELECT count(op.id) FROM `ecampaign_tracks` op WHERE `op`.ebrochure_id = e.id And `op`.email_opened = 1) as opened "
    sql[0] += ", (SELECT count(bon.id) FROM `ecampaign_tracks` bon WHERE `bon`.ebrochure_id = e.id And `bon`.email_bounced = 1) as bounced "
    sql[0] += ", (SELECT sum(cl.clicked) FROM `ecampaign_clicks` cl WHERE `cl`.ebrochure_id = e.id) as clicked "
    sql[0] += ", (SELECT sum(fw.email_forwarded) FROM `ecampaign_tracks` fw WHERE `fw`.ebrochure_id = e.id) as forwarded "
    sql[0] += ", (SELECT count(us.id) FROM `ecampaign_unsubcribes` us WHERE `us`.ebrochure_id = e.id) as unsubcribed "
    sql[0] += " FROM `ebrochures` e "
    sql_part1 = [" WHERE ( #{conditions.to_s} ) ORDER BY `e`.id DESC"]
    sql[0] += sql_part1.to_s
    return sql
  end

  def is_sent_to_all?
    require 'json'
    eb_json = JSON.parse(self.stored_data)
    total_sent = EcampaignTrack.find_all_by_ebrochure_id(self.id)
    total_sent = total_sent.present? ? total_sent.count : 0
    num1 = eb_json["ebrochure_contact_checkbox"].present? ? eb_json["ebrochure_contact_checkbox"].count : 0
    num2 = eb_json["ebrochure_contact_alert_checkbox"].present? ? eb_json["ebrochure_contact_alert_checkbox"].count : 0
    num3 = eb_json["ebrochure_contact_name_checkbox"].present? ? eb_json["ebrochure_contact_name_checkbox"].count : 0
    total_recipients = num1 + num2 + num3
    return total_sent == total_recipients
  end
end
