class SmsMessage < ActiveRecord::Base
  validates_presence_of :receiver_number

  belongs_to :sender, :class_name => "User"
  belongs_to :office


  # Constant
  API_ID = '3454738'
  API_USERNAME = 'agentpoint'
  API_PASSWORD = 'HGQYVBfHccYKee'
  API_URL = 'http://api.clickatell.com/http/sendmsg'

  PURCHASE_URL = 'https://accounts.agentpoint.com.au/purchase-sms-credit/'


  # Class Method
  def self.build_sorting(sort, dir)
    case sort
    when 'id'
      return 'sms_messages.id ' + dir
    when 'receiver_number'
      return 'sms_messages.receiver_number ' + dir
    when 'message'
      return 'sms_messages.message ' + dir
    when 'status'
      return 'sms_messages.status ' + dir
    when 'created_at'
      return 'sms_messages.created_at ' + dir
    when 'message_id'
      return 'sms_messages.message_id ' + dir
    when 'received_at'
      return 'sms_messages.received_at ' + dir
    end
  end

  def self.purchase_credit_url(office_id, redirect_url)
    uri = URI(PURCHASE_URL)
    uri = append_uri(uri, "office_id=#{office_id}")
    uri = append_uri(uri, "redirect=#{redirect_url}")
  end


  # Instance Method
  def send_sms
    begin
      resp = Net::HTTP.post_form(URI.parse(API_URL), sms_params)

      if resp.code.to_i == 200 && parse_sms_response(resp.body)
        save
        return true
      else
        return false
      end
    rescue
      return false
    end
  end

  def update_status(code, timestamp)
    case code
    when '001'
      self.status = "Message unknown"
    when '002'
      self.status = "Message queued"
    when '003'
      self.status = "Delivered to gateway"
    when '004'
      self.status = "Received"
      self.received_at = DateTime.strptime(timestamp, '%s')
    when '005'
      self.status = "Error with message"
    when '006'
      self.status = "Canceled"
    when '007'
      self.status = "Error delivering message"
    when '008'
      self.status = "OK"
    when '009'
      self.status = "Routing Error"
    when '010'
      self.status = "Message expired"
    when '011'
      self.status = "Message queued"
    when '012'
      self.status = "Out of credit"
    when '014'
      self.status = "Maximum MT limit exceeded"
    end

    save
  end


  # Private Method
  private

  def number_of_messages
    self.message.length / 160 + 1
  end

  def sms_params
    {
      :user => API_USERNAME,
      :password => API_PASSWORD,
      :api_id => API_ID,
      :to => self.receiver_number,
      :text => self.message,
      :concat => 3,
      :callback => 3,
      :from => sms_from_param
    }
  end

  def sms_from_param
    return self.office.sms_from.blank? ? 'Agentpoint' : self.office.sms_from
  end

  def parse_sms_response(response)
    arr_resp = response.split(' ')

    if arr_resp[0] == "ID:"
      self.message_id = arr_resp[1]
      return true
    else
      return false
    end
  end

end
