# == Schema Info
# Schema version: 20090614051807
#
# Table name: business_sale_details
#
#  id                            :integer(4)      not null, primary key
#  business_sale_id              :integer(4)
#  additional_notes              :text
#  annual_ebit                   :decimal(16, 3)
#  annual_gross_profit           :decimal(16, 3)
#  annual_turnover               :decimal(16, 3)
#  approximate_stock_value       :decimal(16, 3)
#  auction_datetime              :datetime
#  auction_place                 :string(255)
#  business_name                 :string(255)
#  category                      :string(255)
#  current_outgoings             :decimal(16, 3)
#  current_outgoings_include_tax :boolean(1)
#  current_rent                  :decimal(16, 3)
#  current_rent_include_tax      :boolean(1)
#  energy_efficiency_rating      :string(255)
#  energy_star_rating            :string(255)
#  floor_area                    :decimal(16, 3)
#  forthcoming_auction           :boolean(1)
#  franchise                     :string(255)
#  franchise_levies              :string(255)
#  franchise_royalties           :string(255)
#  land_area                     :decimal(16, 3)
#  latitude                      :decimal(12, 7)
#  lease_commencement            :date
#  lease_end                     :date
#  lease_option                  :string(255)
#  lease_plus_another            :string(255)
#  longitude                     :decimal(12, 7)
#  outlets                       :string(255)
#  parking_comments              :string(255)
#  parking_spaces                :integer(4)      default(0)
#  patron_capacity               :string(255)
#  premise                       :string(255)
#  price_include_stock           :boolean(1)
#  price_include_tax             :boolean(1)
#  staff_casual                  :integer(4)
#  staff_full_time               :integer(4)
#  staff_part_time               :integer(4)
#  virtual_tour                  :string(255)
#  year_built                    :integer(4)

class BusinessSaleDetail < ActiveRecord::Base
  belongs_to :business_sale

  #validates_presence_of :category
  validates_inclusion_of :price_include_tax, :in => [true, false]
  validates_inclusion_of :price_include_stock, :in => [true, false]
  validates_inclusion_of :current_rent_include_tax, :in => [true, false], :unless => Proc.new{ |detail| detail.current_rent.nil? }
  validates_inclusion_of :current_outgoings_include_tax, :in => [true, false], :unless => Proc.new{ |detail| detail.current_outgoings.nil? }
  validates_numericality_of :annual_turnover, :annual_gross_profit, :annual_ebit, :approximate_stock_value, :current_outgoings, :current_rent, :greater_than => 0, :allow_nil => true
  validates_numericality_of :staff_full_time, :staff_part_time, :staff_casual, :greater_than_or_equal_to => 0, :only_integer => true, :allow_nil => true
  validates_numericality_of :year_built, :only_integer => true, :greater_than => 999, :less_than_or_equal_to => Time.now.year, :allow_nil => true

  include DetailHelper

  def get_property_id
    return self.business_sale_id
  end

end
