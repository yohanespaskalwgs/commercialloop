# == Schema Info
# Schema version: 20090614051807
#
# Table name: agent_developer_accesses
#
#  id                :integer(4)      not null, primary key
#  agent_id          :integer(4)
#  developer_user_id :integer(4)

class AgentDeveloperAccess < ActiveRecord::Base
  belongs_to :agent
  belongs_to :developer_user
end
