class TenancyRecord < ActiveRecord::Base
  belongs_to :property
  belongs_to :agent_contact
  has_one :task, :dependent => :destroy
  validates_presence_of :transaction_sales_person, :on => :create, :message => "can't be blank"
  validates_presence_of :lease_expiry_sales_person, :on => :create, :message => "can't be blank"
  validates_presence_of :total_rental_pa

  attr_accessor :skip_validation

  named_scope :valid, :conditions => ["tenancy_records.is_deleted = ?", false]

  def self.build_sorting(sort, dir)
  	case sort
  	  when 'id'
  	  	return 'tenancy_records.id '+dir
  	  when 'start_date'
  	  	return 'tenancy_records.start_date '+dir
  	  when 'end_date'
  	  	return 'tenancy_records.end_date '+dir
  	  when 'rent_pa'
  	  	return 'tenancy_records.rent_pa '+dir
  	end
  end

  def create_task(property, office, user, tenancy)
    task_type = TaskType.find(:first, :conditions => ["name = ?", "Reminder Alert for Lease Property"])
    self.build_task(
      :agent_contact_id => self.agent_contact_id,
      :agent_user_id => user.id,
      :task_type_id => task_type.id,
      :address => property.full_address,
      :description => "Reminder Alert for Lease Property ##{property.id}",
      :task_date => Date.strptime(tenancy[:reminder_alert], '%Y-%m-%d').strftime('%d/%m/%Y'),
      :email_reminder => 1,
      :property_id => property.id,
      :completed => 0,
      :office_id => office.id
      )
  end

  def update_task
    unless self.task.blank?
      self.task.update_attributes({:agent_contact_id => self.agent_contact_id, :task_date => Date.strptime(self.reminder_alert, '%Y-%m-%d').strftime('%d/%m/%Y-')})
    end
  end

  def is_valid?
    rv = false
    if self.is_deleted == false
      rv = true
    end
    rv
  end

end
