class EcampaignTrack < ActiveRecord::Base
  belongs_to :ecampaign
  belongs_to :ebrochure
  belongs_to :agent_contact

  def count_received
    ecampaign_tracks = EcampaignTrack.find(:all, :conditions=> "`agent_contact_id`=#{self.agent_contact_id} And `ecampaign_id`=#{self.ecampaign_id}")
    return ecampaign_tracks.size
  end
end
