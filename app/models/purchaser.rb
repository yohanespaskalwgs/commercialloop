class Purchaser < ActiveRecord::Base
  belongs_to :property
  belongs_to :agent_contact
  attr_accessor :duplicate, :skip_validation
  before_destroy :delete_relate_records
  after_create :add_viewer
  validates_presence_of :date, :amount, :if => proc{|x|["2","3"].include? x.property.status}
  validates_numericality_of :amount, :greater_than => 0, :if => proc{|x|["2","3"].include? x.property.status}

  def delete_relate_records
    ContactNote.destroy_all({:id => self.contact_note_id})
  end

  def add_viewer
    PropertyViewer.create({:property_id => self.property_id, :property_viewer_type_id => 9 ,:date => Time.now})
  end

  def populate_contact_detail
    unless self.agent_contact.blank?
     self.first_name = self.agent_contact.first_name
     self.last_name = self.agent_contact.last_name
     self.email = self.agent_contact.email
     self.contact_number = self.agent_contact.contact_number
     self.home_number = self.agent_contact.home_number
     self.work_number = self.agent_contact.work_number
     self.mobile_number = self.agent_contact.mobile_number
    end
  end

  def update_contact_detail
    unless self.agent_contact.blank?
     self.agent_contact.first_name = self.first_name
     self.agent_contact.last_name = self.last_name
     self.agent_contact.email = self.email
     self.agent_contact.contact_number = self.contact_number
     self.agent_contact.home_number = self.home_number
     self.agent_contact.work_number = self.work_number
     self.agent_contact.mobile_number = self.mobile_number
     self.agent_contact.save
    end
  end
end
