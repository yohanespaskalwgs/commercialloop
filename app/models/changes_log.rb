# property logs
# t.integer :property_id  ---> Property id

# Child example : Opentime
#t.integer :updated_child_id ---> Opentime id
#t.string :updated_child --> Model name (opentime)

#t.string :operation --> Create(1), edit(2), delete(3)

#t.text :updated_attributes --> Attribute in opentime: date, time, etc

#t.integer :agent_user_id --> User that perform editing

class ChangesLog < ActiveRecord::Base
  belongs_to :agent_user
  belongs_to :property

  def self.create_property_log(office, property, user, feat_change, mode)
    user_id = user.present? ? user.id : 0
    prop_changes = property.changes.reject{|k,v| v[0].blank? && v[1].blank?}
    property_changes = {"property" => prop_changes }

    det_changes = property.detail.changes.reject{|k,v| v[0].blank? && v[1].blank?}
    detail_changes = {"detail" => det_changes}

    feat_change = feat_change.present? ? feat_change : {}
    all_changes = property_changes.merge(detail_changes).merge(feat_change)
    if feat_change.present? || prop_changes.present? || det_changes.present?
      self.create(:property_id => property.id, :operation => mode, 
                :updated_attributes => all_changes.to_json, :agent_user_id => user_id, :office_id => office.id)
    end
  end

  def self.create_media_creation_log(office, media, property, curr_user, mode)
    attributes = {"filename" => media.filename, "public_filename" => media.public_filename}
    self.create_log(property, media, attributes, curr_user, office, mode)
  end

  def self.create_media_desc_log(office, media, property, curr_user, mode)
    media_changes = {"media" => media.changes}
    user_id = curr_user.present? ? curr_user.id : 0
    self.create(:property_id => property.id, :updated_child_id => media.id, :updated_child => media.class.to_s,
                :operation => mode, :updated_attributes => media_changes.to_json, :agent_user_id => user_id, 
                :office_id => office.id)
  end

  def self.create_media_destroy_log(office, media, property, curr_user, mode)
    user_id = curr_user.present? ? curr_user.id : 0
    attributes = media.attributes.delete_if{|k,v| v.blank?}
    public_fname = {"public_filename" => media.public_filename}
    complete_attr = attributes.merge(public_fname)
    self.create(:property_id => property.id, :updated_child_id => media.id, :updated_child => media.class.to_s,
                :operation => mode, :updated_attributes => complete_attr.to_json, :agent_user_id => user_id, 
                :office_id => office.id)
  end

  def self.create_opentime_update_log(office, property, opentime, user,changes, mode)
    self.create_log(property, opentime, changes, user, office, mode)
  end

  def self.create_opentime_destroy_log(office, property, opentime, pastopentime, contact_pastopentime_rels, current_user, mode)
    attr = []
    contact_pastopentime_rels.each do |rel|
     attr << rel.attributes
    end
    contact_pastopentime = {"contact_pastopentime" => attr}
    destroyed_attributes = {"past_opentime" => pastopentime.attributes}
    destroyed_attributes = destroyed_attributes.merge(contact_pastopentime)
    self.create_log(property, opentime, destroyed_attributes, current_user, office, mode)
  end

  def self.create_log(property, child, updated_attributes, user, office, mode)
    user_id = user.present? ? user.id : 0
    child_id = child.present? ? child.id : ""
    child_class = child.present? ? child.class.to_s : ""
    self.create(
            :property_id => property.id, :updated_child_id => child_id, :updated_child => child_class,
            :operation => mode, :updated_attributes => updated_attributes.to_json, :agent_user_id => user_id, 
            :office_id => office.id)
  end

  def self.display_pastopentime_log(updated_attributes)
    str = ""
    agent_contact = AgentContact.find(updated_attributes["agent_contact_id"])
    past_opentime = PastOpentime.find(updated_attributes["past_opentime_id"])

    end_time = past_opentime.end_time.to_datetime.strftime("%H:%M:%S") 
    start_time = past_opentime.start_time.to_datetime.strftime("%H:%M:%S") 

    str += "  <td width='32%'>
                <span style='font-size:13px'><b></b></span> <br/>
              </td>
                <td width='32%'>
                  <span style='font-size:13px'><b>Add opetime attendee</b></span><br/>
                  Opentime: #{past_opentime.date.strftime("%d-%m-%Y")}, #{start_time} - #{end_time} <br/>
                  Name: #{agent_contact.full_name}<br/>
                  Comment: #{updated_attributes["comment"]}<br/>
                </td>
            </tr>" rescue ""

    return str
  end

  def self.display_opentimes_log(log, updated_attributes, log_row)
    str = ""
    if log.operation == 1
      end_time = updated_attributes["end_time"].to_datetime.strftime("%H:%M:%S") 
      start_time = updated_attributes["start_time"].to_datetime.strftime("%H:%M:%S")
      date = Date.parse(updated_attributes["date"])
      operation = "Add new opentime"
    elsif log.operation == 3
      end_time = updated_attributes["past_opentime"]["end_time"].to_datetime.strftime("%H:%M:%S") 
      start_time = updated_attributes["past_opentime"]["start_time"].to_datetime.strftime("%H:%M:%S")
      date = Date.parse(updated_attributes["past_opentime"]["date"])
      operation = "Deleted opentime"
    end
    str += log_row
    str += "  <td width='32%'>
                  <span style='font-size:13px'><b></b></span> <br/>
                </td>
                <td width='32%'>
                  <span style='font-size:13px'><b>#{operation}</b></span><br/>
                  Date: #{date.strftime("%d-%m-%Y")}<br/>
                  Start time: #{start_time}<br/>
                  End time: #{end_time}
                </td>
              </tr>" rescue ""

    return str
  end

  def self.display_media_log(log, updated_attributes, log_row)
    str = ""
    if ["Image","Brochure","Floorplan"].include?(log.updated_child)
      if log.operation == 1
        operation = "Add Property #{log.updated_child}"
        str += log_row
        str += "  <td width='32%'>
                    <span style='font-size:13px'><b></b></span> <br/>
                  </td>
                  <td width='32%'>
                    <span style='font-size:13px'><b>#{operation}</b></span><br/>
                    Image filename: #{updated_attributes["filename"]}<br/>
                    Image link: <a href='#{updated_attributes["public_filename"]}'>#{updated_attributes["public_filename"]}</a><br/>
                  </td>
                </tr>" rescue ""
      elsif log.operation == 3
        operation = "Deleted Property #{log.updated_child}"
        str += log_row
        str += "  <td width='32%'>
                    <span style='font-size:13px'><b>#{operation}</b></span> <br/>
                    Image filename: #{updated_attributes["filename"]}<br/>
                    Image link: <a href='#{updated_attributes["public_filename"]}'>#{updated_attributes["public_filename"]}</a><br/>
                  </td>
                  <td width='32%'>
                    <span style='font-size:13px'><b></b></span><br/>
                  </td>
                </tr>" rescue ""
      end
    end
    return str
  end

  def self.display_vendor_detail_log(log, updated_attributes, log_row)
    str = ""
    if log.updated_child == "AgentUser"
      str += log_row
      str += "  <td width='32%'>
                  <span style='font-size:13px'><b>Property vendor detail</b></span> <br/>"
      updated_attributes["agent_user"].map{|k,v|
        str +=    "Agent #{k}: #{v[0]} <br/>"}
      str += "  </td>
                <td width='32%'>
                  <span style='font-size:13px'><b>Property vendor detail</b></span><br/>"
      updated_attributes["agent_user"].map{|k,v|
        str +=    "Agent #{k}: #{v[1]} <br/>"}
      str +=   "</td>
              </tr>" rescue ""
    end
    return str
  end

  def self.display_add_purchaser_log(log, updated_attributes, log_row)
    str = ""
    if log.updated_child == "Purchaser"
      str += log_row
      str += "  <td width='32%'>
                  <span style='font-size:13px'><b></b></span> <br/>"
      str += "  </td>
                <td width='32%'>
                  <span style='font-size:13px'><b>Purchaser data:</b></span><br/>
                  First name: #{updated_attributes["purchaser_contact"]["first_name"]}<br/>
                  Last name: #{updated_attributes["purchaser_contact"]["last_name"]}<br/>
                  Email: #{updated_attributes["purchaser_contact"]["email"]}<br/>
                  Amount: #{updated_attributes["purchaser"]["amount"]}<br/>
                  Date: #{updated_attributes["purchaser"]["date"]} <br/>" 
      str +=   "</td>
              </tr>" rescue ""
    end
    return str
  end
end
