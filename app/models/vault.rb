class Vault < ActiveRecord::Base
  belongs_to :property
  has_many :vault_contacts
end
