class Ecampaign < ActiveRecord::Base
  has_many :ecampaign_tracks
  has_many :ecampaign_clicks
  has_many :ecampaign_unsubcribes
  belongs_to :office
  has_many :ecampaign_images, :as => :attachable, :order => "position asc"


  def self.show_ecampaign(conditions)
    sql = [" SELECT `e`.id,`e`.title,`e`.created_at,`e`.sent_at, (SELECT count(r.id) FROM `ecampaign_tracks` r WHERE `r`.ecampaign_id = e.id) as recipient "]
    sql[0] += ", (SELECT count(op.id) FROM `ecampaign_tracks` op WHERE `op`.ecampaign_id = e.id And `op`.email_opened = 1) as opened "
    sql[0] += ", (SELECT count(bon.id) FROM `ecampaign_tracks` bon WHERE `bon`.ecampaign_id = e.id And `bon`.email_bounced = 1) as bounced "
    sql[0] += ", (SELECT sum(cl.clicked) FROM `ecampaign_clicks` cl WHERE `cl`.ecampaign_id = e.id) as clicked "
    sql[0] += ", (SELECT sum(fw.email_forwarded) FROM `ecampaign_tracks` fw WHERE `fw`.ecampaign_id = e.id) as forwarded "
    sql[0] += ", (SELECT count(us.id) FROM `ecampaign_unsubcribes` us WHERE `us`.ecampaign_id = e.id) as unsubcribed "
    sql[0] += " FROM `ecampaigns` e "
    sql_part1 = [" WHERE ( #{conditions.to_s} ) ORDER BY `e`.id DESC"]
    sql[0] += sql_part1.to_s
    return sql
  end
end
