class Offer < ActiveRecord::Base
  belongs_to :agent_contact
  belongs_to :property
  attr_accessor :name, :agent_id, :office_id, :duplicate
  before_destroy :delete_relate_records
  after_create :add_viewer
  validates_presence_of :agent_contact_id, :date, :detail

  def delete_relate_records
    ContactNote.destroy_all({:id => self.contact_note_id})
  end

  def add_viewer
    PropertyViewer.create({:property_id => self.property_id, :property_viewer_type_id => 9 ,:date => Time.now})
  end
end
