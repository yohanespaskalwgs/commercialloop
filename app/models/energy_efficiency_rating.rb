# == Schema Info
# Schema version: 20090614051807
#
# Table name: energy_efficiency_ratings
#
#  id         :integer(4)      not null, primary key
#  country_id :integer(4)
#  name       :string(255)
#  order      :integer(4)

# DELETE ME: obsolete as EER is stored in countries
class EnergyEfficiencyRating < ActiveRecord::Base
  belongs_to :country

  validates_presence_of :country_id, :name
end
