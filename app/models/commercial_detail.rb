# == Schema Info
# Schema version: 20090614051807
#
# Table name: commercial_details
#
#  id                            :integer(4)      not null, primary key
#  commercial_id                 :integer(4)
#  additional_notes              :text
#  all_the_building              :boolean(1)
#  all_the_floor                 :boolean(1)
#  annual_outgoings              :decimal(16, 3)
#  annual_outgoings_include_tax  :boolean(1)
#  auction_date                  :date
#  auction_place                 :string(255)
#  auction_time                  :time
#  bond                          :decimal(16, 3)
#  building_name                 :string(255)
#  current_leased                :boolean(1)
#  current_outgoings             :decimal(16, 3)
#  current_outgoings_include_tax :boolean(1)
#  current_rent                  :decimal(16, 3)
#  current_rent_include_tax      :boolean(1)
#  date_available                :date
#  energy_efficiency_rating      :string(255)
#  energy_star_rating            :string(255)
#  floor_area                    :decimal(16, 3)
#  forthcoming_auction           :boolean(1)
#  land_area                     :decimal(16, 3)
#  latitude                      :decimal(12, 7)
#  lease_commencement            :string(255)
#  lease_end                     :string(255)
#  lease_option                  :string(255)
#  lease_plus_another            :string(255)
#  longitude                     :decimal(12, 7)
#  number_of_floors              :integer(4)
#  outgoings_paid_by_tenant      :string(255)
#  patron_capacity               :string(255)
#  price_include_tax             :boolean(1)
#  rent_review                   :string(255)
#  return_percent                :decimal(4, 2)
#  virtual_tour                  :string(255)
#  year_built                    :integer(4)
#  zoning                        :string(255)

class CommercialDetail < ActiveRecord::Base
  attr_accessor :deal_type, :property_type
  belongs_to :commercial
  belongs_to :property, :foreign_key => "commercial_id"
  # CommercialLease validations
  # validates_numericality_of :bond, :greater_than => 0, :if => :lease_deal?
  # validates_numericality_of :annual_outgoings, :greater_than_or_equal_to => 0, :if => :lease_deal?

  validates_presence_of :current_rent, :outgoings_paid_by, :if => :lease_deal?

  # CommercialSale validations
  validates_presence_of :authority, :if => :sale_deal?
  validates_presence_of :auction_place, :auction_date, :auction_time, :if => :sale_deal_auction?


  # Common validations
  validates_numericality_of :return_percent, :greater_than => 0, :allow_nil => true
  validates_numericality_of :office_area, :warehouse_area, :retail_area, :other_area, :carport_spaces, :allow_nil => true, :greater_than => 0
  validates_numericality_of :number_of_floors, :greater_than => 0, :only_integer => true, :allow_nil => true
  validates_numericality_of :year_built, :only_integer => true, :greater_than => 999, :less_than_or_equal_to => Time.now.year, :allow_nil => true
  validates_numericality_of :sale_price_from, :greater_than => 5000, :allow_nil => true
  validates_numericality_of :land_area, :greater_than => 0, :if => proc{|x|(x.property_type == "Land" || x.property_type == "Development" || x.property_type == "Farmland") || (x.property_type2 == "Land" || x.property_type2 == "Development" || x.property_type2 == "Farmland") || (x.property_type3 == "Land" || x.property_type3 == "Development" || x.property_type3 == "Farmland")}
  validates_numericality_of :area_range, :greater_than => 0 , :allow_nil => true
  validates_numericality_of :current_rent, :greater_than => 0, :allow_nil => true
  validates_numericality_of :area_range_to, :greater_than => 0 , :allow_nil => true

  validates_presence_of :floor_area, :if => :validate_floor
  validates_presence_of :area_range, :area_range_to, :if => proc{|x|x.listings_this_address == false}
  validates_presence_of :land_area, :if => proc{|x|(x.property_type == "Land" || x.property_type == "Development" || x.property_type == "Farmland") || (x.property_type2 == "Land" || x.property_type2 == "Development" || x.property_type2 == "Farmland") || (x.property_type3 == "Land" || x.property_type3 == "Development" || x.property_type3 == "Farmland")}
  validates_presence_of :sale_price_from, :sale_price_to, :if => :sale_range?
  validates_presence_of :current_rent, :if => proc{|x|x.current_leased == 1} #:both_deal?
  validates_presence_of :method_of_sale
  validate :property_type_validation, :sale_tax, :lease_tax, :all_the_building_validation #, :tenancy_validation

  include DetailHelper

  def get_property_id
    return self.commercial_id
  end

  protected



  def lease_deal?
    deal_type == Property::BOTH || deal_type == Property::LEASE ||(@property.deal_type == Property::BOTH || @property.deal_type == Property::LEASE unless @property.blank?)
  end

  def sale_deal?
    deal_type == Property::BOTH || deal_type == Property::SALE ||(@property.deal_type == Property::BOTH || @property.deal_type == Property::SALE unless @property.blank?)
  end

  def both_deal?
    deal_type == Property::BOTH
  end

  def sale_deal_auction?
    sale_deal? and forthcoming_auction? and (["Auction", "Tender", "Expression of Interest"].include?(authority))
  end

  def sale_range?
    sale_deal? and (listings_this_address == false)
  end

  def property_type_validation
    if !property_type2.blank? && !property_type3.blank?
      if (property_type == property_type2) || (property_type == property_type3) || (property_type2 == property_type3)
        errors.add("", "Property Type, Property Type2, Property Type3 can't be same")
        false
      else
        true
      end
    elsif (property_type2.blank? && !property_type3.blank?) || (!property_type2.blank? && property_type3.blank?)
      if (property_type == property_type2) || (property_type == property_type3) || (property_type2 == property_type3)
        errors.add("", "Property Type, Property Type2, Property Type3 can't be same")
        false
      else
        true
      end
    else
      true
    end
  end

  def tenancy_validation
    #if sale_deal?
    if current_leased.blank?
      errors.add("", "Tenancy can't be blank")
      false
    else
      true
    end
    #end
  end

  def all_the_building_validation
    if all_the_building.blank?
      errors.add("", "Whole or Part can't be blank")
      false
    else
      true
    end
  end

  def validate_floor
    not_include_land_dev = 0
    if listings_this_address == true
      not_include_land_dev = 1 if !["Land", "Development", "Farmland"].include?(property_type)
      not_include_land_dev = 1 if (not_include_land_dev == 0 and !["Land", "Development", "Farmland"].include?(property_type2)) && !property_type2.blank?
      not_include_land_dev = 1 if (not_include_land_dev == 0 and !["Land", "Development", "Farmland"].include?(property_type3)) && !property_type3.blank?

      if not_include_land_dev == 0
        false
      else
        true
      end
    else
      false
    end
  end

  def sale_tax
    if sale_deal?
      if price_include_tax == 0
        errors.add("", "Sale tax can't be blank")
        false
      else
        true
      end
    end
  end

  def lease_tax
    if lease_deal?
      if current_rent_include_tax == 0
        errors.add("", "Rent tax can't be blank")
        false
      else
        true
      end
    end
  end

  def all_the_building_text
    case all_the_building
    when 0
      return "Part Building"
    when 1
      return "Whole Building"
    when 2
      return "Not Applicable"
    else
      return ""
    end
  end

  def current_leased_text
    case current_leased
    when 0
      return "Vacant Possession"
    when 1
      return "Tenanted Investment"
    else
      return ""
    end
  end

  def price_include_tax_text
    case price_include_tax
    when 1
      return "Not Applicable"
    when 2
      return "Exclusive"
    when 3
      return "Inclusive"
    else
      return ""
    end
  end

  def current_rent_include_tax_text
    case current_rent_include_tax
    when 1
      return "Not Applicable"
    when 2
      return "Exclusive"
    when 3
      return "Inclusive"
    else
      return ""
    end
  end

end
