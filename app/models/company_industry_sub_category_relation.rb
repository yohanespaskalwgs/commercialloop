class CompanyIndustrySubCategoryRelation < ActiveRecord::Base
  belongs_to :agent_company
  belongs_to :industry_sub_category
end
