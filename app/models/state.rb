# == Schema Info
# Schema version: 20090614051807
#
# Table name: states
#
#  id         :integer(4)      not null, primary key
#  country_id :integer(4)
#  name       :string(255)

class State < ActiveRecord::Base
  has_many :suburbs, :dependent => :destroy
  belongs_to :country

  validates_presence_of :country_id, :name
end
