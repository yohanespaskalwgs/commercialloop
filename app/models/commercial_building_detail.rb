class CommercialBuildingDetail < ActiveRecord::Base
  attr_accessor :property_type
  belongs_to :commercial_building

  include DetailHelper

  def get_property_id
    return self.commercial_building_id
  end

  def all_the_building_text
    case all_the_building
    when 0
      return "Part Building"
    when 1
      return "Whole Building"
    when 2
      return "Not Applicable"
    else
      return ""
    end
  end

  def current_leased_text
    case current_leased
    when 0
      return "Vacant Possession"
    when 1
      return "Tenanted Investment"
    else
      return ""
    end
  end

  def price_include_tax_text
    case price_include_tax
    when 1
      return "Not Applicable"
    when 2
      return "Exclusive"
    when 3
      return "Inclusive"
    else
      return ""
    end
  end

  def current_rent_include_tax_text
    case current_rent_include_tax
    when 1
      return "Not Applicable"
    when 2
      return "Exclusive"
    when 3
      return "Inclusive"
    else
      return ""
    end
  end

end
