class Contact
  attr_accessor :country, :state, :suburb, :street_type, :street_number, :street, :unit_number, :phone_number, :fax_number, :email, :afterhrs_phone, :mobile, :first_name, :last_name

  def initialize(options={})
    options.each {|k,v| instance_variable_set "@#{k}", v }
  end

  def address
    "#{street_number} #{street}"
  end

  def full_name
    "#{first_name} #{last_name}"
  end

end
