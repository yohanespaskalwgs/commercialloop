class ExternalAgent < ActiveRecord::Base
  has_many :properties

  def full_name
    return self.first_name.to_s + " " + self.last_name.to_s
  end
end
