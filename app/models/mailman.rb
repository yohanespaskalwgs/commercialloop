class Mailman < ActionMailer::Base
  def receive(email)
    arr = email.body.scan(/ec_start:.*?:ec_end/)
    if !arr.blank?
      arr[0]= arr[0].gsub("ect_start:","")
      arr[0]= arr[0].gsub(":ect_end","")
      ecampaign_track_id = arr[0]
      ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track_id}") unless ecampaign_track_id.blank?
    end
  end
end
