class MatchProperty < ActiveRecord::Base
  belongs_to :agent
  belongs_to :office
  belongs_to :property
  validates_uniqueness_of :property_id
end
