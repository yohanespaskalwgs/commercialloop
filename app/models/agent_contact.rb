class AgentContact < ActiveRecord::Base
  require "savon"
  extend Savon::Model
  belongs_to :agent
  belongs_to :category_contact
  belongs_to :industry_sub_category
  belongs_to :property_type
  belongs_to :industry_category
  belongs_to :agent_company
  has_many :company_contact_relations
  has_many :contact_property_type_relations, :dependent => :destroy
  has_many :contact_industry_sub_category_relations, :dependent => :destroy
  has_many :contact_alerts, :dependent => :destroy
  has_many :contact_notes, :dependent => :destroy
  has_many :contact_category_relations, :dependent => :destroy
  has_many :contact_assigned_relations, :dependent => :destroy
  has_many :contact_accessible_relations, :dependent => :destroy
  has_many :contact_pastopentime_relations, :dependent => :destroy
  has_many :group_contact_relations, :dependent => :destroy
  has_many :match_contacts, :dependent => :destroy
  accepts_nested_attributes_for :match_contacts
  has_many :offers
  has_many :properties
  has_many :property_contact_enquiries
  has_many :tasks, :dependent => :destroy

  belongs_to :heard_from
  belongs_to :agent_user
  belongs_to :assigned, :class_name => 'AgentUser', :foreign_key => "assigned_to"
  belongs_to :accessible, :class_name => 'AgentUser', :foreign_key => "accessible_by"
  belongs_to :office
  has_one :purchaser
  has_one :sales_record
  has_one :tenancy_record
  validates_uniqueness_of :email, :allow_nil => true, :allow_blank => true
  validates_presence_of :first_name, :if => proc{|x|x.skip_validation != true}
  #  validates_presence_of :industry_category_id, :industry_sub_category_id, :category_contact_id, :unless => :update_contact_id
  validate :validate_username, :validate_password, :if => proc{|x|x.skip_validation != true}
  attr_accessor :skip_validation,:categories_id, :industry_sub_categories_id,:assigns_id, :check_error, :contract, :alert, :boom_status, :group_id, :ecampaign_contact, :property_types_id
  before_create :check_user
  after_create :update_contact_id
  named_scope :valid, :conditions => ["agent_contacts.id IS NOT NULL"]
  #after_save :use_spawn_for_boom
  #after_create :use_spawn_for_md

  def self.authenticate(login,password)
    return nil if login.blank? || password.blank?
    require "base64"
    ag = AgentContact.find(:first, :conditions => { :username => login, :password => Base64.encode64(password)})
  end

  def check_valid?
    rv = true
    if self.id.blank?
      rv = false
    end
    if self.full_name == " " && self.contact_data_alternative.blank?
      rv = false
    end
    if self.email.blank?
      rv = false
    end
    rv
  end

  def self.check_active(x)
    active_image = "/images/checkbox_yes.png"
    inactive_image = "/images/checkbox_no.png"
    return [x.inactive,active_image,inactive_image]
  end

  def self.check_id(x)
    return [x.id,x.email]
  end

  def self.build_sorting(sort,dir)
    case sort
    when 'id'
      return 'agent_contacts.id '+dir
    when 'category'
      return 'category_contacts.name '+dir
    when 'sub-category'
      return 'industry_sub_categories.name ' +dir
    when 'full_name'
      return 'agent_contacts.full_name '+dir
    when 'first_name'
      return 'agent_contacts.first_name '+dir
    when 'last_name'
      return 'agent_contacts.last_name '+dir
    when 'assigned_to'
      return 'users.first_name '+dir
    when 'accessible_by'
      return 'users.first_name '+dir
    when 'updated_at'
      return 'agent_contacts.updated_at '+dir
    when 'suburb'
      return 'agent_contacts.suburb '+dir
    when 'email'
      return 'agent_contacts.email '+dir
    end
  end

  def update_contact_id
    ActiveRecord::Base.connection.execute("UPDATE agent_contacts SET contact_id = '#{self.id}' WHERE id=#{self.id}")
  end

  def add_groups(ids, status = nil)
    ids ||= []
    old_ids = group_contact_relations.map(&:group_contact_id)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    GroupContactRelation.transaction do
      group_contact_relations.all(:conditions => { :group_contact_id => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        group_contact_relations.create(:group_contact_id => id.to_i) unless id.to_i == 0
      end
    end
  end

  def add_property_types(ids, status = nil)
    ids ||= []
    old_ids = contact_property_type_relations.map(&:property_type_id)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    ContactPropertyTypeRelation.transaction do
      contact_property_type_relations.all(:conditions => { :property_type_id => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        contact_property_type_relations.create(:property_type_id => id.to_i) unless id.to_i == 0
      end
    end
    self.property_type_id = contact_property_type_relations.first.nil? ? nil : contact_property_type_relations.first.property_type_id
    self.save
  end

  def add_categories(ids, status = nil)
    ids ||= []
    old_ids = contact_category_relations.map(&:category_contact_id)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    ContactCategoryRelation.transaction do
      contact_category_relations.all(:conditions => { :category_contact_id => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        contact_category_relations.create(:category_contact_id => id.to_i) unless id.to_i == 0
      end
    end
    self.category_contact_id = contact_category_relations.first.nil? ? nil : contact_category_relations.first.category_contact_id
    self.save
  end

  def add_industry_sub_categories(ids, status = nil)
    ids ||= []
    old_ids = contact_industry_sub_category_relations.map(&:industry_sub_category_id)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    ContactIndustrySubCategoryRelation.transaction do
      contact_industry_sub_category_relations.all(:conditions => { :industry_sub_category_id => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        contact_industry_sub_category_relations.create(:industry_sub_category_id => id.to_i) unless id.to_i == 0
      end
    end
    self.industry_sub_category_id = contact_industry_sub_category_relations.first.nil? ? nil : contact_industry_sub_category_relations.first.industry_sub_category_id
    self.save
  end

  def add_assigns(ids, status = nil)
    ids ||= []
    old_ids = contact_assigned_relations.map(&:assigned_to)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    ContactAssignedRelation.transaction do
      contact_assigned_relations.all(:conditions => { :assigned_to => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        contact_assigned_relations.create(:assigned_to => id.to_i) unless id.to_i == 0
      end
    end
    self.assigned_to = contact_assigned_relations.first.nil? ? nil : contact_assigned_relations.first.assigned_to
    self.save
  end

  def add_access(ids, status = nil)
    ids ||= []
    old_ids = contact_accessible_relations.map(&:accessible_by)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    ContactAccessibleRelation.transaction do
      contact_accessible_relations.all(:conditions => { :accessible_by => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        contact_accessible_relations.create(:accessible_by => id.to_i) unless id.to_i == 0
      end
    end
    self.accessible_by = contact_accessible_relations.first.nil? ? nil : contact_accessible_relations.first.accessible_by
    self.save
  end

  def full_name
    return self.first_name.to_s + " " + self.last_name.to_s
  end

  def validate_email
    if self.office.present? && ((self.office.empty_contact_email.blank? || !self.office.empty_contact_email))
      if email.blank?
        errors.add(:email, "can't be blank")
        false
      else
        unless email =~ Authentication.email_regex
          errors.add(:email, Authentication.bad_email_message)
          false
        end
      end
    end
  end

  def validate_password
    if login_required == true
      if id.blank?
        if password.blank?
          errors.add(:password, "can't be blank")
          false
        end
      end
    end
  end

  def validate_username
    if !username.blank?
      u = User.find(:first, :conditions => "login = '#{username}'")
      if !u.blank?
        errors.add(:username, "already exist")
        false
      else
        ac = AgentContact.find(:first, :conditions => "username = '#{username}'")
        unless ac.blank?
          current_contact = AgentContact.find(id)
          if current_contact.username != username
            errors.add(:username, "already exist")
            false
          end
        end
      end
    else
      if login_required == true
        errors.add(:username, "can't be blank")
        false
      end
    end
  end

  def check_exist_model(type)
    conditions_string = "`office_id` = :office_id and `first_name` = :first_name and `last_name` = :last_name"
    conditions_values = {:office_id => office_id, :first_name => first_name, :last_name => last_name}

    case type
    when 'email'
      conditions_string << "  and `email`= :email"
      conditions_values[:email] = email
    when 'mobile_number'
      conditions_string << "  and `mobile_number`= :mobile_number"
      conditions_values[:mobile_number] = mobile_number
    when 'contact_number'
      conditions_string << "  and `contact_number`= :contact_number"
      conditions_values[:contact_number] = contact_number
    when 'work_number'
      conditions_string << "  and `work_number`= :work_number"
      conditions_values[:work_number] = work_number
    when 'home_number'
      conditions_string << "  and `home_number`= :home_number"
      conditions_values[:home_number] = home_number
    end

    check = AgentContact.find(:first, :conditions => [conditions_string, conditions_values])
    return check.nil? ? 0 : 1
  end

  def check_user
    duplicate = 0
    duplicate = check_exist_model("email") unless email.blank?
    (duplicate = check_exist_model("mobile_number") unless duplicate == 1) unless mobile_number.blank?
    (duplicate = check_exist_model("contact_number") unless duplicate == 1) unless contact_number.blank?
    (duplicate = check_exist_model("work_number") unless duplicate == 1) unless work_number.blank?
    (duplicate = check_exist_model("home_number") unless duplicate == 1) unless home_number.blank?
    return unless duplicate == 1
    errors.add(:check_error, "duplicate")
    false
  end

  def update_first_list
    self.category_contact_id = contact_category_relations.first.nil? ? nil : contact_category_relations.first.category_contact_id
    self.property_type_id = contact_property_type_relations.first.nil? ? nil : contact_property_type_relations.first.property_type_id
    #    self.industry_sub_category_id = contact_industry_sub_category_relations.first.nil? ? nil : contact_industry_sub_category_relations.first.industry_sub_category_id
    self.assigned_to = contact_assigned_relations.first.nil? ? nil : contact_assigned_relations.first.assigned_to
    self.accessible_by = contact_accessible_relations.first.nil? ? nil : contact_accessible_relations.first.accessible_by
    self.save
  end

  def self.check_exist(param, type)
    conditions_string = "`office_id` = :office_id and `first_name` = :first_name and `last_name` = :last_name"
    conditions_values = { :office_id => param[:office_id], :first_name => param[:first_name], :last_name => param[:last_name]}

    case type
    when 'email'
      conditions_string << "  and `email`= :email"
      conditions_values[:email] = param[:email]
    when 'mobile_number'
      conditions_string << "  and `mobile_number`= :mobile_number"
      conditions_values[:mobile_number] = param[:mobile_number]
    when 'contact_number'
      conditions_string << "  and `contact_number`= :contact_number"
      conditions_values[:contact_number] = param[:contact_number]
    when 'work_number'
      conditions_string << "  and `work_number`= :work_number"
      conditions_values[:work_number] = param[:work_number]
    when 'home_number'
      conditions_string << "  and `home_number`= :home_number"
      conditions_values[:home_number] = param[:home_number]
    end

    check = AgentContact.find(:first, :conditions => [conditions_string, conditions_values])
    return check
  end

  def create_note(id, prenote, note_type_id, current_id)
    property = Property.find_by_id(id)
    ad = [ property.unit_number, property.street_number, property.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
    address = [ad, property.suburb ].join(", ")
    note = {:agent_contact_id => self.id,:agent_user_id => current_id, :note_type_id => note_type_id,
      :address => address,:description => prenote, :note_date => Time.now, :property_id => id}
    contact_note = ContactNote.new(note)
    return contact_note.id if contact_note.save
  end

  def create_alert(id, current_id, palert_type = nil)
    property = Property.find_by_id(id)
    alert_id = alertnote_id = ""
    bedroom = (property.detail.bedrooms.nil? ? "" : property.detail.bedrooms) if property.detail.respond_to?(:bedrooms)
    alert_type = palert_type.nil? ? "Newly Listed Properties" : palert_type
    note = {:agent_contact_id => self.id, :alert_type => alert_type, :listing_type => property.type,
      :property_type => property.property_type, :min_bedroom => bedroom, :min_bathroom => "",
      :min_carspace => "", :min_price => "", :max_price => property.price, :suburb => ""}
    contact_alert = ContactAlert.new(note)
    if contact_alert.save
      contact_alert.add_suburbs([property.suburb]) unless property.suburb.nil?
      alert_id = contact_alert.id
      if alert_type == 'Latest News'
        prenote = "Requirements Type = #{alert_type}"
      else
        prenote = "Requirements Type = #{alert_type}, Type = #{property.type}, Property Type = #{property.property_type}, Min bedrooms  = #{bedroom}, Max price  = #{property.price}, Suburb = #{property.suburb}"
      end
      #note type = submit alert
      alertnote_id = self.create_note(id, prenote, 4, current_id)
    end
    return {:alert_id => alert_id, :alert_note_id=> alertnote_id}
  end

  def create_note_for_property(id, prenote, note_type_id, heard_form_id, current_id)
    property = Property.find_by_id(id)
    description = (prenote == "")? "Attend Open Inspection" : prenote
    ad = [ property.unit_number, property.street_number, property.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
    address = [ad, property.suburb ].join(", ")
    note = {:property_id => property.id, :agent_contact_id => self.id, :agent_user_id => current_id,
      :note_type_id => note_type_id,:address => address, :description => description, :note_date => Time.now, :heard_from_id => heard_form_id, :duplicate => 0}
    property_note = PropertyNote.new(note)
    return property_note.id if property_note.save
  end

  def create_note_for_contact(type, pnote, current_id, address = "")
    note = {:agent_contact_id => self.id,:agent_user_id => current_id, :note_type_id => type,
      :address => address, :description => pnote, :note_date => Time.now}
    contact_note = ContactNote.new(note)
    return contact_note.id if contact_note.save
  end

  def create_alert_for_contact(param, current_id, created_from = nil)
    alert_type = (param[:alert_type] == "")? "Latest News" : param[:alert_type]
    note = {:agent_contact_id => self.id, :alert_type => alert_type,
      :listing_type => param[:listing_type], :property_type => param[:property_type], :min_bedroom => param[:min_bedroom],
      :min_bathroom => param[:min_bathroom], :min_carspace => param[:min_carspace], :min_price => param[:min_price], :max_price => param[:max_price],
      :suburb => ""}
    contact_alert = ContactAlert.new(note)
    if contact_alert.save
      contact_alert.add_suburbs(param[:suburbs].split(',')) unless param[:suburbs].blank?
      alert_id = contact_alert.id
      prenote = "Requirements Type = #{alert_type},"
      prenote += "Listing Type = #{param[:listing_type]}," unless param[:listing_type].blank?
      prenote +="Property Type = #{param[:property_type]}," unless param[:property_type].blank?
      prenote += "Min Bedroom = #{param[:min_bedroom]}," unless param[:min_bedroom].blank?
      prenote += "Min Bathroom = #{param[:min_bathroom]}," unless param[:min_bathroom].blank?
      prenote += "Min carspace = #{param[:min_carspace]}," unless param[:min_carspace].blank?
      prenote += "Min Price = #{param[:min_price]}," unless param[:min_price].blank?
      prenote += "Max Price = #{param[:max_price]}," unless param[:max_price].blank?
      prenote += "Suburbs = #{param[:suburb]}," unless param[:suburb].blank?

      #note type = alert sent
      alertnote_id = self.create_note_for_contact((created_from.nil? ? 1 : 4), prenote, current_id)
    end
    return {:alert_id => alert_id, :alert_note_id=> alertnote_id}
  end

  def update_property_type(property_type_id)
    old_ids = contact_property_type_relations.map(&:property_type_id)
    contact_property_type_relations.create(:property_type_id => property_type_id.to_i) unless old_ids.include?(property_type_id)
  end

  def update_category_contact(category_id)
    old_ids = contact_category_relations.map(&:category_contact_id)
    contact_category_relations.create(:category_contact_id => category_id.to_i) unless old_ids.include?(category_id)
  end

  def update_industry_sub_category_company(industry_sub_category_id)
    old_ids = contact_industry_sub_category_relations.map(&:industry_sub_category_id)
    contact_industry_sub_category_relations.create(:industry_sub_category_id => industry_sub_category_id.to_i) unless old_ids.include?(industry_sub_category_id)
  end

  def self.search_for_autocomplete(contact_name, agent_id, office_id)
    AgentContact.find(:all, :conditions => ["(`first_name` LIKE ? OR `last_name` LIKE ?) and `agent_id` = ? and `office_id` = ? ", '%'+contact_name+'%', '%'+contact_name+'%', agent_id, office_id])
  end

  def use_spawn_for_boom
    spawn(:kill => true) do
      self.send_boom_api
    end
  end

  def use_spawn_for_md
    spawn(:kill => true) do
      sleep 30
      self.send_to_mydesktop
    end
  end

  def use_spawn_for_irealty
    spawn(:kill => true) do
      sleep 6
      self.send_to_irealty
    end
  end

  def send_boom_api
    return unless self.boom_status.blank?
    return if self.office.blank?
    return unless self.office.send_contact == true
    return if self.office.boom_username.blank? or self.office.boom_password.blank? or self.office.boom_company_name.blank?

    require 'net/http'
    require 'rexml/document'
    salutation = attention = ""
    unless self.first_name.blank?
      salutation = self.first_name.capitalize
      attention = (self.first_name[0].chr + self.last_name).capitalize
    end
    data = "client[firstname]=#{self.first_name}&client[surname]=#{self.last_name}&client[email]=#{self.email}&client[fax]=#{self.fax_number}&client[mobile]=#{self.mobile_number}&client[phone_bh]=#{self.work_number}&client[phone_ah]=#{self.home_number}&client[salutation]=#{salutation}&client[attention]=#{attention}"

    unless self.contact_alerts.blank?
      data += "&client[property_types]=#{self.contact_alerts.first.property_type}" unless self.contact_alerts.first.property_type.blank?
      data += "&client[beds]=#{self.contact_alerts.first.min_bedroom}" unless self.contact_alerts.first.min_bedroom.blank?
      data += "&client[baths]=#{self.contact_alerts.first.min_bathroom}" unless self.contact_alerts.first.min_bathroom.blank?
      data += "&client[carspace]=#{self.contact_alerts.first.min_carspace}" unless self.contact_alerts.first.min_carspace.blank?
      data += "&client[price_from]=#{self.contact_alerts.first.min_price}" unless self.contact_alerts.first.min_price.blank?
      data += "&client[price_to]=#{self.contact_alerts.first.max_price}" unless self.contact_alerts.first.max_price.blank?
      unless self.contact_alerts.first.contact_alert_suburbs.blank?
        data += "&client[suburbs]=#{(self.contact_alerts.first.contact_alert_suburbs.map{|cat|cat.suburb.to_s})}"
      else
        data += "&client[suburbs]=#{self.suburb}"
      end
    else
      data += "&client[suburbs]=#{self.suburb}"
    end

    unless self.contact_notes.blank?
      data += "&client[buying_criteria_notes]=#{self.contact_notes.first.description}"
    end

    unless self.boom_contact_id.blank?
      data += "&_method=put"
      #       url ="#{self.office.boom_company_name}/clients/#{self.boom_contact_id.to_s}.xml"
      url ="clients/#{self.boom_contact_id.to_s}.xml"
    else
      #       url ="#{self.office.boom_company_name}/clients.xml"
      url ="clients.xml"
    end
    #     xml_data = `curl -X POST -d "#{data}" http://#{self.office.boom_username}:#{self.office.boom_password}@boom.boxdice.com.au/api/#{url}`
    if self.office.id == 454
      xml_data = `curl -X POST -d "#{data}" http://#{self.office.boom_username}:#{self.office.boom_password}@richardson-wrench-noosa.boxdice.com.au/api/#{url}`
      Log.create(:message => "DAta: #{data.inspect}, XML: #{xml_data.inspect}, URL: http://#{self.office.boom_username}:#{self.office.boom_password}@richardson-wrench-noosa.boxdice.com.au/api/#{url}")
    else
      xml_data = `curl -X POST -d "#{data}" http://#{self.office.boom_username}:#{self.office.boom_password}@#{self.office.boom_company_name}.boxdice.com.au/api/#{url}`
      Log.create(:message => "DAta: #{data.inspect}, XML: #{xml_data.inspect}, URL: http://#{self.office.boom_username}:#{self.office.boom_password}@#{self.office.boom_company_name}.boxdice.com.au/api/#{url}")
    end

    doc = REXML::Document.new(xml_data)
    return if doc.blank?
    return if doc.root.blank?
    return if doc.root.elements["clientid-pk"].blank?
    boom_client_id = doc.root.elements["clientid-pk"].text
    self.boom_contact_id = boom_client_id
    self.boom_status = true
    self.save
  end

  def send_to_mydesktop
    return if self.sent_mydesktop == true
    return if self.office.blank?
    return unless self.office.send_to_mydesktop == true
    return if self.office.mydesktop_group_id.blank?

    require 'net/http'
    require 'rexml/document'
    require 'open-uri'
    data = "GroupID=#{self.office.mydesktop_group_id}&Commit=1&EmailAlert=1&Title=#{self.title}&Firstname=#{self.first_name}&Lastname=#{self.last_name}&Telephone=#{self.home_number}&Mobile=#{self.mobile_number}&Email=#{self.email}&Postcode=#{self.post_code}&State=#{self.state}"

    #    data = "GroupID=25585&Commit=1&EmailAlert=1&Title=#{self.title}&Firstname=#{self.first_name}&Lastname=#{self.last_name}&Telephone=#{self.home_number}&Mobile=#{self.mobile_number}&Email=#{self.email}&Postcode=#{self.post_code}&State=#{self.province}"
    data += "&source=#{self.heard_from.name}" unless self.heard_from.blank?

    unless self.contact_alerts.blank?
      data += "&Wantbed=#{self.contact_alerts.first.min_bedroom}" unless self.contact_alerts.first.min_bedroom.blank?
      data += "&Wantbath=#{self.contact_alerts.first.min_bathroom}" unless self.contact_alerts.first.min_bathroom.blank?
      data += "&Wantcar=#{self.contact_alerts.first.min_carspace}" unless self.contact_alerts.first.min_carspace.blank?
      data += "&Fromprice=#{self.contact_alerts.first.min_price}" unless self.contact_alerts.first.min_price.blank?
      data += "&Toprice=#{self.contact_alerts.first.max_price}" unless self.contact_alerts.first.max_price.blank?
      unless self.contact_alerts.first.contact_alert_suburbs.blank?
        data += "&Suburb=#{(self.contact_alerts.first.contact_alert_suburbs.map{|cat|cat.suburb.to_s})}"
      else
        data += "&Suburb=#{self.suburb}"
      end
    else
      data += "&Suburb=#{self.suburb}"
    end

    all_relations = self.contact_category_relations
    param_rental = false
    all_relations.each do |rel|
      cat_id = rel.category_contact_id
      if cat_id == 27
        data += "&OnlineAppraisal=1"
      elsif cat_id == 28
        data += "&Newsletter=1"
      elsif cat_id == 29
        data += "&OnlineRequest=1"
      elsif (cat_id == 9 || cat_id == 10 || cat_id == 11) && !param_rental
        param_rental = true
        data += "&Isrental=1"
      end
    end

    unless self.contact_notes.blank?
      data += "&Comments='#{self.contact_notes.first.description}'"
      data += "&PropertyID=#{self.contact_notes.first.property_id}" unless self.contact_notes.first.property_id.blank?
    end
    #colliersinternational.ca.com.au/cgi-bin/clients/colliers/receivecontact.cgi
    begin
      #    xml_data = Net::HTTP.get_response(URI.parse("http://yoda.ca.com.au/cgi-bin/clients/estatebase4/xml/receivecontact.cgi?#{data}")).body
      data = URI::encode(data)
      xml_data = `curl "http://oyster.ca.com.au/cgi-bin/clients/estatebase4/xml/receivecontact.cgi?#{data}"`
      doc = REXML::Document.new(xml_data)
      Log.create(:message => "send_to_ MyDesktop Net::HTTP doc : #{doc.to_s}. Data : #{data.inspect}")
      return if doc.blank?
      return if doc.root.blank?
      return if doc.root.elements["StatusOK"].blank?
      if doc.root.elements["StatusOK"].text == "Y"
        self.sent_mydesktop = true
        self.save
      end
    rescue Exception => ex
      Log.create(:message => "send_to_MyDesktop ERROR: #{ex.inspect}. Data : #{data.inspect}")
    end
  end

  def send_to_irealty
    return if self.office.blank?
    return unless self.office.send_to_irealty == true
    return if self.office.irealty_username.blank? || self.office.irealty_password.blank?
    begin
      client = Savon.client("http://www.sbm21.com/_Components/API.cfc?wsdl")
      login_name = self.office.irealty_username # "agentpoint";
      password = self.office.irealty_password # "ap123";
      login_response = client.request "LoginAPI" do
        soap.body = {
          :userLogin => login_name,
          :userPassword => password
        }
      end
      login_xml = login_response.http.body
      doc = REXML::Document.new(login_xml)
      login_token = doc.elements.each("soapenv:Envelope/soapenv:Body/LoginAPIResponse/LoginAPIReturn"){|e| e}.first.text
      unit_number = self.unit_number.present? ? "#{self.unit_number}/" : ""
      address = "#{unit_number}#{self.street_number} #{self.street_name}"
      if self.country_id.present?
        country =  Country.find(self.country_id)
        country_name = country
      else
        country_name = ""
      end
      user = client.request "AddUpdateSubscriberSync" do
        soap.body = {
          :login_token => login_token,
          :fieldValues => "#{self.title},#{self.first_name},#{self.last_name},#{self.email},#{self.company},#{self.home_number},#{self.work_number},#{self.mobile_number},#{address},#{self.suburb},#{self.state},#{self.post_code},#{country_name},#{Date.today}",
          :fieldList =>   "Salutation,FirstName,LastName,Email,Company,HomePhone,WorkPhone,MobilePhone,Address1,Suburb,State,Postcode,Country,SignupDate"
        }
      end

      user_xml = user.http.body rescue ''
      if user_xml.present?
        user_doc = REXML::Document.new(user_xml)
        user_id = user_doc.elements.each("soapenv:Envelope/soapenv:Body/AddUpdateSubscriberSyncResponse/AddUpdateSubscriberSyncReturn"){|e| e}.first.text
        if user_id.present? && user_id.to_s != "-1"
          self.sent_irealty = true
          self.save
        end
      end
    rescue Exception => ex
      Log.create(:message => "iRealty (errors)----> #{ex.inspect}" )
    end

  end

  def get_assigns_companies(agent_company)
    @list_assigns = ''
    agent_company.company_assigned_relations.map{|cat| @list_assigns += assigned_name(cat.assigned_to.to_i) + ', ' }
    return @list_assigns.blank? ? "All Team Members" : @list_assigns
  end

  def display_street
    "#{unit_number.blank? ? '' : unit_number + '/'} #{street_number.blank? ? '' : street_number} #{street_name.blank? ? '' : street_name}"
  end

  def display_solicitor
    "#{solicitor_name.blank? ? '' : solicitor_name} #{solicitor_phone.blank? ? '' : ' Ph : ' + solicitor_phone} #{solicitor_email.blank? ? '' : ' Email : ' + solicitor_email}"
  end

  def display_agent
    "#{full_name.blank? ? first_name : full_name} #{display_street.blank? ? '' : ' , ' + display_street}"
  end

  def self.get_info_contact(x)
    [x.id,x.email]
  end

  def self.advanced_search(listing_type,property_type,price_from,price_to,deal_type,availiability,suites,range_from,range_to,type_commercial,type,residential_building,list_status_commercial,commercial_type,list_status_residential,state,region,suburb,agent_internal,external_company,agent_external,yield_from,yield_to,building_from,building_to,land_from,land_to,price_from_commercial,price_to_commercial,price_from_residential,price_to_residential,annual_rental_from,annual_rental_to,floor_from,floor_to,office_from,office_to,warehouse_from,warehouse_to,rent_from,rent_to,lease_from,lease_to,tenant,search_keyword_advanced,tenant_name,start,limit,page,agent_id,office_id,order,sort,dir)
    cond = "agent_contacts.agent_id = '#{agent_id}' AND agent_contacts.office_id = '#{office_id}'"
    cond += " AND contact_alerts.keyword LIKE '%#{search_keyword_advanced}%'" unless search_keyword_advanced.blank?
    unless listing_type == "ProjectSale"
      if ["ResidentialSale", "ResidentialLease", "NewDevelopment", "LandRelease", "HolidayLease"].include?(listing_type)
        cond += " AND contact_alerts.listing_type = 'Residential'"
        cond += " AND contact_alerts.availiability_type = '#{availiability}'" unless availiability.blank?
        cond += " AND contact_alerts.state = '#{state}'" unless state.blank?
        cond += " AND contact_alerts.yield_from = '#{yield_from}'" unless yield_from.blank?
        cond += " AND contact_alerts.yield_to = '#{yield_to}'" unless yield_to.blank?
        cond += " AND contact_alerts.building_area_from = '#{building_from}'" unless building_from.blank?
        cond += " AND contact_alerts.building_area_to = '#{building_to}'" unless building_to.blank?
        cond += " AND contact_alerts.land_area_from = '#{land_from}'" unless land_from.blank?
        cond += " AND contact_alerts.land_area_to = '#{land_to}'" unless land_to.blank?
        cond += " AND contact_alerts.annual_rental_from = '#{annual_rental_from}'" unless annual_rental_from.blank?
        cond += " AND contact_alerts.annual_rental_to = '#{annual_rental_to}'" unless annual_rental_to.blank?
        cond += " AND contact_alerts.listing_select_status_residential = '#{list_status_residential}'" unless list_status_residential.blank?
        cond += " AND contact_alerts.asking_price_from = '#{price_from_residential}'" unless price_from_residential.blank?
        cond += " AND contact_alerts.asking_price_to = '#{price_to_residential}'" unless price_to_residential.blank?
        unless type.blank?
          cond += " AND contact_alerts.id IN (SELECT contact_alert_residential_types.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_residential_types ON contact_alerts.id = contact_alert_residential_types.contact_alert_id WHERE contact_alert_residential_types.residential_type IN (#{type.map(&:inspect).join(', ')}))"
        end
        unless residential_building.blank?
          cond += " AND contact_alerts.id IN (SELECT contact_alert_residential_buildings.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_residential_buildings ON contact_alerts.id = contact_alert_residential_buildings.contact_alert_id WHERE contact_alert_residential_buildings.residential_building IN (#{residential_building.map(&:inspect).join(', ')}))"
        end
        unless region.blank? || region == "Please select"
          region_split = region.split(",")
          region_split.map_with_index{|x, i| region_split.delete_at(i) if x == ""  }
          region_val = region_split.map{|x| "\'#{x}\'"}
          region_fix = region_val.join(',')
          cond += " AND contact_alerts.id IN (SELECT contact_alert_regions.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_regions ON contact_alerts.id = contact_alert_regions.contact_alert_id WHERE contact_alert_regions.region IN (#{region_fix}))"
        end
        unless suburb.blank? || suburb == "Please select"
          suburb_split = suburb.split(",")
          suburb_split.map_with_index{|x, i| suburb_split.delete_at(i) if x == ""  }
          suburb_val = suburb_split.map{|x| "\'#{x}\'"}
          suburb_fix = suburb_val.join(',')
          cond += " AND contact_alerts.id IN (SELECT contact_alert_suburbs.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_suburbs ON contact_alerts.id = contact_alert_suburbs.contact_alert_id WHERE contact_alert_suburbs.suburb IN (#{suburb_fix}))"
        end
        page = start.to_i / limit.to_i + 1
        AgentContact.paginate(:all, :include => [:contact_alerts, [:contact_alerts => :contact_alert_suburbs], [:contact_alerts => :contact_alert_regions],[:contact_alerts => :contact_alert_residential_types],[:contact_alerts => :contact_alert_residential_buildings]], :conditions => cond, :page =>page, :per_page => limit.to_i, :order => order)
      elsif ["Commercial", "CommercialBuilding", "BusinessSale"].include?(listing_type)
        cond += " AND contact_alerts.listing_type = 'Commercial'"
        cond += " OR (contact_alerts.listing_type = 'Commercial'"
        cond += " AND contact_alerts.availiability_type = '#{availiability}'" unless availiability.blank?
        cond += " AND contact_alerts.commercial_property_type = '#{commercial_type}'" unless commercial_type.blank?
        cond += " AND contact_alerts.state = '#{state}'" unless state.blank?
        cond += " AND contact_alerts.yield_from = '#{yield_from}'" unless yield_from.blank?
        cond += " AND contact_alerts.yield_to = '#{yield_to}'" unless yield_to.blank?
        cond += " AND contact_alerts.building_area_from = '#{building_from}'" unless building_from.blank?
        cond += " AND contact_alerts.building_area_to = '#{building_to}'" unless building_to.blank?
        cond += " AND contact_alerts.land_area_from = '#{land_from}'" unless land_from.blank?
        cond += " AND contact_alerts.land_area_to = '#{land_to}'" unless land_to.blank?
        cond += " AND contact_alerts.annual_rental_from = '#{annual_rental_from}'" unless annual_rental_from.blank?
        cond += " AND contact_alerts.annual_rental_to = '#{annual_rental_to}'" unless annual_rental_to.blank?
        cond += " AND contact_alerts.listing_select_status_commercial = '#{list_status_commercial}'" unless list_status_commercial.blank?
        cond += " AND contact_alerts.asking_price_from = '#{price_from_commercial}'" unless price_from_commercial.blank?
        cond += " AND contact_alerts.asking_price_to = '#{price_to_commercial}'" unless price_to_commercial.blank?
        cond += " AND contact_alerts.internal_agent = '#{agent_internal}'" unless agent_internal.blank?
        cond += " AND contact_alerts.external_company = '#{external_company}'" unless external_company.blank?
        cond += " AND contact_alerts.external_agent = '#{agent_external}'" unless agent_external.blank?
        cond += " AND contact_alerts.total_floor_area_from = '#{floor_from}'" unless annual_rental_from.blank?
        cond += " AND contact_alerts.total_floor_area_to = '#{floor_to}'" unless floor_to.blank?
        cond += " AND contact_alerts.office_from = '#{office_from}'" unless office_from.blank?
        cond += " AND contact_alerts.office_to = '#{office_to}'" unless office_to.blank?
        cond += " AND contact_alerts.warehouse_from = '#{warehouse_from}'" unless warehouse_from.blank?
        cond += " AND contact_alerts.warehouse_to = '#{warehouse_to}'" unless warehouse_to.blank?
        cond += " AND contact_alerts.rent_from = '#{rent_from}'" unless rent_from.blank?
        cond += " AND contact_alerts.rent_to = '#{rent_to}'" unless rent_to.blank?
        cond += " AND contact_alerts.lease_expiry_from = '#{lease_from}'" unless lease_from.blank?
        cond += " AND contact_alerts.lease_expiry_to = '#{lease_to}'" unless lease_to.blank?
        cond += " AND contact_alerts.tenant = '#{tenant}'" unless tenant_name.blank?
        cond += " AND contact_alerts.id IN (SELECT contact_alert_commercial_types.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_commercial_types ON contact_alerts.id = contact_alert_commercial_types.contact_alert_id WHERE contact_alert_commercial_types.commercial_type IN (#{type_commercial.map(&:inspect).join(', ')}))" unless type_commercial.blank?
        unless region.blank? || region == "Please select"
          region_split = region.split(",")
          region_split.map_with_index{|x, i| region_split.delete_at(i) if x == ""  }
          region_val = region_split.map{|x| "\'#{x}\'"}
          region_fix = region_val.join(',')
          cond += " AND contact_alerts.id IN (SELECT contact_alert_regions.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_regions ON contact_alerts.id = contact_alert_regions.contact_alert_id WHERE contact_alert_regions.region IN (#{region_fix}))"
        end
        unless suburb.blank? || suburb == "Please select"
          suburb_split = suburb.split(",")
          suburb_split.map_with_index{|x, i| suburb_split.delete_at(i) if x == ""  }
          suburb_val = suburb_split.map{|x| "\'#{x}\'"}
          suburb_fix = suburb_val.join(',')
          cond += " AND contact_alerts.id IN (SELECT contact_alert_suburbs.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_suburbs ON contact_alerts.id = contact_alert_suburbs.contact_alert_id WHERE contact_alert_suburbs.suburb IN (#{suburb_fix}))"
        end
        cond += ")"
        page = start.to_i / limit.to_i + 1
        AgentContact.paginate(:all, :include => [:contact_alerts, [:contact_alerts => :contact_alert_suburbs], [:contact_alerts => :contact_alert_regions],[:contact_alerts => :contact_alert_commercial_types]], :conditions => cond, :page =>page, :per_page => limit.to_i, :order => order)
      end
    else
      cond += " AND contact_alerts.listing_type = 'ProjectSale'" unless listing_type.blank?
      cond += " AND contact_alerts.houseland_property_type = '#{property_type}'" unless property_type.blank? || property_type == "Please select"
      cond += " AND contact_alerts.price_range_from = '#{price_from}'" unless price_from.blank?
      cond += " AND contact_alerts.price_range_to = '#{price_to}'" unless price_to.blank?
      cond += " AND contact_alerts.houseland_status = '#{deal_type}'" unless deal_type.blank?
      cond += " AND contact_alerts.suites = '#{suites}'" unless suites.blank?
      cond += " AND contact_alerts.sqm_range_from = '#{range_from}'" unless range_from.blank?
      cond += " AND contact_alerts.sqm_range_to = '#{range_to}'" unless range_to.blank?
      page = start.to_i / limit.to_i + 1
      AgentContact.paginate(:all, :include => [:contact_alerts], :conditions => cond, :page =>page, :per_page => limit.to_i, :order => order)
    end
  end

end
