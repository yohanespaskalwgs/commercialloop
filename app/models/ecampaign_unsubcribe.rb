class EcampaignUnsubcribe < ActiveRecord::Base
  belongs_to :ecampaign
  belongs_to :agent_contact
end
