class IndustrySubCategory < ActiveRecord::Base
  belongs_to :agent_user
  belongs_to :creator, :class_name => 'AgentUser', :foreign_key => "creator_id"
  validates_presence_of :name
  has_many :contact_industry_sub_category_relations
  has_many :company_industry_sub_category_relations
end
