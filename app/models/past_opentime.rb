class PastOpentime < ActiveRecord::Base
  belongs_to :property
  has_many :contact_pastopentime_relations
end
