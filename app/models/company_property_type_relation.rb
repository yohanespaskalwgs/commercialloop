class CompanyPropertyTypeRelation < ActiveRecord::Base
  belongs_to :agent_company
  belongs_to :property_type
end
