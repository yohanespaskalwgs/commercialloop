class HeardFrom < ActiveRecord::Base
  belongs_to :agent_user
  belongs_to :creator, :class_name => 'AgentUser', :foreign_key => "creator_id"
  belongs_to :agent_contact
  has_many :agent_company
  has_many :contact_pastopentime_relations
  has_many :property_notes
  validates_presence_of :name
end
