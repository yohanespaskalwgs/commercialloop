class ActiveHelpPage < ActiveRecord::Base
  has_and_belongs_to_many :users
  validates_presence_of :key, :content

  def after_create
    if self.user_type == 'DeveloperUser'
      ActiveHelpPage.add_developer_to_agent_active_help_pages(self)
    else
      ActiveHelpPage.add_agent_user_to_agent_active_help_pages(self)
    end
  end

  def self.reset_agent_user_active_help_pages
    User.find(:all, :conditions => ["type = 'AgentUser'"]).each do |user|
      user.active_help_pages = ActiveHelpPage.find(:all, :conditions =>["user_type = 'AgentUser'"])
      user.active_help_status = true
      user.save
      ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id =
#{user.id}")
    end
  end

  def self.reset_developer_user_active_help_pages
    User.find(:all, :conditions => ["type = 'DeveloperUser'"]).each do |user|
      user.active_help_pages = ActiveHelpPage.find(:all)
      user.active_help_status = true
      user.save
      ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id =
#{user.id}")
    end
  end

  def self.add_developer_to_agent_active_help_pages(object)
    User.find(:all, :conditions => ["type = 'DeveloperUser'"]).each do |user|
      user.active_help_pages << object
      user.active_help_status = true
      user.save
      ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id =
#{user.id}")
    end
  end

  def self.add_agent_user_to_agent_active_help_pages(object)
    User.find(:all, :conditions => ["type = 'DeveloperUser' OR type='AgentUser'"]).each do |user|
      user.active_help_pages << object
      user.active_help_status = true
      user.save
      ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id =
#{user.id}")
    end
  end

  def self.add_remaining_developer_user_to_active_help
     User.find(:all, :conditions => ["type = 'DeveloperUser'"]).each do |user|
      if user.active_help_pages.blank?
        user.active_help_pages = ActiveHelpPage.find(:all, :conditions => ["user_type = 'DeveloperUser'"])
        user.active_help_status = true
        user.save
        ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id =
#{user.id}")
      end
    end
  end

  def self.add_remaining_agent_user_to_active_help
     User.find(:all, :conditions => ["type = 'AgentUser'"]).each do |user|
      if user.active_help_pages.blank?
        user.active_help_pages = ActiveHelpPage.find(:all, :conditions => ["user_type = 'AgentUser'"])
        user.active_help_status = true
        user.save
        ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id =
#{user.id}")
      end
    end
  end
end
