class GroupCompanyRelation < ActiveRecord::Base
  belongs_to :agent_company
  belongs_to :group_company
end
