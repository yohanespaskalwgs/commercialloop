class EcampaignMailer < ActionMailer::Base
  def send_ecampaign(full_base_url, sender, ecampaign, ecampaign_track, contact)
    @recipients  = contact.email
    @content_type = "text/html"
    @from        = (sender.blank? ? "Admin <noreply@commercialloopcrm.com.au>" : "#{sender.full_name} <#{sender.email}>")
    @subject     = ecampaign.title
    @sent_on     = Time.now
    email = ecampaign.stored_layout.gsub("{ecampaign_track_property_url}", full_base_url+visit_property_link_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{forward}", full_base_url+forward_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{preview}", full_base_url+preview_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{ecampaign_track.id}")
#    email = email.gsub("{ecampaign_track_url}", full_base_url+visit_link_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign).gsub(" ","")+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{ecampaign_track_url}", full_base_url+"/agents/#{ecampaign.office.agent.id}/offices/#{ecampaign.office.id}/ecampaigns/#{ecampaign.id}/visit_link"+"?&track_id=#{ecampaign_track.id}")
    email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{ecampaign_track_id}", "#{ecampaign_track.id}")
    email = email.gsub("{recipient_mail_address}", "#{contact.email}")
    email_layout = email.gsub("{view_open_url}", "<img  style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_ecampaigns_path(ecampaign.office.agent,ecampaign.office)+"?track_id=#{ecampaign_track.id}&ecampaign_id=#{ecampaign.id}"}'/>")
    @body[:email_layout] =  email_layout
  end

  def send_ebrochure(agent, office, property, full_base_url, sender, ebrochure, ecampaign_track, contact)
    @recipients  = contact.email
    @content_type = "text/html"
    @from        = (sender.blank? ? "Admin <noreply@commercialloopcrm.com.au>" : "#{sender.full_name} <#{sender.email}>")
    @subject     = ebrochure.title
    @sent_on     = Time.now
    email = ebrochure.stored_layout.gsub("{ebrochure_track_property_url}", full_base_url+visit_property_link_agent_office_ebrochure_path(agent,office,ebrochure)+"?track_id=#{ecampaign_track.id}&property_id=#{property.id}")
    email = email.gsub("{forward}", full_base_url+forward_agent_office_ebrochure_path(agent,office,ebrochure)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{preview}", full_base_url+preview_agent_office_ebrochure_path(agent,office,ebrochure)+"?track_id=#{ecampaign_track.id}&property_id=#{property.id}")
    email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_ebrochure_path(agent,office,ebrochure)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{ebrochure_track_id}", "#{ecampaign_track.id}")
    email_layout = email.gsub("{view_open_url}", "<img style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_ebrochures_path(agent,office)+"?track_id=#{ecampaign_track.id}&ebrochure_id=#{ebrochure.id}"}'/>")
    @body[:email_layout] =  email_layout
  end

  def send_ealert(full_base_url, sender, ealert, ecampaign_track, contact)
    @recipients  = contact.email
    #@bcc         = "ari.p@kiranatama.com"
    @content_type = "text/html"
    @from        = (sender.blank? ? "Admin <noreply@commercialloopcrm.com.au>" : "#{sender.full_name} <#{sender.email}>")
    @subject     = ealert.title
    @sent_on     = Time.now
    email = ealert.stored_layout.gsub("{ecampaign_track_property_url}", full_base_url+visit_property_link_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{forward}", full_base_url+forward_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{preview}", full_base_url+preview_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{ecampaign_track_url}", full_base_url+visit_link_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{ecampaign_track_id}", "#{ecampaign_track.id}")
    email_layout = email.gsub("<img src='/images/click_here.gif'/>", "<img  style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_ealerts_path(ealert.office.agent,ealert.office)+"?track_id=#{ecampaign_track.id}&ealert_id=#{ealert.id}"}'/>")
    @body[:email_layout] =  email_layout
  end

  def send_emarketing(full_base_url, sender, emarketing, ecampaign_track, contact)
    @recipients  = contact.email
    @content_type = "text/html"
    @from        = (sender.blank? ? "Admin <noreply@commercialloopcrm.com.au>" : "#{sender.full_name} <#{sender.email}>")
    @subject     = emarketing.title
    @sent_on     = Time.now
    email = emarketing.stored_layout.gsub("{forward}", full_base_url+forward_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{preview}", full_base_url+preview_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{ecampaign_track_url}", full_base_url+visit_link_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{emarketing_track_url}", full_base_url+visit_link_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{ecampaign_track_id}", "#{ecampaign_track.id}")
    email_layout = email.gsub("{view_open_url}", "<img  style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_emarketings_path(emarketing.office.agent,emarketing.office)+"?track_id=#{ecampaign_track.id}&ealert_id=#{emarketing.id}"}'/>")
    @body[:email_layout] =  email_layout
  end

  def send_elist(full_base_url, sender, elist, ecampaign_track, contact)
    @recipients  = contact.email
    @content_type = "text/html"
    @from        = (sender.blank? ? "Admin <noreply@commercialloopcrm.com.au>" : "#{sender.full_name} <#{sender.email}>")
    @subject     = elist.title
    @sent_on     = Time.now
    email = elist.stored_layout.gsub("{forward}", full_base_url+forward_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{elist_track_property_url}", full_base_url+visit_property_link_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{preview}", full_base_url+preview_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{ecampaign_track.id}")
    email = email.gsub("{ecampaign_track_id}", "#{ecampaign_track.id}")
    email_layout = email.gsub("{view_open_url}", "<img  style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_elists_path(elist.office.agent,elist.office)+"?track_id=#{ecampaign_track.id}&ealert_id=#{elist.id}"}'/>")
    @body[:email_layout] =  email_layout
  end

  def send_sample(sender, ecampaign, email)
    @recipients  = email
    @content_type = "text/html"
    @from        = (sender.blank? ? "Admin <noreply@commercialloopcrm.com.au>" : "#{sender.full_name} <#{sender.email}>")
    @subject     = ecampaign.title
    @sent_on     = Time.now
    @body[:email_layout] =  ecampaign.stored_layout.gsub("{view_open_url}", "")
    @body[:email_layout] = @body[:email_layout].gsub("{ecampaign_track_url}&link_url=","")
  end

  def send_delayed_jobs_notification
    @recipients  = "yohanes.setiaharja@kiranatama.com"
    @content_type = "text/html"
    @from        = "Admin <noreply@commercialloopcrm.com.au>"
    @subject     = "[DELAYED_JOBS] Check delayed jobs"
    @sent_on     = Time.now
    @body[:email_layout] =  "Check if delayed jobs works properly."
  end

  def send_sphinx_notification
    @recipients  = "yohanes.setiaharja@kiranatama.com"
    @content_type = "text/html"
    @from        = "Admin <noreply@commercialloopcrm.com.au>"
    @subject     = "[SPHINX] Check Sphinx"
    @sent_on     = Time.now
    @body[:email_layout] =  "Check if sphinx works properly."
  end
end
