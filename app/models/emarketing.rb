class Emarketing < ActiveRecord::Base
  belongs_to :office
  has_many :emarketing_images, :as => :attachable, :order => "position asc"

  def self.show_emarketing(conditions)
    sql = [" SELECT `e`.id,`e`.title,`e`.created_at,`e`.sent_at, (SELECT count(r.id) FROM `ecampaign_tracks` r WHERE `r`.type_ecampaign_id = e.id And type_ecampaign='emarketing') as recipient "]
    sql[0] += ", (SELECT count(op.id) FROM `ecampaign_tracks` op WHERE `op`.type_ecampaign_id = e.id And type_ecampaign='emarketing' And `op`.email_opened = 1) as opened "
    sql[0] += ", (SELECT count(bon.id) FROM `ecampaign_tracks` bon WHERE `bon`.type_ecampaign_id = e.id And type_ecampaign='emarketing' And `bon`.email_bounced = 1) as bounced "
    sql[0] += ", (SELECT sum(cl.clicked) FROM `ecampaign_clicks` cl WHERE `cl`.type_ecampaign_id = e.id And type_ecampaign='emarketing') as clicked "
    sql[0] += ", (SELECT sum(fw.email_forwarded) FROM `ecampaign_tracks` fw WHERE `fw`.type_ecampaign_id = e.id And type_ecampaign='emarketing') as forwarded "
    sql[0] += ", (SELECT count(us.id) FROM `ecampaign_unsubcribes` us WHERE `us`.type_ecampaign_id = e.id And type_ecampaign='emarketing') as unsubcribed "
    sql[0] += " FROM `emarketings` e "
    sql_part1 = [" WHERE ( #{conditions.to_s} ) ORDER BY `e`.id DESC"]
    sql[0] += sql_part1.to_s
    return sql
  end

  def copy_image(img, category)
     unless img.blank?
       open("tmp/cache/#{img.filename}", 'wb') do |file|
         file << open(img.public_filename).read
       end
       image_new = self.emarketing_images.new(:uploaded_data => File.new("tmp/cache/#{img.filename}"))
       image_new.attachable = self
       image_new.category = category
       image_new.save!
       File.delete("tmp/cache/#{img.filename}")
     end
     return image_new
  end
end
