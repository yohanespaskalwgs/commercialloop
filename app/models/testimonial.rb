# == Schema Info
# Schema version: 20090614051807
#
# Table name: testimonials
#
#  id            :integer(4)      not null, primary key
#  agent_user_id :integer(4)
#  content       :text
#  name          :string(255)
#  position      :integer(4)
#  created_at    :datetime
#  updated_at    :datetime

class Testimonial < ActiveRecord::Base
  belongs_to :agent_user
  has_many :testi_image, :as => :attachable

  after_save :update_team
  after_destroy :update_team

  def update_team
    agent_user = self.agent_user
    agent_user.updated_at = self.updated_at
    agent_user.save
  end
end
