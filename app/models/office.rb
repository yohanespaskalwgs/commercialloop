# == Schema Info
# Schema version: 20090614051807
#
# Table name: offices
#
#  id                   :integer(4)      not null, primary key
#  agent_id             :integer(4)
#  developer_contact_id :integer(4)
#  support_contact_id   :integer(4)
#  upgrade_contact_id   :integer(4)
#  code                 :string(255)
#  contact_email        :string(255)
#  contact_first_name   :string(255)
#  contact_last_name    :string(255)
#  country              :string(255)
#  email                :string(255)
#  fax_number           :string(255)
#  head                 :boolean(1)
#  latitude             :string(255)
#  longitude            :string(255)
#  name                 :string(255)
#  phone_number         :string(255)
#  state                :string(255)
#  status               :string(255)
#  street               :string(255)
#  street_number        :string(255)
#  street_type          :string(255)
#  suburb               :string(255)
#  unit_number          :string(255)
#  url                  :string(255)
#  zipcode              :string(255)
#  created_at           :datetime
#  updated_at           :datetime

class Office < ActiveRecord::Base
  has_many :agent_users
  has_many :properties, :through => :agent_users
  has_many :user_defined_features, :order =>"name ASC"
  has_many :logos, :class_name => 'Image', :as => :attachable
  has_many :user_file_categories
  has_one :emarketing_brochure_header, :class_name => "Image", :as => :attachable
  has_one :emarketing_brochure_footer, :class_name => "Image", :as => :attachable
  has_one :pdf_brochure_header, :class_name => "Image", :as => :attachable
  has_one :pdf_brochure_footer, :class_name => "Image", :as => :attachable
  has_one :pdf_configuration, :dependent => :destroy
  belongs_to :agent
  belongs_to :developer_contact, :class_name => 'DeveloperUser'
  belongs_to :support_contact, :class_name => 'DeveloperUser'
  belongs_to :upgrade_contact, :class_name => 'DeveloperUser'
  has_one :subscription, :dependent => :destroy
  has_one :watermark, :as => :attachable
  has_one :vault_template, :dependent => :destroy

  # has_many :through has problem with polymorphic association + STI
  # so I manually write sql here
  has_many :files, :class_name => 'UserFile', :finder_sql => "SELECT `attachments`.* FROM `attachments` INNER JOIN `users` ON `attachments`.attachable_id = `users`.id AND  (`attachments`.attachable_type = 'User' or `attachments`.attachable_type = 'AgentUser') WHERE (((users.office_id = \#{id}) AND (users.deleted_at IS NULL OR users.deleted_at > now()) AND (`users`.`type` = 'AgentUser')) OR (parent_id = \#{id})) AND (`attachments`.`type` = 'UserFile') ORDER BY `attachments`.`updated_at` DESC"

  has_many :searches
  has_many :office_developer_accesses
  has_many :developer_accesses, :through => :agent_developer_accesses, :source => :developer_user
  has_many :portal_accounts, :dependent => :destroy
  has_many :domains, :dependent => :destroy
  has_many :portal_autosubcribes, :dependent => :destroy
  has_many :agent_contacts, :dependent => :destroy
  has_many :agent_companies, :dependent => :destroy
  has_many :stocklist, :dependent => :destroy, :as => 'attachable'
  has_many :office_specialities_relations, :dependent => :destroy
  has_many :extra_detail_names, :dependent => :destroy
  has_many :extra_options
  has_many :match_properties, :dependent => :destroy
  has_many :email_configuration_searches, :dependent => :destroy
  has_many :task_types, :dependent => :destroy
  has_many :tasks, :dependent => :destroy

  named_scope :active, :conditions => ['offices.status = ? AND offices.deleted_at IS NULL',"active"]
  named_scope :inactive, :conditions => ['offices.status = ? AND offices.deleted_at IS NULL',"inactive"]
  named_scope :pending, :conditions => ['offices.status = ?',"pending"]
  named_scope :suspended, :conditions => ['offices.status = ?',"suspended"]
  named_scope :new_development, :conditions => ["offices.new_development = ?", true]
  named_scope :land_release, :conditions => ["offices.land_release = ?", true]
  named_scope :commercial_building, :conditions => ["offices.commercial_building = ?", true]
  named_scope :residential_lease, :conditions => ["offices.residential_lease = ?", true]
  named_scope :residential_sale, :conditions => ["offices.residential_sale = ?", true]
  named_scope :holiday_lease, :conditions => ["offices.holiday_lease = ?", true]
  named_scope :commercial, :conditions => ["offices.commercial = ?", true]
  named_scope :project_sale, :conditions => ["offices.project_sale = ?", true]
  named_scope :business_sale, :conditions => ["offices.business_sale = ?", true]

  validates_presence_of :status, :country
  validates_presence_of :upgrade_contact_id, :support_contact_id, :developer_contact_id, :if => :is_developer
  validates_uniqueness_of :code, :scope => :agent_id, :on => :create
  validates_presence_of :agent_id, :name
  validates_presence_of :office_paypal_account, :property_listing_cost, :if => proc{|x|x.property_payment_required == true}
  validates_format_of :code, :with => /^\d{4}$/i, :message => "office must have a 4 digits code"

  before_validation_on_create :generate_sequential_code
  before_validation :set_status
  after_save :update_developer_summary, :use_spawn

  STATUS = ["active", "inactive", "pending", "suspended"]

  extend ActiveSupport::Memoizable

  composed_of(:address, :class_name => 'Address',
    :mapping =>
      [
      [:unit_number, :unit_number]  ,
      [:street_number, :street_number]  ,
      [:street, :street]  ,
      [:suburb, :suburb]  ,
      [:state, :state]   ,
      [:country, :country]
    ])
  attr_accessor :is_developer, :new_agent_id

  def use_spawn
    if self.count_api_sent.to_i > 0
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE offices SET count_api_sent = 0 WHERE id = #{self.id}"
      sql.commit_db_transaction
    end

    sql = ActiveRecord::Base.connection()
    sql.update "UPDATE offices SET processed = 0 WHERE id = #{self.id}"
    sql.commit_db_transaction

    spawn(:kill => true) do
      # wait 10 seconds before first trying send to third party
      sleep 10
      first_shot = second_shot = third_shot = true

      first_shot = self.check_before_send
      if first_shot == false
        # wait 60 seconds before second trying send to third party
        sleep 60
        second_shot = self.check_before_send
      end
      if second_shot == false
        # wait 60 seconds before third trying send to third party
        sleep 60
        third_shot = self.check_before_send
      end
    end
  end

  def check_before_send
    office = Office.find_by_sql("SELECT * FROM offices WHERE (`processed` = 0 OR `processed` IS NULL) AND (`count_api_sent` < 3 OR `count_api_sent` IS NULL) AND id = #{self.id.to_i}").first
    unless office.blank?
      tmp = office.count_api_sent.to_i + 1
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE offices SET count_api_sent = #{tmp} WHERE id = #{office.id}"
      sql.commit_db_transaction
      office.send_office_api
      return false
    else
      return true
    end
  end

  def send_office_api
    domains = self.domains
    is_http_api = self.http_api_enable
    is_office_active = self.status == 'active' ? true : false
    return if domains.blank? || (is_http_api == false || is_http_api.blank?) || is_office_active == false
    domains.each do |domain|
      next if domain.ipn.blank?
      begin
        office_attr = self.attributes
        encoded_office = {}
        office_attr.each do |off_attr|
          if off_attr.last.is_a?(String)
            encoded_office.merge!(off_attr.first => Base64.encode64(off_attr.last))
          else
            encoded_office.merge!(off_attr.first => off_attr.last)
          end
        end
        logos = get_original_logos
        unless logos.blank?
          encoded_office.merge!("logo" => {"0"=>( logos.first.blank? ? "" : logos.first.real_filename ),"1"=>( logos.second.blank? ? "" : logos.second.real_filename )})
          encoded_office.merge!("logo_s3" => {"0"=>( logos.first.blank? ? "" : logos.first.public_filename ),"1"=>( logos.second.blank? ? "" : logos.second.public_filename )})
        end
        token = Digest::SHA1.hexdigest("#{Time.now}")[8..50]
        GLOBAL_ATTR["TOK-#{self.id}-#{self.agent.id}"] = token
        call_back_url = GLOBAL_ATTR["OFF-#{self.agent.id}-#{self.agent.developer.id}"].blank? ? "commercialloopcrm.com.au" : GLOBAL_ATTR["OFF-#{self.agent.id}-#{self.agent.developer.id}"]

        encoded_specialities = {}
        unless self.office_specialities_relations.blank?
          self.office_specialities_relations.each_with_index do |opt, i|
            encoded_specialities.merge!(i.to_s => {"id"=> opt.office_speciality.id, "name"=> Base64.encode64(opt.office_speciality.name)})
          end
        end

        system("curl -i -X POST -d 'office=#{encoded_office.to_json}&detail=#{self.domains.to_json}&specialities=#{encoded_specialities.to_json}&callback_url=http://#{call_back_url}/developers/#{self.agent.developer.id}/agents/#{self.agent.id}/offices/#{self.id}/office_notification?token=#{token}&type=office' #{domain.ipn}")
        ActiveRecord::Base.connection.execute("UPDATE superapi_offices SET next_listoffices = UNIX_TIMESTAMP() + 15 WHERE office_id = #{self.id}")
        self.api_is_sent = true
      rescue Exception => ex
        Log.create(:message => "property ---> #{self.inspect} :: ---> errors : #{ex.inspect}")
        self.api_is_sent = false
      end
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE offices SET api_is_sent = #{self.api_is_sent} WHERE id = #{self.id}"
      sql.commit_db_transaction
    end
  end

  def get_original_logos
    #return self.logos.map{|logo| logo.public_filename if logo.public_filename.include?('original')}.delete_if{|x|x.nil?} unless self.logos.blank?
    return self.logos.find(:all, :conditions=> ["`parent_id` is NULL"])
  end

  def delete_office
    self.deleted_at = Time.now
    if self.save_with_validation(false)
      undelete_user = User.find_with_deleted(:all,:conditions=>["office_id = ?",self.id])
      undelete_user.each do |user|
        user.update_attribute("deleted_at",nil)
      end
      properties.each do |property|
        #        property.deleted_at = Time.now
        #        property.status = 4
        #        property.save_with_validation(false)
        sql = ActiveRecord::Base.connection()
        sql.update "UPDATE properties SET deleted_at = '#{Time.now.strftime("%Y-%m-%d")}', status = 4 WHERE id = #{property.id}"
        sql.commit_db_transaction
        property.update_attribute(:updated_at, Time.now)
      end
      agent_users.each do |agent_user|
        #        agent_user.deleted_at = Time.now
        #        agent_user.save_with_validation(false)
        sql = ActiveRecord::Base.connection()
        sql.update "UPDATE users SET deleted_at = '#{Time.now.strftime("%Y-%m-%d")}' WHERE id = #{agent_user.id}"
        sql.commit_db_transaction
        agent_user.update_attribute(:updated_at, Time.now)
      end
    end
  end

  def is_agent
    @is_developer == true
  end

  def create_first_user(params_user)
    agent_user = agent_users.build
    agent_user.developer = self.agent.developer
    agent_user.email = self.contact_email
    agent_user.first_name = self.contact_first_name
    agent_user.last_name = self.contact_last_name
    agent_user.phone = self.phone_number
    agent_user.generate_sequential_code # force this as we don't validate
    if !params_user[:login].blank? && !params_user[:password].blank? && !params_user[:password_confirmation].blank?
      agent_user.login = params_user[:login]
      agent_user.password = params_user[:password]
      agent_user.password_confirmation = params_user[:password_confirmation]
    else
      agent_user.generate_username_and_password
    end
    agent_user.save(false) # email might be used as we don't validate uniqueness of contact email

    UserMailer.deliver_agent_user_creation(agent_user, self)
  end

  # apply agent developer access & contacts settings
  def apply_agent_settings
    self.developer_contact = agent.developer_contact
    self.support_contact   = agent.support_contact
    self.upgrade_contact   = agent.upgrade_contact

    self.office_developer_accesses.clear
    agent.agent_developer_accesses.each do |access|
      self.office_developer_accesses.build(:developer_user_id => access.developer_user_id)
    end
  end

  def full_code
    "#{agent.full_code}-#{code}"
  end

  def full_name
    "#{agent.name} - #{name}"
  end


  # composed_of is useless :(
  def contact
    @contact = Contact.new :country => country, :state => state, :suburb => suburb, :street_type => street_type, :street => street, :street_number => street_number, :unit_number => unit_number, :fax_number => fax_number, :phone_number => phone_number, :email => email
  end
  memoize :contact

  def contact=(ct)
    self.country = ct.country
    self.state = ct.state
    self.suburb = ct.suburb
    self.street_type = ct.street_type
    self.street_number = ct.street_number
    self.street = ct.street
    self.unit_number = ct.unit_number
    self.phone_number = ct.phone_number
    self.fax_number = ct.fax_number
    self.email = ct.email
  end

  # return an array which contains office stats
  # [Avaliable, Sold/Leased, Under Offer, Withdrawn, Drafts, Total]
  def stats(type,current_office_id)
    stats_array = []
    [["available",1], ["soldleased",2,3], ["underoffer",5], ["withdrawn",4]].each do |status|
      unless status[0] == "soldleased"
        stats_array << Property.count(:all,:conditions => [" properties.office_id = ? AND properties.type = ? AND properties.status = ? ",current_office_id,type,status[1]])
      else
        stats_array << Property.count(:all,:conditions => [" properties.office_id = ? AND properties.type = ? AND ( properties.status = ? or properties.status = ? )",current_office_id,type,status[1],status[2]])
      end
    end
    stats_array << Property.count(:all,:conditions => [" properties.office_id = ? AND properties.type = ? AND properties.status = 6 ",current_office_id,type])
    stats_array << Property.count(:all,:conditions => [" properties.office_id = ? AND properties.type = ? ",current_office_id,type])

    #    pr = Property.find(:all, :conditions => "office_id = #{current_office_id} AND type = '#{type}' AND status IN (1,2,3,4,5,6)", :select => "status" )
    #    sarr = []
    #    [[1],["sl",2,3],[5],[4],[6]].each do |status|
    #      if status[0] != "sl"
    #        sarr << pr.find_all{|a| a.status == status[0]}.count
    #      else
    #        sarr << pr.find_all{|a| a.status == status[1] || a.status == status[2]}.count
    #      end
    #    end
    #    sarr << pr.count
    #    sarr
    stats_array
  end

  def all_suburbs
    properties.find(:all,:select => "suburb").collect {|p| p.suburb }.uniq.collect {|s| [s,s] }
  end

  def accessible_by?(user)
    case user
    when AdminUser
      true
    when DeveloperUser
      if !user.developer.offices.find(self.id).nil?
        if user.access_level == 1
          true
        elsif user.access_level > 1 && office_developer_accesses.map(&:developer_user_id).include?(user.id)
          true
        else
          false
        end
      else
        false
      end
    when AgentUser
      if user.access_level == 1
        user.agent.offices.find(self.id)
      elsif user.access_level >= 2
        user.office.id == self.id
      else
        false
      end
    else
      false
    end
  end

  def generate_sequential_code
    return if (agent_id.blank? || !code.blank?)
    v = agent.offices.count
    self.code = ("%04d" % v)
    while Office.find_by_agent_id_and_code(agent_id, code)
      v += 1
      self.code = ("%04d" % v)
    end
  end

  def after_create
    sp = subscription_plan('one_monthly')
    if self.property_owner_access?
      total_price = sp['price'] + 4000
    else
      total_price = sp['price']
    end

    if self.country == "Australia"
      sub_taxes = 0.1 * total_price
    else
      sub_taxes = 0
    end

    if self.agent.developer.active_offices == true
      status = "active"
    else
      status = "pending"
    end

    sub = Subscription.create(:account_id => self.agent.developer_id, :tariff_plan_id => "one_monthly",
      :quantity => 1, :taxes_id => "au", :periodicity => "1m",
      :currency => sp['currency'], :net_amount => total_price, :taxes_amount => sub_taxes,
      :starts_on => Time.now, :next_payment_date => nil,
      :ends_on => nil, :status => "pending", :office_id => self.id)
  end

  def before_create
    self.status = "active"
  end

  # Return value is like:
  # {:trial_end_date => '', :next_payment_date => '', :price => '', :currency => ''}
  def subscription_plan(tpi=nil)
    tpi = subscription.tariff_plan_id if tpi.nil?
    rv = {}
    tp = SubscriptionManager.all_tariff_plans[tpi]
    rv['trial_end_date'] = tp['payment_term']['trial_days'].days.since(created_at)
    array = tp['payment_term']['periodicity'].split(//)
    if array[1] == 'm'
      rv['next_payment_date'] = array[0].to_i.months.from_now
    end
    rv['price'] = tp['price']
    rv['currency'] = tp['currency']
    return rv
  end

  def add_autosubcribes(ids, status = nil)
    ids ||= []
    old_ids = portal_autosubcribes.map(&:portal_id)
    new_ids = ids.map { |i| i.to_i }

    if status.nil?
      to_delete = old_ids - new_ids
    end
    to_add = new_ids - old_ids

    PortalAutosubcribe.transaction do
      if status.nil?
        portal_autosubcribes.all(:conditions => { :portal_id => to_delete}).each(&:destroy)
      end
      to_add.each do |id|
        portal_autosubcribes.create(:portal_id => id.to_i) unless id.to_i == 0
      end
    end
  end

  def update_property_exports
    properties.each do |property|
      country = Country.find_by_name(self.country)
      next if country.blank?
      portal_countries = country.portal_countries
      next if portal_countries.blank?
      unless self.id == 301
        allowed = [1,2,3,4,5,6].include?(property.status)
        next unless allowed
      end
      portal_countries.each do |p_c|
        portal_autosubcribe = PortalAutosubcribe.find_by_portal_id_and_office_id(p_c.portal.id,self.id)
        unless portal_autosubcribe.blank?
          cek = PropertyPortalExport.find_by_property_id_and_portal_id(property.id,p_c.portal.id)
          if cek.blank?
            sql="INSERT INTO `property_portal_exports` (`property_id` ,`portal_id`) VALUES ('#{property.id}', '#{p_c.portal.id}');"
            ActiveRecord::Base.connection.execute(sql)
          end
        end
      end
    end
  end

  def add_specialities(ids, status = nil)
    ids ||= []
    old_ids = office_specialities_relations.map(&:office_speciality_id)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    OfficeSpecialitiesRelation.transaction do
      office_specialities_relations.all(:conditions => { :office_speciality_id => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        office_specialities_relations.create(:office_speciality_id => id.to_i) unless id.to_i == 0
      end
    end
  end

  def is_allow_commercial?
    rv = true
    if (self.commercial == false || self.commercial.blank?) && (self.commercial_building == false || self.commercial_building.blank?)
      rv = false
    end
    rv
  end

  def is_allow_business_sale?
    rv = true
    if self.business_sale == false || self.business_sale.blank?
      rv = false
    end
    rv
  end

  def is_allow_residential?
    rv = true
    if (self.residential_sale == false || self.residential_sale.blank?) && (self.residential_lease == false || self.residential_lease.blank?) && (self.new_development == false || self.new_development.blank?)
      rv = false
    end
    rv
  end

  def is_allow_houseland?
    rv = true
    if self.project_sale == false || self.project_sale.blank?
      rv = false
    end
    rv
  end

  def is_only_allow_commercial?
    rv = self.commercial || self.commercial_building
    if self.residential_sale || self.residential_lease || self.new_development || self.land_release || self.holiday_lease || self.business_sale || self.project_sale || self.clearing_sale || self.clearing_sales_event || self.general_sale || self.livestock_sale
      rv = false
    elsif self.commercial || self.commercial_building
      rv = true
    end
    rv
  end

  def is_only_allow_business_sale?
    rv = self.business_sale
    if self.residential_sale || self.residential_lease || self.new_development || self.land_release || self.holiday_lease || self.project_sale || self.clearing_sale || self.clearing_sales_event || self.general_sale || self.livestock_sale || self.commercial || self.commercial_building
      rv = false
    end
    rv
  end

  def is_only_allow_residential?
    rv = self.residential_sale || self.residential_lease || self.new_development
    if self.commercial_building || self.commercial || self.land_release || self.holiday_lease || self.business_sale || self.project_sale || self.clearing_sale || self.clearing_sales_event || self.general_sale || self.livestock_sale
      rv = false
    elsif self.residential_sale || self.residential_lease || self.new_development
      rv = true
    end
    rv
  end

  def is_only_allow_houseland?
    rv = self.project_sale
    if self.new_development || self.residential_sale || self.residential_lease || self.commercial_building || self.commercial || self.land_release || self.holiday_lease || self.business_sale || self.clearing_sale || self.clearing_sales_event || self.general_sale || self.livestock_sale
      rv = false
    elsif self.project_sale
      rv = true
    end
    rv
  end

  def is_allow_all_property_type?
    self.new_development && self.residential_sale && self.residential_lease && self.commercial_building && self.commercial && self.land_release && self.holiday_lease && self.business_sale && self.project_sale && self.clearing_sale && self.clearing_sales_event && self.general_sale && self.livestock_sale
  end

  def subtract_sms_credit(credit)
    unless self.sms_credit == 0
      self.sms_credit -= credit
      save
    end
  end

  private

  def set_status
    if status.blank?
      self.status = "pending"
    end
  end


  def validate_on_update
    if status == "active" && !agent.blank?
      return true if agent.developer.offices.active.map(&:id).include?(id) # already activated
      errors.add("status", "Cannot activate due to insufficient subscription limit") if agent.developer.available_subscriptions < 1
    end
  end

  def update_developer_summary #temporary hook solution, should move to rake task
    spawn(:kill => true) do
      if status_changed?
        agent.developer.generate_developer_summaries unless agent.blank?
      end
    end
  end

end
