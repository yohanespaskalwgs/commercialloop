class ExtraDetail < ActiveRecord::Base
  belongs_to :property
  belongs_to :extra_detail_name, :foreign_key => :extra_detail_name_id
end
