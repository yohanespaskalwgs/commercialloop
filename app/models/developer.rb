# == Schema Info
# Schema version: 20090614051807
#
# Table name: developers
#
#  id            :integer(4)      not null, primary key
#  code          :string(255)
#  company       :string(255)
#  country       :string(255)
#  description   :text
#  domain        :string(255)
#  email         :string(255)
#  entry         :string(255)
#  fax_number    :string(255)
#  name          :string(255)
#  phone_number  :string(255)
#  state         :string(255)
#  status        :string(255)     default("active")
#  street        :string(255)
#  street_number :string(255)
#  street_type   :string(255)
#  suburb        :string(255)
#  time_zone     :string(255)
#  unit_number   :string(255)
#  website       :string(255)
#  created_at    :datetime
#  deleted_at    :datetime
#  updated_at    :datetime

class Developer < ActiveRecord::Base
  extend ActiveSupport::Memoizable
  acts_as_paranoid

  attr_accessor   :agreement

  HUMANIZED_ATTRIBUTES = {
      :company => "Company name",
      :entry => "Unique URL",
      :agreement => "Terms"
    }

  def self.human_attribute_name(attr)
    HUMANIZED_ATTRIBUTES[attr.to_sym] || super
  end


  define_index do
    indexes :name
  end

  has_one :logo, :class_name => 'Image', :as => :attachable
  has_many :agents
  has_many :developer_users, :dependent => :destroy
  has_many :agent_users
  has_many :users
  has_many :offices, :through => :agents
  has_many :message_categories, :dependent => :destroy
  has_many :developer_summaries, :dependent => :destroy
  has_many :domains, :dependent => :destroy
  has_many :portal_combineds, :dependent => :destroy

  has_many :subscriptions, :foreign_key => :account_id
  has_many :subscription_histories, :through => :subscriptions do

    def period(*args)
     options = args.extract_options!
     options[:start] ||= 1.month.ago.to_date
     options[:end] ||= Time.today
     find(:all, :conditions => ["subscription_histories.created_at > ? AND subscription_histories.created_at <= ? ", options[:start], options[:end]], :order => "created_at DESC")
    end

  end

  validates_presence_of :company, :message => "can't be blank"
  validates_presence_of :entry,   :message => "is invalid"
  validates_uniqueness_of :code, :name, :entry
  validates_format_of :entry,
    :with => /^[a-z][a-z0-9\-]{2,}$/i,
    :message => "is invalid"
  validates_exclusion_of :entry, :in => %w( admin www api), :message => "reserved names"
  validates_format_of :code,
    :with => /^[a-z]{2}\d{5}$/i,
    :message => "product code must be start with 2 letter country code, followed by 5 digits"
  validates_acceptance_of :agreement,
    :message => "are not checked"

  before_validation_on_create :generate_random_code
  STATUS = ["active", "inactive", "suspended"]
  PAYMENTS = ["current", "overdue"]

  named_scope :active, :conditions => ['developers.status = "active"']
  named_scope :inactive, :conditions => ['developers.status = "inactive"']
  named_scope :suspended, :conditions => ['developers.status = "suspended"']

  before_create do |d|
    #load system message categories
    MessageCategory.system_categories.each do |cate|
      d.message_categories.build(:name => cate.name)
    end
  end

  def owner
    developer_users.first
  end

  def available_subscriptions
    subscriptions.active.count - offices.active.count
  end

  def generate_developer_summaries
    stats = {}
    total = {:client_a => 0, :client_i => 0, :client_s => 0, :office_a => 0, :office_i => 0, :office_p => 0, :office_s => 0, :team => 0, :listing_a => 0, :listing_i => 0}
    countries.each do |country|
      s = {}
      clients = agents.scoped_by_country(country)
      total[:client_a] += (s[:client_a] = clients.active.count)
      total[:client_i] += (s[:client_i] = clients.inactive.count)
      total[:client_s] += (s[:client_s] = clients.suspended.count)

      the_offices = offices.scoped_by_country(country)
      total[:office_a] += (s[:office_a] = the_offices.active.count)
      total[:office_i] += (s[:office_i] = the_offices.inactive.count)
      total[:office_p] += (s[:office_p] = the_offices.pending.count)
      total[:office_s] += (s[:office_s] = the_offices.suspended.count)

      total[:team] += (s[:team] = agent_users.count(:conditions => { :office_id => the_offices.map(&:id) }))

      al = active_properties_count(country)
      total[:listing_a] += (s[:listing_a] = al)
      total[:listing_i] += (s[:listing_i] = properties_count(country) - al)
      stats[country] = s
    end

    DeveloperSummary.transaction do
      developer_summaries.clear
      stats.each do |c, s|
        developer_summaries.build(s.merge(:country => c))
      end
      developer_summaries.build(total.merge(:country => "Total"))
      save
    end
  end

  #TODO optimize this!!!
  def properties_count(country = nil)
    begin
      if country.blank?
        agents.inject(0) { |c, a| a.properties.count + c }
      else
        #agent properties uses finder_sql, so count(:conditions) doesn't work
        agents.inject(0) { |c, a| a.properties.select{|p| p.country == country}.size + c }
      end
    rescue Exception => ex
      return 0
    end
  end

  #TODO optimize this!!!
  def active_properties_count(country = nil)
    begin
      if country.blank?
        agents.inject(0) { |c, a| a.properties.select{|p| p.status == Property::STATUS[:available]}.size + c }
      else
        agents.inject(0) { |c, a| a.properties.select{|p| p.country == country && p.status == Property::STATUS[:available]}.size + c }
      end
    rescue Exception => ex
      return 0
    end
  end

  #TODO optimize this!!!
  def disk_usage
    begin
      agent_users.inject(0) { |c, u| c + u.disk_usage }
    rescue Exception => ex
      return 0
    end
  end

  def pending_offices_count
    offices.pending.count
  end

  def countries
    # c = []
    #     c.concat offices.collect(&:country)
    #     c.concat agents.collect(&:country)
    #     c.uniq
    agents.all(:order => "agents.country asc", :group => "agents.country", :select => "agents.country").map(&:country)
  end

  memoize :active_properties_count, :properties_count, :pending_offices_count, :countries


  def site_url
    "#{entry}.#{domain.blank? ? DOMAIN_NAME : domain}"
  end


  def full_code
    code
  end

  def postcode
    suburb_model.postcode rescue nil
  end

  def suburb_model
    Suburb.find_by_name_and_country_id(suburb, country_model.id) rescue nil
  end

  def country_model
    Country.find_by_code(country) rescue nil
  end

  def name
    read_attribute("name") || company
  end

  # authenticate user (AgentUser and DeveloperUser) within a developer
  def self.authenticate_user(domain_entry, login, password)
    return nil if login.blank? || password.blank?

    developer = find_by_entry(domain_entry)
    return nil unless developer

    u = developer.users.first(:conditions => { :login => login })

    u && u.authenticated?(password) ? u : nil
  end

  def accessible_by?(user)
    case user
    when AdminUser
      true
    when DeveloperUser
      user.developer.id == self.id
    else
      false
    end
  end


  private

  # The average chance to find a usable code = 1 - model.all.size/100000
  # The algorithm is sufficient fast when there's less than 50000 records
  # Another implementation, that select all codes from db first then random
  # choose one from [code | code <- all code space - selected codes] will
  # be more efficient when there's more 50000 products, but will possiblely
  # consume much more memory
  def generate_random_code
    return true unless code.blank?
    begin
      v ||= rand*100000
      self.code = %Q!au#{"%05d" % v}!
    end while Developer.find_by_code(code)
  end

end
