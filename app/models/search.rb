class Search < ActiveRecord::Base
  attr_accessible :keyword_name
  belongs_to :agent
  belongs_to :office

  def self.build_sorting(sort,dir)
  	case sort
  	when 'id'
  	  return 'searches.id '+dir
  	when 'keyword'
  	  return 'searches.keyword_name '+dir
  	when 'created_at'
  	  return 'searches.created_at '+dir
  	end
  end

  def self.delete_search(id)
  	delete = id
  	return [delete]
  end
end
