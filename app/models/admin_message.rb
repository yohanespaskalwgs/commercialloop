# == Schema Info
# Schema version: 20090614051807
#
# Table name: messages
#
#  id                       :integer(4)      not null, primary key
#  category_id              :integer(4)
#  office_id                :integer(4)
#  sender_id                :integer(4)
#  topic_id                 :integer(4)
#  admin_unread             :boolean(1)      default(TRUE)
#  archive_state            :integer(4)      default(0)
#  message                  :text
#  message_comments_count   :integer(4)      default(0)
#  message_recipients_count :integer(4)      default(0)
#  pending_archive          :boolean(1)
#  rating                   :integer(4)      default(0)
#  title                    :string(255)
#  type                     :string(255)
#  created_at               :datetime
#  updated_at               :datetime

# message that developer sends to admin (commercialloopcrm)
# admin message are visible to all admin users so there's no recipients other than the message creator (developer)

class AdminMessage < Message

  belongs_to :topic, :class_name => "MessageTopic"

  validates_presence_of :topic, :message => "must be selected"

  named_scope :admin_unread, :conditions => { :admin_unread => true }
  named_scope :admin_read, :conditions => { :admin_unread => false }

  def peding_archivable_by?(user)
    AdminUser === user
  end
end
