# == Schema Info
# Schema version: 20090614051807
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime
class Attachment < ActiveRecord::Base
  belongs_to :attachable, :polymorphic => true

  # MessageFile & MessageCommentFile validate attachment in their own way
  validates_presence_of :attachable_id, :attachable_type, :unless => Proc.new { |file| file.kind_of?(MessageFile) || file.kind_of?(MessageCommentFile) || file.kind_of?(Video)}
  validates_presence_of :filename, :size, :content_type, :unless => Proc.new { |file| file.kind_of?(Video)}
end
