# == Schema Info
# Schema version: 20090614051807
#
# Table name: residential_lease_details
#
#  id                       :integer(4)      not null, primary key
#  residential_lease_id     :integer(4)
#  bathrooms                :integer(4)      default(0)
#  bedrooms                 :integer(4)      default(0)
#  bond                     :decimal(16, 3)
#  carport_spaces           :integer(4)      default(0)
#  date_available           :date
#  energy_efficiency_rating :integer(4)
#  energy_star_rating       :integer(4)
#  floor_area               :decimal(16, 3)
#  garage_spaces            :integer(4)      default(0)
#  land_area                :decimal(16, 3)
#  latitude                 :decimal(12, 7)
#  longitude                :decimal(12, 7)
#  number_of_floors         :integer(4)
#  off_street_spaces        :integer(4)      default(0)
#  virtual_tour             :string(255)
#  year_built               :integer(4)

class ResidentialLeaseDetail < ActiveRecord::Base
  belongs_to :residential_lease
  belongs_to :property, :foreign_key => "residential_lease_id"
  attr_accessor :property_type
  attr_accessor :country

  validates_presence_of :date_available
  validates_presence_of :bedrooms, :unless =>:land_and_studio?
  validates_presence_of :bathrooms , :unless =>:land?
  validates_numericality_of :number_of_floors, :greater_than => 0, :only_integer => true, :allow_nil => true
  validates_numericality_of :bond, :greater_than => 0, :allow_nil => true
  validates_numericality_of :year_built, :only_integer => true, :greater_than => 999, :less_than_or_equal_to => Time.now.year, :allow_nil => true

  include DetailHelper

  def land?
    if residential_lease_id
      p = Property.find_by_id(residential_lease_id)
      unless p.blank?
        (p.property_type == "Land" || p.property_type == "Rural" && p.country == "Australia") || (p.property_type == "Section" && p.country == "New Zealand")
      end
    else
      (property_type == "Land" || property_type == "Rural" && country == "Australia") || (property_type == "Section" && country == "New Zealand")
    end
  end

  def land_and_studio?
    if residential_lease_id
      p = Property.find_by_id(residential_lease_id)
      unless p.blank?
        p.property_type = property_type if property_type == "Studio"
        ((p.property_type == "Studio" || p.property_type == "Land" || p.property_type == "Rural") && p.country == "Australia") || (p.property_type == "Section" && p.country == "New Zealand")
      end
    else
      (property_type == "Land" || property_type == "Rural" && country == "Australia") || (property_type == "Section" && country == "New Zealand")
    end
  end

  def get_property_id
    return self.residential_lease_id
  end
end
