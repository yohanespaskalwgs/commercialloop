class ContactAccessibleRelation < ActiveRecord::Base
  belongs_to :agent_contact
  belongs_to :accessible, :class_name => 'AgentUser', :foreign_key => "accessible_by"
end
