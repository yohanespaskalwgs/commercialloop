# == Schema Info
# Schema version: 20090614051807
#
# Table name: active_helps
#
#  id         :integer(4)      not null, primary key
#  data       :text
#  created_at :datetime
#  updated_at :datetime

class ActiveHelp < ActiveRecord::Base
  serialize :data

  def self.first
    self.find_or_create_by_id(1)
  end

  # to retreive data[:residential_sale]
  def data
    read_attribute("data")
  end

  def build_data(help_data_key, help_data)
    self.data = {}
    help_data_key.each_with_index do |k,i|
      self.data[k] = help_data[i]
    end
  end

  private

end
