class ExtraDetailName < ActiveRecord::Base
  belongs_to :office
  has_one :extra_detail
  has_many :extra_options, :foreign_key => :extra_detail_name_id
end
