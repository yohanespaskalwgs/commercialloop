class Export < ActiveRecord::Base
  belongs_to :portal

  has_many :exports_properties
end
