class Address
  attr_reader :unit_number
  attr_reader :street_number
  attr_reader :street
  attr_reader :street_type
  attr_reader :suburb
  attr_reader :state
  attr_reader :country

  def initialize(unit_number, street_number, street, suburb, state, country)
    @unit_number = unit_number
    @street_number = street_number
    @street = street
    @suburb = suburb
    @state = state
    @country = country
  end

  def display_street
    [street_number, @street, @street_type ].reject { |a| a.blank? || a == 'blank' }.join(" ")
  end

  def street_number
    if @street_number
      @street_number.strip
    else
      @street_number
    end
    [@unit_number, @street_number].reject { |a| a.blank? || a == 'blank' }.join(", ")
  end

  def display_suburb
    self.suburb.split(" ").collect! { |x| x.downcase.camelize }.join(" ")
  end

  def to_s
    if country != "NZ"
      "#{display_street}, #{suburb} #{state}"
    else
      [display_street, suburb, Suburb.get_cityRegion(suburb), country ].join(", ")
    end
  end
end
