# == Schema Info
# Schema version: 20090614051807
#
# Table name: countries
#
#  id                       :integer(4)      not null, primary key
#  address1                 :string(255)
#  address2                 :string(255)
#  address3                 :string(255)
#  code                     :string(2)
#  code_iso3                :string(3)
#  currency_code            :string(5)
#  currency_name            :string(255)
#  eer_deviation            :integer(4)
#  eer_lower                :integer(4)
#  eer_upper                :integer(4)
#  energy_efficiency_rating :integer(4)
#  energy_star_rating       :integer(4)
#  flag_content_type        :string(255)
#  flag_file_name           :string(255)
#  flag_file_size           :integer(4)
#  has_states               :boolean(1)      default(TRUE)
#  metrics_building         :string(255)
#  metrics_land             :string(255)
#  metrics_length           :string(255)
#  name                     :string(50)
#  numcode                  :string(3)
#  zoning                   :text
#  flag_updated_at          :datetime

class Country < ActiveRecord::Base
  has_many :states
  has_many :suburbs
  has_many :town_countries
  has_many :energy_efficiency_ratings
  has_many :country_property_type_relationships
  has_many :portal_countries, :dependent => :destroy

  validates_presence_of :name ,:message => ": Country Name can't be blank"
  validates_presence_of :code,:message => ": Country Code can't be blank"
  validates_uniqueness_of :name, :code
  validates_uniqueness_of :code_iso3, :numcode, :allow_nil => true
  validates_numericality_of :eer_lower,:eer_upper,:eer_deviation
  serialize :zoning

  has_attached_file :flag, :styles => { :medium => "400x300>", :thumb => "200x150>" }

  validates_attachment_size :flag, :less_than => 5.megabytes
  validates_attachment_content_type :flag, :content_type => ['image/jpeg', 'image/png']

  LAND_SIZES = ["Acres", "Hectares"]
  BUILDING_SIZES = ["Square Feet", "Square Metres", "Square"]

  LAND_SIZE_OPTIONS = LAND_SIZES.map { |o| [o,o] }
  BUILDING_SIZE_OPTIONS = BUILDING_SIZES.map {|o| [o,o] }

  include NamedParam

  def to_s
    name
  end

  def energy_efficiency_rating
    "#{eer_lower} - #{eer_upper}"
  end

  def validate
    (errors.add("eer_lower","eer lower must be greater than or equal to 0") unless eer_lower >= 0 ) unless eer_lower.blank?
    (errors.add("eer_upper","eer upper must be greater than eer lower") unless eer_lower < eer_upper) if !eer_lower.blank? && !eer_upper.blank?
    (errors.add("eer_deviation","eer deviation must be less than eer upper") unless eer_deviation < eer_upper) if !eer_deviation.blank? && !eer_upper.blank?
    return false if errors.size > 0
  end

end
