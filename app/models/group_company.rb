class GroupCompany < ActiveRecord::Base
  has_many :group_company_relations
  belongs_to :agent_user
  belongs_to :creator, :class_name => 'AgentUser', :foreign_key => "creator_id"
  validates_presence_of :name
end
