  class ContactAlert < ActiveRecord::Base
  belongs_to :agent_contact
  has_many :contact_alert_suburbs, :dependent => :destroy
  has_many :contact_alert_regions, :dependent => :destroy
  has_many :contact_alert_commercial_types, :dependent => :destroy
  has_many :contact_alert_residential_types, :dependent => :destroy
  has_many :contact_alert_residential_buildings, :dependent => :destroy
#  validates_presence_of :alert_type
#  validates_numericality_of :min_price, :max_price, :allow_nil => true

  def add_suburbs(ids)
    ids ||= []
    old_ids = contact_alert_suburbs.map(&:suburb)
    new_ids = ids

    to_delete = old_ids - new_ids
    to_add = new_ids - old_ids

    ContactAlertSuburb.transaction do
      contact_alert_suburbs.all(:conditions => { :suburb => to_delete }).each(&:destroy)
      to_add.each do |id|
        contact_alert_suburbs.create(:suburb => id) unless id.blank?
      end
    end
  end

  def numeric?
    unless self.tenant.blank?
      Float(self.tenant) != nil rescue false
    end
  end

  def add_regions(ids)
    ids ||= []
    old_ids = contact_alert_regions.map(&:region)
    new_ids = ids

    to_delete = old_ids - new_ids
    to_add = new_ids - old_ids

    ContactAlertRegion.transaction do
      contact_alert_regions.all(:conditions => { :region => to_delete }).each(&:destroy)
      to_add.each do |id|
        contact_alert_regions.create(:region => id) unless id.blank?
      end
    end
  end

  def add_commercial_types(ids)
    ids ||= []
    old_ids = contact_alert_commercial_types.map(&:commercial_type)
    new_ids = ids

    to_delete = old_ids - new_ids
    to_add = new_ids - old_ids

    ContactAlertCommercialType.transaction do
      contact_alert_commercial_types.all(:conditions => { :commercial_type => to_delete }).each(&:destroy)
      to_add.each do |id|
        contact_alert_commercial_types.create(:commercial_type => id) unless id.blank?
      end
    end
  end

  def add_residential_types(ids)
    ids ||= []
    old_ids = contact_alert_residential_types.map(&:residential_type)
    new_ids = ids

    to_delete = old_ids - new_ids
    to_add = new_ids - old_ids

    ContactAlertResidentialType.transaction do
      contact_alert_residential_types.all(:conditions => { :residential_type => to_delete }).each(&:destroy)
      to_add.each do |id|
        contact_alert_residential_types.create(:residential_type => id) unless id.blank?
      end
    end
  end

  def add_residential_buildings(ids)
    ids ||= []
    old_ids = contact_alert_residential_buildings.map(&:residential_building)
    new_ids = ids

    to_delete = old_ids - new_ids
    to_add = new_ids - old_ids

    ContactAlertResidentialBuilding.transaction do
      contact_alert_residential_buildings.all(:conditions => { :residential_building => to_delete }).each(&:destroy)
      to_add.each do |id|
        contact_alert_residential_buildings.create(:residential_building => id) unless id.blank?
      end
    end
  end

  def information_of_alert
    case self.listing_type
    when "Commercial"
      return [self.listing_select_status_commercial, self.property_type_list_commercial]
    when "BusinessSale"
      return [self.listing_select_status_commercial, self.property_type_list_commercial]
    when "Residential"
      return [self.listing_select_status_residential, self.property_type_list_residential]
    when "ResidentialSale"
      return [self.listing_select_status_residential, self.property_type_list_residential]
    when "ResidentialLease"
      return [self.listing_select_status_residential, self.property_type_list_residential]
    when "ProjectSale"
      return [self.houseland_status, self.houseland_property_type]
    end
  end

  def property_type_list_residential
    rv = []
    unless self.contact_alert_residential_types.blank?
      self.contact_alert_residential_types.each do |type|
        rv << type.residential_type
      end
    end
    rv.join(", ")
  end

  def property_type_list_commercial
    rv = []
    unless self.contact_alert_commercial_types.blank?
      self.contact_alert_commercial_types.each do |type|
        rv << type.commercial_type
      end
    end
    rv.join(", ")
  end

  def suburb_lists
    rv = []
    unless self.contact_alert_suburbs.blank?
      self.contact_alert_suburbs.each do |suburb|
        rv << suburb.suburb
      end
    end
    rv.join(", ")
  end

  def region_lists
    rv = []
    unless self.contact_alert_regions.blank?
      self.contact_alert_regions.each do |region|
        rv << region.region
      end
    end
    rv.join(", ")
  end

  def availiability_type_mean
    case self.availiability_type
    when "1"
      return "Available"
    when "2"
      return "Sold"
    when "3"
      return "Leased"
    when "4"
      return "Withdrawn"
    when "5"
      return "Underoffer"
    when "6"
      return "Draft"
    when "7"
      return "On Hold"
    end
  end

  def tenant_name
    tenant = ''
    if !self.tenant.blank? && self.numeric?
      tenant = AgentContact.find(self.tenant)
      unless tenant.blank?
        tenant = tenant.full_name
      end
    end
    tenant
  end

  def residential_building_lists
    rv = []
    unless self.contact_alert_residential_buildings.blank?
      self.contact_alert_residential_buildings.each do |res_build|
        rv << res_build.residential_building
      end
    end
    rv.join(", ")
  end

  def self.get_info_contact(x)
    [x.agent_contact.id,x.agent_contact.email]
  end

  def self.advanced_search(listing_type,property_type,business_type,business_category,price_from,price_to,deal_type,availiability,suites,range_from,range_to,type_commercial,type,residential_building,list_status_commercial,commercial_type,list_status_residential,state,region,suburb,agent_internal,external_company,agent_external,yield_from,yield_to,building_from,building_to,land_from,land_to,price_from_commercial,price_to_commercial,price_from_residential,price_to_residential,annual_rental_from,annual_rental_to,floor_from,floor_to,office_from,office_to,warehouse_from,warehouse_to,rent_from,rent_to,lease_from,lease_to,tenant,search_keyword_advanced,tenant_name,start,limit,page,agent_id,office_id,order,sort,dir)
    cond = "agent_contacts.agent_id = '#{agent_id}' AND agent_contacts.office_id = '#{office_id}'"
    cond += " AND contact_alerts.keyword LIKE '%#{search_keyword_advanced}%'" unless search_keyword_advanced.blank?
    unless listing_type == "ProjectSale"
      if ["ResidentialSale", "ResidentialLease", "NewDevelopment", "LandRelease", "HolidayLease"].include?(listing_type)
        cond += " AND contact_alerts.listing_type = 'Residential'"
        cond += " AND contact_alerts.availiability_type = '#{availiability}'" unless availiability.blank?
        cond += " AND contact_alerts.state = '#{state}'" unless state.blank?
        cond += " AND (contact_alerts.yield_from >= '#{yield_from}' OR contact_alerts.yield_from <= '#{yield_from}')" unless yield_from.blank?
        cond += " AND contact_alerts.yield_to >= '#{yield_to}' OR contact_alerts.yield_to <= '#{yield_to}')" unless yield_to.blank?
        cond += " AND (contact_alerts.building_area_from >= '#{building_from}' OR contact_alerts.building_area_from <= '#{building_from}')" unless building_from.blank?
        cond += " AND (contact_alerts.building_area_to >= '#{building_to}' OR contact_alerts.building_area_to <= '#{building_to}')" unless building_to.blank?
        cond += " AND (contact_alerts.land_area_from >= '#{land_from}' OR contact_alerts.land_area_from <= '#{land_from}')" unless land_from.blank?
        cond += " AND (contact_alerts.land_area_to >= '#{land_to}' OR contact_alerts.land_area_to <= '#{land_to}')" unless land_to.blank?
        cond += " AND (contact_alerts.annual_rental_from >= '#{annual_rental_from}' OR contact_alerts.annual_rental_from <= '#{annual_rental_from}')" unless annual_rental_from.blank?
        cond += " AND (contact_alerts.annual_rental_to >= '#{annual_rental_to}' OR contact_alerts.annual_rental_to <= '#{annual_rental_to}')" unless annual_rental_to.blank?
        cond += " AND contact_alerts.listing_select_status_residential = '#{list_status_residential}'" unless list_status_residential.blank?
        cond += " AND (contact_alerts.asking_price_from >= '#{price_from_residential}' OR contact_alerts.asking_price_from <= '#{price_from_residential}')" unless price_from_resiential.blank?
        cond += " AND (contact_alerts.asking_price_to >= '#{price_to_residential}' OR contact_alerts.asking_price_to <= '#{price_to_residential}')" unless price_to_residential.blank?
        unless type.blank?
          cond += " AND contact_alerts.id IN (SELECT contact_alert_residential_types.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_residential_types ON contact_alerts.id = contact_alert_residential_types.contact_alert_id WHERE contact_alert_residential_types.residential_type IN (#{type.map(&:inspect).join(', ')}))"
        end
        unless residential_building.blank?
          cond += " AND contact_alerts.id IN (SELECT contact_alert_residential_buildings.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_residential_buildings ON contact_alerts.id = contact_alert_residential_buildings.contact_alert_id WHERE contact_alert_residential_buildings.residential_building IN (#{residential_building.map(&:inspect).join(', ')}))"
        end
        unless region.blank? || region == "Please select"
          region_split = region.split(",")
          region_split.map_with_index{|x, i| region_split.delete_at(i) if x == ""  }
          region_val = region_split.map{|x| "\'#{x}\'"}
          region_fix = region_val.join(',')
          cond += " AND contact_alerts.id IN (SELECT contact_alert_regions.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_regions ON contact_alerts.id = contact_alert_regions.contact_alert_id WHERE contact_alert_regions.region IN (#{region_fix}))"
        end
        unless suburb.blank? || suburb == "Please select"
          suburb_split = suburb.split(",")
          suburb_split.map_with_index{|x, i| suburb_split.delete_at(i) if x == ""  }
          suburb_val = suburb_split.map{|x| "\'#{x}\'"}
          suburb_fix = suburb_val.join(',')
          cond += " AND contact_alerts.id IN (SELECT contact_alert_suburbs.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_suburbs ON contact_alerts.id = contact_alert_suburbs.contact_alert_id WHERE contact_alert_suburbs.suburb IN (#{suburb_fix}))"
        end
        page = start.to_i / limit.to_i + 1
        ContactAlert.paginate(:all, :include => [:agent_contact, :contact_alert_suburbs, :contact_alert_regions, :contact_alert_commercial_types], :conditions => cond, :page =>page, :per_page => limit.to_i, :order => order)
      elsif ["Commercial", "CommercialBuilding"].include?(listing_type)
        cond += " AND contact_alerts.listing_type = 'Commercial'"
        cond += " AND contact_alerts.availiability_type = '#{availiability}'" unless availiability.blank?
        cond += " AND contact_alerts.commercial_property_type = '#{commercial_type}'" unless commercial_type.blank?
        cond += " AND contact_alerts.state = '#{state}'" unless state.blank?
        cond += " AND (contact_alerts.yield_from >= '#{yield_from}' OR contact_alerts.yield_from <= '#{yield_from}')" unless yield_from.blank?
        cond += " AND contact_alerts.yield_to >= '#{yield_to}' OR contact_alerts.yield_to <= '#{yield_to}')" unless yield_to.blank?
        cond += " AND (contact_alerts.building_area_from >= '#{building_from}' OR contact_alerts.building_area_from <= '#{building_from}')" unless building_from.blank?
        cond += " AND (contact_alerts.building_area_to >= '#{building_to}' OR contact_alerts.building_area_to <= '#{building_to}')" unless building_to.blank?
        cond += " AND (contact_alerts.land_area_from >= '#{land_from}' OR contact_alerts.land_area_from <= '#{land_from}')" unless land_from.blank?
        cond += " AND (contact_alerts.land_area_to >= '#{land_to}' OR contact_alerts.land_area_to <= '#{land_to}')" unless land_to.blank?
        cond += " AND (contact_alerts.annual_rental_from >= '#{annual_rental_from}' OR contact_alerts.annual_rental_from <= '#{annual_rental_from}')" unless annual_rental_from.blank?
        cond += " AND (contact_alerts.annual_rental_to >= '#{annual_rental_to}' OR contact_alerts.annual_rental_to <= '#{annual_rental_to}')" unless annual_rental_to.blank?
        cond += " AND contact_alerts.listing_select_status_commercial = '#{list_status_commercial}'" unless list_status_commercial.blank?
        cond += " AND (contact_alerts.asking_price_from >= '#{price_from_commercial}' OR contact_alerts.asking_price_from <= '#{price_from_commercial}')" unless price_from_commercial.blank?
        cond += " AND (contact_alerts.asking_price_to >= '#{price_to_commercial}' OR contact_alerts.asking_price_to <= '#{price_to_commercial}')" unless price_to_commercial.blank?
        cond += " AND contact_alerts.internal_agent = '#{agent_internal}'" unless agent_internal.blank?
        cond += " AND contact_alerts.external_company = '#{external_company}'" unless external_company.blank?
        cond += " AND contact_alerts.external_agent = '#{agent_external}'" unless agent_external.blank?
        cond += " AND (contact_alerts.total_floor_area_from >= '#{floor_from}' OR contact_alerts.total_floor_area_from <= '#{floor_from}')" unless floor_from.blank?
        cond += " AND (contact_alerts.total_floor_area_to >= '#{floor_to}' OR contact_alerts.total_floor_area_to <= '#{floor_to}')" unless floor_to.blank?
        cond += " AND (contact_alerts.office_from >= '#{office_from}' OR contact_alerts.office_from <= '#{office_from}')" unless office_from.blank?
        cond += " AND (contact_alerts.office_to >= '#{office_to}' OR contact_alerts.office_to <= '#{office_to}')" unless office_to.blank?
        cond += " AND (contact_alerts.warehouse_from >= '#{warehouse_from}' OR contact_alerts.warehouse_from <= '#{warehouse_from}')" unless warehouse_from.blank?
        cond += " AND (contact_alerts.warehouse_to = '#{warehouse_to}' OR contact_alerts.warehouse_to <= '#{warehouse_to}')" unless warehouse_to.blank?
        cond += " AND (contact_alerts.rent_from >= '#{rent_from}' OR contact_alerts.rent_from <= '#{rent_from}')" unless rent_from.blank?
        cond += " AND (contact_alerts.rent_to >= '#{rent_to}' OR contact_alerts.rent_to <= '#{rent_to}')" unless rent_to.blank?
        cond += " AND (contact_alerts.lease_expiry_from >= '#{lease_from}' OR contact_alerts.lease_expiry_from <= '#{lease_from}')" unless lease_from.blank?
        cond += " AND (contact_alerts.lease_expiry_to >= '#{lease_to}' OR contact_alerts.lease_expiry_to <= '#{lease_to}')" unless lease_to.blank?
        cond += " AND contact_alerts.tenant = '#{tenant}'" unless tenant_name.blank?
        cond += " AND contact_alerts.id IN (SELECT contact_alert_commercial_types.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_commercial_types ON contact_alerts.id = contact_alert_commercial_types.contact_alert_id WHERE contact_alert_commercial_types.commercial_type IN (#{type_commercial.map(&:inspect).join(', ')}))" unless type_commercial.blank?
        unless region.blank? || region == "Please select"
          region_split = region.split(",")
          region_split.map_with_index{|x, i| region_split.delete_at(i) if x == ""  }
          region_val = region_split.map{|x| "\'#{x}\'"}
          region_fix = region_val.join(',')
          cond += " AND contact_alerts.id IN (SELECT contact_alert_regions.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_regions ON contact_alerts.id = contact_alert_regions.contact_alert_id WHERE contact_alert_regions.region IN (#{region_fix}))"
        end
        unless suburb.blank? || suburb == "Please select"
          suburb_split = suburb.split(",")
          suburb_split.map_with_index{|x, i| suburb_split.delete_at(i) if x == ""  }
          suburb_val = suburb_split.map{|x| "\'#{x}\'"}
          suburb_fix = suburb_val.join(',')
          cond += " AND contact_alerts.id IN (SELECT contact_alert_suburbs.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_suburbs ON contact_alerts.id = contact_alert_suburbs.contact_alert_id WHERE contact_alert_suburbs.suburb IN (#{suburb_fix}))"
        end
        page = start.to_i / limit.to_i + 1
        ContactAlert.paginate(:all, :include => [:agent_contact, :contact_alert_suburbs, :contact_alert_regions, :contact_alert_commercial_types], :conditions => cond, :page =>page, :per_page => limit.to_i, :order => order)
      elsif listing_type == "BusinessSale"
        cond += " AND contact_alerts.listing_type = 'BusinessSale'"
        cond += " AND contact_alerts.business_type = '#{business_type}'" unless business_type.blank?
        cond += " AND contact_alerts.business_category = '#{business_category}'" unless business_category.blank?
        cond += " AND (contact_alerts.price_range_from >= '#{price_from}' OR contact_alerts.price_range_from <= '#{price_from}')" unless price_from.blank?
        cond += " AND (contact_alerts.price_range_to >= '#{price_to}' OR contact_alerts.price_range_to <= '#{price_to}')" unless price_to.blank?
        cond += " AND contact_alerts.houseland_status = '#{deal_type}'" unless deal_type.blank?
        cond += " AND contact_alerts.suites = '#{suites}'" unless suites.blank?
        cond += " AND (contact_alerts.sqm_range_from >= '#{range_from}' OR contact_alerts.sqm_range_from <= '#{range_from}')" unless range_from.blank?
        cond += " AND (contact_alerts.sqm_range_to >= '#{range_to}' OR contact_alerts.sqm_range_to <= '#{range_to}')" unless range_to.blank?
        page = start.to_i / limit.to_i + 1
        ContactAlert.paginate(:all, :include => [:agent_contact], :conditions => cond, :page =>page, :per_page => limit.to_i, :order => order)
      end
    else
      cond += " AND contact_alerts.listing_type = 'ProjectSale'" unless listing_type.blank?
      cond += " AND contact_alerts.houseland_property_type = '#{property_type}'" unless property_type.blank? || property_type == "Please select"
      cond += " AND (contact_alerts.price_range_from >= '#{price_from}' OR contact_alerts.price_range_from <= '#{price_from}')" unless price_from.blank?
      cond += " AND (contact_alerts.price_range_to >= '#{price_to}' OR contact_alerts.price_range_to <= '#{price_to}')" unless price_to.blank?
      cond += " AND contact_alerts.houseland_status = '#{deal_type}'" unless deal_type.blank?
      cond += " AND contact_alerts.suites = '#{suites}'" unless suites.blank?
      cond += " AND (contact_alerts.sqm_range_from >= '#{range_from}' OR contact_alerts.sqm_range_from <= '#{range_from}')" unless range_from.blank?
      cond += " AND (contact_alerts.sqm_range_to >= '#{range_to}' OR contact_alerts.sqm_range_to <= '#{range_to}')" unless range_to.blank?
      page = start.to_i / limit.to_i + 1
      ContactAlert.paginate(:all, :include => [:agent_contact], :conditions => cond, :page =>page, :per_page => limit.to_i, :order => order)
    end
  end

  def self.check_data(alert)
    land_area = ""
    floor_area = ""
    price = ""

    unless alert.land_area_from.blank?
      land_area = alert.land_area_from
    else
      land_area = "N/A"
    end
    unless alert.land_area_to.blank?
      land_area = land_area + " - " + alert.land_area_to
    else
      land_area = land_area + " - " + "N/A"
    end

    unless alert.building_area_from.blank?
      floor_area = alert.building_area_from
    else
      floor_area = "N/A"
    end
    unless alert.building_area_to.blank?
      floor_area = floor_area + " - " + alert.building_area_to
    else
      floor_area = floor_area + " - " + "N/A"
    end

    unless alert.asking_price_from.blank?
      price = ActionController::Base.helpers.number_to_currency(alert.asking_price_from.to_i, :precision => 0)
    else
      price = "N/A"
    end
    unless alert.asking_price_to.blank?
      price = price + " - " + ActionController::Base.helpers.number_to_currency(alert.asking_price_to.to_i, :precision => 0)
    else
      price = price + " - " + "N/A"
    end

    return [land_area, floor_area, price]
  end

end
