# == Schema Info
# Schema version: 20090224153816
#
# Table name: features
#
#  id            :integer(4)      not null, primary key
#  office_id     :integer(4)
#  name          :string(255)
#  property_type :string(255)
#  type          :string(255)

class GeneralFeature < Feature
end
