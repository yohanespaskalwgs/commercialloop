class CountryPropertyTypeRelationship < ActiveRecord::Base
  belongs_to :country
  belongs_to :property_type
end
