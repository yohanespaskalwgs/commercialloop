class PropertyContactEnquiry < ActiveRecord::Base
  belongs_to :property
  belongs_to :agent_contact
end
