class Portal < ActiveRecord::Base
  has_many :portal_countries, :dependent => :destroy
  has_many :portal_autosubcribes, :dependent => :destroy
  has_many :property_portal_exports, :dependent => :destroy

  def availability
    arr_availability = []
    self.portal_countries.each do |p_c|
      arr_availability << p_c.country.code
    end

    return arr_availability.join(", ")
  end

  def self.child_portal
    return ["Stayz.com.au","Holidayview.com.au","Realcommercial.com.au","Realbusinesses.com.au","Domainbusiness.com.au","Realestate1commercial.com.au","Commercialview.com.au","Businessview.com.au"]
  end
end
