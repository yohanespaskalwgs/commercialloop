class LandRelease < Property
  has_one :detail, :class_name => 'LandReleaseDetail'
  validates_associated :detail
end
