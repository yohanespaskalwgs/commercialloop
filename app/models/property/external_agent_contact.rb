class ExternalAgentContact < AgentContact
  belongs_to :property

  def full_name
    "#{first_name} #{last_name}"
  end
end