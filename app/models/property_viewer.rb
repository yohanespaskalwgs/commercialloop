class PropertyViewer < ActiveRecord::Base
  belongs_to :property
  belongs_to :property_viewer_type
  validates_presence_of :property_viewer_type_id
end
