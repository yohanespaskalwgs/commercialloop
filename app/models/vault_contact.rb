class VaultContact < ActiveRecord::Base
  belongs_to :vault
  belongs_to :agent_contact
end
