# == Schema Info
# Schema version: 20090331151240
#
# Table name: message_comments
#
#  id           :integer(4)      not null, primary key
#  commenter_id :integer(4)
#  message_id   :integer(4)
#  comment      :text
#  rating       :integer(4)
#  created_at   :datetime
#  updated_at   :datetime

class MessageComment < ActiveRecord::Base
  belongs_to :message, :counter_cache => true
  belongs_to :commenter, :class_name => "User"
  white_list :only => [:message]

  validates_presence_of :comment

  has_one :file, :class_name => 'MessageCommentFile', :as => :attachable, :dependent => :destroy
  attr_accessor :uploaded_file_data, :developer_internal

  named_scope :comments_without_teammember, :conditions => 'internal_chat is NULL'

  def save_with_file
    file = MessageCommentFile.new
    begin
      self.transaction do
        if uploaded_file_data && uploaded_file_data.size > 0
          file.uploaded_data = uploaded_file_data
          file.thumbnails.clear
          file.save!
          self.file = file
        end
        save!
      end
    rescue
      if file.errors.on(:size)
        errors.add_to_base("Uploaded image is too big (100 MB max).")
      end
      if file.errors.on(:content_type)
        errors.add_to_base("Uploaded image content-type is not valid.")
      end
      false
    end
  end

  def editable_by?(user)
    # comment with a rating is remark of archive message
    self.rating.nil? && self.commenter == user
  end
end
