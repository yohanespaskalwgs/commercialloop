# == Schema Info
# Schema version: 20090331151240
#
# Table name: message_recipients
#
#  id         :integer(4)      not null, primary key
#  message_id :integer(4)
#  user_id    :integer(4)
#  unread     :boolean(1)      default(TRUE)
#  created_at :datetime
#  updated_at :datetime

class MessageRecipient < ActiveRecord::Base
  validates_uniqueness_of :user_id, :scope => :message_id

  belongs_to :message, :counter_cache => true
  belongs_to :user
end
