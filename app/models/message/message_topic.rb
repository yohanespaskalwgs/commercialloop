# == Schema Info
# Schema version: 20090331151240
#
# Table name: message_topics
#
#  id          :integer(4)      not null, primary key
#  category_id :integer(4)
#  name        :string(255)
#  created_at  :datetime
#  updated_at  :datetime

class MessageTopic < ActiveRecord::Base
  validates_presence_of :name
  validates_uniqueness_of :name,  :case_sensitive => false
  belongs_to :category, :class_name => "MessageCategory", :foreign_key => "category_id"

  has_many :admin_messages, :class_name => "AdminMessage", :foreign_key => "topic_id"

  def can_delete?
    admin_messages.size == 0
  end
end
