class MessageNotification < ActionMailer::Base
  include ActionController::UrlWriter

  def new_message(user, message)
    setup_email(user, get_sender_office_name(message.sender))
    @subject    += "#{message.sender.full_name} sent you a message: #{message.title}"
    @body[:message] = message
    @body[:url]  = get_message_url(user, message)
  end

  def new_comment(user, message, comment)
    setup_email(user, get_sender_office_name(comment.commenter))
    @subject    += "#{comment.commenter.full_name} posted a comment on message: #{message.title}"
    @body[:message] = message
    @body[:comment] = comment
    @body[:url]  = get_message_url(user, message, "message_comment_#{comment.id}", comment)
  end

  def notify_inactive(user,office)
    setup_email(user,get_sender_office_name(user))
    @subject += "Office Inactive"
    @body[:office] = office
  end

  protected
  def setup_email(user, sender = "Commercial Loop")
    @recipients  = "#{user.email}"
    @from        = "#{sender} <noreply@commercialloopcrm.com.au>"
    @subject     = "[Notification] "
    @sent_on     = Time.now
    @body[:user] = user
    @content_type = "text/html"

    default_url_options[:host] = user.site_url.sub('http://', '') unless user.site_url.blank?
  end

  def get_sender_office_name(user)
    case user
    when DeveloperUser
      user.developer.name
    when AgentUser
      user.office.full_name
    end
  end

  def get_message_url(user, message, anchor = nil, comment = nil)
    case user
    when DeveloperUser
      unless comment.nil?
        if comment.internal_chat == true
          path = developer_internal_chat_url(user.developer.id, message.id, :anchor => anchor)
        else
          path = developer_message_url(user.developer, message, :anchor => anchor)
        end
      else
        path = developer_message_url(user.developer, message, :anchor => anchor)
      end
    when AgentUser
      path = agent_message_url(user.office.agent, message, :anchor => anchor)
    else # AdminUser
      unless comment.nil?
        if comment.internal_chat == true
          path = internal_chat_url(message.id, :anchor => anchor)
        else
          path = message_url(message, :anchor => anchor)
        end
      else
        path = message_url(message, :anchor => anchor)
      end
    end
  end
end
