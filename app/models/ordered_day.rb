# == Schema Info
# Schema version: 20090614051807
#
# Table name: ordered_days
#
#  id               :integer(4)      not null, primary key
#  rental_season_id :integer(4)
#  date             :date

class OrderedDay < ActiveRecord::Base
  belongs_to :rental_season
end
