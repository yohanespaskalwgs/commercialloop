class EmailConfigurationSearch < ActiveRecord::Base
  belongs_to :user
  belongs_to :office
  belongs_to :agent
end
