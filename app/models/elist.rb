class Elist < ActiveRecord::Base
  belongs_to :office

  def self.show_elist(conditions)
    sql = [" SELECT `e`.id,`e`.title,`e`.created_at,`e`.sent_at, (SELECT count(r.id) FROM `ecampaign_tracks` r WHERE `r`.type_ecampaign_id = e.id And type_ecampaign='elist') as recipient "]
    sql[0] += ", (SELECT count(op.id) FROM `ecampaign_tracks` op WHERE `op`.type_ecampaign_id = e.id And type_ecampaign='elist' And `op`.email_opened = 1) as opened "
    sql[0] += ", (SELECT count(bon.id) FROM `ecampaign_tracks` bon WHERE `bon`.type_ecampaign_id = e.id And type_ecampaign='elist' And `bon`.email_bounced = 1) as bounced "
    sql[0] += ", (SELECT sum(cl.clicked) FROM `ecampaign_clicks` cl WHERE `cl`.type_ecampaign_id = e.id And type_ecampaign='elist') as clicked "
    sql[0] += ", (SELECT sum(fw.email_forwarded) FROM `ecampaign_tracks` fw WHERE `fw`.type_ecampaign_id = e.id And type_ecampaign='elist') as forwarded "
    sql[0] += ", (SELECT count(us.id) FROM `ecampaign_unsubcribes` us WHERE `us`.type_ecampaign_id = e.id And type_ecampaign='elist') as unsubcribed "
    sql[0] += " FROM `elists` e "
    sql_part1 = [" WHERE ( #{conditions.to_s} ) ORDER BY `e`.id DESC"]
    sql[0] += sql_part1.to_s
    return sql
  end
end
