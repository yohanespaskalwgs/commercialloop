class CompanyAlertsController < ApplicationController
  layout 'agent'

  before_filter :login_required
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation

  def index
     @company_alert = CompanyAlert.new
  end

  def show
  end

  def new
  end

  def edit
    @all_suburbs = @all_suburbs_list =''
    @company_alert = CompanyAlert.find(params[:id])
    @company_alert.company_alert_suburbs.map{|cat|
      @all_suburbs = @all_suburbs + '#' + cat.suburb.to_s
      @all_suburbs_list = @all_suburbs_list + '<span id="'+cat.suburb.to_s+'" style="margin-right:20px;">'+cat.suburb.to_s+' <a href="javascript:remove_suburb(\''+cat.suburb.to_s+'\')">x</a></span>'
    }
  end

  def create
    @company_alert = CompanyAlert.new(params[:company_alert])

    respond_to do |format|
      if @company_alert.save
        prenote = "Requirements Type = #{params[:company_alert][:alert_type]},"
        prenote += "Listing Type = #{params[:company_alert][:listing_type]}," unless params[:company_alert][:listing_type].blank?
        prenote +="Property Type = #{params[:company_alert][:property_type]}," unless params[:company_alert][:property_type].blank?
        prenote += "Min Bedroom = #{params[:company_alert][:min_bedroom]}," unless params[:company_alert][:min_bedroom].blank?
        prenote += "Min Bathroom = #{params[:company_alert][:min_bathroom]}," unless params[:company_alert][:min_bathroom].blank?
        prenote += "Min carspace = #{params[:company_alert][:min_carspace]}," unless params[:company_alert][:min_carspace].blank?
        prenote += "Min Price = #{params[:company_alert][:min_price]}," unless params[:company_alert][:min_price].blank?
        prenote += "Max Price = #{params[:company_alert][:max_price]}," unless params[:company_alert][:max_price].blank?
        prenote += "Suburbs = #{params[:company_alert][:suburb]}," unless params[:company_alert][:suburb].blank?

        @company_alert.agent_company.create_note_for_company(1, prenote, current_user.id)
        add_multiple_value
        flash[:notice] = 'Company Alert was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        feed_multiple_value
        format.html { render :action => "index" }
        format.xml  { render :xml => @company_alert.errors}
      end
    end
  end

  def update
   @company_alert = CompanyAlert.find(params[:id])

    respond_to do |format|
      if @company_alert.update_attributes(params[:company_alert])
        add_multiple_value
        flash[:notice] = 'Company Alert was successfully updated.'
        format.html { redirect_to :action => 'index' }
        format.xml  { head :ok }
      else
        feed_multiple_value
        format.html { render :action => "edit" }
        format.xml  { render :xml => @company_alert.errors}
      end
    end
  end

  def destroy
    @company_alert = CompanyAlert.find(params[:id])
    @company_alert.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
      format.xml  { head :ok }
    end
  end

  def feed_multiple_value
    @all_suburbs = @all_suburbs_list =''
    return if params[:all_suburbs].blank?
      @suburbs_data = params[:all_suburbs].split('#')
      @suburbs_data.map{|cat|
        unless cat == ''
          @all_suburbs = @all_suburbs + '#' + cat.to_s
          @all_suburbs_list = @all_suburbs_list + '<span id="'+cat.to_s+'" style="margin-right:20px;">'+cat.to_s+' <a href="javascript:remove_suburb(\''+cat.to_s+'\')">x</a></span>'
        end
      }
  end

  def add_multiple_value
    return @company_alert.company_alert_suburbs.destroy_all if params[:all_suburbs].blank?
    @ids = params[:all_suburbs].split('#')
    @company_alert.add_suburbs(@ids)
  end

  def prepare_creation
    @agent_company = AgentCompany.find(params[:agent_company_id])
    @range = (1..10).map{|x|[x,x]}.unshift(["Please select", ''])
    @alert_type = [['Please select', nil], ['Newly Listed Properties', 'Newly Listed Properties'], ['Updated Properties', 'Updated Properties'], ['Open Inspections', 'Open Inspections'], ['Sold/Leased Properties', 'Sold/Leased Properties'], ['Auctions', 'Auctions'], ['Latest News', 'Latest News']]
    @listing_type = [['Please select', nil], ['Business Sale', 'BusinessSale'], ['Commercial Sale/Lease', 'Commercial'], ['Residential Lease', 'ResidentialLease'],['Residential Sale', 'ResidentialSale'], ['Holiday Lease', 'HolidayLease'], ['Project Sale', 'ProjectSale']]
    @ptypes = [['Please select', nil]]
    @alerts = @agent_company.company_alerts.paginate(:all, :order => "`updated_at` DESC", :page =>params[:page], :per_page => 10) unless @agent_company.company_alerts.blank?
  end

  def populate_prop
    types = []
    unless params[:ltype].blank?
      country = Country.find_by_name(current_office.country)
      country_ptypes_id = []
      country_ptypes = CountryPropertyTypeRelationship.find_all_by_country_id(country.id,:select =>"property_type_id")
      country_ptypes.each do |c|
        country_ptypes_id << c.property_type_id
      end
      condition=[' 1=1']
      unless country_ptypes_id.blank?
        condition[0] += " AND id IN(?)"
        condition << country_ptypes_id
      end
      category = "Residential" if params[:ltype].include?('Residential') || params[:ltype].include?("Project")
      category = "Holiday" if params[:ltype].include?('Holiday')
      category = "Commercial" if params[:ltype].include?('Commercial')
      category = "Business" if params[:ltype].include?('Business')
      unless category.blank?
        condition[0] += " AND category like(?)"
        condition << category
      end
      types = PropertyType.find(:all,:conditions =>condition).map{|t| [t.name,t.name] }
    end

    return(render(:json => types))
  end
end
