class MessageCategoriesDController < ApplicationController
  layout 'developer'
  require_role 'DeveloperUser'
  prepend_before_filter :get_developer
  before_filter :get_category, :only => %w(edit destroy update)

  def index
    @categories = @developer.message_categories.paginate(:page => params[:page])
  end

  def new
    @category = @developer.message_categories.new
  end

  def create
    @category =@developer.message_categories.new(params[:message_category])
    if @category.save
      flash[:notice] = "Category added."
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def update
    if @category.update_attributes(params[:message_category])
      flash[:notice] = "Category updated."
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def destroy
    @category.destroy if @category.can_delete?
    respond_to do |format|
      format.js {
        render :update do |page|
          page.visual_effect :highlight, "category_#{@category.id}"
          page["category_#{@category.id}"].fadeOut
        end
      }
    end
  end

  private
  def get_developer
    @developer = Developer.find(params[:developer_id])
    @body_id = "messages"
  end

  def get_category
    @category = @developer.message_categories.find(params[:id])
  end
end
