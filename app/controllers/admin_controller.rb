class AdminController < ApplicationController
  layout 'admin'
  prepend_before_filter :login_required
  require_role 'AdminUser'
  skip_before_filter :verify_authenticity_token, :only => [:search]

  def index
    @body_id = 'admin_dashboard'
    #TODO: should make this call in a trigger or background job periodically
    PropertySummary.generate_summary
  end

  def clients
    @clients = Agent.find_where(:all) do |agent|
      agent.developer_id == params[:developer] if params[:developer]
      agent.country == params[:country] if params[:country]
      agent.status == params[:status] if params[:status]
      agent.id == params[:client] if params[:client]
    end
  end

  def offices
    @offices = Office.find_where(:all) do |office|
      office.agent === Developer.find(params[:developer]).agents if params[:developer]
      office.agent_id == params[:client] if params[:client]
      office.id == params[:office] if params[:office]
      office.status == params[:status] if params[:status]
    end
  end

  def toggle_office_status
    office = Office.find(params[:id])
    sp = office.subscription_plan('one_monthly')
    sm = SubscriptionManager
    history = SubscriptionHistory.find(:last, :conditions => ["subscription_id = ?", office.subscription.id], :order => "id ASC")
    # activate office
    if office.status == "inactive"
      office.update_attribute(:status, "active")
      office.subscription.update_attributes({:starts_on => Time.now, :status => "ok", :next_payment_date => sp['trial_end_date'], :ends_on => nil}) unless office.subscription.blank?
    # deactivate office
    else
      # condition if user has already subscribed
      unless history.blank?
        if history.txn_type == "recurring_payment"
          begin
            sm.unsubscribe(office.subscription.id)
            office.subscription.update_attributes({:ends_on => office.subscription.next_payment_date.yesterday, :canceled_at => Time.now, :next_payment_date => nil, :status => "canceled"}) unless office.subscription.blank?
          rescue
          end
          office.update_attribute(:status, "inactive")
          flash[:notice] = "Your office status has successfully updated."
        else
          flash[:notice] = "Failed to update office status. Transcaction still pending PayPal (not subscribed yet)."
        end
        redirect_to :action => "offices"
        return
      end
      # condition if office was just created
      unless office.subscription.blank?
        if office.subscription.status == "pending"
          office.subscription.update_attributes({:starts_on => Time.now, :ends_on => sp['next_payment_date'].yesterday, :canceled_at => Time.now, :next_payment_date => nil, :status => "canceled"})
        end
      end
      office.update_attribute(:status, "inactive")
      User.find(:all,:conditions =>["office_id = ?",office.id]).each do |user|
        if user['type'] == "DeveloperUser" && user.full_access?
          MessageNotification.deliver_notify_inactive(user,office)
        end
      end
    end
    flash[:notice] = "Your office status has successfully updated."
    redirect_to :action => "offices"
  end

  # Search Clients
  def search
    agents = Agent.search(params[:q])
    jsons = []
    agents.each{|a| (jsons << {'id' => a.id, 'name' => a.name, 'href' => clients_admin_path(:client => a.id)}) unless a.blank? } unless agents.nil?
    text = {"results" => jsons, "total" => agents.length }
    render :json => text
  end

  def trigger
    unless params[:id].blank?
      properties_or_agents = nil
      msg = ""
      case params[:type]
      when "1"
        msg = "Available & Under Offer Listings"
        condition = "and (status = 1 or status = 5)"
      when "2"
        msg = "Sold Listings"
        condition = "and status = 2"
      when "3"
        msg = "Leased Listings"
        condition = "and status = 3"
      when "4"
        msg = "Team Members"
      end
      if params[:type] == "4"
        properties_or_agents = AgentUser.find_by_sql("SELECT * FROM users WHERE office_id = #{params[:id]} and (deleted_at IS NULL)")
      else
        properties_or_agents = Property.find_by_sql("SELECT * FROM properties WHERE office_id = #{params[:id]} and (deleted_at IS NULL) #{condition}")
      end
      send_amount = (properties_or_agents.size.to_f/100).ceil
      (0..(send_amount-1)).each do |c|
        limit = c*100
        next_send = c*10
        time = (c == 0)? 1.seconds.from_now.getutc : next_send.minutes.from_now.getutc
        if params[:type] == "4"
          Delayed::Job.enqueue(TriggerApi.new("User", nil, "office_id = #{params[:id]} and (deleted_at IS NULL) LIMIT #{limit}, 100"), 3, time)
        else
          conditions = "office_id = #{params[:id]} and (deleted_at IS NULL) #{condition} LIMIT #{limit}, 100"
          Delayed::Job.enqueue(TriggerApi.new("Property", nil, conditions), 3, time)
        end
      end
      flash[:notice] = "Your #{msg} has successfully triggered"
    end
    redirect_to :action => "offices"
  end
end
