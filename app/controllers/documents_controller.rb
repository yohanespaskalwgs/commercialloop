class DocumentsController < ApplicationController
  layout "vault"
  before_filter :prepare_agent_and_office
  skip_before_filter :login_required
  skip_before_filter :require_accepted
  skip_before_filter :get_agent_and_office, :get_property
  skip_before_filter :verify_authenticity_token

  def index
    @property = Property.find(params[:residential_project_id])
    @pdf = Attachment.find(:all, :conditions => ["attachable_id = ? AND attachable_type = ? AND type = ? AND moved_to_s3 = ? AND position = ?", "#{@property.id}", "Property", "Brochure", true, "16"])
    @floorplans = Attachment.find(:all, :conditions => ["attachable_id = ? AND attachable_type = ? AND type = ? AND thumbnail = ?", "#{@property.id}", "Property", "Floorplan", "thumb"])
  end

   private

  def prepare_agent_and_office
    @agent_contact = AgentContact.find(session[:contact_login])
    @agent = @agent_contact.agent
    @office = @agent_contact.office
  end
end
