class EcampaignsController < ApplicationController
  before_filter :login_required, :except => [:forward, :send_forward, :send_ecampaign, :get_image, :visit_link, :visit_property_link, :preview, :unsubcribe]
  before_filter :get_agent_and_office
  skip_before_filter :verify_authenticity_token, :except => [:index, :destroy,:forward, :sample, :send_forward, :send_ecampaign, :save_ecampaign]
  layout "agent"
  include EcampaignHelper

  def index
  end

  def eproperty_update
    session["ecampaign_id"] = session["ecampaign_status"] = session["ecampaign_layout"] = session["property_soldlease_checkbox"] = session["ecampaign_data"] = session["link_order"] = session["info_links"] = session["web_links"] = session["general_links"] = session["social_links"] = session["featured_property_ids"] = session["property_checkbox"] = session["ecampaign_colour"] = session["ecampaign_group_id"] = session["contact_checkbox"] = session["contact_checkbox_all"] = session["property_rental_checkbox"] = nil
    session["office_ids"] = session["listing_type_ids"] = session["property_type_ids"] = session["office_soldlease_ids"] = session["listing_type_soldlease_ids"] = session["property_type_soldlease_ids"] = session["rental_listing_type_ids"] = session["rental_property_type_ids"] = session["office_rental_ids"] = nil

    sql = Ecampaign.show_ecampaign("e.office_id = #{@office.id} ")
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    @ecampaigns = @mysql_result.all_hashes
    @mysql_result.free
  end

  def stats
    sql = Ecampaign.show_ecampaign("e.id = #{params[:id]} ")
    ecampaign_stats = Ecampaign.find_by_sql(sql.to_s)
    @ecampaign_stats = ecampaign_stats[0]
    @ecampaign_property_clicks = EcampaignClick.find(:all, :conditions => "`ecampaign_id` = #{params[:id]} And `property_id` IS NOT NULL")
    @ecampaign_link_clicks = EcampaignClick.find(:all, :conditions => "`ecampaign_id` = #{params[:id]} And `property_id` IS NULL")

    unless params[:link_id].blank?
      @ecampaign_click = EcampaignClick.find(params[:link_id])
      unless @ecampaign_click.property_id.blank?
         p = Property.find(@ecampaign_click.property_id)
         @destination = p.address
      else
        @destination = @ecampaign_click.destination_name
      end
      @ecampaign_detail_clicks = EcampaignClickDetail.find(:all, :select => "DISTINCT(`agent_contact_id`), ecampaign_click_id", :conditions=>"ecampaign_click_id = #{params[:link_id]}")
    end

    unless params[:type].blank?
      @stats_type = params[:type]
      conditions = "ecampaign_id = #{params[:id]}"
      conditions +=" And email_bounced=1" if params[:type] == "bounced"
      conditions +=" And email_opened=1" if params[:type] == "views"
      conditions +=" And email_forwarded=1" if params[:type] == "forward"
      if params[:type] == "unsubcribe"
        @ecampaign_tracks = EcampaignUnsubcribe.find(:all, :select => "DISTINCT(`agent_contact_id`), ecampaign_id", :conditions=> "ecampaign_id = #{params[:id]}")
      else
        @ecampaign_tracks = EcampaignTrack.find(:all, :select => "DISTINCT(`agent_contact_id`), ecampaign_id", :conditions=> conditions)
      end
    end
  end

  def edit_ecampaign
    require 'json'
    ec = Ecampaign.find_by_id(params[:id])
    if ec.stored_data.blank?
      flash[:notice] = 'Eproperty Update data doesn\'t found.'
      redirect_to agent_office_ecampaigns_path(@agent, @office)
    else
      set_data(JSON.parse(ec.stored_data))
      session["ecampaign_id"] = params[:id]
      redirect_to choose_layout_agent_office_ecampaigns_path(@agent, @office)
    end
  end

  def view_and_send
    require 'json'
    ec = Ecampaign.find_by_id(params[:id])
    if ec.stored_data.blank?
      flash[:notice] = 'Eproperty Update data doesn\'t found.'
      redirect_to agent_office_ecampaigns_path(@agent, @office)
    else
      set_data(JSON.parse(ec.stored_data))
      session["ecampaign_id"] = params[:id]
      redirect_to view_sample_agent_office_ecampaigns_path(@agent, @office)
    end
  end

  def duplicate_ecampaign
    require 'json'
    ec = Ecampaign.find_by_id(params[:id])
    if ec.blank?
      flash[:notice] = 'Eproperty Update data doesn\'t found.'
    else
      set_data(JSON.parse(ec.stored_data))
      ecampaign_new = save_update_ecampaign

      unless ecampaign_new.blank?
         begin
           img = ec.ecampaign_images.find_by_category("main_image") unless ec.blank?
           unless img.blank?
           require 'open-uri'
           open("tmp/cache/#{img.filename}", 'wb') do |file|
              file << open(img.public_filename).read
           end
           img_new = ecampaign_new.ecampaign_images.new(:uploaded_data => File.new("tmp/cache/#{img.filename}"))
           img_new.attachable = ecampaign_new
           img_new.category = "main_image"
           img_new.save!
           File.delete("tmp/cache/#{img.filename}")
           end
         rescue
           Log.create({:message => "Error when duplicate eproperty images id #{ecampaign_new.id}"})
         end
         save_update_ecampaign
      end
      flash[:notice] = 'Eproperty Update has successfully been duplicated.'
    end
    redirect_to eproperty_update_agent_office_ecampaigns_path(@agent, @office)
  end

  def preview
    ecampaign = Ecampaign.find_by_id(params[:id])
    email = ecampaign.stored_layout.gsub("{ecampaign_track_property_url}", full_base_url+visit_property_link_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{params[:track_id]}&property_id=#{params[:property_id]}")
    email = email.gsub("{ecampaign_track_url}", full_base_url+visit_link_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{params[:track_id]}")
    email = email.gsub("{forward}", full_base_url+forward_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{params[:track_id]}")
    email = email.gsub("{preview}", full_base_url+preview_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{params[:track_id]}")
    email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_ecampaign_path(ecampaign.office.agent,ecampaign.office,ecampaign)+"?track_id=#{params[:track_id]}")
    @html = email.gsub("<img src='/images/click_here.gif'/>", "<img  style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_ecampaigns_path(ecampaign.office.agent,ecampaign.office)+"?track_id=#{params[:track_id]}&ecampaign_id=#{ecampaign.id}"}'/>")
    render :layout => false
  end

  def unsubcribe
    track = EcampaignTrack.find_by_id(params[:track_id])
    unless track.blank?
      ac = track.agent_contact
      ac.inactive = true
      ac.save(false)
      ContactNote.create({:agent_contact_id => ac.id, :agent_user_id => ac.office.office_contact, :note_type_id => 22, :description => "User decided to unsubscribe through clicking option in eCampaign", :note_date => Time.now})
      EcampaignUnsubcribe.create({:ecampaign_id=> params[:id], :agent_contact_id => track.agent_contact_id})
    end
    @login_url = full_base_url
    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def visit_link
    #localhost:3000/agents/1/offices/1/ecampaigns/1/visit_link?track_id=1
    #track clicked link
    unless params[:link_url].blank?
      destination_link = params[:link_url].gsub("http://", "").gsub("www", "")
      ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{params[:id]} And `destination_link` LIKE '%#{destination_link}%'")
      unless ec.blank?
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_clicks SET clicked = clicked + 1 WHERE `id`=#{ec.id}")
        unless params[:track_id].blank?
          track = EcampaignTrack.find(params[:track_id])
          EcampaignClickDetail.create({:ecampaign_click_id=> ec.id, :agent_contact_id => track.agent_contact_id})
        end
      end
      redirect_to params[:link_url]
    else
      redirect_to eproperty_update_agent_office_ecampaigns_path(@agent, @office)
    end
  end

  def visit_property_link
    #track property clicked link
    unless params[:property_id].blank?
      ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{params[:id]} And `property_id` = #{params[:property_id]}")
      unless ec.blank?
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_clicks SET clicked = clicked + 1 WHERE `id`=#{ec.id}")
        unless params[:track_id].blank?
          track = EcampaignTrack.find(params[:track_id])
          EcampaignClickDetail.create({:ecampaign_click_id=> ec.id, :agent_contact_id => track.agent_contact_id})
        end
      end
      property = Property.find(params[:property_id])
      if property.detail.property_url.blank?
        if @office.url.present?
          office_url = "http://#{@office.url.gsub("http://", "")}"
          redirect_to "#{office_url}/#{params[:property_id]}"
        else
          redirect_to full_base_url+"/agents/#{property.agent_id}/offices/#{property.office_id}/properties/#{property.id}"
        end
      else
        redirect_to "http://#{property.detail.property_url.gsub("http://", "")}"
      end
    else
      redirect_to eproperty_update_agent_office_ecampaigns_path(@agent, @office)
    end
  end

  def delete_campaign
    unless params[:id].blank?
     ec  = Ecampaign.find_by_id(params[:id])
     unless ec.blank?
       eclicks = EcampaignClick.find(:all, :conditions => "ecampaign_id = #{params[:id]}")
       unless eclicks.blank?
         eclicks.each{|eclick| ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_click_details WHERE ecampaign_click_id = #{eclick.id}")}
         ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_clicks WHERE ecampaign_id = #{ec.id}")
       end
       ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_tracks WHERE ecampaign_id = #{ec.id}")
       ec.destroy
     end
    end
    flash[:notice] = 'Eproperty Update Already deleted.'
    redirect_to eproperty_update_agent_office_ecampaigns_path(@agent, @office)
  end

  # ============================================
  #                    STEPS
  #=============================================
  def choose_layout
    if params[:layout]
      session["ecampaign_layout"] = params[:layout]
      save_update_ecampaign
      redirect_to campaign_copy_agent_office_ecampaigns_path(@agent, @office)
    end
    @layout = session["ecampaign_layout"]
  end

  def campaign_copy
    redirect_to choose_layout_agent_office_ecampaigns_path(@agent, @office) if session["ecampaign_layout"].blank?
    @layout = session["ecampaign_layout"]
    unless params[:update].blank?
      session["ecampaign_data"] = params[:data]
      session["link_order"] = params[:link_order]
      session["info_links"] = params[:info_link]
      session["web_links"] = params[:web_link]
      session["general_links"] = params[:general_link]
      session["social_links"] = { "facebook_social_link" => params[:facebook_social_link], "twitter_social_link" => params[:twitter_social_link], "linkedin_social_link" => params[:linkedin_social_link], "youtube_social_link" => params[:youtube_social_link], "rss_social_link" => params[:rss_social_link]}
      session["property_checkbox"] = params[:property_checkbox]
      session["property_soldlease_checkbox"] = params[:property_soldlease_checkbox]
      session["property_rental_checkbox"] = params[:property_rental_checkbox]

      session["featured_property_ids"] = params[:featured_property_ids].blank? ? "" : params[:featured_property_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["office_ids"] = params[:office_ids].blank? ? "" : params[:office_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["property_type_ids"] = params[:property_type_ids].blank? ? "" : params[:property_type_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["listing_type_ids"] = params[:listing_type_ids].blank? ? "" : params[:listing_type_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["office_soldlease_ids"] = params[:office_soldlease_ids].blank? ? "" : params[:office_soldlease_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["listing_type_soldlease_ids"] = params[:listing_type_soldlease_ids].blank? ? "" : params[:listing_type_soldlease_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["property_type_soldlease_ids"] = params[:property_type_soldlease_ids].blank? ? "" : params[:property_type_soldlease_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["rental_property_type_ids"] = params[:rental_property_type_ids].blank? ? "" : params[:rental_property_type_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["rental_listing_type_ids"] = params[:rental_listing_type_ids].blank? ? "" : params[:rental_listing_type_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["office_rental_ids"] = params[:office_rental_ids].blank? ? "" : params[:office_rental_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}

      ecampaign = Ecampaign.find(session["ecampaign_id"]) unless session["ecampaign_id"].blank?
      if @layout == "theme5"
        unless params[:ecampaign_image].blank?
          # Theme5 Main image
          unless params[:ecampaign_image][:uploaded_data].blank?
            old_ecampaign = ecampaign.ecampaign_images.find_by_category("main_image")
            ecampaign_image = EcampaignImage.new(params[:ecampaign_image])
            ecampaign_image.category = "main_image"
            ecampaign_image.attachable = ecampaign

            if ecampaign_image.save
              old_ecampaign.destroy unless old_ecampaign.blank?
            end
          end
        end
      end
      save_update_ecampaign
      redirect_to choose_colour_agent_office_ecampaigns_path(@agent, @office)
    else
      session["ecampaign_data"] = { "sender_email" =>"", "sender_description" =>"", "sender_title" =>"","order" =>"", "status" =>"", "featured_property_ids" =>"", "office_id" =>"", "display_all_contact" =>"", "featured_listing_title" => "Featured Listings", "current_listing_title" => "Current Listings", "sold_listing_title" => "Sold Listings", "footer_text" => "", "hide_top_img" => ""} if session["ecampaign_data"].blank?
      session["social_links"] = { "facebook_social_link" => "", "twitter_social_link" => "", "linkedin_social_link" => "", "youtube_social_link" => "", "rss_social_link" => ""} if session["social_links"].blank?
    end

    @data = session["ecampaign_data"]
    @property_checkbox = session["property_checkbox"]
    @property_checkbox_soldlease = session["property_soldlease_checkbox"]
    @property_rental_checkbox = session["property_rental_checkbox"]
    if @agent.developer.allow_listing_office == true
      @offices = @agent.offices.find(:all, :conditions => "id=#{@office.id}")
    else
      @offices = @agent.developer.offices
    end

    @all_office_value_ids = @all_office_value = @all_office_value_sold = ""
    @offices.each{|office|
      @all_office_value_ids = @all_office_value_ids + "#"+ office.id.to_s
      @all_office_value = @all_office_value + '<span style=\"margin-right: 20px;\" id=\"office'+office.id.to_s+'\">'+office.name.to_s+'<a href=\"javascript:remove_options(\'office\',\''+office.id.to_s+'\',\'office\')\">x</a></span> '
      @all_office_value_sold = @all_office_value_sold + '<span style=\"margin-right: 20px;\" id=\"office_soldlease'+office.id.to_s+'\">'+office.name.to_s+'<a href=\"javascript:remove_options(\'office_soldlease\',\''+office.id.to_s+'\',\'office_soldlease\')\">x</a></span> '
    } unless @offices.blank?

    @featured_property_ids = session["featured_property_ids"].join("#") unless session["featured_property_ids"].blank?
    @value_office = session["office_ids"].join("#") unless session["office_ids"].blank?
    @value_office_soldlease = session["office_soldlease_ids"].join("#") unless session["office_soldlease_ids"].blank?
    @value_office_rental = session["office_rental_ids"].join("#") unless session["office_rental_ids"].blank?
    @value_property_type = session["property_type_ids"].join("#") unless session["property_type_ids"].blank?
    @value_property_type_soldlease = session["property_type_soldlease_ids"].join("#") unless session["property_type_soldlease_ids"].blank?
    @value_rental_property_type = session["rental_property_type_ids"].join("#") unless session["rental_property_type_ids"].blank?
    @value_listing_type = session["listing_type_ids"].join("#") unless session["listing_type_ids"].blank?
    @value_rental_listing_type = session["rental_listing_type_ids"].join("#") unless session["rental_listing_type_ids"].blank?

    @featured_properties = ""
    session["featured_property_ids"].each{|listing|
      p = Property.find_by_id(listing)
      @featured_properties = @featured_properties + '<span id="featured_property_id'+listing+'" style="margin-right:20px;">'+p.address.to_s+'<a href="javascript:remove_properties(\''+listing+'\')">x</a></span> ' unless p.blank?
    } unless session["featured_property_ids"].blank?

    @value_offices = ""
    session["office_ids"].each{|office|
      of = Office.find_by_id(office)
      @value_offices = @value_offices + '<span style="margin-right: 20px;" id="office'+office+'">'+of.name+'<a href="javascript:remove_options(\'office\',\''+office+'\',\'office\')">x</a></span> ' unless of.blank?
    } unless session["office_ids"].blank?

    @value_office_soldleases = ""
    session["office_soldlease_ids"].each{|office|
      of = Office.find_by_id(office)
      @value_office_soldleases = @value_office_soldleases + '<span style="margin-right: 20px;" id="office_soldlease'+office+'">'+of.name+'<a href="javascript:remove_options(\'office_soldlease\',\''+office+'\',\'office_soldlease\')">x</a></span> ' unless of.blank?
    } unless session["office_soldlease_ids"].blank?

    @value_offices_rental = ''
    session["office_rental_ids"].each{|office|
      of = Office.find_by_id(office)
      @value_offices_rental = @value_offices_rental+ '<span style="margin-right: 20px;" id="office_rental'+office+'">'+of.name+'<a href="javascript:remove_options(\'office_rental\',\''+office+'\',\'office_rental\')">x</a></span> ' unless of.blank?
    } unless session["office_rental_ids"].blank?

    @value_property_types = ""
    session["property_type_ids"].each{|property_type|
      @value_property_types = @value_property_types + '<span style="margin-right: 20px;" id="'+property_type+'">'+property_type+'<a href="javascript:remove_options(\'property_type\',\''+property_type+'\','')">x</a></span> '
    } unless session["property_type_ids"].blank?

    @value_property_type_soldleases = ""
    session["property_type_soldlease_ids"].each{|property_type|
      @value_property_type_soldleases = @value_property_type_soldleases + '<span style="margin-right: 20px;" id="soldlease'+property_type+'">'+property_type+'<a href="javascript:remove_options(\'property_type_soldlease\',\''+property_type+'\',\'soldlease\')">x</a></span> '
    } unless session["property_type_soldlease_ids"].blank?

    @value_rental_property_types = ''
    session["rental_property_type_ids"].each{|property_type|
      @value_rental_property_types = @value_rental_property_types + '<span style="margin-right: 20px;" id="rental'+property_type+'">'+property_type+'<a href="javascript:remove_options(\'rental_property_type\',\''+property_type+'\',\'rental\')">x</a></span> '
    } unless session["rental_property_type_ids"].blank?

    @value_listing_types = ""
    session["listing_type_ids"].each{|listing_type|
      @value_listing_types = @value_listing_types + '<span style="margin-right: 20px;" id="listing_type'+listing_type+'">'+listing_type+'<a href="javascript:remove_options(\'listing_type\',\''+listing_type+'\',\'listing_type\')">x</a></span> '
    } unless session["listing_type_ids"].blank?

    @value_listing_type_soldleases = ""
    session["listing_type_soldlease_ids"].each{|listing_type|
      @value_listing_type_soldleases = @value_listing_type_soldleases + '<span style="margin-right: 20px;" id="listing_type_soldlease'+listing_type+'">'+listing_type+'<a href="javascript:remove_options(\'listing_type_soldlease\',\''+listing_type+'\',\'listing_type_soldlease\')">x</a></span> '
    } unless session["listing_type_soldlease_ids"].blank?

    @value_rental_listing_types = ""
    session["rental_listing_type_ids"].each{|listing_type|
      @value_rental_listing_types = @value_rental_listing_types + '<span style="margin-right: 20px;" id="rental_listing_types'+listing_type+'">'+listing_type+'<a href="javascript:remove_options(\'rental_listing_types\',\''+listing_type+'\',\'rental_listing_types\')">x</a></span> '
    } unless session["rental_listing_type_ids"].blank?

    @property_checkbox_ids = @property_checkbox_soldlease_ids = @property_rental_checkbox_ids = ""
    @property_checkbox.each{|cid| @property_checkbox_ids = @property_checkbox_ids + '#' + cid } unless @property_checkbox.blank?
    @property_checkbox_soldlease.each{|cid| @property_checkbox_soldlease_ids = @property_checkbox_soldlease_ids + '#' + cid } unless @property_checkbox_soldlease.blank?
    @property_rental_checkbox.each{|cid| @property_rental_checkbox_ids = @property_rental_checkbox_ids + '#' + cid } unless @property_rental_checkbox.blank?

    @contacts = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq

    ptypes = Property.find(:all, :conditions => {:office_id => @agent.offices}, :select => "DISTINCT(`property_type`)")
    @property_types = ptypes.map{|m| [m.property_type, m.property_type] }.delete_if{|x, y| x.blank?}.unshift(['Please select',nil])

    listing_type_arr = [['Residential Sale', 'ResidentialSale'], ['Residential Lease', 'ResidentialLease'], ['Commercial Sale', 'CommercialSale'], ['Commercial Lease', 'CommercialLease'], ['Building', 'CommercialBuilding'],['Holiday Lease', 'HolidayLease'], ['Project Sale', 'ProjectSale'], ['Business Sale', 'BusinessSale'], ['Residential Building', 'ResidentialBuilding'], ['Land Release', 'LandRelease']]
    @listing_types = listing_type_arr.map{|m, n| [m, n] }.delete_if{|x, y| x.blank?}.unshift(['Please select',nil])

    rental_listing_type_arr = [['Residential Lease', 'ResidentialLease'], ['Commercial Lease', 'CommercialLease'], ['Holiday Lease', 'HolidayLease']]
    @rental_listing_types = rental_listing_type_arr.map{|m, n| [m, n] }.delete_if{|x, y| x.blank?}.unshift(['Please select',nil])

    @info_links = @web_links = @general_links = ""
    session["info_links"].each{|link| @info_links +='<div class="campaingn_link_wrapper"><div class="campaign-link-title">Title Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="info_link[][title]" class="single" style="width:200px;" value="'+link["title"]+'" /></div><div class="campaign-link-title">&nbsp;Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="info_link[][url]" class="single" style="width:200px;" value="'+link["url"]+'" /></div></div>'} unless session["info_links"].blank?
    session["web_links"].each{|link| @web_links +='<div class="campaingn_link_wrapper"><div class="campaign-link-title">Title Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="web_link[][title]" class="single" style="width:200px;" value="'+link["title"]+'" /></div><div class="campaign-link-title">&nbsp;Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="web_link[][url]" class="single" style="width:200px;" value="'+link["url"]+'" /></div></div>'} unless session["web_links"].blank?
    session["general_links"].each{|link| @general_links +='<div class="campaingn_link_wrapper marg"><div class="campaign-link-title">Headline:&nbsp;</div><div class="campaign-link-link"><input type="text" name="general_link[][title]" class="single" style="width:200px;" value="'+link["title"]+'" /></div><div class="campaign-link-title">&nbsp;Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="general_link[][url]" class="single" style="width:200px;" value="'+link["url"]+'" /></div><div style="clear:both;"></div></div><div class="campaingn_link_wrapper marg"><div class="campaign-link-title">Description:&nbsp;</div><div class="campaign-link-link"><textarea name="general_link[][desc]" style="width:495px;">'+link["desc"]+'</textarea></div><div style="clear:both;"></div></div>'} unless session["general_links"].blank?
    if @office.use_social_links
      session["social_links"]["facebook_social_link"] = @office.facebook if @office.facebook.present? && session["social_links"]["facebook_social_link"].blank?
      session["social_links"]["twitter_social_link"] = @office.twitter if @office.twitter.present? && session["social_links"]["twitter_social_link"].blank?
      session["social_links"]["youtube_social_link"] = @office.youtube if @office.youtube.present? && session["social_links"]["youtube_social_link"].blank?
      @office_web_link = @office.url if @office.url.present? && session["web_links"].blank?
    end
    @facebook_social_link = session["social_links"]["facebook_social_link"]
    @twitter_social_link = session["social_links"]["twitter_social_link"]
    @linkedin_social_link = session["social_links"]["linkedin_social_link"]
    @youtube_social_link = session["social_links"]["youtube_social_link"]
    @rss_social_link = session["social_links"]["rss_social_link"]
    @link_order = session["link_order"]

    if @layout == "theme5"
      ecampaign = Ecampaign.find(session["ecampaign_id"]) unless session["ecampaign_id"].blank?
      @ecampaign_image = ecampaign.ecampaign_images.find_by_category("main_image") unless ecampaign.blank?
    end
  end

  def choose_colour
    @layout = session["ecampaign_layout"]
    @header_image = find_image("Brochure Header")
    @footer_image = find_image("Brochure Footer")
    @find_side_image = find_image("Side Image")
    @color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #999999 #CCCCCC #f0f0f0 #FFFFFF)
    unless params[:choose_colours].blank?
      session["ecampaign_colour"] = { "bgcolor" => params[:bgcolor], "textcolor" => params[:textcolor], "heading" =>params[:heading], "headingtextcolor" =>params[:headingtextcolor], "thinline" =>params[:thinline], "mainbg" =>params[:mainbg], "maintext" =>params[:maintext], "icon_colour" =>params[:icon_colour], "bottomlinecolor" => params[:bottomline], "proptypecolor" => params[:proptype], "sidelinkcolor" => params[:sidelink]}
      save_update_ecampaign
      redirect_to select_recipient_agent_office_ecampaigns_path(@agent, @office)
    else
      session["ecampaign_colour"] = { "bgcolor" =>"", "textcolor" =>"", "heading" =>"", "headingtextcolor" =>"", "thinline" =>"", "generaltext" =>"", "mainbg" =>"", "maintext" =>"", "icon_colour" => "", "bottomlinecolor" => "", "proptypecolor" => "", "sidelinkcolor" => ""} if session["ecampaign_colour"].blank?
    end

    if @layout == "theme4"
      @bgcolor_default = "#0156A4"
      @textcolor_default = "#FFFFFF"
      @heading_default = "#FFFFFF"
      @headingtextcolor_default = "#000000"
      @thinline_default = "#EF4135"
      @mainbg_default = "#004484"
      @maintext_default = "#FFFFFF"
      @icon_colour_default = "black"
    elsif @layout == "theme1"
      @bgcolor_default = "#e8e0cd"
      @textcolor_default = "#666666"
      @heading_default = "#be002e"
      @headingtextcolor_default = "#666666"
      @thinline_default = "#000000"
      @mainbg_default = "#004484"
      @maintext_default = "#FFFFFF"
      @icon_colour_default = "black"
    elsif @layout == "theme5"
      @bgcolor_default = "#F0F0F0"
      @textcolor_default = "#333333"
      @heading_default = "#CC0031"
      @thinline_default = "#CC0031"
    else
      @bgcolor_default = "#000000"
      @textcolor_default = "#000000"
      @heading_default = "#FFFFFF"
      @headingtextcolor_default = "#FFFFFF"
      @thinline_default = "#000000"
      @mainbg_default = "#FFFFFF"
      @maintext_default = "#000000"
      @icon_colour_default = "black"
      @bottomlinecolor_default = "#D1203C"
    end

    @bgcolor = session["ecampaign_colour"]["bgcolor"].blank? ? @bgcolor_default : session["ecampaign_colour"]["bgcolor"]
    @textcolor =  session["ecampaign_colour"]["textcolor"].blank? ? @textcolor_default : session["ecampaign_colour"]["textcolor"]
    @heading =  session["ecampaign_colour"]["heading"].blank? ? @heading_default : session["ecampaign_colour"]["heading"]
    @headingtextcolor =  session["ecampaign_colour"]["headingtextcolor"].blank? ? @headingtextcolor_default : session["ecampaign_colour"]["headingtextcolor"]
    @thinline =  session["ecampaign_colour"]["thinline"].blank? ? @thinline_default : session["ecampaign_colour"]["thinline"]
    @mainbg =  session["ecampaign_colour"]["mainbg"].blank? ? @mainbg_default : session["ecampaign_colour"]["mainbg"]
    @maintext =  session["ecampaign_colour"]["maintext"].blank? ? @maintext_default : session["ecampaign_colour"]["maintext"]
    @icon_colour =  session["ecampaign_colour"]["icon_colour"].blank? ? @icon_colour_default : session["ecampaign_colour"]["icon_colour"]
    @bottomlinecolor = session["ecampaign_colour"]["bottomlinecolor"].blank? ? @bottomlinecolor_default : session["ecampaign_colour"]["bottomlinecolor"]
    @proptypecolor = session["ecampaign_colour"]["proptypecolor"].blank? ? "#D1203C" : session["ecampaign_colour"]["proptypecolor"]
    @sidelinkcolor = session["ecampaign_colour"]["sidelinkcolor"].blank? ? "#EF4135" : session["ecampaign_colour"]["sidelinkcolor"]
  end

  def select_recipient
    if @agent.developer.allow_ecampaign == true
      @group_contacts = GroupContact.find(:all)
    else
      @group_contacts = GroupContact.find(:all, :conditions => "office_id=#{@office.id}")
    end

    unless params[:update].blank?
      session["ecampaign_data"]["display_all_contact"] = params[:display_all_contact]
      session["ecampaign_group_id"] = params[:group_ids].blank? ? "" : params[:group_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["contact_checkbox"] = params[:contact_checkbox]
      session["contact_checkbox_all"] = params[:contact_checkbox_all]
      save_update_ecampaign
      redirect_to view_sample_agent_office_ecampaigns_path(@agent, @office)
    end

    @value_group = session["ecampaign_group_id"].join("#") unless session["ecampaign_group_id"].blank?
    @value_groups = ""
    session["ecampaign_group_id"].each{|group|
      gc = GroupContact.find_by_id(group)
      @value_groups = @value_groups + '<span style="margin-right: 20px;" id="group'+group+'">'+gc.name+'<a href="javascript:remove_options(\'group\',\''+group+'\',\'group\')">x</a></span> '
    } unless session["ecampaign_group_id"].blank?

    @contact_checkbox = session["contact_checkbox"]
    @contact_checkbox_ids = ""
    @contact_checkbox.each{|cid|
      @contact_checkbox_ids = @contact_checkbox_ids + '#' + cid
    } unless @contact_checkbox.blank?

    @contact_checkbox_all = session["contact_checkbox_all"]
    @contact_checkbox_all_ids = ""
    @contact_checkbox_all.each{|cid|
      @contact_checkbox_all_ids = @contact_checkbox_all_ids + '#' + cid
    } unless @contact_checkbox_all.blank?

    @display_all_contact = session["ecampaign_data"]["display_all_contact"]
  end

  def view_sample
    redirect_to send_ecampaign_agent_office_ecampaigns_path(@agent, @office) if params[:send_ecampaign]
    ecampaign_data
  end

  def send_ecampaign
    ecampaign_data
    html = render_to_string(:layout => false)
    title = session["ecampaign_data"]["sender_title"]
    sender_id = session["ecampaign_data"]["sender_email"]
    if session["ecampaign_data"]["display_all_contact"] == "true"
      sending_to_contacts = session["contact_checkbox_all"]
    else
      sending_to_contacts = session["contact_checkbox"]
    end

    all_data = formated_data
    unless session["ecampaign_id"].blank?
      ecampaign = Ecampaign.find(session["ecampaign_id"])
      ecampaign.title = title
      ecampaign.stored_layout = html
      ecampaign.stored_data = all_data.to_json
      ecampaign.sent_at = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      ecampaign.save!
    else
      ecampaign = Ecampaign.create(:office_id => @office.id, :title => title, :stored_layout => html, :stored_data => all_data.to_json, :sent_at => Time.now.strftime("%Y-%m-%d %H:%M:%S"))
    end

    if ecampaign
      @info_links.each{|info|
        destination_link = info["url"].gsub("http://", "").gsub("www", "")
        ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{ecampaign.id} And `destination_link` LIKE '%#{destination_link}%'")
        EcampaignClick.create(:ecampaign_id => ecampaign.id, :destination_name => info["title"], :destination_link => info["url"], :clicked => 0) if ec.blank?
      } unless @info_links.blank?
      @web_links.each{|info|
        destination_link = info["url"].gsub("http://", "").gsub("www", "")
        ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{ecampaign.id} And `destination_link` LIKE '%#{destination_link}%'")
        EcampaignClick.create(:ecampaign_id => ecampaign.id, :destination_name => info["title"], :destination_link => info["url"], :clicked => 0) if ec.blank?
      } unless @web_links.blank?
      @featured_properties.each{|property|
        ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{ecampaign.id} And `property_id` = #{property.id}")
        EcampaignClick.create(:ecampaign_id => ecampaign.id, :property_id => property.id, :clicked => 0) if ec.blank?
      } unless @featured_properties.blank?
      @property_checkbox_ids.each{|property|
        ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{ecampaign.id} And `property_id` = #{property.id}")
        EcampaignClick.create(:ecampaign_id => ecampaign.id, :property_id => property.id, :clicked => 0) if ec.blank?
      } unless @property_checkbox_ids.blank?
      @property_checkbox_soldlease_ids.each{|property|
        ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{ecampaign.id} And `property_id` = #{property.id}")
        EcampaignClick.create(:ecampaign_id => ecampaign.id, :property_id => property.id, :clicked => 0) if ec.blank?
      } unless @property_checkbox_soldlease_ids.blank?

      @property_rental_checkbox_ids.each{|property|
        ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{ecampaign.id} And `property_id` = #{property.id}")
        EcampaignClick.create(:ecampaign_id => ecampaign.id, :property_id => property.id, :clicked => 0) if ec.blank?
      } unless @property_rental_checkbox_ids.blank?

      ["facebook_social_link", "twitter_social_link", "linkedin_social_link", "youtube_social_link", "rss_social_link"].each do |sclink|
        unless session["social_links"][sclink].blank?
          destination_link = session["social_links"][sclink].gsub("http://", "").gsub("www", "")
          ec  = EcampaignClick.find(:first, :conditions => "`ecampaign_id` = #{ecampaign.id} And `destination_link` LIKE '%#{destination_link}%'")
          EcampaignClick.create(:ecampaign_id => ecampaign.id, :destination_name => session["social_links"][sclink], :destination_link => session["social_links"][sclink], :clicked => 0) if ec.blank?
        end
      end

      spawn(:kill => true) do
        sending_to_contacts.each_with_index do |contact_id, index|
          contact = AgentContact.find_by_id(contact_id)
          unless contact.blank?
          if contact.inactive != true
            ecampaign_track = EcampaignTrack.create(:ecampaign_id => ecampaign.id, :agent_contact_id => contact_id)
            if ecampaign_track
              #send email for each contact
              sender = AgentUser.find_by_id(sender_id)
              begin
                EcampaignMailer.deliver_send_ecampaign(@base_url, sender, ecampaign, ecampaign_track, contact)
              #rescue Net::SMTPServerBusy
              rescue
                ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
              end
              ContactNote.create({:agent_contact_id => contact.id, :agent_user_id => sender_id, :note_type_id => 23, :description => "An eCampaign was sent to this user", :note_date => Time.now})
              create_all_property_note(contact.id, sender_id)
            end
          end
          end
        end
      end
    end
    flash[:notice] = "Eproperty Update has successfully been sent"
    redirect_to eproperty_update_agent_office_ecampaigns_path(@agent, @office)
  end

  def sample
    @ecampaign = Ecampaign.find(params[:id])
    respond_to do |format|
      format.html { render :layout => false}
    end
  end

  def send_sample
    unless params[:id].blank?
      unless params[:email].blank?
        ecampaign_data
        html = render_to_string(:layout => false)
        @ecampaign = Ecampaign.find(params[:id])
        @ecampaign.stored_layout = html
        @ecampaign.save!

        sender = AgentUser.find_by_id(session["ecampaign_data"]["sender_email"])
        params[:email].each do |email|
        unless email.blank?
          begin
            EcampaignMailer.deliver_send_sample(sender, @ecampaign, email)
          rescue
            Log.create({:message => "Failed send sample email ecampaign #{@ecampaign.id}"})
          end
        end
        end
      end
      flash[:notice] = 'Sample Email already sent.'
    end
    redirect_to sample_agent_office_ecampaign_path(@agent,@office,@ecampaign)
  end

  def formated_data
    all_data = {"ecampaign_layout" => session["ecampaign_layout"],
    "property_soldlease_checkbox" => session["property_soldlease_checkbox"],
    "property_rental_checkbox" => session["property_rental_checkbox"],
    "ecampaign_data" => session["ecampaign_data"],
    "link_order" => session["link_order"],
    "info_links" => session["info_links"],
    "web_links" => session["web_links"],
    "general_links" => session["general_links"],
    "social_links" => session["social_links"],
    "featured_property_ids" => session["featured_property_ids"],
    "property_checkbox" => session["property_checkbox"],
    "ecampaign_colour" => session["ecampaign_colour"],
    "ecampaign_group_id" => session["ecampaign_group_id"],
    "contact_checkbox" => session["contact_checkbox"],
    "contact_checkbox_all" => session["contact_checkbox_all"],
    "office_ids" => session["office_ids"],
    "listing_type_ids" => session["listing_type_ids"],
    "property_type_ids" => session["property_type_ids"],
    "office_soldlease_ids" => session["office_soldlease_ids"],
    "listing_type_soldlease_ids" => session["listing_type_soldlease_ids"],
    "property_type_soldlease_ids" => session["property_type_soldlease_ids"],
    "rental_listing_type_ids" => session["rental_listing_type_ids"],
    "office_rental_ids" => session["office_rental_ids"]}
    return all_data
  end

  def save_update_ecampaign
    all_data = formated_data
    unless session["ecampaign_id"].blank?
      ec = Ecampaign.find(session["ecampaign_id"])
      ec.title = session["ecampaign_data"]["sender_title"] unless session["ecampaign_data"].blank?
      ec.stored_data = all_data.to_json
      ec.save!
    else
      ec = Ecampaign.create(:office_id => @office.id, :title => (session["ecampaign_data"].blank? ? "": session["ecampaign_data"]["sender_title"]), :stored_data => all_data.to_json)
      session["ecampaign_id"] = ec.id
    end
    return ec
  end

  def forward
    #forward_agent_office_ecampaigns_path
    #localhost:3000/agents/1/offices/1/ecampaigns/1/forward?track_id=1
    @agent_contact = AgentContact.new
    @track_id = params[:track_id]
    @ecampaign = Ecampaign.find(params[:id])

    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def send_forward
    @agent_contact = AgentContact.new(params[:agent_contact])
    @track_id = params[:track_id]
    respond_to do |format|
      @agent_contact.ecampaign_contact = true
      if @agent_contact.save
        ecampaign_track = EcampaignTrack.find_by_id(params[:track_id])
        if ecampaign_track
          spawn(:kill => true) do
            ecampaign = ecampaign_track.ecampaign
            sender = AgentContact.find_by_id(ecampaign_track.agent_contact_id)
            begin
              EcampaignMailer.deliver_send_ecampaign(full_base_url, sender, ecampaign, ecampaign_track, @agent_contact)
            rescue
              ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
            end
          end
        end
        #track forwarded link
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_forwarded = 1 WHERE id=#{@track_id}") unless @track_id.blank?
        flash[:notice] = 'Eproperty Update has successfully been forwarded.'
        format.html { redirect_to :action => 'forward' }
      else
        format.html { render :action => "forward", :layout => "new_level4" }
      end
    end
  end

  def get_image
    #track opened email
    ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_opened =1 WHERE id=#{params[:track_id]}") unless params[:track_id].blank?
    image = RAILS_ROOT+"/public/images/transparant.png"
    render :text => open(image, "rb").read
  end

  def ecampaign_data
    session["ecampaign_data"] = { "sender_email" =>"", "sender_description" =>"", "sender_title" =>"","order" =>"", "status" =>"", "featured_property_ids" =>"", "office_id" =>""} if session["ecampaign_data"].blank?
    session["social_links"] = { "facebook_social_link" => "", "twitter_social_link" => "", "linkedin_social_link" => "", "youtube_social_link" => "", "rss_social_link" => ""} if session["social_links"].blank?
    session["ecampaign_colour"] = { "bgcolor" =>"", "textcolor" =>"", "heading" =>"", "headingtextcolor" =>"", "thinline" =>"", "generaltext" =>"", "mainbg" =>"", "maintext" =>"", "icon_colour" => "", "bottomlinecolor" => ""} if session["ecampaign_colour"].blank?
    @layout = session["ecampaign_layout"]
    @base_url = full_base_url
    @ecampaign_id = session["ecampaign_id"]
    @ecampaign = Ecampaign.find_by_id(@ecampaign_id)
    @side_image = find_image("Side Image")
    @header_image = find_image("Brochure Header")
    @footer_image = find_image("Brochure Footer")
    @info_links = session["info_links"]
    @web_links = session["web_links"]
    @general_links = session["general_links"]
    @data = session["ecampaign_data"]
    @img_nm = "p"
    @icon_colour = "#000000"
    unless session["ecampaign_colour"].blank?
      @bgcolor = session["ecampaign_colour"]["bgcolor"]
      @textcolor = session["ecampaign_colour"]["textcolor"]
      @heading = session["ecampaign_colour"]["heading"]
      @headingtextcolor = session["ecampaign_colour"]["headingtextcolor"]
      @thinline = session["ecampaign_colour"]["thinline"]
      @mainbg = session["ecampaign_colour"]["mainbg"].blank? ? "#FFFFFF" : session["ecampaign_colour"]["mainbg"]
      @maintext = session["ecampaign_colour"]["maintext"]
      @icon_colour = session["ecampaign_colour"]["icon_colour"] == "white" ? "#ffffff" : "#000000"
      @img_nm = "white_p" if session["ecampaign_colour"]["icon_colour"] == "white"
      @bottomlinecolor = session["ecampaign_colour"]["bottomlinecolor"]
      @proptypecolor= session["ecampaign_colour"]["proptypecolor"]
      @sidelinkcolor = session["ecampaign_colour"]["sidelinkcolor"]
    end
    @ecampaign_status = session["ecampaign_status"]

    unless @data.blank?
      unless @data["sender_email"].blank?
        @sender = AgentUser.find_by_id(@data["sender_email"])
        unless @sender.blank?
          portraits = @sender.portrait_image
          unless portraits.blank?
            portraits.each do |img_portrait|
              if img_portrait.position == 1
                @img_portrait_1 = img_portrait
              else
                @img_portrait_2 = img_portrait
              end
            end
          end
        end
      end
    end

    @featured_properties = Property.find(:all, :conditions => {:id => session["featured_property_ids"]}) unless session["featured_property_ids"].blank?
    @property_checkbox_ids = Property.find(:all, :conditions => {:id => session["property_checkbox"]}) unless session["property_checkbox"].blank?
    @property_checkbox_soldlease_ids = Property.find(:all, :conditions => {:id => session["property_soldlease_checkbox"]}) unless session["property_soldlease_checkbox"].blank?
    @property_rental_checkbox_ids = Property.find(:all, :conditions => {:id => session["property_rental_checkbox"]}) unless session["property_rental_checkbox"].blank?
    @link_order = session["link_order"]
    if session["social_links"].reject{|a,b| b.blank?}.present?
      @socials = session["social_links"]
    end
    ecampaign = Ecampaign.find(session["ecampaign_id"]) unless session["ecampaign_id"].blank?
    if @layout == "theme5"
      @ecampaign_image = ecampaign.ecampaign_images.find_by_category("main_image") unless ecampaign.blank?
    end
  end

  def set_data(db_data)
      session["ecampaign_layout"] = db_data["ecampaign_layout"]
      session["property_soldlease_checkbox"] = db_data["property_soldlease_checkbox"]
      session["property_rental_checkbox"] = db_data["property_rental_checkbox"]
      session["ecampaign_data"] = db_data["ecampaign_data"]
      session["link_order"] = db_data["link_order"]
      session["info_links"] = db_data["info_links"]
      session["web_links"] = db_data["web_links"]
      session["general_links"] = db_data["general_links"]
      session["social_links"] = db_data["social_links"]
      session["featured_property_ids"] = db_data["featured_property_ids"]
      session["property_checkbox"] = db_data["property_checkbox"]
      session["ecampaign_colour"] = db_data["ecampaign_colour"]
      session["ecampaign_group_id"] = db_data["ecampaign_group_id"]
      session["contact_checkbox"] = db_data["contact_checkbox"]
      session["contact_checkbox_all"] = db_data["contact_checkbox_all"]
      session["office_ids"] = db_data["office_ids"]
      session["listing_type_ids"] = db_data["listing_type_ids"]
      session["property_type_ids"] = db_data["property_type_ids"]
      session["office_soldlease_ids"] = db_data["office_soldlease_ids"]
      session["listing_type_soldlease_ids"] = db_data["listing_type_soldlease_ids"]
      session["property_type_soldlease_ids"] = db_data["property_type_soldlease_ids"]
      session["ecampaign_status"] = "edit"
      session["rental_listing_type_ids"] = db_data["rental_listing_type_ids"]
      session["office_rental_ids"] = db_data["office_rental_ids"]
  end

  def search
    if params[:status] == "sold"
      condition = "`status` IN (2,3)"
    else
      condition = "`status` IN (1,5)"
    end

    text = ''
    unless params[:search_value].blank?
      if @agent.developer.allow_listing_office == true
        condition = "((`suburb` LIKE '%#{params[:search_value]}%' OR `street` LIKE '%#{params[:search_value]}%') OR `id` = '#{params[:search_value]}') AND `status` IN (1,5) And `office_id`=#{@office.id}"
      else
        condition = "((`suburb` LIKE '%#{params[:search_value]}%' OR `street` LIKE '%#{params[:search_value]}%') OR `id` = '#{params[:search_value]}') AND `status` IN (1,5)"
      end
    else
      unless params[:selection_office_id].blank?
        tmp_office_id = params[:selection_office_id].split("#").delete_if{|x| x.blank?}
        office_id = tmp_office_id.join(",")
        condition += " And `office_id` IN (#{office_id})"
      else
        condition += " And `office_id` = #{@office.id}" if @agent.developer.allow_listing_office == true
      end

      unless params[:selection_property_type].blank?
        tmp_property_type = params[:selection_property_type].split("#").delete_if{|x| x.blank?}
        condition += " And `property_type` IN ('#{tmp_property_type.join("','")}')"
      end

      unless params[:selection_listing_type].blank?
        tmp_listing_type = params[:selection_listing_type].split("#").delete_if{|x| x.blank?}
        ori_listings = tmp_listing_type.join("','")
        deal_types = ""
        if ori_listings.include?("CommercialSale")
          ori_listings = ori_listings.gsub("CommercialSale", "Commercial")
          deal_types += "'Sale'"
        end
        if ori_listings.include?("CommercialLease")
          ori_listings = ori_listings.gsub("CommercialLease", "Commercial")
          if deal_types.blank?
            deal_types += "'Lease'"
          else
            deal_types += ",'Lease', 'Both'"
          end
        end
        condition += " And `type` IN ('#{ori_listings}')"
        condition += " And (`deal_type` IN (#{deal_types}) OR `deal_type` IS NULL)" unless deal_types.blank?
      end
    end

    properties = Property.find(:all, :conditions => condition)
    jsons = []
    unless properties.blank?
      properties.each{|p|
        unless p.blank?
        ad = [ p.unit_number, p.street_number, p.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
        address = [ad, p.suburb ].join(", ")
        jsons << {'id' => p.id, 'address' => "#{address unless p.address.blank?}",'suburb' =>"#{p.suburb}",'type' =>"#{p.type}",'price' =>"$#{p.price.to_i}",'status' =>"#{p.status_name}", 'href' => agent_office_properties_path(@agent, @office)+"?ID=#{p.id}"}
        end
      } unless properties.nil?
      text = {"results" => jsons, "total" => (properties ? properties.length : 0) }
    end

    render :json => text
  end

  def search_contact
    jsons = []
    if params[:type] == "all"
      agent_contacts = AgentContact.find(:all, :conditions => "office_id=#{@office.id}")
      agent_contacts.each do |agent_contact|
        unless agent_contact.blank?
          jsons << {'id' => agent_contact.id, 'name' => "#{agent_contact.full_name}"} if agent_contact.inactive != true
        end
      end
      text = {"results" => jsons, "total" => (agent_contacts ? agent_contacts.length : 0) }
    else
      unless params[:selection_group_id].blank?
        group_ids = params[:selection_group_id].split("#").delete_if{|x| x.blank?}
        gc = GroupContactRelation.find(:all, :select => "DISTINCT(agent_contact_id)", :conditions => {:group_contact_id => group_ids})
      end
      if !gc.blank?
        gc.each do |ac|
          unless ac.agent_contact.blank?
            jsons << {'id' => ac.agent_contact.id, 'name' => "#{ac.agent_contact.full_name}"} if ac.agent_contact.inactive != true
          end
        end
        text = {"results" => jsons, "total" => (gc ? gc.length : 0) }
      else
        text = ''
      end
    end
    render :json => text
  end

  def find_image(type)
    get_header_footer(@office.id, type)
  end

  def ecampaign_note_for_property(contact_id, property, sender_id)
    ad = [ property.unit_number, property.street_number, property.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
    address = [ad, property.suburb ].join(", ")
    note = {:skip_enquiry => true, :property_id => property.id, :agent_contact_id => contact_id, :agent_user_id => sender_id,
            :note_type_id => 24,:address => address, :description => "Property Alert sent via eCampaign", :note_date => Time.now, :duplicate => 0}
    property_note = PropertyNote.new(note)
    property_note.save!

    ContactNote.create({:agent_contact_id => contact_id, :agent_user_id => sender_id, :note_type_id => 1, :description => "Property Alert sent via eCampaign", :note_date => Time.now, :address => address, :property_id => property.id})
  end

  def create_all_property_note(contact_id, sender_id)
    featured_properties = Property.find(:all, :conditions => {:id => session["featured_property_ids"]}) unless session["featured_property_ids"].blank?
    property_checkbox_ids = Property.find(:all, :conditions => {:id => session["property_checkbox"]}) unless session["property_checkbox"].blank?
    property_checkbox_soldlease_ids = Property.find(:all, :conditions => {:id => session["property_soldlease_checkbox"]}) unless session["property_soldlease_checkbox"].blank?
    property_rental_checkbox_ids = Property.find(:all, :conditions => {:id => session["property_rental_checkbox"]}) unless session["property_rental_checkbox"].blank?

    featured_properties.each{|property| ecampaign_note_for_property(contact_id, property, sender_id) } unless featured_properties.blank?
    property_checkbox_ids.each{|property| ecampaign_note_for_property(contact_id, property, sender_id)} unless property_checkbox_ids.blank?
    property_checkbox_soldlease_ids.each{|property| ecampaign_note_for_property(contact_id, property, sender_id)} unless property_checkbox_soldlease_ids.blank?
    property_rental_checkbox_ids.each{|property| ecampaign_note_for_property(contact_id, property, sender_id)} unless property_rental_checkbox_ids.blank?
  end
end