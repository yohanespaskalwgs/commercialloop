require 'will_paginate/array'
class MatchContactsController < ApplicationController
  layout "agent"
  before_filter :login_required
  before_filter :get_agent_and_office, :get_property
  before_filter :require_accepted, :only => [:new]
  before_filter :prepare_search
  skip_before_filter :verify_authenticity_token, :only => [:email_configuration, :new, :create, :auto_complete]

  include ActionView::Helpers::TextHelper
  include PropertiesHelper

  def index
    get_session_listing
    get_session_array
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    if params[:listing_type].blank?
      feed_multiple_value
    else
      unless params[:listing_type] == "ProjectSale"
        feed_locations
      end
    end
    session[:url_search] = request.url
    @listing = params[:listing]
    @type = params[:type]
    @search = params[:search_keyword]
    @properties_filter = Property.find(:all,:include => [:primary_contact],:conditions=>["office_id=?",current_office.id], :select => "suburb,id, property_type,status,primary_contact_id");
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Listings' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?

    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def feed_locations
    @all_suburbs = @all_suburbs_list = @all_regions = @all_regions_list = ''
    unless params[:regions_id].blank?
      @regions_data = params[:regions_id].split(',')
      @regions_data.map{|cat|
        random = (0..100).to_a.rand.to_s
        unless cat == ''
          @all_regions = params[:regions_id]
          @all_regions_list = @all_regions_list + '<div id="'+random+'"style="padding-left:176px;width:200px"class="property_type_selected">'+cat+'<a href="#" onclick="remove_region('+random+',\''+cat+'\')">x</a></div>'
        end
      }
    end

    unless params[:suburbs_id].blank?
      @suburbs_data = params[:suburbs_id].split(',')
      @suburbs_data.map{|cat|
        random = (0..100).to_a.rand.to_s
        unless cat == ''
          @all_suburbs = params[:suburbs_id]
          @all_suburbs_list = @all_suburbs_list + '<div id="'+random+'"style="padding-left:176px;width:200px"class="property_type_selected">'+cat+'<a href="#" onclick="remove_suburb('+random+',\''+cat+'\')">x</a></div>'
        end
      }
    end
  end

  def feed_multiple_value
    #locations
    suburb_id = Suburb.find_by_name(@property.suburb).id
    region_id = TownCountry.find_by_name(@property.town_village).id
    @status = @property.status.to_s
    @suburbs = @suburb = @regions = @region = ''
    @all_suburbs = "," + @property.suburb
    @all_suburbs_list = '<div id="'+suburb_id.to_s+'"style="padding-left:176px;width:200px"class="property_type_selected">'+@property.suburb+'<a href="#" onclick="remove_suburb('+suburb_id.to_s+',\''+@property.suburb+'\')">x</a></div>'
    @suburbs = Suburb.find_all_by_region_name(@property.town_village).map{|x|[x.name,x.name]}.unshift(['Please select', nil])
    @all_regions = "," + @property.town_village
    @regions = TownCountry.find_all_by_state_name(@property.state).map{|x|[x.name,x.name]}.unshift(['Please select', nil])
    @all_regions_list = '<div id="'+region_id.to_s+'"style="padding-left:176px;width:200px"class="property_type_selected">'+@property.town_village+'<a href="#" onclick="remove_region('+region_id.to_s+',\''+@property.town_village+'\')">x</a></div>'
    @state = @property.state

    unless ["NewDevelopment", "LandRelease"].include?(@property.type)
      @land_area = @property.detail.blank? ? "" : (@property.detail.land_area.blank? ? "" : @property.detail.land_area)
    end
    @floor_area = @property.detail.blank? ? "" : (@property.detail.floor_area.blank? ? "" : @property.detail.floor_area)
    @price = @property.price rescue nil
    @price_to = @property.to_price rescue nil
    @size_from = @property.size_from rescue nil
    @size_to = @property.size_to rescue nil
    @number_of_suites = @property.number_of_suites rescue nil
    @australian_states = State.find(:all, :conditions => "states.country_id = '12'").map{|x|[x.name,x.name]}.unshift('Please select', nil)
    @features = @property.features.collect(&:name) unless @property.features.blank?

    if ["Commercial", "CommercialBuilding"].include?(@property.type)
      @rental_from = @property.detail.rental_price rescue nil
      @rental_to = @property.detail.rental_price_to rescue nil
      @office_area = @property.detail.office_area rescue nil
      @warehouse_area = @property.detail.warehouse_area rescue nil
      @current_rent = @property.detail.current_rent rescue nil
      @deal_type = @property.deal_type rescue nil
      @lease_start = Date.strptime(@property.detail.lease_commencement, '%d/%m/%Y').strftime('%Y-%m-%d') rescue nil
      @lease_end = Date.strptime(@property.detail.lease_end, '%d/%m/%Y').strftime('%Y-%m-%d') rescue nil
      @area_from = @floor_area
      @area_to = @floor_area
      @sale_price_from = @property.detail.sale_price_from.present? ? @property.detail.sale_price_from : @property.price
      @sale_price_to = @property.detail.sale_price_to.present? ? @property.detail.sale_price_to : @property.price
      @tenant = @property.tenancy_records.blank? ? "" : (@property.tenancy_records.valid.blank? ? "" : (@property.tenancy_records.valid.first.agent_contact.blank? ? "" : (@property.tenancy_records.valid.first.agent_contact.full_name.blank? ? "" : @property.tenancy_records.valid.first.agent_contact.full_name)))
      @tenant_id = @property.tenancy_records.blank? ? "" : (@property.tenancy_records.valid.blank? ? "" : (@property.tenancy_records.valid.first.agent_contact_id.blank? ? "" : @property.tenancy_records.valid.first.agent_contact_id))
    elsif ["ResidentialSale", "ResidentialLease", "NewDevelopment"].include?(@property.type)
      @yield = @property.detail.rental_yield rescue nil
      @price_from_residential = @property.price rescue nil
      @price_to_residential = @property.to_price rescue nil
    else
      @ptype = @property.property_type
    end
  end

  def populate_combobox_contact
    suburb, category, last_name, first_name, assigned_to, accessible_by, condition = [],[],[],[],[],[], []
    condition = [" agent_contacts.office_id = ? AND agent_contacts.id IN (?)", current_office.id, session[:agent_contact_id]]
    contacts = AgentContact.find(:all, :conditions => condition, :select => "DISTINCT(agent_contacts.category_contact_id), agent_contacts.suburb, agent_contacts.first_name, agent_contacts.last_name, agent_contacts.assigned_to, agent_contacts.accessible_by, agent_contacts.id")
    contacts.map{|x|
      first_name << {
        :first_name => (x.first_name.to_s.slice(0,1).capitalize unless x.first_name.blank?) ,
        :first_nameText => (x.first_name.to_s.slice(0,1).capitalize unless x.first_name.blank?)
      } unless x.first_name.blank?

      last_name << {
        :last_name => (x.last_name.to_s.slice(0,1).capitalize unless x.last_name.blank?) ,
        :last_nameText => (x.last_name.to_s.slice(0,1).capitalize unless x.last_name.blank?)
      } unless x.last_name.blank?

      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText => x.suburb.to_s
      } unless x.suburb.blank?;
    }

    categories = ContactCategoryRelation.find(:all, :select => "DISTINCT(contact_category_relations.category_contact_id)")
    cid = categories.map{|c| c.category_contact_id}
    cat_cons = CategoryContact.find(:all, :conditions => "id IN (#{cid.join(',')})") if cid.present?

    cat_cons.map{|x|
      category << {
        :category => (x.id) ,
        :categoryText => (x.name.to_s)
      }
    } unless cat_cons.blank?

    assigned_to_db = ContactAssignedRelation.find(:all, :select => "DISTINCT(contact_assigned_relations.assigned_to)")
    a = assigned_to_db.map{|p| p.assigned_to.to_i unless p.assigned_to.blank?}
    usrs = User.find(:all, :conditions =>"id IN (#{a.join(',')}) and office_id = #{@office.id}") if a.present?

    usrs.map{|x|
      assigned_to << {
        :assigned_to => (x.id) ,
        :assigned_toText => (x.full_name)
      }
    } unless usrs.blank?

    accessible_by_db = ContactAccessibleRelation.find(:all, :select => "DISTINCT(contact_accessible_relations.accessible_by)")
    b = accessible_by_db.map{|p| p.accessible_by.to_i unless p.accessible_by.blank?}
    usrs2 = User.find(:all, :conditions =>"id IN (#{b.join(',')})and office_id = #{@office.id}") if b.present?
    usrs2.map{|x|
      accessible_by << {
        :accessible_by => (x.id) ,
        :accessible_byText => (x.full_name)
      }
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_assigned_to =[]
    uniq_accessible_by =[]
    uniq_first_name =[]
    uniq_last_name =[]
    uniq_category =[]

    uniq_assigned_to << {:assigned_to=>'all_team',:assigned_toText=>"All Team Members"}
    uniq_accessible_by << {:accessible_by=>'all_team',:accessible_byText=>"All Team Members"}
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    accessible_by.each{|x|uniq_accessible_by << x if !uniq_accessible_by.include?(x) and !x.blank?}
    category.each{|x|uniq_category << x if !uniq_category.include?(x) and !x.blank?}
    first_name.each{|x|uniq_first_name << x if !uniq_first_name.include?(x) and !x.blank?}
    last_name.each{|x|uniq_last_name << x if !uniq_last_name.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_accessible_by.to_a.sort!{|x,y| x[:accessible_byText].to_s <=> y[:accessible_byText].to_s}
    uniq_category.to_a.sort!{|x,y| x[:categoryText].to_s <=> y[:categoryText].to_s}
    uniq_first_name.to_a.sort!{|x,y| x[:first_nameText].to_s <=> y[:first_nameText].to_s}
    uniq_last_name.to_a.sort!{|x,y| x[:last_nameText].to_s <=> y[:last_nameText].to_s}

    return(render(:json =>{:results => contacts.length, :team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Assigned To"})].flatten,
          :access=>[uniq_accessible_by.unshift({:accessible_by=>'all',:accessible_byText=>"All Accessible By"})].flatten,
          :f_name=>[uniq_first_name.unshift({:first_name=>'all',:first_nameText=>"All First Name"})].flatten,
          :l_name=>[uniq_last_name.unshift({:last_name=>'all',:last_nameText=>"All Last Name"})].flatten,
          :suburb=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,
          :category=>[uniq_category.unshift({:category=>'all',:categoryText=>"All Categories"})].flatten
        }))
  end

  def view_contact_listing
    if params[:page_name] == 'listing_view'
      unless session[:ID].blank?
        condition = ["agent_contacts.id = #{session[:ID]} "]
      else
        get_session_contact_listing
        condition = [" agent_contacts.office_id = ? ", current_office.id]

        unless session[:first_name] == "all"
          if(!session[:first_name].blank?)
            condition[0] += " AND agent_contacts.first_name LIKE ?"
            condition <<  session[:first_name]+"%"
          end
        end

        unless session[:last_name] == "all"
          if(!session[:last_name].blank?)
            condition[0] += " AND agent_contacts.last_name LIKE ?"
            condition <<  session[:last_name]+"%"
          end
        end

        unless session[:email] == 'all'
          if(!session[:email].blank?)
            condition[0] += " AND agent_contacts.email LIKE ?"
            condition <<  session[:email]+"%"
          end
        end

        unless session[:company] == 'all'
          if(!session[:company].blank?)
            condition[0] += " AND (SELECT agent_companies.company FROM agent_companies INNER JOIN agent_contacts ON agent_companies.id = agent_contacts.agent_company_id )"
            condition <<  session[:company]+"%"
          end
        end

        unless session[:suburb] == 'all'
          if(!session[:suburb].blank?)
            condition[0] += " AND agent_contacts.suburb LIKE ?"
            condition << session[:suburb]+"%"
          end
        end

        unless session[:agent_contact_id].blank?
          condition[0] += " AND agent_contacts.id IN (?)"
          condition << session[:agent_contact_id]
        end

      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = AgentContact.build_sorting(params[:sort],params[:dir])
    if current_user.limited_client_access?
      contact_accessible_by2 = ContactAccessibleRelation.find(:all, :conditions => ["`accessible_by` = ?", current_user.id])
      condition[0] += " AND agent_contacts.id IN (?)"
      condition <<  contact_accessible_by2.collect(&:agent_contact_id)
      include = params[:sort] == 'assigned_to'? [:accessible, :assigned] : [:accessible, :category_contact]
    else
      include = params[:sort] == 'assigned_to'? [:assigned] : [:accessible, :category_contact]
    end


    @agent_contacts = AgentContact.paginate(:all, :include => include, :conditions => condition, :order => order, :page =>params[:page], :per_page => params[:limit].to_i)
    results = params[:limit].to_i == 20 ? AgentContact.find(:all,:select => 'count(agent_contacts.id)',:conditions => condition,:limit => params[:limit].to_i) : AgentContact.count(:conditions => condition)

    return(render(
        :json =>{
          :results => results,
          :rows=>@agent_contacts.map{|x|{
              'id' => x.id.to_s + '#'+ current_user.include_accessible?(x.id).to_s,
              'full_name' => (x.full_name.blank? ? "" : x.full_name.to_s),
              'phone' => (x.contact_number.blank? ? "" : x.contact_number),
              'suburb' => (x.suburb.blank? ? "" : x.suburb),
              'email' => (x.email.blank? ? "" : x.email.to_s),
              'company' => (x.agent_company.blank? ? "" : x.agent_company.company.to_s),
              'active' => (AgentContact.check_active(x)),
              'actions'=> AgentContact.check_id(x)}}}))
  rescue
    return(render(:json =>{:results => nil, :rows=> nil}))
  end

  def get_session_contact_listing
    session[:contact_type] = params[:contact_type].nil? ? "all" : params[:contact_type]
    session[:company_type] = params[:company_type].nil? ? "all" : params[:company_type]
    session[:first_name] = params[:first_name].nil? ? "all" : params[:first_name]
    session[:last_name] = params[:last_name].nil? ? "all" : params[:last_name]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:assigned_to] = params[:assigned_to].nil? ? "all" : params[:assigned_to]
    session[:ID] = params[:ID].nil? ? nil : params[:ID]
    session[:sort_by] = []
    session[:sort_by] << {["agent_contacts.updated_at DESC"],["agent_companies.updated_at DESC"]}
  end

  def nil_session_contact_listing
    session[:status] = nil
    session[:company_type] = nil
    session[:contact_type] = nil
    session[:suburb] = nil
    session[:first_name] = nil
    session[:last_name] = nil
    session[:assigned_to] = nil
  end

  def auto_complete
    result = []
    contacts = AgentContact.find(:all, :conditions => ["(`first_name` LIKE ? OR `last_name` LIKE ?) and `agent_id` = ? and `office_id` = ? ", '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', @agent.id, @office.id])
    contacts.each{|x|
      full_name = x.first_name.to_s + " " + x.last_name.to_s
      result << {'id' => "#{x.id.to_s}", 'name' => "#{full_name}" } unless x.first_name.blank? }
    render :json => {"results" => result, "total" => (contacts ? contacts.length : 0) }
  end

  def create_match_contacts
    unless params[:agent_contact_id].blank?
      session[:agent_contact_id] = params[:agent_contact_id]
      params[:agent_contact_id].each do |x|
        @match_contact = MatchContact.new({
            :agent_contact_id => x,
            :property_id => @property.id}).save
      end
      redirect_to new_agent_office_property_match_contact_path(@agent,@office,@property)
    else
      flash[:notice] = "Please select contact first."
      redirect_to agent_office_property_match_contacts_path(@agent,@office,@property)
    end
  end

  # ------------------------------------------listing------------------------------------------------
  def populate_combobox
    suburb,type,assigned_to,condition = [],[],[],[]
    condition = [" properties.office_id = ? "]
    condition << current_office.id

    properties = Property.find(:all,:include =>[:primary_contact], :conditions => condition,:select => "properties.primary_contact_id, DISTINCT(properties.suburb),DISTINCT(properties.property_type),users.first_name,users.last_name")
    properties.map{|x|
      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText =>x.suburb.to_s
      } unless x.suburb.blank?;
      type << {
        :type => x.property_type.to_s,
        :typeText =>x.property_type.to_s
      } unless x.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?) ,
        :assigned_toText => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?)
      } unless x.primary_contact.blank?
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    return(render(:json =>{:results => properties.length,:suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,:types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,:team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        if current_user.limited_client_access?
          if !@office.control_level3
            condition[0] += " AND properties.primary_contact_id = ? "
            condition << current_user.id
          end
        elsif current_user.allow_property_owner_access?
          condition[0] += " AND properties.agent_user_id = ?"
          condition << current_user.id
        end

        if(!session[:type].blank? && Property::TYPES.map{|s| s[1]}.include?(session[:type]))
          condition[0] += " AND properties.type = ?"
          condition << session[:type]
        end


        if ((current_user.allow_property_owner_access? && session[:status] == 1) || (current_user.allow_property_owner_access?  && session[:status].blank?))|| current_user.allow_property_owner_access?
          session[:status] = "all"
        end

        unless session[:status] == "all"
          unless session[:status].blank?
            condition[0] += " AND properties.status = ?"
            condition << session[:status]
          end
        end

        unless session[:suite] == "all" || session[:suite].blank?
          condition[0] += " AND properties.street_number = ?"
          condition << session[:suite]
        end

        unless session[:suburb] == "all" || session[:suburb].blank?
          condition[0] += " AND properties.suburb = ?"
          condition << session[:suburb]
        end

        unless session[:primary_contact_fullname] == "all" || session[:primary_contact_fullname].blank?
          name =  session[:primary_contact_fullname].split(' ')
          condition[0] += " AND users.first_name  = ?"
          condition << name[0]
          unless name[1].blank?
            condition[0] += " AND users.last_name  = ?"
            condition << name[1]
          end
        end

        unless session[:property_type] == "all" || session[:property_type].blank?
          condition[0] += " AND properties.property_type = ?"
          condition << session[:property_type]
        end
      end
    end

    if params[:page_name] == 'office_view'
      if params[:listing_type].blank?
        condition = "agent_contacts.agent_id = '#{@agent.id}' AND agent_contacts.office_id = '#{@office.id}'"
        unless @property.type.to_s == "ProjectSale"
          if ["ResidentialSale", "ResidentialLease", "NewDevelopment", "LandRelease", "HolidayLease"].include?(@property.type.to_s)
            condition += " AND contact_alerts.listing_type = 'Residential'"
            condition += " AND contact_alerts.price_from_residential = '#{@property.price rescue nil}'" unless @property.price.blank?
            condition += " AND contact_alerts.price_to_residential = '#{@property.to_price rescue nil}'" unless @property.to_price.blank?
          elsif ["Commercial", "CommercialBuilding", "BusinessSale"].include?(@property.type.to_s)
            condition += " AND contact_alerts.listing_type = 'Commercial'"
            condition += " AND contact_alerts.listing_select_status_commercial = '#{@property.deal_type rescue nil}'" unless @property.deal_type.blank?
            condition += " AND contact_alerts.commercial_property_type = '#{@property.type.to_s rescue nil}'" unless @property.type.blank?
            if @property.deal_type.to_s == "Lease"
              condition += " AND contact_alerts.total_floor_area_from = '#{@property.detail.floor_area rescue nil}'" unless @property.detail.floor_area.blank?
              condition += " AND contact_alerts.total_floor_area_to = '#{@property.detail.floor_area rescue nil}'" unless @property.detail.floor_area.blank?
              condition += " AND contact_alerts.office_from = '#{@property.detail.office_area rescue nil}'" unless @property.detail.office_area.blank?
              condition += " AND contact_alerts.office_to = '#{@property.detail.office_area rescue nil}'" unless @property.detail.office_area.blank?
              condition += " AND contact_alerts.warehouse_from = '#{@property.detail.warehouse_area rescue nil}'" unless @property.detail.warehouse_area.blank?
              condition += " AND contact_alerts.warehouse_to = '#{@property.detail.warehouse_area rescue nil}'" unless @property.detail.warehouse_area.blank?
              condition += " AND contact_alerts.rent_from = '#{@property.detail.current_rent rescue nil}'" unless @property.detail.current_rent.blank?
              condition += " AND contact_alerts.rent_to = '#{@property.detail.current_rent rescue nil}'" unless @property.detail.current_rent.blank?
              condition += " AND contact_alerts.lease_expiry_from = '#{Date.strptime(@property.detail.lease_commencement, '%d/%m/%Y').strftime('%Y-%m-%d') rescue nil}'" unless @property.detail.lease_commencement.blank?
              condition += " AND contact_alerts.lease_expiry_to = '#{Date.strptime(@property.detail.lease_end, '%d/%m/%Y').strftime('%Y-%m-%d') rescue nil}'" unless @property.detail.lease_end.blank?
              condition += " AND contact_alerts.tenant = '#{@property.tenancy_records.valid.first.agent_contact_id rescue nil}'" unless @property.tenancy_records.blank?
            end
          end
          condition += " AND contact_alerts.annual_rental_from = '#{@property.detail.rental_price rescue nil}'" unless @property.detail.rental_price.blank?
          condition += " AND contact_alerts.annual_rental_to = '#{@property.detail.rental_price_to rescue nil}'" unless @property.detail.rental_price_to.blank?
          condition += " AND contact_alerts.availiability_type = '#{@property.status}'" unless @property.status.blank?
          condition += " AND contact_alerts.asking_price_from = '#{@property.price}'" unless @property.price.blank?
          condition += " AND contact_alerts.asking_price_to = '#{@property.to_price}'" unless @property.to_price.blank?
          condition += " AND contact_alerts.state = '#{@property.state}'" unless @property.state.blank?
          condition += " AND contact_alerts.id IN (SELECT contact_alert_suburbs.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_suburbs ON contact_alerts.id = contact_alert_suburbs.contact_alert_id WHERE contact_alert_suburbs.suburb IN ('#{@property.suburb}'))" unless @property.suburb.blank?
          condition += " AND contact_alerts.id IN (SELECT contact_alert_regions.contact_alert_id FROM contact_alerts LEFT OUTER JOIN contact_alert_regions ON contact_alerts.id = contact_alert_regions.contact_alert_id WHERE contact_alert_regions.region IN ('#{@property.town_village}'))" unless @property.town_village.blank?
          condition += " AND contact_alerts.land_area_from = '#{@property.detail.land_area rescue nil}'" unless @property.detail.land_area.blank?
          condition += " AND contact_alerts.land_area_to = '#{@property.detail.land_area rescue nil}'" unless @property.detail.land_area.blank?
          condition += " AND contact_alerts.building_area_from = '#{@property.detail.area_range rescue nil}'" unless @property.detail.area_range.blank?
          condition += " AND contact_alerts.building_area_to = '#{@property.detail.area_range_to rescue nil}'" unless @property.detail.area_range_to.blank?
        else
          condition += " AND contact_alerts.houseland_property_type = '#{@property.property_type rescue nil}'" unless @property.property_type.blank?
          condition += " AND contact_alerts.price_range_from = '#{@property.price rescue nil}'" unless @property.price.blank?
          condition += " AND contact_alerts.price_range_to = '#{@property.to_price rescue nil}'" unless @property.to_price.blank?
        end
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = AgentContact.build_sorting(params[:sort],params[:dir])

    if !params[:listing_type].blank?
      get_session_listing
      @contacts = ContactAlert.advanced_search(params[:listing_type],params[:property_type],params[:business_type],params[:business_category],params[:price_from],params[:price_to],params[:deal_type],params[:availiability],params[:suites],params[:range_from],params[:range_to],session[:array_type_commercial],session[:array_type],session[:array_residential_building],params[:list_status_commercial],params[:commercial_type],params[:list_status_residential],params[:state],params[:regions_id],params[:suburbs_id],params[:agent_internal],params[:external_company],params[:agent_external],params[:yield_from],params[:yield_to],params[:building_from],params[:building_to],params[:land_from],params[:land_to],params[:price_from_commercial],params[:price_to_commercial],params[:price_from_residential],params[:price_to_residential],params[:annual_rental_from],params[:annual_rental_to],params[:floor_from],params[:floor_to],params[:office_from],params[:office_to],params[:warehouse_from],params[:warehouse_to],params[:rent_from],params[:rent_to],params[:lease_from],params[:lease_to],params[:tenant],params[:search_keyword_advanced],params[:tenant_name_input],params[:start],params[:limit],params[:page],@agent.id,@office.id,order,params[:sort],params[:dir])
    else
      @contacts = ContactAlert.paginate(:all, :include => [:agent_contact, :contact_alert_suburbs, :contact_alert_regions, :contact_alert_commercial_types], :conditions => condition, :order => order, :page =>params[:page], :per_page => params[:limit].to_i)
    end
    results = 0
    if params[:limit].to_i == 50
      if !params[:listing_type].blank?
        results = ContactAlert.advanced_search(params[:listing_type],params[:property_type],params[:business_type],params[:business_category],params[:price_from],params[:price_to],params[:deal_type],params[:availiability],params[:suites],params[:range_from],params[:range_to],session[:array_type_commercial],session[:array_type],session[:array_residential_building],params[:list_status_commercial],params[:commercial_type],params[:list_status_residential],params[:state],params[:regions_id],params[:suburbs_id],params[:agent_internal],params[:external_company],params[:agent_external],params[:yield_from],params[:yield_to],params[:building_from],params[:building_to],params[:land_from],params[:land_to],params[:price_from_commercial],params[:price_to_commercial],params[:price_from_residential],params[:price_to_residential],params[:annual_rental_from],params[:annual_rental_to],params[:floor_from],params[:floor_to],params[:office_from],params[:office_to],params[:warehouse_from],params[:warehouse_to],params[:rent_from],params[:rent_to],params[:lease_from],params[:lease_to],params[:tenant],params[:search_keyword_advanced],params[:tenant_name_input],params[:start],params[:limit],params[:page],@agent.id,@office.id,order,params[:sort],params[:dir])
      else
        results = ContactAlert.count(:include => [:agent_contact, :contact_alert_suburbs, :contact_alert_regions, :contact_alert_commercial_types], :conditions => condition)
      end
    end
    return(render(:json =>{:results => results,
          :rows=> @contacts.map{|x|{
              'id' => ContactAlert.get_info_contact(x),
              'full_name' => x.agent_contact.full_name,
              'email' => x.agent_contact.email,
              'company_name' => x.agent_contact.agent_company.blank? ? "" : (x.agent_contact.agent_company.company.blank? ? "" : x.agent_contact.agent_company.company),
              'listing_type' => x.listing_type,
              'type' => (x.contact_alert_commercial_types.blank? ? "N/A" : x.contact_alert_commercial_types.map(&:commercial_type).join(",")),
              'land_area' => ContactAlert.check_data(x)[0],
              'floor_area' => ContactAlert.check_data(x)[1],
              'price' => ContactAlert.check_data(x)[2],
              'suburb' => (x.contact_alert_suburbs.blank? ? "N/A" : x.contact_alert_suburbs.map(&:suburb).join(",")) } }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end

  def find_image(type)
    get_header_footer(@office.id, type)
  end

  def new
    @email = EmailConfiguration.new
    @agent_contacts = AgentContact.find(:all)
    @selected_contacts = AgentContact.find(:all, :conditions => ["agent_contacts.id IN (?)", session[:agent_contact_id]])
    @detail = @property.detail
    @contents = "#{@property.headline}\n\nDescription = #{@property.description}\nAddress = #{@property.address}"
    @url_search = session[:url_search]
  end

  def create
    if !params[:agent_contact_id].blank? || !params[:contact_name].blank?
      @email = EmailConfiguration.new(params[:email_configuration])
      @email.user_id = current_user.id
      @email.property_id = @property.id
      @agent_contacts = AgentContact.find(:all, :conditions => ["agent_contacts.id IN (?) OR agent_contacts.contact_id IN (?)", params[:agent_contact_id], params[:contact_name]])
      @send_to = @agent_contacts.collect(&:email).join(",")
      @contact_name = @agent_contacts.collect(&:first_name)
      @current_property = @property
      obj_images = @property.images
      @main_image = obj_images.blank? ? "/images/no-image.jpeg" : (obj_images.first.blank? ? "/images/no-image.jpeg" : obj_images.first.public_filename)
      @thumb_image1 = obj_images.blank? ? "/images/no-image.jpeg" : (obj_images.second.blank? ? "/images/no-image.jpeg" : obj_images.second.public_filename)
      @thumb_image2 = obj_images.blank? ? "/images/no-image.jpeg" : (obj_images.third.blank? ? "/images/no-image.jpeg" : obj_images.third.public_filename)
      @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Header"])
      @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Footer"])
      @side_image = find_image("Side Image")
      @host = request.host
      respond_to do |format|
        if @email.save
          @agent_contacts.each do |agent_contact|
            @agent_contact_email = agent_contact.email
            UserMailer.deliver_email_configuration(@email,agent_contact,@agent_contact_email,@current_property,@agent,@office,@property,@main_image,@thumb_image1,@thumb_image2,@header_image,@footer_image,@side_image,@host)
          end
          nil_session_listing
          @agent_contacts.each do |contact|
            @contact_note = ContactNote.new({
                :agent_user_id => current_user.id,
                :note_type_id => "13",
                :address => "#{@property.street_number.blank? ? '' : @property.street_number} #{@property.street.blank? ? '' : @property.street}, #{@property.suburb.blank? ? '' : @property.suburb}",
                :note_date => Date.today.strftime("%Y-%m-%d"),
                :agent_contact_id => contact.id,
                :property_id => @current_property.id,
                :description => "An email has been sent to #{contact.username.blank? ? (contact.full_name.blank? ? contact.first_name : contact.full_name) : contact.username} (#{contact.email unless contact.blank?})."
              })
            @contact_note.save
          end
          flash[:notice] = "Email was successfuly sent."
          format.html { redirect_to session[:url_search] }
        else
          format.html { render :action => "new"}
        end
      end
    else
      flash[:notice] = "Please select contact first."
      redirect_to new_agent_office_property_match_contact_path(@agent,@office,@property)
    end
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
    session[:search_keyword] = params[:search_keyword]
    session[:search_keyword_advanced] = params[:search_keyword_advanced]
  end

  def get_session_array
    session[:array_type] = params[:type]
    session[:array_residential_building] = params[:residential_building]
    session[:array_type_commercial] = params[:type_commercial]
  end

  def nil_session_listing
    session[:contact_type] = nil
    session[:full_name] = nil
    session[:email] = nil
    session[:company] = nil
    session[:assigned_to] = nil
  end

  def populate_regions
    unless params[:state_name].blank?
      @regions = TownCountry.find_all_by_state_name(params[:state_name]).map{|x|[x.name, x.name]}.unshift(['Please select', ''])
    end
    render :layout => false
  end

  def populate_suburbs
    @regions = TownCountry.find_all_by_state_name(params[:state_name]).map{|x|[x.name,x.name]}.unshift(['Please select', ''])
    unless params[:region_name].blank?
      @suburbs = Suburb.find_all_by_region_name(params[:region_name]).map{|x|[x.name, x.name]}.unshift(['Please select', ''])
    end
    render :layout => false
  end

  private

  def require_accepted
    unless proccess_selected?
      flash[:notice] = "Please select contact first."
      redirect_to agent_office_property_match_contacts_path(@agent,@office,@property)
    end
  end

  def next_step
    @match_contact = MatchContact.find(:first, :conditions => ["match_contacts.property_id = #{@property.id} AND match_contacts.agent_contact_id IS NOT NULL"])
    @match_contact = MatchContact.new(params[:match_contact])
  end

  def proccess_selected?
    !!next_step
  end

  def prepare_search
    @listing_options = ['Commercial', 'ResidentialSale', 'ProjectSale']
    @property_type = PropertyType.find(:all, :conditions => ["property_types.category != 'Commercial'"]).map{|x|[x.name,x.name]}.unshift('Please select', nil)
    @business_type = PropertyType.business.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    @price_from = ['0..10000']
    @suites = (1..30).map{|x|[x,x]}.unshift(["Please select", ''])
    @ranges = (1..30).map{|x|[x,x]}.unshift(["Please select", ''])
    @type_options = PropertyType.find(:all, :conditions => ["property_types.category = ?", "Commercial"])
    @residential_type = PropertyType.find(:all, :conditions => ["property_types.category = ?", "Residential"])
    @available_options = ['available','sold','leased','withdrawn','underoffer','draft']
    @states = State.find_all_by_country_id(12).map{|x|[x.name,x.name]}.unshift(['Please select', ''])
    @regions = TownCountry.all
    @internal_agents = AgentUser.find(:all, :conditions => ["developer_id = ? AND office_id = ?", @agent.developer_id, @office.id])
    @external_agents = AgentContact.find(:all, :conditions => ["office_id = ?", @office.id])
    @external_companies = AgentCompany.find(:all)
    @residential_buildings = Property.find(:all, :conditions => ["type = ? AND office_id = ?", "NewDevelopment", @office]).collect(&:suburb).uniq
    @internal_features_residential = Feature.internal.find(:all, :conditions => ["property_type = ?", "ResidentialSale"])
    @external_features_residential = Feature.external.find(:all, :conditions => ["property_type = ?", "ResidentialSale"])
    @security_features_residential = Feature.security.find(:all, :conditions => ["property_type = ?", "ResidentialSale"])
    @general_features_residential = Feature.general.find(:all, :conditions => ["property_type = ?", "ResidentialSale"])
    @location_features_residential = Feature.location.find(:all, :conditions => ["property_type = ?", "ResidentialSale"])
    @lifestyle_features_residential = Feature.lifestyle.find(:all, :conditions => ["property_type = ?", "ResidentialSale"])

    @commercial_types = PropertyType.commercial.all
    @residential_types = PropertyType.residential.all
  end
end
