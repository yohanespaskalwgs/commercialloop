class HeardFromsController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation

  def index
    @heard_from = HeardFrom.new
  end

  def show
  end

  def new
  end

  def edit
    @heard_from = HeardFrom.find(params[:id])
  end

  def create
   @heard_from = HeardFrom.new(params[:heard_from].merge(:creator_id => current_user.id, :status => "heard_office"))

    respond_to do |format|
      if @heard_from.save
        flash[:notice] = 'HeardFrom was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
    @heard_from = HeardFrom.find(params[:id])

    respond_to do |format|
      if @heard_from.update_attributes(params[:heard_from])
        flash[:notice] = 'HeardFrom was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @related = AgentContact.find(:all, :conditions => { :heard_from_id => params[:id] })
    @related.each { |x| x.update_attributes(:heard_from_id => nil) }
    @heard_from = HeardFrom.find(params[:id])
    @heard_from.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def prepare_creation
    @heards = HeardFrom.paginate(:all, :conditions => ["`creator_id` is NULL OR `creator_id` = ?", current_user.id], :order => "`created_at` DESC", :page =>params[:page], :per_page => 10)
  end
end
