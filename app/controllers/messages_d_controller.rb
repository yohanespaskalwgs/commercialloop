#--------------------------------------------------
# for developer version messages centre
#--------------------------------------------------
class MessagesDController < ApplicationController
  layout 'developer', :except => [:add_estimation, :update_estimation]
  prepend_before_filter :login_required
  require_role 'DeveloperUser'
  prepend_before_filter :get_developer, :prepare_data
  before_filter :get_sort_by, :only => %w(client unread_client archives_client commercial_loop_crm unread_commercial_loop_crm archives_commercial_loop_crm)
  before_filter :get_client_message, :only => %w(edit_client update_client)
  before_filter :get_admin_message, :only => %w(edit_commercial_loop_crm update_commercial_loop_crm)

  before_filter :check_access, :only => %w(commercial_loop_crm unread_commercial_loop_crm archives_commercial_loop_crm new_commercial_loop_crm create_commercial_loop_crm edit_commercial_loop_crm update_commercial_loop_crm)
  before_filter :show_message, :only => %w(show internal)

  def client
    @messages = current_user.client_messages.inbox.unquote.filter_by_office(params[:office]).filter_by_category(params[:category]).order_by(params[:sortby], current_user).paginate(:page => params[:page])
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Client Messages'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end

  def unread_client
    @messages = current_user.unread_client_messages.filter_by_category(params[:category]).order_by(params[:sortby], current_user).paginate(:page => params[:page])
    render :action => "client"
  end

  def archives_client
    @messages = current_user.client_messages.filter_by_category(params[:category]).archives.order_by(params[:sortby], current_user).paginate(:page => params[:page])
    render :action => "client"
  end

  def quote_required
    @messages = current_user.client_messages.filter_by_category(params[:category]).quote_required.order_by(params[:sortby], current_user).paginate(:page => params[:page])
    render :action => "client"
  end

  def quote_approved
    @messages = current_user.client_messages.filter_by_category(params[:category]).quote_approved.order_by(params[:sortby], current_user).paginate(:page => params[:page])
    render :action => "client"
  end

  def new_client
    @message = AgentMessage.new
  end

  def create_client
    params[:recipients] ||= []

    @message = AgentMessage.new(params[:message].merge(:sender_id => current_user.id))
    @message.recipients = params[:recipients]

    if @message.save_with_file
      @message.assign_recipients(@message.recipients)

      @message.notify_recipients

      flash[:notice] = "Message has been sent."
      redirect_to client_developer_messages_path(@developer)
    else
      render :action => "new_client"
    end

  end

  def update_client
    @message.attributes = params[:message]
    if @message.save_with_file

      redirect_to developer_message_path(@developer, @message)
      flash[:notice] = "Message has been updated."
    else
      render :action => "edit_client"
    end
  end

#  def commercial_loop_crm
#    @messages = current_user.admin_messages.inbox_with_client_unread.filter_by_category(params[:category]).order_by(params[:sortby], current_user).paginate(:page => params[:page])
#    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Commercial Loop Messages'")
#    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
#  end

  def commercial_loop_crm
    @messages = current_user.admin_messages.inbox_with_client_unread.filter_by_category(params[:category]).order_by(params[:sortby], current_user).paginate(:page => params[:page])
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Commercial Loop Messages'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end

  def topic_commercial_loop_crm
    @category = MessageCategory.find(params[:category_id])
    render :layout => false
  end

  def unread_commercial_loop_crm
    @messages = current_user.unread_admin_messages.filter_by_category(params[:category]).order_by(params[:sortby], current_user).paginate(:page => params[:page])
    render :action => "commercial_loop_crm"
  end

  def archives_commercial_loop_crm
    @messages = current_user.admin_messages.archives_with_client_read.filter_by_category(params[:category]).order_by(params[:sortby], current_user).paginate(:page => params[:page])
    render :action => "commercial_loop_crm"
  end

  def new_commercial_loop_crm
    @message = AdminMessage.new
  end

  def create_commercial_loop_crm
    @message = AdminMessage.new(params[:message].merge(:sender_id => current_user.id))
    if @message.save_with_file
      AdminUser.all.each do |admin_user|
          MessageRecipient.create(:message_id => @message.id, :user_id => admin_user.id.to_i)
          MessageNotification.deliver_new_message(admin_user, @message)
      end
      flash[:notice] = "Message has been sent."
      redirect_to commercial_loop_crm_developer_messages_path(@developer)
    else
      render :action => "new_commercial_loop_crm"
    end
  end

  def update_commercial_loop_crm
    @message.attributes = params[:message]
    if @message.save_with_file

      redirect_to developer_message_path(@developer, @message)
      flash[:notice] = "Message has been updated."
    else
      render :action => "edit_commercial_loop_crm"
    end
  end

  def show
    @status = "client"
  end

  def internal
    @status = "internal"
    @developer_internal = true
    render :action => "show"
  end

  def assign_recipients
    @message = Message.find(params[:id])
    @message.assign_recipients(params[:recipients])
    respond_to do |format|
      format.js {
        render :update do |page|
          page << "toggle_recipients_selector();"
        end
      }
    end
  end

  def update_quote
    @message = Message.find(params[:message_id])
    @message.update_attribute(:quote, (@message.quote == true ? false : true))
    redirect_to developer_message_path(@developer, @message)
  end

  def update_quote_approved
    @message = Message.find(params[:message_id])
    @message.update_attribute(:quote_approved, true)
    redirect_to developer_message_path(@developer, @message)
  end

  def update_priority
    @message = Message.find(params[:message_id])
    @message.update_attribute(:priority, params[:priority])
    redirect_to client_developer_messages_path(@developer)
  end

  def add_estimation
    @days = (0..30).map{|x|[x,x]}
    @hours = (0..23).map{|x| [x,x] }
    @minutes = (0..59).map{|x| [x,x] }
    @message = Message.find(params[:message_id])
  end

  def update_estimation
    @days = (0..30).map{|x|[x,x]}
    @hours = (0..23).map{|x| [x,x] }
    @minutes = (0..59).map{|x| [x,x] }

    @message = Message.find(params[:message_id])
    @message.attributes = params[:message].merge(:estimated_at => Time.zone.now)
    if @message.save
      @comment = MessageComment.new({:comment => "The Estimated completion time has changed to #{@message.estimation_day}d #{@message.estimation_hour}h #{@message.estimation_min}m", :commenter_id => current_user.id})
      @message.add_comment(@comment) if @comment.save
      flash[:notice] = "Estimation Message has been updated."
      redirect_to add_estimation_developer_messages_path(@developer)+"?message_id=#{@message.id}"
    else
      render :action => "add_estimation"
    end
  end

  private

  def get_developer
    @developer = Developer.find(params[:developer_id])
    if current_user
      render_optional_error_file(401) and return false unless current_user.developer_id.to_s == params[:developer_id] || current_user.class.name.include?("AdminUser")
    end
    @body_id = "messages"
  end

  def prepare_data
    @priorities = [['Low', 'Low'],['Medium', 'Medium'],['High', 'High'],['Critical', 'Critical']]
  end

  def check_access
    #Everyone has full access to messages.
    #render_optional_error_file(401) and return false unless current_user.full_access?
  end

  def get_sort_by
    params[:sortby] ||= 'date'
  end

  def get_client_message
    @message = (current_user.client_messages.find(params[:id]) if current_user.attribute_present?(:client_messages)) unless current_user.blank?
    unless @message.blank?
      redirect_to :action => 'client' and return false unless @message.editable_by?(current_user)
    else
      redirect_to :action => 'client' and return false
    end
  end

  def get_admin_message
    @message = (current_user.admin_messages.find(params[:id]) if current_user.attribute_present?(:admin_messages)) unless current_user.blank?
    unless @message.blank?
      redirect_to :action => 'commercial_loop_crm' and return false unless @message.editable_by?(current_user)
    else
      redirect_to :action => 'commercial_loop_crm' and return false
    end
  end

  def show_message
    @message = Message.find(params[:id])
    @message.mark_read_for!(current_user)
  end
end
