class ChangesLogsController < ApplicationController
  layout 'agent'

  prepend_before_filter :check_office_ownership,:except =>[:property_ipn,:property_notification]
  prepend_before_filter :get_agent_and_office
  before_filter :login_required

  def index
    @all_logs = ChangesLog.find(:all, :conditions => "office_id = #{@office.id}")
    if @all_logs.present?
      property_ids = @all_logs.map{|a| a.property_id}
      @properties = Property.find_with_deleted(:all, :conditions => "id in (#{property_ids.join(",")}) and office_id = #{@office.id}", :order => "updated_at DESC")
    end
  end

  def show
    @property = Property.find(:first, :conditions => "id = #{params[:id]} and office_id = #{@office.id}")
    if @property.present?
      @property_logs = ChangesLog.find(:all, :conditions => "property_id = #{@property.id}")
    end
  end

end
