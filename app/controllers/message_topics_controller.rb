class MessageTopicsController < AdminController
  before_filter :get_category
  before_filter :get_topic, :only => %w(edit update destroy)

  def index
    @topics = @category.message_topics.paginate(:page => params[:page])
  end

  def new
    @topic = @category.message_topics.new
  end

  def create
    @topic = @category.message_topics.build(params[:message_topic])
    if @topic.save
      flash[:notice] = "Topic added."
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def update
    if @topic.update_attributes(params[:message_topic])
      flash[:notice] = "Topic updated."
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def destroy
    @topic.destroy if @topic.can_delete?
    respond_to do |format|
      format.js {
        render :update do |page|
          page.visual_effect :highlight, "topic_#{@topic.id}"
          page["topic_#{@topic.id}"].fadeOut
        end
      }
    end
  end


  private
  def get_category
    @category = MessageCategory.system_categories.find(params[:message_category_id])
  end

  def get_topic
    @topic = @category.message_topics.find(params[:id])
  end
end
