class PropertyListenersController < ApplicationController
  layout false
  before_filter :find_agent_or_developer_by_access_key, :only => [:create]
  prepend_before_filter :get_agent_and_office
  append_before_filter :prepare_creation, :only => [:residential_sale, :residential_lease, :new_property]
  skip_before_filter :verify_authenticity_token

  include PropertiesHelper

  #form -localhost:3000/agents/1/offices/1/property_listeners/new/residential_sale
  #save -localhost:3000/agents/1/offices/1/property_listeners/new_property
  #update -localhost:3000/agents/1/offices/1/property_listeners/update_property

  def residential_lease
    @ptype = "residential_lease"
  end

  def residential_sale
    @ptype = "residential_sale"
  end

  def holiday_lease
    @ptype = "holiday_lease"
  end

  def commercial
    @ptype = "commercial"
  end

  def commercial_building
    @ptype = "commercial_building"
  end

  def project_sale
    @ptype = "project_sale"
  end

  def business_sale
    @ptype = "business_sale"
  end

  def new_property
  end

  def update_property
    params[:feature_ids] ||= []
    success = false
    errors = ''

    if !params[:id].blank?
      if !params[:listing_type].blank?
        @property = Property.find params[:id]
        last_status = @property.status
        previous_agent_user_id = @property.agent_user_id
        previous_agent_contact_id = @property.agent_contact_id

        @property.description= decoded_and_converted_to(@property.description)
        @ptype = @property.type.to_s.underscore
        @property.detail.unavailable_date = params[:unavailable_date] if @property.detail.class.name.underscore == "holiday_lease_detail"
        @property.detail.property_type = @property.property_type if @property.detail.respond_to?(:property_type)
        @property.detail.country = @property.country if @property.detail.respond_to?(:country)
        @property.detail.deal_type = @property.deal_type if @property.detail.respond_to?(:deal_type)

        if @property.is_a?(HolidayLease) && params[:rental_season]
          @property.rental_seasons.destroy_all
          save_rental_seasons
        end

        @param_property= property_params(params)
        @param_detail= detail_params(params)

        @property.attributes = @param_property

    #    if current_user.allow_property_owner_access? and (@office.property_owner_access and @office.property_draft_for_updated)
    #      @property.status = 6
    #      @property.role_6_mode = true
    #    end

        if params[:save_status] == Property::DRAFT
          @property.status = 6
          is_send = false
        end
       @property.agent_user_id = params[:primary_contact_id].blank? ? previous_agent_user_id : params[:primary_contact_id]
       @property.office_id = params[:office_id]
       @property.agent_id = params[:agent_id]

        if @property.detail.is_a?(CommercialDetail)
          @property.detail.deal_type = @property.deal_type
        end

        if(last_status != 1 && @property.status == 1)
          @property.activation = true
        else
          @property.activation = nil
        end
        @property.update_attribute(:deactivation,nil) if (last_status == 6 && @property.status != 6)
        errors = @property.detail.errors unless @property.detail.update_attributes(@param_detail)
        errors = @property.errors unless @property.save
        success = true if errors == ''

        if !@property.vendor_first_name.blank? and !@property.vendor_last_name.blank? and @property.agent_contact.blank?
          @property.update_attribute(:agent_contact_id, Property.create_import_contact(params))
          @property.agent_contact.create_note(@property.id, "Property Listed", 19, nil) unless @property.agent_contact.blank?
        else
          (@property.update_contact_detail if previous_agent_contact_id == @property.agent_contact.id) unless @property.agent_contact.blank?
        end
      else
        errors= "Action can't be blank"
      end
    else
      errors= "Property Id can't be blank"
    end

    property_data = []
    property_data << {:success => success, :error_message => errors, :id => (@property.id unless @property.blank?), :data => params.delete_if{|x,y| %w(action submit controller).include?(x)}}
    respond_to do |format|
      format.xml { render :xml => property_data}
    end
  end

  private
  def prepare_creation
    if %w(residential_lease residential_sale commercial commercial_building holiday_lease project_sale business_sale).include?(params[:listing_type])
      pclass = Kernel.const_get get_property_type(params[:listing_type])
      @ptype = params[:listing_type]
      if request.post?
        is_send = false
        success = false
        errors = ''

        @param_property= property_params(params)
        @param_detail= detail_params(params)

        @property = pclass.new @param_property
        @detail = @property.build_detail @param_detail

        @property.description= decoded_and_converted_to(@property.description)
        @property.detail.unavailable_date = params[:unavailable_date] if @property.detail.class.name.underscore == "holiday_lease_detail"
        @detail.deal_type = @property.deal_type if @detail.respond_to?(:deal_type)
        @detail.property_type = @property.property_type if @detail.respond_to?(:property_type)
        @detail.country = @property.country if @detail.respond_to?(:country)
        save_rental_seasons if params[:rental_season]

        @feature_ids = @property.feature_ids
        #holiday_lease_duplicate if params[:holiday_duplication]

        if @property.new_record?
          @property.agent_user_id = params[:primary_contact_id]
        end

        if params[:save_status] == "Draft"
          @property.status = 6
          @property.deactivation = true
          is_send = false
        end

        @property.office_id = params[:office_id]
        @property.agent_id = params[:agent_id]

        if @property.save
          @property.update_attribute(:agent_contact_id, Property.create_import_contact(params))
          @property.update_attribute(:deactivation, nil) if @property.status == 1
          @property.agent_contact.create_note(@property.id, "Property Listed", 19, nil) unless @property.agent_contact_id.nil?
          country = Country.find_by_name(@office.country)
          init_exports(country.portal_countries)
          success = true
        else
          errors = @property.errors
        end

        property_data = []
        property_data << {:success => success, :error_message => errors, :id => (@property.id unless @property.blank?), :data => params.delete_if{|x,y| %w(action submit controller).include?(x)}}
        respond_to do |format|
          format.xml { render :xml => property_data}
        end
      else
        @property = pclass.new
        @detail = @property.build_detail
      end
    end
  end

  def init_exports(portal_countries)
    unless portal_countries.blank?
      allowed = [1,5,6].include?(@property.status)
      if allowed
        portal_countries.each do |p_c|
          portal_autosubcribe = PortalAutosubcribe.find_by_portal_id_and_office_id(p_c.portal.id,@office.id)
          unless portal_autosubcribe.blank?
            cek = PropertyPortalExport.find_by_property_id_and_portal_id(@property.id,p_c.portal.id)
            PropertyPortalExport.create(:property_id => @property.id, :portal_id => p_c.portal.id) if cek.blank?
          end
        end
      end
    end
  end

  def save_rental_seasons
    params[:rental_season].each_with_index do |rental_season,index|
      save_rental_sessions = RentalSeason.new(rental_season)
      @property.rental_seasons << save_rental_sessions
    end
  end

  def rental_params
    rparams = [
  {"start_date"=>params[:normal_start_date], "position"=> "0", "minimum_stay"=>params[:normal_minimum_stay],
    "season"=>"normal", "end_date"=> params[:normal_end_date]},
  {"start_date"=>params[:mid_start_date], "position"=>"1", "minimum_stay"=>params[:mid_minimum_stay],
    "season"=>"mid", "end_date"=>params[:mid_end_date]},
  {"start_date"=>params[:high_start_date], "position"=>"1", "minimum_stay"=>params[:high_minimum_stay],
    "season"=>"high", "end_date"=>params[:high_end_date]},
  {"start_date"=>params[:peak_start_date], "position"=>"1", "minimum_stay"=>params[:peak_minimum_stay],
    "season"=>"peak", "end_date"=>params[:peak_end_date]}]
    return rparams
  end
end
