class ProjectDesignTypesController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new, :refresh]
  before_filter :get_agent_and_office

  def index
    @project_design_types = ProjectDesignType.find(:all, :conditions => ["`office_id` = ?", @office.id])
  end

  def show
  end

  def new
  end

  def edit
    @project_design_type = ProjectDesignType.find(params[:id])
  end

  def create
   @project_design_type = ProjectDesignType.new(params[:project_design_type].merge(:office_id => @office.id))
    respond_to do |format|
      if @project_design_type.save
        flash[:notice] = 'ProjectDesignType was successfully created.'
        format.html { redirect_to :action => 'new' }
      else
        format.html { render :action => "new" }
      end
    end
  end

  def update
    @project_design_type = ProjectDesignType.find(params[:id])
    respond_to do |format|
      if @project_design_type.update_attributes(params[:project_design_type])
        flash[:notice] = 'ProjectDesignType was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @project_design_type = ProjectDesignType.find_by_id(params[:id])
    @project_design_type.destroy unless @project_design_type.blank?
    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def refresh
    @project_design_types = ProjectDesignType.find(:all, :conditions => ["`office_id` = ?", params[:office_id]]).map{|c| [c.name, c.id]}
    return(render(:json => @project_design_types))
  end
end
