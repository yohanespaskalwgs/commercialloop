class TaskTypesController < ApplicationController
  layout false

  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  before_filter :new_task_type, :only => [:index, :create]
  before_filter :get_task_type, :only => [:edit, :update, :destroy]
  append_before_filter :prepare_creation

  def index;end

  def show;end

  def new;end

  def edit;end

  def create
    @task_type.office_id = @office.id

    respond_to do |format|
      if @task_type.save
        flash[:notice] = 'Task Type was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
    respond_to do |format|
      if @task_type.update_attributes(params[:task_type])
        flash[:notice] = 'Task Type was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @task_type.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  protected

  def new_task_type
    @task_type = TaskType.new(params[:task_type])
  end

  def get_task_type
    @task_type = TaskType.find(params[:id])
  end

  def prepare_creation
    @task_types = TaskType.paginate(:all, :conditions => ["`office_id` is NULL OR `office_id` = ?", @office.id], :order => "`created_at` DESC", :page =>params[:page], :per_page => 10)
  end

end
