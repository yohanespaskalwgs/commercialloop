class AgentsController < ApplicationController
  layout 'developer'

  require_role ['AgentUser', 'DeveloperUser']

  skip_before_filter :verify_authenticity_token, :only => [:search]
  before_filter :get_developer_and_agent,:except => [:api_details]
  before_filter :prepare_form, :only => [:new, :edit, :create, :update]
  before_filter :add_or_edit_active_help, :only => [:new, :edit]

  # GET /agents
  # GET /agents.xml
  def index
    return access_denied if params[:developer_id].blank?
    session[:sorted_by] = "agents.created_at #{params[:sort_type]}" if params[:sort_by] == "created_at"
    session[:sorted_by] = "agents.name #{params[:sort_type]}" if params[:sort_by] == "name"
    session[:sorted_by] = "agents.country #{params[:sort_type]}" if params[:sort_by] == "country"
    session[:sorted_by] = "agents.holder_first_name #{params[:sort_type]}" if params[:sort_by] == "holder_first_name"
    conditions = [" "]
    unless params[:country].blank?
      conditions[0] += " AND agents.country = "
      conditions << "'#{params[:country]}'"
    end
    unless params[:status].blank?
      conditions[0] += " AND agents.status = "
      conditions << "'#{params[:status]}'"
    end
    unless params[:client].blank?
      conditions[0] += " AND agents.id = "
      conditions << "'#{params[:client]}'"
    end
    conditions << " AND agents.deleted_at IS NULL "
    count_properties_agent = "(select count(id) from properties where agent_id = agents.id and deleted_at IS NULL) as count_properties, (select count(id) from users where office_id = offices.id and type = 'AgentUser' and deleted_at IS NULL) as count_agents"
    order_by = "ORDER BY count_active #{params[:sort_type]}" if params[:sort_by] == "offices.active.count"
    order_by = "ORDER BY count_properties #{params[:sort_type]}" if params[:sort_by] == "properties.count"
    order_by = "ORDER BY count_agents #{params[:sort_type]}" if params[:sort_by] == "agent_users.count"

    @agents = Agent.find_by_sql("SELECT agents.* , COUNT( offices.id ) AS count_active, #{count_properties_agent} FROM agents LEFT OUTER JOIN offices ON offices.agent_id = agents.id AND offices.status = 'active' WHERE agents.developer_id = #{params[:developer_id]} #{conditions}  GROUP BY agents.id #{(order_by unless order_by.blank?)}")
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Client Summary'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /agents/1
  # GET /agents/1.xml
  def show
    unless check_agent_ownership
      return access_denied
    else
      respond_to do |format|
        format.html # show.html.erb
      end
    end
  end

  # GET /agents/new
  # GET /agents/new.xml
  def new
    @agent = Agent.new
    @agent_developer_accesses_ids = []
    @developer.developer_users.each do |dev|
      unless dev.limited_client_access?
        @agent_developer_accesses_ids << dev.id
      end
    end

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /agents/1/edit
  def edit
    return access_denied unless check_agent_ownership
    @agent_developer_accesses_ids = @agent.agent_developer_accesses.map(&:developer_user_id)
  end

  # POST /agents""
  # POST /agents.xml
  def create
    @agent = @developer.agents.build params[:agent]
    (params[:developer_accesses] ||= []).each do |access|
      @agent.agent_developer_accesses.build(:developer_user_id => access)
    end
    respond_to do |format|
      if @agent.save
        flash[:notice] = 'Client was successfully created.'
        format.html { redirect_to :action => 'index' }
        format.xml  { render :xml => @agent, :status => :created, :location => @agent }
      else
        format.html {
          @agent_developer_accesses_ids = @agent.agent_developer_accesses.map(&:developer_user_id)
          render :action => "new"
        }
      end
    end
  end

  # PUT /agents/1
  # PUT /agents/1.xml
  def update
    return access_denied unless check_agent_ownership
    @agent_developer_accesses_ids = (params[:developer_accesses] || []).map(&:to_i)
    respond_to do |format|
      if @agent.update_attributes(params[:agent])
        @agent.agent_developer_accesses.clear
        @agent_developer_accesses_ids.each do |access|
          @agent.agent_developer_accesses.create(:developer_user_id => access)
        end
        @agent.apply_to_offices

        flash[:notice] = 'Client was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html {
          render :action => "edit"
        }
      end
    end
  end

  # DELETE /agents/1
  # DELETE /agents/1.xml
  def destroy
    return access_denied unless check_agent_ownership
    @agent = @agents.find params[:id]
    @agent.destroy

    respond_to do |format|
      format.html { redirect_to(agents_url) }
    end
  end

  # only for developer user
  def offices

  end

  def search
    agents = Agent.search(params[:q])
    jsons = []
    agents.each{|a| jsons << {'id' => a.id, 'name' => a.name, 'href' => developer_agents_path(@developer, :client => a.id)} } unless agents.nil?
    text = {"results" => jsons, "total" => agents.length }
    render :json => text
  end

  def delete_agent
    agent = Agent.find(params[:id])
    agent.delete_agent
    respond_to do |format|
      flash[:notice] = 'Agent was successfully deleted.'
      format.html { redirect_to :action => 'index' }
    end
  end

  def api_details
    @agent = Agent.find(params[:id])
    @developer = @agent.developer
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'API'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end

  private

  def add_or_edit_active_help
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add/Edit Client'")
    @page_status = current_user.check_user_page_status(@active_help.id)
  end

  def get_developer_and_agent
    if current_user.class.name.include?('AgentUser')
      unless params[:id].blank?
        @agent = current_user.office.agent
        render_optional_error_file(401) unless @agent == Agent.find(params[:id])
      end
    else
      render_optional_error_file(401) unless current_user.developer_id.to_s == params[:developer_id] || current_user.class.name.include?("AdminUser")
      @developer = current_user.class.name.include?("AdminUser") ? Developer.find_by_id(params[:developer_id]) : current_user.developer
      @agents = @developer.agents unless @developer.blank?
      @agent = @agents.find params[:id] unless params[:id].blank?
      @agent = @agents.find params[:id] unless params[:id].blank?
    end
  end

  def prepare_form
    countries = Country.find(:all, :conditions => "display_country = 1")
    @countries = countries.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    statuses = @agent.nil? ? ["Active"] : Agent::STATUS
    @status = statuses.map{|s| [s.capitalize, s.downcase]}.unshift(['Please select', nil])
    @contacts = @developer.developer_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil])
  end

end
