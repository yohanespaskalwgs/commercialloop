class NoteTypesController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation

  def index
    @note_type = NoteType.new
  end

  def show
  end

  def new
  end

  def edit
    @note_type = NoteType.find(params[:id])
  end

  def create
   @note_type = NoteType.new(params[:note_type].merge(:creator_id => current_user.id, :status => "contact_note"))

    respond_to do |format|
      if @note_type.save
        flash[:notice] = 'NoteType was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
    @note_type = NoteType.find(params[:id])
    respond_to do |format|
      if @note_type.update_attributes(params[:note_type])
        flash[:notice] = 'NoteType was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @related = ContactNote.find(:all, :conditions => { :note_type_id => params[:id] })
    @related.each { |x| x.update_attributes(:note_type_id => nil) }
    @note_type = NoteType.find_by_id(params[:id])
    @note_type.destroy unless @note_type.blank?

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def prepare_creation
    @notes = NoteType.paginate(:all, :conditions => ["(`creator_id` is NULL OR `creator_id` = ?) and`status` = 'contact_note'", current_user.id], :order => "`created_at` DESC", :page =>params[:page], :per_page => 10)
  end
end
