class PaypalIpnController < ApplicationController
  include ActiveMerchant::Billing::Integrations

  skip_before_filter :login_required
  protect_from_forgery :except => [:notify, :paypal_ipn, :property_ipn]
  verify :method => :post, :only => [:paypal_ipn]

  # Examples IPN Parameters: {"residence_country"=>"US", "tax"=>"0.00", "payer_email"=>"rex_1240028127_per@rorcraft.com",
  #  "amount_per_cycle"=>"2500.00", "next_payment_date"=>"N/A", "receiver_email"=>"rex_1239802854_biz@rorcraft.com",
  #   "profile_status"=>"Cancelled", "outstanding_balance"=>"0.00",
  #   "verify_sign"=>"An5ns1Kso7MWUdW4ErQKJJJ4qi4-A7ifuJh8coCGqgmAqRSsJ6vKjNo1",
  #   "product_type"=>"1", "action"=>"notify",
  #   "txn_type"=>"recurring_payment_profile_cancel",
  #   "time_created"=>"03:44:42 Apr 20, 2009 PDT",
  #   "recurring_payment_id"=>"I-BSG7PNYVUB40",
  #   "initial_payment_amount"=>"0.00", "charset"=>"windows-1252", "controller"=>"paypal_ipn",
  #   "payment_cycle"=>"Monthly", "period_type"=>" Regular", "payer_status"=>"verified", "notify_version"=>"2.8",
  #   "amount"=>"2500.00", "shipping"=>"0.00", "test_ipn"=>"1", "first_name"=>"Test", "last_name"=>"User",
  #   "currency_code"=>"USD", "payer_id"=>"N56KEALG6GCFJ", "product_name"=>"SubscriptionPlans: One Agent"}

  def property_ipn
    pp params
    pp "------------------------------------------------------------------------------------------------"
    pp "------------------------------------------------------------------------------------------------"
    pp "------------------------------------------------------------------------------------------------"
    pp "------------------------------------------------------------------------------------------------"
    pp "------------------------------------------------------------------------------------------------"
    pp "------------------------------------------------------------------------------------------------"
    pp "------------------------------------------------------------------------------------------------"
    raise params
  end

  def notify
    if request.post?
      notify = Paypal::ExpressRecurringNotification.new(request.raw_post)

      # logger.debug notify.inspect

      if notify.acknowledge
        # @payment = Payment.find_by_confirmation(notify.transaction_id) ||
        #   enrollment.invoice.payments.create(:amount => notify.amount,
        #     :payment_method => 'paypal', :confirmation => notify.transaction_id,
        #     :description => notify.params['item_name'], :status => notify.status,
        #     :test => notify.test?)
        debugger
        @sub_history = SubscriptionHistory.new_from_notification(notify)
        @sub_history.save unless @sub_history.blank?

        case notify.type
        when "recurring_payment_profile_cancel"
          begin
            profile = RecurringPaymentProfile.find_by_gateway_reference(notify.profile_id)
            @subscription = profile.subscriptions.first #could optimise this

            # NB: StandardError (Cannot cancel subscription through gateway: Internal Error):
            #     Because it is already cancelled from Paypal site.
            SubscriptionManager.unsubscribe(@subscription.id, :gateway => false)
          end
        when "recurring_payment_profile_created"

        when "recurring_payment_profile_....paid?" # TODO
          # save payment history
        end

      end
    end
  ensure
    render :nothing => true
  end

  def paypal_ipn
		notify = Paypal::Notification.new(request.raw_post)
		sh = SubscriptionHistory.new_from_notification(notify, params[:recurring_payment_id])
    unless sh.blank?
      sh.profile_status = params[:profile_status]
      sh.payer_email = params[:payer_email]
      sh.payer_id = params[:payer_id]
      sh.amount = params[:amount]
      sh.currency_code = params[:currency_code]
      sh.save
      subscription = sh.subscription
      office = Office.find(subscription.office_id)
      if notify.acknowledge
        begin
          total_price = (subscription.net_amount + subscription.taxes_amount) / 100.0
          if notify.complete? and total_price.to_f == params[:amount].to_f
            office.update_attribute(:status, 'active')
            sp = office.subscription_plan
            subscription.update_attributes({:next_payment_date => sp['next_payment_date'], :status =>'ok', :canceled_at => nil, :ends_on => nil})
            sh.update_attribute(:office_id, subscription.office_id)
          elsif notify.type == "recurring_payment"
            sh.update_attribute(:error_log, sh.error_log + ' // ' + 'Failed to verify Paypal notification')
            office.update_attribute(:status, 'inactive')
            subscription.update_attribute(:status, 'invalid')
          end
        rescue => e
          #sh.update_attribute(:error_log, sh.error_log + ' // ' + 'Failed to verify Paypal notification - Exception')
          office.update_attribute(:status, 'inactive')
          subscription.update_attribute(:status, 'invalid')
        end
      end
		end


    render :nothing => true
  end

end
