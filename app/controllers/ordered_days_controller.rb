class OrderedDaysController < ApplicationController
  skip_before_filter :verify_authenticity_token
  def create
    ordered_day = Date.parse(params[:ordered_date])
    OrderedDay.create(:date => ordered_day, :rental_season_id => params[:rental_date_id])
    render :nothing => true
  end

  def destroy
    d = OrderedDay.find(params[:ordered_day_id])
    d.destroy unless d.nil?
    render :nothing => true
  end
end
