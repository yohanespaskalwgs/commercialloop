
  require 'will_paginate/array'
class SearchResultsController < ApplicationController
  layout 'agent'
  before_filter :login_required, :except => [:generate_pdf]
  # before_filter :require_accepted, :only => [:history]
  before_filter :get_pdf_configuration
  before_filter :email_data, :only => [:email_body_preview, :save_email_configuration]
  prepend_before_filter :get_agent_and_office, :prepare_search
  skip_before_filter :verify_authenticity_token, :only => [:auto_complete, :auto_complete_region, :auto_complete_suburb, :auto_complete_tenant]

  include ActionView::Helpers::TextHelper
  include PropertiesHelper

  def index
    get_session_listing
    get_session_array
    get_session_url
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    if ["Commercial", "Residential"].include?(params[:listing_type])
      feed_locations
    end
    session[:url_search] = request.url
    @listing = params[:listing]
    @type = params[:type]
    @search = params[:search_keyword]
    @properties_filter = Property.find(:all,:include => [:primary_contact],:conditions=>["office_id=?",current_office.id], :select => "properties.suburb,properties.id, properties.property_type,properties.status,properties.primary_contact_id");
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Listings' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    @search = Search.new
    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def feed_locations
    @all_suburbs = @all_suburbs_list = @all_regions = @all_regions_list = ''
    unless params[:regions_id].blank?
      @regions_data = params[:regions_id].split(',')
      @regions_data.map{|cat|
        random = (0..100).to_a.rand.to_s
        unless cat == ''
          @all_regions = params[:regions_id]
          @all_regions_list = @all_regions_list + '<div id="'+random+'"style="padding-left:176px;;width:200px"class="property_type_selected">'+cat+'<a href="#" onclick="remove_region('+random+',\''+cat+'\')">x</a></div>'
        end
      }
    end

    unless params[:suburbs_id].blank?
      @suburbs_data = params[:suburbs_id].split(',')
      @suburbs_data.map{|cat|
        random = (0..100).to_a.rand.to_s
        unless cat == ''
          @all_suburbs = params[:suburbs_id]
          @all_suburbs_list = @all_suburbs_list + '<div id="'+random+'"style="padding-left:176px;;width:200px"class="property_type_selected">'+cat+'<a href="#" onclick="remove_suburb('+random+',\''+cat+'\')">x</a></div>'
        end
      }
    end
  end

  def new
    @search_result = Search.new
  end

  def create
    @search_result = Search.new(params[:search])
    @search_result.agent_id = @agent.id
    @search_result.office_id = @office.id
    @search_result.agent_user_id = current_user.id
    unless @search_result.keyword_name.blank?
      @search_result.save
      respond_to do |format|
        flash[:notice] = "Search results was successfully saved."
        format.html { redirect_to agent_office_search_results_path(@agent,@office) }
      end
    else
      respond_to do |format|
        flash[:notice] = "Search keyword can't be blank."
        format.html { redirect_to agent_office_search_results_path(@agent,@office) }
      end
    end
  end

  def auto_complete
    result = []
    contacts = AgentContact.find(:all, :conditions => ["(`first_name` LIKE ? OR `last_name` LIKE ?) and `agent_id` = ? and `office_id` = ? ", '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', @agent.id, @office.id])
    contacts.each{|x|
      full_name = x.first_name.to_s + " " + x.last_name.to_s
      result << {'id' => "#{x.id.to_s}", 'name' => "#{full_name}" } unless x.first_name.blank? }
    render :json => {"results" => result, "total" => (contacts ? contacts.length : 0) }
  end

  def auto_complete_region
    results = []
    regions = TownCountry.find(:all, :conditions => ["name LIKE ? AND country_id = ?", '%' + params[:q].to_s + '%', "12"])
    regions.each{|x|
      results << {'id' => "#{x.id.to_s}", 'name' => "#{x.name.to_s}"}}
    render :json => {"results" => results, "total" => (regions ? regions.length : 0)}
  end

  def auto_complete_suburb
    results = []
    suburbs = Suburb.find(:all, :conditions => ["name LIKE ? AND country_id = ?", '%' + params[:q].to_s + '%', "12"])
    suburbs.each{|x|
      results << {'id' => "#{x.id.to_s}", 'name' => "#{x.name.to_s}"}}
    render :json => {"results" => results, "total" => (suburbs ? suburbs.length : 0)}
  end

  def auto_complete_tenant
    results = []
    contacts = AgentContact.find(:all, :conditions => ["first_name LIKE ? OR last_name LIKE ?", '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%'])
    contacts.each{|x|
      results << {'id' => "#{x.id.to_s}", 'name' => "#{x.full_name.to_s}"}}
    render :json => {"results" => results, "total" => (contacts ? contacts.length : 0)}
  end

  def find_image(type)
    get_header_footer(@office.id, type)
  end

  def email_configuration
    session[:email_configuration_url] = request.url
    @agent_contacts = AgentContact.find(:all)
    get_session_contact_listing
    nil_session_contact_listing if params[:sort_by].blank? && params[:filter].blank?
  end

  def email_configuration_process
    if params[:contact_name].present? || params[:contact_id].present?
      session[:email_configuration_search] = params[:email_configuration_search]
      session[:contact_name] = params[:contact_name]
      session[:contact_id] = params[:contact_id]

      redirect_to email_preview_new_agent_office_search_result_path(@agent, @office)
    else
      flash[:notice] = "Please select contact first."
      redirect_to email_configuration_new_agent_office_search_result_path(@agent, @office)
    end
  end

  def email_preview
    @email_configuration_url = session[:email_configuration_url]
  end

  def email_body_preview
    render :file => "user_mailer/email_configuration_search", :layout => false
  end

  def save_email_configuration
    @host = request.host
    respond_to do |format|
      if @email.save
        @agent_contacts.each do |agent_contact|
          @agent_contact_email = agent_contact.email
          UserMailer.deliver_email_configuration_search(@email,@agent_contact_email,agent_contact,@agent,@office,@properties,@header_image,@footer_image,@side_image,@host)
        end
        @agent_contacts.each do |contact|
          @properties.each do |property|
            @notes = ContactNote.new({
              :agent_user_id => current_user.id,
              :note_type_id => "13",
              :address => "#{property.street_number.blank? ? '' : property.street_number} #{property.street.blank? ? '' : property.street}, #{property.suburb.blank? ? '' : property.suburb}",
              :note_date => Date.today.strftime("%Y-%m-%d"),
                :agent_contact_id => contact.id,
                :property_id => property.id,
                :description => "An email has been sent to #{contact.username.blank? ? (contact.full_name.blank? ? contact.first_name : contact.full_name) : contact.username} (#{contact.email unless contact.blank?})."
            })
            @notes.save
          end
        end
        nil_session_listing
        flash[:notice] = "Email was successfully sent."
        format.html { redirect_to session[:url_search] }
      else
        format.html { render :action => "email_configuration" }
      end
    end
  end

  def share_on_facebook
    @property = Property.find(params[:id])
    respond_to do |format|
      format.html { render :layout => false }
    end
  end

  def tweet(property)
    unless property.blank?
      case property.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{property.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{property.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'commercial_building'
        tweet = "Building Listing: #{property.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'new_development'
        tweet = "Residential Building: #{property.address.to_s.split(",").join(", ").to_s}" + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'land_release'
        tweet = "Land Release: #{property.address.to_s.split(",").join(", ").to_s}" + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      end
    end
    return tweet
  end

  def preview
    @property = @office.properties.find params[:id], :include => [:detail]
    @detail = @property.detail
    @feature_ids = @property.features
    @ptype = @property.type.to_s.underscore
    if @property.display_address == 0
      @property.street = ""
      @property.street_number = ""
    end
    begin
      @property.description= BlueCloth.new(decoded_and_converted_to(@property.description)).to_html
    rescue
    end
    self.send @ptype
    @header_image = find_header

    respond_to do |format|
      if @ptype == "commercial"
        format.html { render :file => "/properties/preview_commercial", :layout => "iframe"}
      elsif @ptype == "commercialbuilding"
        format.html { render :file => "/properties/preview_commercial_building", :layout => "iframe" }
      else
        format.html { render :layout => 'iframe' }
      end
    end
  end

  def listing
    session[:sort_by] = "properties.updated_at DESC"
    if params[:type] != "all_listing"
      type = get_property_type params[:type]
      @properties = @office.properties.paginate_by_type type ,:include =>[:agent], :page => params[:page] == "" ? 1 : params[:page], :per_page => 20 ,:order =>session[:sort_by]
      @properties_filter = Property.find(:all,:conditions=>["office_id=? AND status = 1",current_office.id]);
      respond_to do |format|
        format.html {render :action => 'index'}
        format.js
      end
    else
      @properties = @office.properties.paginate :all ,:include =>[:agent], :page => params[:page] == "" ? 1 : params[:page], :per_page => 50 ,:order =>session[:sort_by]
      @properties_filter = Property.find(:all,:conditions=>["office_id=? AND status = 1",current_office.id]);
      respond_to do |format|
        format.html {render :action => 'index'}
        format.js
      end
    end
  end

  def populate_combobox
    suburb,type,assigned_to,condition = [],[],[],[]
    condition = [" properties.office_id = ? "]
    condition << current_office.id

    properties = Property.find(:all,:include =>[:primary_contact], :conditions => condition,:select => "properties.primary_contact_id, DISTINCT(properties.suburb),DISTINCT(properties.property_type),users.first_name,users.last_name")
    properties.map{|x|
      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText =>x.suburb.to_s
      } unless x.suburb.blank?;
      type << {
        :type => x.property_type.to_s,
        :typeText =>x.property_type.to_s
      } unless x.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?) ,
        :assigned_toText => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?)
      } unless x.primary_contact.blank?
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    return(render(:json =>{:results => properties.length,:suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,:types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,:team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def uniq(ary)
    ary = ary.dup
    ary.uniq!
    ary
  end

  def uniq!(ary)
    hash = ary_make_hash(ary, 0)
    return nil if ary.length == hash.length

    j = 0
    (0...ary.length).each do |idx|
      if hash.delete(ary[idx])
        ary[j] = ary[idx]
        j += 1
      end
    end
    ary.slice!(0, j)
    ary
  end

  def exact_match_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        if current_user.limited_client_access?
          if !@office.control_level3
            condition[0] += " AND properties.primary_contact_id = ? "
            condition << current_user.id
          end
        elsif current_user.allow_property_owner_access?
          condition[0] += " AND properties.agent_user_id = ?"
          condition << current_user.id
        end

        if(!session[:type].blank? && Property::TYPES.map{|s| s[1]}.include?(session[:type]))
          condition[0] += " AND properties.type = ?"
          condition <<  session[:type]
        end


        if ((current_user.allow_property_owner_access? && session[:status] == 1) || (current_user.allow_property_owner_access?  && session[:status].blank?))|| current_user.allow_property_owner_access?
          session[:status] = "all"
        end

        unless session[:status] == "all"
          unless session[:status].blank?
            condition[0] += " AND properties.status = ?"
            condition <<  session[:status]
          end
        end

        unless session[:suite] == "all" || session[:suite].blank?
          condition[0] += " AND properties.street_number = ?"
          condition << session[:suite]
        end

        unless session[:suburb] == "all" || session[:suburb].blank?
          condition[0] += " AND properties.suburb = ?"
          condition << session[:suburb]
        end

        unless session[:primary_contact_fullname] == "all" || session[:primary_contact_fullname].blank?
          name =  session[:primary_contact_fullname].split(' ')
          condition[0] += " AND users.first_name  = ?"
          condition << name[0]
          unless name[1].blank?
            condition[0] += " AND users.last_name  = ?"
            condition << name[1]
          end
        end

        unless session[:property_type] == "all" || session[:property_type].blank?
          condition[0] += " AND properties.property_type = ?"
          condition << session[:property_type]
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      if @office.is_only_allow_residential?
        condition[0] += " AND properties.type IN ('ResidentialSale', 'NewDevelopment', 'ResidentialLease')"
      elsif @office.is_only_allow_commercial?
        condition[0] += " AND properties.type IN ('Commercial', 'CommercialBuilding')"
      elsif @office.is_only_allow_houseland? || @office.is_allow_all_property_type?
        condition[0] += " AND properties.type = 'ProjectSale'"
      end
    end

    if params[:search_keyword].blank? && params[:listing_type].blank?
      condition[0] += " AND properties.deleted_at IS NULL"
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = Property.build_sorting(params[:sort],params[:dir])
    if !params[:search_keyword].blank?
      @properties = Property.basic_search(params[:search_keyword], params[:start], params[:limit], params[:page],@agent.id,@office.id,order,params[:sort],params[:dir])
    elsif !params[:listing_type].blank?
      get_session_listing
      @properties = Property.advanced_search(params[:listing_type],params[:property_type],params[:business_type],params[:business_category],params[:ownership],params[:price_from],params[:price_to],params[:deal_type],params[:availiability],params[:suites],params[:range_from],params[:range_to],session[:array_type_commercial],session[:array_type],session[:array_residential_building],params[:list_status_commercial],params[:commercial_type],params[:list_status_residential],params[:state],params[:regions_id],params[:suburbs_id],params[:agent_internal],params[:network_agent],params[:agent_external],params[:yield_from],params[:yield_to],params[:building_from],params[:building_to],params[:land_from],params[:land_to],params[:price_from_commercial],params[:price_to_commercial],params[:price_from_residential],params[:price_to_residential],params[:annual_rental_from],params[:annual_rental_to],params[:property_owner],params[:floor_from],params[:floor_to],params[:office_from],params[:office_to],params[:warehouse_from],params[:warehouse_to],params[:rent_from],params[:rent_to],params[:lease_from],params[:lease_to],params[:tenant],params[:search_keyword_advanced],params[:start],params[:limit],params[:page],@agent.id,@office.id,order,params[:sort],params[:dir])
    else
      @properties = Property.paginate(:all, :include => [:primary_contact], :conditions => condition, :order => order, :page =>params[:page], :per_page => params[:limit].to_i)
    end


    if @properties.present?
      @properties.each do |property|
        check_and_update_icon(property)
      end
    end
    results = 0
    if params[:limit].to_i == 50
      if !params[:search_keyword].blank?
        results = Property.basic_search(params[:search_keyword],params[:start],params[:limit],params[:page], @agent.id, @office.id,order,params[:sort],params[:dir]).count
      elsif !params[:listing_type].blank?
        results = Property.advanced_search(params[:listing_type],params[:property_type],params[:business_type],params[:business_category],params[:ownership],params[:price_from],params[:price_to],params[:deal_type],params[:availiability],params[:suites],params[:range_from],params[:range_to],session[:array_type_commercial],session[:array_type],session[:array_residential_building],params[:list_status_commercial],params[:commercial_type],params[:list_status_residential],params[:state],params[:regions_id],params[:suburbs_id],params[:agent_internal],params[:network_agent],params[:agent_external],params[:yield_from],params[:yield_to],params[:building_from],params[:building_to],params[:land_from],params[:land_to],params[:price_from_commercial],params[:price_to_commercial],params[:price_from_residential],params[:price_to_residential],params[:annual_rental_from],params[:annual_rental_to],params[:property_owner],params[:floor_from],params[:floor_to],params[:office_from],params[:office_to],params[:warehouse_from],params[:warehouse_to],params[:rent_from],params[:rent_to],params[:lease_from],params[:lease_to],params[:tenant],params[:search_keyword_advanced],params[:start],params[:limit],params[:page],@agent.id,@office.id,order,params[:sort],params[:dir])
      else
        results = Property.count(:include =>[:primary_contact],:conditions => condition)
      end
    end
    return(render(:json =>{:results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'listing'=>Property.abbrv_properties(x.type),
              'property_type'=>x.property_type.blank? ? "" : x.property_type,
              'address' =>((x.street_number.blank? ? "" : x.street_number)+" "+(x.street.blank? ? "" : x.street.to_s)),
              'suburb' => x.suburb,
              'type' => x.property_type,
              'suite'=>(x.unit_number.blank? ? "" : x.unit_number),
              'sqm'=>((x.detail.blank? ? "" : (x.detail.floor_area.blank? ? "" : x.detail.floor_area)) unless x.type == "NewDevelopment" || x.type == "LandRelease" || x.type == "ResidentialBuilding"),
              'I/E'=> x.ownership.blank? ? "Internal" : x.ownership,
              'price' => Property.check_price(x),
              'assigned_to'=>x.primary_contact.blank? ? '' : x.primary_contact.full_name.to_s.strip,
              'actions'=>Property.check_color(x),
              'status' => Property.status_name(x),
              'fb'=> [x.id,@office.url.blank? ? (x.detail.respond_to?(:deal_type) ? x.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}", tweet(x)]} unless x.blank? }}))
 # rescue Exception => ex
 #   Log.create(:message => "Exact Match Listings ---> errors : #{ex.inspect} ")
 #   return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def similiar_match_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id} AND properties.agent_id = #{current_agent.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        condition = [" properties.agent_id = ? "]
        condition << current_agent.id

        if current_user.limited_client_access?
          if !@office.control_level3
            condition[0] += " AND properties.primary_contact_id = ? "
            condition << current_user.id
          end
        elsif current_user.allow_property_owner_access?
          condition[0] += " AND properties.agent_user_id = ?"
          condition << current_user.id
        end

        if(!session[:type].blank? && Property::TYPES.map{|s| s[1]}.include?(session[:type]))
          condition[0] += " AND properties.type = ?"
          condition <<  session[:type]
        end


        if ((current_user.allow_property_owner_access? && session[:status] == 1) || (current_user.allow_property_owner_access?  && session[:status].blank?))|| current_user.allow_property_owner_access?
          session[:status] = "all"
        end

        unless session[:status] == "all"
          unless session[:status].blank?
            condition[0] += " AND properties.status = ?"
            condition <<  session[:status]
          end
        end

        unless session[:suburb] == "all" || session[:suburb].blank?
          condition[0] += " AND properties.suburb = ?"
          condition << session[:suburb]
        end

        unless session[:primary_contact_fullname] == "all" || session[:primary_contact_fullname].blank?
          name =  session[:primary_contact_fullname].split(' ')
          condition[0] += " AND users.first_name  = ?"
          condition << name[0]
          unless name[1].blank?
            condition[0] += " AND users.last_name  = ?"
            condition << name[1]
          end
        end

        unless session[:property_type] == "all" || session[:property_type].blank?
          condition[0] += " AND properties.property_type = ?"
          condition << session[:property_type]
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      condition[0] += " AND properties.agent_id = ?"
      condition << current_agent.id

      if current_user.limited_client_access?
        if !@office.control_level3
          condition[0] += " AND properties.primary_contact_id = ? "
          condition << current_user.id
        end
      elsif current_user.allow_property_owner_access?
        condition[0] += " AND properties.agent_user_id = ?"
        condition << current_user.id
      end
    end
    if params[:search_keyword].blank? && params[:listing_type].blank?
      condition[0] += " AND properties.deleted_at IS NULL"
    end

    #===================== row =======================#
    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = Property.build_sorting(params[:sort],params[:dir])
    if !params[:search_keyword].blank?
      @by_keyword = Property.basic_search(params[:search_keyword], params[:start], params[:limit], params[:page],@agent.id,@office.id,order,params[:sort],params[:dir])
      if @by_keyword.size > 1
        @keyword_types = @by_keyword.collect(&:type).join("|")
        @keyword_ids = @by_keyword.collect(&:id)
        @keyword_countries = @by_keyword.collect(&:country).join("|")
        @keyword_states = @by_keyword.collect(&:state).join("|")
        @keyword_town_villages = @by_keyword.collect(&:town_village).join("|")
        size_from = 50
        size_to = 50
        @properties = Property.sort_by_more_similiar(@keyword_ids,@keyword_types,size_from,size_to,@keyword_countries,@keyword_states,@keyword_town_villages,@agent,@office,params[:page],params[:sort],params[:dir],params[:limit])
      else
        if @by_keyword.size == 1
          @by_keyword.each do |keyword|
            @properties = Property.sort_by_one_similiar(params[:page],params[:sort],params[:dir],params[:limit],keyword,size_from,size_to,@agent,@office)
          end
        elsif @by_keyword.size == 0
          return false
        end
      end
    elsif !params[:listing_type].blank?
      @by_keyword = Property.advanced_search(params[:listing_type],params[:property_type],params[:price_from],params[:price_to],params[:deal_type],params[:availiability],params[:suites],params[:range_from],params[:range_to],session[:array_type_commercial],session[:array_type],session[:array_residential_building],params[:list_status_commercial],params[:commercial_type],params[:list_status_residential],params[:state],params[:regions_id],params[:suburbs_id],params[:agent_internal],params[:network_agent],params[:agent_external],params[:yield_from],params[:yield_to],params[:building_from],params[:building_to],params[:land_from],params[:land_to],params[:price_from_commercial],params[:price_to_commercial],params[:price_from_residential],params[:price_to_residential],params[:annual_rental_from],params[:annual_rental_to],params[:property_owner],params[:floor_from],params[:floor_to],params[:office_from],params[:office_to],params[:warehouse_from],params[:warehouse_to],params[:rent_from],params[:rent_to],params[:lease_from],params[:lease_to],params[:tenant],params[:search_keyword_advanced],params[:start],params[:limit],params[:page],@agent.id,@office.id,order,params[:sort],params[:dir])
      if @by_keyword.size > 1
        @keyword_types = @by_keyword.collect(&:type).join("|")
        @keyword_ids = @by_keyword.collect(&:id)
        @keyword_countries = @by_keyword.collect(&:country).join("|")
        @keyword_states = @by_keyword.collect(&:state).join("|")
        @keyword_town_villages = @by_keyword.collect(&:town_village).join("|")
        @properties = Property.sort_by_more_similiar(@keyword_ids,@keyword_types,size_from,size_to,@keyword_countries,@keyword_states,@keyword_town_villages,@agent,@office,params[:page],params[:sort],params[:dir],params[:limit])
      else
        @by_keyword.each do |keyword|
          @properties = Property.sort_by_one_similiar(params[:page],params[:sort],params[:dir],params[:limit],keyword,size_from,size_to,@agent,@office)
        end
      end
    else
      if params[:sort] == 'sqm' || params[:sort] == 'I/E'
        @properties = Property.sort_by_customize(params[:sort],params[:controller],params[:dir],params[:page],params[:limit],@agent.id,@office.id, @property.id)
      else
        @properties = Property.paginate(:all,:include =>[:primary_contact], :conditions => condition, :order => order, :page =>params[:page], :per_page => params[:limit].to_i)
      end
    end

    if @properties.present?
      @properties.each do |property|
        check_and_update_icon(property)
      end
    end

    #====================== result ==================#
    results = 0
    if !params[:search_keyword].blank?
      @by_keyword = Property.by_keyword(params[:search_keyword]).paginate(:all, :include =>[:primary_contact], :conditions => condition, :page =>params[:page], :per_page => params[:limit].to_i)
      if @by_keyword.size > 1
        @keyword_types = @by_keyword.collect(&:type).join("|")
        @keyword_ids = @by_keyword.collect(&:id)
        @keyword_countries = @by_keyword.collect(&:country).join("|")
        @keyword_states = @by_keyword.collect(&:state).join("|")
        @keyword_town_villages = @by_keyword.collect(&:town_village).join("|")
        @properties = Property.paginate(:all, :conditions => ["properties.id NOT IN (?) AND properties.type REGEXP (?) AND properties.size_from <= ? AND properties.size_to >= ? AND properties.country REGEXP (?) AND properties.state REGEXP (?) AND properties.town_village REGEXP (?) AND properties.agent_id = ? AND properties.office_id = ?", @keyword_ids, @keyword_types, 50, 50, @keyword_countries, @keyword_states, @keyword_town_villages, @agent.id, @office.id], :select =>"properties.headline,properties.id,properties.agent_id,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,users.first_name,users.last_name,properties.size_from,properties.size_to,properties.number_of_suites,properties.state,properties.town_village,properties.suburb,properties.agent_contact_id,properties.secondary_contact_id,agent_contacts.agent_company_id,commercial_details.rental_yield,commercial_details.area_range,commercial_details.area_range_to,commercial_details.land_area,commercial_details.sale_price_from,commercial_details.sale_price_to,commercial_details.rental_price,commercial_details.rental_price_to,commercial_details.floor_area,commercial_details.office_area,commercial_details.warehouse_area,commercial_details.current_rent,commercial_details.lease_end,commercial_details.current_leased", :include =>[:primary_contact], :page =>params[:page], :per_page => params[:limit].to_i)
        results = @properties.count
      else
        @by_keyword.each do |keyword|
          @properties = Property.paginate(:all, :conditions => ["properties.type = ? AND properties.id != ? AND properties.agent_id = ? AND properties.office_id = ?", "#{keyword.type}", "#{keyword.id}", @agent.id, @office.id], :select =>"properties.headline,properties.id,properties.agent_id,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,users.first_name,users.last_name,properties.size_from,properties.size_to,properties.number_of_suites,properties.state,properties.town_village,properties.suburb,properties.agent_contact_id,properties.secondary_contact_id", :include =>[:primary_contact], :page =>params[:page], :per_page => params[:limit].to_i)
          results = @properties.count
        end
      end
    elsif !params[:listing_type].blank?
      @by_keyword = Property.advanced_search(params[:listing_type],params[:property_type],params[:price_from],params[:price_to],params[:deal_type],params[:availiability],params[:suites],params[:range_from],params[:range_to],session[:array_type_commercial],session[:array_type],session[:array_residential_building],params[:list_status_commercial],params[:commercial_type],params[:list_status_residential],params[:state],params[:regions_id],params[:suburbs_id],params[:agent_internal],params[:network_agent],params[:agent_external],params[:yield_from],params[:yield_to],params[:building_from],params[:building_to],params[:land_from],params[:land_to],params[:price_from_commercial],params[:price_to_commercial],params[:price_from_residential],params[:price_to_residential],params[:annual_rental_from],params[:annual_rental_to],params[:property_owner],params[:floor_from],params[:floor_to],params[:office_from],params[:office_to],params[:warehouse_from],params[:warehouse_to],params[:rent_from],params[:rent_to],params[:lease_from],params[:lease_to],params[:tenant],params[:search_keyword_advanced],params[:start],params[:limit],params[:page],@agent.id,@office.id,order,params[:sort],params[:dir])
      if @by_keyword.size > 1
        @keyword_types = @by_keyword.collect(&:type).join("|")
        @keyword_ids = @by_keyword.collect(&:id)
        @keyword_countries = @by_keyword.collect(&:country).join("|")
        @keyword_states = @by_keyword.collect(&:state).join("|")
        @keyword_town_villages = @by_keyword.collect(&:town_village).join("|")
        @properties = Property.paginate(:all, :conditions => ["properties.id NOT IN (?) AND properties.type REGEXP (?) AND properties.size_from <= ? AND properties.size_to >= ? AND properties.country REGEXP (?) AND properties.state REGEXP (?) AND properties.town_village REGEXP (?) AND properties.agent_id = ? AND properties.office_id = ?", @keyword_ids, @keyword_types, 50, 50, @keyword_countries, @keyword_states, @keyword_town_villages, @agent.id, @office.id], :select =>"properties.headline,properties.id,properties.agent_id,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.deal_type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,users.first_name,users.last_name,properties.size_from,properties.size_to,properties.number_of_suites,properties.state,properties.town_village,properties.suburb,properties.agent_contact_id,properties.secondary_contact_id,agent_contacts.agent_company_id,commercial_details.rental_yield,commercial_details.area_range,commercial_details.area_range_to,commercial_details.land_area,commercial_details.sale_price_from,commercial_details.sale_price_to,commercial_details.rental_price,commercial_details.rental_price_to,commercial_details.floor_area,commercial_details.office_area,commercial_details.warehouse_area,commercial_details.current_rent,commercial_details.lease_end,commercial_details.current_leased", :include =>[:primary_contact], :page =>params[:page], :per_page => params[:limit].to_i)
        results = @properties.count
      else
        @by_keyword.each do |keyword|
          @properties = Property.paginate(:all, :conditions => ["properties.type = ? AND properties.id != ? AND properties.agent_id = ? AND properties.office_id = ?", "#{keyword.type}", "#{keyword.id}", @agent.id, @office.id], :select =>"properties.headline,properties.id,properties.agent_d,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,properties.deal_type,users.first_name,users.last_name,properties.size_from,properties.size_to,properties.number_of_suites,properties.state,properties.town_village,properties.suburb,properties.agent_contact_id,properties.secondary_contact_id,agent_contacts.agent_company_id,commercial_details.rental_yield,commercial_details.area_range,commercial_details.area_range_to,commercial_details.land_area,commercial_details.sale_price_from,commercial_details.sale_price_to,commercial_details.rental_price,commercial_details.rental_price_to,commercial_details.floor_area,commercial_details.office_area,commercial_details.warehouse_area,commercial_details.current_rent,commercial_details.lease_end,commercial_details.current_leased,properties.detail.floor_area" ,:include =>[:primary_contact], :page =>params[:page], :per_page => params[:limit].to_i)
          results = @properties.count
        end
      end
    else
      results = Property.count(:include =>[:primary_contact],:conditions => condition)
    end

    return(render(:json =>{:results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'listing'=>Property.abbrv_properties(x.type),
              'property_type'=>x.property_type.blank? ? "" : x.property_type,
              'address' =>((x.street_number.blank? ? "" : x.street_number)+" "+(x.street.blank? ? "" : x.street.to_s)),
              'suburb' => x.suburb,
              'type' => x.property_type,
              'suite'=>(x.unit_number.blank? ? "" : x.unit_number),
              'sqm'=>((x.detail.blank? ? "" : (x.detail.floor_area.blank? ? "" : x.detail.floor_area)) unless x.type == "NewDevelopment" || x.type == "LandRelease" || x.type == "ResidentialBuilding"),
              'I/E'=>"Internal",
              'price' => Property.check_price(x),
              'assigned_to'=>x.primary_contact.blank? ? '' : x.primary_contact.full_name.to_s.strip,
              'actions'=>Property.check_color(x),
              'status' => Property.status_name(x),
              'fb'=> [x.id,@office.url.blank? ? (x.detail.respond_to?(:deal_type) ? x.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}", tweet(x)]} unless x.blank? }}))
  rescue Exception => ex
    Log.create(:message => "Similiar Match Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def populate_combobox_contact
    suburb, category, last_name, first_name, assigned_to, accessible_by, condition = [],[],[],[],[],[], []
    condition = [" agent_contacts.office_id = ?", current_office.id]
    contacts = AgentContact.find(:all, :conditions => condition, :select => "DISTINCT(agent_contacts.category_contact_id), agent_contacts.suburb, agent_contacts.first_name, agent_contacts.last_name, agent_contacts.assigned_to, agent_contacts.accessible_by")
    contacts.map{|x|
      first_name << {
        :first_name => (x.first_name.to_s.slice(0,1).capitalize unless x.first_name.blank?) ,
        :first_nameText => (x.first_name.to_s.slice(0,1).capitalize unless x.first_name.blank?)
      } unless x.first_name.blank?

      last_name << {
        :last_name => (x.last_name.to_s.slice(0,1).capitalize unless x.last_name.blank?) ,
        :last_nameText => (x.last_name.to_s.slice(0,1).capitalize unless x.last_name.blank?)
      } unless x.last_name.blank?

      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText => x.suburb.to_s
      } unless x.suburb.blank?;
    }

    categories = ContactCategoryRelation.find(:all, :select => "DISTINCT(contact_category_relations.category_contact_id)")
    cid = categories.map{|c| c.category_contact_id}
    cat_cons = CategoryContact.find(:all, :conditions => "id IN (#{cid.join(',')})") if cid.present?

    cat_cons.map{|x|
      category << {
        :category => (x.id) ,
        :categoryText => (x.name.to_s)
      }
    } unless cat_cons.blank?

    assigned_to_db = ContactAssignedRelation.find(:all, :select => "DISTINCT(contact_assigned_relations.assigned_to)")
    a = assigned_to_db.map{|p| p.assigned_to.to_i unless p.assigned_to.blank?}
    usrs = User.find(:all, :conditions =>"id IN (#{a.join(',')}) and office_id = #{@office.id}") if a.present?

    usrs.map{|x|
      assigned_to << {
        :assigned_to => (x.id) ,
        :assigned_toText => (x.full_name)
      }
    } unless usrs.blank?

    accessible_by_db = ContactAccessibleRelation.find(:all, :select => "DISTINCT(contact_accessible_relations.accessible_by)")
    b = accessible_by_db.map{|p| p.accessible_by.to_i unless p.accessible_by.blank?}
    usrs2 = User.find(:all, :conditions =>"id IN (#{b.join(',')})and office_id = #{@office.id}") if b.present?
    usrs2.map{|x|
      accessible_by << {
        :accessible_by => (x.id) ,
        :accessible_byText => (x.full_name)
      }
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_assigned_to =[]
    uniq_accessible_by =[]
    uniq_first_name =[]
    uniq_last_name =[]
    uniq_category =[]

    uniq_assigned_to << {:assigned_to=>'all_team',:assigned_toText=>"All Team Members"}
    uniq_accessible_by << {:accessible_by=>'all_team',:accessible_byText=>"All Team Members"}
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    accessible_by.each{|x|uniq_accessible_by << x if !uniq_accessible_by.include?(x) and !x.blank?}
    category.each{|x|uniq_category << x if !uniq_category.include?(x) and !x.blank?}
    first_name.each{|x|uniq_first_name << x if !uniq_first_name.include?(x) and !x.blank?}
    last_name.each{|x|uniq_last_name << x if !uniq_last_name.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_accessible_by.to_a.sort!{|x,y| x[:accessible_byText].to_s <=> y[:accessible_byText].to_s}
    uniq_category.to_a.sort!{|x,y| x[:categoryText].to_s <=> y[:categoryText].to_s}
    uniq_first_name.to_a.sort!{|x,y| x[:first_nameText].to_s <=> y[:first_nameText].to_s}
    uniq_last_name.to_a.sort!{|x,y| x[:last_nameText].to_s <=> y[:last_nameText].to_s}

    return(render(:json =>{:results => contacts.length, :team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Assigned To"})].flatten,
          :access=>[uniq_accessible_by.unshift({:accessible_by=>'all',:accessible_byText=>"All Accessible By"})].flatten,
          :f_name=>[uniq_first_name.unshift({:first_name=>'all',:first_nameText=>"All First Name"})].flatten,
          :l_name=>[uniq_last_name.unshift({:last_name=>'all',:last_nameText=>"All Last Name"})].flatten,
          :suburb=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,
          :category=>[uniq_category.unshift({:category=>'all',:categoryText=>"All Categories"})].flatten
        }))
  end

  def view_contact_listing
    if params[:page_name] == 'listing_view'
      unless session[:ID].blank?
        condition = ["agent_contacts.id = #{session[:ID]} "]
      else
        get_session_contact_listing
        condition = [" agent_contacts.office_id = ? ", current_office.id]

        unless session[:first_name] == "all"
          if(!session[:first_name].blank?)
            condition[0] += " AND agent_contacts.first_name LIKE ?"
            condition << session[:first_name]+"%"
          end
        end

        unless session[:last_name] == "all"
          if(!session[:last_name].blank?)
            condition[0] += " AND agent_contacts.last_name LIKE ?"
            condition << session[:last_name]+"%"
          end
        end

        unless session[:email] == 'all'
          if(!session[:email].blank?)
            condition[0] += " AND agent_contacts.email LIKE ?"
            condition << session[:email]+"%"
          end
        end

        unless session[:company] == 'all'
          if(!session[:company].blank?)
            condition[0] += " AND (SELECT agent_companies.company FROM agent_companies INNER JOIN agent_contacts ON agent_companies.id = agent_contacts.agent_company_id )"
            condition << session[:company]+"%"
          end
        end

        unless session[:suburb] == 'all'
          if(!session[:suburb].blank?)
            condition[0] += " AND agent_contacts.suburb LIKE ?"
            condition << session[:suburb]+"%"
          end
        end

      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = AgentContact.build_sorting(params[:sort],params[:dir])
    if current_user.limited_client_access?
      contact_accessible_by2 = ContactAccessibleRelation.find(:all, :conditions => ["`accessible_by` = ?", current_user.id])
      condition[0] += " AND agent_contacts.id IN (?)"
      condition <<  contact_accessible_by2.collect(&:agent_contact_id)
      include = params[:sort] == 'assigned_to'? [:accessible, :assigned] : [:accessible, :category_contact]
    else
      include = params[:sort] == 'assigned_to'? [:assigned] : [:accessible, :category_contact]
    end


    @agent_contacts = AgentContact.paginate(:all, :include => include, :conditions => condition, :order => order, :page =>params[:page], :per_page => params[:limit].to_i)
    results = params[:limit].to_i == 20 ? AgentContact.find(:all,:select => 'count(agent_contacts.id)',:conditions => condition,:limit => params[:limit].to_i) : AgentContact.count(:conditions => condition)

    return(render(:json =>{:results => results,:rows=>@agent_contacts.map{|x|{
              'id' => AgentContact.check_id(x),
              'full_name' => (x.full_name.blank? ? "" : x.full_name.to_s),
              'phone' => (x.contact_number.blank? ? "" : x.contact_number),
              'suburb' => (x.suburb.blank? ? "" : x.suburb),
              'email' => (x.email.blank? ? "" : x.email.to_s),
              'company' => (x.agent_company.blank? ? "" : x.agent_company.company.to_s),
              'active' => (AgentContact.check_active(x)),
              'actions'=> ""}}}))
  rescue
    return(render(:json =>{:results => nil, :rows=> nil}))
  end

  def check_and_update_icon(property)
    img_count = property.images.count rescue 0
    floorp_count = property.floorplans.count rescue 0
    if !property.image_exist && img_count > 0
      property.image_exist = true
      property.save(false)
    end
    if property.image_exist && img_count == 0
      property.image_exist = false
      property.save(false)
    end
    if !property.floorplan_exist && floorp_count > 0
      property.floorplan_exist = true
      property.save(false)
    end
    if property.floorplan_exist && floorp_count == 0
      property.floorplan_exist = false
      property.save(false)
    end
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
    session[:search_keyword] = params[:search_keyword]
    session[:search_keyword_advanced] = params[:search_keyword_advanced]
  end

  def get_session_array
    session[:array_type] = params[:type]
    session[:array_residential_building] = params[:residential_building]
    session[:array_type_commercial] = params[:type_commercial]
  end

  def get_session_url
    session[:url] = request.url
  end

  def nil_session_listing
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
    session[:property_id] = nil
  end

  def get_session_contact_listing
    session[:contact_type] = params[:contact_type].nil? ? "all" : params[:contact_type]
    session[:company_type] = params[:company_type].nil? ? "all" : params[:company_type]
    session[:first_name] = params[:first_name].nil? ? "all" : params[:first_name]
    session[:last_name] = params[:last_name].nil? ? "all" : params[:last_name]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:assigned_to] = params[:assigned_to].nil? ? "all" : params[:assigned_to]
    session[:ID] = params[:ID].nil? ? nil : params[:ID]
    session[:sort_by] = []
    session[:sort_by] << {["agent_contacts.updated_at DESC"],["agent_companies.updated_at DESC"]}
  end

  def nil_session_contact_listing
    session[:status] = nil
    session[:company_type] = nil
    session[:contact_type] = nil
    session[:suburb] = nil
    session[:first_name] = nil
    session[:last_name] = nil
    session[:assigned_to] = nil
  end

  def history
    @searches = Search.paginate(:all, :conditions => ["searches.agent_id =? AND searches.office_id = ? AND searches.agent_user_id = ?", @agent.id, @office.id, current_user.id], :order => "created_at DESC", :page => params[:page], :per_page => 10)
  end

  def destroy
    @search = Search.find(params[:id])
    @search.destroy
    respond_to do |format|
      flash[:notice] = "Search keyword was successfully deleted."
      format.html { redirect_to history_agent_office_search_results_path(@agent.id,@office.id) }
    end
  end

  def create_match_properties
    if !params[:property_id].blank?
      if params[:commit] == "Email"
        session[:property_id] = params[:property_id]
        params[:property_id].each do |x|
          @match_property = MatchProperty.new({
              :property_id => x,
              :user_id => current_user.id,
              :agent_id => @agent.id,
              :office_id => @office.id}).save
        end
        redirect_to email_configuration_new_agent_office_search_result_path(@agent,@office)
      else
        session[:property_id] = params[:property_id]
        redirect_to print_pdf_agent_office_search_results_url(@agent, @office) + "?property_id=#{session[:property_id].join(",")}&search_keyword=#{params[:keyword]}"
      end
    else
      flash[:notice] = "Please select property first."
      redirect_to agent_office_search_results_path(@agent,@office)
    end
  end

  def generate_pdf
    @var = params[:property_id].split(",")
    @property_ids = []
    @property_ids += @var
    @properties = Property.find(:all, :conditions => ["properties.id IN (?)", @var])
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
    @editor_limit = Brochure.editor_limit(@office.pdf_configuration.theme) unless @office.pdf_configuration.blank?
    render :layout => false
  end

  def print_pdf
    @properties = Property.find(:all, :conditions => ["properties.id IN (?)", session[:property_id]])
    html = render_to_string(:layout => false)
    title = Time.zone.now.strftime("%d%m%y%H%M")
    filename = title + ".pdf"
    if session[:print_pdf].blank?
      pdf_file = Attachment.create(:size => 1000, :content_type => "application/pdf", :filename => filename, :attachable_id => @properties.last.id, :attachable_type => "Property", :title => title, :pdf_layout => html, :send_to_api => false, :position => 15)
      if pdf_file
        pdf_file.update_attribute(:type, "Brochure")
        pdf = Attachment.find(pdf_file.id)
        path = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
        path_folder = path.gsub("/"+filename, "")
        url_path = generate_pdf_agent_office_search_results_url(@agent, @office) + "?property_id=#{session[:property_id].join(",")}"
        system("mkdir -p #{path_folder}")
        temporary = "#{RAILS_ROOT}/tmp/#{pdf.filename}"
        temporary.gsub!(".pdf","-#{pdf.id}.pdf")
        width_height = ""
        spawn(:kill => true) do
          if RAILS_ENV == "development"
            system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{width_height} #{url_path} #{temporary}")
          else
            system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{width_height} #{url_path} #{temporary}")
          end
          begin
            if FileTest.exist?(temporary)
              uploaded = pdf.upload_to_s3(temporary)
            end
          rescue Exception => ex
            Log.create(:message => "ERROR search_results = #{ex.inspect}")
          end
        end
      end
      if pdf.moved_to_s3 == false
        session[:print_pdf] = pdf.id
        flash[:notice] = "the pdf will take a few seconds to create."
        redirect_to notification_agent_office_search_results_path(@agent, @office)
      else
        session[:print_pdf] = nil
        redirect_to pdf.public_filename
      end
    else
      file_pdf = Attachment.find(session[:print_pdf])
      file_temporary = "#{RAILS_ROOT}/tmp/#{file_pdf.filename}"
      file_temporary.gsub!(".pdf","-#{file_pdf.id}.pdf")
      unless file_pdf.blank?
        begin
          if file_pdf.moved_to_s3 == false
            if FileTest.exist?(file_temporary)
              uploaded = file_pdf.upload_to_s3(file_temporary)
            end
            if uploaded
              session[:print_pdf] = nil
              send_file(file_temporary, :type => "application/pdf", :disposition => "attachment", :filename => "#{file_pdf.filename.gsub!(".pdf", "")}")
            else
              session[:print_pdf] = file_pdf.id
              flash[:notice] = "the pdf will take a few seconds to create."
              redirect_to notification_agent_office_search_results_path(@agent, @office)
            end
          else
            session[:print_pdf] = nil
            send_file(file_temporary, :type => "application/pdf", :disposition => "attachment", :filename => "#{file_pdf.filename.gsub!(".pdf", "")}")
          end
        rescue Exception => ex
          Log.create(:message => "ERROR while generate PDF = #{ex.inspect}")
        end
      end
    end
  end

  def notification
    if session[:property_id]
      @display_url = print_pdf_agent_office_search_results_path(@agent, @office) + "?property_id=#{session[:property_id].join(",")}"
    else
      flash[:notice] = "The PDF file not found, please select the property first"
      redirect_to agent_office_search_results_path(@agent, @office)
    end
    @url_search = session[:url_search]
  end

  def download
    pdf  = Attachment.find(:last, :conditions => ["attachments.attachable_id = ?", session[:property_id].last])
    if !pdf.blank?
      file = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
      if pdf.moved_to_s3 == false
        if FileTest.exist?(file)
          pdf.upload_to_s3(file)
          redirect_to pdf.public_filename
        else
          if pdf.created_at.strftime("%Y-%m-%d") < Time.zone.now.strftime("%Y-%m-%d")
            flash[:notice] = "Search Results not found."
          else
            redirect_to pdf.public_filename
            #            flash[:notice] = "the pdf is still creating please try again in a few minutes."
          end
          redirect_to agent_office_search_results_path(@agent,@office)
        end
      else
        system("rm -rf #{file.gsub(pdf.filename, "")}") if FileTest.exist?(file)
        redirect_to pdf.public_filename
      end
    else
      flash[:notice] = "Search Results not found."
      redirect_to agent_office_search_results_path(@agent,@office)
    end
  end

  def change_property_types
    unless params[:ptype].blank?
      @categories = PropertyType.find_all_by_category(params[:ptype]).map{|x|[x.name, x.name]}.unshift(['Please select', ''])
    end
    render :layout => false
  end

  def populate_regions
    unless params[:state_name].blank?
      @regions = TownCountry.find_all_by_state_name(params[:state_name]).map{|x|[x.name, x.name]}.unshift(['Please select', ''])
    end
    render :layout => false
  end

  def populate_suburbs
    #    @regions = TownCountry.find_all_by_state_name(params[:state_name]).map{|x|[x.name,x.name]}.unshift(['Please select', ''])
    unless params[:region_name].blank? && params[:state_name].blank?
      @suburbs = Suburb.find(:all, :include => [:town_country, :state], :conditions => ["town_countries.name = ? AND states.name = ?", "#{params[:region_name]}", "#{params[:state_name]}"]).map{|x|[x.name, x.name]}.unshift(['Please select', ''])
    end
    @all_suburbs = @all_suburbs_list = ''
    unless params[:arr_suburb].blank?
      @suburbs_data = params[:arr_suburb].split(',')
      @suburbs_data.map{|cat|
        unless cat == ''
          random = (0..100).to_a.rand.to_s
          @all_suburbs = params[:arr_suburb]
          @all_suburbs_list = @all_suburbs_list + '<div id="'+random+'"style="padding-left:176px;;width:200px"class="property_type_selected">'+cat+'<a href="#" onclick="remove_suburb('+random+',\''+cat+'\')">x</a></div>'
        end
      }
    end
    render :layout => false
  end

  def view_history_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["searches.office_id = #{current_office.id}"]
      else
        # get_session_listing
        condition = ["searches.agent_id = ?"]
        condition << current_agent.id
        condition[0] += " AND searches.office_id = ?"
        condition << current_office.id
      end
    end

    if params[:page_name] == 'office_view'
      condition = ["searches.office_id = ?"]
      condition << current_office.id
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = Search.build_sorting(params[:sort],params[:dir])
    @searches = Search.paginate(:all, :conditions => condition, :page => params[:page], :per_page => 10)
    results = 0
    if params[:limit].to_i == 50
      #      results = 20 #Property.find(:all,:select => 'count(properties.id), properties.primary_contact_id',:include =>[:primary_contact],:conditions => condition,:limit => params[:limit].to_i)
      results = Search.count(:conditions => condition)
    else
      results = Search.count(:conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @searches.map{|x|{
              'id' =>x.id,
              'keyword'=>"<a href='/agents/#{@agent.id}/offices/#{@office.id}/search_results?search_keyword=#{x.keyword_name}&commit=Search'>#{x.keyword_name}</a>",
              'created_at'=>x.created_at.strftime('%d/%m/%Y'),
              'actions'=> Search.delete_search(x.id) }}}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  private

  def prepare_search
    @listing_options = ['Commercial', 'ResidentialSale', 'ProjectSale']
    @property_type = PropertyType.find(:all, :conditions => ["property_types.category != 'Commercial'"])
    @commercial_property_types = [["Please select", nil],["Building", "CommercialBuilding"],["Commercial Listing", "Commercial"]]
    @price_from = ['0..10000']
    @suites = (1..30).map{|x|[x,x]}.unshift(["Please select", ''])
    @ranges = (1..30).map{|x|[x,x]}.unshift(["Please select", ''])
    @type_options = PropertyType.find(:all, :conditions => ["property_types.category = ?", "Commercial"])
    @residential_type = PropertyType.find(:all, :conditions => ["property_types.category = ?", "Residential"])
    @available_options = ['available','sold','leased','withdrawn','underoffer','draft']
    @states = State.find_all_by_country_id(12).map{|x|[x.name,x.name]}.unshift(['Please select', ''])
    @internal_agents = AgentUser.find(:all, :conditions => ["developer_id = ? AND office_id = ?", @agent.developer_id, @office.id])
    @external_agents = AgentContact.find(:all, :include => [:contact_category_relations], :conditions => ["contact_category_relations.category_contact_id = ? AND agent_contacts.office_id = ?", 4, @office.id])
    @property_owners = AgentContact.find(:all, :include => [:contact_category_relations], :conditions => ["contact_category_relations.category_contact_id = ? AND agent_contacts.office_id = ?", 2, @office.id])
    @ptypes = PropertyType.business.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    offices_dev = ""
    offices_name = {}
    @network_agents = []
    offices = Office.find_by_sql("SELECT offices.id, offices.name FROM offices INNER JOIN agents ON offices.agent_id = agents.id WHERE agents.developer_id = #{@agent.developer_id} AND offices.deleted_at IS NULL")
    offices.each_with_index{|of, index|
      offices_dev += ((index == 0) ? "#{of.id}" :",#{of.id}")
      offices_name.merge!(of.id => "#{of.name}")
    }
    contacts = AgentUser.find(:all, :conditions => "roles.id = 3 OR roles.id = 4 OR roles.id = 5 And `users`.office_id IN (#{offices_dev}) And `users`.office_id != #{@office.id}", :include => "roles", :order => "`users`.first_name ASC")
    contacts.each{|u| @network_agents << [u.full_name + " (#{offices_name[u.office_id]})" , u.id] unless offices_name[u.office_id].blank? }

    @residential_buildings = Property.find(:all, :conditions => ["type = ? AND office_id = ?", "NewDevelopment", @office]).collect(&:suburb).uniq

    @commercial_types = PropertyType.commercial.all
    @residential_types = PropertyType.residential.all
  end

  def get_pdf_configuration
    @pdf_configuration = @office.pdf_configuration rescue nil
  end

  def require_accepted
    unless show_history?
      flash[:notice] = "You are not have permission to access search history page."
      redirect_to agent_office_path(current_agent, current_office)
    end
  end

  def display_history
    @display_search = Search.find_by_agent_user_id(current_user.id)
  end

  def show_history?
    !!display_history
  end

  def email_data
    @email = EmailConfigurationSearch.new({
      :sender_email => params[:email_configuration_search][:sender_email],
      :sender_name => params[:email_configuration_search][:sender_name],
      :send_bcc => params[:email_configuration_search][:send_bcc],
      :contact_name => params[:email_configuration_search][:contact_name],
      :title => params[:email_configuration_search][:title],
      :comments => params[:email_configuration_search][:comments],
      :user_id => current_user.id,
      :office_id => @office.id,
      :agent_id => @agent.id
    })

    @agent_contacts = AgentContact.find(
      :all,
      :conditions => ["agent_contacts.id IN (?) OR agent_contacts.contact_id IN (?)", params[:contact_name], params[:contact_id]]
    )

    @send_to = @agent_contacts.collect(&:email).join(",")
    @properties = Property.find(:all, :conditions => ["properties.id IN (?)", session[:property_id]])
    @header_image = Attachment.find(
      :last,
      :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Header"]
    )

    @footer_image = Attachment.find(
      :last,
      :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Footer"]
    )

    @side_image = find_image("Side Image")

  end
end
