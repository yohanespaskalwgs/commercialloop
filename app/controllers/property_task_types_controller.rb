class PropertyTaskTypesController < TaskTypesController

  protected

  def new_task_type
    @task_type = PropertyTaskType.new(params[:task_type])
  end

  def get_task_type
    @task_type = PropertyTaskType.find(params[:id])
  end

  def prepare_creation
    @task_types = PropertyTaskType.paginate(:all, :conditions => ["`office_id` is NULL OR `office_id` = ?", @office.id], :order => "`created_at` DESC", :page =>params[:page], :per_page => 10)
  end

end
