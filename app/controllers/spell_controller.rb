class SpellController < ApplicationController
  def spellcheck
    results = []
    unless params[:text].blank?
      params[:text].split("+").each do |word|
        err_word = check_spelling_new(word, 'checkWords', "en_US")
        unless err_word.blank?
          results << err_word if !results.include?(err_word)
        end
      end
    end
    results = check_spelling_new(params[:suggest], 'getSuggestions', "en_US") unless params[:suggest].blank?

    respond_to do |format|
      format.js {render :json => results.to_json}
    end
  end

  private
  def check_spelling_new(spell_check_text, command, lang)
    json_response_values = Array.new
    spell_check_response = `echo "#{spell_check_text}" | aspell -a -l #{lang}`
    if (spell_check_response != '')
      spelling_errors = spell_check_response.split(' ').slice(1..-1)
      if (command == 'checkWords')
        i = 0
        while i < spelling_errors.length
          spelling_errors[i].strip!
          if (spelling_errors[i].to_s.index('&') == 0)
            match_data = spelling_errors[i + 1]
            json_response_values << match_data
          end
          i += 1
        end
      elsif (command == 'getSuggestions')
        arr = spell_check_response.split(':')
        suggestion_string = arr[1]
        suggestions = suggestion_string.split(',')
        for suggestion in suggestions
          suggestion.strip!
          json_response_values << suggestion
        end
      end
    end
    return json_response_values
  end
end