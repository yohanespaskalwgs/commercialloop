class SubscriptionController < ApplicationController
  prepend_before_filter :login_required
  require_role "AdminUser", :only => :index

  before_filter :setup_subscription_manager

  def index
    @active_subscriptions = Subscription.find(:all, :conditions => ['account_id = ? and status = ?', current_user.id, 'ok'])
    @inactive_subscriptions = Subscription.find(:all, :conditions => ['account_id = ? and status not in (?)', current_user.id, ['ok','pending']])
  end

  def show
    @subscription = Subscription.find(params[:id])
    @tariff_plan = @sm.all_tariff_plans[@subscription.tariff_plan_id]
  end

  def subscribe
    if !params[:submit].nil? && !params[:tariff_plan_id].nil?
      subscription_options = {
          :account_id => @current_account[:id],
          :account_country => @current_account[:country],
          :account_state => @current_account[:state],
          :tariff_plan => params[:tariff_plan_id],
          :quantity => params[:quantity].to_i,
          :start_date => Date.parse(params[:start_date]),
          :end_date => Date.parse(params[:end_date])
        }
      credit_card = ActiveMerchant::Billing::CreditCard.new({
          :type => params[:card_type],
          :number => params[:card_number].to_i,
          :month => params[:card_expiration_month].to_i,
          :year => params[:card_expiration_year].to_i,
          :first_name => params[:card_first_name],
          :last_name => params[:card_last_name],
          :verification_value => params[:card_verification_value]
      })
      # subscribe
      subscription_id = @sm.subscribe(subscription_options)

      # this block should be different in multi-step subscription
      begin
        @sm.pay_for_subscription(subscription_id, credit_card, {})
      rescue
        Subscription.delete(subscription_id)
        raise
      end

      redirect_to :action => nil
      return
    end
  end

  def unsubscribe
    @subscription = Subscription.find(params[:id])
    if !params[:submit].nil?
      @sm.unsubscribe(@subscription.id)
      redirect_to :action => nil
      return
    end
  end

  def update_card
    @subscription = Subscription.find(params[:id])
    if !params[:submit].nil?
      credit_card = ActiveMerchant::Billing::CreditCard.new({
          :type => params[:card_type],
          :number => params[:card_number].to_i,
          :month => params[:card_expiration_month].to_i,
          :year => params[:card_expiration_year].to_i,
          :first_name => params[:card_first_name],
          :last_name => params[:card_last_name],
          :verification_value => params[:card_verification_value]
      })
      @sm.update_subscription(@subscription.id, {:card => credit_card})
      redirect_to :action => 'show', :id => @subscription.id
      return
    end
  end

  protected

  def setup_subscription_manager
    @sm = SubscriptionManager
  end

end
