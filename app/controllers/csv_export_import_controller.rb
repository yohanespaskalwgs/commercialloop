class CsvExportImportController < ApplicationController
  layout 'agent'
  before_filter :login_required , :except =>[:export_contact_csv, :export_property_csv]
  before_filter :get_agent_and_office, :prepare_data

  CUSTOM = ["opentime_date1", "opentime_start_time1", "opentime_end_time1", "opentime_date2", "opentime_start_time2", "opentime_end_time2", "opentime_date3", "opentime_start_time3", "opentime_end_time3", "image1", "image2", "image3", "image4", "image5", "image6", "image7", "image8", "floorplan1", "floorplan2", "floorplan3", "floorplan4", "floorplan5", "floorplan6", "floorplan7", "floorplan8"]

  include PropertiesHelper
  require 'date'
  require 'rubygems'
  require 'faster_csv'

  def index
    @category_contacts = CategoryContact.find(:all)
    @group_contacts = GroupContact.find(:all, :conditions => ["`office_id` = ?", @office.id])
    @contacts = @office.agent_users.find(:all, :conditions => "roles.id = 3 OR roles.id = 4 OR roles.id = 5", :include => "roles", :order => "`users`.first_name ASC")
  end

  def import_residential_sale_price_list_csv
    unless params[:parent_listing].blank?
      unless params[:residential_sale_price_list_csv][:file].blank?
        count = count_update = 0
        begin
          ActiveRecord::Base.transaction do
            FasterCSV.parse(params[:residential_sale_price_list_csv][:file], :headers => true, :col_sep => "\t") do |row|
              begin
                parent = Property.find(params[:parent_listing])
                unit_number = row[0].split("unit ")[1] unless row[0].blank?
                property = ResidentialSale.find(:first, :conditions => ["type = ? AND unit_number = ? AND parent_category = ? AND parent_listing_id = ?", "ResidentialSale", "#{unit_number}", "NewDevelopment", "#{parent.id}"]) unless row[0].blank?
                primary_contact = AgentUser.find(:first, :conditions => "first_name LIKE '#{current_user.first_name}' AND last_name LIKE '#{current_user.last_name}'")

                price = price = row[9].slice(1..-1).to_f*1000 unless row[9].blank?

                #Property
                property_attributes = {
                  :skip_validation => true,
                  :price => price,
                  :unit_number => unit_number,
                  :primary_contact_id => primary_contact.id,
                  :deleted_at => nil,
                  :created_at => Time.now,
                  :state => parent.state,
                  :updated_at => Time.now,
                  :agent_id => @agent.id,
                  :office_id => @office.id,
                  :status => (row[14] == " Sold " ? 2 : 1),
                  :country => parent.country,
                  :suburb => parent.suburb,
                  :town_village => parent.town_village,
                  :street_number => parent.street_number,
                  :street => parent.street,
                  :latitude => parent.latitude,
                  :longitude => parent.longitude,
                  :zipcode => parent.zipcode,
                  :agent_user_id => current_user.id,
                  :save_status => parent.save_status,
                  :delta => 1,
                  :display_price => 1,
                  :property_type => "Apartment",
                  :display_address => 1,
                  :parent_category => "NewDevelopment",
                  :parent_listing_id => parent.id,
                  :is_children_listing => 1
                }
                if property
                  property.update_attributes(property_attributes)
                  count_update += 1
                else
                  property = ResidentialSale.new(property_attributes)
                  property.save!
                  children = Property.find_all_by_parent_category_and_parent_listing_id("NewDevelopment", "#{parent.id}")
                  parent.update_attributes({:is_have_children_listing => 1}) if children.count > 0
                  count += 1
                end


                #Property Detail
                if property
                  internal_area = (row[1].blank? ? "" : row[1])
                  detail = {
                    :residential_sale_id => property.id,
                    :skip_validation => true,
                    :internal_area => internal_area,
                    :floor_area => internal_area,
                    :floor_area_metric => "Square Metres",
                    :balcony_area => row[2],
                    :bedrooms => row[4],
                    :bathrooms => row[5],
                    :carport_spaces => (row[7].blank? ? "" : 1)}
                  unless property.detail.blank?
                    property.detail.update_attributes(detail)
                  else
                    ResidentialSaleDetail.create(detail)
                  end
                end

                if property
                  extra_attributes = {
                    :property_id => property.id,
                    :field_name1 => "Floor Coverings",
                    :field_name2 => "Aspect",
                    :field_name3 => "Stamp Duty Payable",
                    :field_name4 => "Estimated Stamp Duty Saving",
                    :field_name5 => "Owner Corporation Cost",
                    :field_name6 => "Notes",
                    :field_name7 => "Deposit Paid",
                    :field_name8 => "Finance",
                    :field_name9 => "FIRB",
                    :field_name10 => "Flooring Option",
                    :field_name11 => "Settle Date",
                    :field_name12 => "Trans Rec.",
                    :field_name13 => "Dis Let",
                    :field_name14 => "Sett",
                    :field_name15 => "Multi Purpose Room",
                    :field_name16 => "Clause",
                    :field_name17 => "Colour Scheme",
                    :field_name18 => "NRAS Rec",
                    :field_name19 => "NRAS Sign",
                    :field15 => (row[6] == "Yes" ? "Yes" : "No"),
                    :field2 => row[8],
                    :field3 => row[10],
                    :field4 => row[11],
                    :field5 => row[13],
                    :field6 => (row[14].blank? ? "" : row[14].to_s)}
                  unless property.extra.blank?
                    property.extra.update_attributes(extra_attributes)
                  else
                    Extra.create(extra_attributes)
                  end
                end

              end
            end
          end
          flash[:notice] = "CSV successfully imported. #{count} properties added and #{count_update} properties updated to database."
        end
      else
        flash[:notice] = "Please select residential building first"
      end
      redirect_to :action => "index"
    end
  end

  def import_residential_sale_summary_csv
    unless params[:parent].blank?
      unless params[:residential_sale_summary_csv][:file].blank?
        count = count_update = count_contact = count_contact_update = 0
        begin
          ActiveRecord::Base.transaction do
            FasterCSV.parse(params[:residential_sale_summary_csv][:file], :headers => true, :col_sep => "\t") do |row|
              begin
                parent = Property.find(params[:parent])
                property = ResidentialSale.find(:first, :conditions => ["type = ? AND unit_number = ? AND parent_category = ? AND parent_listing_id = ?", "ResidentialSale", "#{row[0]}", "NewDevelopment", "#{parent.id}"]) unless row[0].blank?
                contact_data = row[2].strip unless row[2].blank?
                solicitor_data = row[3].strip unless row[3].blank?
                agent_contact = AgentContact.find(:first, :conditions => "contact_data_alternative LIKE '#{contact_data}' AND solicitor_data_alternative LIKE '#{solicitor_data}'")
                unit_number = row[0].strip unless row[0].blank?
                sold_on = Date.strptime(row[9], "%d.%m.%y") unless row[9].blank?
                assigned_to = AgentUser.find(:first, :conditions => "first_name LIKE '#{current_user.first_name}' AND last_name LIKE '#{current_user.last_name}'")
                purchaser = Purchaser.find(:first, :joins => [:agent_contact], :conditions => "contact_data_alternative LIKE '#{contact_data}' AND solicitor_data_alternative LIKE '#{solicitor_data}'")
                accessible_by = assigned_to.id
                price = row[4].slice(1..-1).to_f*1000 unless row[4].blank?
                primary_contact_id = accessible_by
                parent = Property.find(params[:parent])
                contact_attributes = {
                  :skip_validation => true,
                  :contact_type => "Individual",
                  :agent_id => @agent.id,
                  :office_id => @office.id,
                  :assigned_to => (assigned_to.id unless assigned_to.blank?),
                  :accessible_by => (accessible_by unless accessible_by.blank?),
                  :contact_data_alternative => contact_data,
                  :solicitor_data_alternative => solicitor_data,
                  :category_contact_id => 1
                }

                # Contact
                unless row[2].blank?
                  if agent_contact
                    agent_contact.update_attributes(contact_attributes)
                    count_contact_update += 1
                  else
                    agent_contact = AgentContact.new(contact_attributes)
                    if agent_contact.save
                      ContactCategoryRelation.create({:agent_contact_id => agent_contact.id, :category_contact_id => 1})
                      count_contact += 1
                    end
                  end
                end

                #Property
                property_attributes = {
                  :deleted_at => nil,
                  :updated_at => Time.now,
                  :skip_validation => true,
                  :agent_id => @agent.id,
                  :office_id => @office.id,
                  :is_children_listing => true,
                  :parent_category => "NewDevelopment",
                  :parent_listing_id => parent.id,
                  :sold_price => (row[3].blank? ? "" : price),
                  :show_price => (row[3].blank? ? 0 : 1),
                  :sold_on => sold_on,
                  :agent_user_id => current_user.id,
                  :primary_contact_id => primary_contact_id,
                  :unit_number => unit_number,
                  :country => parent.country,
                  :suburb => parent.suburb,
                  :state => parent.state,
                  :latitude => parent.latitude,
                  :longitude => parent.longitude,
                  :town_village => parent.town_village,
                  :street_number => parent.street_number,
                  :street => parent.street,
                  :status => (row[2].blank? ? 1 : 2)
                }
                if property
                  property.update_attributes(property_attributes)
                  count_update += 1
                else
                  property = ResidentialSale.new(property_attributes)
                  property.save!
                  count += 1
                end

                #Property Details
                if property
                  detail_attributes = {
                      :skip_validation => true,
                      :carport_spaces => (row[1] == "0" ? 0 : 1),
                      :residential_sale_id => property.id}
                  unless property.detail.blank?
                    property.detail.update_attributes(detail_attributes)
                  else
                    ResidentialSaleDetail.create(detail_attributes)
                  end
                end

                #Purchaser
                unless row[3].blank?
                  purchaser_attributes = {
                    :skip_validation => true,
                    :agent_contact_id => agent_contact.id,
                    :date => sold_on,
                    :amount => price,
                    :display_price => 1,
                    :property_id => property.id}
                  if property
                    unless property.purchaser.blank?
                      property.purchaser.update_attributes(purchaser_attributes)
                    else
                      purchaser = Purchaser.create(purchaser_attributes)
                    end
                  end
                end

                #Extras
                unless row[3].blank?
                  extras_attributes = {
                    :property_id => property.id,
                    :field_name1 => "Floor Coverings",
                    :field_name2 => "Aspect",
                    :field_name3 => "Stamp Duty Payable",
                    :field_name4 => "Estimated Stamp Duty Saving",
                    :field_name5 => "Owner Corporation Cost",
                    :field_name6 => "Notes",
                    :field_name7 => "Deposit Paid",
                    :field_name8 => "Finance",
                    :field_name9 => "FIRB",
                    :field_name10 => "Flooring Option",
                    :field_name11 => "Settle Date",
                    :field_name12 => "Trans Rec.",
                    :field_name13 => "Dis Let",
                    :field_name14 => "Sett",
                    :field_name15 => "Multi Purpose Room",
                    :field_name16 => "Clause",
                    :field_name17 => "Colour Scheme",
                    :field_name18 => "NRAS Rec",
                    :field_name19 => "NRAS Sign",
                    :field7 => row[5],
                    :field8 => row[6],
                    :field9 => row[7],
                    :field17 => row[8],
                    :field11 => row[10],
                    :field12 => row[11],
                    :field13 => row[12],
                    :field14 => row[14]
                  }
                  if property
                    unless property.extra.blank?
                      property.extra.update_attributes(extras_attributes)
                    else
                      extra = extra.create(extras_attributes)
                    end
                  end
                end

              end
            end
          end
        end
        flash[:notice] = "CSV successfully imported. #{count} properties added and #{count_update} properties updated to database. Also #{count_contact} contacts added and #{count_contact_update} contacts updated."
      end
    else
      flash[:notice] = "Please select residential building first"
    end
    redirect_to :action => "index"
  end

  def import_contact_csv
    unless params[:contact_csv][:file].blank?
      count = count_update = 0
      begin
        FasterCSV.parse(params[:contact_csv][:file], :headers => true, :col_sep => ",") do |row|
          row = row[0].split("\t")
          begin
            contact_id = row[0].strip unless row[0].blank?
            agent_contact = AgentContact.find_by_id(contact_id) unless contact_id.blank?
            #          agent_contact = AgentContact.find(:first, :conditions => "contact_id = #{contact_id} And office_id = #{@office.id}") unless contact_id.blank?
            unless row[20].blank?
              groups = row[20].split(";")
              group_ids = []
              groups.each do |group|
                group = group.strip
                group_contact = GroupContact.find(:first, :conditions => "name LIKE '#{group}' And office_id = #{@office.id}")
                group_contact = GroupContact.create({:name => group, :office_id => @office.id}) if group_contact.blank?
                group_ids << group_contact.id
              end
            end
            category_contact = CategoryContact.find(:first, :conditions => "name LIKE '#{row[17]}'") unless row[17].blank?
            assigned_to = AgentUser.find(:first, :conditions => "CONCAT(`first_name`, ' ' ,`last_name`) LIKE '#{row[18]}'") unless row[18].blank?
            accessible_by = AgentUser.find(:first, :conditions => "CONCAT(`first_name`, ' ' ,`last_name`) LIKE '#{row[19]}'") unless row[19].blank?
            contact = {:contact_type => "Individual", :skip_validation => true, :agent_id => @agent.id, :office_id => @office.id, :title => row[1], :first_name => row[2], :last_name => row[3], :email => row[4],
              :home_number => row[5], :work_number => row[6], :mobile_number => row[7], :contact_number => row[8],
              :fax_number => row[9], :unit_number => row[10], :street_number => row[11], :street_name => row[12], :province => row[13], :suburb => row[14], :post_code => row[15],
              :inactive => row[16], :category_contact_id => (category_contact.blank? ? 12 : category_contact.id),
              :assigned_to => (assigned_to.id unless assigned_to.blank?), :accessible_by => (accessible_by.id unless accessible_by.blank?), :purchaser_name => row[21], :purchaser_email => row[22], :purchaser_phone => row[23], :address_alternative => row[24] }
            if agent_contact
              if agent_contact.update_attributes(contact)
                count_update += 1
              end
            else
              agent_contact = AgentContact.new(contact)
              if agent_contact.save
                ContactCategoryRelation.create({:agent_contact_id => agent_contact.id, :category_contact_id => 12})
                count += 1
              end
            end

            if agent_contact
              agent_contact.add_access([accessible_by.id]) unless accessible_by.blank?
              agent_contact.add_assigns([assigned_to.id]) unless assigned_to.blank?
              agent_contact.add_categories([category_contact.id]) unless category_contact.blank?
              agent_contact.add_groups(group_ids)
              agent_contact.use_spawn_for_boom
              agent_contact.use_spawn_for_md
              agent_contact.use_spawn_for_irealty
            end
            #        rescue Exception => ex
          end
        end
        #      rescue Exception => ex
      end
      flash[:notice] = "CSV successfully imported. #{count} records added and #{count_update} records updated to database."
    end
    redirect_to :action => "index"
  end

  def export_contact_csv
    @contacts = AgentContact.find(:all, :conditions => "office_id = #{@office.id}")
    csv_string = FasterCSV.generate do |csv|
      # header row
      csv << ["user_id", "title", "first_name_compulsory", "last_name_compulsory", "email_compulsory", "home_number", "work_number", "mobile_number", "contact_number", "fax_number", "unit_number", "street_number", "street_name", "province", "suburb", "post_code", "inactive", "category", "assigned_to", "accessible_by", "mailing_group" ]
      # data rows
      @contacts.each do |contact|
        group_ids = contact.group_contact_relations.map{|a|a.group_contact_id } rescue ''
        if group_ids.present?
          gc = GroupContact.find(:all,:conditions=>"id IN (#{group_ids.join(',')})")
          group = gc.map{|a|a.name}.join(';') if gc.present?
        end
        #        group = contact.group_contact_relations.first.group_contact.name unless contact.group_contact_relations.first.blank?
        csv << [contact.contact_id, contact.title, contact.first_name, contact.last_name, contact.email, contact.home_number, contact.work_number, contact.mobile_number, contact.contact_number, contact.fax_number, contact.unit_number, contact.street_number, contact.street_name, contact.province, contact.suburb, contact.post_code, contact.inactive, (contact.category_contact.name unless contact.category_contact.blank?), (contact.assigned.full_name unless contact.assigned.blank?), (contact.accessible.full_name unless contact.accessible.blank?), group ]
      end
    end
    # send it to the browsah
    send_data csv_string, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=commercialloopcrm_contacts_office_#{@office.id}.csv"
  end

  #---------------------------------------------------------------------

  def import_property_csv
    unless params[:property_csv][:file].blank?
      type = params[:property_csv][:listing_type]
      valid_csv = 1
      counter = count_create = count_update = 0
      invalid_row = check = ""
      csv_header = property_array_params(type, true)
      csv_format = property_array_params(type)
      CUSTOM.each do |t|
        csv_header << t.to_s
        csv_format << t.to_s
      end

      FasterCSV.parse(params[:property_csv][:file], :headers => false, :col_sep => ",") do |row|
        if counter == 0
          csv_header.each_with_index{ |arr, i| valid_csv = 0 if arr != row[i] }
          break if valid_csv == 0
        else
          parameter = convert_csv_to_param(csv_format, row, type)
          if parameter[:xml_id].blank?
            check = create_property(parameter)
            if check == true
              count_create += 1
            else
              invalid_row += "Line #{counter} Error, #{check} <br/>"
            end
          else
            property_xmls = Property.find(:all, :conditions => "xml_id = '#{parameter[:xml_id]}' and office_id='#{@office.id}' and agent_id='#{@agent.id}'")
            unless property_xmls.blank?
              property_xmls.each do |property|
                check = update_property(parameter, property.id)
                if check == true
                  count_update += 1
                else
                  invalid_row += "Line #{counter} Error, #{check} <br/>"
                end
              end
            else
              check = create_property(parameter)
              if check == true
                count_create += 1
              else
                invalid_row += "Line #{counter} Error, #{check} <br/>"
              end
            end
          end
        end
        counter += 1
      end
      if valid_csv == 0
        flash[:notice] = "Invalid csv format!"
      else
        flash[:notice] = "CSV successfully imported. #{count_create} #{((count_create > 1)? 'records':'record')} added, #{count_update}  #{((count_update > 1)? 'records':'record')} updated to database, #{((invalid_row.blank?)? 'No error found': 'Some error found, Please fix and reupload those line!')}"
      end
      @errors_import = invalid_row
    end
    render :action => "index"
  end

  def export_property_csv
    unless params[:export_listing_type].blank?
      type = params[:export_listing_type]
      @properties = Property.find(:all, :conditions => "type = '#{type.camelize}' And office_id = #{@office.id}")
      csv_header = property_array_params(type, true)
      CUSTOM.each{|t| csv_header << t.to_s}

      listing_format = property_array_params(type)
      csv_string = FasterCSV.generate do |csv|
        csv << csv_header
        @properties.each do |property|
          tmp = []
          detail = property.detail
          if !property.blank? and !detail.blank?
            listing_format.each{|val| tmp << check_param(property, detail, val.to_s)}

            # define custom data
            tmp << (property.opentimes.first.blank? ? "" : property.opentimes.first.date.strftime("%Y-%m-%d").to_s)
            tmp << (property.opentimes.first.blank? ? "" : property.opentimes.first.start_time.strftime("%H:%M:%S").to_s)
            tmp << (property.opentimes.first.blank? ? "" : property.opentimes.first.end_time.strftime("%H:%M:%S").to_s)
            tmp << (property.opentimes.second.blank? ? "" : property.opentimes.second.date.strftime("%Y-%m-%d").to_s)
            tmp << (property.opentimes.second.blank? ? "" : property.opentimes.second.start_time.strftime("%H:%M:%S").to_s)
            tmp << (property.opentimes.second.blank? ? "" : property.opentimes.second.end_time.strftime("%H:%M:%S").to_s)
            tmp << (property.opentimes.third.blank? ? "" : property.opentimes.third.date.strftime("%Y-%m-%d").to_s)
            tmp << (property.opentimes.third.blank? ? "" : property.opentimes.third.start_time.strftime("%H:%M:%S").to_s)
            tmp << (property.opentimes.third.blank? ? "" :property.opentimes.third.end_time.strftime("%H:%M:%S").to_s)

            images = property.images
            floors = property.floorplans
            (0..7).each do |num|
              if !images[num].blank?
                tmp << images[num].public_filename.to_s
              else
                tmp << ""
              end
            end

            (0..7).each do |num|
              if !floors[num].blank?
                tmp << floors[num].public_filename.to_s
              else
                tmp << ""
              end
            end
            csv << tmp
          end
        end
      end
      send_data csv_string, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=commercialloopcrm_#{type}_office#{@office.id}_#{Time.zone.now.strftime("%d-%m-%Y_%H%M%S")}.csv"
    end
  end

  def prepare_data
    @listings = ['residential_sale', 'residential_lease', 'holiday_lease', 'commercial', 'project_sale', 'business_sale', 'new_development', 'land_release'].map{|m| [m,m]}
    @residential_buildings = Property.find(:all, :conditions => ["type = ? AND agent_id = ? AND office_id = ?", "NewDevelopment", @agent.id, @office.id]).map{|x| [x.address, x.id]}.unshift(["Please select", nil])
  end

  def create_property(param)
    if %w(residential_lease residential_sale commercial holiday_lease project_sale business_sale new_development land_release).include?(param[:listing_type])
      pclass = Kernel.const_get get_property_type(param[:listing_type])
      @param_property= property_params(param)
      @param_detail= detail_params(param)

      @property = pclass.new @param_property
      @detail = @property.build_detail @param_detail

      @property.description= decoded_and_converted_to(@property.description)
      @property.detail.unavailable_date = param[:unavailable_date] if @property.detail.class.name.underscore == "holiday_lease_detail"
      @detail.deal_type = @property.deal_type if @detail.respond_to?(:deal_type)
      @detail.property_type = @property.property_type if @detail.respond_to?(:property_type)
      @detail.country = @property.country if @detail.respond_to?(:country)
      save_rental_seasons(param) if param[:rental_season]

      @feature_ids = @property.feature_ids
      @property.agent_user_id = param[:primary_contact_id]
      @property.office_id = param[:office_id]
      @property.agent_id = param[:agent_id]

      if @property.save
        @property.update_attribute(:agent_contact_id, Property.create_import_contact(param))
        @property.update_attribute(:deactivation, nil) if @property.status == 1
        @property.agent_contact.create_note(@property.id, "Property Listed", 19, nil) unless @property.agent_contact_id.nil?
        country = Country.find_by_name(@office.country)
        init_exports(country.portal_countries)
        @property.opentimes.create({:date => param[:opentime_date1], :start_time => param[:opentime_start_time1], :end_time => param[:opentime_end_time1]}) if !param[:opentime_date1].blank? && !param[:opentime_start_time1].blank? && !param[:opentime_end_time1].blank?
        @property.opentimes.create({:date => param[:opentime_date2], :start_time => param[:opentime_start_time2], :end_time => param[:opentime_end_time2]}) if !param[:opentime_date2].blank? && !param[:opentime_start_time2].blank? && !param[:opentime_end_time2].blank?
        @property.opentimes.create({:date => param[:opentime_date3], :start_time => param[:opentime_start_time3], :end_time => param[:opentime_end_time3]}) if !param[:opentime_date3].blank? && !param[:opentime_start_time3].blank? && !param[:opentime_end_time3].blank?
        (1..8).each{|no| Delayed::Job.enqueue(ExportImportImageCsv.new(@property.id, no, param["image#{no}".to_sym], "Image"), 3) unless param["image#{no}".to_sym].blank? }
        (1..8).each{|no| Delayed::Job.enqueue(ExportImportImageCsv.new(@property.id, no, param["floorplan#{no}".to_sym], "Floorplan"), 3) unless param["floorplan#{no}".to_sym].blank? }
        return true
      else
        return "Property: #{@property.errors.full_messages.inspect}, Detail: #{@property.detail.errors.full_messages.inspect}"
      end
    end
  end

  def update_property(param, property_id)
    param[:feature_ids] ||= []
    if !property_id.blank? && !param[:listing_type].blank?
      @property = Property.find property_id
      param[:listing_type] = @property.class.name.underscore
      last_status = @property.status
      previous_agent_user_id = @property.agent_user_id
      previous_agent_contact_id = @property.agent_contact_id
      @property.description= decoded_and_converted_to(@property.description)
      @property.detail.unavailable_date = param[:unavailable_date] if @property.detail.class.name.underscore == "holiday_lease_detail"
      @property.detail.property_type = @property.property_type if @property.detail.respond_to?(:property_type)
      @property.detail.country = @property.country if @property.detail.respond_to?(:country)
      @property.detail.deal_type = @property.deal_type if @property.detail.respond_to?(:deal_type)

      if @property.is_a?(HolidayLease) && param[:rental_season]
        @property.rental_seasons.destroy_all
        save_rental_seasons
      end

      @param_property= property_params(param)
      @param_detail= detail_params(param)
      @property.attributes = @param_property
      @property.agent_user_id = param[:primary_contact_id].blank? ? previous_agent_user_id : param[:primary_contact_id]
      @property.office_id = param[:office_id]
      @property.agent_id = param[:agent_id]
      @property.detail.deal_type = @property.deal_type if @property.detail.is_a?(CommercialDetail)
      @property.activation = ((last_status != 1 && @property.status == 1) ? true : nil)
      @property.update_attribute(:deactivation,nil) if (last_status == 6 && @property.status != 6)
      errors = @property.detail.errors unless @property.detail.update_attributes(@param_detail)
      errors = @property.errors unless @property.save
      if errors.blank?
        @property.opentimes.create({:date => param[:opentime_date1], :start_time => param[:opentime_start_time1], :end_time => param[:opentime_end_time1]}) if !param[:opentime_date1].blank? && !param[:opentime_start_time1].blank? && !param[:opentime_end_time1].blank?
        @property.opentimes.create({:date => param[:opentime_date2], :start_time => param[:opentime_start_time2], :end_time => param[:opentime_end_time2]}) if !param[:opentime_date2].blank? && !param[:opentime_start_time2].blank? && !param[:opentime_end_time2].blank?
        @property.opentimes.create({:date => param[:opentime_date3], :start_time => param[:opentime_start_time3], :end_time => param[:opentime_end_time3]}) if !param[:opentime_date3].blank? && !param[:opentime_start_time3].blank? && !param[:opentime_end_time3].blank?
        if !@property.vendor_first_name.blank? and !@property.vendor_last_name.blank? and @property.agent_contact.blank?
          @property.update_attribute(:agent_contact_id, Property.create_import_contact(param))
          @property.agent_contact.create_note(@property.id, "Property Listed", 19, nil) unless @property.agent_contact.blank?
        else
          (@property.update_contact_detail if previous_agent_contact_id == @property.agent_contact.id) unless @property.agent_contact.blank?
        end
        (1..3).each{|no| Delayed::Job.enqueue(ExportImportImageCsv.new(@property.id, no, param["image#{no}".to_sym], "Image"), 3) unless param["image#{no}".to_sym].blank? }
        (1..3).each{|no| Delayed::Job.enqueue(ExportImportImageCsv.new(@property.id, no, param["floorplan#{no}".to_sym], "Floorplan"), 3) unless param["floorplan#{no}".to_sym].blank? }
        return true
      else
        return "Property: #{@property.errors.full_messages.inspect}, Detail: #{@property.detail.errors.full_messages.inspect}"
      end
    end
  end

  private

  def init_exports(portal_countries)
    unless portal_countries.blank?
      allowed = [1,5,6].include?(@property.status)
      if allowed
        portal_countries.each do |p_c|
          portal_autosubcribe = PortalAutosubcribe.find_by_portal_id_and_office_id(p_c.portal.id,@office.id)
          unless portal_autosubcribe.blank?
            cek = PropertyPortalExport.find_by_property_id_and_portal_id(@property.id,p_c.portal.id)
            PropertyPortalExport.create(:property_id => @property.id, :portal_id => p_c.portal.id) if cek.blank?
          end
        end
      end
    end
  end

  def save_rental_seasons(param)
    param[:rental_season].each_with_index do |rental_season,index|
      save_rental_sessions = RentalSeason.new(rental_season)
      @property.rental_seasons << save_rental_sessions
    end
  end

  def convert_csv_to_param(csv_format, param, type)
    fparams = {}
    csv_format.each_with_index{ |arr, i| fparams = fparams.merge!({arr.to_sym => param[i]}) }
    user = @office.agent_users.find(:first, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
    fparams.merge!({:office_id => @office.id, :agent_id => @agent.id, :primary_contact_id => user.id, :listing_type => type})
    return fparams
  end

  def residential_sale_array_params(type, header = false)
    unless type.blank?
      array = []
      p = Property.find(:last, :conditions => "type = '#{type.camelize}'")
      property = p.attributes.delete_if { |x,y| ["deleted_at","created_at","updated_at","office_id","agent_id","agent_user_id", "processed", "api_is_sent", "count_api_sent", "cron_checked", "delta", "primary_contact_id", "primary_contact_id", "secondary_contact_id", "third_contact_id", "fourth_contact_id", "activation", "agent_contact_id", "deactivation", "secondary_contact_type", "third_contact_type", "fourth_contact_type", "image_exist", "upfront_fee", "save_status", "show_price", "sold_on", "sold_price", "xml_updated_at", "cre_id", "rca_id", "counter","id"].include?(x)  }
      detail = p.detail.attributes.delete_if { |x,y| ["created_at","updated_at","residential_sale_id","residential_lease_id","business_sale_id","commercial_id", "commercial_building_id","project_sale_id","holiday_lease_id", "new_development_id", "land_release_id","additional_notes","id"].include?(x)  }
      property.each{|arr,val| array << arr.to_s}
      detail.each{|arr,val| array << arr.to_s}
      if header
        cpl = compulsory_listing(type)
        list = array.sort
        list.each_with_index{|arr, index| list[index] = arr+"_compulsory" if cpl.include?(arr)}
        return list
      else
        return array.sort
      end
    end
  end

  def property_array_params(type, header = false)
    unless type.blank?
      array = []
      p = Property.find(:last, :conditions => "type = '#{type.camelize}'")
      property = p.attributes.delete_if { |x,y| ["deleted_at","created_at","updated_at","office_id","agent_id","agent_user_id", "processed", "api_is_sent", "count_api_sent", "cron_checked", "delta", "primary_contact_id", "primary_contact_id", "secondary_contact_id", "third_contact_id", "fourth_contact_id", "activation", "agent_contact_id", "deactivation", "secondary_contact_type", "third_contact_type", "fourth_contact_type", "image_exist", "upfront_fee", "save_status", "show_price", "sold_on", "sold_price", "xml_updated_at", "cre_id", "rca_id", "counter","id"].include?(x)  }
      detail = p.detail.attributes.delete_if { |x,y| ["created_at","updated_at","residential_sale_id","residential_lease_id","business_sale_id","commercial_id", "commercial_building_id","project_sale_id","holiday_lease_id", "new_development_id", "land_release_id","additional_notes","id"].include?(x)  }
      property.each{|arr,val| array << arr.to_s}
      detail.each{|arr,val| array << arr.to_s}
      if header
        cpl = compulsory_listing(type)
        list = array.sort
        list.each_with_index{|arr, index| list[index] = arr+"_compulsory" if cpl.include?(arr)}
        return list
      else
        return array.sort
      end
    end
  end

  def check_param(property, detail, param)
    data = nil
    data = property[param.to_sym].to_s.gsub(/\s+/, " ").gsub(",", ".") if property.respond_to?(param.to_sym)
    data = detail[param.to_sym].to_s.gsub(/\s+/, " ").gsub(",", ".") if detail.respond_to?(param.to_sym) and data.blank?
    return data.to_s
  end

  def compulsory_listing(type)
    compulsory_property = ["country", "state", "suburb", "headline", "zipcode", "price", "status"]
    compulsory_detail = []
    case type
    when "residential_sale"
      compulsory_detail = ["property_type","street_number", "street", "bedrooms", "bathrooms"]
    when "residential_lease"
      compulsory_detail = ["property_type","street_number", "street", "date_available", "bedrooms", "bathrooms"]
    when "holiday_lease"
      compulsory_detail = ["property_type","street_number", "street", "bedrooms", "bathrooms"]
    when "commercial"
      compulsory_detail = ["property_type","street_number", "street", "method_of_sale"]
      #    when "commercial_building"
      #      compulsory_detail = ["property_type","street_number", "street", "method_of_sale"]
    when "project_sale"
      compulsory_detail = ["property_type","street_number", "street", "bedrooms", "bathrooms", "category" ]
    when "business_sale"
      compulsory_detail = ["property_type"]
    when "new_development"
      compulsory_detail = ["street_number", "street", "development_name"]
    when "land_release"
      compulsory_detail = ["street_number", "street", "release_name"]
    end
    compulsory_detail.each{|t| compulsory_property << t.to_s} unless compulsory_detail.blank?

    return compulsory_property
  end
end
