class ContactTaskTypesController < TaskTypesController

  protected

  def new_task_type
    @task_type = ContactTaskType.new(params[:task_type])
  end

  def get_task_type
    @task_type = ContactTaskType.find(params[:id])
  end

  def prepare_creation
    @task_types = ContactTaskType.paginate(:all, :conditions => ["`office_id` is NULL OR `office_id` = ?", @office.id], :order => "`created_at` DESC", :page =>params[:page], :per_page => 10)
  end

end
