class UsersController < ApplicationController
  layout "signup"

  protect_from_forgery :except => [:check_domain, :check_login, :check_email]

  # render new.rhtml
  def new
    @title = "Signup"
    if request.post?
      create
    else
      @developer = Developer.new
      @user = User.new

      respond_to do |format|
        format.html # new.html.erb
      end
    end
  end

  def create
    logout_keeping_session!
    Developer.transaction do
      @developer = Developer.new(params[:developer])
      @user = DeveloperUser.new(params[:users])
      @user.active_help_status = true
      @developer.developer_users << @user
      @developer.save!
      @active_helps = ActiveHelpPage.find_by_sql("SELECT * FROM active_help_pages WHERE user_type = 'DeveloperUser'")
      @user.active_help_pages = @active_helps
      ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id = #{@user.id}")
      begin
       UserMailer.deliver_signup_notification(@user)
      rescue
      end
    end
    @subdomain = params[:developer]['entry']
    respond_to do |format|
      @user.toggle_admin # <= obsolete?
      @user.access_level = 1

      flash[:notice] = 'Signed up successfully'
      format.html { redirect_to "http://#{@subdomain}.commercialloopcrm.com.au/login" }
    end

  end

  def activate
    @title = "Activate Account"
    logout_keeping_session!
    user = User.find_by_activation_code(params[:activation_code]) unless params[:activation_code].blank?
    case
    when (!params[:activation_code].blank?) && user && !user.active?
      user.activate!
      flash[:notice] = "Signup complete! Please sign in to continue."
      redirect_to '/login'
    when params[:activation_code].blank?
      flash[:error] = "The activation code was missing.  Please follow the URL from your email."
      redirect_back_or_default('/')
    else
      flash[:error]  = "We couldn't find a user with that activation code -- check your email? Or maybe you've already activated -- try signing in."
      redirect_back_or_default('/')
    end
  end

  def check_domain
    if params[:entry].blank? || Authentication.entry_regex.match(params[:entry]).nil? || params[:entry].length < 3
      render :text => "<em style=\'color:red;font-weight:bold;font-size:12px;\'>Please enter valid unique URL</em>", :layout => false
    else
      @developer = Developer.find(:first, :conditions => ["LOWER(entry)=?", params[:entry].strip.downcase])
      if @developer.nil?
        render :text => "<em style=\'color:green;font-weight:bold;font-size:12px;\'>Congratulations! the URL is available</em>", :layout => false
      else
        render :text => "<em style=\'color:red;font-weight:bold;font-size:12px;\'>URL is not available, please choose another</em>", :layout => false
      end
    end
  end

  def check_login
    if params[:login].blank?
      render :text => "<em style=\'color:red;font-weight:bold;font-size:12px;\'>Please enter your username</em>", :layout => false
    else
      @developer = User.find(:first, :conditions => ["LOWER(login)=?", params[:login].strip.downcase])
      if @developer.nil?
        render :text => "<em style=\'color:green;font-weight:bold;font-size:12px;\'>Username is available</em>", :layout => false
      else
        render :text => "<em style=\'color:red;font-weight:bold;font-size:12px;\'>Username is not available, please choose another</em>", :layout => false
      end
    end
  end

  def check_email
    if params[:email].blank? || Authentication.email_regex.match(params[:email]).nil?
      render :text => "<em style=\'color:red;font-weight:bold;font-size:12px;\'>Please enter valid email</em>", :layout => false
    else
      @developer = User.find(:first, :conditions => ["LOWER(email)=?", params[:email].strip.downcase])
      if @developer.nil?
        render :text => "<em style=\'color:green;font-weight:bold;font-size:12px;\'>OK</em>", :layout => false
      else
        render :text => "<em style=\'color:red;font-weight:bold;font-size:12px;\'>This email already registered, <a href=\'/forgot_password\'>forgot your password?</a></em>", :layout => false
      end
    end
  end

  def forgot_password
    @title = "Forgot Password"
    if request.post?
      user = User.find_by_email(params[:user][:email])
      if user
        user.create_reset_code
        UserMailer.deliver_reset_notification(user)
        flash.now[:notice] = "Reset code sent to #{user.email}"
      else
        flash.now[:notice] = "#{params[:user][:email]} does not exist in our database"
      end
    end
  end

  def reset
    @title = "Reset Password"
    @user = User.find_by_reset_code(params[:reset_code]) unless params[:reset_code].nil?
    if request.post?
      if @user
        if @user.update_attributes(:password => params[:user][:password], :password_confirmation => params[:user][:password_confirmation])
          self.current_user = @user
          @user.delete_reset_code
          #flash[:notice] = "Password reset successfully for #{@user.email}"
          flash.now[:notice] = "Your password has successfully been updated"
        else
          render :action => :reset
        end
      end
    end
  end
end
