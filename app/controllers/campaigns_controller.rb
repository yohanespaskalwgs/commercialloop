class CampaignsController < ApplicationController
  require_role 'AgentUser'
  skip_before_filter :verify_authenticity_token, :only => [:create, :destroy]
  prepend_before_filter :get_agent_and_office
  # GET /campaign
  # GET /campaign.xml
  def index
    @campaigns = Campaign.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @campaigns }
    end
  end

  # GET /campaign/1
  # GET /campaign/1.xml
  def show
    @campaign = Campaign.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @campaign }
    end
  end

  # GET /campaign/new
  # GET /campaign/new.xml
  def new
    @campaign = Campaign.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @campaign }
    end
  end

  # GET /campaign/1/edit
  def edit
    @campaign = Campaign.find(params[:id])
  end

  # POST /campaign
  # POST /campaign.xml
  def create
    @property = Property.find(params[:property_id])
    @campaign = @property.campaigns.new(:start_date => Time.now, :current => true)

    respond_to do |format|
      if @campaign.save
        flash[:notice] = 'Campaign was successfully created.'
        format.js { render :layout => false }
        format.html { redirect_to(:action => 'index') }
        format.xml  { render :xml => @campaign, :status => :created, :location => @campaign }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @campaign.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /campaign/1
  # PUT /campaign/1.xml
  def update
    @campaign = Campaign.find(params[:id])

    respond_to do |format|
      if @campaign.update_attributes(params[:campaign])
        flash[:notice] = 'Campaign was successfully updated.'
        format.html { redirect_to(:action => 'index') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @campaign.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /campaign/1
  # DELETE /campaign/1.xml
  def destroy
    @campaign = Campaign.find(params[:id])
    @campaign.destroy

    respond_to do |format|
      format.js { render :layout => false }
      format.html { redirect_to(campaigns_url) }
      format.xml  { head :ok }
    end
  end
end
