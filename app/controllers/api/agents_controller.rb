class Api::AgentsController < Api::ApiBaseController
  def index
    unless params[:office_id].blank?
      condition = [" users.office_id = ? "]
      condition << params[:office_id]
    end
    if @api_page == "normal"
      api_page
      WillPaginateHelpers.param_request_host = request.env["HTTP_HOST"]
      condition = " users.updated_at >= '#{32.minutes.ago}' " unless request.url().scan("updated").blank?
      @agents = current_agent.agent_users.paginate(:all,:conditions => condition,:select =>"users.activated_at, users.active_help_status, users.code, users.created_at, users.description, users.developer_id, users.email, users.facebook_username, users.fax, users.first_name, users.group, users.id, users.im_service, users.im_username, users.last_name, users.linkedin_username, users.login, users.mobile, users.office_id, users.phone, users.position, users.remember_token, users.remember_token_expires_at, users.role, users.suburb,users.video_url,users.video_code, users.twitter_username, users.updated_at", :order => "users.position ASC",:page =>params[:page].blank? ? 1 : params[:page], :per_page => 10)
    end

    if @api_page == "client"
      api_page
      @agents = current_agent.agent_users.paginate(:all,:conditions => condition,:select =>"users.activated_at, users.active_help_status, users.code, users.created_at, users.description, users.developer_id, users.email, users.facebook_username, users.fax, users.first_name, users.group, users.id, users.im_service, users.im_username, users.last_name, users.linkedin_username, users.login, users.mobile,users.video_url,users.video_code, users.office_id, users.phone, users.position, users.remember_token, users.remember_token_expires_at, users.role, users.twitter_username, users.updated_at", :order => "users.position ASC")
    end

    if @api_page == "developer"
      api_page
      condition = " users.updated_at >= '#{32.minutes.ago}' " unless request.url().scan("updated").blank?
      @agents = @developer.agent_users.paginate(:all,:conditions => condition,:select =>"users.activated_at, users.active_help_status, users.code, users.created_at, users.description, users.developer_id, users.email, users.facebook_username, users.fax, users.first_name, users.group, users.id, users.im_service, users.im_username, users.last_name, users.linkedin_username, users.login, users.mobile, users.office_id, users.phone,users.video_url,users.video_code, users.position, users.remember_token, users.remember_token_expires_at, users.role, users.twitter_username, users.updated_at", :order => "users.position ASC",:page =>params[:page].blank? ? 1 : params[:page], :per_page => 10)
    end

    respond_to do |format|
      format.xml { render :xml => @agents}
    end
  rescue
    return render :xml => @agents
  end
end