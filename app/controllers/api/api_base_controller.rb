class Api::ApiBaseController < ApplicationController

  before_filter :find_agent_or_developer_by_access_key
  before_filter :find_office

  protected

  def api_page
    require 'to_xml_extensions'
    WillPaginateHelpers.param_request_uri = request.url()
  end

  private

  def find_agent_or_developer_by_access_key
    authenticate_or_request_with_http_basic("Commercial Loop CRM API") do |username, password|
      if params[:developer_id].blank?
        @agent = Agent.find_by_access_key_and_private_key(username, password)
        @agent
      else
        @developer = Developer.find_by_access_key_and_private_key(username, password)
        @developer
      end
    end
  end

  def find_office
    unless @agent.blank?
      if params[:office_id].blank?
        @api_page = "client"
      else
        @api_page = "normal"
        @office = @agent.offices.find(params[:office_id])
      end
    else
      @api_page = "developer"
    end
  end



end