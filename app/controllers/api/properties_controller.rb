class Api::PropertiesController < Api::ApiBaseController
  # GET /properties
  def index
    begin
      condition = [' 1=1 ']
      condition[0] += " AND (offices.status = 'active' OR offices.status = 'pending') "
      order = "properties.updated_at desc"
      order = "properties.id asc" unless params[:sort_by_id_asc].blank?
      order = "properties.id desc" unless params[:sort_by_id_desc].blank?

      unless params[:developer_id].blank?
        api_page
        unless request.url().scan("updated").blank?
          condition[0] +=" AND (properties.status IN(1,2,3,4,5) AND properties.updated_at >= '#{32.minutes.ago}' AND properties.status <> 6  AND properties.agent_id IN(?)AND (offices.status = 'active' OR offices.status = 'pending')) "
          condition << @developer.agents.map{|m|m.id}.compact
          @properties = Property.paginate(:all,:with_deleted => true,:include =>[:office,:agent],:conditions => condition, :order => order,:page =>params[:page].blank? ? 1 : params[:page], :per_page => params[:per_page].blank? ? 10 : params[:per_page])
        else
          unless @developer.blank?
            condition[0] += " AND properties.agent_id IN(?) "
            condition << @developer.agents.map{|m|m.id}.compact
          end
          condition[0] +=" AND properties.status IN(1,5) OR (properties.status IN(2,3,4) AND properties.updated_at >= '#{7.days.ago}' AND properties.status <> 6  AND properties.agent_id IN(?) ) AND (offices.status = 'active' OR offices.status = 'pending') "
          condition << @developer.agents.map{|m|m.id}.compact
          @properties = Property.paginate(:all,:include =>[:office,:agent],:conditions => condition, :order => order,:page =>params[:page].blank? ? 1 : params[:page], :per_page => params[:per_page].blank? ? 10 : params[:per_page])
        end
      else
        api_page
        if @api_page == "normal"
          unless @office.blank?
            condition[0] += " AND properties.office_id = ? "
            condition << @office.id
          end
          unless @agent.blank?
            condition[0] += " AND properties.agent_id = ? "
            condition << @agent.id
          end

          unless params[:sold_leased].blank?
            condition[0] +=" AND properties.status NOT IN(1,4,5) OR (properties.status IN(2,3) AND properties.status <> 6 AND properties.office_id = #{@office.id}  AND properties.agent_id = #{@agent.id} AND (offices.status = 'active' OR offices.status = 'pending')) "
            @properties = Property.paginate(:all,:include =>[:office,:agent],:conditions => condition, :order => order,:page =>params[:page].blank? ? 1 : params[:page], :per_page => params[:per_page].blank? ? 50 : params[:per_page])
          else
            unless request.url().scan("updated").blank?
              condition[0] +=" AND ( properties.updated_at >= '#{32.minutes.ago}' AND properties.office_id = #{@office.id}  AND properties.agent_id = #{@agent.id} AND (offices.status = 'active' OR offices.status = 'pending')) "
              @properties = Property.paginate(:all,:with_deleted => true,:include =>[:office,:agent],:conditions => condition, :order => order,:page =>params[:page].blank? ? 1 : params[:page], :per_page => params[:per_page].blank? ? 50 : params[:per_page])
              unless @properties.blank?
                @properties.map{|x| x.status = 4 if x.status == 6}
              end
            else
              unless params[:withdrawn].blank?
                condition[0] +=" AND properties.status IN(4,6) OR (properties.status NOT IN(1,2,3,5) AND properties.office_id = #{@office.id}  AND properties.agent_id = #{@agent.id} AND (offices.status = 'active' OR offices.status = 'pending')) "
                @properties = Property.paginate(:all,:with_deleted => true,:include =>[:office,:agent],:conditions => condition, :order => order,:page =>params[:page].blank? ? 1 : params[:page], :per_page => params[:per_page].blank? ? 50 : params[:per_page])
                unless @properties.blank?
                  @properties.map{|x| x.status = 4 if x.status == 6}
                end
              else
                condition[0] +=" AND properties.status IN(1,5) OR (properties.status NOT IN(2,3,4) AND properties.status <> 6 AND properties.office_id = #{@office.id}  AND properties.agent_id = #{@agent.id} AND (offices.status = 'active' OR offices.status = 'pending')) "
                @properties = Property.paginate(:all,:include =>[:office,:agent],:conditions => condition, :order => order,:page =>params[:page].blank? ? 1 : params[:page], :per_page => params[:per_page].blank? ? 50 : params[:per_page])
              end
            end
          end
        else
          unless @office.blank?
            condition[0] += " AND properties.office_id IN(?) "
            condition << @agent.offices.map{|m|m.id}.compact
          end
          unless @agent.blank?
            condition[0] += " AND properties.agent_id = ? "
            condition << @agent.id
          end
          condition[0] +=" AND properties.status IN(1,5) OR (properties.status IN(2,3,4) AND properties.updated_at >= '#{7.days.ago.strftime("%Y-%m-%d")}'  AND properties.status <> 6 AND properties.agent_id = #{@agent.id} AND properties.office_id IN(?)  AND (offices.status = 'active' OR offices.status = 'pending') ) "
          condition << @agent.offices.map{|m|m.id}.compact
          @properties = Property.paginate(:all,:include =>[:office,:agent],:conditions => condition, :order => order,:page =>params[:page].blank? ? 1 : params[:page], :per_page => params[:per_page].blank? ? 10 : params[:per_page])
        end
      end
      return render :xml => @properties
   rescue Exception => ex
     return render :xml => @properties
   end
  end

  def photos
    @property = Property.find(params[:id])
    render :xml => @property.images
  end

  def floorplans
    @property = Property.find(params[:id])
    render :xml => @property.floorplans
  end

  def show
    find_property
    render :xml => @property.to_xml(:public)
  end

  private

  def find_property(*args)
    @property = Property.find_by_id params[:id], :include => [:detail, :images, :floorplans]
  end

  def clear_property
    unless @properties.blank?
      @properties.delete_if { |property|
        (property.status == 2 || property.status == 3 || property.status == 4)&&(property.updated_at <= 7.days.ago)
      }
    end
  end

end
