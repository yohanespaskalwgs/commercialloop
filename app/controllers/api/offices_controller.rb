class Api::OfficesController < Api::ApiBaseController

  def index
    if @api_page == "normal"
      @office = Office.find(params[:office_id],:include =>[:domains])
      return
    end
    if @api_page == "client"
      api_page
      @offices = @agent.offices.paginate(:all,:include =>[:domains], :order => "updated_at DESC",:page =>params[:page].blank? ? 1 : params[:page], :per_page => 10)
      render :xml => @offices
    end
    if @api_page == "developer"
      api_page
      unless request.url().scan("updated").blank?
        condition = " offices.updated_at >= '#{32.minutes.ago}' "
        @offices = @developer.offices.paginate(:all,:include =>[:domains],:conditions => condition, :order => 'offices.updated_at desc',:page =>params[:page].blank? ? 1 : params[:page], :per_page => 10)
      else
        @offices = @developer.offices.paginate(:all,:include =>[:domains], :order => "updated_at DESC",:page =>params[:page].blank? ? 1 : params[:page], :per_page => 10)
      end
      render :xml => @offices
      return
    end
  rescue
    return render :xml => @offices
  end
end
