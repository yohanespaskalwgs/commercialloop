class NotesController < ApplicationController
  layout 'agent'

  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation

  def index
     @contact_note = ContactNote.new
  end

  def show
  end

  def new
  end

  def edit
    @contact_note = ContactNote.find(params[:id])
    @contact_note.note_date = Time.now.strftime("%m/%d/%Y") if @contact_note.note_date.nil?
  end

  def create
    params[:contact_note][:note_date] = Time.now if params[:contact_note][:note_date].blank?
    @contact_note = ContactNote.new(params[:contact_note])

    respond_to do |format|
      if @contact_note.save
        @contact_note.create_offer
        flash[:notice] = 'Contact Note was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
   @contact_note = ContactNote.find(params[:id])

    respond_to do |format|
      if @contact_note.update_attributes(params[:contact_note])
        flash[:notice] = 'Contact Note was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @contact_note = ContactNote.find(params[:id])
    @contact_note.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def prepare_creation
    @agent_contact = AgentContact.find(params[:agent_contact_id])
    @notes_type = NoteType.find(:all, :conditions => ["`status` = 'contact_note'"]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @team_members = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    @notes = @agent_contact.contact_notes.paginate(:all, :order => "`created_at` DESC", :page =>params[:page], :per_page => 30)
  end
end
