require 'will_paginate/array'
class TransactionsController < ApplicationController
  layout 'agent'
  before_filter :get_agent_and_office, :get_property

  def index
    get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def edit
    @property = Property.find(params[:property_id])
    @transaction = SalesRecord.find(params[:id])
    @method_of_sales = [['Please select', nil]]
    @investments = [['Please select', nil],['Yes', true],['No', false]]
    @sales_person = AgentUser.find(:all, :conditions => "office_id = '#{@office.id}'").map{|x|[x.full_name.blank? ? x.first_name : x.full_name,x.id]}.unshift(['Please select', nil])
    @contact_name = @transaction.agent_contact.full_name.blank? ? @transaction.agent_contact.contact_data_alternative :  @transaction.agent_contact.full_name unless @transaction.agent_contact.nil?

    if @transaction.blank?
      @purchaser = SalesRecord.new
      @purchaser_contact = @purchaser.build_agent_contact
    else
      @purchaser = @transaction
      @purchaser_contact = @purchaser.agent_contact_id.nil? ? @purchaser.build_agent_contact : @purchaser.agent_contact

      #Populate Transaction
      @method_of_sale = @transaction.method_of_sale
      @investment = @transaction.investment
      @fee = @transaction.fee.blank? ? "" : @transaction.fee
      @date = @transaction.date.blank? ? "" : @transaction.date
      @amount = @transaction.amount.blank? ? "" : @transaction.amount
      @display_price = @transaction.display_price.blank? ? "" : @transaction.display_price
      @settlement_date = @transaction.settlement_date.blank? ? "" : @transaction.settlement_date
      @office_note = @transaction.office_note.blank? ? "" : @transaction.office_note
      @transaction_sales_person = @transaction.transaction_sales_person.blank? ? "" : @transaction.transaction_sales_person
      @external_agent_company = @transaction.external_agent_company.blank? ? "" : @transaction.external_agent_company
      @external_agent_sales_person = @transaction.external_agent_sales_person.blank? ? "" : @transaction.external_agent_sales_person
      @conjunction_fee = @transaction.conjunction_fee.blank? ? "" : @transaction.conjunction_fee
      @scale_of_fee = @transaction.scale_of_fee.blank? ? "" : @transaction.scale_of_fee
      @contract_link = @transaction.contract_link.blank? ? "" : @transaction.contract_link
      #End
    end
  end

  def update_status
    @property = Property.find(params[:property_id])
    @transaction = SalesRecord.find(38)
    new_purchaser = create_note_for_contact = new_transaction = false
    @transaction.nil? ? new_purchaser = true : @purchaser = @transaction
    (previous_agent_contact_id = @transaction.agent_contact_id unless @transaction.agent_contact_id.nil?) unless @transaction.nil?

    respond_to do |format|
      begin
        SalesRecord.transaction do
          category_id = (@property.type == 'ResidentialLease')? 5 : 1
          params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => @agent.id, :office_id => @office.id, :contact_type => "Individual")
          if new_purchaser
            unless params[:transaction].blank?
              @purchaser = SalesRecord.new({
                  :property_id => @property.id,
                  :agent_contact_id => params[:purchaser][:agent_contact_id],
                  :date => params[:transaction][:date],
                  :amount => params[:transaction][:amount],
                  :display_price => params[:transaction][:display_price],
                  :method_of_sale => params[:transaction][:method_of_sale],
                  :investment => params[:transaction][:investment],
                  :fee => params[:transaction][:fee],
                  :settlement_date => params[:transaction][:settlement_date],
                  :office_note => params[:transaction][:office_note],
                  :transaction_sales_person => params[:transaction][:transaction_sales_person],
                  :external_agent_company => params[:transaction][:external_agent_company],
                  :external_agent_sales_person => params[:transaction][:external_agent_sales_person],
                  :conjunction_fee => params[:transaction][:conjunction_fee],
                  :scale_of_fee => params[:transaction][:scale_of_fee],
                  :contract_link => params[:transaction][:contract_link]
                })
            end
            save_purchaser = @purchaser.save

            if params[:edit_selected_contact] == "true"
              @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
            end
            @purchaser_contact = @purchaser.build_agent_contact({
                :first_name => params[:nil_class][:first_name],
                :last_name => params[:nil_class][:last_name],
                :email => params[:nil_class][:email],
                :home_number => params[:nil_class][:home_number],
                :work_number => params[:nil_class][:work_number],
                :mobile_number => params[:nil_class][:mobile_number],
                :contact_number => params[:nil_class][:contact_number]
              }) if params[:purchaser][:agent_contact_id].blank?
          else
            save_purchaser = @purchaser.update_attributes({
                :agent_contact_id => params[:purchaser][:agent_contact_id],
                :date => params[:transaction][:date],
                :amount => params[:transaction][:amount],
                :display_price => params[:transaction][:display_price],
                :method_of_sale => params[:transaction][:method_of_sale],
                :investment => params[:transaction][:investment],
                :fee => params[:transaction][:fee],
                :settlement_date => params[:transaction][:settlement_date],
                :office_note => params[:transaction][:office_note],
                :transaction_sales_person => params[:transaction][:transaction_sales_person],
                :external_agent_company => params[:transaction][:external_agent_company],
                :external_agent_sales_person => params[:transaction][:external_agent_sales_person],
                :conjunction_fee => params[:transaction][:conjunction_fee],
                :scale_of_fee => params[:transaction][:scale_of_fee],
                :contract_link => params[:transaction][:contract_link]
              })
            if params[:edit_selected_contact] == "true"
              @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
            end
            @purchaser.agent_contact_id.nil? ? (@purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?) : (@purchaser_contact = @purchaser.agent_contact)
          end
          raise 'Failed to save Purchaser' unless save_purchaser

          if params[:add_new_contact] == "true" || !params[:purchaser][:agent_contact_id].blank?

            if new_purchaser || @purchaser.agent_contact_id.nil?
              if params[:purchaser][:agent_contact_id].blank?
                save_purchaser_contact = @purchaser_contact.save
                @purchaser.update_attribute(:agent_contact_id, @purchaser_contact.id) unless @purchaser_contact.blank?
                if params[:edit_selected_contact] == "true"
                  @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                end
              else
                save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                if params[:edit_selected_contact] == "true"
                  @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                end
                @purchaser_contact = @purchaser.agent_contact
              end
              create_note_for_contact = true
            else
              if previous_agent_contact_id.to_s == params[:purchaser][:agent_contact_id].to_s
                if params[:mark_for_deleted] == "true"
                  save_purchaser_contact = @purchaser.update_attributes!({:agent_contact_id => nil})
                else
                  save_purchaser_contact = @purchaser.agent_contact.update_attributes(params[:agent_contact])
                end
              else
                if params[:mark_for_deleted] == "true"
                  save_purchaser_contact = @purchaser.update_attributes!({:agent_contact_id => nil})
                else
                  save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                end
                create_note_for_contact = true
              end
              @purchaser_contact = @purchaser.agent_contact
            end

            raise 'Failed to save Purchaser contact' unless save_purchaser_contact

            if params[:add_new_contact] == "true"
              team = []
              team << @property.primary_contact_id unless @property.primary_contact_id.nil?
              team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
              @purchaser_contact.add_categories([category_id])
              @purchaser_contact.add_assigns(team)
              @purchaser_contact.add_access(team)
            end
          end
        end
        flash[:notice] = 'Transaction record was successfully updated.'
        format.html { redirect_to :action => 'edit' }
      rescue
        @contact_name = params[:agent_name]
        @add_new_contact = params[:add_new_contact]
        @method_of_sales = [['Please select', nil]]
        @investments = [['Please select', nil],['Yes', true],['No', false]]
        @sales_person = AgentUser.find(:all, :conditions => "office_id = '#{@office.id}'").map{|x|[x.full_name.blank? ? x.first_name : x.full_name,x.id]}.unshift(['Please select', nil])
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @transaction = SalesRecord.find(params[:id])
    respond_to do |format|
      @transaction.update_attributes({:is_deleted => true})
      @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil})
      flash[:notice] = "Transaction record was successfully deleted"
      format.html {redirect_to agent_office_property_transactions_path(@agent,@office,@property)}
    end
  end

  def populate_combobox
    floor,suburb,type,assigned_to,condition = [],[],[],[],[]
    condition = [" properties.office_id = ? "]
    condition << current_office.id
    if @property.type.to_s == "Commercial" && @property.is_have_transaction? == true
      properties = SalesRecord.find(:all,:include =>[:property], :conditions => "sales_records.property_id = '#{@property.id}'",:select => "properties.primary_contact_id, DISTINCT(properties.suburb),DISTINCT(properties.property_type),users.first_name,users.last_name")
    elsif @property.type.to_s == "CommercialBuilding"
      properties = SalesRecord.find(:all, :include => [:property], :conditions => "properties.parent_listing_id = '#{@property.id}'")
    end
    properties.map{|x|
      suburb << {
        :suburb => x.property.suburb.to_s,
        :suburbText =>x.property.suburb.to_s
      } unless x.property.suburb.blank?;
      type << {
        :type => x.property.property_type.to_s,
        :typeText =>x.property.property_type.to_s
      } unless x.property.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.property.primary_contact.full_name.to_s unless x.property.primary_contact.blank?) ,
        :assigned_toText => (x.property.primary_contact.full_name.to_s unless x.property.primary_contact.blank?)
      } unless x.property.primary_contact.blank?
      ((floor << {
            :floor => x.property.detail.number_of_floors.to_s,
            :floorText => x.property.detail.number_of_floors.to_s
          } unless x.property.detail.number_of_floors.blank?) unless x.property.type == "ProjectSale" || x.property.type == "BusinessSale")
    } unless properties.blank?
    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    uniq_floor =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    floor.each{|x|uniq_floor << x if !uniq_floor.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_floor.to_a.sort!{|x,y| x[:floorText].to_s <=> y[:floorText].to_s}
    return(render(:json =>{
      :results => properties.length,
      :suburbs=>[uniq_suburb.unshift({:suburb=>'all',
      :suburbText=>"All Suburbs"})].flatten,
      :floors=>[uniq_floor.unshift({:floor=>'all',:floorText=>"All Floors"})].flatten,
      :types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,
      :team=>[uniq_assigned_to.unshift({:assigned_to=>'all',
      :assigned_toText=>"All Team Member"})].flatten}))
    rescue Exception => ex
    Log.create(:message => "Transaction Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        if @property.type.to_s == "Commercial"
          condition[0] += " AND sales_records.property_id = ?"
          condition << "#{@property.id}"
        elsif @property.type.to_s == "CommercialBuilding"
          condition[0] += " AND properties.parent_listing_id = ?"
          condition << "#{@property.id}"
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      if @property.type.to_s == "Commercial"
        condition[0] += " AND sales_records.property_id = ?"
        condition << "#{@property.id}"
      elsif @property.type.to_s == "CommercialBuilding"
        condition[0] += " AND properties.parent_listing_id = ?"
        condition << "#{@property.id}"
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = SalesRecord.build_sorting(params[:sort],params[:dir])
    if params[:sort] == 'sqm'
      @properties = Property.sort_by_customize(params[:sort],params[:controller],params[:dir],params[:page],params[:limit],@agent.id,@office.id, @property.id)
    else
      @properties = SalesRecord.paginate(:all, :include => [:property,:agent_contact], :conditions => condition, :order => order, :page => params[:page], :per_page => params[:limit].to_i)
    end

    if @properties.present?
      @properties.each do |x|
        check_and_update_icon(x.property)
      end
    end
    results = 0
    if params[:limit].to_i == 20
      results = SalesRecord.find(:all, :select => 'count(sales_records.property_id)',:include =>[:property, :agent_contact], :conditions => condition,:limit => params[:limit].to_i)
    else
      results = SalesRecord.count(:include =>[:property, :agent_contact], :conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'property_id' => x.property.id,
              'listing'=>Property.abbrv_properties(x.property.type),
              'address' =>((x.property.street_number.blank? ? "" : x.property.street_number)+" "+(x.property.street.blank? ? "" : x.property.street.to_s)),
              'suite'=>(x.property.unit_number.blank? ? "" : x.property.unit_number),
              'sqm'=>x.property.detail.floor_area,
              'level'=>((x.property.detail.number_of_floors.blank? ? "" : x.property.detail.number_of_floors) unless x.property.type == "ProjectSale" || x.property.type == "BusinessSale"),
              'I/E'=> x.property.ownership.blank? ? "Internal" : x.property.ownership,
              'purchaser'=>(x.agent_contact.blank? ? "" : (x.agent_contact.full_name.blank? ? x.agent_contact.first_name : x.agent_contact.full_name)),
              'sold_date'=>(x.date.blank? ? "" : x.date),
              'amount'=>(x.amount.blank? ? "" : x.amount),
              'suburb' => x.property.suburb,
              'type' => x.property.property_type,
              'price' => Property.check_price(x.property),
              'assigned_to'=>x.property.primary_contact.blank? ? '' : x.property.primary_contact.full_name.to_s.strip,
              'edit'=>[x.property.id, x.id],
              'status' => Property.status_name(x.property),
              'fb'=> [x.property.id,@office.url.blank? ? (x.property.detail.respond_to?(:deal_type) ? x.property.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.property.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.property.id}", tweet(x.property)]} unless x.property.blank? }}))
  # rescue Exception => ex
  #   Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
  #   return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:floor] = params[:floor].blank? ? "all" : params[:floor]
    session[:sold_price] = params[:sold_price].blank? ? "all" : params[:sold_price]
    session[:sold_date] = params[:sold_date].blank? ? "all" : params[:sold_date]
    session[:purchaser] = params[:purchaser].blank? ? "all" : params[:purchaser]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
  end

  def nil_session_listing
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:floor] = nil
    session[:sold_price] = nil
    session[:purchaser] = nil
    session[:sold_date] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
  end

  def check_and_update_icon(children_listing)
    img_count = children_listing.images.count rescue 0
    floorp_count = children_listing.floorplans.count rescue 0
    if !children_listing.image_exist && img_count > 0
      children_listing.image_exist = true
      children_listing.save(false)
    end
    if children_listing.image_exist && img_count == 0
      children_listing.image_exist = false
      children_listing.save(false)
    end
    if !children_listing.floorplan_exist && floorp_count > 0
      children_listing.floorplan_exist = true
      children_listing.save(false)
    end
    if children_listing.floorplan_exist && floorp_count == 0
      children_listing.floorplan_exist = false
      children_listing.save(false)
    end
  end

  def tweet(children_listing)
    unless children_listing.blank?
      case children_listing.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      end
    end
    return tweet
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end

  def view_transaction
    @property = Property.find(params[:property_id])
    @transaction = SalesRecord.find(params[:id])
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
    render :layout => 'iframe'
  end
end
