class CategoryContactsController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation

  def index
    @category_contact = CategoryContact.new
  end

  def show
  end

  def new
  end

  def edit
    @category_contact = CategoryContact.find(params[:id])
  end

  def create
   @category_contact = CategoryContact.new(params[:category_contact].merge(:creator_id => current_user.id))
    respond_to do |format|
      if @category_contact.save
        flash[:notice] = 'CategoryContact was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
    @category_contact = CategoryContact.find(params[:id])

    respond_to do |format|
      if @category_contact.update_attributes(params[:category_contact])
        flash[:notice] = 'CategoryContact was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    ContactCategoryRelation.find(:all, :conditions => { :category_contact_id => params[:id] }).each(&:destroy)
    @category_contact = CategoryContact.find(params[:id])
    @category_contact.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def prepare_creation
    @categories = CategoryContact.paginate(:all, :conditions => ["`creator_id` is NULL OR `creator_id` = ?", current_user.id], :order => "`created_at` DESC", :page =>params[:page], :per_page => 10)
  end
end
