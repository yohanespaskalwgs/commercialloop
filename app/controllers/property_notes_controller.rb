class PropertyNotesController < Api::ApiBaseController
  layout "agent", :except => [:new, :create]
  skip_before_filter :verify_authenticity_token, :only => [:auto_complete_for_contact_name]
  require_role ['AgentUser','DeveloperUser'],  :except => [:set_viewer, :get_viewer]
  prepend_before_filter :login_required,  :except => [:set_viewer, :get_viewer]
  before_filter :get_property, :get_agent_and_office, :prepare_data
  before_filter :find_agent_or_developer_by_access_key, :only => [:set_viewer, :get_viewer]
  before_filter :check_permission_page

  def index
    get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    @property_note = PropertyNote.new
    @property_viewers = PropertyNote.property_viewers(@property.id, "`status` is NULL ")
    @general_viewers = PropertyNote.property_viewers(@property.id, "`status` = 'general'")
    @property_enquires = PropertyNote.property_enquires(@property.id)
    @unindicated_source = PropertyNote.unindicated_source(@property.id)
  end

  def show
  end

  def new
    @property_note = PropertyNote.new
    @agent_contact = @property_note.build_agent_contact
  end

  def edit

  end

  def create
    create_contact
    params[:property_note][:note_date] = Time.now if params[:property_note][:note_date].blank?
    @property_note = PropertyNote.new(params[:property_note])
    respond_to do |format|
      if @property_note.save
        @agent_contact= AgentContact.find(params[:property_note][:agent_contact_id])
        @agent_contact.create_note(@property.id, params[:property_note][:description], params[:property_note][:note_type_id], current_user.id)

        @agent_contact.create_alert(@property.id, current_user.id) if params[:property_note][:alert].to_i == 1
        if params[:property_note][:contract].to_i == 1
          @agent_contact.create_note(@property.id, "Take Contract/Application", 12, current_user.id)
          params[:property_note][:note_type_id] = 16
          property_note_contract = PropertyNote.new(params[:property_note])
          property_note_contract.save
        end

        flash[:notice] = 'Property Note was successfully created.'
        format.html { redirect_to :action => 'new' }
      else
        format.html { render :action => "new" }
      end
    end
  end

  def update
  end

  def destroy
  end

  def destroy_note
    @property_note = PropertyNote.find(params[:id])
    @property_note.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def prepare_data
    @property = Property.find(params['property_id'])
    @notes_type = NoteType.find(:all, :conditions => ["`status` = 'property_note'"]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @team_members = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    @heard_froms = HeardFrom.find(:all, :conditions => ["`status` = 'heard_property'"]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
  end

  def auto_complete_for_contact_name
   contacts = AgentContact.search_for_autocomplete(params[:contact][:name], @agent.id, @office.id)
   render :partial => 'offers/contact_list', :locals => { :contacts => contacts}
  end

  def create_contact
    category_id = (@property.type == 'ResidentialLease')? 11 : 8

    unless params[:property_note][:agent_contact_id].blank?
      agent_contact= AgentContact.find_by_id(params[:property_note][:agent_contact_id])
      agent_contact.update_category_contact(category_id) unless agent_contact.blank?
      params[:property_note] = params[:property_note].merge(:duplicate => 0)
    else
      team = []
      team << @property.primary_contact_id unless @property.primary_contact_id.nil?
      team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
      params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id])

      @agent_contact = AgentContact.new(params[:agent_contact])
      if @agent_contact.save
        @agent_contact.add_categories([category_id])
        @agent_contact.add_assigns(team)
        @agent_contact.add_access(team)
        @agent_contact.use_spawn_for_boom
        @agent_contact.use_spawn_for_md
        @agent_contact.use_spawn_for_irealty
        params[:property_note] = params[:property_note].merge(:agent_contact_id => @agent_contact.id, :duplicate => 0)
      end
    end
  end

  def update_duplicate
    category_id = (@property.type == 'ResidentialLease')? 11 : 8

    duplicate = 0
    detected = nil
    params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id])

    %w(email mobile_number contact_number work_number home_number).each{|w|
      unless params[:agent_contact][w].blank?
        check = AgentContact.check_exist(params[:agent_contact], w)
        unless check.nil?
          detected = check
          duplicate = 1
        end
      end
    }

    if duplicate == 1
      unless detected.nil?
        @agent_contact = AgentContact.find(detected.id)
          if @agent_contact.update_attributes(params[:agent_contact].delete_if{|x,y| y.blank?})
            @agent_contact.update_category_contact(category_id)
            params[:property_note][:note_date] = Time.now if params[:property_note][:note_date].blank?
            params[:property_note] = params[:property_note].merge(:agent_contact_id => @agent_contact.id, :duplicate => 0)
            @property_note = PropertyNote.new(params[:property_note])
            @property_note.save
          end
      end
    end
    redirect_to new_agent_office_property_property_note_path(@agent,@office, @property)
  end

  def populate_combobox
    dates, types, heards = [],[], []
    condition = [" property_notes.property_id = ? ", @property.id]
    property_notes = PropertyNote.find(:all, :include => [:note_type, :heard_from], :conditions => condition)
    property_notes.map{|x|
      dates << {
        :date => ( x.note_date.to_s unless x.note_date.blank?) ,
        :dateText => (x.note_date.strftime("%d/%m/%y") unless x.note_date.blank?)
      } unless x.note_date.blank?
      types << {
        :type => (x.note_type_id unless x.note_type_id.blank?) ,
        :typeText => (x.note_type.name unless x.note_type_id.blank?)
      } unless x.note_type.nil?
      heards << {
        :heard => (x.heard_from_id unless x.heard_from_id.blank?) ,
        :heardText => (x.heard_from.name unless x.heard_from_id.blank?)
      } unless x.heard_from_id.nil?
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_dates =[]
    uniq_type =[]
    uniq_heards =[]
    dates.each{|x|uniq_dates << x if !uniq_dates.include?(x) and !x.blank?}
    types.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    heards.each{|x|uniq_heards << x if !uniq_heards.include?(x) and !x.blank?} unless heards.nil?

    uniq_dates.to_a.sort!{|x,y| x[:dateText].to_s <=> y[:dateText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_heards.to_a.sort!{|x,y| x[:heardText].to_s <=> y[:heardText].to_s}

    return(render(:json =>{:results => property_notes.length,
          :dates=>[uniq_dates.unshift({:date =>'all',:dateText=>"All Date"})].flatten,
          :types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Note Types"})].flatten,
          :heards=>[uniq_heards.unshift({:heard=>'all',:heardText=>"All Heard About Property"})].flatten
        }))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      unless session[:ID].blank?
        condition = ["property_notes.property_id = #{session[:ID]} "]
      else
        get_session_listing
        condition = [" property_notes.property_id = #{@property.id} "]

        unless session[:start_date] == "all"
          if(!session[:start_date].blank?)
            start_date = session[:start_date]
            end_date = session[:end_date]
            condition[0] += " AND (property_notes.note_date >= '#{start_date}' and property_notes.note_date <= '#{end_date}')"
          end
        end

        unless session[:date] == "all"
          if(!session[:date].blank?)
            condition[0] += " AND property_notes.note_date = ?"
            condition <<  session[:date]
          end
        end

        unless session[:note_type] == "all"
          if(!session[:note_type].blank?)
            condition[0] += " AND property_notes.note_type_id = ?"
            condition <<  session[:note_type]
          end
        end

        unless session[:heard] == "all"
          if(!session[:heard].blank?)
            if session[:heard] == "NULL"
              condition[0] += " AND property_notes.heard_from_id IS NULL"
            else
              condition[0] += " AND property_notes.heard_from_id = ?"
              condition <<  session[:heard]
            end
          end
        end
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = PropertyNote.build_sorting(params[:sort],params[:dir])
    @property_note = PropertyNote.paginate(:all, :conditions => condition, :include => [:agent_contact, :note_type, :heard_from], :order => order, :page =>params[:page], :per_page => params[:limit].to_i)
    results = 0
    if params[:limit].to_i == 20
      results = PropertyNote.find(:all,:select => 'count(property_notes.property_id)',:conditions => condition,:limit => params[:limit].to_i)
    else
      results = PropertyNote.count(:conditions => condition)
    end
    return(render(:json =>{:results => results,:rows=>@property_note.map{|x|{
               'id' =>  x.id.to_s + "~" + x.agent_contact_id.to_s,
              'date' => (x.note_date.blank? ? "" : x.note_date.strftime("%d/%m/%y")),
              'note_type' => (x.note_type.blank? ? "" : x.note_type.name),
              'first_name' => (x.agent_contact.blank? ? "" : x.agent_contact.first_name),
              'last_name' => (x.agent_contact.blank? ? "" : x.agent_contact.last_name),
              'heard' => (x.heard_from.blank? ? "" : x.heard_from.name),
              'note' =>(x.description.blank? ? "" : x.description)
            } unless x.blank? }}))
  rescue
     return(render(:json =>{:results => nil, :rows=> nil}))
  end

 def get_session_listing
    session[:date] = params[:date].blank? ? "all" : params[:date]
    session[:note_type] = params[:type].blank? ? "all" : params[:type]
    session[:first_name] = params[:first_name].blank? ? "all" : params[:first_name]
    session[:last_name] = params[:last_name].blank? ? "all" : params[:last_name]
    session[:heard] = params[:heard].blank? ? "all" : params[:heard]
    session[:note] = params[:note].blank? ? "all" : params[:note]
    session[:start_date] = params[:start_date].blank? ? "all" : params[:start_date]
    session[:end_date] = params[:end_date].blank? ? "all" : params[:end_date]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "property_notes.note_date DESC"
  end

  def nil_session_listing
    session[:date] = nil
    session[:note_type] = nil
    session[:first_name] = nil
    session[:last_name] = nil
    session[:heard] = nil
    session[:note] = nil
    session[:start_date] = nil
    session[:end_date] = nil
  end

  #--------------------------------------------------
  #   API FOR VIEWER
  #--------------------------------------------------

  def get_viewer
    # get_viewer?type_id=2
    viewer_data = []
    @viewer = PropertyViewer.find_all_by_property_id_and_property_viewer_type_id(params[:property_id],params[:type_id]) unless params[:property_id].blank?
    viewer_data << {:success => true, :errors => " ", :total => @viewer.size}
    respond_to do |format|
      format.xml { render :xml => viewer_data}
    end
  end

  def set_viewer
    # set_viewer?type_id=7&ip_address=127.0.0.1
    viewer_data = []
    unless params[:property_id].blank?
      pro_view = PropertyViewer.new({:property_id => params[:property_id], :property_viewer_type_id => params[:type_id],
          :ip_address  => params[:ip_address], :date => Time.now})

      if pro_view.save
        success = true
        error_messages = ""
      else
        success = false
        error_messages = pro_view.errors
      end
      @viewer = PropertyViewer.find_all_by_property_id_and_property_viewer_type_id(params[:property_id], params[:type_id])
      viewer_data << {:success => success, :errors => error_messages, :total => @viewer.size}
    end
    respond_to do |format|
      format.xml { render :xml => viewer_data}
    end
  end
end
