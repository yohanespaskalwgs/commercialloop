class ResidentialBuildingChildrenListingsController < ApplicationController
  layout "agent"
  before_filter :get_agent_and_office, :get_property

  def index
    get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    @properties_filter = Property.find(:all,:include => [:primary_contact],:conditions=>["office_id=?",current_office.id], :select => "suburb,id, property_type,status,primary_contact_id");
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Listings' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def populate_combobox
    floor,suburb,type,assigned_to,condition = [],[],[],[],[]
    properties = Property.find(:all,:include =>[:primary_contact], :conditions => ["parent_category = ? AND parent_listing_id = ? AND office_id = ?", "NewDevelopment", "#{@property.id}", "#{current_office.id}"])
    properties.map{|x|
      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText =>x.suburb.to_s
      } unless x.suburb.blank?;
      type << {
        :type => x.property_type.to_s,
        :typeText =>x.property_type.to_s
      } unless x.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?) ,
        :assigned_toText => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?)
      } unless x.primary_contact.blank?
      floor << {
        :floor => x.number_of_floors.to_s,
        :floorText => x.number_of_floors.to_s
      } unless x.number_of_floors.blank?;
    }
    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    uniq_floor =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    floor.each{|x|uniq_floor << x if !uniq_floor.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_floor.to_a.sort!{|x,y| x[:floorText].to_s <=> y[:floorText].to_s}
    return(render(:json =>{
          :results => properties.length,
          :suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,
          :floors=>[uniq_floor.unshift({:floor=>'all',:floorText=>"All Floors"})].flatten,
          :types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,
          :team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        if current_user.limited_client_access?
          if !@office.control_level3
            condition[0] += " AND properties.primary_contact_id = ? "
            condition << current_user.id
          end
        elsif current_user.allow_property_owner_access?
          condition[0] += " AND properties.agent_user_id = ?"
          condition << current_user.id
        end

        if(!session[:type].blank? && Property::TYPES.map{|s| s[1]}.include?(session[:type]))
          condition[0] += " AND properties.type = ?"
          condition <<  session[:type]
        end


        if ((current_user.allow_property_owner_access? && session[:status] == 1) || (current_user.allow_property_owner_access?  && session[:status].blank?))|| current_user.allow_property_owner_access?
          session[:status] = "all"
        end

        unless session[:status] == "all"
          unless session[:status].blank?
            condition[0] += " AND properties.status = ?"
            condition <<  session[:status]
          end
        end

        unless session[:suburb] == "all" || session[:suburb].blank?
          condition[0] += " AND properties.suburb = ?"
          condition <<  session[:suburb]
        end

        unless session[:floor] == "all"
          unless session[:floor].blank?
            condition[0] += " AND properties.number_of_floors = ?"
            condition <<  session[:floor]
          end
        end

        #        unless session[:bedroom] == "all" || session[:bedroom].blank?
        #          condition[0] += " AND properties.detail.bedrooms = ?"
        #          condition <<  session[:bedroom]
        #        end
        #
        #        unless session[:bathroom] == "all" || session[:bathroom].blank?
        #          condition[0] += " AND properties.detail.bathrooms = ?"
        #          condition <<  session[:bathroom]
        #        end
        #
        #        unless session[:carspace] == "all" || session[:carspace].blank?
        #          condition[0] += " AND properties.detail.carport_spaces = ?"
        #          condition <<  session[:carspace]
        #        end
        #
        #        unless session[:storage] == "all" || session[:storage].blank?
        #          condition[0] += " AND properties.detail.storage = ?"
        #          condition <<  session[:storage]
        #        end

        unless session[:primary_contact_fullname] == "all" || session[:primary_contact_fullname].blank?
          name =  session[:primary_contact_fullname].split(' ')
          condition[0] += " AND users.first_name  = ?"
          condition << name[0]
          unless name[1].blank?
            condition[0] += " AND users.last_name  = ?"
            condition << name[1]
          end
        end

        unless session[:property_type] == "all" || session[:property_type].blank?
          condition[0] += " AND properties.property_type = ?"
          condition << session[:property_type]
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      if current_user.limited_client_access?
        if !@office.control_level3
          condition[0] += " AND properties.primary_contact_id = ? "
          condition << current_user.id
        end
      elsif current_user.allow_property_owner_access?
        condition[0] += " AND properties.agent_user_id = ?"
        condition << current_user.id
      end
    end
    condition[0] += " AND properties.deleted_at is NULL"

    condition[0] += " AND parent_category = ?"
    condition << "NewDevelopment"

    condition[0] += " AND parent_listing_id = ?"
    condition << @property.id

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = Property.build_sorting(params[:sort],params[:dir])
    if params[:sort] == 'sqm' || params[:sort] == 'bedroom' || params[:sort] == 'bathroom' || params[:sort] == 'carspace'
      @properties = Property.sort_by_customize(params[:sort],params[:controller],params[:dir],params[:page],params[:limit],@agent.id,@office.id,@property.id)
    else
      @properties = Property.paginate(:all, :include =>[:primary_contact,:residential_sale_details,[:purchaser => :agent_contact]], :conditions => condition, :order => order,:select => "properties.id,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,users.first_name,users.last_name,properties.detail.bedrooms,properties.detail.bathrooms,properties.detail.carport_spaces,properties.detail.storage,properties.purchaser.amount,properties.purchaser.date,properties.purchaser.agent_contact.first_name,properties.agent_contact.last_name,properties.detail.number_of_floors,properties.detail.floor_area,residential_sale_details.number_of_floors", :page =>params[:page], :per_page => params[:limit].to_i)
    end

    if @properties.present?
      @properties.each do |property|
        check_and_update_icon(property)
      end
    end
    results = 0
    if params[:limit].to_i == 50
#      results = 50 #Property.find(:all,:select => 'count(properties.id), properties.primary_contact_id',:include =>[:primary_contact],:conditions => condition,:limit => params[:limit].to_i)
results = Property.count(:include =>[:primary_contact,[:purchaser => :agent_contact],:residential_sale_details],:conditions => condition)
    else
      results = Property.count(:include =>[:primary_contact,[:purchaser => :agent_contact],:residential_sale_details],:conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'listing'=>Property.abbrv_properties(x.type),
              'property_type'=>x.property_type.blank? ? "" : x.property_type,
              'address' =>((x.street_number.blank? ? "" : x.street_number)+" "+(x.street.blank? ? "" : x.street.to_s)),
              'suite'=>(x.unit_number.blank? ? "" : x.unit_number),
              'sqm'=>(x.detail.blank? ? "" : (x.detail.floor_area.blank? ? "" : x.detail.floor_area)),
              #'floor'=>((x.detail.number_of_floors.blank? ? "" : x.detail.number_of_floors) unless x.type == "ProjectSale" || x.type == "BusinessSale"),
              #'floor'=>x.number_of_floors,
              'bedroom'=>((x.detail.blank? ? "" : (x.detail.bedrooms.blank? ? "" : x.detail.bedrooms)) unless x.type == "BusinessSale" || x.type == "Commercial"),
              'bathroom'=>((x.detail.blank? ? "" : (x.detail.bathrooms.blank? ? "" : x.detail.bathrooms)) unless x.type == "BusinessSale" || x.type == "Commercial"),
              'carspace'=>((x.detail.blank? ? "" : (x.detail.carport_spaces.blank? ? "" : x.detail.carport_spaces)) unless x.type == "BusinessSale"),
              'storage'=>(x.detail.blank? ? "No" : (x.detail.storage.blank? || x.detail.storage == false ? "No" : "Yes") if x.type == "ResidentialSale"),
              'suburb' => x.suburb,
              'type' => x.property_type,
              'price' => Property.check_price(x),
              'sold_price' =>(x. purchaser.blank? ? "" : (x.purchaser.amount.blank? ? "" : x.purchaser.amount)),
              'sold_date' =>(x.purchaser.blank? ? "" : (x.purchaser.date.blank? ? "" : x.purchaser.date.strftime('%D'))),
              'purchaser' =>(x.purchaser.blank? ? "" : (x.purchaser.agent_contact.blank? ? "" : (x.purchaser.agent_contact.full_name.blank? ? x.purchaser.agent_contact.first_name : x.purchaser.agent_contact.full_name))),
              'assigned_to'=>x.primary_contact.blank? ? '' : x.primary_contact.full_name.to_s.strip,
              'actions'=>Property.check_color(x),'status' => Property.status_name(x),
              'fb'=> [x.id,@office.url.blank? ? (x.detail.respond_to?(:deal_type) ? x.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}", tweet(x)]} unless x.blank? }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:floor] = params[:floor].blank? ? "all" : params[:floor]
    session[:bedroom] = params[:bedroom].blank? ? "all" : params[:bedroom]
    session[:bathroom] = params[:bathroom].blank? ? "all" : params[:bathroom]
    session[:carspace] = params[:carspace].blank? ? "all" : params[:carspace]
    session[:price] = params[:price].blank? ? "all" : params[:price]
    session[:sold_price] = params[:sold_price].blank? ? "all" : params[:sold_price]
    session[:sold_date] = params[:sold_date].blank? ? "all" : params[:sold_date]
    session[:purchaser] = params[:purchaser].blank? ? "all" : params[:purchaser]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
  end

  def nil_session_listing
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:floor] = nil
    session[:bedroom] = nil
    session[:bathroom] = nil
    session[:carspace] = nil
    session[:price] = nil
    session[:sold_price] = nil
    session[:purchaser] = nil
    session[:sold_date] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
  end

  def check_and_update_icon(children_listing)
    img_count = children_listing.images.count rescue 0
    floorp_count = children_listing.floorplans.count rescue 0
    if !children_listing.image_exist && img_count > 0
      children_listing.image_exist = true
      children_listing.save(false)
    end
    if children_listing.image_exist && img_count == 0
      children_listing.image_exist = false
      children_listing.save(false)
    end
    if !children_listing.floorplan_exist && floorp_count > 0
      children_listing.floorplan_exist = true
      children_listing.save(false)
    end
    if children_listing.floorplan_exist && floorp_count == 0
      children_listing.floorplan_exist = false
      children_listing.save(false)
    end
  end

  def tweet(children_listing)
    unless children_listing.blank?
      case children_listing.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      end
    end
    return tweet
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end
end
