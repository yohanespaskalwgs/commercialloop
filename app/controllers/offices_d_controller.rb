class OfficesDController < OfficesController
  layout 'developer'
  before_filter :login_required, :except => [:new, :create_office]
  require_role 'DeveloperUser', :except => [:new, :create_office]

  prepend_before_filter :get_developer_and_agent
  append_before_filter :prepare_data, :only => [:new, :create, :edit, :update, :create_office]
  before_filter :add_or_edit_active_help, :only => [:edit]
  before_filter :set_hostname
  before_filter :clear_cache, :only => [:create, :delete_office]
  # caches_action :index, :cache_path => Proc.new {|c| c.request.url }

  def index
    session[:sorted_by] = "offices.created_at #{params[:sort_type]}" if params[:sort_by] == "created_at"
    session[:sorted_by] = "offices.name #{params[:sort_type]}" if params[:sort_by] == "name"
    session[:sorted_by] = "offices.id #{params[:sort_type]}" if params[:sort_by] == "id"
    conditions = [" "]
    conditions << " AND offices.id = '#{params[:office]}' " unless params[:office].blank?
    conditions << " AND offices.status = '#{params[:status]}' " unless params[:status].blank?
    conditions << " AND agents.id = '#{params[:client]}' " unless params[:client].blank?
    conditions << " AND offices.deleted_at IS NULL "
    conditions << " AND agents.deleted_at IS NULL "

    count_properties_agent = "(select count(id) from properties where office_id = offices.id and deleted_at IS NULL) as count_properties, (select count(id) from users where office_id = offices.id and type = 'AgentUser' and deleted_at IS NULL) as count_agents"
    order_by = "ORDER BY agents.name #{params[:sort_type]}" if params[:sort_by] == "client_name"
    order_by = "ORDER BY count_properties #{params[:sort_type]}" if params[:sort_by] == "listing"
    order_by = "ORDER BY count_agents #{params[:sort_type]}" if params[:sort_by] == "team"
    order_by = "ORDER BY offices.created_at #{params[:sort_type]}" if params[:sort_by] == "created_at"
    order_by = "ORDER BY offices.name #{params[:sort_type]}" if params[:sort_by] == "name"
    order_by = "ORDER BY offices.id #{params[:sort_type]}" if params[:sort_by] == "id"

    order_by = "ORDER BY offices.name ASC" if order_by.blank?
    sql = "SELECT offices.*, #{count_properties_agent} FROM offices INNER JOIN agents ON offices.agent_id = agents.id WHERE agents.developer_id = #{@developer.id} #{conditions} #{(order_by unless order_by.blank?)}"
    @offices = Office.paginate_by_sql(sql, :page => (params[:page].blank? ? 1 : params[:page]), :per_page => 100)
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE '%Client Office%'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end

  def new
    if request.url().scan("outside").blank?
      unless logged_in?
        redirect_to find_user_home_path and return
      end
      add_or_edit_active_help
      @office = @agent.offices.new
      @office.apply_agent_settings
      @office_developer_accesses_ids = @office.office_developer_accesses.map(&:developer_user_id) unless @office.office_developer_accesses.blank?
      @agent_user = @office.agent_users.build
      respond_to do |format|
        format.html # new.html.erb
      end
    else
      @office = @agent.offices.new
      @office.apply_agent_settings
      @office_developer_accesses_ids = @office.office_developer_accesses.map(&:developer_user_id) unless @office.office_developer_accesses.blank?
      @agent_user = @office.agent_users.build
      respond_to do |format|
        format.html { render :file => "/offices_d/outside", :layout => "new_level4"}
      end
    end
  end

  def create_office
    contact = @developer.developer_users.first
    params[:office] = params[:office].merge({:upgrade_contact_id => contact.id, :support_contact_id => contact.id, :developer_contact_id => contact.id})
    @office = @agent.offices.new(params[:office])
    (params[:developer_accesses] ||= []).each do |access|
      @office.office_developer_accesses.build(:developer_user_id => access)
    end
    @office.is_developer = true
    @office.xml_api_enable = @agent.developer.xml_api_enable
    @office.http_api_enable = @agent.developer.http_api_enable
    @office.status = "active" if @agent.developer.active_offices == true
    is_saved = @office.save
    if is_saved
      if @agent.developer.active_offices == true
        sp = @office.subscription_plan('one_monthly')
        @office.subscription.update_attributes({:starts_on => Time.now, :status => "ok", :next_payment_date => sp['trial_end_date'], :ends_on => nil})
      end
      #==== Adding domains for new office ====
      unless @agent.developer.domains.blank?
        @agent.developer.domains.each do |dev_dom|
          domain_new = Domain.new(dev_dom.attributes)
          domain_new.office_id = @office.id
          domain_new.developer_id = nil
          domain_new.save!
        end
      end
      #==== Adding export for new office ====
      unless @agent.developer.portal_combineds.blank?
       @agent.developer.portal_combineds.each do |pc|
        @office.portal_accounts << PortalAccount.create(:portal_name => pc.portal.portal_name, :active_feeds => 1)
        PortalAutosubcribe.create(:office_id => @office.id, :portal_id => pc.portal.id) if pc.autosubcribe == true
       end
      end
    end

    @agent_user = AgentUser.new
    @agent_user.developer = @office.agent.developer
    @agent_user.email = params[:agent_user][:email]
    @agent_user.first_name = params[:agent_user][:first_name]
    @agent_user.last_name = params[:agent_user][:last_name]
    @agent_user.generate_sequential_code # force this as we don't validate
    @agent_user.login = params[:agent_user][:login]
    @agent_user.password = params[:agent_user][:password]
    @agent_user.password_confirmation = params[:agent_user][:password_confirmation]
    @agent_user.code = @office.generate_sequential_code
    @agent_user.office_id = @office.id
    @agent_user.active_help_status = true
    @agent_user.group = "Sales Agent"
    @agent_user.roles << Role.find(3)
    active_helps = ActiveHelpPage.find_by_sql("SELECT * FROM active_help_pages WHERE user_type = 'AgentUser'")
    @agent_user.active_help_pages = active_helps
    respond_to do |format|
      if @agent_user.save and is_saved
        ActiveRecord::Base.connection.execute("UPDATE offices SET office_contact = '#{@agent_user.id}' WHERE id =#{@agent_user.office.id}")
        ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id =#{@agent_user.id}")
        UserMailer.deliver_agent_user_creation(@agent_user, @office) unless params[:notification].blank?
        @office= nil
        flash[:notice] = 'Office was successfully added.'
        format.html { redirect_to new_developer_agent_office_path(@developer, @agent) + "?outside" }
      else
        @office.destroy
        @office_developer_accesses_ids = @office.office_developer_accesses.map(&:developer_user_id) unless @office.office_developer_accesses.blank?
        format.html { render :file => "/offices_d/outside", :layout => "new_level4" }
      end
    end
  end

  # POST /offices
  # POST /offices.xml
  def create
    @office = @agent.offices.new(params[:office])
    (params[:developer_accesses] ||= []).each do |access|
      @office.office_developer_accesses.build(:developer_user_id => access)
    end
    @office.is_developer = true
    developer = @agent.developer
    @office.xml_api_enable = developer.xml_api_enable
    @office.http_api_enable = developer.http_api_enable
    @office.status = "active" if developer.active_offices == true
    is_saved = @office.save
    if is_saved
      spawn(:kill => true) do
        if developer.active_offices == true
          sp = @office.subscription_plan('one_monthly')
          @office.subscription.update_attributes({:starts_on => Time.now, :status => "ok", :next_payment_date => sp['trial_end_date'], :ends_on => nil})
        end
        unless params[:domain].blank?
          params[:domain].each do |domain|
            domain_att = Domain.find_by_name(domain[:name])
            if !domain_att.blank?
              domain_att.attributes = domain
            else
              domain_att = Domain.new(domain)
              domain_att.office = (@office)
            end
            domain_att.save
          end
        end
        #==== Adding domains for new office ====
        unless developer.domains.blank?
          developer.domains.each do |dev_dom|
            domain_new = Domain.new(dev_dom.attributes)
            domain_new.office_id = @office.id
            domain_new.developer_id = nil
            domain_new.save!
          end
        end
        #==== Adding export for new office ====
        unless developer.portal_combineds.blank?
         developer.portal_combineds.each do |pc|
          @office.portal_accounts << PortalAccount.create(:portal_name => pc.portal.portal_name, :active_feeds => 1)
          PortalAutosubcribe.create(:office_id => @office.id, :portal_id => pc.portal.id) if pc.autosubcribe == true
         end
        end
      end
    end

    @agent_user = AgentUser.new
    @agent_user.developer = @office.agent.developer
    @agent_user.email = params[:agent_user][:email]
    @agent_user.first_name = params[:agent_user][:first_name]
    @agent_user.last_name = params[:agent_user][:last_name]
    @agent_user.generate_sequential_code # force this as we don't validate
    @agent_user.login = params[:agent_user][:login]
    @agent_user.password = params[:agent_user][:password]
    @agent_user.password_confirmation = params[:agent_user][:password_confirmation]
    @agent_user.code = @office.generate_sequential_code
    @agent_user.office_id = @office.id
    @agent_user.active_help_status = true
    @agent_user.group = "Sales Agent"
    @agent_user.roles << Role.find(3)
    active_helps = ActiveHelpPage.find_by_sql("SELECT * FROM active_help_pages WHERE user_type = 'AgentUser'")
    @agent_user.active_help_pages = active_helps
    respond_to do |format|
      if @agent_user.save and is_saved
        ActiveRecord::Base.connection.execute("UPDATE offices SET office_contact = '#{@agent_user.id}' WHERE id =#{@office.id}")
        ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id =#{@agent_user.id}")
        unless params[:notification].blank?
          spawn(:kill => true) do
            UserMailer.deliver_agent_user_creation(@agent_user, @office)
          end
        end
        flash[:notice] = 'Office was successfully added.'
        format.html { redirect_to edit_developer_agent_office_path(@developer, @agent, @office) }
      else
        @office.destroy
        @office_developer_accesses_ids = @office.office_developer_accesses.map(&:developer_user_id) unless @office.office_developer_accesses.blank?
        format.html { render :action => 'new' }
      end
    end
  end

  def edit
    @office = @agent.offices.find(params[:id])
    return access_denied unless @office.accessible_by? current_user
    @office_developer_accesses_ids = @office.office_developer_accesses.map(&:developer_user_id) unless @office.office_developer_accesses.blank?
    @watermark = @office.watermark
    @specialities = @list_specialities = ''
    @office.office_specialities_relations.map{|cat|
      @specialities = @specialities + '#' + cat.office_speciality_id.to_s
      @list_specialities = @list_specialities + '<div id="'+cat.office_speciality_id.to_s+'" style="margin-right:20px;">'+cat.office_speciality.name+'<a href="javascript:remove_speciality('+cat.office_speciality_id.to_s+')" style="color:#0E7FC7">x</a></div>'
    }
    respond_to do |format|
      format.html # edit.html.erb
    end
  end

  def delete_domain
    domain = Domain.find_by_id(params[:domain_id])
    if domain
      ActiveRecord::Base.connection.execute("DELETE FROM property_domain_exports WHERE domain_id = #{params[:domain_id]}")
      if domain.destroy
        return(render(:json => true))
      else
        return(render(:json => false))
      end
    else
      return(render(:json => false))
    end
  end

  def delete_website
    so = SuperapiOffice.find(:first, :conditions => "office_id = #{params[:id]} AND website_id = #{params[:website_id]}")
    if so
      if so.destroy
        return(render(:json => true))
      else
        return(render(:json => false))
      end
    else
      return(render(:json => false))
    end
  end

  # PUT /offices/1
  # PUT /offices/1.xml
  def update
    @office = @agent.offices.find(params[:id])
    @office_developer_accesses_ids = (params[:developer_accesses] || []).map(&:to_i)
    return access_denied unless @office.accessible_by? current_user

    unless params[:office][:new_agent_id].blank?
      new_agent_id = params[:office][:new_agent_id]
      ActiveRecord::Base.connection.execute("UPDATE properties SET agent_id = '#{new_agent_id}' WHERE office_id =#{@office.id}")
      ActiveRecord::Base.connection.execute("UPDATE agent_contacts SET agent_id = '#{new_agent_id}' WHERE office_id =#{@office.id}")
      params[:office][:agent_id] = params[:office][:new_agent_id]
    end

    @office.attributes = params[:office]
    @office.is_developer = true
    @office.include_watermark = params[:watermark][:include_watermark].blank? ? false : true
    @office.send_contact = params[:office][:send_contact].blank? ? false : true
    @office.update_listing_weekly = params[:office][:update_listing_weekly].blank? ? false : true
    @office.activate_vault = params[:office][:activate_vault].blank? ? false : true
    respond_to do |format|
      if @office.save!
        @office.office_developer_accesses.clear
        @agent_user = AgentUser.find(params[:office][:office_contact])
        @agent_user.office.office_contact = @agent_user.id
        @agent_user.office.save
        if @agent_user.crypted_password.blank?
          @agent_user.generate_password
          @agent_user.save
        end
        UserMailer.deliver_agent_user_creation(@agent_user, @office) unless params[:notification].blank?
        @office_developer_accesses_ids.each do |access|
          @office.office_developer_accesses.create(:developer_user_id => access)
        end
        unless params[:specialities_id].blank?
          @ids = params[:specialities_id].split('#')
          @office.add_specialities(@ids)
        else
          @office.office_specialities_relations.delete_all unless @office.office_specialities_relations.blank?
        end

        flash[:notice] = 'Office was successfully updated.'

        unless params[:office][:property_url].blank?
          if params[:office][:property_url] == '1'
            spawn(:kill => true) do
              @office.properties.each do |property|
                begin
                  if !@office.url.blank? && !property.blank?
                    property.detail.update_attribute(:property_url, "http://#{@office.url.gsub('http://', '')}/#{property.id}") unless property.detail.blank?
                  end
                rescue
                end
              end
            end
          end
        end

        unless params[:watermark][:uploaded_data].blank?
          old_watermark = @office.watermark
          watermark = Watermark.new(params[:watermark])
          if watermark.content_type.to_s == "image/gif"
            watermark.attachable = @office
            if watermark.save
              old_watermark.destroy unless old_watermark.blank?
            end
          else
            flash[:notice] = 'Please upload image for gif format.'
            format.html { render :action => "edit" }
          end
        end
        format.html { redirect_to developer_offices_path(@developer) }
      else
        @specialities = @list_specialities = ''
        unless params[:specialities_id].blank?
          @ids = params[:specialities_id].split('#')
          @office_specialities_data = OfficeSpeciality.find(:all, :conditions => ['`id` IN (?)', @ids])
          @specialities = @list_specialities = ''
          @office_specialities_data.map{|cat|
            @specialities = @specialities + '#' + cat.id.to_s
            @list_specialities = @list_specialities + '<div id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.name+'<a href="javascript:remove_speciality('+cat.id.to_s+')">x</a></div>'
          }
        end
        format.html { render :action => "edit" }
      end
    end
  end

  def edit_export_setting
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Exports'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
    @office = Office.find(params[:id])
    country = Country.find_by_name(@office.country)
    @portal_countries = country.portal_countries
    @periods = [['Please select', nil], ['Updates Today', 24*60], ['Updates in Last Week', 24*7*60], ['Updates in Last Month', 24*30*60], ['All Updates', 'All']]

    # Domain Project exports
    @domain_projects = ExportsDomainProject.find(:all, :conditions => "developer_id = #{@developer.id}")
  end

  def send_export
    @office = Office.find(params[:id])
    @portal = Portal.find(params[:portal_id])
    respond_to do |format|
      format.js {render :partial => "offices_d/send_export", :layout => false }
    end
  end

  def send_export_to_all
    @office = Office.find(params[:id])
    portal = Portal.find_by_id(params[:portal][:id])
    portal_account = PortalAccount.find_by_sql("SELECT * FROM portal_accounts WHERE portal_name = '#{portal.portal_name}' AND office_id = #{@office.id}")[0]
    url = translate_url(request.env["HTTP_HOST"] + "/xml_file/portalxmlexport/scan_office.php?id=#{@office.id}" + "&portal=#{portal.portal_name.downcase}"+"&period=120")
    if system("curl " + "\"#{url}\"")
      respond_to do |format|
        if portal_account.update_attribute(:first_time_export, true)
          flash[:notice] = 'Export was successfully sent.'
          format.js {redirect_to :action => "edit_export_setting"}
        else
          flash[:error] = 'Export has failed.'
          format.js
        end
      end
    else
      respond_to do |format|
        flash[:error] = 'Export has failed.'
        format.js {redirect_to :action => "edit_export_setting"}
      end
    end
  end

  def update_export_setting
    @office = Office.find(params[:id])
    country = Country.find_by_name(@office.country)
    if !params[:portal_feeds].blank?
      params[:portal_feeds].each do |x|
        portal = Portal.find_by_id(x)
        unless portal.blank?
          portal_account = PortalAccount.find_by_sql("SELECT * FROM portal_accounts WHERE portal_name = '#{portal.portal_name}' AND office_id = #{@office.id}")[0]
          unless portal_account.blank?
            checked = params["active_feeds"]["#{portal.id}"].blank? ? 0 : params["active_feeds"]["#{portal.id}"]
            portal_account.update_attributes({:account_id => params["account_id_#{portal.id}"], :active_feeds => checked})
          else
            @office.portal_accounts << PortalAccount.new(:portal_name => portal.portal_name, :account_id => params["account_id_#{portal.id}"], :active_feeds => 1)
#            send_portal_notif_to_level1(portal)
          end
        end
      end
      unless country.portal_countries.blank?
        country.portal_countries.each do |portal_country|
          unless params[:portal_feeds].include?("#{portal_country.portal.id}")
            portal_account = PortalAccount.find_by_sql("SELECT * FROM portal_accounts WHERE portal_name = '#{portal_country.portal.portal_name}' AND office_id = #{@office.id}")[0]
            portal_account.update_attributes({:active_feeds => params["active_feeds"]["#{portal_country.portal.id}"]}) unless portal_account.blank?
          end
        end
      end
    else
      params[:active_feeds].delete_if{|k,v| v == "0"}
      params[:active_feeds].each_key do |x|
        portal = Portal.find_by_id(x)
        unless portal.blank?
          portal_account = PortalAccount.find_by_sql("SELECT * FROM portal_accounts WHERE portal_name = '#{portal.portal_name}' AND office_id = #{@office.id}")[0]
          unless portal_account.blank?
            checked = params["active_feeds"]["#{portal.id}"].blank? ? 0 : params["active_feeds"]["#{portal.id}"]
            portal_account.update_attributes({:account_id => params["account_id_#{portal.id}"], :active_feeds => checked})
          else
            @office.portal_accounts << PortalAccount.new(:portal_name => portal.portal_name, :account_id => params["account_id_#{portal.id}"], :active_feeds => 1)
#            send_portal_notif_to_level1(portal)
          end
        end
      end
      unless country.portal_countries.blank?
        country.portal_countries.each do |portal_country|
          unless params[:active_feeds].include?("#{portal_country.portal.id}")
            portal_account = PortalAccount.find_by_sql("SELECT * FROM portal_accounts WHERE portal_name = '#{portal_country.portal.portal_name}' AND office_id = #{@office.id}")[0]
            portal_account.update_attributes({:active_feeds => params["active_feeds"]["#{portal_country.portal.id}"]}) unless portal_account.blank?
          end
        end
      end
    end

    if @office.save
       @office.add_autosubcribes(params[:portal_autos])
       @office.update_property_exports
      flash[:notice] = 'Export settings for this office has been updated.'
    else
      flash[:error] = 'Export settings for this office has failed.'
    end
    redirect_to edit_export_setting_developer_office_path(@developer, @office)
  end

  def update_domain_projects
    if params["domain_projects"].present?
      params["domain_projects"].each do |project|
        if project["project_name"].present? && project["domain_id"].present?
          if project["id"].present?
            domain_project = ExportsDomainProject.find(:first, :conditions => "id = #{project["id"]} and developer_id = #{@developer.id}")
            domain_project.update_attributes(project)
            if project["enabled"].blank?
              domain_project.update_attributes(:enabled => false)
            end
          else
            new_domain_project = ExportsDomainProject.new(project)
            new_domain_project.developer_id = @developer.id
            new_domain_project.save
          end
        end
      end
    end
    office = Office.find(params[:id])
    redirect_to edit_export_setting_developer_office_path(@developer, office)
  end

  def delete_domain_project
    office = Office.find(params[:id])
    domain_project = ExportsDomainProject.find(:first, :conditions => "id = #{params["project_id"]} and developer_id = #{@developer.id}")
    if domain_project.present?
      property_domain_projects = PropertyDomainProject.find(:all, :conditions => "domain_project_id = #{domain_project.id}")
      if property_domain_projects.present?
        ActiveRecord::Base.connection.execute("UPDATE properties SET domain_project = NULL WHERE id IN (#{property_domain_projects.map{|a| a.property_id}.join(',')})")
        property_domain_projects.each do |pdp|
          pdp.destroy
        end
      end
      domain_project.destroy
    end
    redirect_to edit_export_setting_developer_office_path(@developer, office)
  end

  def send_portal_notif_to_level1(portal)
    users = @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
    users.each do |user|
      pas = PortalAutosubcribe.find(:first, :conditions => "office_id = #{@office.id} And portal_id = #{portal.id}")
      begin
        UserMailer.deliver_portal_activated_for_level1(user, portal.portal_name, (pas.blank? ? false : true)) unless portal.blank?
      rescue
      end
    end
  end

  def api_details
    @office = Office.find_by_id(params[:id])
    @superapi_offices =  SuperapiOffice.find(:all, :conditions => "office_id = #{@office.id}")
#    @superapi_offices = SuperapiOffice.find_by_sql("SELECT * from superapi_offices JOIN superapi_websites ON superapi_websites.id = superapi_offices.website_id WHERE office_id = #{@office.id}")
    if @office
      @agent = @office.agent
      @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'API'")
      @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
    else
      redirect_to developer_offices_path(@developer)
    end
  end

  # office api : xml_api and http_api
  def update_office_api
    @office = Office.find(params[:id])
    @office.xml_api_enable = params[:office][:xml_api_enable]
    unless params[:domain].blank?
      is_filled = false
      params[:domain].each do |domain|
        domain_att = Domain.find(:all, :conditions => ["name = ?",domain[:name]])
        is_filled = true if !domain[:name].blank? && !domain[:ipn].blank? && params[:office][:http_api_enable] == "1"
        is_updated = false

        portal_durl = domain[:name].downcase.gsub('http://', '')
        portal_durl = portal_durl.gsub('www.', '')
        portal = Portal.find(:first, :conditions => "`ftp_url` LIKE '%#{portal_durl}%'")
        unless portal.blank?
          pext = {"portal_export" => portal.http_api_enable}
        else
          pext = {"portal_export" => nil}
        end

        domain[:ipn] = domain[:ipn].downcase.gsub(/\s+/, "")
        domain[:ipn] = "http://#{domain[:ipn]}" unless domain[:ipn].include?("http://")
        domain[:enable_export] = 0 if domain[:enable_export].blank?

        enable_export = domain[:enable_export]
        domain.delete(:enable_export)

        domain = domain.merge pext
        if domain_att
          if !domain_att.map{|x| x.office_id}.include?(params[:id].to_i)
            domain_att = Domain.new(domain)
            domain_att.office = (@office)
            domain_att.enable_export = enable_export
          else
            domain_ex = Domain.find(:first, :conditions => ["name = ? and office_id = ?",domain[:name], @office.id])
            domain_ex.update_attribute(:enable_export, enable_export)
            is_updated = true
          end
        else
          domain_att = Domain.new(domain)
          domain_att.office = (@office)
          domain_att.enable_export = enable_export
        end

        if is_updated
          domain_att.is_a?(Array) ? domain_att.each { |dom| dom.update_attributes(domain) } : domain_att.update_attributes(domain)
        else
          domain_att.is_a?(Array) ? domain_att.each { |dom| dom.save } : domain_att.save
        end
      end
      #delete updated domains
      office_domains = Domain.find(:all, :conditions => ["office_id = ?", params[:id].to_i])
      office_domains.each do |dom|
        Domain.destroy(dom.id) if !params[:domain].map{|x| x[:name]}.include?(dom.name)
      end
      @office.http_api_enable = is_filled
    else
      @office.http_api_enable = false
    end

    #Adding websites -------------------------------------------------------------
    require 'net/http'

    if params[:website].present?
      params[:website].each do |web|
        @url_valid = false
        begin
          if web["url"].present?
            web_url = web["url"].gsub("http://","")
            web_url = web_url.gsub(" ","")
            web_url += "/" if web_url.last != "/"
            web_response = Net::HTTP.get_response(URI.parse("http://#{web_url}?zooapi=ping"))
            json_response = ActiveSupport::JSON.decode(web_response.body)
            if json_response["status"] == 1 || json_response["status"] == true
              @url_valid = true
              sw = SuperapiWebsite.find(:first, :conditions => "url LIKE '%#{web_url}%'")
              if sw.blank?
  #              ActiveRecord::Base.connection.execute("INSERT INTO `superapi_websites` (`server_id`, `url`, `version`, `wp_version`, `verified`, `next_ping`, `errors`, `first_seen_down`, `created`, `updated`) VALUES ('0', '#{"http://#{web_url}"}', '', '', '0', '0', '0', '0', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());")
                sql = ActiveRecord::Base.connection()
                sql.insert "INSERT INTO `superapi_websites` (`server_id`, `url`, `version`, `wp_version`, `verified`, `next_ping`, `errors`, `first_seen_down`, `created`, `updated`) VALUES ('0', '#{"http://#{web_url}"}', '', '', '0', '0', '0', '0', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());"
                sql.commit_db_transaction
                sw = SuperapiWebsite.find(:first, :conditions => "url LIKE '%#{web_url}%'")
              end
              so = SuperapiOffice.find(:first, :conditions => "office_id = #{@office.id} AND website_id = #{sw.id}")
              if so.blank?
                ActiveRecord::Base.connection.execute("INSERT INTO `superapi_offices` (`website_id`, `office_id`, `enabled`, `send_full_offices`, `next_listoffices`, `send_full_contacts`, `next_listcontacts`, `send_full_properties`, `next_listproperties`, `created`, `updated`) VALUES ('#{sw.id}', '#{@office.id}', '1', '0', '0', '0', '0', '0', '0', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());")
              else
                if web["enable"].blank? && so.enabled == 1
                  so.update_attributes(:enabled => 0)
                  so.update_attributes(:updated => Time.now.to_i)
                elsif web["enable"].present? && so.enabled == 0
                  so.update_attributes(:enabled => 1)
                  so.update_attributes(:updated => Time.now.to_i)
                end
              end
            else
              flash[:url_invalid] = "Invalid URL: #{web_url} is not a valid URL for API."
            end
          end
        rescue Exception => ex
          if !@url_valid
            flash[:url_invalid] = "Invalid URL: #{web_url} is not a valid URL for API."
          end
          Log.create(:message => "Superapiwebsite add error : #{ex.inspect}, URL: #{web["url"]}, Office: #{@office.id}")
        end
      end
    end

    respond_to do |format|
      if @office.save
        flash[:notice] = 'Office was successfully updated.'
        if @url_valid
          format.html { redirect_to developer_offices_path(@developer) }
        else
          format.html { redirect_to api_details_developer_office_path(@developer, @office) }
        end
      else
        format.html { redirect_to api_details_developer_office_path(@developer, @office) }
      end
    end
  end

  def delete_office
    office = Office.find_by_id(params[:id])
    office.delete_office unless office.blank?
    respond_to do |format|
      flash[:notice] = 'Office was successfully deleted.'
      format.html { redirect_to :action => 'index' }
      format.xml  { head :ok }
    end
  end

  def populate_specialities
    @office_specialities = OfficeSpeciality.find(:all).map{|c| [c.name, c.id]}
    return(render(:json => @office_specialities))
  end

  def trigger
    @office = Office.find(params[:id])
    unless params[:domain_id].blank?
      conditions = "and office_id = #{params[:id]} and (deleted_at IS NULL)"
      Delayed::Job.enqueue(TriggerApi.new("PropertyDomain", params[:domain_id], conditions), 3, 1.seconds.from_now.getutc)
      flash[:notice] = "Your domain has successfully triggered"
    end
    redirect_to api_details_developer_office_path(@developer, @office)
  end

  def send_properties
    so = SuperapiOffice.find(params[:so_id])
    if so.present?
      so.update_attributes(:next_listproperties => 0)
      so.update_attributes(:updated => Time.now.to_i)
    end
    redirect_to api_details_developer_office_path(@developer, params[:id])
  end

  def send_contacts
    so = SuperapiOffice.find(params[:so_id])
    if so.present?
      so.update_attributes(:next_listcontacts => 0)
      so.update_attributes(:updated => Time.now.to_i)
    end
    redirect_to api_details_developer_office_path(@developer, params[:id])
  end

  def send_office
    so = SuperapiOffice.find(params[:so_id])
    if so.present?
      so.update_attributes(:next_listoffices => 0)
      so.update_attributes(:updated => Time.now.to_i)
    end
    redirect_to api_details_developer_office_path(@developer, params[:id])
  end

  private

  def clear_cache
    expire_action :action => :index
    begin
      system("rm -rf #{RAILS_ROOT}/public/page_cache/views/#{request.subdomains[0]}.#{request.domain}/developers/#{@developer.id} #{RAILS_ROOT}/public/page_cache/views/commercialloopcrm.com.au/developers/#{@developer.id}")
    rescue
    end
  end

  def add_or_edit_active_help
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add/Edit Office'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end

  def get_developer_and_agent
    if params[:token].blank?
      if current_user
        render_optional_error_file(401) and return false unless (current_user.developer_id.to_s == params[:developer_id] || current_user.class.name.include?("AdminUser"))
      end
    end
    @developer = Developer.find params[:developer_id]
    if params[:agent_id]
      @agent = @developer.agents.find params[:agent_id]
    end
  end

  def prepare_data
    @color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #999999 #CCCCCC #f0f0f0 #FFFFFF)
    @agents = @developer.agents.map{|t| [t.name,t.id]}.unshift(['Please select', nil])
    @office_specialities = OfficeSpeciality.find(:all).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    countries = Country.find(:all, :conditions => "display_country = 1")
    @countries = countries.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    @contacts = @developer.developer_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil])
    @status = %w(Active Inactive Pending Suspended).map{|s| [s, s.downcase]}.unshift(['Please select', nil])
  end
end
