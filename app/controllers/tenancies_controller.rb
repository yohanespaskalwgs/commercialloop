require 'will_paginate/array'
class TenanciesController < ApplicationController
  layout 'agent'
  before_filter :get_agent_and_office, :get_property

  def index
    get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def edit
    @property = Property.find(params[:property_id])
    @tenancy = TenancyRecord.find(params[:id])
    @net_gross = [['Please select', nil],['Gross', 'Gross'],['Net', 'Net']]
    @sales_person = AgentUser.find(:all, :conditions => "office_id = '#{@office.id}'").map{|x|[x.full_name.blank? ? x.first_name : x.full_name,x.id]}.unshift(['Please select', nil])
    @contact_name = @tenancy.agent_contact.full_name.blank? ? @tenancy.agent_contact.contact_data_alternative :  @tenancy.agent_contact.full_name unless @tenancy.agent_contact.nil?

    if @tenancy.blank?
      @purchaser = TenancyRecords.new
      @purchaser_contact = @purchaser.build_agent_contact
    else
      @purchaser = @tenancy
      @purchaser_contact = @purchaser.agent_contact_id.nil? ? @purchaser.build_agent_contact : @purchaser.agent_contact

      # Tenancy
      @company = @tenancy.agent_contact.blank? ? "" : (@tenancy.agent_contact.agent_company.blank? ? "" : (@tenancy.agent_contact.agent_company.company.blank? ? "" : @tenancy.agent_contact.agent_company.company))
      @contact = @tenancy.agent_contact.blank? ? "" : (@tenancy.agent_contact.full_name.blank? ? @tenancy.agent_contact.first_name : @tenancy.agent_contact.full_name)
      @phone = @tenancy.agent_contact.blank? ? "" : (@tenancy.agent_contact.contact_number.blank? ? "" : @tenancy.agent_contact.contact_number)
      @mobile = @tenancy.agent_contact.blank? ? "" : (@tenancy.agent_contact.mobile_number.blank? ? "" : @tenancy.agent_contact.mobile_number)
      @email = @tenancy.agent_contact.blank? ? "" : (@tenancy.agent_contact.email.blank? ? "" : @tenancy.agent_contact.email)
      @rent = @tenancy.rent.blank? ? "" : @tenancy.rent
      @net_gross = @tenancy.net_gross.blank? ? "" : @tenancy.net_gross
      @outgoing = @tenancy.outgoing.blank? ? "" : @tenancy.outgoing
      @total_rent_psm = @tenancy.total_rental_psm.blank? ? "" : @tenancy.total_rental_psm
      @total_rent_pa = @tenancy.total_rental_pa.blank? ? "" : @tenancy.total_rental_pa
      @lease_term = @tenancy.lease_term.blank? ? "" : @tenancy.lease_term
      @options = @tenancy.option.blank? ? "" : @tenancy.option
      @start_date = @tenancy.start_date.blank? ? "" : @tenancy.start_date
      @end_date = @tenancy.end_date.blank? ? "" : @tenancy.end_date
      @tenancy_fee = @tenancy.fee.blank? ? "" : @tenancy.fee
      @lease_signed = @tenancy.lease_signed.blank? ? "" : @tenancy.lease_signed
      @office_note = @tenancy.office_note.blank? ? "" : @tenancy.office_note
      @tenancy_transaction_sales_person = @tenancy.transaction_sales_person.blank? ? "" : @tenancy.transaction_sales_person
      @tenancy_external_agent_company = @tenancy.external_agent_company.blank? ? "" : @tenancy.external_agent_company
      @tenancy_external_agent_sales_person = @tenancy.external_agent_sales_person.blank? ? "" : @tenancy.external_agent_sales_person
      @tenancy_conjunction_fee = @tenancy.conjunction_fee.blank? ? "" : @tenancy.conjunction_fee
      @tenancy_scale_of_fee = @tenancy.scale_of_fee.blank? ? "" : @tenancy.scale_of_fee
      @reminder_alert = @tenancy.reminder_alert.blank? ? "" : @tenancy.reminder_alert
      @lease_expiry_sales_person = @tenancy.lease_expiry_sales_person.blank? ? "" : @tenancy.lease_expiry_sales_person
      @contract_link = @tenancy.contract_link.blank? ? "" : @tenancy.contract_link
      # End Tenancy
    end
  end

  def update_status
    @property = Property.find(params[:property_id])
    @tenancy = TenancyRecord.find(params[:id])
    new_purchaser = create_note_for_contact = new_transaction = false
    @tenancy.nil? ? new_purchaser = true : @purchaser = @tenancy
    (previous_agent_contact_id = @tenancy.agent_contact_id unless @tenancy.agent_contact_id.nil?) unless @tenancy.nil?
    respond_to do |format|
      begin
        TenancyRecord.transaction do
          category_id = (@property.type == 'ResidentialLease')? 5 : 1
          params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => @agent.id, :office_id => @office.id, :contact_type => "Individual")
          if new_purchaser
            unless params[:transaction].blank?
              @purchaser = TenancyRecord.new({
                  :property_id => @property.id,
                  :agent_contact_id => params[:purchaser][:agent_contact_id],
                  :rent => params[:tenancy][:rent],
                  :net_gross => params[:tenancy][:net_gross],
                  :outgoing => params[:tenancy][:outgoing],
                  :total_rental_pa => params[:tenancy][:total_rental_pa],
                  :total_rental_psm => params[:tenancy][:total_rental_psm],
                  :lease_term => params[:tenancy][:lease_term],
                  :option => params[:tenancy][:option],
                  :start_date => params[:tenancy][:start_date],
                  :end_date => params[:tenancy][:end_date],
                  :fee => params[:tenancy][:fee],
                  :lease_signed => params[:tenancy][:lease_signed],
                  :office_note => params[:tenancy][:office_note],
                  :transaction_sales_person => params[:tenancy][:sales_person],
                  :external_agent_company => params[:tenancy][:external_agent_company],
                  :external_agent_sales_person => params[:tenancy][:external_agent_sales_person],
                  :conjunction_fee => params[:tenancy][:conjunction_fee],
                  :scale_of_fee => params[:tenancy][:scale_of_fee],
                  :reminder_alert => params[:tenancy][:reminder_alert],
                  :lease_expiry_sales_person => params[:tenancy][:lease_expiry_sales_person],
                  :contract_link => params[:tenancy][:contract_link]
                })
            end
            save_purchaser = @purchaser.save && @property.update_attributes!({:skip_validation => true, :leased_price => params[:tenancy][:total_rental_pa]})

            if params[:edit_selected_contact] == "true"
              @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number],:company => params[:purchaser_contact][:company]})
            end
            @purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?
          else
            save_purchaser = @purchaser.update_attributes({
                :agent_contact_id => params[:purchaser][:agent_contact_id],
                :rent => params[:tenancy][:rent],
                :net_gross => params[:tenancy][:net_gross],
                :outgoing => params[:tenancy][:outgoing],
                :total_rental_pa => params[:tenancy][:total_rental_pa],
                :total_rental_psm => params[:tenancy][:total_rental_psm],
                :lease_term => params[:tenancy][:lease_term],
                :option => params[:tenancy][:option],
                :start_date => params[:tenancy][:start_date],
                :end_date => params[:tenancy][:end_date],
                :fee => params[:tenancy][:fee],
                :lease_signed => params[:tenancy][:lease_signed],
                :office_note => params[:tenancy][:office_note],
                :transaction_sales_person => params[:tenancy][:sales_person],
                :external_agent_company => params[:tenancy][:external_agent_company],
                :external_agent_sales_person => params[:tenancy][:external_agent_sales_person],
                :conjunction_fee => params[:tenancy][:conjunction_fee],
                :scale_of_fee => params[:tenancy][:scale_of_fee],
                :reminder_alert => params[:tenancy][:reminder_alert],
                :lease_expiry_sales_person => params[:tenancy][:lease_expiry_sales_person],
                :contract_link => params[:tenancy][:contract_link],
                :is_notified => false
              }) && (@purchaser.property.update_attributes!({:skip_validation => true,:leased_price => params[:tenancy][:total_rental_pa]}) if @purchaser.is_valid? )
            if params[:edit_selected_contact] == "true"
              @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number],:company => params[:purchaser_contact][:company]})
            end
            @purchaser.agent_contact_id.nil? ? (@purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?) : (@purchaser_contact = @purchaser.agent_contact)
          end
          raise 'Failed to save Purchaser' unless save_purchaser

          if params[:add_new_contact] == "true" || !params[:purchaser][:agent_contact_id].blank?

            if new_purchaser || @purchaser.agent_contact_id.nil?
              if params[:purchaser][:agent_contact_id].blank?
                save_purchaser_contact = @purchaser_contact.save
                @purchaser.update_attribute(:agent_contact_id, @purchaser_contact.id) unless @purchaser_contact.blank?
                if params[:edit_selected_contact] == "true"
                  @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number],:company => params[:purchaser_contact][:company]})
                end
              else
                save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                if params[:edit_selected_contact] == "true"
                  @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number],:company => params[:purchaser_contact][:company]})
                end
                @purchaser_contact = @purchaser.agent_contact
              end
              create_note_for_contact = true
            else
              if previous_agent_contact_id.to_s == params[:purchaser][:agent_contact_id].to_s
                if params[:mark_for_deleted] == "true"
                  save_purchaser_contact = @purchaser.update_attributes!({:agent_contact_id => nil})
                else
                  save_purchaser_contact = @purchaser.agent_contact.update_attributes(params[:agent_contact])
                end
              else
                if params[:mark_for_deleted] == "true"
                  save_purchaser_contact = @purchaser.update_attributes!({:agent_contact_id => nil})
                else
                  save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                end
                create_note_for_contact = true
              end
              @purchaser_contact = @purchaser.agent_contact
            end

            raise 'Failed to save Purchaser contact' unless save_purchaser_contact

            if params[:add_new_contact] == "true"
              team = []
              team << @property.primary_contact_id unless @property.primary_contact_id.nil?
              team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
              @purchaser_contact.add_categories([category_id])
              @purchaser_contact.add_assigns(team)
              @purchaser_contact.add_access(team)
            end
          end
        end
        flash[:notice] = 'Tenancy record was successfully updated.'
        format.html { redirect_to :action => 'edit' }
      rescue
        @contact_name = params[:agent_name]
        @add_new_contact = params[:add_new_contact]
        @method_of_sales = [['Please select', nil]]
        @investments = [['Please select', nil],['Yes', true],['No', false]]
        @sales_person = AgentUser.find(:all, :conditions => "office_id = '#{@office.id}'").map{|x|[x.full_name.blank? ? x.first_name : x.full_name,x.id]}.unshift(['Please select', nil])
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @tenancy = TenancyRecord.find(params[:id])
    respond_to do |format|
      @tenancy.update_attributes({:is_deleted => true})
      flash[:notice] = "Tenancy record was successfully deleted"
      format.html {redirect_to agent_office_property_tenancies_path(@agent,@office,@property)}
    end
  end

  def populate_combobox
    floor,suburb,type,assigned_to,condition = [],[],[],[],[]
    condition = [" properties.office_id = ? "]
    condition << current_office.id
    if @property.type.to_s == "Commercial" && @property.is_have_tenancy? == true
      properties = TenancyRecord.find(:all,:include =>[:property, :agent_contact], :conditions => "tenancy_records.property_id = '#{@property.id}'")
    elsif @property.type.to_s == "CommercialBuilding"
      properties = TenancyRecord.find(:all,:include =>[:property, :agent_contact], :conditions => "properties.parent_listing_id = '#{@property.id}'")
    end
    properties.map{|x|
      suburb << {
        :suburb => x.property.suburb.to_s,
        :suburbText =>x.property.suburb.to_s
      } unless x.property.suburb.blank?;
      type << {
        :type => x.property.property_type.to_s,
        :typeText =>x.property.property_type.to_s
      } unless x.property.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.property.primary_contact.full_name.to_s unless x.property.primary_contact.blank?) ,
        :assigned_toText => (x.property.primary_contact.full_name.to_s unless x.property.primary_contact.blank?)
      } unless x.property.primary_contact.blank?
      ((floor << {
            :floor => x.property.detail.number_of_floors.to_s,
            :floorText => x.property.detail.number_of_floors.to_s
          } unless x.property.detail.number_of_floors.blank?) unless x.property.type == "ProjectSale" || x.property.type == "BusinessSale")
    }
    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    uniq_floor =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    floor.each{|x|uniq_floor << x if !uniq_floor.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_floor.to_a.sort!{|x,y| x[:floorText].to_s <=> y[:floorText].to_s}
    return(render(:json =>{:results => properties.length,:suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,:floors=>[uniq_floor.unshift({:floor=>'all',:floorText=>"All Floors"})].flatten,:types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,:team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        if @property.type.to_s == "Commercial"
          condition[0] += " AND tenancy_records.property_id = ?"
          condition << "#{@property.id}"
        elsif @property.type.to_s == "CommercialBuilding"
          condition[0] += " AND properties.parent_listing_id = ?"
          condition << "#{@property.id}"
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      if @property.type.to_s == "Commercial"
        condition[0] += " AND tenancy_records.property_id = ?"
        condition << "#{@property.id}"
      elsif @property.type.to_s == "CommercialBuilding"
        condition[0] += " AND properties.parent_listing_id = ?"
        condition << "#{@property.id}"
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = TenancyRecord.build_sorting(params[:sort],params[:dir])
    if params[:sort] == 'sqm'
      @properties = Property.sort_by_customize(params[:sort],params[:controller],params[:dir],params[:page],params[:limit],@agent.id,@office.id, @property.id)
    else
      @properties = TenancyRecord.paginate(:all, :include => [:property,:agent_contact], :conditions => condition, :order => order, :page => params[:page], :per_page => params[:limit].to_i)
    end

    if @properties.present?
      @properties.each do |x|
        check_and_update_icon(x.property)
      end
    end
    results = 0
    if params[:limit].to_i == 20
      results = TenancyRecord.find(:all,:select => 'count(tenancy_records.property_id)',:include =>[:property, :agent_contact],:conditions => condition,:limit => params[:limit].to_i)
    else
      results = TenancyRecord.count(:all, :include => [:property,:agent_contact], :conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'property_id' => x.property.id,
              'listing'=>Property.abbrv_properties(x.property.type),
              'address' =>((x.property.street_number.blank? ? "" : x.property.street_number)+" "+(x.property.street.blank? ? "" : x.property.street.to_s)),
              'suite'=>(x.property.unit_number.blank? ? "" : x.property.unit_number),
              'sqm'=>x.property.detail.floor_area.blank? ? "" : x.property.detail.floor_area,
              'level'=>((x.property.detail.number_of_floors.blank? ? "" : x.property.detail.number_of_floors) unless x.property.type == "ProjectSale" || x.property.type == "BusinessSale"),
              'I/E'=> x.property.ownership.blank? ? "Internal" : x.property.ownership,
              'tenancy'=>(x.agent_contact.blank? ? "" : (x.agent_contact.full_name.blank? ? x.agent_contact.first_name : x.agent_contact.full_name)),
              'rent_pa'=>(x.total_rental_pa.blank? ? "" : x.total_rental_pa),
              'start_date'=>(x.start_date.blank? ? "" : x.start_date),
              'end_date'=>(x.end_date.blank? ? "" : x.end_date),
              'suburb' => x.property.suburb.blank? ? "" : x.property.suburb,
              'type' => x.property.property_type.blank? ? "" : x.property.property_type,
              'price' => Property.check_price(x.property),
              'assigned_to'=>x.property.primary_contact.blank? ? '' : x.property.primary_contact.full_name.to_s.strip,
              'edit'=>[x.property.id, x.id],
              'status' => Property.status_name(x.property),
              'fb'=> [x.property.id,@office.url.blank? ? (x.property.detail.respond_to?(:deal_type) ? x.property.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.property.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.property.id}", tweet(x.property)]} unless x.property.blank? }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:floor] = params[:floor].blank? ? "all" : params[:floor]
    session[:sold_price] = params[:sold_price].blank? ? "all" : params[:sold_price]
    session[:sold_date] = params[:sold_date].blank? ? "all" : params[:sold_date]
    session[:purchaser] = params[:purchaser].blank? ? "all" : params[:purchaser]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
  end

  def nil_session_listing
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:floor] = nil
    session[:sold_price] = nil
    session[:purchaser] = nil
    session[:sold_date] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
  end

  def check_and_update_icon(children_listing)
    img_count = children_listing.images.count rescue 0
    floorp_count = children_listing.floorplans.count rescue 0
    if !children_listing.image_exist && img_count > 0
      children_listing.image_exist = true
      children_listing.save(false)
    end
    if children_listing.image_exist && img_count == 0
      children_listing.image_exist = false
      children_listing.save(false)
    end
    if !children_listing.floorplan_exist && floorp_count > 0
      children_listing.floorplan_exist = true
      children_listing.save(false)
    end
    if children_listing.floorplan_exist && floorp_count == 0
      children_listing.floorplan_exist = false
      children_listing.save(false)
    end
  end

  def tweet(children_listing)
    unless children_listing.blank?
      case children_listing.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      end
    end
    return tweet
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end

  def view_tenancy
    @property = Property.find(params[:property_id])
    @tenancy = TenancyRecord.find(params[:id])
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
    render :layout => 'iframe'
  end

end
