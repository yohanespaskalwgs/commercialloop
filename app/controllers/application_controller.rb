# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  # AuthenticatedSystem must be included for RoleRequirement, and is provided by installing acts_as_authenticates and running 'script/generate authenticated account user'.
  include AuthenticatedSystem
  # You can move this into a different controller, if you wish.  This module gives you the require_role helpers, and others.
  include RoleRequirementSystem

  include SslRequirement

  include ExceptionNotifiable

  class NotAuthorized < StandardError ; end

  helper :all # include all helpers, all the time
  helper_method :current_agent, :current_office, :current_developer

  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
  protect_from_forgery # :secret => 'cf004638d801507e81d04ce7b51f1f36'
  # filter_parameter_logging :password, :password_confirmation

  # See ActionController::Base for details
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password").
  # filter_parameter_logging :password

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  rescue_from ActionView::MissingTemplate, :with => :record_not_found
  rescue_from ActionController::MethodNotAllowed, :with => :not_allowed
  rescue_from ActionController::InvalidAuthenticityToken, :with => :deny_access
  rescue_from NotAuthorized, :with => :deny_access
  rescue_from ActiveRecord::RecordInvalid do |exception|
    render :action => (exception.record.new_record? ? :new : :edit)
  end

  model_folders = %w(attachment feature message property)
  for folder in model_folders do
    Dir["#{RAILS_ROOT}/app/models/#{folder}/*.rb"].each { |file|
      require_dependency "#{folder}/#{file[file.rindex('/') + 1...-3]}"
    }

  end

  before_filter  :set_p3p, :destroy_session_search_keyword, :destroy_session_agent_contact_id, :destroy_session_industry_category_id, :destroy_session_building_data, :destroy_session_price_list
  def set_p3p
    response.headers["P3P"]='CP="CAO PSA OUR"'
  end

  protected

  # Automatically respond with 404 for ActiveRecord::RecordNotFound
  def record_not_found
    render :file => File.join(RAILS_ROOT, 'public', '404.html'), :status => 404
  end

  # Automatically respond for ActionController::MethodNotAllowed
  def not_allowed
    render :file => File.join(RAILS_ROOT, 'public', '422.html'), :status => 422
  end

  def deny_access
    flash[:error] = "Access Denied"
    if logged_in?
      redirect_to find_user_home_path
    else
      render :file => File.join(RAILS_ROOT, 'public', '401.html'), :status => 401
    end
  end

  def render_to_pdf(options = nil)
    pdf = PDF::HTMLDoc.new
    pdf.set_option :bodycolor, :white
    pdf.set_option :bodyfont, :helvetica # arial helvetica sans serif
    pdf.set_option :footer, '.'
    pdf.set_option :header, '...'
    pdf.set_option :size, :universal
    pdf.set_option :toc, false
    pdf.set_option :portrait, true
    pdf.set_option :links, false
    pdf.set_option :webpage, true
    pdf.set_option :left, '1cm'
    pdf.set_option :right, '1cm'
    pdf.set_option :bottom, '1cm'
    pdf << render_to_string(options)
    pdf.generate
  end

  def destroy_session_building_data
    if controller_name != "residential_building_reports"
      unless session[:building_data].blank?
        session[:building_data] = nil
      end
    end
  end

  def destroy_session_price_list
    if controller_name != "residential_building_reports"
      unless session[:price_list].blank?
        session[:price_list] = nil
      end
    end
  end

  def destroy_session_search_keyword
    if controller_name != "search_results"
      if controller_name != "media" && controller_name != "opentimes" && controller_name != "properties"
        unless session[:search_keyword].blank?
          session[:search_keyword] = nil
        end
      end
    end
  end

  def destroy_session_agent_contact_id
    if controller_name != 'match_contacts'
      unless session[:agent_contact_id].blank?
        session[:agent_contact_id] = nil
      end
    end
  end

  def destroy_session_industry_category_id
    if controller_name != 'agent_contacts'
      unless session[:industry_category_id].blank?
        session[:industry_category_id] = nil
      end
    end
  end

  def get_agent_and_office
    @agent = Agent.find params[:agent_id]
    @office = @agent.offices.find params[:office_id]
    @property = @agent.properties.find params[:property_id]
    GLOBAL_ATTR["Host-#{@office.id}"] = request.host
  end

  def get_property
    @property = Property.find(params[:property_id])
  end

  def get_sub_domain
    subdomain = self.request.subdomains[0]
    domain = self.request.domain
    if domain == "com.au"
      if subdomain == "commercialloopcrm"
        ""
      else
        subdomain
      end
    else
      subdomain
    end

  end

  def check_sub_domain
    self.request.subdomains
  end

  def current_agent
    @agent
  end

  def current_office
    if (@office.nil? || @office.new_record?)
      unless current_user.blank?
        user = User.find(current_user.id)
        if user.type == "DeveloperUser"
          office = @agent.offices.find_by_id(session[:office_id]) unless session[:office_id].blank?
          @agent.offices.find(:first) if office.blank?
        else
          if current_user.attribute_present?(:office)
            current_user.office
          else
            @agent.offices.find(:first)
          end
        end
      end
    else
      @office
    end
  end

  def current_property
    if (@property.nil? || @property.new_record?)
      unless current_user.blank?
        user = User.find(current_user.id)
        if user.type == "DeveloperUser"
          property = @agent.properties.find_by_id(session[:property_id]) unless session[:property_id].blank?
          @agent.offices.find(:first) if office.blank?
        else
          if current_user.attribute_present?(:property)
            current_user.property
          else
            @agent.properties.find(:first)
          end
        end
      end
    else
      @property
    end
  end

  def current_developer
    @developer
  end

  def current_agent=(_agent)
    @agent = _agent
  end

  def current_office=(_office)
    @office = _office
  end

  def current_developer=(_developer)
    @developer = _developer
  end

  def full_base_url
    if !self.request.subdomains[0].blank?
      url = "http://#{self.request.subdomains[0]}.commercialloopcrm.com.au"
    else
      url = 'http://www.commercialloopcrm.com.au'
    end
    return url
  end

  def check_permission_page
    check_permission_func(params[:property_id]) unless params[:property_id].blank?
  end

  def check_permission_page_property
    check_permission_func(params[:id]) unless params[:id].blank?
  end

  def check_permission_func(property_id)
    if current_user.is_a?(AgentUser)
      if current_user.full_access?
        arr_off = []
        property = Property.find(property_id)
        property.agent.offices.each{|off| arr_off << off.id }
        render_optional_error_file(401) unless arr_off.include?(current_user.office.id)
      else
        if current_office.id != current_user.office.id
          render_optional_error_file(401)
        else
          property = Property.find(property_id)
          render_optional_error_file(401) if property.office_id != current_office.id
        end
      end
    end
  end

  def check_export_permission
    if @office.limit_access && !current_user.full_access?
      render_optional_error_file(401)
    end
  end

  def get_header_footer(office_id, type)
    footer = Attachment.find_by_sql("SELECT `attachments`.* FROM `attachments` INNER JOIN `users` ON `attachments`.attachable_id = `users`.id AND  (`attachments`.attachable_type = 'User' or `attachments`.attachable_type = 'AgentUser') WHERE (((users.office_id = #{office_id}) AND (users.deleted_at IS NULL OR users.deleted_at > now()) AND (`users`.`type` = 'AgentUser')) OR (parent_id = #{office_id})) AND (`attachments`.`type` = 'UserFile' AND `attachments`.`category` = '#{type}') LIMIT 0 , 1")
    return footer[0]
  end

  private

  def find_user_home_path
    case current_user
    when AdminUser
      admin_path
    when AgentUser
      agent_office_path(current_user.agent, current_user.office)
    when DeveloperUser
      developer_path(current_user.developer)
    else
      root_path
    end
  end

  def require_role_gte(klass)
    return access_denied unless current_user.role_gte? klass
    true
  end

  def check_agent_ownership
    current_agent.accessible_by? current_user
  end

  def check_office_ownership
    current_office.accessible_by? current_user
  end

  def check_developer_ownership
    current_developer.accessible_by? current_user
  end

end
