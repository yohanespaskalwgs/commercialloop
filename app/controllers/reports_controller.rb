class ReportsController < ApplicationController
  layout 'agent'
  before_filter :login_required , :except =>[:export_to_csv, :populate_property_type]
  before_filter :get_agent_and_office
  skip_before_filter :verify_authenticity_token
  FIELDS = ["property_id", "address","suburb", "portals","state", "primary_agent", "floor_area", "eer", "office_name", "agent_names", "headline", "created_at"]

  def index
    session[:rp_fields] = session[:rp_listing_type] = session[:rp_status] = session[:rp_property_type] = session[:rp_data] = ""
    redirect_to :action => "choose_properties"
  end

  def choose_properties
    populate_data
    arr_stat = [['available',1], ['sold',2], ['leased',3], ['withdrawn',4], ["underoffer",5], ["draft",6]]
    @statuss = arr_stat.map{|m, n| [m,n]}.unshift(['Please select',nil])
    @listings = ['ResidentialSale', 'ResidentialLease', 'HolidayLease', 'Commercial', 'CommercialBuilding','ProjectSale', 'BussinessSale', 'NewDevelopment', 'LandRelease'].map{|m| [m,m]}.unshift(['Please select',nil])
    ptypes = Property.find_by_sql("SELECT DISTINCT(`property_type`) FROM `properties` WHERE office_id = #{@office.id} And `property_type` IS NOT NULL ORDER BY `property_type` ASC")
    @property_types = ptypes.map{|m| [m.property_type, m.property_type] }.delete_if{|x, y| x.blank?}.unshift(['Please select',nil])
    @contacts = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq

    @value_listing_type = @value_listing_types = ""
    @listing_type.each{|listing|
      @value_listing_type = @value_listing_type + '~' + listing
      @value_listing_types = @value_listing_types + '<span id="listing_type'+listing+'" style="margin-right:20px;">'+listing+'<a href="javascript:remove_options(\'listing_type\',\''+listing+'\')">x</a></span>' unless listing == "All"
    } unless @listing_type.blank?

    @value_property_type = @value_property_types = ""
    @property_type.each{|property_type|
      @value_property_type = @value_property_type + '~' + property_type
      @value_property_types = @value_property_types + '<span id="property_type'+property_type+'" style="margin-right:20px;">'+property_type+'<a href="javascript:remove_options(\'property_type\',\''+property_type+'\')">x</a></span>' unless property_type == "All"
    } unless @property_type.blank?

    @value_status = @value_statuss = ""
    @status.each{|status|
      @value_status = @value_status + '~' + status
      @value_statuss = @value_statuss + '<span id="status'+status+'" style="margin-right:20px;">'+arr_stat[(status.to_i-1)][0]+'<a href="javascript:remove_options(\'status\',\''+status+'\')">x</a></span>' unless status == "All"
    } unless @status.blank?
  end

  def choose_data
    unless params[:choose_properties].blank?
      session[:rp_listing_type] =  params[:listing_type]
      session[:rp_status] =  params[:status]
      session[:rp_property_type] =  params[:property_type]
      session[:rp_data] = { :primary_contact => params[:primary_contact], :secondary_contact => params[:secondary_contact], :third_contact => params[:third_contact], :fourth_contact => params[:fourth_contact], :sort => params[:sort], :min_price => params[:min_price], :max_price => params[:max_price], :min_floor => params[:min_floor], :max_floor => params[:max_floor], :min_bed => params[:min_bed], :max_bed => params[:max_bed]}
    end
    @fields = FIELDS.map{|m| [m,m]}.unshift(['Please select',nil])
  end

  def preview
    populate_properties
    respond_to do |format|
      format.html { render :layout => 'iframe' }
    end
  end

  def export_to_csv
    populate_properties
    csv_string = FasterCSV.generate do |csv|
      # header row
      header = []
      header << "Property ID" if @criteria_fields.include?("property_id")
      header << "Adress" if @criteria_fields.include?("address")
      header << "Suburbs" if @criteria_fields.include?("suburb")
      header << "Portals" if @criteria_fields.include?("portals")
      header << "State" if @criteria_fields.include?("state")
      header << "Primary Agent" if @criteria_fields.include?("primary_agent")
      header << "Floor Area" if @criteria_fields.include?("floor_area")
      header << "EER" if @criteria_fields.include?("eer")
      header << "Office Name" if @criteria_fields.include?("office_name")
      header << "Agent Names" if @criteria_fields.include?("agent_names")
      header << "Headline" if @criteria_fields.include?("headline")
      header << "Created at" if @criteria_fields.include?("created_at")
      csv << header
      @properties.each do |property|
        detail = []
        p = Property.find_by_id(property["id"])
        address = "#{property["unit_number"]} #{property["street_number"]} #{property["street"]}"
        portals = floor = eer = ""
        detail << property["id"] if @criteria_fields.include?("property_id")
        detail << address if @criteria_fields.include?("address")
        detail << property["suburb"] if @criteria_fields.include?("suburb")
        detail << property["headline"] if @criteria_fields.include?("headline")
        detail << property["created_at"] if @criteria_fields.include?("created_at")

        if @criteria_fields.include?("portals")
          pro_e = PropertyPortalExport.find(:all, :include=>[:portal], :conditions=>"`property_id` =#{property["id"]}", :order => "portals.portal_name ASC")
          pro_e.each {|pe| portals += "#{pe.portal.portal_name};"} unless pro_e.blank?
          detail << portals
        end
        detail << property["state"] if @criteria_fields.include?("state")
        detail << property["primary_agent"] if @criteria_fields.include?("primary_agent")
        if @criteria_fields.include?("floor_area")
          floor = p.detail.floor_area if p.detail.respond_to?(:floor_area)
          detail << floor
        end
        if @criteria_fields.include?("eer")
          eer = p.detail.energy_efficiency_rating if p.detail.respond_to?(:energy_efficiency_rating)
          detail << eer
        end

        detail << p.office.name if @criteria_fields.include?("office_name")
        if @criteria_fields.include?("agent_names")
          agent_names = ""
          unless p.blank?
            agent_names += p.primary_contact.full_name unless p.primary_contact.blank?
            agent_names += "##{p.secondary_contact.full_name}" unless p.secondary_contact.blank?
            agent_names += "##{p.third_contact.full_name}" unless p.third_contact.blank?
            agent_names += "##{p.fourth_contact.full_name}" unless p.fourth_contact.blank?
          end
          detail << agent_names
        end
        csv << detail
      end
    end
    # send it to the browser
    title = "portal_summary_report_for_#{@office.name.gsub(" ", "_")}-#{Time.now.strftime("%d-%m-%Y")}"
    send_data csv_string, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=#{title}.csv"
  end

  def populate_data
    session[:rp_fields] = params[:field] unless params[:field].blank?
    @criteria_fields =  session[:rp_fields].blank? ? "" : session[:rp_fields].split("~").delete_if{|x| x.blank?}

    #unless session[:rp_listing_type].blank?
      @listing_type =  session[:rp_listing_type].blank? ? "" : session[:rp_listing_type].split("~").delete_if{|x| x.blank?}
      @status =  session[:rp_status].blank? ? "" : session[:rp_status].split("~").delete_if{|x| x.blank?}
      @property_type =  session[:rp_property_type].blank? ? "" : session[:rp_property_type].split("~").delete_if{|x| x.blank?}
      unless session[:rp_data].blank?
        @primary_contact = session[:rp_data][:primary_contact]
        @secondary_contact = session[:rp_data][:secondary_contact]
        @third_contact = session[:rp_data][:third_contact]
        @fourth_contact = session[:rp_data][:fourth_contact]
        @order_by = session[:rp_data][:sort]
        @min_price = session[:rp_data][:min_price]
        @max_price = session[:rp_data][:max_price]
        @min_floor = session[:rp_data][:min_floor]
        @max_floor = session[:rp_data][:max_floor]
        @min_bed = session[:rp_data][:min_bed]
        @max_bed = session[:rp_data][:max_bed]
      end
    #end
  end

  def populate_properties
    populate_data
    order_by = @order_by.blank? ? "`properties`.updated_at DESC" : "`properties`.#{@order_by} ASC"
#    if @agent.developer_id == 186
#      dev = Developer.find(186)
#      ids = ""
#      ind = 0
#      dev.agents.each do |ag|
#        ag.offices.each do |off|
#          if ind == 0
#            ids += "#{off.id}"
#          else
#            ids += ",#{off.id}"
#          end
#          ind = ind + 1
#        end
#      end
#      conditions = " `properties`.office_id IN (#{ids}) "
#    else
#      conditions = " `properties`.office_id = #{@office.id} "
#    end
    conditions = " `properties`.office_id = #{@office.id} "
    conditions += " AND `properties`.created_at > '2011-09-1'" if @agent.developer_id == 186
    conditions += " AND `properties`.created_at >= '2011-07-1'" if @office.id == 1071

    sql = [" SELECT `properties`.id, `properties`.headline, `properties`.created_at, `properties`.unit_number, `properties`.street_number, `properties`.street, `properties`.suburb, `properties`.state, `properties`.state, CONCAT(IFNULL(`users`.first_name,''), ' ', IFNULL(`users`.last_name,'')) as primary_agent FROM properties "]
    sql[0] += " LEFT JOIN `users` ON (`users`.`id` = `properties`.`primary_contact_id` ) LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  ) "
    conditions += " AND `properties`.type IN ('#{@listing_type.join("','")}')" unless @listing_type.blank?
    conditions += " AND `properties`.status IN ('#{@status.join("','")}')" unless @status.blank?
    conditions += " AND `properties`.property_type IN ('#{@property_type.join("','")}')" unless @property_type.blank?
    conditions += " AND `properties`.primary_contact_id = #{@primary_contact}" unless @primary_contact.blank?
    conditions += " AND `properties`.secondary_contact_id = #{@secondary_contact}" unless @secondary_contact.blank?
    conditions += " AND `properties`.third_contact_id = #{@third_contact}" unless @third_contact.blank?
    conditions += " AND (`properties`.fourth_contact_id = #{@fourth_contact} And `properties`.fourth_contact_type = 'Normal')" unless @fourth_contact.blank?

    if !@min_price.blank? and !@max_price.blank?
      if @max_price.to_i > @min_price.to_i
        conditions += " AND (`properties`.price BETWEEN #{@min_price} AND #{@max_price})"
      end
    end

    if !@min_floor.blank? && !@max_floor.blank?
      if @listing_type.blank? || @listing_type.include?("ResidentialSale") || @listing_type.include?("Commercial") || @listing_type.include?("BussinessSale") || @listing_type.include?("ResidentialLease") || @listing_type.include?("ProjectSale")
        conditions += "And ((commercial_details.floor_area >= #{@min_floor} And commercial_details.floor_area <= #{@max_floor})"
        conditions += " OR (residential_sale_details.floor_area >= #{@min_floor} And residential_sale_details.floor_area <= #{@max_floor})"
        conditions += " OR (residential_lease_details.floor_area >= #{@min_floor} And residential_lease_details.floor_area <= #{@max_floor})"
        conditions += " OR (project_sale_details.floor_area >= #{@min_floor} And project_sale_details.floor_area <= #{@max_floor})"
        conditions += " OR (business_sale_details.floor_area >= #{@min_floor} And business_sale_details.floor_area <= #{@max_floor}))"
      end
    end

    if !@min_bed.blank? && !@max_bed.blank?
      if @listing_type.blank? || @listing_type.include?("ResidentialSale") || @listing_type.include?("ResidentialLease") || @listing_type.include?("ProjectSale") || @listing_type.include?("HolidayLease")
        conditions += " And ((residential_sale_details.bedrooms >= #{@min_bed} And residential_sale_details.bedrooms <= #{@max_bed})"
        conditions += " OR (residential_lease_details.bedrooms >= #{@min_bed} And residential_lease_details.bedrooms <= #{@max_bed})"
        conditions += " OR (project_sale_details.bedrooms >= #{@min_bed} And project_sale_details.bedrooms <= #{@max_bed})"
        conditions += " OR (holiday_lease_details.bedrooms >= #{@min_bed} And holiday_lease_details.bedrooms <= #{@max_bed}))"
      end
    end

    sql_part1 = [" WHERE ( #{conditions} ) "]
    sql_part2 = [" AND ( properties.deleted_at IS NULL ) "]
    sql_part3 = [" ORDER BY #{order_by}"]
    sql[0] += sql_part1.to_s
    sql[0] += sql_part2.to_s
    sql[0] += sql_part3.to_s
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    @properties = @mysql_result.all_hashes
    @mysql_result.free
  end

  def populate_property_type
    lt_conditions = ""
    listing_type = params[:listings].split("~").delete_if{|x| x.blank?} unless params[:listings].blank?
    unless listing_type.blank?
      listing_type.each_with_index{|list,index|
        lt_conditions += "," if index > 0
        lt_conditions += "'#{list}'"
      }
      listing_conditions = listing_type.include?("All") ? "" : "And (properties.type IN (#{lt_conditions}))"
    end
    ptypes = Property.find_by_sql("SELECT DISTINCT(`property_type`) FROM `properties` WHERE office_id = #{@office.id} And `property_type` IS NOT NULL #{listing_conditions} ORDER BY `property_type` ASC")
    @property_types = ptypes.map{|m| [m.property_type, m.property_type] }.delete_if{|x, y| x.blank?}
    return(render(:json => @property_types))
  end
end
