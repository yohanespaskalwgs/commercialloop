class UserFileCategoriesController < ApplicationController
  layout 'agent'

  before_filter :login_required
  before_filter :get_agent_and_office
  before_filter :check_office_ownership

  # GET /user_file_categories
  # GET /user_file_categories.xml
  def index
    @user_file_categories = @office.user_file_categories

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @user_file_categories }
    end
  end

  # GET /user_file_categories/new
  # GET /user_file_categories/new.xml
  def new
    @user_file_category = UserFileCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user_file_category }
    end
  end

  # GET /user_file_categories/1/edit
  def edit
    @user_file_category = UserFileCategory.find(params[:id])
  end

  # POST /user_file_categories
  # POST /user_file_categories.xml
  def create
    @user_file_category = @office.user_file_categories.build(params[:user_file_category])

    respond_to do |format|
      if @user_file_category.save
        flash[:notice] = 'UserFileCategory was successfully created.'
        format.html { redirect_to :action => 'index' }
        format.xml  { render :xml => @user_file_category, :status => :created, :location => @user_file_category }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user_file_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /user_file_categories/1
  # PUT /user_file_categories/1.xml
  def update
    @user_file_category = @office.user_file_categories.find(params[:id])

    respond_to do |format|
      if @user_file_category.update_attributes(params[:user_file_category])
        flash[:notice] = 'UserFileCategory was successfully updated.'
        format.html { redirect_to :action => 'index'}
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user_file_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /user_file_categories/1
  # DELETE /user_file_categories/1.xml
  def destroy
    @user_file_category = @office.user_file_categories.find(params[:id])
    @user_file_category.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { head :ok }
    end
  end
end
