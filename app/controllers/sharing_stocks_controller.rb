class SharingStocksController < ApplicationController
  layout "agent"

  before_filter :get_agent_and_office
  before_filter :login_required
  before_filter :require_accepted, :only => [:index]

  def index
  end

  def update
    @agent_user = @office.agent_users.find(current_user.id)
    respond_to do |format|
      if @agent_user.update_attributes(params[:agent_user])
      flash[:notice] = "Agent users was successfuly updated"
      format.html {redirect_to agent_office_sharing_stocks_path(@agent,current_office) }
      end
    end
  end

  private

  def require_accepted
    unless show_agent_user?
      flash[:notice] = "You don't have permission to change sharing stock configuration for this agent user, please sign in with another agent user account"
      redirect_to "/agents/#{@agent.id}/offices/#{@office.id}"
    end
  end

  def display_agent_user
    @agent_user = AgentUser.find(:first, :conditions => "id = '#{current_user.id}' AND office_id = '#{@office.id}'")
  end

  def show_agent_user?
    !!display_agent_user
  end

end
