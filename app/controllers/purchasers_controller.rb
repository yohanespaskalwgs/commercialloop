class PurchasersController < ApplicationController
  layout false
  skip_before_filter :verify_authenticity_token, :only => [:auto_complete_for_offer_name]
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  before_filter :prepare_purchaser

  def index
    @purchaser = Purchaser.new
  end

  def show
  end

  def new
  end

  def edit

  end

  def create
    create_contact
    params[:purchaser] = params[:purchaser].merge(:property_id => @property.id)
    @purchaser = Purchaser.new(params[:purchaser])

    respond_to do |format|
      if @purchaser.save
        create_notes
        format.html { redirect_to :action => 'index' }
      else
        session[:close_popup_purchaser] = false
        format.html { render :action => "index" }
        format.xml  { render :xml => @purchaser.errors}
      end
    end
  end

  def create_contact
    category_id = (@property.type == 'ResidentialLease')? 11 : 8

    unless params[:purchaser][:agent_contact_id].blank?
      agent_contact= AgentContact.find_by_id(params[:purchaser][:agent_contact_id])
      agent_contact.update_category_contact(category_id) unless agent_contact.blank?
      params[:purchaser] = params[:purchaser].merge(:duplicate => 0)
    else
      team = []
      team << @property.primary_contact_id unless @property.primary_contact_id.nil?
      team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?

      contact = contact_fields(category_id)

      @agent_contact = AgentContact.new(contact)
      if @agent_contact.save
        @agent_contact.add_categories([category_id])
        @agent_contact.add_assigns(team)
        @agent_contact.add_access(team)
        @agent_contact.use_spawn_for_boom
        @agent_contact.use_spawn_for_md
        @agent_contact.use_spawn_for_irealty
        params[:purchaser] = params[:purchaser].merge(:agent_contact_id => @agent_contact.id, :duplicate => 0)
      else
        duplicate = @agent_contact.errors.on :check_error
        if duplicate == "duplicate"
          params[:purchaser] = params[:purchaser].merge(:agent_contact_id => 1)
        else
          params[:purchaser] = params[:purchaser].merge(:duplicate => 0)
        end
        @agent_contact
      end
    end
  end

  def update_duplicate
    category_id = (@property.type == 'ResidentialLease')? 11 : 8

    duplicate = 0
    detected = nil
    contact = contact_fields(category_id)

    %w(email mobile_number contact_number work_number home_number).each{|w|
      unless params[:purchaser][w].blank?
        check = AgentContact.check_exist(params[:purchaser], w)
        unless check.nil?
          detected = check
          duplicate = 1
        end
      end
    }

    if duplicate == 1
      unless detected.nil?
        @agent_contact = AgentContact.find(detected.id)
          if @agent_contact.update_attributes(contact.delete_if{|x,y| y.blank?})
            @agent_contact.update_category_contact(category_id)
            params[:purchaser] = params[:purchaser].merge(:property_id => @property.id, :agent_contact_id => @agent_contact.id, :duplicate => 0)
            @purchaser = Purchaser.new(params[:purchaser])
            create_notes if @purchaser.save
          end
      end
    end
    redirect_to agent_office_property_purchasers_path(@agent,@office, @property)
  end

  def update

  end

  def destroy
    @purchaser = Purchaser.find(params[:id])
    @purchaser.destroy
    redirect_to status_agent_office_property_path(@agent,@office,@property)
  end

  def prepare_purchaser
    @property = Property.find(params[:property_id])
  end

  def auto_complete_for_offer_name
   contacts = AgentContact.search_for_autocomplete(params[:offer][:name], @agent.id, @office.id)
   render :partial => 'offers/contact_list', :locals => { :contacts => contacts}
  end

  def create_notes
    description = "Date = "+params[:purchaser][:date] +", Price = "+params[:purchaser][:amount] +', Display Price = '+ (params[:purchaser][:display_price] == '1' ? "Yes": "No")
    @agent_contact= AgentContact.find_by_id(@purchaser.agent_contact_id)
    unless @agent_contact.blank?
      #note type = Purchase Property
      @agent_contact.create_note(@property.id, description, 8, current_user.id)

      if (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
        note_type_id = 8  #purchase property
      else
        note_type_id = 9  #lease property
      end
      #note type for property
      @agent_contact.create_note_for_property(@property.id, description, note_type_id, "", current_user.id)
    end

    @purchaser.update_attributes({:contact_note_id => @purchaser.agent_contact_id})
    @property.update_attribute(:sold_price,params[:purchaser][:amount])
    @property.update_attribute(:show_price,params[:purchaser][:display_price])
    @property.update_attribute(:sold_on, params[:purchaser][:date])

    session[:close_popup_purchaser] = true
    flash[:notice] = 'Purchaser Property was successfully created.'
  end

  def contact_fields(category_id)
    {:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id],
      :first_name => params[:purchaser][:first_name], :last_name => params[:purchaser][:last_name],
      :email => params[:purchaser][:email], :home_number => params[:purchaser][:home_number],
      :work_number => params[:purchaser][:work_number], :mobile_number => params[:purchaser][:mobile_number],
      :contact_number => params[:purchaser][:contact_number] }
  end
end
