class StockListsController < ApplicationController
  before_filter :login_required, :except =>[:generate_pdf]
  before_filter :get_agent_and_office
  skip_before_filter :verify_authenticity_token, :except => [:index, :list, :destroy]
  layout "agent"

  def index
    redirect_to :action => "list"
  end

  def list
    unless params[:delete_btn].blank?
      redirect_to choose_properties_agent_office_stock_lists_path(@agent, @office)
    end
    @stock_lists = @office.stocklist.find(:all, :order => "`created_at` DESC")
    ["stocklist_property_checkbox","stock_layout", "stock_opentimes", "stock_auctions", "stock_first_level", "stock_second_level", "stock_opentime_type", "stock_opentime_startdate","stock_list_name", "stock_list_color", "stock_status", "stock_period", "stock_listing_type", "stock_suburb", "stock_property_type", "stock_sale_lease", "stock_primary_contact", "stock_min_price", "stock_max_price", "stock_min_rent", "stock_max_rent", "stock_min_size", "stock_max_size", "stock_building_min_size", "stock_building_max_size", "stock_display_bed", "stock_display_car", "stock_display_bath", "stock_display_bond", "stock_display_contact", "stock_display_price", "stock_display_property_id", "stock_display_internet_id", "stock_display_available", "stock_display_auction", "stock_display_land", "stock_display_headline", "stock_display_desc", "stock_display_suburb", "stock_display_address", "stock_display_fullsize", "black_text"].each{|x|session[x.to_sym] = nil }
  end

  def multiple_delete
    unless params[:stocklist_ids].blank?
     params[:stocklist_ids].each do |id|
       pdf  = Attachment.find_by_id(id)
       pdf.destroy unless pdf.blank?
     end
     flash[:notice] = "Stocklists already deleted."
    end
    redirect_to list_agent_office_stock_lists_path(@agent, @office)
  end

  def choose_layout
    unless session[:stock_layout].blank?
      @stock_layout = session[:stock_layout]
    end
    if params[:layout]
      session[:stock_layout] = params[:layout]
      redirect_to choose_properties_agent_office_stock_lists_path(@agent, @office)
    end
  end

  def choose_properties
    unless session[:stock_listing_type].blank?
      @opentime = session[:stock_opentimes]
      @auction = session[:stock_auctions]
      @status = session[:stock_status]
      @period = session[:stock_period]
      @min_price = session[:stock_min_price]
      @max_price = session[:stock_max_price]
      @min_rent = session[:stock_min_rent]
      @max_rent = session[:stock_max_rent]
      @min_size = session[:stock_min_size]
      @max_size = session[:stock_max_size]
      @building_min_size = session[:stock_building_min_size]
      @building_max_size = session[:stock_building_max_size]
      @baths = session[:stock_baths]
      @beds = session[:stock_beds]
      @cars = session[:stock_cars]
      @property_checkbox_ids = session[:stocklist_property_checkbox].join(",") unless session[:stocklist_property_checkbox].blank?

      @value_listing_type = @value_listing_types = ""
      session[:stock_listing_type].each{|listing|
        @value_listing_type = @value_listing_type + '#' + listing
        @value_listing_types = @value_listing_types + '<span id="'+listing+'" style="margin-right:20px;">'+listing+'<a href="javascript:remove_options(\'listing_type\',\''+listing+'\')">x</a></span>' unless listing == "All"
      } unless session[:stock_listing_type].blank?

      @value_sale_lease = @value_sale_leases = ""
      session[:stock_sale_lease].each{|sale_lease|
        @value_sale_lease = @value_sale_lease + '#' + sale_lease
        @value_sale_leases = @value_sale_leases + '<span id="'+sale_lease+'" style="margin-right:20px;">'+sale_lease+'<a href="javascript:remove_options(\'sale_lease\',\''+sale_lease+'\')">x</a></span>' unless sale_lease == "All"
      } unless session[:stock_sale_lease].blank?

      @value_property_type = @value_property_types = ""
      session[:stock_property_type].each{|property_type|
        @value_property_type = @value_property_type + '#' + property_type
        @value_property_types = @value_property_types + '<span id="'+property_type+'" style="margin-right:20px;">'+property_type+'<a href="javascript:remove_options(\'property_type\',\''+property_type+'\')">x</a></span>' unless property_type == "All"
      } unless session[:stock_property_type].blank?

      @value_suburb = @value_suburbs = ""
      session[:stock_suburb].each{|suburb|
        @value_suburb = @value_suburb + '#' + suburb
        @value_suburbs = @value_suburbs + '<span id="'+suburb+'" style="margin-right:20px;">'+suburb+'<a href="javascript:remove_options(\'suburb\',\''+suburb+'\')">x</a></span>' unless suburb == "All"
      } unless session[:stock_property_type].blank?

      @value_primary_contact = @value_primary_contacts = ""
      session[:stock_primary_contact].each{|primary_contact|
        @value_primary_contact = @value_primary_contact + '#' + primary_contact
        @value_primary_contacts = @value_primary_contacts + '<span id="'+primary_contact+'" style="margin-right:20px;">'+primary_contact+'<a href="javascript:remove_options(\'primary_contact\',\''+primary_contact+'\')">x</a></span>' unless primary_contact == "All"
      } unless session[:stock_property_type].blank?
    end
    if params[:listing_type]
      init_data
      session[:stocklist_property_checkbox] = params[:property_checkbox]
      redirect_to choose_data_agent_office_stock_lists_path(@agent, @office)
    end
    prepare_data
  end

  def choose_data
    @first_level = session[:stock_first_level] unless session[:stock_first_level].blank?
    @second_level = session[:stock_second_level] unless session[:stock_second_level].blank?
    @opentime_type = session[:stock_opentime_type] unless session[:stock_opentime_type].blank?
    @opentime_startdate= session[:stock_opentime_startdate] unless session[:stock_opentime_startdate].blank?
    @display_bed = session[:stock_display_bed] unless session[:stock_display_bed].blank?
    @display_bath = session[:stock_display_bath] unless session[:stock_display_bath].blank?
    @display_car = session[:stock_display_car] unless session[:stock_display_car].blank?
    @display_bond = session[:stock_display_bond] unless session[:stock_display_bond].blank?
    @display_contact = session[:stock_display_contact] unless session[:stock_display_contact].blank?
    @display_price = session[:stock_display_price] unless session[:stock_display_price].blank?
    @display_property_id = session[:stock_display_property_id] unless session[:stock_display_property_id].blank?
    @display_internet_id = session[:stock_display_internet_id] unless session[:stock_display_internet_id].blank?
    @display_available = session[:stock_display_available] unless session[:stock_display_available].blank?
    @display_auction = session[:stock_display_auction] unless session[:stock_display_auction].blank?
    @display_land = session[:stock_display_land] unless session[:stock_display_land].blank?
    @display_headline = session[:stock_display_headline] unless session[:stock_display_headline].blank?
    @display_desc = session[:stock_display_desc] unless session[:stock_display_desc].blank?
    @display_suburb = session[:stock_display_suburb] unless session[:stock_display_suburb].blank?
    @display_address = session[:stock_display_address] unless session[:stock_display_address].blank?
    @display_fullsize = session[:stock_display_fullsize] unless session[:stock_display_fullsize].blank?


    if params[:choose_data] == "update"
      session[:stock_first_level] = params[:first_level]
      session[:stock_second_level] = params[:second_level]
      session[:stock_opentime_type] = params[:opentime_type]
      session[:stock_opentime_startdate] = params[:opentime_startdate]
      session[:stock_display_bed] = params[:display_bed].blank? ? "false" : "true"
      session[:stock_display_bath] = params[:display_bath].blank? ? "false" : "true"
      session[:stock_display_car] = params[:display_car].blank? ? "false" : "true"
      session[:stock_display_bond] = params[:display_bond].blank? ? "false" : "true"
      session[:stock_display_contact] = params[:display_contact].blank? ? "false" : "true"
      session[:stock_display_price] = params[:display_price].blank? ? "false" : "true"
      session[:stock_display_property_id] = params[:display_property_id].blank? ? "false" : "true"
      session[:stock_display_internet_id] = params[:display_internet_id].blank? ? "false" : "true"
      session[:stock_display_available] = params[:display_available].blank? ? "false" : "true"
      session[:stock_display_auction] = params[:display_auction].blank? ? "false" : "true"
      session[:stock_display_land] = params[:display_land].blank? ? "false" : "true"
      session[:stock_display_headline] = params[:display_headline].blank? ? "false" : "true"
      session[:stock_display_desc] = params[:display_desc].blank? ? "false" : "true"
      session[:stock_display_suburb] = params[:display_suburb].blank? ? "false" : "true"
      session[:stock_display_address] = params[:display_address].blank? ? "false" : "true"
      session[:stock_display_fullsize] = params[:display_fullsize].blank? ? "false" : "true"
      redirect_to choose_colour_agent_office_stock_lists_path(@agent, @office)
    end
  end

  def choose_colour
#    params.merge!({"brochure_header" => "1"})
#    params[:borchure][:header] = "header1"
    @header_image = find_header("Brochure Header")
    @header_image2 = find_header("Brochure Header2")
    @footer_image = find_header("Brochure Footer")
    @color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #999999 #CCCCCC #f0f0f0 #FFFFFF)
    unless session[:stock_list_name].blank?
      @list_name = session[:stock_list_name]
      @list_color = session[:stock_list_color]
    end
    @black_text = session[:black_text]
    session[:black_text] = params[:black_text]
    if params[:list_name]
      session[:stock_list_name] = params[:list_name]
      session[:stock_list_color] = params[:list_color]
      redirect_to preview_agent_office_stock_lists_path(@agent, @office)
    end
  end

  def preview
    @count = 1
    @preview = true
    redirect_to generate_pdf_agent_office_stock_lists_path(@agent, @office) if params[:generate]
    pdf_data(true)
  end

  def generate_pdf
    @count = 1
    if params[:pdf_id]
      pdf_file  = Attachment.find_by_id(params[:pdf_id]) unless params[:pdf_id].blank?
      @html = pdf_file.pdf_layout
      render :layout => false
    else
      pdf_data(true)
      html = render_to_string(:layout => false)
      title = session[:stock_list_name]
      title_tmp = (title.blank?) ? "stocklist" : title.gsub(/[^0-9A-Za-z]/, ' ').squeeze(" ").strip.gsub(" ","-").downcase
      filename = "#{title_tmp}_#{Time.zone.now.strftime("%d%m%Y-%H%I%S")}" + ".pdf"
      pdf_file = Attachment.create(:size => 1001, :content_type => "application/pdf", :filename => filename, :attachable_id => @office.id, :attachable_type => "Office", :title => title, :pdf_layout => html, :send_to_api => false, :position => 15)
      if pdf_file
        pdf_file.update_attribute(:type, "Stocklist")
        pdf = Attachment.find(pdf_file.id)
        path = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
        path_folder = path.gsub("/"+filename, "")
        url_path = BASE_URL+generate_pdf_agent_office_stock_lists_path(@agent, @office)+"?pdf_id="+pdf_file.id.to_s
        temporary = "#{RAILS_ROOT}/tmp/#{pdf.filename}"
        temporary.gsub!(".pdf","-#{pdf.id}.pdf")
        spawn(:kill => true) do
          if session[:stock_layout] == "theme7"
            system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 -O Landscape --page-size A3 #{url_path} #{temporary}")
          else
            system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{url_path} #{temporary}")
          end

          begin
            exists = FileTest.exist?(temporary)
            if exists
              uploaded = pdf.upload_to_s3(temporary)
            end
            try = 0
            while try < 5 && !uploaded && exists
              try = try + 1
              uploaded = pdf.upload_to_s3(temporary)
              sleep(5)
            end
            if uploaded
              system("rm -rf #{temporary}") if FileTest.exist?(temporary)
            end
          rescue Exception => ex
            Log.create(:message => "ERROR stoklist =#{ex.inspect}")
          end
        end
      end
      redirect_to list_agent_office_stock_lists_path(@agent, @office)
#      redirect_to preview_agent_office_stock_lists_path(@agent, @office)
    end
  end

  def download
    unless params[:id].blank?
     pdf  = Attachment.find_by_id(params[:id])
     if !pdf.blank?
       file = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
       if pdf.moved_to_s3 == false
         if FileTest.exist?(file)
           pdf.upload_to_s3(file)
           redirect_to pdf.public_filename
         else
           if pdf.created_at.strftime("%Y-%m-%d") < Time.zone.now.strftime("%Y-%m-%d")
             flash[:notice] = "Stock List not found."
           else
             flash[:notice] = "the pdf is still creating please try again in a few minutes."
           end
           redirect_to :action => "list"
         end
#          flash[:notice] = "the pdf is still creating please try again in a few minutes."
#          redirect_to :action => "list"
       else
         system("rm -rf #{file.gsub(pdf.filename, "")}") if FileTest.exist?(file)
         redirect_to pdf.public_filename
       end
     else
       flash[:notice] = "Stock List not found."
       redirect_to :action => "list"
     end
    end
  end

  def delete_stock
    unless params[:id].blank?
     pdf  = Attachment.find_by_id(params[:id])
     pdf.destroy unless pdf.blank?
    end
    redirect_to list_agent_office_stock_lists_path(@agent, @office)
  end

  def pdf_data(check_checkbox = nil)
    @stock_layout = session[:stock_layout].blank? ? "theme1" : session[:stock_layout]
    @title_list = session[:stock_list_name]
    @title_color = session[:stock_list_color]
    @black_text = session[:black_text]
    @header_image = find_header(((@stock_layout == "theme6")? "Brochure Header3" : "Brochure Header"))
    @header_image2 = find_header("Brochure Header2")
    @footer_image = find_header("Brochure Footer")
    @opentime = session[:stock_opentimes]
    @auction = session[:stock_auctions]
    @first_level = session[:stock_first_level]
    @second_level = session[:stock_second_level]
    @status = session[:stock_status]
    @period = session[:stock_period]
    @min_price = session[:stock_min_price].gsub(/[^0-9.]/, '') unless session[:stock_min_price].blank?
    @max_price = session[:stock_max_price].gsub(/[^0-9.]/, '') unless session[:stock_max_price].blank?
    @min_rent = session[:stock_min_rent].gsub(/[^0-9.]/, '') unless session[:stock_min_rent].blank?
    @max_rent = session[:stock_max_rent].gsub(/[^0-9.]/, '') unless session[:stock_max_rent].blank?
    @min_size = session[:stock_min_size].gsub(/[^0-9.]/, '') unless session[:stock_min_size].blank?
    @max_size = session[:stock_max_size].gsub(/[^0-9.]/, '') unless session[:stock_max_size].blank?
    @building_min_size = session[:stock_building_min_size].gsub(/[^0-9.]/, '') unless session[:stock_building_min_size].blank?
    @building_max_size = session[:stock_building_max_size].gsub(/[^0-9.]/, '') unless session[:stock_building_max_size].blank?
    @baths = session[:stock_baths]
    @beds = session[:stock_beds]
    @cars = session[:stock_cars]
    @opentime_type = session[:stock_opentime_type]
    @opentime_startdate = session[:stock_opentime_startdate]
    @display_bed = session[:stock_display_bed]
    @display_bath = session[:stock_display_bath]
    @display_car = session[:stock_display_car]
    @display_bond = session[:stock_display_bond]
    @display_contact = session[:stock_display_contact]
    @display_price = session[:stock_display_price]
    @display_property_id = session[:stock_display_property_id]
    @display_internet_id = session[:stock_display_internet_id]
    @display_available = session[:stock_display_available]
    @display_auction = session[:stock_display_auction]
    @display_land = session[:stock_display_land]
    @display_headline = session[:stock_display_headline]
    @display_desc = session[:stock_display_desc]
    @display_suburb = session[:stock_display_suburb]
    @display_address = session[:stock_display_address]
    @display_fullsize = session[:stock_display_fullsize]

    @listing_type = session[:stock_listing_type].blank? ? "All" : session[:stock_listing_type]
    @suburb = session[:stock_suburb].blank? ? "All" : session[:stock_suburb]
    @property_type = session[:stock_property_type].blank? ? "All" : session[:stock_property_type]
    @sale_lease = session[:stock_sale_lease].blank? ? "All" : session[:stock_sale_lease]
    @primary_contact = session[:stock_primary_contact].blank? ? "All" : session[:stock_primary_contact]
    @property_checkbox_ids = session[:stocklist_property_checkbox].join(",") unless session[:stocklist_property_checkbox].blank?
    days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', "Friday", "Saturday", "Sunday"]

    order_by = "`properties`.`id`"
    unless @first_level.blank?
      if @stock_layout == "theme4"
        order_by = (@first_level == "status_order") ? "#{@first_level}, auction_date_order, property_type_order" : "`properties`.#{@first_level}"
      else
        order_by = (@first_level == "status_order") ? @first_level : "`properties`.#{@first_level}"
      end
    end

    unless @second_level.blank?
      order_by += (@second_level == "status_order") ? ", #{@second_level}" : ", `properties`.#{@second_level}"
    end

    ids_sql = "properties.id IN (#{@property_checkbox_ids})" if check_checkbox == true && !@property_checkbox_ids.blank?

    if !ids_sql.blank?
      conditions = ["#{ids_sql} AND (`attachments`.position = (SELECT min(atc.`position`) as latest FROM `attachments` as atc WHERE `atc`.`attachable_id` = `properties`.id AND `atc`.`type` LIKE 'Image' AND `atc`.`attachable_type` LIKE 'Property' AND `atc`.thumbnail IS NULL))"]
      @properties = stocklist_sql(conditions, order_by)
    else

      lt_conditions = ""
      unless @listing_type.blank?
        @listing_type.each_with_index{|list,index|
          lt_conditions += "," if index > 0
          lt_conditions += "'#{list}'"
        }
        listing_conditions = @listing_type.include?("All") ? "" : "And (properties.type IN (#{lt_conditions}))"
      end

      sb_conditions = ""
      unless @suburb.blank?
        @suburb.each_with_index{|sb,index|
          sb_conditions += "," if index > 0
          sb_conditions += "'#{sb}'"
        }
        suburb_conditions = @suburb.include?("All") ? "" : "And (properties.suburb IN (#{sb_conditions}))"
      end

      pt_conditions = ""
      unless @property_type.blank?
        @property_type.each_with_index{|pt,index|
          pt_conditions += "," if index > 0
          pt_conditions += "'#{pt}'"
        }
        property_type_conditions = @property_type.include?("All") ? "" : "And (properties.property_type IN (#{pt_conditions}))"
      end

      sl_conditions = ""
      unless @sale_lease.blank?
        @sale_lease.each_with_index{|sl,index|
          sl_conditions += "," if index > 0
          sl_conditions += "'#{sl}'"
        }
        sale_lease_conditions = @sale_lease.include?("All") ? "" : "And (properties.deal_type IN (#{sl_conditions}))"
      end

      pc_conditions = ""
      unless @primary_contact.blank?
        @primary_contact.each_with_index{|pc,index|
          p_user = AgentUser.find_by_sql("SELECT id FROM `users` WHERE CONCAT(`first_name`, ' ', `last_name`) = '#{pc}'").first
          next if p_user.blank?
          pc_conditions += "," if index > 0
          pc_conditions += "#{p_user.id}"
        }
        primary_contact_conditions = @primary_contact.include?("All") ? "" : "And (properties.primary_contact_id IN (#{pc_conditions}))"
      end

      price_conditions = size_conditions = ""
      if !@min_price.blank? && !@max_price.blank?
        price_conditions = "And ((properties.price >= #{@min_price} And properties.price <= #{@max_price})"
        if !@min_rent.blank? && !@max_rent.blank?
          if @listing_type.include?("All") || @listing_type.include?("Commercial")
            price_conditions += " OR (commercial_details.current_rent >= #{@min_rent} And commercial_details.current_rent <= #{@max_rent})"
          end
        end
        price_conditions += ")"
      end

      if !@min_size.blank? && !@max_size.blank?
        if @listing_type.include?("All") || @listing_type.include?("Commercial")
          size_conditions = "And (commercial_details.land_area >= #{@min_size} And commercial_details.land_area <= #{@max_size})"
        end
      end

      if !@building_min_size.blank? && !@building_max_size.blank?
        if @listing_type.include?("All") || @listing_type.include?("Commercial")
          size_conditions = "And (commercial_details.floor_area >= #{@building_min_size} And commercial_details.floor_area <= #{@building_max_size})"
        end
      end

      if !@beds.blank?
        if @listing_type.include?("All") || @listing_type.include?("ResidentialSale") || @listing_type.include?("ResidentialLease")
          size_conditions = "And (residential_sale_details.bedrooms >= #{@beds} OR residential_lease_details.bedrooms >= #{@beds})"
        end
      end

      if !@baths.blank?
        if @listing_type.include?("All") || @listing_type.include?("ResidentialSale") || @listing_type.include?("ResidentialLease")
          size_conditions = "And (residential_sale_details.bathrooms >= #{@baths} OR residential_lease_details.bathrooms >= #{@baths})"
        end
      end

      if !@cars.blank?
        if @listing_type.include?("All") || @listing_type.include?("ResidentialSale") || @listing_type.include?("ResidentialLease")
          size_conditions = "And (residential_sale_details.carport_spaces >= #{@cars} OR residential_lease_details.carport_spaces >= #{@cars})"
        end
      end

      opentime_sql_day = @opentime_days = ""
      if @opentime.try.include?("All")
        @opentime_days = "All"
        @opentime.each_with_index{|d,index|
          opentime_sql_day += (index==1)? "#{d}" : ",#{d}" unless d == "All"
        } unless @opentime.blank?
      else
        @opentime.each_with_index{|d,index|
          @opentime_days  += (index==0)? "#{days[d.to_i]}" : ",#{days[d.to_i]}"
          opentime_sql_day += (index==0)? "#{d}" : ",#{d}"
        } unless @opentime.blank?
      end

      auction_sql_day = @auction_days = ""
      if @auction.try.include?("All")
        @auction_days = "All"
        auction_sql_day = "0,1,2,3,4,5,6"
      else
        @auction.each_with_index{|d,index|
          @auction_days  += (index==0)? "#{days[d.to_i]}" : ",#{days[d.to_i]}"
          auction_sql_day += (index==0)? "#{d}" : ",#{d}"
        } unless @auction.blank?
      end

      status_sql = ""
      if @status.blank?
        status_sql = " AND (properties.status = 1 OR properties.status = 5)"
      else
        sts = ''
        @status.each_with_index{|d,index| sts += (index==0)? "#{d}" : ",#{d}" }
        status_sql = " AND properties.status IN (#{sts}) "
        unless @period.blank?
          if @period == '7d' || @period == '14d'
            #if sts.include?("2")
              status_sql += " AND ((properties.status NOT IN (2,3,5) And properties.updated_at > '#{1.year.ago.strftime("%Y-%m-%d %H:%M")}') OR (properties.status = 2 And properties.sold_on > '#{7.day.ago.strftime("%Y-%m-%d")}') OR (properties.status = 3 And properties.leased_on > '#{7.day.ago.strftime("%Y-%m-%d")}') OR (properties.status = 5 And properties.updated_at > '#{7.day.ago.strftime("%Y-%m-%d")}')) " if @period == '7d'
              status_sql += " AND ((properties.status NOT IN (2,3,5) And properties.updated_at > '#{1.year.ago.strftime("%Y-%m-%d %H:%M")}') OR (properties.status = 2 And properties.sold_on > '#{14.day.ago.strftime("%Y-%m-%d")}') OR (properties.status = 3 And properties.leased_on > '#{14.day.ago.strftime("%Y-%m-%d")}') OR (properties.status = 5 And properties.updated_at > '#{14.day.ago.strftime("%Y-%m-%d")}')) " if @period == '14d'
            #end
          else
            ( status_sql += " AND ((properties.status NOT IN (2,3,5) And properties.updated_at > '#{1.year.ago.strftime("%Y-%m-%d %H:%M")}') OR (properties.status = 2 And properties.sold_on > '#{@period.to_i.month.ago.strftime("%Y-%m-%d")}') OR (properties.status = 3 And properties.leased_on > '#{@period.to_i.month.ago.strftime("%Y-%m-%d")}')) " if sts.include?("2") || sts.include?("3"))
          end
        end
      end

      conditions = ["properties.office_id = #{@office.id} #{status_sql} AND (`attachments`.position = (SELECT min(atc.`position`) as latest FROM `attachments` as atc WHERE `atc`.`attachable_id` = `properties`.id AND `atc`.`type` LIKE 'Image' AND `atc`.`attachable_type` LIKE 'Property' AND `atc`.thumbnail IS NULL)) #{listing_conditions} #{suburb_conditions} #{property_type_conditions} #{sale_lease_conditions} #{primary_contact_conditions} #{price_conditions} #{size_conditions}"]
      unless @opentime.blank?
        conditions[0] += " AND weekday(`opentimes`.date) IN (#{opentime_sql_day})" unless opentime_sql_day.blank?
      end

#      unless @opentime_type.blank?
#        if @opentime_type != "all"
#          start_date = Date.parse(@opentime_startdate.gsub("/","-")) if @opentime_startdate.present?
#          today = @opentime_startdate.present? ? start_date : Date.today
#          date_until = today + @opentime_type.to_i
#          conditions[0] += " AND (`opentimes`.date <= '#{date_until.strftime("%Y-%m-%d")}' && `opentimes`.date >= '#{today}') "
#        end
#      end

      unless auction_sql_day.blank?
        if @listing_type.include?("ResidentialSale") || @listing_type.include?("Commercial") || @listing_type.include?("BusinessSale")
          auction_conditions = ""
          auction_conditions = " weekday(`residential_sale_details`.auction_date) IN (#{auction_sql_day})" if @listing_type.include?("ResidentialSale")
          if @listing_type.include?("Commercial")
            auction_conditions += " OR " unless auction_conditions.blank?
            auction_conditions += "weekday(`commercial_details`.auction_date) IN (#{auction_sql_day})"
          end
          if @listing_type.include?("BusinessSale")
            auction_conditions += " OR " unless auction_conditions.blank?
            auction_conditions += "weekday(`business_sale_details`.auction_date) IN (#{auction_sql_day})" if @listing_type.include?("BusinessSale")
          end
          conditions[0] += " AND (#{auction_conditions})"
        end
      end

      @properties = stocklist_sql(conditions, order_by)
      @psize = @properties.size
      if @stock_layout == "theme1"
        tmp_min = (@psize+1)%10
        tmp_min = tmp_min - 1
        min = 8 - tmp_min
        (1..min).step.each{ |a| @properties << "" } if tmp_min != 0 && min > 0
      end

      if @stock_layout == "theme3"
        tmp_min = @psize%4
        min = 4 - tmp_min
        (1..min).step.each{ |a| @properties << "" } if tmp_min != 0
      end

      if @stock_layout == "theme5"
        @psize_min = 0
        if @psize != 0
          if @psize % 12 != 0
            page_size = (@psize.to_f/12).ceil
            all_property = page_size * 12
            @psize_min = all_property - @psize
          else
            @psize_min = 1
          end
        end
      end

      if @stock_layout == "theme7"
        if @psize.present?
          tmp = @psize % 16
          adder = 16 - tmp
          (1..adder).step.each{ |a| @properties << "" } if adder != 0
        end
      end

    end
  end

  def stocklist_sql(conditions, order_by)
    sql = [" SELECT DISTINCT `properties`.id, `properties`.status, IF(`properties`.property_type = 'Land' , 'ZLand', 'a') as property_type_order, IF(`residential_sale_details`.forthcoming_auction = 1 , 'a', IF(`commercial_details`.forthcoming_auction = 1 , 'a', IF(`business_sale_details`.forthcoming_auction = 1 , 'a', 'z'))) as auction_date_order, IF(`properties`.status = 2 , 12 , IF(`properties`.status = 5 , 11, `properties`.status)) as status_order, `properties`.type, `properties`.property_id, `properties`.headline, `properties`.description, `properties`.unit_number, `properties`.display_address, `properties`.street_number, `properties`.street, `properties`.suburb, `properties`.display_price, `properties`.price, `properties`.display_price_text, `properties`.primary_contact_id, `properties`.deal_type "]
    sql[0] += ", `attachments`.id AS image_id, `attachments`.parent_id AS image_p_id, `attachments`.filename AS image_file , `residential_sale_details`.`auction_place` AS `residential_sale_auction_place`,`residential_sale_details`.`auction_time` AS `residential_sale_auction_time`,`residential_sale_details`.`auction_date` AS `residential_sale_auction_date`, `residential_sale_details`.`half_bedroom` as residential_sale_half_bedroom, `residential_sale_details`.`bedrooms` as residential_sale_bedrooms,`residential_sale_details`.`half_bathroom` as residential_sale_half_bathroom,`residential_sale_details`.`bathrooms` as residential_sale_bathrooms,`residential_sale_details`.`carport_spaces` as residential_sale_carport_spaces, `residential_sale_details`.`garage_spaces` as residential_sale_garage_spaces,`residential_sale_details`.`garage_spaces` as residential_sale_garage_spaces,`residential_sale_details`.`land_area` as residential_sale_land,`residential_sale_details`.`land_area_metric` as residential_sale_land_metric,`residential_sale_details`.`method_of_sale` as residential_sale_method_of_sale,`residential_sale_details`.`closing_date` as residential_sale_closing_date,`residential_sale_details`.`property_url` as residential_sale_property_url,`residential_lease_details`.`half_bedroom` as residential_lease_half_bedroom,`residential_lease_details`.`bedrooms` as residential_lease_bedrooms,`residential_lease_details`.`half_bathroom` as residential_lease_half_bathroom,`residential_lease_details`.`bathrooms` as residential_lease_bathrooms, `residential_lease_details`.`carport_spaces` as residential_lease_carport_spaces, `residential_lease_details`.`garage_spaces` as residential_lease_garage_spaces,`residential_lease_details`.`garage_spaces` as residential_lease_garage_spaces,`residential_lease_details`.`land_area` as residential_lease_land,`residential_lease_details`.`land_area_metric` as residential_lease_land_metric,`residential_lease_details`.`bond` as residential_lease_bond,`residential_lease_details`.`property_url` as residential_lease_property_url, `commercial_details`.`auction_place` AS `commercial_auction_place`, `commercial_details`.`auction_date` AS `commercial_auction_date`, `commercial_details`.`auction_time` AS `commercial_auction_time`, `commercial_details`.`current_rent` AS `commercial_current_rent`, `commercial_details`.`land_area` AS `commercial_land`, `commercial_details`.`land_area_metric` AS `commercial_land_metric`,`commercial_details`.`property_url` as commercial_property_url, `business_sale_details`.`auction_place` AS `business_sale_auction_place`, `business_sale_details`.`auction_date` AS `business_sale_auction_date`, `business_sale_details`.`auction_time` AS `business_sale_auction_time`, `business_sale_details`.`land_area` AS `business_sale_land`, `business_sale_details`.`land_area_metric` AS `business_sale_land_metric`,`business_sale_details`.`property_url` as business_sale_property_url, `project_sale_details`.`half_bedroom` as project_sale_half_bedroom, `project_sale_details`.`bedrooms` as project_sale_bedrooms, `project_sale_details`.`half_bathroom`  as project_sale_half_bathroom, `project_sale_details`.`bathrooms` as project_sale_bathrooms, `project_sale_details`.`carport_spaces` as project_sale_carport_spaces, `project_sale_details`.`garage_spaces` as project_sale_garage_spaces,`project_sale_details`.`garage_spaces` as project_sale_garage_spaces, `project_sale_details`.`land_area` as project_sale_land, `project_sale_details`.`land_area_metric` as project_sale_land_metric,`project_sale_details`.`property_url` as project_sale_property_url,`holiday_lease_details`.`half_bedroom` as holiday_lease_half_bedroom,`holiday_lease_details`.`bedrooms` as holiday_lease_bedrooms, `holiday_lease_details`.`half_bathroom` as holiday_lease_half_bathroom, `holiday_lease_details`.`bathrooms` as holiday_lease_bathrooms, `holiday_lease_details`.`carport_spaces` as holiday_lease_carport_spaces, `holiday_lease_details`.`garage_spaces` as holiday_lease_garage_spaces,`holiday_lease_details`.`garage_spaces` as holiday_lease_garage_spaces, `holiday_lease_details`.`land_area` as holiday_lease_land, `holiday_lease_details`.`land_area_metric` as holiday_lease_land_metric,`holiday_lease_details`.`property_url` as holiday_lease_property_url, `commercial_details`.`date_available` AS `commercial_date_available`,`residential_lease_details`.`date_available` as residential_lease_date_available, CONCAT(`users`.first_name, ' ', `users`.last_name) as contact, `users`.first_name as contact_first_name, `users`.last_name as contact_last_name, `users`.phone, `users`.mobile from `properties` "
    sql[0] += " LEFT JOIN `attachments` ON (`attachments`.`attachable_id` = `properties`.`id` AND `attachments`.`type` LIKE 'Image' AND `attachments`.`attachable_type` LIKE 'Property' AND `attachments`.thumbnail IS NULL) LEFT JOIN `users` ON (`users`.`id` = `properties`.`primary_contact_id` ) LEFT JOIN `opentimes` ON (`opentimes`.`property_id` = `properties`.`id` ) LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "
    sql_part1 = [" WHERE ( #{conditions.to_s} ) "]
    sql_part2 = [" AND ( properties.deleted_at IS NULL ) "]
    sql_part3 = [" ORDER BY #{order_by} ASC"]
    sql[0] += sql_part1.to_s
    sql[0] += sql_part2.to_s
    sql[0] += sql_part3.to_s
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    properties = @mysql_result.all_hashes
    @mysql_result.free
    return properties
  end

  def find_header(type)
    get_header_footer(@office.id, type)
  end

  def prepare_data
    @listings = ['ResidentialSale', 'ResidentialLease', 'HolidayLease', 'Commercial', 'CommercialBuilding','ProjectSale', 'BusinessSale', 'NewDevelopment', 'LandRelease'].map{|m| [m,m]}.unshift(['Please select',nil])
    ptypes = Property.find_by_sql("SELECT DISTINCT(`property_type`) FROM `properties` WHERE office_id = #{@office.id} And `property_type` IS NOT NULL ORDER BY `property_type` ASC")
    @property_types = ptypes.map{|m| [m.property_type, m.property_type] }.delete_if{|x, y| x.blank?}.unshift(['Please select',nil])

    suburbs = Property.find_by_sql("SELECT DISTINCT(`suburb`) FROM `properties` WHERE office_id = #{@office.id} And `suburb` IS NOT NULL ORDER BY `suburb` ASC")
    @suburbs = suburbs.map{|m| [m.suburb, m.suburb] unless m.suburb.blank?}.delete_if{|x, y| x.blank?}.unshift(['Please select',nil])
    @sale_leases = [["Sale & Lease", "Both"], ["Sale", "Sale"], ["Lease", "Lease"]].map{|k, v| [k, v]}.unshift(['Please select',nil])
    @primary_contacts = @office.agent_users.find(:all, :conditions => "roles.id = 3 OR roles.id = 4 OR roles.id = 5", :include => "roles", :order => "`users`.first_name ASC").map{|u| [u.full_name, u.full_name]}.unshift(['Please select', nil]).uniq
  end

  def populate_property_type
    lt_conditions = ""
    listing_type = params[:listings].split("#").delete_if{|x| x.blank?}
    unless listing_type.blank?
      listing_type.each_with_index{|list,index|
        lt_conditions += "," if index > 0
        lt_conditions += "'#{list}'"
      }
      listing_conditions = listing_type.include?("All") ? "" : "And (properties.type IN (#{lt_conditions}))"
    end
    ptypes = Property.find_by_sql("SELECT DISTINCT(`property_type`) FROM `properties` WHERE office_id = #{@office.id} And `property_type` IS NOT NULL #{listing_conditions} ORDER BY `property_type` ASC")
    @property_types = ptypes.map{|m| [m.property_type, m.property_type] }.delete_if{|x, y| x.blank?}
    return(render(:json => @property_types))
  end

  def search_property
    init_data
    pdf_data
    property_ids = params[:property_ids]
    property_ids = property_ids.split(",") unless property_ids.blank?

    jsons = []
    @properties.each{|property|
      address = "#{property["unit_number"].blank? ? property["street_number"].try.downcase : property["unit_number"]+"/"+property["street_number"].try.downcase} #{property["street"].try.downcase}"
      if !property_ids.blank?
        checked = (property_ids.include?(property["id"]))? "checked" : ""
      else
        checked = "checked"
      end
      jsons << {'id' => property["id"], 'address' => address, 'checked' => checked}
    } unless @properties.nil?
    text = {"results" => jsons, "total" => @properties.length }
    return(render(:json => text))
  end

  def init_data
      session[:stock_opentimes] = params[:opentimes]
      session[:stock_auctions] = params[:auctions]
      session[:stock_status] = params[:status]
      session[:stock_period] = params[:period]
      session[:stock_min_price] = params[:min_price]
      session[:stock_max_price] = params[:max_price]
      session[:stock_min_rent] = params[:min_rent]
      session[:stock_max_rent] = params[:max_rent]
      session[:stock_min_size] = params[:min_size]
      session[:stock_max_size] = params[:max_size]
      session[:stock_building_min_size] = params[:building_min_size]
      session[:stock_building_max_size] = params[:building_max_size]
      session[:stock_baths] = params[:baths]
      session[:stock_beds] = params[:beds]
      session[:stock_cars] = params[:cars]
      session[:stock_listing_type] =  params[:listing_type].blank? ? "" : params[:listing_type].split("#").delete_if{|x| x.blank?}
      session[:stock_suburb] =  params[:suburb].blank? ? "" : params[:suburb].split("#").delete_if{|x| x.blank?}
      session[:stock_property_type] = params[:property_type].blank? ? "" : params[:property_type].split("#").delete_if{|x| x.blank?}
      session[:stock_sale_lease] = params[:sale_lease].blank? ? "" : params[:sale_lease].split("#").delete_if{|x| x.blank?}
      session[:stock_primary_contact] = params[:primary_contact].blank? ? "" : params[:primary_contact].split("#").delete_if{|x| x.blank?}
  end
end