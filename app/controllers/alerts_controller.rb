class AlertsController < ApplicationController
  layout 'agent'

  before_filter :login_required
  before_filter :get_agent_and_office
  before_filter :prepare_form
  before_filter :prepare_contact
  append_before_filter :prepare_creation

  def index
    @contact_alert = ContactAlert.new
  end

  def show
  end

  def new
  end

  def edit
    @all_regions = @all_regions_list = @all_suburbs = @all_suburbs_list =''
    @contact_alert = ContactAlert.find(params[:id])
    @tenant = AgentContact.find(@contact_alert.tenant) if @contact_alert.tenant && @contact_alert.numeric?
    unless @contact_alert.region.blank?
      @regions_data = @contact_alert.region.split(',')
      @regions_data.map{|cat|
        random = (0..100).to_a.rand.to_s
        unless cat == ''
          @all_regions = @contact_alert.region
          @all_regions_list = @all_regions_list + '<div id="'+random+'"style="padding-left:176px;;width:200px"class="property_type_selected">'+cat+'<a href="#" onclick="remove_region('+random+',\''+cat+'\')">x</a></div>'
        end
      }
    end

    unless @contact_alert.suburb.blank?
      @suburbs_data = @contact_alert.suburb.split(',')
      @suburbs_data.map{|cat|
        random = (0..100).to_a.rand.to_s
        unless cat == ''
          @all_suburbs = @contact_alert.suburb
          @all_suburbs_list = @all_suburbs_list + '<div id="'+random+'"style="padding-left:176px;;width:200px"class="property_type_selected">'+cat+'<a href="#" onclick="remove_suburb('+random+',\''+cat+'\')">x</a></div>'
        end
      }
    end
  end

  def create
    params[:contact_alert] = params[:contact_alert]
#    unless params[:suburb].blank?
#      params[:contact_alert] = params[:contact_alert].merge(:suburb => params[:suburb])
#    end
#
#    unless params[:region].blank?
#      params[:contact_alert] = params[:contact_alert].merge(:region => params[:region])
#    end

    @contact_alert = ContactAlert.new(params[:contact_alert])
    @contact_alert.agent_contact_id = params[:agent_contact_id]
    respond_to do |format|
      if @contact_alert.save
        #        prenote = "Requirements Type = #{params[:contact_alert][:alert_type]},"
        #        prenote += "Listing Type = #{params[:contact_alert][:listing_type]}," unless params[:contact_alert][:listing_type].blank?
        #        prenote +="Property Type = #{params[:contact_alert][:property_type]}," unless params[:contact_alert][:property_type].blank?
        #        prenote += "Min Bedroom = #{params[:contact_alert][:min_bedroom]}," unless params[:contact_alert][:min_bedroom].blank?
        #        prenote += "Min Bathroom = #{params[:contact_alert][:min_bathroom]}," unless params[:contact_alert][:min_bathroom].blank?
        #        prenote += "Min carspace = #{params[:contact_alert][:min_carspace]}," unless params[:contact_alert][:min_carspace].blank?
        #        prenote += "Min Price = #{params[:contact_alert][:min_price]}," unless params[:contact_alert][:min_price].blank?
        #        prenote += "Max Price = #{params[:contact_alert][:max_price]}," unless params[:contact_alert][:max_price].blank?
        #        prenote += "Suburbs = #{params[:contact_alert][:suburb]}," unless params[:contact_alert][:suburb].blank?

        #        @contact_alert.agent_contact.create_note_for_contact(1, prenote, current_user.id)
        ContactAlert.transaction do
          add_multiple_value
        end
        flash[:notice] = 'Contact Alert was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        feed_multiple_value
        format.html { render :action => "index" }
        format.xml  { render :xml => @contact_alert.errors}
      end
    end
  end

  def update
    @contact_alert = ContactAlert.find(params[:id])
    previous_listing_type = @contact_alert.listing_type unless @contact_alert.listing_type.blank?
    respond_to do |format|
      params[:contact_alert] = params[:contact_alert]
      #      destroy_suburb = @contact_alert.contact_alert_suburbs.destroy_all unless @contact_alert.contact_alert_suburbs.blank?
      #      destroy_region = @contact_alert.contact_alert_regions.destroy_all unless @contact_alert.contact_alert_regions.blank?
      #      destroy_residential_type = @contact_alert.contact_alert_residential_types.destroy_all unless @contact_alert.contact_alert_residential_types.blank?
      #      destroy_residential_building = @contact_alert.contact_alert_residential_buildings.destroy_all unless @contact_alert.contact_alert_residential_buildings.blank?
      #      destroy_commercial_type = @contact_alert.contact_alert_commercial_types.destroy_all unless @contact_alert.contact_alert_commercial_types.blank?
      if params[:contact_alert][:listing_type] == "ProjectSale"
        if previous_listing_type == "Residential"
          params[:contact_alert] = params[:contact_alert].merge(
            :availiability_type => nil,
            :listing_select_status_residential => nil,
            :residential_type => nil,
            :residential_building => nil,
            :state => nil,
            :region => nil,
            :suburb => nil,
            :internal_agent => nil,
            :external_company => nil,
            :external_agent => nil,
            :yield_from => nil,
            :yield_to => nil,
            :building_area_from => nil,
            :building_area_to => nil,
            :land_area_from => nil,
            :land_area_to => nil,
            :asking_price_from => nil,
            :annual_rental_from => nil,
            :annual_rental_to => nil,
            :total_floor_area_from => nil,
            :total_floor_area_to => nil,
            :office_from => nil,
            :office_to => nil,
            :warehouse_from => nil,
            :warehouse_to => nil,
            :rent_from => nil,
            :rent_to => nil,
            :lease_expiry_from => nil,
            :lease_expiry_to => nil,
            :tenant => nil)
        elsif previous_listing_type == "Commercial"
          params[:contact_alert] = params[:contact_alert].merge(
            :availiability_type => nil,
            :listing_select_status_commercial => nil,
            :commercial_property_type => nil,
            :state => nil,
            :region => nil,
            :suburb => nil,
            :internal_agent => nil,
            :external_company => nil,
            :external_agent => nil,
            :yield_from => nil,
            :yield_to => nil,
            :building_area_from => nil,
            :building_area_to => nil,
            :land_area_from => nil,
            :land_area_to => nil,
            :asking_price_from => nil,
            :annual_rental_from => nil,
            :annual_rental_to => nil,
            :total_floor_area_from => nil,
            :total_floor_area_to => nil,
            :office_from => nil,
            :office_to => nil,
            :warehouse_from => nil,
            :warehouse_to => nil,
            :rent_from => nil,
            :rent_to => nil,
            :lease_expiry_from => nil,
            :lease_expiry_to => nil,
            :tenant => nil)
        end
      elsif params[:contact_alert][:listing_type] == "Residential"
        if previous_listing_type == "Commercial"
          params[:contact_alert] = params[:contact_alert].merge(:listing_select_status_commercial => nil,:commercial_property_type => nil)
        elsif previous_listing_type == "ProjectSale"
          params[:contact_alert] = params[:contact_alert].merge(:houseland_property_type => nil, :price_range_from => nil, :price_range_to => nil, :houseland_status => nil, :suites => nil, :sqm_range_from => nil, :sqm_range_to => nil)
        end
      else
        if previous_listing_type == "Residential"
          params[:contact_alert] = params[:contact_alert].merge(:listing_select_status_residential => nil)
        elsif previous_listing_type == "ProjectSale"
          params[:contact_alert] = params[:contact_alert].merge(:houseland_property_type => nil, :price_range_from => nil, :price_range_to => nil, :houseland_status => nil, :suites => nil, :sqm_range_from => nil, :sqm_range_to => nil)
        end
      end

#      unless params[:suburb].blank?
#        params[:contact_alert] = params[:contact_alert].merge(:suburb => params[:suburb])
#      end
#
#      unless params[:region].blank?
#        params[:contact_alert] = params[:contact_alert].merge(:region => params[:region])
#      end

      if @contact_alert.update_attributes(params[:contact_alert])
        add_multiple_value
        if @contact_alert.listing_type == "ProjectSale"
          @contact_alert.contact_alert_commercial_types.destroy_all unless @contact_alert.contact_alert_commercial_types.blank?
          @contact_alert.contact_alert_residential_types.destroy_all unless @contact_alert.contact_alert_residential_types.blank?
          @contact_alert.contact_alert_residential_buildings.destroy_all unless @contact_alert.contact_alert_residential_buildings.blank?
          @contact_alert.contact_alert_regions.destroy_all unless @contact_alert.contact_alert_regions.blank?
          @contact_alert.contact_alert_suburbs.destroy_all unless @contact_alert.contact_alert_suburbs.blank?
        else
          if params[:delete_residential_attributes] == "true"
            @contact_alert.contact_alert_residential_types.destroy_all unless @contact_alert.contact_alert_residential_types.blank?
            @contact_alert.contact_alert_residential_buildings.destroy_all unless @contact_alert.contact_alert_residential_buildings.blank?
          elsif params[:delete_commercial_attributes] == "true"
            @contact_alert.contact_alert_commercial_types.destroy_all unless @contact_alert.contact_alert_commercial_types.blank?
          end
        end
        flash[:notice] = 'Contact Alert was successfully updated.'
        format.html { redirect_to :action => 'index' }
        format.xml  { head :ok }
      else
        feed_multiple_value
        format.html { render :action => "edit" }
        format.xml  { render :xml => @contact_alert.errors}
      end
    end
  end

  def destroy
    @contact_alert = ContactAlert.find(params[:id])
    @contact_alert.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
      format.xml  { head :ok }
    end
  end

  def feed_multiple_value
    @all_suburbs = @all_suburbs_list = @all_regions = @all_regions_list = ''
    unless params[:suburb].blank?
      @suburbs_data = params[:suburb].split(',')
      @suburbs_data.map{|cat|
        unless cat == ''
          @all_suburbs = params[:suburb]
          @all_suburbs_list = @all_suburbs_list + '<div id="'+cat.id.to_s+'"style="padding-left:176px;;width:200px"class="property_type_selected">'+cat.suburb.to_s+'<a href="#" onclick="remove_suburb('+cat.id.to_s+',\''+cat.suburb.to_s+'\')">x</a></div>'
        end
      }
    end

    unless params[:region].blank?
      @regions_data = params[:region].split(',')
      @regions_data.map{|cat|
        unless cat == ''
          @all_regions = params[:region]
          @all_regions_list = @all_regions_list + '<div id="'+cat.id.to_s+'"style="padding-left:176px;;width:200px"class="property_type_selected">'+cat.town_village.to_s+'<a href="#" onclick="remove_suburb('+cat.id.to_s+',\''+cat.town_village.to_s+'\')">x</a></div>'
        end
      }
    end
  end

  def add_multiple_value
    unless params[:contact_alert][:listing_type].blank?
      if params[:contact_alert][:listing_type] == "Residential"
        if params[:contact_alert][:residential_type].blank?
          @contact_alert.contact_alert_residential_types.destroy_all
        else
          @contact_alert.add_residential_types(params[:contact_alert][:residential_type])
        end

        if params[:contact_alert][:residential_building].blank?
          @contact_alert.contact_alert_residential_buildings.destroy_all
        else
          @contact_alert.add_residential_buildings(params[:contact_alert][:residential_building])
        end
      elsif params[:contact_alert][:listing_type] == "Commercial"
        if params[:contact_alert][:commercial_type].blank?
          @contact_alert.contact_alert_commercial_types.destroy_all
        else
          @contact_alert.add_commercial_types(params[:contact_alert][:commercial_type])
        end
      end
    end

    if params[:contact_alert][:suburb].blank?
      @contact_alert.contact_alert_suburbs.destroy_all
    else
      suburb = params[:contact_alert][:suburb].split(",")
      @suburb_ids = suburb.drop(1)
      @contact_alert.add_suburbs(@suburb_ids)
    end

    if params[:contact_alert][:region].blank?
      @contact_alert.contact_alert_regions.destroy_all
    else
      region = params[:contact_alert][:region].split(",")
      @region_ids = region.drop(1)
      @contact_alert.add_regions(@region_ids)
    end
  end

  def prepare_creation
    @agent_contact = AgentContact.find(params[:agent_contact_id])
    @range = (1..10).map{|x|[x,x]}.unshift(["Please select", ''])
    @alert_type = [['Please select', nil], ['Newly Listed Properties', 'Newly Listed Properties'], ['Updated Properties', 'Updated Properties'], ['Open Inspections', 'Open Inspections'], ['Sold/Leased Properties', 'Sold/Leased Properties'], ['Auctions', 'Auctions'], ['Latest News', 'Latest News']]
    @listing_type = [['Please select', nil], ['Business Sale', 'BusinessSale'], ['Commercial Sale/Lease', 'Commercial'], ['Residential Lease', 'ResidentialLease'],['Residential Sale', 'ResidentialSale'], ['Holiday Lease', 'HolidayLease'], ['Project Sale', 'ProjectSale']]
    @ptypes = [['Please select', nil]]
    @alerts = @agent_contact.contact_alerts.paginate(:all, :order => "`updated_at` DESC", :page =>params[:page], :per_page => 10) unless @agent_contact.contact_alerts.blank?
  end

  def populate_prop
    types = []
    unless params[:ltype].blank?
      country = Country.find_by_name(current_office.country)
      country_ptypes_id = []
      country_ptypes = CountryPropertyTypeRelationship.find_all_by_country_id(country.id,:select =>"property_type_id")
      country_ptypes.each do |c|
        country_ptypes_id << c.property_type_id
      end
      condition=[' 1=1']
      unless country_ptypes_id.blank?
        condition[0] += " AND id IN(?)"
        condition << country_ptypes_id
      end
      category = "Residential" if params[:ltype].include?('Residential') || params[:ltype].include?("Project")
      category = "Holiday" if params[:ltype].include?('Holiday')
      category = "Commercial" if params[:ltype].include?('Commercial')
      category = "Business" if params[:ltype].include?('Business')
      unless category.blank?
        condition[0] += " AND category like(?)"
        condition << category
      end
      types = PropertyType.find(:all,:conditions =>condition).map{|t| [t.name,t.name] }
    end

    return(render(:json => types))
  end

  def populate_regions
    unless params[:state_name].blank?
      @regions = TownCountry.find_all_by_state_name(params[:state_name]).map{|x|[x.name, x.name]}.unshift(['Please select', ''])
    end
    render :layout => false
  end

  def populate_suburbs
    @contact_alert = ContactAlert.find(params[:id])
    #    @regions = TownCountry.find_all_by_state_name(params[:state_name]).map{|x|[x.name,x.name]}.unshift(['Please select', ''])
    unless params[:region_name].blank? && params[:state_name].blank?
      @suburbs = Suburb.find(:all, :include => [:town_country, :state], :conditions => ["town_countries.name = ? AND states.name = ?", "#{params[:region_name]}", "#{params[:state_name]}"]).map{|x|[x.name, x.name]}.unshift(['Please select', ''])
    else
      unless @contact_alert.state.blank? && @contact_alert.region.blank?
        @suburbs = Suburb.find(:all, :joins => [:town_country, :state], :conditions => ["town_countries.name = ? AND states.name = ?", "#{@contact_alert.region.split(',').last}", "#{@contact_alert.state}"]).map{|x|[x.name, x.name]}.unshift(['Please select', ''])
      end
    end
    @all_suburbs = @all_suburbs_list = ''
    unless params[:arr_suburb].blank?
      @suburbs_data = params[:arr_suburb].split(',')
      @suburbs_data.map{|cat|
        unless cat == ''
          random = (0..100).to_a.rand.to_s
          @all_suburbs = params[:arr_suburb]
          @all_suburbs_list = @all_suburbs_list + '<div id="'+random+'"style="padding-left:176px;;width:200px"class="property_type_selected">'+cat+'<a href="#" onclick="remove_suburb('+random+',\''+cat+'\')">x</a></div>'
        end
      }
    end
    render :layout => false
  end

  def change_property_types
    unless params[:ptype].blank?
      @categories = PropertyType.find_all_by_category(params[:ptype])
    end
    render :layout => false
  end

  def edit_property_types
    unless params[:ptype].blank?
      @categories = PropertyType.find_all_by_category(params[:ptype])
    end
    render :layout => false
  end

  private

  def prepare_form
    @listing_options = ['Commercial', 'ResidentialSale', 'ProjectSale']
    @property_type = PropertyType.find(:all, :conditions => ["property_types.category != 'Commercial'"]).map{|x|[x.name, x.name]}.unshift(['Please select', nil])
    @business_type = PropertyType.business.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    @price_from = ['0..10000']
    @suites = (1..30).map{|x|[x,x]}.unshift(["Please select", ''])
    @ranges = (1..30).map{|x|[x,x]}.unshift(["Please select", ''])
    @type_options = PropertyType.find(:all, :conditions => ["property_types.category = ?", "Commercial"])
    @residential_type = PropertyType.find(:all, :conditions => ["property_types.category = ?", "Residential"])
    @commercial_type = [["Office"], ["Medical / Consulting"], ["Land / Development"], ["Industrial"], ["Other"], ["Showroom / Bulky Goods"], ["Retail"], ["Hotel / Motel"], ["Rural / Farms"]]
    @available_options = ['available','sold','leased','withdrawn','underoffer','draft']
    @states = State.find_all_by_country_id(12).map{|x|[x.name,x.name]}.unshift(['Please select', ''])
    @regions = TownCountry.all.map{|x|[x.name,x.name]}.unshift(['Please select', ''])
    @internal_agents = AgentUser.find(:all, :conditions => ["developer_id = ? AND office_id = ?", @agent.developer_id, @office.id]).map{|x|[x.full_name.blank? ? x.first_name : x.full_name, x.full_name.blank? ? x.first_name : x.full_name]}.unshift(['Please Select From List', nil])
    @external_agents = AgentContact.find(:all, :conditions => ["office_id = ?", @office.id]).map{|x|[x.full_name.blank? ? x.first_name : x.full_name, x.full_name.blank? ? x.first_name : x.full_name]}.unshift(['Please Select From List', nil])
    @external_companies = AgentCompany.find(:all, :conditions => ["agent_id != ? AND office_id != ?", current_agent.id, current_office.id]).map{|x|[x.company, x.company]}.unshift(['Please Select From List', nil])
    @residential_buildings = Property.find(:all, :conditions => ["type = ? AND office_id = ?", "NewDevelopment", @office]).collect(&:suburb).uniq

    @commercial_types = PropertyType.commercial.all
    @residential_types = PropertyType.residential.all
  end

  def prepare_contact
    @agent_contact = AgentContact.find(params[:agent_contact_id])
  end
end
