class Admin::UpdatesController < ApplicationController
  layout 'admin'
  require_role 'AdminUser'

  def show
    @news_data = News.first.data

  end

  def update
    @news = News.first
    @news.build_data(params[:news_data_key], params[:news_data])
    @news.save
    redirect_to :action => "show"
  end
end
