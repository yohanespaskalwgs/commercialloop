class Admin::ActiveHelpsController < ApplicationController

  layout 'admin'
  require_role 'AdminUser'

  # make_resourceful do
  #   actions :all
  # end
  def show
    @help_data = ActiveHelp.first.data
  end

  def update
    @active_help = ActiveHelp.first
    @active_help.build_data(params[:help_data_key], params[:help_data])
    @active_help.save
    redirect_to :action => "show"
  end

  private

  def prepare_form
    @pages = [['Please Select', nil],['My Dashboard', 'My Dashboard'],['Client Centre', 'Client Centre'],['Client Summary', 'Client Summary'],['Client Office', 'Client Office'],['Message Centre', 'Message Centre'],['Client Messages', 'Client Messages'],['Commercial Loop Messages', 'Commercial Loop Messages'],['Account', 'Account'],['Current Plans', 'Current Plans'],['Billing', 'Billing'], ['Business Details', 'Business Details'], ['Team Member', 'Team Member']]
  end

end
