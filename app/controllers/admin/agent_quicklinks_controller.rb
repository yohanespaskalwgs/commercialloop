class Admin::AgentQuicklinksController < ApplicationController
  layout 'admin'
  require_role 'AdminUser'

  def show
    @updates_data = AgentQuicklink.first.data
  end

  def update
    @updates = AgentQuicklink.first
    @updates.build_data(params[:updates_data_key], params[:updates_data])
    @updates.save
    redirect_to :action => "show"
  end
end
