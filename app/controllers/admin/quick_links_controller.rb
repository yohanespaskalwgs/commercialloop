class Admin::QuickLinksController < ApplicationController
  layout 'admin'
  require_role 'AdminUser'

  def show
    @quicklink_data = Quicklink.first.data
  end

  def update
    @quicklink = Quicklink.first
    @quicklink.build_data(params[:quicklink_data_key], params[:quicklink_data])
    @quicklink.save
    redirect_to :action => "show"
  end
end
