class Admin::ActiveHelpPageAgentsController < ApplicationController

  layout 'admin'
  require_role 'AdminUser'
  before_filter :prepare_form, :only => [:new, :create, :edit, :update]
  before_filter :find_active_help, :except => [:new, :index, :create]

  def index
    @help_pages = ActiveHelpPage.find(:all, :conditions => "user_type = 'AgentUser'")
  end

  def new
    @help_page = ActiveHelpPage.new
  end

  def create
    @help_page = ActiveHelpPage.new(params[:help])
    @help_page.user_type = 'AgentUser'
    if @help_page.save
      flash[:notice] = "Your data has successfully created"
      redirect_to :action => "index" and return
    end

    flash.now[:error] = "Failed to created data"
    render :action => "edit"
  end

  def edit;
  end

  def update
    if @help_page.update_attributes(params[:help])
      flash[:notice] = "Your data has successfully update"
      redirect_to :action => "index" and return
    end

    flash.now[:error] = "Failed to update data"
    render :action => "edit"
  end

  def destroy
    flash[:notice] = @help_page.destroy ? "Your data has successfully deleted" : "Failed to delete data"
    redirect_to :action => "index"
  end

  private

  def find_active_help
    @help_page = ActiveHelpPage.find(params[:id])
    flash[:error] = "ID cant be found" and redirect_to :action => "index" and return unless @help_page
  end

  def prepare_form
    @pages = [['Please Select', nil],['Dashboard', 'Dashboard'],['Listings', 'Listings'],['Add Residential Sale', 'Add Residential Sale'],['Add Residential Lease', 'Add Residential Lease'],['Add Commercial', 'Add Commercial'],['Add Project Sale', 'Add Project Sale'],['Add Holiday Lease', 'Add Holiday Lease'],['Add Business Sale', 'Add Business Sale'],['Media', 'Media'],['Team Management', 'Team Management'],['Add a Team Member', 'Add a Team Member'],['Update Team Preference', 'Update Team Preference'],['Files', 'Files'],['All Messages', 'All Messages'],['Create a New Message', 'Create a New Message'],['View Unread', 'View Unread'],['View Archives'],['Offices', 'Offices'],['Add a New Office', 'Add a New Office'],['Account', 'Account']]
  end

end
