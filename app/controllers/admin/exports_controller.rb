class Admin::ExportsController < ApplicationController
  layout 'admin'
  require_role 'AdminUser'

  def show
    @portals = Portal.find(:all, :order => "portal_name ASC")
  end

  def new
    @portal = Portal.new
  end

  def edit
    @portal = Portal.find(params[:id])
  end

  def create
    respond_to do |format|
      params[:portal][:portal_name] = params[:portal][:portal_name].strip
      params[:portal][:portal_url] = params[:portal][:portal_url].strip
      @portal = Portal.new(params[:portal])
      if params[:time] != ""
        @portal.timeframe = params[:time] + " " + params[:frame]
      else
        @portal.timeframe = ""
      end
      if @portal.save
        portal_country_ids = params[:portal_country_ids]
        if portal_country_ids && portal_country_ids.size > 0
          portal_country_ids.each do |country_id|
            @portal.portal_countries << PortalCountry.create(:country_id => country_id)
          end
        end
        flash.now[:notice] = 'Portal was successfully created.'
        format.html { redirect_to :action => 'show' }
        format.xml  { render :xml => @portal, :status => :created, :location => @portal }
      else
        format.html { render :action => 'new' }
        format.xml  { render :xml => @portal.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @portal = Portal.find(params[:portal][:id])
      portal_country_ids = params[:portal_country_ids]
      params[:portal][:portal_name] = params[:portal][:portal_name].strip
      params[:portal][:portal_url] = params[:portal][:portal_url].strip
      if @portal.update_attributes(params[:portal])
        if params[:time] != ""
          @portal.timeframe = params[:time] + " " + params[:frame]
        else
          @portal.timeframe = ""
        end
        if portal_country_ids && portal_country_ids.size > 0
          @portal.portal_countries.delete_all
          portal_country_ids.each do |country_id|
            @portal.portal_countries << PortalCountry.create(:country_id => country_id)
          end
        end
        @portal.save
        unless params[:portal][:ftp_url].blank?
          portal_durl = params[:portal][:ftp_url].downcase.gsub('http://', '')
          portal_durl = portal_durl.gsub('www.', '')
          Domain.update_all("`portal_export` = #{params[:portal][:http_api_enable]}", "`name` LIKE '%#{portal_durl}%'" )
        end
        flash[:notice] = 'Portal was successfully updated.'
        format.html { redirect_to :action => 'show' }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @portal.errors, :status => :unprocessable_entity }
      end
    end
  end
end
