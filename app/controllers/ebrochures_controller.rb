class EbrochuresController < ApplicationController
  before_filter :login_required, :except => [:forward, :send_forward, :send_ebrochure, :get_image, :visit_link, :visit_property_link, :preview, :unsubcribe]
  before_filter :get_agent_and_office
  before_filter :check_hide_ebrochure, :except => [:forward, :send_forward, :send_ebrochure, :get_image, :visit_link, :visit_property_link, :preview, :unsubcribe]
  skip_before_filter :verify_authenticity_token, :except => [:index, :destroy,:forward, :sample, :send_forward, :send_ebrochure, :save_ebrochure]
  layout "agent"
  include EcampaignHelper

  def index
#    require 'json'
    session["ebrochure_id"] = session["ebrochure_status"] = session["ebrochure_layout"] = session["ebrochure_data"]  = session["ebrochure_colour"] = session["ebrochure_recipient_data"] = session["ebrochure_group_id"] = session["ebrochure_contact_checkbox"] = session["ebrochure_contact_alert_checkbox"]  = session["ebrochure_contact_name_checkbox"] = nil

    sql = Ebrochure.show_ebrochure("e.office_id = #{@office.id} ")
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    @ebrochures = @mysql_result.all_hashes
    @mysql_result.free

#    if @office.id == 66
#      ebrochure = Ebrochure.find(486)
#      if ebrochure.present?
#        @ems = []
#        @dipp = []
#        eb_json = JSON.parse(ebrochure.stored_data)
#        num1 = eb_json["ebrochure_contact_checkbox"]
#        contacts = AgentContact.find(:all, :conditions => "id IN (#{num1.join(',')})")
#        emails = contacts.map{|a| a.email}
#        emails.each do |em|
#          if @ems.include?(em)
#            @dipp << em
#          else
#            @ems << em
#          end
#        end
#        email_uniq= emails.uniq
#        Log.create(:message => "emails.count: #{emails.count}, email_uniq.count: #{email_uniq.count}, duplicated: #{@dipp.inspect}")
#      end
#    end

  end

  def stats
    sql = Ebrochure.show_ebrochure("e.id = #{params[:id]} ")
    ebrochure_stats = Ebrochure.find_by_sql(sql.to_s)
    @ebrochure_stats = ebrochure_stats[0]
    @ebrochure_property_clicks = EcampaignClick.find(:all, :conditions => "`ebrochure_id` = #{params[:id]} And `property_id` IS NOT NULL")
    @property = Property.find_by_id(@ebrochure_stats.property_id)

    unless params[:type].blank?
      @stats_type = params[:type]
      conditions = "ebrochure_id = #{params[:id]}"
      conditions +=" And email_bounced=1" if params[:type] == "bounced"
      conditions +=" And email_opened=1" if params[:type] == "views"
      conditions +=" And email_forwarded=1" if params[:type] == "forward"
      if params[:type] == "unsubcribe"
        @ecampaign_tracks = EcampaignUnsubcribe.find(:all, :select => "DISTINCT(`agent_contact_id`), ebrochure_id", :conditions=> "ebrochure_id = #{params[:id]}")
      else
        if params[:type] == "clicks"
          @ecampaign_click = EcampaignClick.find(:first, :conditions=> "ebrochure_id = #{@ebrochure_stats.id}")
          @ecampaign_tracks = EcampaignClickDetail.find(:all, :select => "DISTINCT(`agent_contact_id`), ecampaign_click_id", :conditions=>"ecampaign_click_id = #{@ecampaign_click.id}") unless @ecampaign_click.blank?
        else
          @ecampaign_tracks = EcampaignTrack.find(:all, :select => "DISTINCT(`agent_contact_id`), ebrochure_id", :conditions=> conditions)
        end
      end
    end
  end

  def edit_ebrochure
    require 'json'
    eb = Ebrochure.find_by_id(params[:id])
    if eb.stored_data.blank?
      flash[:notice] = 'Eproperty Update data doesn\'t found.'
      redirect_to agent_office_ebrochures_path(@agent, @office)
    else
      set_data(JSON.parse(eb.stored_data))
      session["ebrochure_id"] = params[:id]
      redirect_to choose_layout_agent_office_ebrochures_path(@agent, @office)
    end
  end

  def view_and_send
    require 'json'
    eb = Ebrochure.find_by_id(params[:id])
    if eb.stored_data.blank?
      flash[:notice] = 'Eproperty Update data doesn\'t found.'
      redirect_to agent_office_ebrochures_path(@agent, @office)
    else
      set_data(JSON.parse(eb.stored_data))
      session["ebrochure_id"] = params[:id]
      redirect_to view_sample_agent_office_ebrochures_path(@agent, @office)
    end
  end

  def duplicate_ebrochure
    ActiveRecord::Base.connection.execute("INSERT INTO ebrochures (`property_id`, `title`, `stored_layout`, `stored_data`, `created_at`, `updated_at`, `office_id`) SELECT `property_id`, `title`, `stored_layout`, `stored_data`, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), `office_id` FROM ebrochures WHERE id = #{params[:id]}") unless params[:id].blank?
    flash[:notice] = 'Ebrochure has successfully been duplicated.'
    redirect_to agent_office_ebrochures_path(@agent, @office)
  end

  def preview
    ebrochure = Ebrochure.find_by_id(params[:id])
    if !ebrochure.blank?
      email = ebrochure.stored_layout.gsub("{ebrochure_track_property_url}", full_base_url+visit_property_link_agent_office_ebrochure_path(@agent,@office,ebrochure)+"?track_id=#{params[:track_id]}&property_id=#{params[:property_id]}")
      email = email.gsub("{forward}", full_base_url+forward_agent_office_ebrochure_path(@agent,@office,ebrochure)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{preview}", full_base_url+preview_agent_office_ebrochure_path(@agent,@office,ebrochure)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_ebrochure_path(@agent,@office,ebrochure)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{ebrochure_track_id}", "#{params[:track_id]}")
      email_layout = email.gsub("{view_open_url}", "<img style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_ebrochures_path(@agent,@office)+"?track_id=#{params[:track_id]}&ebrochure_id=#{ebrochure.id}"}'/>")
      @html = email_layout
    else
      @html = "<center><b>Ebrochure Not Found!</b></center>"
    end
    render :layout => false
  end

  def unsubcribe
    track = EcampaignTrack.find_by_id(params[:track_id])
    unless track.blank?
      ac = track.agent_contact
      ac.inactive = true
      ac.save(false)
      ContactNote.create({:agent_contact_id => ac.id, :agent_user_id => ac.office.office_contact, :note_type_id => 22, :description => "User decided to unsubscribe through clicking option in eEbrochure", :note_date => Time.now})
      EcampaignUnsubcribe.create({:ebrochure_id => params[:id], :agent_contact_id => track.agent_contact_id})
    end
    @login_url = full_base_url
    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def visit_property_link
    unless params[:property_id].blank?
      ec  = EcampaignClick.find(:first, :conditions => "`ebrochure_id` = #{params[:id]} And `property_id` = #{params[:property_id]}")
      unless ec.blank?
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_clicks SET clicked = clicked + 1 WHERE `id`=#{ec.id}")
        unless params[:track_id].blank?
          track = EcampaignTrack.find(params[:track_id])
          EcampaignClickDetail.create({:ecampaign_click_id=> ec.id, :agent_contact_id => track.agent_contact_id})
        end
      end
      property = Property.find(params[:property_id])
      if property.detail.property_url.blank?
        if @office.url.present?
          office_url = "http://#{@office.url.gsub("http://", "")}"
          redirect_to "#{office_url}/#{params[:property_id]}"
        else
          redirect_to full_base_url+"/agents/#{property.agent_id}/offices/#{property.office_id}/properties/#{property.id}"
        end
      else
        redirect_to "http://#{property.detail.property_url.gsub("http://", "")}"
      end
    else
      redirect_to agent_office_ebrochures_path(@agent, @office)
    end
  end

  def delete_ebrochure
    unless params[:id].blank?
     ec  = Ebrochure.find_by_id(params[:id])
     unless ec.blank?
       ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_tracks WHERE ebrochure_id = #{ec.id}")
       ec.destroy
     end
    end
    flash[:notice] = 'Ebrochure Already deleted.'
    redirect_to agent_office_ebrochures_path(@agent, @office)
  end

  # ============================================
  #                    STEPS
  #=============================================
  def choose_layout
    if params[:layout]
      session["ebrochure_layout"] = params[:layout]
      save_update_ebrochure
      redirect_to campaign_copy_agent_office_ebrochures_path(@agent, @office)
    end
    @ebrochure_layout = session["ebrochure_layout"]
  end

  def campaign_copy
    redirect_to choose_layout_agent_office_ebrochures_path(@agent, @office) if session["ebrochure_layout"].blank?
    unless params[:update].blank?
      unless params[:data][:property_id].blank?
        session["ebrochure_data"] = params[:data]
        save_update_ebrochure
        redirect_to choose_colour_agent_office_ebrochures_path(@agent, @office)
      end
    else
      session["ebrochure_data"] = { "sender_email" =>"", "sender_description" =>"", "with_region" => "", "top_headline" => "", "no_readmore" => "",
        "sender_title" =>"", "status" =>"", "property_id" =>"", "opentime_type" => "", "no_price_word" => "", "large_font" => "", "no_footer" => "",
        "display_headline" =>"", "display_desc" =>"", "display_suburb" =>"", "display_address" => "", "display_ca" =>"", "ca_link" => "",
        "display_price" =>"", "display_contact" =>"", "display_land" =>"", "display_auction" =>"", "display_bond" => "",
        "display_property_id" =>"", "display_internet_id" =>"", "display_available" =>"", "display_fullsize" => "", "display_feature" => "",

        "display_opentime" =>"","display_side_opentime" =>"", "display_side_price" =>"", "display_side_bed" =>"", "display_side_bath" =>"", "display_side_car" => "",
        "display_side_contact" =>"", "display_side_land" =>"", "display_side_auction" =>"", "display_side_bond" => "",
        "display_side_property_id" =>"", "display_side_internet_id" =>"", "display_side_available" =>"", "display_side_feature" => ""
       } if session["ebrochure_data"].blank?
    end
    @data = session["ebrochure_data"]
    p = Property.find_by_id(@data["property_id"])
    @selected_listing = p.address unless p.blank?
    @contacts = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    @ebrochure_layout = session["ebrochure_layout"]
  end

  def choose_colour
    @ebrochure_layout = session["ebrochure_layout"]
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Footer"])
#    @header_image = find_image("Brochure Header")
#    @footer_image = find_image("Brochure Footer")
    @find_side_image = find_image("Side Image")
    @color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #999999 #CCCCCC #f0f0f0 #FFFFFF)
    if !params[:choose_colours].blank?
      session["ebrochure_colour"] = { "bgcolor" => params[:bgcolor], "generaltextcolor" => params[:generaltextcolor], "sidecolor" =>params[:sidecolor], "sidetextcolor" =>params[:sidetextcolor], "icon_colour" =>params[:icon_colour], "linecolor" => params[:linecolor]}
      save_update_ebrochure
      redirect_to select_recipient_agent_office_ebrochures_path(@agent, @office)
    else
      session["ebrochure_colour"] = { "bgcolor" =>"", "generaltextcolor" =>"", "sidecolor" =>"", "sidetextcolor" =>"", "icon_colour" => "", "linecolor" =>""} if session["ebrochure_colour"].blank?
    end
    @bgcolor = session["ebrochure_colour"]["bgcolor"].blank? ? "#FFFFFF": session["ebrochure_colour"]["bgcolor"]
    @generaltextcolor =  session["ebrochure_colour"]["generaltextcolor"].blank? ? "#000000" : session["ebrochure_colour"]["generaltextcolor"]
    @sidecolor =  session["ebrochure_colour"]["sidecolor"].blank? ? "#FFFFFF" : session["ebrochure_colour"]["sidecolor"]
    @sidetextcolor =  session["ebrochure_colour"]["sidetextcolor"].blank? ? "#000000" : session["ebrochure_colour"]["sidetextcolor"]
    @icon_colour =  session["ebrochure_colour"]["icon_colour"].blank? ? "black" : session["ebrochure_colour"]["icon_colour"]
    @linecolor =  session["ebrochure_colour"]["linecolor"].blank? ? "#FDCE44" : session["ebrochure_colour"]["linecolor"]
  end

  def select_recipient
    if @agent.developer.allow_ecampaign == true
      @group_contacts = GroupContact.find(:all)
    else
      @group_contacts = GroupContact.find(:all, :conditions => "office_id=#{@office.id}")
    end

    if !params[:update].blank?
      session["ebrochure_group_id"] = params[:group_ids].blank? ? "" : params[:group_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["ebrochure_contact_checkbox"] = params[:contact_checkbox]
      session["ebrochure_contact_name_checkbox"] = params[:contact_name_checkbox]
      session["ebrochure_contact_alert_checkbox"] = params[:contact_alert_checkbox]
      session["ebrochure_recipient_data"] = params[:recipient_data]
      save_update_ebrochure
      redirect_to view_sample_agent_office_ebrochures_path(@agent, @office)
    else
      session["ebrochure_recipient_data"] = {"selection_alert_type" => "", "selection_listing_type" => "",
        "selection_property_type" => "", "selection_min_bedroom" => "",
        "selection_min_bathroom" => "", "selection_min_carspace" => "",
        "min_price" => "", "max_price" => "", "search_contact_name" => ""
       } if session["ebrochure_recipient_data"].blank?
    end
    @recipient_data = session["ebrochure_recipient_data"]

    @value_group = session["ebrochure_group_id"].join("#") unless session["ebrochure_group_id"].blank?
    @value_groups = ""
    session["ebrochure_group_id"].each{|group|
      gc = GroupContact.find_by_id(group)
      @value_groups = @value_groups + '<span style="margin-right: 20px;" id="group'+group+'">'+gc.name+'<a href="javascript:remove_options(\'group\',\''+group+'\',\'group\')">x</a></span> '
    } unless session["ebrochure_group_id"].blank?

    @contact_checkbox = session["ebrochure_contact_checkbox"]
    @contact_checkbox_ids = ""
    @contact_checkbox.each{|cid|
      @contact_checkbox_ids = @contact_checkbox_ids + '#' + cid
    } unless @contact_checkbox.blank?

    @contact_name_checkbox = session["ebrochure_contact_name_checkbox"]
    @contact_name_checkbox_ids = ""
    @contact_name_checkbox.each{|cid|
      @contact_name_checkbox_ids = @contact_name_checkbox_ids + '#' + cid
    } unless @contact_name_checkbox.blank?

    @contact_alert_checkbox = session["ebrochure_contact_alert_checkbox"]
    @contact_alert_checkbox_ids = ""
    @contact_alert_checkbox.each{|cid|
      @contact_alert_checkbox_ids = @contact_alert_checkbox_ids + '#' + cid
    } unless @contact_alert_checkbox.blank?

    @alert_type = [['Newly Listed Properties', 'Newly Listed Properties'], ['Updated Properties', 'Updated Properties'], ['Open Inspections', 'Open Inspections'], ['Sold/Leased Properties', 'Sold/Leased Properties'], ['Auctions', 'Auctions'], ['Latest News', 'Latest News']]
    @listing_type = [['Business Sale', 'BusinessSale'], ['Commercial Sale/Lease', 'Commercial'], ['Residential Lease', 'ResidentialLease'],['Residential Sale', 'ResidentialSale'], ['Holiday Lease', 'HolidayLease'], ['Project Sale', 'ProjectSale']]
    @range = (1..10).map{|x|[x,x]}
  end

  def view_sample
    redirect_to send_ebrochure_agent_office_ebrochures_path(@agent, @office) if params[:send_ebrochure]
    ebrochure_data
  end

  def send_ebrochure
    ebrochure_data
    html = render_to_string(:layout => false)
    title = session["ebrochure_data"]["sender_title"]
    sender_id = session["ebrochure_data"]["sender_email"]
    property_id = session["ebrochure_data"]["property_id"]
    contact_groups = session["ebrochure_contact_checkbox"]
    contact_names = session["ebrochure_contact_name_checkbox"]
    contact_alerts = session["ebrochure_contact_alert_checkbox"]
    all_data = formated_data
    unless session["ebrochure_id"].blank?
      ebrochure = Ebrochure.find(session["ebrochure_id"])
      ebrochure.title = title
      ebrochure.stored_layout = html
      ebrochure.stored_data = all_data.to_json
      ebrochure.sent_at = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      ebrochure.save!
    end
    property = Property.find_by_id(property_id)
    contacts_ids = []
    if contact_groups.present?
      contacts_ids << contact_groups
    end
    if contact_names.present?
      contacts_ids << contact_names
    end
    if contact_alerts.present?
      contacts_ids << contact_alerts
    end
    contacts_ids.uniq!
    if ebrochure
      ec  = EcampaignClick.find(:first, :conditions => "`ebrochure_id` = #{ebrochure.id} And `property_id` = #{property_id}")
      EcampaignClick.create(:ebrochure_id => ebrochure.id, :property_id => property_id, :clicked => 0) if ec.blank?
#      sent_to = EcampaignTrack.find_all_by_ebrochure_id(ebrochure.id).map{|a| a.agent_contact_id.to_s} rescue []
      spawn(:kill => true) do
        begin
          ActiveRecord::Base.connection.execute("UPDATE ebrochures SET send_process = 1 WHERE `id`=#{ebrochure.id}")
          contacts = AgentContact.find(:all, :conditions => "id IN (#{contacts_ids.join(',')})") if contacts_ids.present?
          if contacts.present?
            @emails = []
            contacts.each{|contact|
              if contact.email.present? && !@emails.include?(contact.email)
                send_mail_ebrochure(property, contact, sender_id, ebrochure)
                @emails << contact.email
              end
            }
          end
#          contact_groups.each{|contact_group_id| send_mail_ebrochure(property, contact_group_id, sender_id, ebrochure) unless sent_to.include?(contact_group_id)} unless contact_groups.blank?
#          contact_names.each{|contact_name_id| send_mail_ebrochure(property, contact_name_id, sender_id, ebrochure) unless sent_to.include?(contact_name_id)} unless contact_names.blank?
#          contact_alerts.each{|contact_alert_id| send_mail_ebrochure(property, contact_alert_id, sender_id, ebrochure) unless sent_to.include?(contact_alert_id)} unless contact_alerts.blank?
          ActiveRecord::Base.connection.execute("UPDATE ebrochures SET send_process = 0 WHERE `id`=#{ebrochure.id}")
        rescue Exception => ex
          Log.create({:message => "LOOP ERROR send_ebrochure: ebrochure_id: #{ebrochure.id}. ERROR -->  #{ex.inspect}"})
          ActiveRecord::Base.connection.execute("UPDATE ebrochures SET send_process = 0 WHERE `id`=#{ebrochure.id}")
        end
      end
    end
    flash[:notice] = "Ebrochures Update has successfully been sent"
    redirect_to agent_office_ebrochures_path(@agent, @office)
  end

  def send_mail_ebrochure(property, contact, sender_id, ebrochure)
    contact_id = contact.id
    unless contact.blank?
    if contact.inactive != true
      ecampaign_track = EcampaignTrack.create(:ebrochure_id => ebrochure.id, :agent_contact_id => contact_id)
      if ecampaign_track
        sender = AgentUser.find_by_id(sender_id)
        if !contact.email.blank?
          begin
            EcampaignMailer.deliver_send_ebrochure(@agent, @office, property, @base_url, sender, ebrochure, ecampaign_track, contact)
          rescue Exception => ex
            ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
            Log.create({:message => "send_mail_ebrochure error : Office:#{@office.id}, Contact:#{contact.inspect}, Sender:#{sender.inspect}
                         ebrochure:#{ebrochure.inspect}. ERROR -->  #{ex.inspect}"})
          end
        else
          ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
        end
          ContactNote.create({:agent_contact_id => contact.id, :agent_user_id => sender_id, :note_type_id => 23, :description => "An Ebrochure was sent to this user", :note_date => Time.now})
          ebrochure_note_for_property(contact.id, property, sender_id)
      end
    end
    end
  end

  def sample
    @ebrochure = Ebrochure.find(params[:id])
    respond_to do |format|
      format.html { render :layout => false}
    end
  end

  def send_sample
    unless params[:id].blank?
      unless params[:email].blank?
        ebrochure_data
        html = render_to_string(:layout => false)
        @ebrochure = Ebrochure.find(params[:id])
        @ebrochure.stored_layout = html
        @ebrochure.save!

        sender = AgentUser.find_by_id(session["ebrochure_data"]["sender_email"])
        params[:email].each do |email|
        unless email.blank?
          begin
            EcampaignMailer.deliver_send_sample(sender, @ebrochure, email)
          rescue
            Log.create({:message => "Failed send sample email ebrochure #{@ebrochure.id}"})
          end
        end
        end
      end
      flash[:notice] = 'Sample Email already sent.'
    end
    redirect_to sample_agent_office_ebrochure_path(@agent,@office,@ebrochure)
  end

  def formated_data
    all_data = {
    "ebrochure_layout" => session["ebrochure_layout"],
    "ebrochure_data" => session["ebrochure_data"],
    "ebrochure_colour" => session["ebrochure_colour"],
    "ebrochure_group_id" => session["ebrochure_group_id"],
    "ebrochure_contact_checkbox" => session["ebrochure_contact_checkbox"],
    "ebrochure_contact_alert_checkbox" => session["ebrochure_contact_alert_checkbox"],
    "ebrochure_contact_name_checkbox" => session["ebrochure_contact_name_checkbox"],
    "ebrochure_recipient_data" => session["ebrochure_recipient_data"]
    }
    return all_data
  end

  def save_update_ebrochure
    all_data = formated_data
    if !session["ebrochure_id"].blank?
      ec = Ebrochure.find(session["ebrochure_id"])
      ec.title = session["ebrochure_data"]["sender_title"] unless session["ebrochure_data"].blank?
      ec.property_id = session["ebrochure_data"]["property_id"] unless session["ebrochure_data"].blank?
      ec.stored_data = all_data.to_json
      ec.save!
    else
      ec = Ebrochure.create(:office_id => @office.id, :property_id => (session["ebrochure_data"].blank? ? "": session["ebrochure_data"]["property_id"]), :title => (session["ebrochure_data"].blank? ? "": session["ebrochure_data"]["sender_title"]), :stored_data => all_data.to_json)
      session["ebrochure_id"] = ec.id
    end
  end

  def forward
    @agent_contact = AgentContact.new
    @track_id = params[:track_id]
    @ebrochure = Ebrochure.find(params[:id])

    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def send_forward
    @agent_contact = AgentContact.new(params[:agent_contact])
    @track_id = params[:track_id]
    respond_to do |format|
      @agent_contact.ecampaign_contact = true
      if @agent_contact.save
        ecampaign_track = EcampaignTrack.find_by_id(params[:track_id])
        if ecampaign_track
          spawn(:kill => true) do
            ebrochure = ecampaign_track.ebrochure
            sender = AgentContact.find_by_id(ecampaign_track.agent_contact_id)
            begin
              EcampaignMailer.deliver_send_ebrochure(@agent, @office, ebrochure.property, @base_url, sender, ebrochure, ecampaign_track, @agent_contact)
            rescue
              ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
            end
          end
        end
        #track forwarded link
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_forwarded = 1 WHERE id=#{@track_id}") unless @track_id.blank?
        flash[:notice] = 'Ebrochure Update has successfully been forwarded.'
        format.html { redirect_to :action => 'forward' }
      else
        format.html { render :action => "forward", :layout => "new_level4" }
      end
    end
  end

  def get_image
    #track opened email
    ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_opened =1 WHERE id=#{params[:track_id]}") unless params[:track_id].blank?
    image = RAILS_ROOT+"/public/images/transparant.png"
    render :text => open(image, "rb").read
  end

  def ebrochure_data
    session["ebrochure_data"] = { "sender_email" =>"", "sender_description" =>"", "with_region" => "", "top_headline" => "", "no_readmore" => "",
        "sender_title" =>"", "status" =>"", "property_id" =>"", "opentime_type" => "","no_price_word" => "", "large_font" => "", "no_footer" => "",
        "display_headline" =>"", "display_desc" =>"", "display_suburb" =>"", "display_address" => "", "display_ca" => "","ca_link" => "",
        "display_price" =>"", "display_contact" =>"", "display_land" =>"", "display_auction" =>"", "display_bond" => "",
        "display_property_id" =>"", "display_internet_id" =>"", "display_available" =>"", "display_fullsize" => "",

        "display_opentime" =>"","display_side_opentime" =>"", "display_side_price" =>"", "display_side_bed" =>"", "display_side_bath" =>"", "display_side_car" => "",
        "display_side_contact" =>"", "display_side_land" =>"", "display_side_auction" =>"", "display_side_bond" => "",
        "display_side_property_id" =>"", "display_side_internet_id" =>"", "display_side_available" =>"", "display_side_feature" => ""
       } if session["ebrochure_data"].blank?

    session["ebrochure_colour"] = { "bgcolor" =>"", "textcolor" =>"", "heading" =>"", "headingtextcolor" =>"", "thinline" =>"", "generaltext" =>"", "mainbg" =>"", "maintext" =>"", "linecolor" => "", "icon_colour" => ""} if session["ebrochure_colour"].blank?

    session["ebrochure_recipient_data"] = {"selection_alert_type" => "", "selection_listing_type" => "",
        "selection_property_type" => "", "selection_min_bedroom" => "",
        "selection_min_bathroom" => "", "selection_min_carspace" => "",
        "min_price" => "", "max_price" => "", "search_contact_name" => ""
       } if session["ebrochure_recipient_data"].blank?

    @ebrochure_layout = session["ebrochure_layout"]
    @base_url = full_base_url
    @ebrochure_id = session["ebrochure_id"]
    @ebrochure = Ebrochure.find_by_id(@ebrochure_id)
    @data = session["ebrochure_data"]
    @recipient_data = session["ebrochure_recipient_data"]
    @side_image = find_image("Side Image")
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Footer"])
#    @header_image = find_image("Brochure Header")
#    @footer_image = find_image("Brochure Footer")
    @img_nm = "p"
    @icon_colour = "#000000"
    unless session["ebrochure_colour"].blank?
      @bgcolor = session["ebrochure_colour"]["bgcolor"]
      @generaltextcolor = session["ebrochure_colour"]["generaltextcolor"]
      @sidetextcolor = session["ebrochure_colour"]["sidetextcolor"]
      @sidecolor = session["ebrochure_colour"]["sidecolor"]
      @linecolor = session["ebrochure_colour"]["linecolor"]
      @icon_colour = session["ebrochure_colour"]["icon_colour"] == "white" ? "#ffffff" : "#000000"
      @img_nm = "white_p" if session["ebrochure_colour"]["icon_colour"] == "white"
    end
    @ebrochure_status = session["ebrochure_status"]
    @property = Property.find_by_id(session["ebrochure_data"]["property_id"])

    @contact_photo1 = @property.primary_contact.portrait_image.first.public_filename rescue ''
    @contact_photo2 = @property.secondary_contact.portrait_image.first.public_filename rescue ''

    unless @property.blank?
      unless @property.images.blank?
        obj_images = @property.images
        @main_image = obj_images.first.public_filename unless obj_images.first.blank?
        @thumb_image1 = obj_images.second.public_filename(:thumb) unless obj_images.second.blank?
        @thumb_image2 = obj_images.third.public_filename(:thumb) unless obj_images.third.blank?
        @thumb_image3 = obj_images[3].public_filename(:thumb) unless obj_images[3].blank?
      end
    end
  end

  def set_data(db_data)
    session["ebrochure_layout"] = db_data["ebrochure_layout"]
    session["ebrochure_data"] = db_data["ebrochure_data"]
    session["ebrochure_colour"] = db_data["ebrochure_colour"]
    session["ebrochure_group_id"] = db_data["ebrochure_group_id"]
    session["ebrochure_contact_checkbox"] = db_data["ebrochure_contact_checkbox"]
    session["ebrochure_contact_alert_checkbox"] = db_data["ebrochure_contact_alert_checkbox"]
    session["ebrochure_contact_name_checkbox"] = db_data["ebrochure_contact_name_checkbox"]
    session["ebrochure_recipient_data"] = db_data["ebrochure_recipient_data"]
    session["ebrochure_status"] = "edit"
  end

  def search_contact
    jsons = []

    if params[:search_contact_name].blank?
      unless params[:selection_group_id].blank?
        group_ids = params[:selection_group_id].split("#").delete_if{|x| x.blank?}
        gc = GroupContactRelation.find(:all, :select => "DISTINCT(agent_contact_id)", :conditions => {:group_contact_id => group_ids})
      end

      unless params[:search_alert].blank?
        condition = "1=1"
        condition += " And alert_type = '#{params[:selection_alert_type]}' " unless params[:selection_alert_type].blank?
        condition += " And listing_type = '#{params[:selection_listing_type]}' " unless params[:selection_listing_type].blank?
        condition += " And property_type = '#{params[:selection_property_type]}' " unless params[:selection_property_type].blank?
        condition += " And min_bedroom = '#{params[:selection_min_bedroom]}' " unless params[:selection_min_bedroom].blank?
        condition += " And min_bathroom = '#{params[:selection_min_bathroom]}' " unless params[:selection_min_bathroom].blank?
        condition += " And min_carspace = '#{params[:selection_min_carspace]}' " unless params[:selection_min_carspace].blank?
        condition += " And min_price = '#{params[:selection_min_price]}' " unless params[:selection_min_price].blank?
        condition += " And max_price = '#{params[:selection_max_price]}' " unless params[:selection_max_price].blank?

        gc = ContactAlert.find(:all, :select => "DISTINCT(agent_contact_id)", :conditions => condition)
      end

      if !gc.blank?
        gc.each do |ac|
          unless ac.agent_contact.blank?
            allow_office = false
            if @agent.developer.allow_ecampaign == true
              allow_office = true
            else
              allow_office = true if ac.agent_contact.office_id == params[:office_id].to_i
            end
            if allow_office == true
              name = "#{(ac.agent_contact.first_name.blank? ? "No Name" : ac.agent_contact.first_name)} #{(ac.agent_contact.last_name.blank? ? "No Name" : ac.agent_contact.last_name)}"
              if ac.agent_contact.first_name.blank? && ac.agent_contact.last_name.blank?
                name = ac.agent_contact.email
              end
              jsons << {'id' => ac.agent_contact.id, 'name' => name} if ac.agent_contact.inactive != true
            end
          end
        end
        text = {"results" => jsons, "total" => (gc ? gc.length : 0) }
      else
        text = ''
      end
    else
      condition = " And office_id = #{params[:office_id]}" unless @agent.developer.allow_ecampaign == true
      ag = AgentContact.find(:all, :conditions => "(`first_name` LIKE '%#{params[:selection_contact_name]}%' OR `last_name` LIKE '%#{params[:selection_contact_name]}%') #{condition}")
      if !ag.blank?
        ag.each do |ac|
          unless ac.blank?
            name = "#{(ac.first_name.blank? ? "No Name" : ac.first_name)} #{(ac.last_name.blank? ? "No Name" : ac.last_name)}"
            jsons << {'id' => ac.id, 'name' => name} if ac.inactive != true
          end
        end
        text = {"results" => jsons, "total" => (ag ? ag.length : 0) }
      else
        text = ''
      end
    end

    render :json => text
  end

  def find_image(type)
    get_header_footer(@office.id, type)
  end

  def ebrochure_note_for_property(contact_id, property, sender_id)
    ad = [ property.unit_number, property.street_number, property.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
    address = [ad, property.suburb ].join(", ")
    note = {:property_id => property.id, :agent_contact_id => contact_id, :agent_user_id => sender_id,
            :note_type_id => 24,:address => address, :description => "Property Alert sent via eCampaign", :note_date => Time.now, :duplicate => 0}
    property_note = PropertyNote.new(note)
    property_note.save!

    ContactNote.create({:agent_contact_id => contact_id, :agent_user_id => sender_id, :note_type_id => 1, :description => "Property Alert sent via eBrochure", :note_date => Time.now, :address => address, :property_id => property.id})
  end

  def populate_prop
    types = []
    unless params[:ltype].blank?
      country = Country.find_by_name(@office.country)
      country_ptypes_id = []
      country_ptypes = CountryPropertyTypeRelationship.find_all_by_country_id(country.id,:select =>"property_type_id")
      country_ptypes.each do |c|
        country_ptypes_id << c.property_type_id
      end
      condition=[' 1=1']
      unless country_ptypes_id.blank?
        condition[0] += " AND id IN(?)"
        condition << country_ptypes_id
      end
      category = "Residential" if params[:ltype].include?('Residential') || params[:ltype].include?("Project")
      category = "Holiday" if params[:ltype].include?('Holiday')
      category = "Commercial" if params[:ltype].include?('Commercial')
      category = "Business" if params[:ltype].include?('Business')
      unless category.blank?
        condition[0] += " AND category like(?)"
        condition << category
      end
      types = PropertyType.find(:all,:conditions =>condition).map{|t| [t.name,t.name] }
    end
    return(render(:json => types))
  end

  def check_hide_ebrochure
    if current_user.allow_property_owner_access? && @office.hide_ebrochure
      render_optional_error_file(401)
    end
  end

  def filter_email_and_send(contacts,sender,emarketing)
    unless contacts.blank?
      @emails = []
      contacts2 = AgentContact.find(:all, :conditions => "id IN (#{contacts.join(',')})")
      contacts2.each{|contact|
        if contact.email.present? && !@emails.include?(contact.email)
          send_mail_emarketing(contact.id, sender, emarketing)
          @emails << contact.email
        end
      }
    end
  end
end
