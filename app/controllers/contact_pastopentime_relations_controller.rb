class ContactPastopentimeRelationsController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  before_filter :prepare_delete, :only => [:destroy, :destroy_attendee]
  append_before_filter :prepare_creation, :except => [:destroy_attendee]
  skip_before_filter :verify_authenticity_token, :only => :destroy

  def index
     @contact_pastopentime_relation = ContactPastopentimeRelation.new
     @agent_contact = @contact_pastopentime_relation.build_agent_contact
  end

  def show
    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def new
  end

  def edit
  end

  def create
    create_contact
    @contact_pastopentime_relation = ContactPastopentimeRelation.new(params[:contact_pastopentime_relation])
    respond_to do |format|
      if @contact_pastopentime_relation.save
        @pastopentime.update_attributes({:total_attended => @pastopentime.contact_pastopentime_relations.size})
        ChangesLog.create_log(@property, @contact_pastopentime_relation, @contact_pastopentime_relation.attributes, current_user, @office, 1)
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
  end

  def destroy
    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def destroy_attendee
    @property = Property.find(params[:property_id])
    redirect_to agent_office_property_opentimes_path(@agent, current_office, @property)
  end

  def prepare_delete
    @contact_pastopentime_relation = ContactPastopentimeRelation.find(params[:id])
    pastopentime = PastOpentime.find(@contact_pastopentime_relation.past_opentime_id)
    @contact_pastopentime_relation.destroy
    pastopentime.update_attributes({:total_attended => pastopentime.contact_pastopentime_relations.size})
  end

  def prepare_creation
    @property = Property.find(params[:property_id])
    @pastopentime = PastOpentime.find(params[:past_opentime_id])
    @heard_froms = HeardFrom.find(:all, :conditions => ["`status` = 'heard_property'"]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @attended_users = @pastopentime.contact_pastopentime_relations
  end

  def create_contact
    category_id =  (@property.type == 'ResidentialLease')? 11 : 8

    team = []
    team << @property.primary_contact_id unless @property.primary_contact_id.nil?
    team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?

    params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id])
    @agent_contact = AgentContact.new(params[:agent_contact])
    if @agent_contact.save
      @agent_contact.add_categories([category_id])
      @agent_contact.add_assigns(team)
      @agent_contact.add_access(team)

      #note type = Open Inspection Attendee
      if params[:contact_pastopentime_relation][:contract].to_i == 1
        PropertyViewer.create({:property_id => @property.id, :property_viewer_type_id => 8 ,:date => Time.now})
        @agent_contact.create_note(@property.id, "Take Contract/Application", 12, current_user.id)
      end
      note_id = @agent_contact.create_note(@property.id, "Attend Open Inspection", 5, current_user.id)

      #note type for property = Open Inspection Attendee
      @agent_contact.create_note_for_property(@property.id, "", 5, params[:contact_pastopentime_relation][:heard_from_id], current_user.id)

      #note type for property = Open Inspection Attendee and take contract
      @agent_contact.create_note_for_property(@property.id, "Take Contract/Application", 16, params[:contact_pastopentime_relation][:heard_from_id], current_user.id) if params[:contact_pastopentime_relation][:contract].to_i == 1

      #alert Alerts Sent
      alert_id = alertnote_id = nil
      if params[:contact_pastopentime_relation][:alert].to_i == 1
        rs_alert = @agent_contact.create_alert(@property.id, current_user.id)
        alert_id = rs_alert[:alert_id]
        alertnote_id = rs_alert[:alert_note_id]
      end
      @agent_contact.use_spawn_for_boom
      @agent_contact.use_spawn_for_md
      @agent_contact.use_spawn_for_irealty
      params[:contact_pastopentime_relation] = params[:contact_pastopentime_relation].merge(:past_opentime_id => params[:past_opentime_id], :agent_contact_id => @agent_contact.id, :contact_note_id => note_id, :contact_alert_id => alert_id, :contact_alertnote_id => alertnote_id, :duplicate => 0)
    end
  end

  def update_duplicate
    duplicate = 0
    detected = nil
    params[:agent_contact] = params[:agent_contact].merge(:office_id => params[:office_id])

    %w(email mobile_number contact_number work_number home_number).each{|w|
      unless params[:agent_contact][w].blank?
        check = AgentContact.check_exist(params[:agent_contact], w)
        unless check.nil?
          detected = check
          duplicate = 1
        end
      end
    }

    if duplicate == 1
      unless detected.nil?
        @agent_contact = AgentContact.find(detected.id)
          if @agent_contact.update_attributes(params[:agent_contact].delete_if{|x,y| y.blank?})
              category_id =  (@property.type == 'ResidentialLease')? 11 : 8
              @agent_contact.update_category_contact(category_id)
              description = "Attend Open Inspection"
              if params[:contact_pastopentime_relation][:contract].to_i == 1
                description = description + " + Take Contract/Application"
                PropertyViewer.create({:property_id => @property.id, :property_viewer_type_id => 8 ,:date => Time.now})
              end
              note_id = @agent_contact.create_note(@property.id, description, 4, current_user.id)
              alert_id = alertnote_id = nil
              if params[:contact_pastopentime_relation][:alert].to_i == 1
                rs_alert = @agent_contact.create_alert(@property.id, current_user.id)
                alert_id = rs_alert[:alert_id]
                alertnote_id = rs_alert[:alert_note_id]
              end
            params[:contact_pastopentime_relation] = params[:contact_pastopentime_relation].merge(:past_opentime_id => params[:past_opentime_id], :agent_contact_id => @agent_contact.id, :contact_note_id => note_id, :contact_note_id => note_id, :contact_alert_id => alert_id, :duplicate => 0)
            @contact_pastopentime_relation = ContactPastopentimeRelation.new(params[:contact_pastopentime_relation])
            @pastopentime.update_attributes({:total_attended => @pastopentime.contact_pastopentime_relations.size}) if @contact_pastopentime_relation.save
          end
      end
    end
    redirect_to agent_office_property_past_opentime_contact_pastopentime_relations_path(@agent,@office, @property, @pastopentime)
  end
end
