class MailMergesController < ApplicationController
  layout "agent"
  before_filter :login_required
  prepend_before_filter :check_office_ownership
  before_filter :load_property
  prepend_before_filter :get_agent_and_office

  def index
    @contacts = AgentContact.find(:all, :conditions => "office_id = #{@office.id} and inactive is not true")
    @contacts = @contacts.present? ? @contacts.map{|a| [a.first_name, a.id]}.unshift(["Please Select", ""]) : [["Please Select", ""]]
    @doc_format = [["Please select",""],["doc - Word 97 Document", "doc"], ["docx - Word 2010 Document", "docx"], ["odt - Open Office Document", "odt"], ["pdf - PDF Document", "pdf"]]
    @docs = MailMerge.find(:all, :conditions => "office_id = #{@office.id} and deleted is not true")
    @docs = @docs.present? ? @docs.map{|a| [a.title, a.attachment_id]}.unshift(["Please Select", ""]) : [["Please Select", ""]]
#    deleted = MailMerge.find(:all, :conditions => "office_id = #{@office.id} and deleted is true")
  end

  def upload_doc
    if current_user.full_access?
      document = UserFile.new(:uploaded_data => params[:document][:uploaded_data])
      document.attachable_id = @office.id
      document.attachable_type = "Office"
      document.category = "MailMerge"
      document.title = params[:document][:title]
      name = document.filename
      if [".doc",".odt"].include?(name[name.length-4..name.length-1]) || [".docx"].include?(name[name.length-5..name.length-1])
        if document.save
          if @property.present?
            MailMerge.create(:attachment_id => document.id, :office_id => @office.id, :property_id => @property.id, :title => params[:document][:title])
          else
            MailMerge.create(:attachment_id => document.id, :office_id => @office.id, :title => params[:document][:title])
          end
          flash[:notice] = 'Letter successfully uploaded.'
        else
          Log.create(:message => "Mail merge template upload err: #{document.errors.inspect}")
        end
      else
        flash[:notice] = 'File format is not supported.'
      end
    else
      flash[:notice] = 'Only level1 user is allowed to upload letter.'
    end
    redirect
  end

  def create
    if params[:delete].present?
      if current_user.full_access?
        letter = MailMerge.find(:first, :conditions => "office_id = #{@office.id} and attachment_id = #{params[:document][:id]}") rescue ''
        if letter.present?
          letter.destroy 
          uploaded_doc = Attachment.find(params[:document][:id]) rescue ''
          uploaded_doc.destroy if uploaded_doc.present?
          flash[:notice] = 'Letter successfully deleted.'
        end
      else
        flash[:notice] = 'Only level1 user allowed to delete letter.'
      end
      redirect
    else
      contacts = params[:contact_ids].present? ? params[:contact_ids].split(",").delete_if{|a| a.blank?}.join(",") : ""
      uploaded_doc = Attachment.find(params[:document][:id]) rescue ''
      if contacts.present? && params[:document][:id].present? && params[:document][:format].present?
         if @property.present?
          redirect_to "http://www.agentaccount.com/mailmerge/index.php?attachment=#{params[:document][:id]}&contacts=#{contacts}&format=#{params[:document][:format]}&property_id=#{params[:property_id]}&office_id=#{@office.id}&filename=#{uploaded_doc.filename}&commercialloop=1"
         else
          redirect_to "http://www.agentaccount.com/mailmerge/index.php?attachment=#{params[:document][:id]}&contacts=#{contacts}&format=#{params[:document][:format]}&office_id=#{@office.id}&filename=#{uploaded_doc.filename}&commercialloop=1"
         end
#         redirect
      else
        redirect
      end
    end
  end

  def load_property
    if params[:property_id].present?
      @property = Property.find(params[:property_id])
    end
  end

  def redirect
    if @property.present?
      redirect_to agent_office_property_mail_merges_path(@agent,@office,@property)
    else
      redirect_to agent_office_mail_merges_path(@agent, @office)
    end
  end

end

