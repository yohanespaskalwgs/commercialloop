class ConfigurationsController < ApplicationController
  layout 'agent'
  prepend_before_filter :login_required
  before_filter :get_agent_and_office

  def index
    @emarketing_brochure_header = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Header"])
    @emarketing_brochure_footer = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Footer"])
    @pdf_brochure_header = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
    @pdf_brochure_footer = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
  end

  def create
    respond_to do |format|
      unless params[:emarketing_brochure_header].blank? || params[:emarketing_brochure_header][:uploaded_data].blank?
        @emarketing_brochure_header = Image.new(params[:emarketing_brochure_header])
        old_emarketing_brochure_header = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Header"])
        @emarketing_brochure_header.dimension = Image::BROCHURE
        @emarketing_brochure_header.attachable = @office
        @emarketing_brochure_header.description = "Emarketing Brochure Header"
        if @emarketing_brochure_header.save
          old_emarketing_brochure_header.try(:destroy)
          flash[:notice] = "Emarketing Brochure Header was successfuly uploaded"
          @office.reload
        else
          flash[:notice] = 'Upload failed.' + @emarketing_brochure_header.errors.to_s
        end
        format.html {redirect_to :action => 'index'}
      end
      unless params[:emarketing_brochure_footer].blank? || params[:emarketing_brochure_footer][:uploaded_data].blank?
        @emarketing_brochure_footer = Image.new(params[:emarketing_brochure_footer])
        old_emarketing_brochure_footer = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Footer"])
        @emarketing_brochure_footer.dimension = Image::BROCHURE
        @emarketing_brochure_footer.attachable = @office
        @emarketing_brochure_footer.description = "Emarketing Brochure Footer"
        if @emarketing_brochure_footer.save
          old_emarketing_brochure_header.try(:destroy)
          flash[:notice] = "Emarketing Brochure Footer was successfuly uploaded"
          @office.reload
        else
          flash[:notice] = 'Upload failed.' + @emarketing_brochure_footer.errors.to_s
        end
        format.html {redirect_to :action => 'index'}
      end
      unless params[:pdf_brochure_header].blank? || params[:pdf_brochure_header][:uploaded_data].blank?
        @pdf_brochure_header = Image.new(params[:pdf_brochure_header])
        old_pdf_brochure_header = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
        @pdf_brochure_header.dimension = Image::BROCHURE
        @pdf_brochure_header.attachable = @office
        @pdf_brochure_header.description = "PDF Brochure Header"
        if @pdf_brochure_header.save
          old_pdf_brochure_header.try(:destroy)
          flash[:notice] = "PDF Brochure Header was successfuly uploaded"
          @office.reload
        else
          flash[:notice] = 'Upload failed.' + @pdf_brochure_header.errors.to_s
        end
        format.html {redirect_to :action => 'index'}
      end
      unless params[:pdf_brochure_footer].blank? || params[:pdf_brochure_footer][:uploaded_data].blank?
        @pdf_brochure_footer = Image.new(params[:pdf_brochure_footer])
        old_pdf_brochure_footer = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
        @pdf_brochure_footer.dimension = Image::BROCHURE
        @pdf_brochure_footer.attachable = @office
        @pdf_brochure_footer.description = "PDF Brochure Footer"
        if @pdf_brochure_footer.save
          old_pdf_brochure_footer.try(:destroy)
          flash[:notice] = "PDF Brochure Footer was successfuly uploaded"
          @office.reload
        else
          flash[:notice] = 'Upload failed.' + @pdf_brochure_footer.errors.to_s
        end
        format.html {redirect_to :action => 'index'}
      end
    end
  end

end
