class DeveloperUsersController < ApplicationController
  layout 'developer'

  require_role 'DeveloperUser'

  before_filter :get_developer, :except => [:toggle_active_help, :close_button_per_page]
  before_filter :get_user, :only => %w(edit update destroy)

  def index
    @users = @developer.developer_users
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Team Member'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
    respond_to do |format|
      format.html
    end
  end

  def new
    @user = DeveloperUser.new

    respond_to do |format|
      format.html
      format.xml { render :xml => @user }
    end
  end

  def toggle_active_help
    render :update do |page|
      current_user.toggle_activate_help!
      page.replace_html :active_help_area, :partial => 'layouts/active_help'
      page.call :toggle_active_help, current_user.active_help_status
    end
  end

  def close_button_per_page
    render :update do |page|
      current_user.close_per_page(params[:id])
      page.call :toggle_active_help, false
    end
  end

  def create
    @user = @developer.developer_users.build params[:user]

    respond_to do |format|
      if @user.save
        if !params[:avatar].blank? || !params[:avatar][:uploaded_data].blank?
          @logo = Image.new params[:avatar]
          old_logo = @user.avatar
          @logo.dimension = Image::AVATAR
          @logo.attachable = @user
          if @logo.save
            old_logo.try(:destroy)
            flash.now[:notice] = 'Avatar was successfully uploaded.'
            @user.reload
          else
            p @logo.errors
            flash.now[:notice] = 'Upload failed.'
          end
          format.html {redirect_to :action => 'index'}
        end
        flash[:notice] = 'Team member was successfully created.'
        format.html { redirect_to :action => 'index' }
        format.xml  { render :xml => @user, :status => :created, :location => @user }
        UserMailer.deliver_signup_member_notification(@user) unless params[:notification].blank?
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit

    respond_to do |format|
      format.html
      format.xml { render :xml => @user }
    end
  end

  def update

    respond_to do |format|
      if params[:avatar].blank? || params[:avatar][:uploaded_data].blank?
        parameters = params[:user]
        parameters = parameters.delete_if{|x,y| x=='password'} if params[:user][:password_confirmation].blank?
        if @user.update_attributes(params[:user])
          flash[:notice] = 'Team member was successfully updated.'
          format.html { redirect_to :action => 'index' }
          format.xml  { head :ok }
        else
          format.html { render :action => "edit" }
          format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
        end
      else # upload avatar
        @logo = Image.new params[:avatar]
        old_logo = @user.avatar
        @logo.dimension = Image::AVATAR
        @logo.attachable = @user
        if @logo.save
          old_logo.try(:destroy)
          flash.now[:notice] = 'Avatar was successfully uploaded.'
          @user.reload
        else
          flash.now[:notice] = 'Upload failed.'
        end
        format.html {render :action => 'edit'}
      end
    end
  end

  def destroy
    ActiveRecord::Base.connection.execute("UPDATE users SET deleted_at = '#{Time.now.strftime("%Y-%m-%d")}' WHERE id = #{@user.id}")

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { head :ok }
    end
  end

  private

  def get_developer
    @developer = current_user.class.name.include?("AdminUser") ? Developer.find_by_id(params[:developer_id]) : current_user.developer
    unless current_user.full_access? && params[:developer_id] == @developer.id.to_s
      render_optional_error_file(401) and return false
    end
  end

  def get_user
    @user = @developer.developer_users.find params[:id]
  end

end
