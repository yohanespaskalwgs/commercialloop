class UserDefinedFeaturesController < ApplicationController
  layout false

    require_role ['AgentUser','DeveloperUser']

  before_filter :get_agent_and_office

  # GET /user_defined_features
  # GET /user_defined_features.xml
  def index
    @user_defined_features = @office.user_defined_features.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @user_defined_features }
    end
  end

  # GET /user_defined_features/1
  # GET /user_defined_features/1.xml
  def show
    @user_defined_feature = @office.user_defined_features.find(params[:id])

    respond_to do |format|
      format.html
      format.xml  { render :xml => @user_defined_feature }
    end
  end

  # GET /user_defined_features/new
  # GET /user_defined_features/new.xml
  def new
    @user_defined_feature = @office.user_defined_features.build
    session[:ptype] = params[:ptype]
    respond_to do |format|
      format.html
      format.xml  { render :xml => @user_defined_feature }
    end
  end

  # GET /user_defined_features/1/edit
  def edit
    @user_defined_feature = @office.user_defined_features.find(params[:id])

    respond_to do |format|
      format.html { render :layout =>false} # edit.html.erb
      format.xml  { render :xml => @user_defined_feature }
    end
  end

  # POST /user_defined_features
  # POST /user_defined_features.xml
  def create
    @user_defined_feature = @office.user_defined_features.build(params[:user_defined_feature])

    respond_to do |format|
      if @user_defined_feature.save
        flash[:notice] = 'User Defined Feature was successfully created.'
        format.html { redirect_to agent_office_user_defined_features_path(@agent,@office)  }
        format.xml  { render :xml => @user_defined_feature, :status => :created, :location => @user_defined_feature }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user_defined_feature.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /user_defined_features/1
  # PUT /user_defined_features/1.xml
  def update
    @user_defined_feature = @office.user_defined_features.find(params[:id])

    respond_to do |format|
      if @user_defined_feature.update_attributes(params[:user_defined_feature])
        flash[:notice] = 'User Defined Feature was successfully updated.'
        format.html { redirect_to agent_office_user_defined_feature_path(@agent,@office,@user_defined_feature) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user_defined_feature.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /user_defined_features/1
  # DELETE /user_defined_features/1.xml
  def destroy
    @user_defined_feature = @office.user_defined_features.find(params[:id])
    @user_defined_feature.destroy

    respond_to do |format|
      format.html { redirect_to agent_office_user_defined_features_path(@agent,@office) }
      format.xml  { head :ok }
    end
  end

end
