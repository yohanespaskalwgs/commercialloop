class TasksController < ApplicationController
  include TasksHelper

  layout 'agent'

  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  before_filter :new_task, :only => [:index, :create]
  before_filter :get_task, :only => [:edit, :update, :destroy, :update_task_status]
  before_filter :date_range, :only => [:populate_combobox, :view_listing]
  append_before_filter :prepare_creation

  def index
    destroy_session_listing if params[:sort_by].blank? && params[:filter].blank?
  end

  def show;end

  def new;end

  def edit
    @task.task_date = Time.now.strftime("%m/%d/%Y") if @task.task_date.blank?
  end

  def create
    @task.task_date = Time.now if params[:task][:task_date].blank?
    @task.office_id = @office.id

    respond_to do |format|
      if @task.save
        @task.create_offer
        flash[:notice] = 'Task was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
    respond_to do |format|
      if @task.update_attributes(params[:task])
        flash[:notice] = 'Task was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @task.destroy

    respond_to do |format|
      flash[:notice] = 'Task was successfully deleted'
      format.html { redirect_to :action => 'index'}
    end
  end

  def update_task_status
    if @task.update_attribute(:completed, params[:completed])
      render :json => @task.as_json
    else
      render :json => @task.errors.full_messages, :status => :unprocessable_entity
    end
  end

  def populate_task_types
    @task_types = TaskType.find(:all, :conditions => ["office_id is NULL OR office_id = ?", @office.id]).map{|c| [c.name, c.id]}
    render :json => @task_types
  end

  def populate_combobox
    start_date, end_date = build_monthly_date_range(date_range.first, date_range.last) rescue [nil, nil]

    statuses = [
      {
        :status => "all",
        :statusText => "All Status"
      },
      {
        :status => "completed",
        :statusText => "Completed"
      },
      {
        :status => "incomplete",
        :statusText => "Incomplete"
      }
    ]

    return render :json => {
      :results => @office.tasks.length,
      :statuses => statuses,
      :startDates => start_date.unshift({:date=>'all', :dateText=>"Beginning of Time"}),
      :endDates => end_date.unshift({:date=>'all', :dateText=>"End of Time"})
    }
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if session[:id].present?
        condition = ["tasks.id = ? AND tasks.office_id = ?", session[:id], current_office.id]
      else
        get_session_listing
        condition = [" tasks.office_id = ? "]
        condition << current_office.id

        if (session[:status] == "completed")
          condition[0] += " AND tasks.completed = ?"
          condition <<  true
        elsif (session[:status] == "incomplete")
          condition[0] += " AND tasks.completed = ?"
          condition <<  false
        end

        condition[0] += " AND tasks.task_date BETWEEN ? AND ?"

        if session[:start_date] == "all" || session[:start_date].blank?
          condition << date_range.first
        else
          condition << session[:start_date].to_date
        end

        if session[:end_date] == "all" || session[:end_date].blank?
          condition << date_range.last
        else
          condition << session[:end_date].to_date
        end
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1

    order = Task.build_sorting(params[:sort], params[:dir])

    @tasks = Task.paginate(:all, :include => [:agent_user, :agent_contact, :task_type], :conditions => condition, :order => order, :page => params[:page], :per_page => params[:limit].to_i)

    return render :json => {
      :results => Task.count(:conditions => condition),
      :rows => @tasks.map{|t| t.as_json }
    }
  rescue Exception => ex
    Log.create(:message => "Task View Listings ---> errors : #{ex.inspect} ")
    return render :json => {:results => nil, :rows => nil}
  end

  protected

  def new_task
   @task = Task.new(params[:task])
  end

  def get_task
   @task = Task.find(params[:id])
  end

  def prepare_creation
    @task_types = TaskType.all.map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @team_members = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    @tasks = @office.tasks.paginate(:all, :order => "`created_at` DESC", :page =>params[:page], :per_page => 30)
  end

  def date_range
    date_range = @office.tasks.find(:all, :conditions => ["task_date IS NOT NULL"], :order => "task_date ASC").map{|t| t.task_date}
  end

  def get_session_listing
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:start_date] = params[:start_date].blank? ? "all" : params[:start_date]
    session[:end_date] = params[:end_date].blank? ? "all" : params[:end_date]
    session[:id] = params[:id].blank? ? nil : params[:id]
    session[:sort_by] = "tasks.updated_at DESC"
  end

  def destroy_session_listing
    session[:status] = nil
    session[:start_date] = nil
    session[:end_date] = nil
    session[:id] = nil
    session[:sort_by] = nil
  end

end
