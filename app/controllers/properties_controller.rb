class PropertiesController < ApplicationController
  layout 'agent'
  before_filter :login_required,:except =>[:property_ipn,:property_notification]

  prepend_before_filter :check_office_ownership,:except =>[:property_ipn,:property_notification]
  prepend_before_filter :get_agent_and_office
  before_filter :check_permission_create, :only => [:residential_lease, :residential_sale, :holiday_lease, :commercial, :commercial_building, :project_sale, :business_sale, :new_development, :land_release, :general_sale, :livestock_sale, :clearing_sales_event, :clearing_sale]
  before_filter :check_permission_edit, :only => [:edit]
  before_filter :check_level3, :only => [:vendor_detail]
  before_filter :check_export_permission, :only =>[:exports]
  #  before_filter :check_level4, :only => [:exports, :preview, :brochure]
  before_filter :check_hide_exports , :only => [:exports]
  before_filter :check_hide_brochure , :only => [:brochure]
  before_filter :check_hide_preview , :only => [:preview]
  before_filter :check_permission_page_property, :only =>[:edit,:status,:exports]
  # prepend_check
  append_before_filter :prepare_creation, :only => [:residential_lease, :residential_sale, :holiday_lease, :commercial, :commercial_building, :project_sale, :business_sale, :new_development, :land_release, :general_sale, :livestock_sale, :clearing_sales_event, :clearing_sale]
  skip_before_filter :verify_authenticity_token, :only => [:property_ipn,:search, :sub_categories,:property_notification, :auto_complete, :auto_complete_sale_property, :auto_complete_lease_property, :auto_complete_external_agent, :populate_agent, :sql_search]

  include ActionView::Helpers::TextHelper
  include Geokit::Geocoders
  include PropertiesHelper

  # GET /properties
  # GET /properties.xml

  def property_ipn
    @property = Property.find(params[:id])
    unless params[:payment_status].blank?
      @property.update_attribute("payment_status", params[:payment_status])
      @property.update_attribute("status", @office.property_owner_access && @office.property_draft_for_new ? 6 : 1)
    end
    if request.post?
      flash[:notice] = "Property Payment Status is  #{params[:payment_status]}." unless params[:payment_status].blank?
      redirect_to(agent_office_path(@agent,@office))
    else
      render :nothing => :true
    end
  end

  def property_notification
    begin
      @property = Property.find_by_sql("SELECT * FROM properties WHERE id = #{params[:id]}").first
      unless params[:processed].blank?
        arr = params[:token].split("_")
        token = arr[0]
        sender_uid = arr[1] unless arr[1].blank?
        domain_id = arr[2] unless arr[2].blank?
        if params[:processed].to_i == 1
          ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE property_id = #{params[:id]} And domain_id = #{domain_id} And sender_uid = #{sender_uid}")
        end
        GLOBAL_ATTR.delete_if{|k,v| v == token}
        return render(:text => "Property with id : #{@property.id} is successfully updated")
      else
        return render(:text => "false")
      end
    rescue
      return render(:text => "false")
    end
  end

  def import_callback
    @property = Property.find_by_sql("SELECT * FROM properties WHERE id = #{params[:id]}").first
    unless @property.blank?
      if @property.update_attributes({:updated_at => Time.now})
        return render(:text => "Property with id : #{@property.id} is successfully updated")
      else
        return render(:text => "false")
      end
    else
      return render(:text => "false")
    end
  end

  def index
    flash[:notice] = "Property Payment Status is  #{params[:payment_status]}." unless params[:payment_status].blank?
    get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    @properties_filter = Property.find(:all,:include => [:primary_contact],:conditions=>["office_id=?",current_office.id], :select => "suburb,id, property_type,status,primary_contact_id");
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Listings' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?

    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def listing
    session[:sort_by] = "properties.updated_at DESC"
    if params[:type] != "all_listing"
      type = get_property_type params[:type]
      @properties = @office.properties.paginate_by_type type ,:include =>[:agent], :page => params[:page] == "" ? 1 : params[:page], :per_page => 20 ,:order =>session[:sort_by]
      @properties_filter = Property.find(:all,:conditions=>["office_id=? AND status = 1",current_office.id]);
      respond_to do |format|
        format.html {render :action => 'index'}
        format.js
      end
    else
      @properties = @office.properties.paginate :all ,:include =>[:agent], :page => params[:page] == "" ? 1 : params[:page], :per_page => 50 ,:order =>session[:sort_by]
      @properties_filter = Property.find(:all,:conditions=>["office_id=? AND status = 1",current_office.id]);
      respond_to do |format|
        format.html {render :action => 'index'}
        format.js
      end
    end
  end

  # GET /properties/1
  # GET /properties/1.xml
  def show
    redirect_to :action => "edit"
  end

  def residential_lease
    if params[:id].blank?
      if params[:action] == "residential_lease"
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Residential Lease' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @price_per = [["Monthly", Property::METRIC_MONTHLY], ["Weekly", Property::METRIC_WEEKLY], ["Quarterly", Property::METRIC_QARTERLY], ["Yearly", Property::METRIC_YEARLY]]
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @feature_ids = @property.feature_ids
    end
  end

  def residential_sale
    if params[:id].blank?
      if params[:action] == 'residential_sale'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Residential Sale' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @periods = %w(Week Month Quarter Year).map{|s| "Per #{s}"}.unshift(['Please select', nil])
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @feature_ids = @property.feature_ids
    end
  end

  def general_sale
    if params[:id].blank?
      if params[:action] == 'general_sale'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add General Sale' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @periods = %w(Week Month Quarter Year).map{|s| "Per #{s}"}.unshift(['Please select', nil])
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @feature_ids = @property.feature_ids
    end
  end

  def livestock_sale
    if params[:id].blank?
      if params[:action] == 'livestock_sale'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Livestock Sale' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @periods = %w(Week Month Quarter Year).map{|s| "Per #{s}"}.unshift(['Please select', nil])

      @sale_types = LivestockSaleType.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @sale_type_auction_id = LivestockSaleType.auction_type_id

      @livestock_types = LivestockType.build_select_options
      @livestock_sexes = LivestockSex.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_pregnancies = LivestockPregnancy.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_frames = LivestockFrame.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_conditions = LivestockCondition.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_qualities = LivestockQuality.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_horn_types = LivestockHorn.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_tick_statuses = LivestockTickStatus.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])

      @livestock_hgp_options = [["Please select", nil], ["Yes - Treated", true], ["No - Non Treated", false]]
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @feature_ids = @property.feature_ids
    end
  end

  def clearing_sales_event
    if params[:id].blank?
      if params[:action] == 'clearing_sales_event'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Clearing Sales Event' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @periods = %w(Week Month Quarter Year).map{|s| "Per #{s}"}.unshift(['Please select', nil])

      @clearing_sales_event_gst = [["Yes", true], ["No", false]].unshift(['Please select', nil])
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @feature_ids = @property.feature_ids
    end
  end

  def clearing_sale
    if params[:id].blank?
      if params[:action] == 'clearing_sale'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Clearing Sale' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @periods = %w(Week Month Quarter Year).map{|s| "Per #{s}"}.unshift(['Please select', nil])
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @feature_ids = @property.feature_ids
    end
  end

  def holiday_lease
    if params[:id].blank?
      if params[:action] == 'holiday_lease'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Holiday Lease' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @price_per_day = ['Please select', 'Nightly', 'Weekly', 'Monthly']
      @minimum_stay = (1..13).map{|i| pluralize(i, 'night')}.push(%w(2 3 4 8).map{|i| "#{i} weeks"}).push(%w(3 6 12).map{|i| "#{i} months"}).flatten!
      init_rental_seasons
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @feature_ids = @property.feature_ids
      @minimum_stay = (1..13).map{|i| pluralize(i, 'night')}.push(%w(2 3 4 8).map{|i| "#{i} weeks"}).push(%w(3 6 12).map{|i| "#{i} months"}).flatten!
    end
  end

  def commercial
    if params[:id].blank?
      if params[:action] == 'commercial'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Commercial' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end

      @ptypes = PropertyType.commercial.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @deal_types =  [["Sale & Lease", "Both"], ["Sale", "Sale"], ["Lease", "Lease"]].map{|k, v| [k, v]}.unshift(['Please select', nil])
      @lease_years = (1..10).map{|i| i}.unshift('Please select')
      @tax_options = [['Yes', true], ['No', false], ['N/A', nil], ['Please select', nil]]
      @options_bool = [['Please select',nil], ['Yes', true], ['No', false]]
      @price_period = ['Please select', 'Weekly', 'Monthly', 'Annually']
      @price_per = [["Monthly", Property::METRIC_MONTHLY], ["Weekly", Property::METRIC_WEEKLY], ["Quarterly", Property::METRIC_QARTERLY], ["Yearly", Property::METRIC_YEARLY]]
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @deal_types =  [["Sale & Lease", "Both"], ["Sale", "Sale"], ["Lease", "Lease"]].map{|k, v| [k, v]}.unshift(['Please select', nil])
    end
  end

  def commercial_building
    if params[:id].blank?
      if params[:action] == 'commercial_building'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Building' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.commercial.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @ptypes = PropertyType.commercial.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    end
  end

  def project_sale
    if params[:id].blank?
      if params[:action] == 'project_sale'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Project Sale' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @categories = ['Land Packages', 'House Packages', 'House & Land Packages', 'Apartment Packages']
      @periods = %w(Week Month Quarter Year).map{|s| "Per #{s}"}.unshift(['Please select', nil])
      @length_metrics = ['Metres', 'Feet', 'Yards'].map{|l| [l, l]}.unshift(['Please select', nil])
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @categories = ['Land Packages', 'House Packages', 'House & Land Packages', 'Apartment Packages']
      @ptypes = PropertyType.residential.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @feature_ids = @property.feature_ids
    end
  end

  def business_sale
    if params[:id].blank?
      if params[:action] == 'business_sale'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Business Sale' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.business.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @presimes = [['Please select',''], 'No Premises', 'Leased', 'Business Owned']
      @lease_years = (1..10).map{|i| i.to_s}.unshift(['Please select',''])
      @tax_options = ['Please select', 'whole', '2', '3', '4', '5']
      @tax_options_bool = [['Yes', true], ['No', false], ['N/A', nil], ['Please select', nil]]
      @franchise_options = [["Please select",''], ['No','No'], ['New Franchise','New Franchise'], ['Franchise Resale','Franchise Resale']]
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @ptypes = PropertyType.business.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
      @presimes = [['Please select',''], 'No Premises', 'Leased', 'Business Owned']
      @tax_options = ['Please select', 'whole', '2', '3', '4', '5']
      @tax_options_bool = [['Yes', true], ['No', false], ['N/A', nil], ['Please select', nil]]
      @lease_years = (1..10).map{|i| i.to_s}.unshift(['Please select',''])
      @franchise_options = [["Please select",''], ['No','No'], ['New Franchise','New Franchise'], ['Franchise Resale','Franchise Resale']]
    end
  end

  def new_development
    if params[:id].blank?
      if params[:action] == 'new_development'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add New Development' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.business.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    else
      @property = Property.find(params[:id])
      @detail = @property.detail
      @feature_ids = @property.feature_ids
    end
  end

  def land_release
    if params[:id].blank?
      if params[:action] == 'land_release'
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add Land Release' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
      end
      @ptypes = PropertyType.business.map{|t| [t.name,t.name]}.unshift(['Please select', nil])
    else
      @property = Property.find(params[:id])
      @feature_ids = @property.feature_ids
      @detail = @property.detail
    end
  end

  def update_area
    areas = {:floor_area => params[:floor_area].to_f, :floor_area_metric => params[:floor_area_metric], :porch_terrace_area => params[:porch_terrace_area].to_f, :porch_terrace_area_metric => params[:porch_terrace_area_metric], :garage_area => params[:garage_area].to_f, :garage_area_metric => params[:garage_area_metric]}
    render :inline => "#{ProjectSaleDetail.new(areas).total_area} Square Metres"
  end

  def sub_categories
    @sub_categories = PropertyType.find_all_by_category(params[:category])
    unless @sub_categories.blank?
      render :json =>@sub_categories.map{|t| [t.name]}
    else
      render :json =>[]
    end
  end

  def search_country
    location = GoogleGeocoder.geocode(params[:id])
    unless location.blank?
      render :json =>[location.lat,location.lng,location.accuracy]
    else
      render :json =>[]
    end
  end

  # GET /properties/1/edit
  def edit
    @property = Property.find_by_id_and_office_id params[:id], params[:office_id], :include => [:detail]
    # @property = @office.properties.find params[:id], :include => [:detail]
    if @property.detail.blank?
      Log.create({:message => "Property Detail not found for Id #{@property.id}"})
      render_optional_error_file(404) and return
    end
    session[:parent_listing_id] = @property.parent_listing_id
    convert_to_i
    @feature_ids = @property.feature_ids
    @ptype = @property.type.to_s.underscore
    @property.description= decoded_and_converted_to(@property.description)
    prepare_creation
    self.send @ptype

    case @ptype
    when 'business_sale'
      types = PropertyType.business.map{|t| t.name}
    when 'residential_sale'
      types = PropertyType.residential.map{|t| t.name }
    when 'residential_lease'
      types = PropertyType.residential.map{|t| t.name}
    when 'commercial'
      types = PropertyType.commercial.map{|t| t.name}
    when 'commercial_building'
      types = PropertyType.commercial.map{|t| t.name}
    when 'project_sale'
      types = PropertyType.residential.map{|t| t.name}
    when 'holiday_lease'
      types = PropertyType.residential.map{|t| t.name}
    when 'livestock_sale'

      @sale_types = LivestockSaleType.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @sale_type_auction_id = LivestockSaleType.auction_type_id

      @livestock_types = LivestockType.build_select_options
      @livestock_sexes = LivestockSex.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_pregnancies = LivestockPregnancy.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_frames = LivestockFrame.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_conditions = LivestockCondition.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_qualities = LivestockQuality.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_horn_types = LivestockHorn.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])
      @livestock_tick_statuses = LivestockTickStatus.all.map {|s| [ s.name, s.id ] }.unshift(['Please select', nil])

      @livestock_hgp_options = [["Please select", nil], ["Yes - Treated", true], ["No - Non Treated", false]]
    when 'clearing_sales_event'
      @clearing_sales_event_gst = [["Yes", true], ["No", false]].unshift(['Please select', nil])
    when 'clearing_sale'
    end

    unless types.blank?
      types.each do |type|
        arr_type = type.split(' ')
        if @property.property_type == "#{arr_type[0]}#{arr_type[1]}"
          @property.property_type = type
        end
      end
    end

    unless @property.agent_contact.blank?
      @property.vendor_first_name = @property.agent_contact.first_name
      @property.vendor_last_name = @property.agent_contact.last_name
      @property.vendor_email = @property.agent_contact.email
      @property.vendor_phone = @property.agent_contact.contact_number
      @property.home_number = @property.agent_contact.home_number
      @property.work_number = @property.agent_contact.work_number
      @property.mobile_number = @property.agent_contact.mobile_number
    end
    @contact_name = @property.agent_contact.blank? ? "" : @property.agent_contact.full_name

    unless @property.external_agent_contact.blank?
      @property.external_agent_first_name = @property.external_agent_contact.first_name
      @property.external_agent_last_name = @property.external_agent_contact.last_name
      @property.external_agent_email = @property.external_agent_contact.email
      @property.external_agent_phone = @property.external_agent_contact.contact_number
      @property.external_agent_home_number = @property.external_agent_contact.home_number
      @property.external_agent_work_number = @property.external_agent_contact.work_number
      @property.external_agent_mobile_number = @property.external_agent_contact.mobile_number
    end
    @external_agent_name = @property.external_agent_contact.blank? ? "" : @property.external_agent_contact.full_name

    respond_to do |format|
      format.html {render "edit_#{@ptype}"}
    end
  end

  # GET /properties/1/preview
  def preview
    # @property = @office.properties.find params[:id], :include => [:detail]
    @property = Property.find_by_id_and_office_id params[:id], params[:office_id], :include => [:detail]
    @ptype = @property.type.to_s.underscore
    @detail = @property.detail
    #added by winarti@kiranatama.com, to make reusable variable
    @images = @property.images
    @primary_contact = @property.primary_contact
    @secondary_contact = @property.secondary_contact
    @third_contact = @property.third_contact
    @display_address = @property.display_address
    @address = @property.address
    @vendor_name = [@property.vendor_first_name, @property.vendor_last_name]
    @agent_contact = @property.agent_contact
    @features = @property.features
    @features_label = [
      {:name => "InternalFeature", :text => "Internal"},
      {:name => "ExternalFeature", :text => "External"},
      {:name => "SecurityFeature", :text => "Security"},
      {:name => "GeneralFeature", :text => "General"},
      {:name => "LocationFeature", :text => "Location"},
      {:name => "LifestyleFeature", :text => "Lifestyle"},
      {:name => "UserDefinedFeature", :text => "Your features"},
    ]
    @property_size = [@property.size_from, @property.size_to]
    @pre_defined_features = Feature::ALL_PRE_DEFINED.inject([]) { |ary, name| ary << Kernel.const_get(name).send(@ptype) }
    @dev_detail = Property.find(@detail.project_development_id).detail rescue nil
    @parent_listing = @property.parent_listing
    @design_type = @detail.project_design_type rescue nil
    @style = @detail.project_style rescue nil
    #end of reusable variable added by winarti@kiranatama.com
    @feature_ids = @property.features
    if @property.display_address == 0
      @property.street = ""
      @property.street_number = ""
    end
    begin
      @property.description= BlueCloth.new(decoded_and_converted_to(@property.description)).to_html
    rescue
    end
    self.send @ptype
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])

    respond_to do |format|
      if @ptype == "commercial"
        format.html { render :file => "/properties/preview_commercial", :layout => "iframe"}
      elsif @ptype == "commercialbuilding"
        format.html { render :file => "/properties/preview_commercial_building", :layout => "iframe" }
      else
        format.html { render :layout => 'iframe' }
      end
    end
  end

  def vendor_detail
    # @property = @office.properties.find params[:id], :include => [:detail]
    @property = Property.find_by_id_and_office_id params[:id], params[:office_id], :include => [:detail]
    @agent_user = @property.agent_user
    @group = AgentUser::GROUP.collect{|group| group}.unshift(['Please select', nil])
    if request.put?
      @agent_user.group = !params[:agent_user][:group].blank? ? params[:agent_user][:group] : @agent_user.group
      begin
        @agent_user.generate_sequential_code if @agent_user.code.blank?
      rescue Exception => ex
        v = @office.agent_users.length
        @agent_user.code = ("%03d" % v)
      end

      @agent_user.attributes = params[:agent_user]
      if @agent_user.changes.present?
        filtered_changes = @agent_user.changes.reject{|k,v| v[0].blank? && v[1].blank?}
        user_changes = {"agent_user" => filtered_changes} if filtered_changes.present?
      end

      if @agent_user.save
        @properties = Property.find(:all,:conditions =>["agent_user_id = ? OR primary_contact_id = ? OR secondary_contact_id = ?",@agent_user.id,@agent_user.id,@agent_user.id])
        unless @properties.blank?
          @properties.each do |property|
            property.update_attribute("updated_at", Time.now) if property.status.to_i == 1 || property.status.to_i == 5
          end
        else
          flash[:notice] = 'User was successfully updated but Properties failed to update'
        end
        ChangesLog.create_log(@property, @agent_user, user_changes, current_user, @office, 2) if user_changes.present?
        flash[:notice] = 'User was successfully updated.'
      else
        flash[:error_hash] = {}
        @agent_user.errors.each do |x,y|
          if x == "login"
            x = "username"
          end
          flash[:error_hash][x.humanize] = y.downcase
        end
      end
    end
    respond_to do |format|
      format.html
    end
  end

  # PUT /properties/1
  # PUT /properties/1.xml
  def update
    params[:property][:feature_ids] ||= []
    # @property = @office.properties.find params[:id], :include => [:detail]
    @property = Property.find_by_id_and_office_id params[:id], params[:office_id], :include => [:detail]
    last_status = @property.status
    previous_agent_user_id = @property.agent_user_id
    previous_agent_contact_id = @property.agent_contact_id
    previous_extrernal_agent_id = @property.external_agent_contact_id
    session[:parent_listing_id] = @property.parent_listing_id unless @property.parent_listing_id.blank?
    is_send = false
    @property.description= decoded_and_converted_to(@property.description)
    @ptype = @property.type.to_s.underscore
    @property.detail.unavailable_date = params[:unavailable_date] if @property.detail.class.name.underscore == "holiday_lease_detail"
    if @property.detail.respond_to?(:property_type)
      @property.detail.property_type = (params[:property][:property_type].blank? ? @property.property_type : params[:property][:property_type])
    end
    @property.detail.country = @property.country if @property.detail.respond_to?(:country)
    @property.detail.deal_type = @property.deal_type if @property.detail.respond_to?(:deal_type)
    params["#{@ptype}_detail"]["auction_date"] = change_date_format(params["#{@ptype}_detail"]["auction_date"]) if @property.detail.respond_to?(:auction_date)
    params["#{@ptype}_detail"]["date_available"] = change_date_format(params["#{@ptype}_detail"]["date_available"]) if @property.detail.respond_to?(:date_available)
    params["#{@ptype}_detail"]["lease_commencement"] = change_date_format(params["#{@ptype}_detail"]["lease_commencement"]) if @property.detail.respond_to?(:lease_commencement)
    params["#{@ptype}_detail"]["date"] = change_date_format(params["#{@ptype}_detail"]["date"]) if @property.detail.respond_to?(:date)
    if @property.detail.respond_to?(:lease_end)
      params["#{@ptype}_detail"]["lease_end"] = change_date_format(params["#{@ptype}_detail"]["lease_end"]) if @property.detail.class.name.underscore == "business_sale_detail"
    end
    if @property.detail.respond_to?(:auction_time)
      if !params["auction_hour"].blank? && !params["auction_min"].blank?
        params["#{@ptype}_detail"]["auction_time"]= "#{params["auction_hour"]}:#{params["auction_min"]}"
      else
        params["#{@ptype}_detail"]["auction_time"] = nil
      end
    end
    if @property.detail.respond_to?(:time)
      if !params["hour"].blank? && !params["min"].blank?
        params["#{@ptype}_detail"]["time"]= "#{params["hour"]}:#{params["min"]}"
      else
        params["#{@ptype}_detail"]["time"] = nil
      end
    end
    unless params[:property][:country].blank?
      if params[:property][:country].downcase == "new zealand"
        params[:property][:re_suburb] = params[:property][:suburb] if params[:property][:re_suburb].blank?
        params[:property][:rca_suburb] = params[:property][:suburb] if params[:property][:rca_suburb].blank?
        params[:property][:re_state] = params[:property][:state] if params[:property][:re_state].blank?
        params[:property][:rca_state] = params[:property][:state] if params[:property][:rca_state].blank?
        params[:property][:re_postcode] = params[:property][:zipcode] if params[:property][:re_postcode].blank?
        params[:property][:rca_postcode] = params[:property][:zipcode] if params[:property][:rca_postcode].blank?
      end
    end

    respond_to do |format|
      begin
        Property.transaction do
          if @property.is_a?(HolidayLease) && params[:rental_season]
            @property.rental_seasons.destroy_all
            save_rental_seasons
          end
          @property.attributes = params[:property]

          heading_chage = @property.headline_changed?
          desc_change = @property.description_changed?

          if @office.edit_alert && current_user.allow_property_owner_access? && (heading_chage || desc_change)
            @property.status = 6
            @property.role_6_mode = true
            is_send =true
          end

          if current_user.allow_property_owner_access? and (@office.property_owner_access and @office.property_draft_for_updated)
            @property.status = 6
            @property.role_6_mode = true
            is_send =true
          end

          if params[:property][:save_status] == Property::DRAFT
            @property.status = 6
            is_send = false
          end

          if @office.edit_alert && current_user.allow_property_owner_access?
            unless heading_chage || desc_change
              is_send = false
            end
          end

          if current_office.property_owner_access
            @property.agent_user_id =  previous_agent_user_id
          else
            if params[:property].include?('primary_contact_id')
              @property.agent_user_id = params[:property][:primary_contact_id]
            else
              @property.agent_user_id =  previous_agent_user_id
            end
          end

          @property.office = current_office
          @property.agent = current_agent
          if @property.detail.is_a?(CommercialDetail)
            @property.detail.deal_type = @property.deal_type
          end

          if(last_status != 1 && @property.status == 1)
            @property.activation = true
          else
            @property.activation = nil
          end

          #          unless params[:parent_category].blank? && params[:parent_listing_id].blank?
          #            @property.update_attributes(:is_children_listing => true)
          #          end

          if @property.type == "ResidentialSale"
            if params[:residential_sale_detail][:number_of_floors] != @property.detail.number_of_floors
              @property.update_attributes(:number_of_floors => params[:residential_sale_detail][:number_of_floors])
            end
          end

          if @property.type == "ResidentialLease"
            if params[:residential_lease_detail][:number_of_floors] != @property.detail.number_of_floors
              @property.update_attributes(:number_of_floors => params[:residential_lease_detail][:number_of_floors])
            end
          end

          if @property.type == "HolidayLease"
            if params[:holiday_lease_detail][:number_of_floors] != @property.detail.number_of_floors
              @property.update_attributes(:number_of_floors => params[:holiday_lease_detail][:number_of_floors])
            end
          end

          if @property.type == "Commercial"
            if params[:commercial_detail][:number_of_floors] != @property.detail.number_of_floors
              @property.update_attributes(:number_of_floors => params[:commercial_detail][:number_of_floors])
            end
          end

          if @property.type == "GeneralSale"
            if params[:general_sale_detail][:number_of_floors] != @property.detail.number_of_floors
              @property.update_attributes(:number_of_floors => params[:general_sale_detail][:number_of_floors])
            end
          end

          if @property.type == "LivestockSale"
            if params[:livestock_sale_detail][:number_of_floors] != @property.detail.number_of_floors
              @property.update_attributes(:number_of_floors => params[:livestock_sale_detail][:number_of_floors])
            end
          end

          ActiveRecord::Base.connection.execute("UPDATE properties SET deactivation = NULL WHERE id = #{@property.id}") if @property.status != 6
          @property.detail.attributes = params["#{@ptype}_detail"]
          ChangesLog.create_property_log(@office, @property, current_user, nil, 2)
          save_detail = @property.detail.save
          save_property = @property.save

          if !@property.vendor_first_name.blank? and !@property.vendor_last_name.blank? and @property.agent_contact.blank?
            tmp_contact_id = create_contact
            contact_id = tmp_contact_id.blank? ? 'NULL' : tmp_contact_id
            ActiveRecord::Base.connection.execute("UPDATE properties SET agent_contact_id = #{contact_id} WHERE id = #{@property.id}")
            @property.agent_contact.create_note(@property.id, "Property Listed", 19, current_user.id) unless @property.agent_contact.blank?
          else
            (@property.update_contact_detail if previous_agent_contact_id == @property.agent_contact.id) unless @property.agent_contact.blank?
          end

          if !@property.external_agent_first_name.blank? and !@property.external_agent_last_name.blank? and @property.external_agent_contact.blank?
            tmp_external_agent_id = create_external_agent
            external_agent_id = tmp_external_agent_id.blank? ? 'NULL' : tmp_external_agent_id
            ActiveRecord::Base.connection.execute("UPDATE properties SET external_agent_contact_id = #{external_agent_id} WHERE id = #{@property.id}")
          else
            (@property.update_external_agent_detail if previous_extrernal_agent_id == @property.external_agent_contact.id) unless @property.external_agent_contact.blank?
          end

          if save_property && save_detail
            #            unless @property.parent_listing_id.blank?
            #              @property.update_attributes(:is_children_listing => "1")
            #              parent_listing = Property.find(@property.parent_listing_id)
            #              parent_listing.update_attributes(:is_have_children_listing => "1")
            #            end
            #            if !@property.parent_listing_id.blank? || @property.parent_listing_id != ""
            #              unless session[:parent_listing_id].blank?
            #                if(session[:parent_listing_id] != params[:property][:parent_listing_id])
            #                  @previous_parent_listing = Property.find(session[:parent_listing_id])
            #                  @current_parent_listing = Property.find(params[:property][:parent_listing_id])
            #
            #                  @children_listing_count = Property.find(:all, :conditions => ["parent_listing_id = ? AND deleted_at IS NULL", session[:parent_listing_id]]).count
            #                  if @children_listing_count < 1 || @children_listing_count == 0
            #                    @previous_parent_listing.update_attributes(:is_have_children_listing => "0")
            #                  end
            #                  @current_parent_listing.update_attributes(:is_have_children_listing => "1")
            #                end
            #              end
            #            end
            spawn(:kill => true) do
              ExportError.destroy_all({:property_id => @property.id}) unless @property.export_errors.blank?
            end
          end

          raise 'Failed to save property' unless save_property
          raise 'Failed to save property detail' unless save_detail

        end

        # holiday_lease_duplicate if params[:holiday_duplication]
        convert_to_i
        flash[:notice] = 'Property was successfully updated.'

        if @office.enable_send_email == true
          users = @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
          users.each do |user|
            begin
              UserMailer.deliver_listing_notification_for_level1(user, current_user, @office, @property) if is_send
            rescue Exception => ex
              Log.create(:message => "Listing notif for level1 failed, error:#{ex.inspect}, user:#{user.inspect}, current_user:#{current_user.inspect}, office:#{@office.id}, prop:#{@property.id}")
            end
          end
        end

        format.html do
          session[:update_listing] = "#{@property.type.to_s.underscore}_#{@property.id}"
          session[:parent_listing_id] = nil
          redirect_to(agent_office_property_path(@agent,@office,@property) || edit_agent_office_property_path(@agent,@office,@property) )
        end

      rescue
        convert_to_i
        @feature_ids = @property.feature_ids
        prepare_creation
        @error_messages = @property.errors.full_messages.delete_if {|x| x == "Detail is invalid"} + @detail.errors.full_messages
        self.send @ptype
        @property.description= decoded_and_converted_to(@property.description)
        format.html { render "edit_#{@ptype}" }
      end
    end
  end

  # DELETE /properties/1
  # DELETE /properties/1.xml
  def destroy
    @property = Property.find(params[:id])
    @property.destroy

    respond_to do |format|
      format.html { redirect_to agent_office_properties_path(@agent,@office) }
      format.xml  { head :ok }
    end
  end

  def update_child_property_address
    unless params[:property_id].blank?
      @property = Property.find(params[:property_id])
    else
      @property = Property.new(params[:property])
    end
    @parent_listing = Property.find(:first, :conditions => ["properties.agent_id = ? AND properties.office_id = ? AND properties.id = ?", @agent.id, @office.id, params[:property_parent_listing_id]])
    @help_data = ActiveHelp.first.data
    #    @countries = Country.find(:all,:conditions=>["display_country=?",true])
    @countries = Country.find(:all,:conditions=>["display_country=?",true]).map{|c| [c.name, c.name]}.unshift(['Please select', nil])
    @country ||= Country.find_by_name(@office.country)
    @ptype = @property.type.to_s.underscore
    render :layout => false
  end

  def get_agent_contact_purchaser
    $controller = params[:controllah]
    $property = Property.find(params[:property_id])
    unless params[:purchaser_agent_contact_id].blank?
      $agent_contact = AgentContact.find(params[:purchaser_agent_contact_id])
    end
    render :layout => false
  end

  def search
    with ={}
    with = with.merge(:primary_contact_id  => current_user.id) if current_user.limited_client_access?
    params[:search_value].gsub!("/"," ") if params[:search_value].present?
    properties = Property.quick_search(params[:search_value], {:office_id => current_office.id}, with)
    jsons = []
    unless properties.blank?
      properties.each{|p|
        unless p.blank?
          ad = [ p.unit_number, p.street_number, p.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
          address = [ad, p.suburb ].join(", ")
          jsons << {'id' => p.id, 'address' => "#{address unless p.address.blank?}",'suburb' =>"#{p.suburb}",'type' =>"#{abbrv_properties(p.type)}",'price' =>"$#{p.price.to_i}",'status' =>"#{p.status_name}", 'href' => agent_office_properties_path(@agent, @office)+"?ID=#{p.id}"}
        end
      } unless properties.nil?
      text = {"results" => jsons, "total" => (properties ? properties.length : 0) }
    else
      text = ''
    end
    render :json => text
  end

  def sql_search
    if params[:search_value].present?
      search_keys = params[:search_value].split(' ')
      street_name = search_keys[1..search_keys.length].join(" ") rescue ""
      properties = Property.find(:all, :conditions => "office_id = #{@office.id} and (id LIKE '%#{params[:search_value]}%' OR street LIKE '%#{street_name.present? ? street_name : params[:search_value]}%' OR state LIKE '%#{params[:search_value]}%' OR suburb LIKE '%#{params[:search_value]}%')")
    end

    jsons = []
    unless properties.blank?
      properties.each{|p|
        unless p.blank?
          ad = [ p.unit_number, p.street_number, p.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
          address = [ad, p.suburb ].join(", ")
          jsons << {'id' => p.id, 'address' => "#{address unless p.address.blank?}",'suburb' =>"#{p.suburb}",'type' =>"#{abbrv_properties(p.type)}",'price' =>"$#{p.price.to_i}",'status' =>"#{p.status_name}", 'href' => agent_office_properties_path(@agent, @office)+"?ID=#{p.id}"}
        end
      } unless properties.nil?
      text = {"results" => jsons, "total" => (properties ? properties.length : 0) }
    else
      text = ''
    end
    render :json => text
  end

  def assign
    @property = Property.find(params[:id])
    if current_office.property_owner_access && current_office.property_with_owner_details && current_user.allow_property_owner_access? #access_level == 4
      @contacts = @office.agent_users.find(:all, :conditions => ["id =?", current_user.id]).map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    else
      if current_office.property_owner_access
        contacts = @office.agent_users.find(:all, :conditions => "roles.id = 4", :include => "roles")
        @contacts = contacts.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
      else
        @contacts = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
      end

      if current_office.property_with_owner_details
        contacts = @office.agent_users.find(:all, :conditions => "roles.id = 6", :include => "roles")
        @contacts = contacts.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
      end
    end
    respond_to do |format|
      format.html { render :layout => false }
    end
  end

  def update_assign
    # @property = @office.properties.find params[:id], :include => [:detail]
    @property = Property.find_by_id_and_office_id params[:id], params[:office_id], :include => [:detail]
    previous_agent_user_id = @property.agent_user_id
    @property.attributes = params[:property]
    @property.update_attribute(:updated_at,Time.now)

    if current_office.property_owner_access
      @property.agent_user_id =  previous_agent_user_id
    else
      if params[:property].include?('primary_contact_id')
        @property.agent_user_id = params[:property][:primary_contact_id]
      else
        @property.agent_user_id =  previous_agent_user_id
      end
    end

    @property.update_attribute(:secondary_contact_id,params[:property][:secondary_contact_id])
    flash[:notice] = 'Property was successfully updated.'
    session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-assign_#{@property.primary_contact.full_name}"
    render :layout => false
  end

  def gmap
    @property = Property.find(params[:id])
    if request.post?
      @property.update_attributes(params[:property])
      session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_map-1"
      respond_to do |format|
        format.html { render :layout => false }
      end
    else
      respond_to do |format|
        format.js { render :layout => false }
        format.html { render :layout => false }
      end
    end
  end

  def photos
    @property = Property.find(params[:id])
    respond_to do |format|
      format.html {    render :partial => "media/photos", :layout => 'iframe.html.erb' }
    end

  end

  def floorplans
    @property = Property.find(params[:id])
    respond_to do |format|
      format.html {    render :partial => "media/plans", :layout => 'iframe.html.erb' }
    end
  end

  def brochure
    @property = Property.find(params[:id])
    respond_to do |format|
      format.html {  render :layout => 'iframe.html.erb' }
    end
  end

  def price
    @property = Property.find(params[:id])
    @periods = %w(Week Month Quarter Year).map{|s| "Per #{s}"}.unshift('Please select')
    @tax_options = [['Yes', true], ['No', false], ['N/A', nil], ['Please select', nil]]
    @options_bool = [['Please select',nil], ['Yes', true], ['No', false]]
    @price_periods = ['Please select', 'Weekly', 'Monthly', 'Annually'].map { |i| [i,i]}
    @tax_options_bool = [['Yes', true], ['No', false], ['N/A', nil], ['Please select', nil]]
    @price_per = [["Monthly", Property::METRIC_MONTHLY], ["Weekly", Property::METRIC_WEEKLY], ["Quarterly", Property::METRIC_QARTERLY], ["Yearly", Property::METRIC_YEARLY]]
    convert_to_i
    unless params[:come_from].blank?
      data = params[:come_from].split("/")
      params[:come_from]= price_agent_office_property_path(@agent, @office, params[:id], :come_from => params[:come_from]) if data.last == "view_listing"
    end
    respond_to do |format|
      format.html { render :layout => false }
    end
  end

  def update_price
    # @property = @office.properties.find params[:id], :include => [:detail]
    @property = Property.find_by_id_and_office_id params[:id], params[:office_id], :include => [:detail]
    @ptype = @property.type.to_s.underscore
    @property.attributes = params[:property]
    @property.save
    @property.detail.update_attributes(params["#{@ptype}_detail"])
    flash[:notice] = 'Property was successfully updated.'
    session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-price_#{@property.price.to_i}"
    render :layout => false
  end

  def ticket
    @property = Property.find(params[:id])
    @message = Message.new
    find_agent
    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  def status
    session[:close_popup] = false
    @property = Property.find(params[:id])
    @offers = @property.offers
    @method_of_sale = ['For Sale','Auction', 'Sale by Negotiation', 'Tender', 'Expression of Interest', 'Offers to Purchase'].map{|c| [c, c]}.unshift(["Please select", nil])
    @sales_person = AgentUser.find(:all, :conditions => "office_id = '#{@office.id}'").map{|x|[x.full_name.blank? ? x.first_name : x.full_name,x.id]}.unshift(['Please select', nil])

    if @property.valid_transactions == true
      @sales_record = @property.sales_records.find(:first, :conditions => "is_deleted = '0'")
      unless @sales_record.blank?
        # Transaction
        @method_of_sale = @sales_record.method_of_sale
        @investment = @sales_record.investment
        @fee = @sales_record.fee.blank? ? "" : @sales_record.fee
        @date = @sales_record.date.blank? ? "" : @sales_record.date
        @amount = @sales_record.amount.blank? ? "" : @sales_record.amount
        @display_price = @sales_record.display_price.blank? ? "" : @sales_record.display_price
        @settlement_date = @sales_record.settlement_date.blank? ? "" : @sales_record.settlement_date
        @office_note = @sales_record.office_note.blank? ? "" : @sales_record.office_note
        @transaction_sales_person = @sales_record.transaction_sales_person.blank? ? "" : @sales_record.transaction_sales_person
        @external_agent_company = @sales_record.external_agent_company.blank? ? "" : @sales_record.external_agent_company
        @external_agent_sales_person = @sales_record.external_agent_sales_person.blank? ? "" : @sales_record.external_agent_sales_person
        @conjunction_fee = @sales_record.conjunction_fee.blank? ? "" : @sales_record.conjunction_fee
        @scale_of_fee = @sales_record.scale_of_fee.blank? ? "" : @sales_record.scale_of_fee
        @contract_link = @sales_record.contract_link rescue nil
        # End Transaction
      end
    end

    if @property.valid_tenancies == true
      @tenancy_record = @property.tenancy_records.find(:first, :conditions => "is_deleted = '0'")
      unless @tenancy_record.blank?
        # Tenancy
        @company = @tenancy_record.agent_contact.blank? ? "" : (@tenancy_record.agent_contact.agent_company.blank? ? "" : (@tenancy_record.agent_contact.agent_company.company.blank? ? "" : @tenancy_record.agent_contact.agent_company.company))
        @contact = @tenancy_record.agent_contact.blank? ? "" : (@tenancy_record.agent_contact.first_name.blank? ? "" : @tenancy_record.agent_contact.full_name)
        @phone = @tenancy_record.agent_contact.blank? ? "" : (@tenancy_record.agent_contact.contact_number.blank? ? "" : @tenancy_record.agent_contact.contact_number)
        @mobile = @tenancy_record.agent_contact.blank? ? "" : (@tenancy_record.agent_contact.mobile_number.blank? ? "" : @tenancy_record.agent_contact.mobile_number)
        @email = @tenancy_record.agent_contact.blank? ? "" : (@tenancy_record.agent_contact.email.blank? ? "" : @tenancy_record.agent_contact.email)
        @rent = @tenancy_record.rent.blank? ? "" : @tenancy_record.rent
        @net_gross = @tenancy_record.net_gross.blank? ? "" : @tenancy_record.net_gross
        @outgoing = @tenancy_record.outgoing.blank? ? "" : @tenancy_record.outgoing
        @total_rent_psm = @tenancy_record.total_rental_psm.blank? ? "" : @tenancy_record.total_rental_psm
        @total_rent_pa = @tenancy_record.total_rental_pa.blank? ? "" : @tenancy_record.total_rental_pa
        @lease_term = @tenancy_record.lease_term.blank? ? "" : @tenancy_record.lease_term
        @options = @tenancy_record.option.blank? ? "" : @tenancy_record.option
        @start_date = @tenancy_record.start_date.blank? ? "" : @tenancy_record.start_date
        @end_date = @tenancy_record.end_date.blank? ? "" : @tenancy_record.end_date
        @tenancy_fee = @tenancy_record.fee.blank? ? "" : @tenancy_record.fee
        @lease_signed = @tenancy_record.lease_signed.blank? ? "" : @tenancy_record.lease_signed
        @office_note = @tenancy_record.office_note.blank? ? "" : @tenancy_record.office_note
        @tenancy_transaction_sales_person = @tenancy_record.transaction_sales_person.blank? ? "" : @tenancy_record.transaction_sales_person
        @tenancy_external_agent_company = @tenancy_record.external_agent_company.blank? ? "" : @tenancy_record.external_agent_company
        @tenancy_external_agent_sales_person = @tenancy_record.external_agent_sales_person.blank? ? "" : @tenancy_record.external_agent_sales_person
        @tenancy_conjunction_fee = @tenancy_record.conjunction_fee.blank? ? "" : @tenancy_record.conjunction_fee
        @tenancy_scale_of_fee = @tenancy_record.scale_of_fee.blank? ? "" : @tenancy_record.scale_of_fee
        @reminder_alert = @tenancy_record.reminder_alert.blank? ? "" : @tenancy_record.reminder_alert
        @lease_expiry_sales_person = @tenancy_record.lease_expiry_sales_person.blank? ? "" : @tenancy_record.lease_expiry_sales_person
        @external_agent_company = @tenancy_record.external_agent_company.blank? ? "" : @tenancy_record.external_agent_company
        @external_agent_sales_person = @tenancy_record.external_agent_sales_person.blank? ? "" : @tenancy_record.external_agent_sales_person
        @conjunction_fee = @tenancy_record.conjunction_fee.blank? ? "" : @tenancy_record.conjunction_fee
        @scale_of_fee = @tenancy_record.scale_of_fee.blank? ? "" : @tenancy_record.scale_of_fee
        @contract_link = @tenancy_record.contract_link rescue nil
        # End Tenancy
      end
    end

    if @property.type.to_s == "Commercial"
      if @property.deal_type.to_s == "Sale" || @property.deal_type.to_s == "Both"
        if @property.valid_transactions == true
          @sales_record = @property.sales_records.find(:first, :conditions => "is_deleted = '0'") rescue nil
          if @sales_record.blank?
            @purchaser = SalesRecord.new
            @purchaser_contact = @purchaser.build_agent_contact
          else
            @purchaser = @sales_record
            @purchaser_contact = @purchaser.agent_contact_id.nil? ? @purchaser.build_agent_contact : @purchaser.agent_contact
          end
        else
          @purchaser = SalesRecord.new
          @purchaser_contact = @purchaser.build_agent_contact
        end
      else
        if @property.valid_tenancies == true
          @tenancy_record = @property.tenancy_records.find(:first, :conditions => "is_deleted = '0'") rescue nil
          if @tenancy_record.blank?
            @purchaser = TenancyRecord.new
            @purchaser_contact = @purchaser.build_agent_contact
          else
            @purchaser = @tenancy_record
            @purchaser_contact = @purchaser.agent_contact_id.nil? ? @purchaser.build_agent_contact : @purchaser.agent_contact
          end
        else
          @purchaser = TenancyRecord.new
          @purchaser_contact = @purchaser.build_agent_contact
        end
      end

      if @property.type.to_s == "Commercial"
        if @property.deal_type.to_s == "Sale" || @property.deal_type.to_s == "Both"
          unless @sales_record.blank?
            @contact_name = @sales_record.agent_contact.full_name.blank? ? @sales_record.agent_contact.contact_data_alternative : @sales_record.agent_contact.full_name unless @sales_record.agent_contact.nil?
          end
        else
          unless @tenancy_record.blank?
            @contact_name = @tenancy_record.agent_contact.full_name.blank? ? @tenancy_record.agent_contact.contact_data_alternative : @tenancy_record.agent_contact.full_name unless @tenancy_record.agent_contact.nil?
          end
        end
      else
        unless @property.purchaser.nil?
          @contact_name = @property.purchaser.agent_contact.full_name.blank? ? @property.purchaser.agent_contact.contact_data_alternative :  @property.purchaser.agent_contact.full_name unless @property.purchaser.agent_contact.nil?
        end
      end
    else
      if @property.purchaser.nil?
        @purchaser = Purchaser.new
        @purchaser_contact = @purchaser.build_agent_contact
      else
        @purchaser = @property.purchaser
        @purchaser_contact = @purchaser.agent_contact_id.nil? ? @purchaser.build_agent_contact : @purchaser.agent_contact
      end

      unless @property.purchaser.nil?
        @contact_name = @property.purchaser.agent_contact.full_name.blank? ? @property.purchaser.agent_contact.contact_data_alternative :  @property.purchaser.agent_contact.full_name unless @property.purchaser.agent_contact.nil?
      end
    end

  end

  def share_on_facebook
    @property = Property.find(params[:id])
    respond_to do |format|
      format.html { render :layout => false }
    end
  end

  def tweet(property)
    unless property.blank?
      case property.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{property.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{property.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'commercial_building'
        tweet = "Building Listing: #{property.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'new_development'
        tweet = "Residential Building: #{property.address.to_s.split(",").join(", ").to_s}" + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      when 'land_release'
        tweet = "Land Release: #{property.address.to_s.split(",").join(", ").to_s}" + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{property.id}")}"
      end
    end
    return tweet
  end

  def update_status
    # @property = @office.properties.find params[:id], :include => [:detail]
    @property = Property.find_by_id_and_office_id params[:id], params[:office_id], :include => [:detail]

    unless params[:property].blank?
      if (params[:property][:status] == '2') || params[:property][:status] == '7'
        if @property.type.to_s == "Commercial"
          if @property.deal_type.to_s == "Sale" || @property.deal_type.to_s == "Both"
            new_purchaser = create_note_for_contact = new_transaction = false
            @sales_record = @property.sales_records.find(:first, :conditions => "is_deleted = '0'") rescue nil
            @sales_record.nil? ? new_purchaser = true : @purchaser = @sales_record
            (previous_agent_contact_id = @sales_record.agent_contact_id unless @sales_record.agent_contact_id.nil?) unless @sales_record.nil?
          end
        else
          params[:purchaser] = params[:purchaser].merge(:property_id => @property.id, :date => params[:property][:date], :amount => params[:property][:amount], :display_price => params[:property][:display_price])
          new_purchaser = create_note_for_contact = new_transaction = false
          @property.purchaser.nil? ? new_purchaser = true : @purchaser = @property.purchaser
          (previous_agent_contact_id = @property.purchaser.agent_contact_id unless @property.purchaser.agent_contact_id.nil?) unless @property.purchaser.nil?
        end

        unless @property.type.to_s == "Commercial"
          respond_to do |format|
            begin
              Purchaser.transaction do
                category_id = (@property.type == 'ResidentialLease')? 5 : 1
                params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => @agent.id, :office_id => @office.id, :contact_type => "Individual")
                if new_purchaser
                  @purchaser = Purchaser.new(params[:purchaser])
                  save_purchaser = @purchaser.save
                  if params[:edit_selected_contact] == "true"
                    @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                  end
                  @purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?
                else
                  @purchaser.attributes = params[:purchaser]
                  purchaser_changes = {"purchaser" => @purchaser.changes}
                  ChangesLog.create_log(@property, @purchaser, purchaser_changes, current_user, @office, 2)

                  save_purchaser = @purchaser.save
                  if params[:edit_selected_contact] == "true"
                    @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                  end
                  @purchaser.agent_contact_id.nil? ? (@purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?) : (@purchaser_contact = @purchaser.agent_contact)
                end
                raise 'Failed to save Purchaser' unless save_purchaser

                if params[:add_new_contact] == "true" || !params[:purchaser][:agent_contact_id].blank?

                  if new_purchaser || @purchaser.agent_contact_id.nil?
                    if params[:purchaser][:agent_contact_id].blank?
                      save_purchaser_contact = @purchaser_contact.save
                      @purchaser.update_attribute(:agent_contact_id, @purchaser_contact.id) unless @purchaser_contact.blank?
                      if params[:edit_selected_contact] == "true"
                        @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                      end

                      purchaser_contact = @purchaser_contact.present? ? {"purchaser_contact" => @purchaser_contact.attributes.delete_if{|k,v| v.blank?}} : {}
                      purchaser_attr = {"purchaser" => @purchaser.attributes.delete_if{|k,v| v.blank?}}
                      updated_attributes = purchaser_attr.merge(purchaser_contact)
                      ChangesLog.create_log(@property, @purchaser, updated_attributes, current_user, @office, 1)
                    else
                      save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                      if params[:edit_selected_contact] == "true"
                        @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                      end
                      @purchaser_contact = @purchaser.agent_contact
                    end
                    create_note_for_contact = true
                  else
                    if previous_agent_contact_id.to_s == params[:purchaser][:agent_contact_id].to_s
                      if params[:mark_for_deleted] == "true"
                        save_purchaser_contact = @purchaser.update_attributes!({:skip_validation => true, :agent_contact_id => nil})
                      else
                        save_purchaser_contact = @purchaser.agent_contact.update_attributes(params[:agent_contact])
                      end
                    else
                      if params[:mark_for_deleted] == "true"
                        save_purchaser_contact = @purchaser.update_attributes!({:skip_validation => true, :agent_contact_id => nil})
                      else
                        save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                      end
                      create_note_for_contact = true
                    end
                    @purchaser_contact = @purchaser.agent_contact
                  end

                  raise 'Failed to save Purchaser contact' unless save_purchaser_contact

                  if params[:add_new_contact] == "true"
                    team = []
                    team << @property.primary_contact_id unless @property.primary_contact_id.nil?
                    team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
                    @purchaser_contact.add_categories([category_id])
                    @purchaser_contact.add_assigns(team)
                    @purchaser_contact.add_access(team)
                  end
                end
              end
              update_property_status(create_note_for_contact)
              if params[:property][:status] == '7'
                @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil, :on_hold_by => current_user.id, :on_hold_by_user_type => "Agent User"})
                unless @property.purchaser.blank?
                  @property.purchaser.update_attributes({:date => nil, :amount => nil, :display_price => nil})
                end
                if @property.on_holder.blank?
                  @on_holder = OnHolder.new({:property_id => @property.id, :name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name), :user_type => "Agent User"})
                  @on_holder.save
                else
                  if @property.on_holder.name != "#{current_user.full_name.blank? ? current_user.first_name : current_user.full_name}"
                    @property.on_holder.update_attributes({:name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name)})
                  end
                end
                users = @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
                users.each do |user|
                  begin
                    UserMailer.deliver_listing_notification_for_level1(user, current_user, @office, @property)
                  rescue
                  end
                end
              else
                @property.update_attributes({:on_hold_by => nil, :on_hold_by_user_type => nil})
                if params[:property][:status] == "2"
                  unless @property.purchaser.blank?
                    @property.purchaser.update_attributes({:date => params[:property][:date], :amount => params[:property][:amount], :display_price => params[:property][:display_price]})
                  end
                else
                  @property.purchaser.destroy
                  unless @sales_record.blank?
                    @sales_record.update_attributes({:is_deleted => true})
                  end
                  unless @tenancy_record.blank?
                    @tenancy_record.update_attributes({:is_deleted => true})
                  end
                end
                unless @property.on_holder.blank?
                  @on_holder = @property.on_holder
                  @on_holder.destroy
                end
              end
              flash[:notice] = 'Property was successfully updated.'
              format.html { redirect_to :action => 'status' }
            rescue
              if @property
                @property.status = params[:property][:status]
                unless (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
                  @property.leased_price = params[:property][:amount] unless params[:property][:amount].blank?
                  @property.leased_on = params[:property][:date] unless params[:property][:date].blank?
                else
                  @property.sold_price = params[:property][:amount] unless params[:property][:amount].blank?
                  @property.sold_on = params[:property][:date] unless params[:property][:date].blank?
                end
              end
              @contact_name = params[:agent_name]
              @add_new_contact = params[:add_new_contact]
              format.html { render :action => "status" }
            end
          end
        else
          if @property.deal_type.to_s == "Sale" || @property.deal_type.to_s == "Both"
            respond_to do |format|
              begin
                SalesRecord.transaction do
                  category_id = (@property.type == 'ResidentialLease')? 5 : 1
                  params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => @agent.id, :office_id => @office.id, :contact_type => "Individual")
                  if new_purchaser
                    @purchaser = SalesRecord.new({
                        :property_id => @property.id,
                        :agent_contact_id => params[:purchaser][:agent_contact_id],
                        :date => params[:transaction][:date],
                        :amount => params[:transaction][:amount],
                        :display_price => params[:transaction][:display_price],
                        :method_of_sale => params[:transaction][:method_of_sale],
                        :investment => params[:transaction][:investment],
                        :fee => params[:transaction][:fee],
                        :settlement_date => params[:transaction][:settlement_date],
                        :office_note => params[:transaction][:office_note],
                        :transaction_sales_person => params[:transaction][:transaction_sales_person],
                        :external_agent_company => params[:transaction][:external_agent_company],
                        :external_agent_sales_person => params[:transaction][:external_agent_sales_person],
                        :conjunction_fee => params[:transaction][:conjunction_fee],
                        :scale_of_fee => params[:transaction][:scale_of_fee],
                        :contract_link => params[:transaction][:contract_link]
                      })
                    save_purchaser = @purchaser.save
                    if params[:edit_selected_contact] == "true"
                      @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                    end
                    @purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?
                  else
                    save_purchaser = @purchaser.update_attributes({
                        :agent_contact_id => params[:purchaser][:agent_contact_id],
                        :date => params[:transaction][:date],
                        :amount => params[:transaction][:amount],
                        :display_price => params[:transaction][:display_price],
                        :method_of_sale => params[:transaction][:method_of_sale],
                        :investment => params[:transaction][:investment],
                        :fee => params[:transaction][:fee],
                        :settlement_date => params[:transaction][:settlement_date],
                        :office_note => params[:transaction][:office_note],
                        :transaction_sales_person => params[:transaction][:transaction_sales_person],
                        :external_agent_company => params[:transaction][:external_agent_company],
                        :external_agent_sales_person => params[:transaction][:external_agent_sales_person],
                        :conjunction_fee => params[:transaction][:conjunction_fee],
                        :scale_of_fee => params[:transaction][:scale_of_fee],
                        :contract_link => params[:transaction][:contract_link]
                      })
                    if params[:edit_selected_contact] == "true"
                      @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                    end
                    @purchaser.agent_contact_id.nil? ? (@purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?) : (@purchaser_contact = @purchaser.agent_contact)
                  end
                  raise 'Failed to save Purchaser' unless save_purchaser

                  if params[:add_new_contact] == "true" || !params[:purchaser][:agent_contact_id].blank?

                    if new_purchaser || @purchaser.agent_contact_id.nil?
                      if params[:purchaser][:agent_contact_id].blank?
                        save_purchaser_contact = @purchaser_contact.save
                        @purchaser.update_attribute(:agent_contact_id, @purchaser_contact.id) unless @purchaser_contact.blank?
                        if params[:edit_selected_contact] == "true"
                          @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                        end
                      else
                        save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                        if params[:edit_selected_contact] == "true"
                          @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number]})
                        end
                        @purchaser_contact = @purchaser.agent_contact
                      end
                      create_note_for_contact = true
                    else
                      if previous_agent_contact_id.to_s == params[:purchaser][:agent_contact_id].to_s
                        if params[:mark_for_deleted] == "true"
                          save_purchaser_contact = @purchaser.update_attributes!({:skip_validation => true, :agent_contact_id => nil})
                        else
                          save_purchaser_contact = @purchaser.agent_contact.update_attributes(params[:agent_contact])
                        end
                      else
                        if params[:mark_for_deleted] == "true"
                          save_purchaser_contact = @purchaser.update_attributes!({:skip_validation => true, :agent_contact_id => nil})
                        else
                          save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                        end
                        create_note_for_contact = true
                      end
                      @purchaser_contact = @purchaser.agent_contact
                    end

                    raise 'Failed to save Purchaser contact' unless save_purchaser_contact

                    if params[:add_new_contact] == "true"
                      team = []
                      team << @property.primary_contact_id unless @property.primary_contact_id.nil?
                      team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
                      @purchaser_contact.add_categories([category_id])
                      @purchaser_contact.add_assigns(team)
                      @purchaser_contact.add_access(team)
                    end
                  end
                end
                update_property_status(create_note_for_contact)
                if params[:property][:status] == '7'
                  @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil, :on_hold_by => current_user.id, :on_hold_by_user_type => "Agent User"})
                  unless @property.purchaser.blank?
                    @property.purchaser.update_attributes({:date => nil, :amount => nil, :display_price => nil})
                  end
                  if @property.on_holder.blank?
                    @on_holder = OnHolder.new({:property_id => @property.id, :name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name), :user_type => "Agent User"})
                    @on_holder.save
                  else
                    if @property.on_holder.name != "#{current_user.full_name.blank? ? current_user.first_name : current_user.full_name}"
                      @property.on_holder.update_attributes({:name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name)})
                    end
                  end
                  users = @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
                  users.each do |user|
                    begin
                      UserMailer.deliver_listing_notification_for_level1(user, current_user, @office, @property)
                    rescue
                    end
                  end
                else
                  @property.update_attributes({:on_hold_by => nil, :on_hold_by_user_type => nil})
                  if params[:property][:status] == "2"
                    unless @property.purchaser.blank?
                      @property.purchaser.update_attributes({:date => params[:property][:date], :amount => params[:property][:amount], :display_price => params[:property][:display_price]})
                    end
                  end
                  unless @property.on_holder.blank?
                    @on_holder = @property.on_holder
                    @on_holder.destroy
                  end
                end
                flash[:notice] = 'Property was successfully updated.'
                format.html { redirect_to :action => 'status' }
              rescue
                if @property
                  @property.status = params[:property][:status]
                  unless (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
                    @property.leased_price = params[:property][:amount] unless params[:property][:amount].blank?
                    @property.leased_on = params[:property][:date] unless params[:property][:date].blank?
                  else
                    @property.sold_price = params[:property][:amount] unless params[:property][:amount].blank?
                    @property.sold_on = params[:property][:date] unless params[:property][:date].blank?
                  end
                end
                @contact_name = params[:agent_name]
                @add_new_contact = params[:add_new_contact]
                @sales_person = AgentUser.find(:all, :conditions => "office_id = '#{@office.id}'").map{|x|[x.full_name.blank? ? x.first_name : x.full_name,x.id]}.unshift(['Please select', nil])
                format.html { render :action => "status" }
              end
            end
          end
        end
      elsif params[:property][:status] == '3'
        if @property.type.to_s == "Commercial"
          new_purchaser = create_note_for_contact = new_transaction = false
          @tenancy_record = @property.tenancy_records.find(:first, :conditions => "is_deleted = '0'") rescue nil
          @tenancy_record.nil? ? new_purchaser = true : @purchaser = @tenancy_record
          (previous_agent_contact_id = @tenancy_record.agent_contact_id unless @tenancy_record.agent_contact_id.nil?) unless @tenancy_record.nil?
          respond_to do |format|
            begin
              TenancyRecord.transaction do
                category_id = (@property.type == 'ResidentialLease')? 5 : 1
                params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => @agent.id, :office_id => @office.id, :contact_type => "Individual")
                if new_purchaser
                  @purchaser = TenancyRecord.new({
                      :property_id => @property.id,
                      :agent_contact_id => params[:purchaser][:agent_contact_id],
                      :rent => params[:tenancy][:rent],
                      :net_gross => params[:tenancy][:net_gross],
                      :outgoing => params[:tenancy][:outgoing],
                      :total_rental_pa => params[:tenancy][:total_rental_pa],
                      :total_rental_psm => params[:tenancy][:total_rental_psm],
                      :lease_term => params[:tenancy][:lease_term],
                      :option => params[:tenancy][:option],
                      :start_date => params[:tenancy][:start_date],
                      :end_date => params[:tenancy][:end_date],
                      :fee => params[:tenancy][:fee],
                      :lease_signed => params[:tenancy][:lease_signed],
                      :office_note => params[:tenancy][:office_note],
                      :transaction_sales_person => params[:tenancy][:sales_person],
                      :external_agent_company => params[:tenancy][:external_agent_company],
                      :external_agent_sales_person => params[:tenancy][:external_agent_sales_person],
                      :conjunction_fee => params[:tenancy][:conjunction_fee],
                      :scale_of_fee => params[:tenancy][:scale_of_fee],
                      :reminder_alert => params[:tenancy][:reminder_alert],
                      :lease_expiry_sales_person => params[:tenancy][:lease_expiry_sales_person],
                      :contract_link => params[:tenancy][:contract_link],
                      :agent_id => current_agent.id,
                      :office_id => current_office.id
                    })
                  save_purchaser = @purchaser.save && @purchaser.create_task(@property, @office, current_user, params[:tenancy]) && @property.update_attributes!({:skip_validation => true, :leased_price => params[:tenancy_record][:total_rental_pa]})
                  if params[:edit_selected_contact] == "true"
                    @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number],:company => params[:purchaser_contact][:company]})
                  end
                  @purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?
                else
                  save_purchaser = @purchaser.update_attributes({
                      :agent_contact_id => params[:purchaser][:agent_contact_id],
                      :rent => params[:tenancy][:rent],
                      :net_gross => params[:tenancy][:net_gross],
                      :outgoing => params[:tenancy][:outgoing],
                      :total_rental_pa => params[:tenancy][:total_rental_pa],
                      :total_rental_psm => params[:tenancy][:total_rental_psm],
                      :lease_term => params[:tenancy][:lease_term],
                      :option => params[:tenancy][:option],
                      :start_date => params[:tenancy][:start_date],
                      :end_date => params[:tenancy][:end_date],
                      :fee => params[:tenancy][:fee],
                      :lease_signed => params[:tenancy][:lease_signed],
                      :office_note => params[:tenancy][:office_note],
                      :transaction_sales_person => params[:tenancy][:sales_person],
                      :external_agent_company => params[:tenancy][:external_agent_company],
                      :external_agent_sales_person => params[:tenancy][:external_agent_sales_person],
                      :conjunction_fee => params[:tenancy][:conjunction_fee],
                      :scale_of_fee => params[:tenancy][:scale_of_fee],
                      :reminder_alert => params[:tenancy][:reminder_alert],
                      :lease_expiry_sales_person => params[:tenancy][:lease_expiry_sales_person],
                      :contract_link => params[:tenancy][:contract_link],
                      :is_notified => false,
                      :agent_id => current_agent.id,
                      :office_id => current_office.id
                    })
                  unless @purchaser.task.blank?
                    save_purchaser = save_purchaser && @purchaser.update_task && (@purchaser.property.update_attributes!({:skip_validation => true, :leased_price => params[:tenancy][:total_rental_pa]}) if @purchaser.is_valid?)
                  else
                    save_purchaser = save_purchaser && @purchaser.create_task(@property, @office, current_user, params[:tenancy]) && (@purchaser.property.update_attributes!({:skip_validation => true, :leased_price => params[:tenancy][:total_rental_pa]}) if @purchaser.is_valid?)
                  end
                  if params[:edit_selected_contact] == "true"
                    @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:home_number => params[:purchaser_contact][:home_number],:work_number => params[:purchaser_contact][:work_number],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number],:company => params[:purchaser_contact][:company]})
                  end
                  @purchaser.agent_contact_id.nil? ? (@purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?) : (@purchaser_contact = @purchaser.agent_contact)
                end
                raise 'Failed to save Purchaser' unless save_purchaser

                if params[:add_new_contact] == "true" || !params[:purchaser][:agent_contact_id].blank?

                  if new_purchaser || @purchaser.agent_contact_id.nil?
                    if params[:purchaser][:agent_contact_id].blank?
                      save_purchaser_contact = @purchaser_contact.save
                      @purchaser.update_attribute(:agent_contact_id, @purchaser_contact.id) unless @purchaser_contact.blank?
                      if params[:edit_selected_contact] == "true"
                        @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number],:company => params[:purchaser_contact][:company]})
                      end
                    else
                      save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                      if params[:edit_selected_contact] == "true"
                        @purchaser.agent_contact.update_attributes!({:skip_validation => true, :first_name => params[:purchaser_contact][:first_name], :last_name => params[:purchaser_contact][:last_name],:email => params[:purchaser_contact][:email],:mobile_number => params[:purchaser_contact][:mobile_number],:contact_number => params[:purchaser_contact][:contact_number],:company => params[:purchaser_contact][:company]})
                      end
                      @purchaser_contact = @purchaser.agent_contact
                    end
                    create_note_for_contact = true
                  else
                    if previous_agent_contact_id.to_s == params[:purchaser][:agent_contact_id].to_s
                      if params[:mark_for_deleted] == "true"
                        save_purchaser_contact = @purchaser.update_attributes!({:skip_validation => true, :agent_contact_id => nil})
                      else
                        save_purchaser_contact = @purchaser.agent_contact.update_attributes(params[:agent_contact])
                      end
                    else
                      if params[:mark_for_deleted] == "true"
                        save_purchaser_contact = @purchaser.update_attributes!({:skip_validation => true, :agent_contact_id => nil})
                      else
                        save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                      end
                      create_note_for_contact = true
                    end
                    @purchaser_contact = @purchaser.agent_contact
                  end

                  raise 'Failed to save Purchaser contact' unless save_purchaser_contact

                  if params[:add_new_contact] == "true"
                    team = []
                    team << @property.primary_contact_id unless @property.primary_contact_id.nil?
                    team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
                    @purchaser_contact.add_categories([category_id])
                    @purchaser_contact.add_assigns(team)
                    @purchaser_contact.add_access(team)
                  end
                end
              end
              update_property_status(create_note_for_contact)
              if params[:property][:status] == '7'
                @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil, :on_hold_by => current_user.id, :on_hold_by_user_type => "Agent User"})
                unless @property.purchaser.blank?
                  @property.purchaser.update_attributes({:date => nil, :amount => nil, :display_price => nil})
                end
                if @property.on_holder.blank?
                  @on_holder = OnHolder.new({:property_id => @property.id, :name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name), :user_type => "Agent User"})
                  @on_holder.save
                else
                  if @property.on_holder.name != "#{current_user.full_name.blank? ? current_user.first_name : current_user.full_name}"
                    @property.on_holder.update_attributes({:name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name)})
                  end
                end
                users = @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
                users.each do |user|
                  begin
                    UserMailer.deliver_listing_notification_for_level1(user, current_user, @office, @property)
                  rescue
                  end
                end
              else
                @property.update_attributes({:on_hold_by => nil, :on_hold_by_user_type => nil})
                if params[:property][:status] == "2"
                  unless @property.purchaser.blank?
                    @property.purchaser.update_attributes({:date => params[:property][:date], :amount => params[:property][:amount], :display_price => params[:property][:display_price]})
                  end
                else
                  unless @sales_record.blank?
                    @sales_record.update_attributes({:is_deleted => true})
                  end
                end
                unless @property.on_holder.blank?
                  @on_holder = @property.on_holder
                  @on_holder.destroy
                end
              end
              flash[:notice] = 'Property was successfully updated.'
              format.html { redirect_to :action => 'status' }
            rescue
              if @property
                @property.status = params[:property][:status]
                unless (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
                  @property.leased_price = params[:property][:amount] unless params[:property][:amount].blank?
                  @property.leased_on = params[:property][:date] unless params[:property][:date].blank?
                else
                  @property.sold_price = params[:property][:amount] unless params[:property][:amount].blank?
                  @property.sold_on = params[:property][:date] unless params[:property][:date].blank?
                end
              end
              @contact_name = params[:agent_name]
              @add_new_contact = params[:add_new_contact]
              @sales_person = AgentUser.find(:all, :conditions => "office_id = '#{@office.id}'").map{|x|[x.full_name.blank? ? x.first_name : x.full_name,x.id]}.unshift(['Please select', nil])
              format.html { render :action => "status" }
            end
          end
        else
          update_property_status
          if @property.status == "8"
            @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil})
            unless @property.purchaser.blank?
              @property.purchaser.update_attributes({:date => nil, :amount => nil, :display_price => nil})
            end
            if @property.on_holder.blank?
              @on_holder = OnHolder.new({:property_id => @property.id, :name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name), :user_type => "Agent User"})
              @on_holder.save
            else
              if @property.on_holder.name != "#{current_user.full_name.blank? ? current_user.first_name : current_user.full_name}"
                @property.on_holder.update_attributes({:name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name)})
              end
            end
          else
            unless @property.status == "2" || @property.status == "3" || @property.status == "8"
              unless @property.purchaser.blank?
                @property.purchaser.destroy
              end
              unless @property.sales_records.blank?
                @sales_record = @property.sales_records.find(:first, :conditions => "is_deleted = '0'")
                unless @sales_record.blank?
                  @sales_record.update_attributes({:is_deleted => true})
                  @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil})
                end
              end
              unless @property.tenancy_records.blank?
                @tenancy_record = @property.tenancy_records.find(:first, :conditions => "is_deleted = '0'")
                @tenancy_record.update_attributes({:is_deleted => true})
              end
            end
            unless @property.on_holder.blank?
              @on_holder = @property.on_holder
              @on_holder.destroy
            end
          end
          flash[:notice] = 'Property was successfully updated.'
          session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-statusChanged_#{Property::STATUS_NAME[@property.status - 1]}"
          redirect_to status_agent_office_property_path(@agent,@office,@property)
        end
      else
        update_property_status
        if @property.status == "8"
          @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil})
          unless @property.purchaser.blank?
            @property.purchaser.update_attributes({:date => nil, :amount => nil, :display_price => nil})
          end
          if @property.on_holder.blank?
            @on_holder = OnHolder.new({:property_id => @property.id, :name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name), :user_type => "Agent User"})
            @on_holder.save
          else
            if @property.on_holder.name != "#{current_user.full_name.blank? ? current_user.first_name : current_user.full_name}"
              @property.on_holder.update_attributes({:name => (current_user.full_name.blank? ? current_user.first_name : current_user.full_name)})
            end
          end
        else
          unless @property.status == "2" || @property.status == "3" || @property.status == "8"
            unless @property.purchaser.blank?
              @property.purchaser.destroy
            end
            unless @property.sales_records.blank?
              @sales_record = @property.sales_records.find(:first, :conditions => "is_deleted = '0'")
              unless @sales_record.blank?
                @sales_record.update_attributes({:is_deleted => true})
                @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil})
              end
            end
            unless @property.tenancy_records.blank?
              @tenancy_record = @property.tenancy_records.find(:first, :conditions => "is_deleted = '0'")
              @tenancy_record.update_attributes({:is_deleted => true})
            end
          end
          unless @property.on_holder.blank?
            @on_holder = @property.on_holder
            @on_holder.destroy
          end
        end
        flash[:notice] = 'Property was successfully updated.'
        session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-statusChanged_#{Property::STATUS_NAME[@property.status - 1]}"
        redirect_to status_agent_office_property_path(@agent,@office,@property)
      end
    end
  end

  def exports
    @property = Property.find(params[:id])
    PropertyDomainExport.synchronize_domain_export(@property)

    property_country = Country.find_by_name(@property.country)
    @office = Office.find(params[:office_id])
    country = Country.find_by_name(@office.country)
    @portal_countries = country.portal_countries
    @domain_exports = @international_portal = @residential_portals = @commercial_business_portals = @holiday_portals = @real_estate_search_engines = @marketing_crm_based_software = @social_networking = @additional_holiday_portals = @addtional_commercial_business_portals =''

    #    @additional_portals = PortalAccount.find(:all, :conditions => "portal_name IN ('Stayz.com.au','Holidayview.com.au','Realcommercial.com.au','Realbusinesses.com.au','Domainbusiness.com.au','Realestate1commercial.com.au','Commercialview.com.au','Businessview.com.au') and office_id = #{@office.id}")
    #    @portal_ids = Portal.find(:all, :conditions => "portal_name IN ('Stayz.com.au','Holidayview.com.au','Realcommercial.com.au','Realbusinesses.com.au','Domainbusiness.com.au','Realestate1commercial.com.au','Commercialview.com.au','Businessview.com.au')")
    #    @additional_autosubs = PropertyPortalExport.find(:all, :conditions => "portal_id IN (#{@portal_ids.map{|a|a.id}.join(',')}) and property_id = #{@property.id}") if @portal_ids.present?

    @portal_countries.each do |p_c|
      unless p_c.blank?
        portal_country = PortalCountry.find_all_by_portal_id(p_c.portal.id)
        portal_autosubcribe = PropertyPortalExport.find_by_property_id_and_portal_id_and_uncheck(@property.id,p_c.portal.id,false)

        active_feed = PortalAccount.find_by_portal_name_and_office_id(p_c.portal.portal_name, @office.id)
        unless active_feed.blank?
          disabled = active_feed.active_feeds != "1" ? 'disabled' : ''
        else
          disabled = 'disabled'
        end
        checked = portal_autosubcribe.blank? ?  "" : "checked"
        disabled = '' if (checked == "checked" && @property.status == 6)
        disabled = 'disabled' if (checked == "checked" && @property.status != 6) || (checked == "checked" && @property.deactivation != true && @property.status == 6)
        #national
        @residential_portals += add_export_list(p_c.portal, checked, disabled) if portal_country.size == 1 && p_c.portal.country == property_country.code && p_c.portal.portal_type == 'Residential Portals'
        @commercial_business_portals += add_export_list(p_c.portal, checked, disabled) if portal_country.size == 1 && p_c.portal.country == property_country.code && p_c.portal.portal_type == 'Commercial & Business Portal'
        @holiday_portals += add_export_list(p_c.portal, checked, disabled) if portal_country.size == 1 && p_c.portal.country == property_country.code && p_c.portal.portal_type == 'Holiday Portals'
        @international_portal += add_export_list(p_c.portal, checked, disabled) if portal_country.size > 1 && p_c.portal.portal_type != 'Classifieds'
        @social_networking += add_export_list(p_c.portal, checked, disabled) if p_c.portal.portal_type == 'Social Networking'
        @real_estate_search_engines += add_export_list(p_c.portal, checked, disabled) if p_c.portal.portal_type == 'Real Estate Search Engines'
        @marketing_crm_based_software += add_export_list(p_c.portal, checked, disabled) if p_c.portal.portal_type == 'Marketing & CRM Based Software'
        if p_c.portal.id == 2
          @additional_holiday_portals += add_addtional_export_list(69, checked, "Stayz.com.au", disabled)
        end
        if p_c.portal.id == 8
          @additional_holiday_portals += add_addtional_export_list(70, checked, "Holidayview.com.au", disabled)
        end
        if country.id == 12
          @addtional_commercial_business_portals += add_addtional_export_list(71, checked, "Realcommercial.com.au", disabled) if p_c.portal.id == 1
          @addtional_commercial_business_portals += add_addtional_export_list(72, checked, "Realbusinesses.com.au", disabled) if p_c.portal.id == 1
          @addtional_commercial_business_portals += add_addtional_export_list(73, checked, "Domainbusiness.com.au", disabled) if p_c.portal.id == 2
          @addtional_commercial_business_portals += add_addtional_export_list(74, checked, "Realestate1commercial.com.au", disabled) if p_c.portal.id == 22
          @addtional_commercial_business_portals += add_addtional_export_list(75, checked, "Commercialview.com.au", disabled) if p_c.portal.id == 8
          @addtional_commercial_business_portals += add_addtional_export_list(76, checked, "Businessview.com.au", disabled) if p_c.portal.id == 8
        end
      end
    end
    unless @property.property_domain_exports.blank?
      @property.property_domain_exports.each do |de|
        checked = (de.send_api == true) ?  "checked" : ""
        disabled = (checked.blank? ? "" : "disabled")
        disabled = "" if @property.deactivation == true
        @domain_exports += add_domain_export_list(de.id, checked, de.domain.name.downcase.gsub('http://', ''), disabled) unless de.domain.blank?
      end
    end
  end

  def exports2
    @property = Property.find(params[:id])
    PropertyDomainExport.synchronize_domain_export(@property)

    @portal_autosubcribes = PropertyPortalExport.find_all_by_property_id_and_uncheck(@property.id,false)
    @portal_accounts = PortalAccount.find(:all, :conditions => "office_id = #{@office.id} and active_feeds = '1'")
    portals_name_id = Portal.find(:all, :conditions => "portal_name IN ('#{@portal_accounts.map{|a| a.portal_name}.join("','")}')")
    if portals_name_id.present?
      if portals_name_id.detect{|a| a.id == 1}.present?
        if portals_name_id.detect{|a| a.portal_name == "Realcommercial.com.au"}.blank?
          @portal_accounts << PortalAccount.new(:portal_name => "Realcommercial.com.au", :office_id => @office.id)
        end
        if portals_name_id.detect{|a| a.portal_name == "Realbusinesses.com.au"}.blank?
          @portal_accounts << PortalAccount.new(:portal_name => "Realbusinesses.com.au", :office_id => @office.id)
        end
      end
      if portals_name_id.detect{|a| a.id == 2}.present?
        if portals_name_id.detect{|a| a.portal_name == "Stayz.com.au"}.blank?
          @portal_accounts << PortalAccount.new(:portal_name => "Stayz.com.au", :office_id => @office.id)
        end
        if portals_name_id.detect{|a| a.portal_name == "Domainbusiness.com.au"}.blank?
          @portal_accounts << PortalAccount.new(:portal_name => "Domainbusiness.com.au", :office_id => @office.id)
        end
      end
    end
    @portals_name_id = Portal.find(:all, :conditions => "portal_name IN ('#{@portal_accounts.map{|a| a.portal_name}.join("','")}')")
    @export_properties = ExportsProperty.find(:all, :conditions => "property_id = #{params[:id]}")
    export_ids = @export_properties.map{|e| e.export_id} if @export_properties.present?
    @exports = Export.find(:all, :conditions => "id IN (#{export_ids.join(',')})") if export_ids.present?
    if @portal_accounts.present?
      @portals = Portal.find(:all)
      subscribed_name = @portal_accounts.map{|a| a.portal_name}
      @not_subscribed = @portals.reject{|a| subscribed_name.include?(a.portal_name)}
    end
    if @office.http_api_enable && @property.property_domain_exports.present?
      @domain_exports = ""
      @property.property_domain_exports.each do |de|
        checked = (de.send_api == true) ?  "checked" : ""
        disabled = (checked.blank? ? "" : "disabled")
        disabled = "" if @property.deactivation == true
        @domain_exports += add_domain_export_list(de.id, checked, de.domain.name.downcase.gsub('http://', ''), disabled) unless de.domain.blank?
      end
    end

    # Property domain projects
    @domain_projects = ExportsDomainProject.find(:all, :conditions => "developer_id = #{@agent.developer.id} and enabled is true")
    if @domain_projects.present?
      @domain_projects = @domain_projects.map{|a| [a.project_name, a.id]}.unshift(["Please Select",""])
      @selected_project = @property.property_domain_project.present? ? @property.property_domain_project.domain_project_id : ""
    end

    # Enable / disable Super API check boxes
    @domain_projects = ExportsDomainProject.find(:all, :conditions => "developer_id = #{@agent.developer.id} and enabled is true")
    if @domain_projects.present?
      @domain_projects = @domain_projects.map{|a| [a.project_name, a.id]}.unshift(["Please Select",""])
      @selected_project = @property.property_domain_project.present? ? @property.property_domain_project.domain_project_id : ""
    end
  end

  def export_portals
    @property = Property.find(params[:id])
    ActiveRecord::Base.connection.execute("UPDATE properties SET `updated_at`='#{Time.zone.now.to_formatted_s(:db_format)}' WHERE id=#{@property.id}")
    spawn(:kill => true) do
      system("curl 'http://commercialloopcrm.com.au/xml_file/xmlexport/send_feed.php?property=#{@property.id}&office=#{@office.id}&portal=realestate.com.au'")
      system("curl 'http://commercialloopcrm.com.au/xml_file/xmlexport/send_feed.php?property=#{@property.id}&office=#{@office.id}&portal=commercialrealestate'")
    end
    flash[:notice] = 'Property was successfully exported to portals.'
    redirect_to exports_agent_office_property_path(@agent,@office,@property)
  end

  def update_exports
    # @property = Property.find(params[:id])
    # if @property.deactivation == true
    #   PropertyDomainExport.update_all({:send_api => false}, {:property_id => params[:id]})
    #   PropertyDomainExport.update_all({:send_api => true}, {:id => params[:domain_export]}) unless params[:domain_export].blank?
    # else
    #   PropertyDomainExport.update_all({:send_api => true}, {:id => params[:domain_export]}) unless params[:domain_export].blank?
    # end

    # unless params[:property_portals].blank?
    #   @property.deactivation == true && @property.status == 6 ? @property.add_property_portal(params[:property_portals]) : @property.add_property_portal(params[:property_portals], true)
    # else
    #   @property.add_property_portal([0]) if @property.deactivation == true && @property.status == 6
    # end

    # @property.updated_at = Time.now
    # @property.save(false)
    # flash[:notice] = 'Export was successfully updated.'
    # redirect_to exports_agent_office_property_path(@agent,@office,@property)
    update_property_export_settings
    redirect_to exports_agent_office_property_path(@agent,@office,@property)
  end

  def update_portal_exports2
    # @property = Property.find(params[:id])
    # unless params[:property_portals].blank?
    #   @property.deactivation == true && @property.status == 6 ? @property.add_property_portal(params[:property_portals]) : @property.add_property_portal(params[:property_portals], true)
    # else
    #   @property.add_property_portal([0]) if @property.deactivation == true && @property.status == 6
    # end

    # @property.updated_at = Time.now
    # @property.save(false)
    # flash[:notice] = 'Export was successfully updated.'
    # redirect_to exports2_agent_office_property_path(@agent,@office,@property)
    update_property_export_settings

    # Update property domain project
    if params["domain_project"].present? && params["domain_project"]["enabled"].present?
      if params["domain_project"]["domain_project_id"].present?
        if @property.property_domain_project.present?
          @property.property_domain_project.update_attributes(:domain_project_id => params["domain_project"]["domain_project_id"])
          ActiveRecord::Base.connection.execute("UPDATE properties SET domain_project = 1 WHERE id = #{@property.id}")
        else
          PropertyDomainProject.create(:property_id => @property.id, :domain_project_id => params["domain_project"]["domain_project_id"] )
          ActiveRecord::Base.connection.execute("UPDATE properties SET domain_project = 1 WHERE id = #{@property.id}")
        end
      else
        flash[:notice] = 'Please select project.'
      end
    elsif params["domain_project"].present? && params["domain_project"]["enabled"].blank?
      ActiveRecord::Base.connection.execute("UPDATE properties SET domain_project = NULL WHERE id = #{@property.id}")
      if params["domain_project"]["domain_project_id"].blank? && @property.property_domain_project.present?
        @property.property_domain_project.destroy
      end
    end

    # Update Super API properties
    # SuperapiProperty.update_superapi_properties(params, @office, @property)
    redirect_to exports2_agent_office_property_path(@agent,@office,@property)
  end

  def refresh_features
    @pre_defined_features = Feature::ALL_PRE_DEFINED.inject([]) do |ary, name|
      ary << Kernel.const_get(name).send(params[:ptype])
    end
    @your_features = @office.user_defined_features.send params[:ptype]
    @help_data = ActiveHelp.first.data
    respond_to do |format|
      format.js
    end
  end

  def delete_listing
    property = Property.find_by_id(params[:property_id])
    session[:parent_listing_id] = property.parent_listing_id unless property.parent_listing_id.blank?
    parent_listing = Property.find(session[:parent_listing_id])
    children_listing_count = Property.find(:all, :conditions => ["parent_listing_id = ? AND deleted_at IS NULL", session[:parent_listing_id]]).count

    if children_listing_count == 1
      parent_listing.update_attributes(:is_have_children_listing => "0")
    end

    unless property.blank?
      property.updated_at = Time.zone.now
      property.deleted_at = property.updated_at + 10.hours
      property.status = Property::STATUS[:withdrawn]

      if property.extra.blank?
        extra = Extra.new({:property_id => property.id, :office_id => property.office_id, :featured_property => false})
      else
        extra = property.extra
        extra.update_attribute(:featured_property, false)
      end
      property.save(false)
      attributes = property.attributes.reject{|k,v| v.blank?}
      attributes = {"property" => attributes}
      ChangesLog.create_log(property, "", attributes, current_user, @office, 3)
    end
    respond_to do |format|
      format.html { redirect_to agent_office_properties_path(@agent,@office) }
      format.xml  { head :ok }
    end
    session[:parent_listing_id] = nil
  end

  def update_listing
    id = []
    unless session[:update_listing].blank?
      id = session[:update_listing]
    end
    render :json =>id
  end

  def populate_combobox
    suburb,type,assigned_to,condition = [],[],[],[]
    condition = [" properties.office_id = ? "]
    condition << current_office.id

    properties = Property.find(:all,:include =>[:primary_contact], :conditions => condition,:select => "properties.primary_contact_id, DISTINCT(properties.suburb),DISTINCT(properties.property_type),users.first_name,users.last_name")
    properties.map{|x|
      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText =>x.suburb.to_s
      } unless x.suburb.blank?;
      type << {
        :type => x.property_type.to_s,
        :typeText =>x.property_type.to_s
      } unless x.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?) ,
        :assigned_toText => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?)
      } unless x.primary_contact.blank?
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    return(render(:json =>{:results => properties.length,:suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,:types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,:team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        if current_user.limited_client_access?
          if !@office.control_level3
            condition[0] += " AND properties.primary_contact_id = ? "
            condition << current_user.id
          end
        elsif current_user.allow_property_owner_access?
          condition[0] += " AND properties.agent_user_id = ?"
          condition << current_user.id
        end

        if(!session[:type].blank? && Property::TYPES.map{|s| s[1]}.include?(session[:type]))
          condition[0] += " AND properties.type = ?"
          condition <<  session[:type]
        end


        if ((current_user.allow_property_owner_access? && session[:status] == 1) || (current_user.allow_property_owner_access?  && session[:status].blank?))|| current_user.allow_property_owner_access?
          session[:status] = "all"
        end

        unless session[:status] == "all"
          unless session[:status].blank?
            condition[0] += " AND properties.status = ?"
            condition <<  session[:status]
          end
        end

        unless session[:suburb] == "all" || session[:suburb].blank?
          condition[0] += " AND properties.suburb = ?"
          condition << session[:suburb]
        end

        unless session[:primary_contact_fullname] == "all" || session[:primary_contact_fullname].blank?
          name =  session[:primary_contact_fullname].split(' ')
          condition[0] += " AND users.first_name  = ?"
          condition << name[0]
          unless name[1].blank?
            condition[0] += " AND users.last_name  = ?"
            condition << name[1]
          end
        end

        unless session[:property_type] == "all" || session[:property_type].blank?
          condition[0] += " AND properties.property_type = ?"
          condition << session[:property_type]
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      if current_user.limited_client_access?
        if !@office.control_level3
          condition[0] += " AND properties.primary_contact_id = ? "
          condition << current_user.id
        end
      elsif current_user.allow_property_owner_access?
        condition[0] += " AND properties.agent_user_id = ?"
        condition << current_user.id
      end
    end
    condition[0] += " AND properties.deleted_at is NULL"

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = Property.build_sorting(params[:sort],params[:dir])
    if params[:sort] == 'sqm' || params[:sort] == "A/P"
      @properties = Property.sort_by_customize(params[:sort],params[:controller],params[:dir],params[:page],params[:limit],@agent.id,@office.id, @property.id)
    else
      @properties = Property.paginate(:all,:include =>[:primary_contact], :conditions => condition, :order => order,:select => "properties.id,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,users.first_name,users.last_name,properties.size_from,properties.size_to,properties.number_of_suites,properties.detail.floor_area", :page =>params[:page], :per_page => params[:limit].to_i)
    end
    if @properties.present?
      @properties.each do |property|
        check_and_update_icon(property)
      end
    end
    results = 0
    if params[:limit].to_i == 50
      #      results = 20 #Property.find(:all,:select => 'count(properties.id), properties.primary_contact_id',:include =>[:primary_contact],:conditions => condition,:limit => params[:limit].to_i)
      results = Property.count(:include =>[:primary_contact],:conditions => condition)
    else
      results = Property.count(:include =>[:primary_contact],:conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'listing'=>Property.abbrv_properties(x.type),
              'property_type'=>x.property_type.blank? ? "" : x.property_type,
              'address' =>((x.street_number.blank? ? "" : x.street_number)+" "+(x.street.blank? ? "" : x.street.to_s)),
              'suburb' => x.suburb,'type' => x.property_type,
              'suite'=>(x.unit_number.blank? ? "" : x.unit_number),
              'sqm'=>((x.detail.blank? ? "" : (x.detail.floor_area.blank? ? "" : x.detail.floor_area)) unless x.type == "NewDevelopment" || x.type == "LandRelease" || x.type == "ResidentialLease"),
              'A/P'=> x.ownership.blank? ? "Agency" : x.ownership,
              'price' => Property.check_price(x),
              'assigned_to'=>x.primary_contact.blank? ? '' : x.primary_contact.full_name.to_s.strip,
              'actions'=>Property.check_color(x),
              'status' => Property.status_name(x),
              'fb'=> [x.id,@office.url.blank? ? (x.detail.respond_to?(:deal_type) ? x.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}", tweet(x)]} unless x.blank? }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def preparing_changes
    id = []
    if session[:status] == 1
      id = ['statusValue',1]
    end
    if session[:status] == 0
      id = ['statusValue','']
    end
    unless session[:update_listing].blank?
      attr = session[:update_listing].split("-")
      if attr.length == 4
        id = [attr[0],attr[1],attr[2],attr[3]]
      else
        attr_name = attr[2].split("_")
        id = [attr_name[0],attr_name[1],attr[0],attr[1]]
      end
    end
    session[:update_listing] = nil
    render :json =>id
  rescue
    session[:update_listing] = nil
    render :json =>id
  end

  def check_color
    property = Property.find(params[:id])
    property.images.count > 0 ? photo = "/images/icons/icon_photo.gif" : photo = "/images/icons/icon_photo_grey.gif"
    property.floorplans.count > 0 ? plan = "/images/icons/icon_plan.gif" : plan = "/images/icons/icon_plan_grey.gif"
    property.opentimes.blank? ? open = "/images/icons/icon_date_grey.gif" : open = "/images/icons/icon_date.gif"
    #    property.videos.blank? ? video = "/images/icons/icon_video_grey.gif" : video = "/images/icons/icon_video.gif"
    #    render :json =>[photo,plan,open,video]
    render :json =>[photo,plan,open]
  end

  def check_and_update_icon(property)
    img_count = property.images.count rescue 0
    floorp_count = property.floorplans.count rescue 0
    if !property.image_exist && img_count > 0
      property.image_exist = true
      property.save(false)
    end
    if property.image_exist && img_count == 0
      property.image_exist = false
      property.save(false)
    end
    if !property.floorplan_exist && floorp_count > 0
      property.floorplan_exist = true
      property.save(false)
    end
    if property.floorplan_exist && floorp_count == 0
      property.floorplan_exist = false
      property.save(false)
    end
  end

  def auto_complete
    result = []
    unless request.url().scan("auto_office").blank?
      offices = Office.find(:all, :conditions => ["`name` LIKE ? ", '%'+params[:q]+'%'])
      offices.each{|x| result << {'id' => "#{x.id.to_s}", 'name' => "#{x.name}" } }
      render :json => {"results" => result, "total" => (offices ? offices.length : 0) }
    else

      contacts = AgentContact.find(:all, :conditions => ["( `first_name` LIKE ? OR `last_name` LIKE ? OR `contact_data_alternative` LIKE ?) and `agent_id` = ? and `office_id` = ? ", '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', @agent.id, @office.id])
      contacts.each{|x|
        full_name = x.first_name.to_s + " " + x.last_name.to_s
        contact_data = x.contact_data_alternative.to_s
        result << {'id' => "#{x.id.to_s}", 'name' => "#{full_name}" } unless x.first_name.blank? and x.last_name.blank?
        result << {'id' => "#{x.id.to_s}", 'name' => "#{contact_data}" } unless x.contact_data_alternative.blank?
      }
      companies = AgentCompany.find(:all, :conditions => ["( `company` LIKE ?) AND `agent_id` = ? AND `office_id` = ?", '%' + params[:q].to_s + '%', @agent.id, @office.id])
      companies.each{|x|
        company_name = x.company
        result << {'id' => "#{x.id.to_s}", 'name' => "#{x.company}"} unless x.company.blank?
      }
      totals = contacts + companies
      render :json => {"results" => result, "total" => (totals ? totals.length : 0) }
    end
  end

  def auto_complete_sale_property
    result = []
    contacts = AgentContact.find(:all, :include => [:contact_category_relations], :conditions => ["(agent_contacts.first_name LIKE ? OR agent_contacts.last_name LIKE ? OR agent_contacts.contact_data_alternative LIKE ?) AND agent_contacts.agent_id = ? AND agent_contacts.office_id = ? AND contact_category_relations.category_contact_id = ?", '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', @agent.id, @office.id, 2])
    contacts.each{|x|
        full_name = x.first_name.to_s + " " + x.last_name.to_s
        contact_data = x.contact_data_alternative.to_s
        result << {'id' => "#{x.id.to_s}", 'name' => "#{full_name}" } unless x.first_name.blank? and x.last_name.blank?
        result << {'id' => "#{x.id.to_s}", 'name' => "#{contact_data}" } unless x.contact_data_alternative.blank?
      }
    render :json => {"results" => result, "total" => (contacts ? contacts.length : 0) }
  end

  def auto_complete_lease_property
    result = []
    contacts = AgentContact.find(:all, :include => [:contact_category_relations], :conditions => ["(agent_contacts.first_name LIKE ? OR agent_contacts.last_name LIKE ? OR agent_contacts.contact_data_alternative LIKE ?) AND agent_contacts.agent_id = ? AND agent_contacts.office_id = ? AND contact_category_relations.category_contact_id = ?", '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', @agent.id, @office.id, 5])
    contacts.each{|x|
        full_name = x.first_name.to_s + " " + x.last_name.to_s
        contact_data = x.contact_data_alternative.to_s
        result << {'id' => "#{x.id.to_s}", 'name' => "#{full_name}" } unless x.first_name.blank? and x.last_name.blank?
        result << {'id' => "#{x.id.to_s}", 'name' => "#{contact_data}" } unless x.contact_data_alternative.blank?
      }
    render :json => {"results" => result, "total" => (contacts ? contacts.length : 0) }
  end

  def auto_complete_external_agent
    result = []
    unless request.url().scan("auto_office").blank?
      offices = Office.find(:all, :conditions => ["`name` LIKE ? ", '%'+params[:q]+'%'])
      offices.each{|x| result << {'id' => "#{x.id.to_s}", 'name' => "#{x.name}" } }
      render :json => {"results" => result, "total" => (offices ? offices.length : 0) }
    else
      contacts = ExternalAgentContact.find(:all, :conditions => ["( `first_name` LIKE ? OR `last_name` LIKE ?) and `agent_id` = ? and `office_id` = ? ", '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', @agent.id, @office.id])
      contacts.each{|x|
        full_name = x.first_name.to_s + " " + x.last_name.to_s
        result << {'id' => "#{x.id.to_s}", 'name' => "#{full_name}" } unless x.first_name.blank?
      }
      render :json => {"results" => result, "total" => (contacts ? contacts.length : 0) }
    end
  end

  def populate_agent
    unless params[:type_agent].blank?
      if params[:type_agent] == "Conjunctional"
        contacts = AgentUser.find(:all, :conditions => "`developer_id`='#{@agent.developer_id}' And roles.id = 7", :include => "roles", :order => "`users`.first_name ASC")
        @contacts = contacts.map{|u| [u.full_name + ((u.conjunctional.blank?)? " (#{u.conjunctional_office_name})" : " (#{u.conjunctional.conjunctional_office_name})") , u.id]}.uniq
      elsif params[:type_agent] == "Network"
        offices_dev = ""
        offices_name = {}
        @contacts = []
        offices = Office.find_by_sql("SELECT offices.id, offices.name FROM offices INNER JOIN agents ON offices.agent_id = agents.id WHERE agents.developer_id = #{@agent.developer_id} AND offices.deleted_at IS NULL")
        offices.each_with_index{|of, index|
          offices_dev += ((index == 0) ? "#{of.id}" :",#{of.id}")
          offices_name.merge!(of.id => "#{of.name}")
        }
        contacts = AgentUser.find(:all, :conditions => "roles.id = 3 OR roles.id = 4 OR roles.id = 5 And `users`.office_id IN (#{offices_dev}) And `users`.office_id != #{@office.id}", :include => "roles", :order => "`users`.first_name ASC")
        contacts.each{|u| @contacts << [u.full_name + " (#{offices_name[u.office_id]})" , u.id] unless offices_name[u.office_id].blank? }
      else
        contacts = normal_contact
        @contacts = contacts.map{|u| [u.full_name, u.id]}.uniq
      end
    else
      contacts = normal_contact
      @contacts = contacts.map{|u| [u.full_name, u.id]}.uniq
    end
    return(render(:json => @contacts))
  end

  def duplication
    unless params[:duplication_number].blank?
      parent_property = Property.find_by_id(params[:id])
      unless parent_property.blank?
        unless parent_property.images.blank?
          err = 0
          (1..params[:duplication_number].to_i).each{|c|
            pclass = Kernel.const_get parent_property.type
            parent_property.created_at = ""
            parent_property.updated_at = ""
            tmp_property = pclass.new(parent_property.attributes)
            tmp_property.feature_ids = parent_property.feature_ids unless parent_property.feature_ids.blank?
            tmp_property.status = 6
            tmp_property.deactivation = true
            tmp_property.xml_id = nil
            tmp_property.type = params[:duplication_listing]
            tmp_property.save

            detail_save = nil
            new_property = Property.find_by_id(tmp_property.id)
            unless new_property.blank?
              if parent_property.type.to_s == params[:duplication_listing]
                new_property.build_detail parent_property.detail.attributes
                detail_save = new_property.save
              else
                detail_attr = nil
                arr = parent_property.parse_duplication(params[:duplication_listing])
                detail_attr = parent_property.detail.attributes.delete_if { |x,y| arr.include?(x)} unless arr.blank?
                unless detail_attr.blank?
                  case params[:duplication_listing]
                  when "ResidentialSale"
                    detail_attr = detail_attr.merge({:tax_rate_period => "", :condo_strata_fee_period => "", :estimate_rental_return_period => "", :water_rate_period => ""})
                    detail_attr = detail_attr.merge({:number_of_floors => 1}) if parent_property.type == "ProjectSale"
                  when "ResidentialLease"
                    detail_attr = detail_attr.merge({:date_available => Time.now.strftime("%Y-%m-%d")})
                  when "ProjectSale"
                    detail_attr = detail_attr.merge({:category => "Apartment Packages"})
                  end
                  new_property.build_detail(detail_attr)
                  detail_save = new_property.save
                end
              end
            end

            if detail_save
              if parent_property.type.to_s == 'HolidayLease'
                unless parent_property.rental_seasons.blank?
                  #========Copying Opentime========
                  parent_property.rental_seasons.each{ |src|
                    begin
                      rs = new_property.rental_seasons.new(src.attributes)
                      rs.save!
                    rescue
                    end
                  }
                end
              end

              unless parent_property.extra.blank?
                begin
                  ex = new_property.build_extra(parent_property.extra.attributes)
                  ex.save!
                rescue
                end
              end

              unless parent_property.extra_options.blank?
                #========Copying Extra option========
                parent_property.extra_options.each{ |src|
                  begin
                    eo = new_property.extra_options.new(src.attributes)
                    eo.save!
                  rescue
                  end
                }
              end

              unless parent_property.opentimes.blank?
                #========Copying Opentime========
                parent_property.opentimes.each{ |src|
                  begin
                    op = new_property.opentimes.new(src.attributes)
                    op.save!
                  rescue
                  end
                }
              end

              #                unless parent_property.property_portal_exports.blank?
              #                  #========Copying Portal Export========
              #                  parent_property.property_portal_exports.each{ |src|
              #                    begin
              #                      pe = new_property.property_portal_exports.new(src.attributes)
              #                      pe.save!
              #                    rescue
              #                    end
              #                  }
              #                end

              country = Country.find_by_name(@office.country)
              @property = new_property
              init_exports(country.portal_countries)

              #                new_property.update_attributes({:created_at => Time.zone.now, :updated_at => Time.zone.now})
              jobs = Delayed::Job.all
              id_jobs = []
              unless jobs.blank?
                jobs.each do |job|
                  handler = job.handler.split
                  id_jobs << job.id if handler[handler.size-2].include?("new_property_id") && new_property.id.to_i == handler.last.to_i
                end
              end
              Delayed::Job.enqueue(DuplicateProperty.new(parent_property.id, new_property.id), 3) if id_jobs.blank?
            else
              (ActiveRecord::Base.connection.execute("DELETE FROM properties WHERE id = #{tmp_property.id}") unless tmp_property.id.blank?) unless tmp_property.blank?
              err = 1
            end
          }
          if err == 0
            if params[:duplication_number].to_i == 1
              flash[:notice] = "#{params[:duplication_number]} property has been duplicated"
            else
              flash[:notice] = "#{params[:duplication_number]} properties have been duplicated"
            end
          else
            flash[:notice] = "Property Detail Is Invalid, try to update the property to fix this."
          end
        else
          flash[:notice] = 'Property should has minimum 1 image.'
        end
      end
    end
    redirect_to agent_office_path(@agent, @office)
  end

  private

  def prepare_creation
    if %w(residential_lease residential_sale commercial commercial_building holiday_lease project_sale business_sale new_development land_release general_sale livestock_sale clearing_sales_event clearing_sale).include?(params[:action])
      pclass = Kernel.const_get get_property_type(params[:action])
      @ptype = params[:action]
      if request.post?
        is_send = false
        params["#{@ptype}_detail"]["auction_date"] = change_date_format(params["#{@ptype}_detail"]["auction_date"]) unless params["#{@ptype}_detail"]["auction_date"].blank?
        params["#{@ptype}_detail"]["date_available"] = change_date_format(params["#{@ptype}_detail"]["date_available"]) unless params["#{@ptype}_detail"]["date_available"].blank?
        params["#{@ptype}_detail"]["lease_commencement"] = change_date_format(params["#{@ptype}_detail"]["lease_commencement"]) unless params["#{@ptype}_detail"]["lease_commencement"].blank?
        unless params["#{@ptype}_detail"]["lease_end"].blank?
          params["#{@ptype}_detail"]["lease_end"] = change_date_format(params["#{@ptype}_detail"]["lease_end"]) if @ptype == "business_sale"
        end
        if !params["auction_hour"].blank? && !params["auction_min"].blank?
          params["#{@ptype}_detail"]["auction_time"]= "#{params["auction_hour"]}:#{params["auction_min"]}"
        end

        params["#{@ptype}_detail"]["date"] = change_date_format(params["#{@ptype}_detail"]["date"]) unless params["#{@ptype}_detail"]["date"].blank?
        if !params["hour"].blank? && !params["min"].blank?
          params["#{@ptype}_detail"]["time"]= "#{params["hour"]}:#{params["min"]}"
        end

        unless params[:property][:country].blank?
          if params[:property][:country].downcase == "new zealand"
            params[:property][:re_suburb] = params[:property][:suburb] if params[:property][:re_suburb].blank?
            params[:property][:rca_suburb] = params[:property][:suburb] if params[:property][:rca_suburb].blank?
            params[:property][:re_state] = params[:property][:state] if params[:property][:re_state].blank?
            params[:property][:rca_state] = params[:property][:state] if params[:property][:rca_state].blank?
            params[:property][:re_postcode] = params[:property][:zipcode] if params[:property][:re_postcode].blank?
            params[:property][:rca_postcode] = params[:property][:zipcode] if params[:property][:rca_postcode].blank?
          end
        end

        @property = pclass.new params[:property]
        @detail = @property.build_detail params["#{params[:action]}_detail"]

        @property.description = decoded_and_converted_to(@property.description)
        # here we need pass deal type in advance
        # commercial sale/lease detail will check its related commercial's deal type
        # but at this time commercial is not saved, has no id, so commercial detail
        # can't find its related commercial property and can't know the deal type
        @property.detail.unavailable_date = params[:unavailable_date] if @property.detail.class.name.underscore == "holiday_lease_detail"
        @detail.deal_type = @property.deal_type if @detail.respond_to?(:deal_type)
        @detail.property_type = @property.property_type if @detail.respond_to?(:property_type)
        @detail.country = @property.country if @detail.respond_to?(:country)
        save_rental_seasons if params[:rental_season]

        @feature_ids = @property.feature_ids
        # duplicate a holiday lease property from residential lease if the
        # options is selected.
        holiday_lease_duplicate if params[:holiday_duplication]
        # @property.agent_user = if current_user.is_a? AgentUser
        #   current_user
        # else
        #   current_office.agent_users.first
        # end

        if @property.new_record?
          if current_user.allow_property_owner_access?
            @property.agent_user_id =  current_user.id
          else
            if @detail.class == NewDevelopmentDetail
              contact = @office.agent_users.find(:first, :conditions => "roles.id = 3 OR roles.id = 4 OR roles.id = 5", :include => "roles")
              @property.agent_user_id =  contact.id
              @property.primary_contact_id =  contact.id
            else
              if @ptype == "commercial_building"
                if params[:property][:primary_contact_id].blank?
                  @property.agent_user_id = current_user.id
                  @property.status = "1"
                else
                  @property.agent_user_id = params[:property][:primary_contact_id]
                end
              else
                @property.agent_user_id = params[:property][:primary_contact_id]
              end
            end
          end

          if (current_user.allow_property_owner_access? and @office.property_payment_required) or (current_user.allow_property_owner_access? and (@office.property_owner_access and @office.property_draft_for_new))
            @property.status = 6
            @property.role_6_mode = true
            is_send = true
          end

        end

        @property.office = current_office
        @property.agent = current_agent

        if params[:property][:save_status] == "Draft"
          @property.status = 6
          @property.deactivation = true
          is_send = false
        end

        if params[:action] == "residential_sale" || params[:action] == "residential_lease" || params[:action] == "holiday_lease" || params[:action] == "business_sale" || params[:action] == "project_sale" || params[:action] == "commercial" || params[:action] == "clearing_sale"
          unless params[:parent_category].blank? && params[:parent_listing_id].blank?
            @property.is_children_listing = "1"
          else
            @property.is_children_listing = "0"
          end
        end

        if params[:action] == "residential_sale"
          unless params[:residential_sale_detail][:number_of_floors].blank?
            @property.number_of_floors = params[:residential_sale_detail][:number_of_floors]
          end
        end

        if params[:action] == "residential_lease"
          unless params[:residential_lease_detail][:number_of_floors].blank?
            @property.number_of_floors = params[:residential_lease_detail][:number_of_floors]
          end
        end

        if params[:action] == "holiday_lease"
          unless params[:holiday_lease_detail][:number_of_floors].blank?
            @property.number_of_floors = params[:holiday_lease_detail][:number_of_floors]
          end
        end

        if params[:action] == "commercial"
          unless params[:commercial_detail][:number_of_floors].blank?
            @property.number_of_floors = params[:commercial_detail][:number_of_floors]
          end
        end

        if params[:action] == "general_sale"
          unless params[:general_sale_detail][:number_of_floors].blank?
            @property.number_of_floors = params[:general_sale_detail][:number_of_floors]
          end
        end

        if params[:action] == "livestock_sale"
          unless params[:livestock_sale_detail][:number_of_floors].blank?
            @property.number_of_floors = params[:livestock_sale_detail][:number_of_floors]
          end
        end

        if params[:action] == "clearing_sales_event"
          unless params[:clearing_sales_event_detail][:number_of_floors].blank?
            @property.number_of_floors = params[:clearing_sales_event_detail][:number_of_floors]
          end
        end

        if params[:action] == "clearing_sale"
          unless params[:clearing_sale_detail][:number_of_floors].blank?
            @property.number_of_floors = params[:clearing_sale_detail][:number_of_floors]
          end
        end

        if @ptype == "commercial_building"
          save_property = @property.save(false)
        else
          save_property = @property.save
        end

        if save_property
          unless params[:property][:parent_listing_id].blank?
            parent_property =  Property.find(params[:property][:parent_listing_id])
            child_property = Property.find_all_by_parent_listing_id(params[:property][:parent_listing_id])

            if child_property.size > 0
              parent_property.update_attributes(:is_have_children_listing => "1")
            end
          end
          if params[:action] == "residential_sale"
            @extra = Extra.new({
                :property_id => @property.id,
                :office_id => @property.office_id,
                :field_name1 => "Floor Coverings",
                :field_name2 => "Aspect",
                :field_name3 => "Stamp Duty Payable",
                :field_name4 => "Estimated Stamp Duty Saving",
                :field_name5 => "Owners Corporation Cost",
                :field_name6 => "Notes",
                :field_name7 => "Deposit Paid",
                :field_name8 => "Finance",
                :field_name9 => "FIRB",
                :field_name10 => "Flooring Option",
                :field_name11 => "Settle Date",
                :field_name12 => "Trans Rec.",
                :field_name13 => "Dis Let",
                :field_name14 => "Sett"
              })
            @extra.save
          end
          tmp_contact_id = create_contact
          contact_id = tmp_contact_id.blank? ? 'NULL' : tmp_contact_id
          tmp_external_agent_id = create_external_agent
          external_agent_id = tmp_external_agent_id.blank? ? 'NULL' : tmp_external_agent_id
          ActiveRecord::Base.connection.execute("UPDATE properties SET new_project_rea = 1 WHERE id = #{@property.id}") if ["Development", "Land"].include?(@property.property_type)
          ActiveRecord::Base.connection.execute("UPDATE properties SET agent_contact_id = #{contact_id} WHERE id = #{@property.id}")
          ActiveRecord::Base.connection.execute("UPDATE properties SET external_agent_contact_id = #{external_agent_id} WHERE id = #{@property.id}")
          ActiveRecord::Base.connection.execute("UPDATE properties SET deactivation = NULL WHERE id = #{@property.id}") if @property.status == 1
          @property.agent_contact.create_note(@property.id, "Property Listed", 19, current_user.id) unless @property.agent_contact_id.nil?
          users = @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
          users.each do |user|
            begin
              UserMailer.deliver_listing_notification_for_level1(user, current_user, @office, @property) if is_send
            rescue
            end
          end
          country = Country.find_by_name(@office.country)
          init_exports(country.portal_countries)
          #          unless %w(new_development commercial_building land_release).include?(params[:action])
          session[:parent_category] = nil
          session[:parent_listing_id] = nil
          #          end
          redirect_to(agent_office_property_media_path(@agent,@office,@property)) and return
        end
        @property.description= decoded_and_converted_to(@property.description)
      else
        @property = pclass.new
        @detail = @property.build_detail
      end
    end
    @countries = Country.find(:all,:conditions=>["display_country=?",true]).map{|c| [c.name, c.name]}.unshift(['Please select', nil])
    @country ||= Country.find_by_name(@office.country)
    @vendors = @office.agent_users.map{|u| ["#{u.full_name} (#{u.phone})", u.id]}.unshift(['Please select', nil])

    #    Role 3 	LEVEL 1
    #    Role 4 	LEVEL 2
    #    Role 5 	LEVEL 3
    #    Role 6 	LEVEL 4
    #    Role 7 	LEVEL 5


    contacts = normal_contact
    @contacts = ( contacts.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq ) unless contacts.blank?
    @parent = Property.find(:all, :conditions => "agent_id = '#{@agent.id}' AND office_id = '#{@office.id}' AND type IN ('CommercialBuilding','NewDevelopment','LandRelease') AND deleted_at IS NULL")
    @price_per = [["Monthly", Property::METRIC_MONTHLY], ["Weekly", Property::METRIC_WEEKLY], ["Quarterly", Property::METRIC_QARTERLY], ["Yearly", Property::METRIC_YEARLY]]
    @range = (1..30).map{|x|[x,x]}.unshift(["Please select", ''])
    #TODO: ratings should be specific from countries table. implement current_country.eer.to_select
    @parent_categories = [["Residential Building", "NewDevelopment"], ["Land Release", "LandRelease"], ["Building", "CommercialBuilding"]].map{|k, v| [k, v]}.unshift(['Please select', nil])
    @residential_building_options = Property.find(:all, :conditions => ["type = ? AND office_id = ? AND deleted_at IS NULL", "NewDevelopment", "#{current_office.id}"], :select => "id, suburb, street_number, unit_number, street, country, state, town_village")
    @land_release_options = Property.find(:all, :conditions => ["type = ? AND office_id = ? AND deleted_at IS NULL", "LandRelease", "#{current_office.id}"], :select => "id, suburb, street_number, unit_number, street, country, state, town_village")
    @commercial_building_options = Property.find(:all, :conditions => ["type = ? AND office_id = ? AND deleted_at IS NULL", "CommercialBuilding", "#{current_office.id}"], :select => "id, suburb, street_number, unit_number, street, country, state, town_village")
    @general_sale_options = Property.find(:all, :conditions => ["type = ? AND office_id = ? AND deleted_at IS NULL", "GeneralSale", "#{current_office.id}"], :select => "id, suburb, street_number, unit_number, street, country, state, town_village")
    @livestock_sale_options = Property.find(:all, :conditions => ["type = ? AND office_id = ? AND deleted_at IS NULL", "LivestockSale", "#{current_office.id}"], :select => "id, suburb, street_number, unit_number, street, country, state, town_village")
    @clearing_sales_event_options = Property.find(:all, :conditions => ["type = ? AND office_id = ? AND deleted_at IS NULL", "CLearingSalesEvent", "#{current_office.id}"], :select => "id, suburb, street_number, unit_number, street, country, state, town_village")

    @ratings = (1..30).map{|i| [i,i]}.unshift(['Please select', nil])
    @area_metrics = ['Acres', 'Hectares', 'Square Feets', 'Square Metres', 'Square Yards'].map{|m| [m,m]}.unshift(['Please select',nil])
    @floors = (1..100).map{|i| [i,i]}.unshift(['Please select', nil])
    @price_period = ['Please select', 'Weekly', 'Monthly', 'Annually']
    @periods = %w(Week Month Quarter Year).map{|s| "Per #{s}"}.unshift(['Please select', nil])
    @land_area_metrics = ['Acres', 'Hectares', 'Square Feet', 'Square Metres', 'Square Yards'].map{|m| [m,m]}.unshift(['Please select',nil])
    @floor_area_metrics = ['Squares', 'Square Feet', 'Square Metres', 'Square Yards'].map{|m| [m,m]}.unshift(['Please select',nil])
    @office_area_metrics = ['Squares', 'Square Feet', 'Square Metres', 'Square Yards'].map{|m| [m,m]}.unshift(['Please select',nil])
    @warehouse_area_metrics = ['Squares', 'Square Feet', 'Square Metres', 'Square Yards'].map{|m| [m,m]}.unshift(['Please select',nil])
    @retail_area_metrics = ['Squares', 'Square Feet', 'Square Metres', 'Square Yards'].map{|m| [m,m]}.unshift(['Please select',nil])
    @other_area_metrics = ['Squares', 'Square Feet', 'Square Metres', 'Square Yards'].map{|m| [m,m]}.unshift(['Please select',nil])
    @depth_metrics = ['Metres', 'Feet', 'Yards'].map{|m| [m,m]}.unshift(['Please select',nil])
    @land_crossover = ['Left', 'Right', 'Center'].map{|m| [m,m]}.unshift(['Please select',nil])
    @price_per_period = ['Please select', 'Nightly', 'Weekly', 'Monthly']
    @length_metrics = ['Metres', 'Feet', 'Yards'].map{|l| [l, l]}.unshift(['Please select', nil])
    @pre_defined_features = Feature::ALL_PRE_DEFINED.inject([]) do |ary, name|
      ary << Kernel.const_get(name).send(@ptype)
    end
    @your_features = @office.user_defined_features.send @ptype
    @property.display_price = 1 if (@property.new_record? and @property.errors.blank?)
    @help_data = ActiveHelp.first.data
    @fee_terms = ['Not Applicable','By Owner', 'No Fee Broker', 'Fee Broker']
    @method_of_sale = ['Exclusive','Open', 'Conjunctional', 'Sole Agency']
    @authority = ['For Sale','Auction', 'Sale by Negotiation', 'Tender', 'Expression of Interest', 'Offers to Purchase']
    @contact_types = ['Normal','Conjunctional', 'Network']
    @current_leaseds = [['Please select',nil], ['Vacant Possession', 0], ['Tenanted Investment', 1]]
    @rent_types = ['Gross Rent', 'Net Rent'].map{|m| [m,m]}.unshift(['Please select',nil])
    @auction_hours = (0..23).map{|x| x.to_s.size < 2 ? ["0"+x.to_s, "0"+x.to_s] : [x.to_s,x.to_s] }.unshift(['-',nil])
    @auction_mins = (0..59).step(5).map{|x| x.to_s.size < 2 ? ["0"+x.to_s, "0"+x.to_s] : [x.to_s,x.to_s] }.unshift(["-", nil])
    @project_developments = Property.find(:all, :conditions => ["`type` = 'NewDevelopment' And `status` != 6 And `office_id` = ?", params[:office_id]]).map{|c| [c.detail.development_name, c.id]}.unshift(['Please select',nil])
    @project_design_types = ProjectDesignType.find(:all, :conditions => ["`office_id` = ?", params[:office_id]]).map{|c| [c.name, c.id]}.unshift(['Please select',nil])
    @project_styles = ProjectStyle.find(:all, :conditions => ["`office_id` = ?", params[:office_id]]).map{|c| [c.name, c.id]}.unshift(['Please select',nil])
    @new_development_categories = NewDevelopmentCategory.find(:all, :conditions => ["`property_id` = ?", params[:id]]).map{|c| [c.name]}
    @tenancy_options = [['Please select',nil],['Direct lease',1],['Sub-lease',2],['Assignment of lease',3],
      ['Surrender of lease',4]]
    @hours = (0..23).map{|x| x.to_s.size < 2 ? ["0"+x.to_s, "0"+x.to_s] : [x.to_s,x.to_s] }.unshift(['-',nil])
    @mins = (0..59).step(5).map{|x| x.to_s.size < 2 ? ["0"+x.to_s, "0"+x.to_s] : [x.to_s,x.to_s] }.unshift(["-", nil])
  end

  def holiday_lease_duplicate
    holiday_lease = HolidayLease.new(@property.attributes)
    feature_ids = @property.features.map do |f|
      feature = Feature.find(:first, :conditions => ['name = ? and property_type = ?', f.name, 'HolidayLease'])
      feature ? feature.id : nil
    end
    holiday_lease.feature_ids = feature_ids.compact
    detail = holiday_lease.build_detail(@property.detail.attributes.reject{|k, v| ['residential_lease_id', 'date_available', 'fee_terms'].include?(k)})
    holiday_lease.save(false) && detail.save(false)
  end

  def save_rental_seasons
    params[:rental_season].each_with_index do |rental_season,index|
      #      rental_season[:start_date] = change_date_format(rental_season[:start_date])
      #      rental_season[:end_date] = change_date_format(rental_season[:end_date])
      save_rental_sessions = RentalSeason.new(rental_season)
      @property.rental_seasons << save_rental_sessions
    end
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
  end

  def nil_session_listing
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end

  def ary_make_hash(ary, val)
    ary.inject({}) do |hash, el|
      hash[el] = val
      hash
    end
  end

  def uniq(ary)
    ary = ary.dup
    ary.uniq!
    ary
  end

  def uniq!(ary)
    hash = ary_make_hash(ary, 0)
    return nil if ary.length == hash.length

    j = 0
    (0...ary.length).each do |idx|
      if hash.delete(ary[idx])
        ary[j] = ary[idx]
        j += 1
      end
    end
    ary.slice!(0, j)
    ary
  end

  def convert_to_i
    @property.price = @property.price.to_i unless @property.price.blank?
    @property.leased_price = @property.leased_price.to_i unless @property.leased_price.blank?
    @detail = @property.detail
    unless @detail.blank?
      if @detail.class == ResidentialSaleDetail
        @detail.condo_strata_fee =  @detail.condo_strata_fee.to_i unless @detail.condo_strata_fee.blank?
        @detail.estimate_rental_return =  @detail.estimate_rental_return.to_i unless @detail.estimate_rental_return.blank?
      end

      if @detail.class == CommercialDetail
        @detail.current_rent = @detail.current_rent.to_i unless @detail.current_rent.blank?
        @detail.return_percent = @detail.return_percent.to_i unless @detail.return_percent.blank?
        @detail.bond = @detail.bond.to_i unless @detail.bond.blank?
        @detail.annual_outgoings = @detail.annual_outgoings.to_i unless @detail.annual_outgoings.blank?
      end

      if @detail.class == ProjectSaleDetail
        @detail.porch_terrace_area = @detail.porch_terrace_area.to_i unless @detail.porch_terrace_area.blank?
        @detail.land_width = @detail.land_width.to_i unless @detail.land_width.blank?
        @detail.land_depth = @detail.land_depth.to_i unless @detail.land_depth.blank?
        @detail.garage_area  = @detail.garage_area.to_i unless @detail.garage_area .blank?
        @detail.estimate_rental_return =  @detail.estimate_rental_return.to_i unless @detail.estimate_rental_return.blank?
      end
      if @detail.class == HolidayLeaseDetail
        @detail.mid_season_price = @detail.mid_season_price.to_i unless @detail.mid_season_price.blank?
        @detail.high_season_price = @detail.high_season_price.to_i  unless @detail.high_season_price.blank?
        @detail.peak_season_price = @detail.peak_season_price.to_i unless @detail.peak_season_price.blank?
      end
      if @detail.class == BusinessSaleDetail
        @detail.current_rent = @detail.current_rent.to_i unless @detail.current_rent.blank?
        @detail.current_outgoings = @detail.current_outgoings.to_i unless @detail.current_outgoings.blank?
        @detail.annual_turnover = @detail.annual_turnover.to_i unless @detail.annual_turnover.blank?
        @detail.annual_gross_profit = @detail.annual_gross_profit.to_i unless @detail.annual_gross_profit.blank?
        @detail.annual_ebit = @detail.annual_ebit.to_i unless @detail.annual_ebit.blank?
      end
    end
  end

  def init_rental_seasons
    @ordered_days = []
    @normal_seasons = @property.get_rental_seasons('normal')
    @normalsn = {}
    unless @normal_seasons[0].nil? || @normal_seasons[0].new_record?
      unless @normal_seasons[0].start_date.blank? || @normal_seasons[0].end_date.blank?
        @normalsn = {:start_date => (@normal_seasons[0].start_date.strftime("%m/%d/%Y") unless @normal_seasons[0].start_date.blank?), :end_date => (@normal_seasons[0].end_date.strftime("%m/%d/%Y") unless @normal_seasons[0].end_date.blank?), :id => @normal_seasons[0].id }
        @normal_seasons[0].ordered_days.each do |d|
          @ordered_days << { :ordered => d.date.strftime("%m/%d/%Y"), :id => d.id }
        end
      end
    end
    @mid_seasons = @property.get_rental_seasons('mid')
    @midsn = []
    @mid_seasons.each do |mid_season|
      unless mid_season.start_date.blank? && mid_season.end_date.blank?
        @midsn << {:start_date => (mid_season.start_date.strftime("%m/%d/%Y") unless mid_season.start_date.blank?), :end_date => (mid_season.end_date.strftime("%m/%d/%Y") unless mid_season.end_date.blank?), :id => mid_season.id } unless mid_season.new_record?
        mid_season.ordered_days.each do |d|
          @ordered_days << { :ordered => d.date.strftime("%m/%d/%Y"), :id => d.id }
        end
      end
    end
    @high_seasons = @property.get_rental_seasons('high')
    @highsn = []
    @high_seasons.each do |high_season|
      unless high_season.start_date.blank? && high_season.end_date.blank?
        @highsn << {:start_date => (high_season.start_date.strftime("%m/%d/%Y") unless high_season.start_date.blank?), :end_date => (high_season.end_date.strftime("%m/%d/%Y") unless high_season.end_date.blank?), :id => high_season.id} unless high_season.new_record?
        high_season.ordered_days.each do |d|
          @ordered_days << { :ordered => d.date.strftime("%m/%d/%Y"), :id => d.id }
        end
      end
    end
    @peak_seasons = @property.get_rental_seasons('peak')
    @peaksn = []
    @peak_seasons.each do |peak_season|
      unless peak_season.start_date.blank? && peak_season.end_date.blank?
        @peaksn << {:start_date => (peak_season.start_date.strftime("%m/%d/%Y") unless peak_season.start_date.blank?), :end_date => (peak_season.end_date.strftime("%m/%d/%Y") unless peak_season.end_date.blank?), :id => peak_season.id} unless peak_season.new_record?
        peak_season.ordered_days.each do |d|
          @ordered_days << { :ordered => d.date.strftime("%m/%d/%Y"), :id => d.id }
        end
      end
    end
    @seasons = {:normal_season => @normalsn, :mid_season => @midsn, :high_season => @highsn, :peak_season => @peaksn, :ordered_days => @ordered_days}
  end

  def add_export_list(portal, checked, disable)
    id = portal.id
    portal_name = portal.display_name.present? ? portal.display_name : portal.portal_name
    return "<div class='export_list'><input type='checkbox' name='property_portals[]' id='ck#{id}' onchange='update_checkbox(this);' value='#{id}' #{checked} #{disable}/>&nbsp;#{portal_name}</div>"
  end

  def add_addtional_export_list(id, checked, portal_name, disable)
    return "<div class='export_list'><input type='checkbox' onchange='update_checkbox(this);' name='property_portals[]' id='addtional_#{id}' value='#{id}' #{checked} #{disable}/>&nbsp;#{portal_name}</div>"
  end

  def add_domain_export_list(id, checked, domain_name, disable)
    return "<div class='export_list'><input type='checkbox' name='domain_export[]' id='domain_export_#{id}' value='#{id}' #{checked} #{disable}/>&nbsp;#{domain_name}</div>"
  end

  def create_contact
    category_id = ["residential_lease", "commercial", "holiday_lease"].include?(@ptype) ? 5 : 2
    unless params[:property][:agent_contact_id].blank?
      agent_contact= AgentContact.find_by_id(params[:property][:agent_contact_id])
      agent_contact.update_category_contact(category_id) unless agent_contact.blank?
      return params[:property][:agent_contact_id]
    else
      team = []
      team << params[:property][:primary_contact_id] unless params[:property][:primary_contact_id].blank?
      team << params[:property][:secondary_contact_id] unless params[:property][:secondary_contact_id].blank?

      contact = {:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id],
        :first_name => params[:property][:vendor_first_name], :last_name => params[:property][:vendor_last_name],
        :email => params[:property][:vendor_email], :contact_number => params[:property][:vendor_phone],
        :home_number => params[:property][:home_number], :work_number => params[:property][:work_number],
        :mobile_number => params[:property][:mobile_number], :contact_type => "Individual", :creator_id => current_user.id
      }

      @agent_contact = AgentContact.new(contact)
      if @agent_contact.save
        @agent_contact.add_categories([category_id])
        @agent_contact.add_assigns(team)
        @agent_contact.add_access(team)
        @agent_contact.use_spawn_for_boom
        @agent_contact.use_spawn_for_md
        @agent_contact.use_spawn_for_irealty
        return @agent_contact.id
      else
        return nil
      end

    end
  end

  def create_external_agent
    unless params[:property][:external_agent_contact_id].blank?
      external_agent= ExternalAgentContact.find_by_id(params[:property][:external_agent_contact_id])
      return params[:property][:external_agent_contact_id]
    else
      team = []
      team << params[:property][:primary_contact_id] unless params[:property][:primary_contact_id].blank?
      team << params[:property][:secondary_contact_id] unless params[:property][:secondary_contact_id].blank?

      external_agent = {:agent_id => params[:agent_id], :office_id => params[:office_id], :first_name => params[:property][:external_agent_first_name],
        :last_name => params[:property][:external_agent_last_name], :email => params[:property][:external_agent_email], :contact_number => params[:property][:external_agent_phone],
        :home_number => params[:property][:home_number], :work_number => params[:property][:work_number], :mobile_number => params[:property][:mobile_number], :creator_id => current_user.id, :type_contact => "External"}

      @external_agent = ExternalAgentContact.new(external_agent)
      if @external_agent.save
        return @external_agent.id
      else
        return nil
      end

    end
  end

  def init_exports(portal_countries)
    unless portal_countries.blank?
      allowed = [1,5,6].include?(@property.status)
      if allowed
        portal_countries.each do |p_c|
          portal_autosubcribe = PortalAutosubcribe.find_by_portal_id_and_office_id(p_c.portal.id,@office.id)
          unless portal_autosubcribe.blank?
            cek = PropertyPortalExport.find_by_property_id_and_portal_id(@property.id,p_c.portal.id)
            PropertyPortalExport.create(:property_id => @property.id, :portal_id => p_c.portal.id) if cek.blank?
          end
        end
      end
    end
  end

  def update_property_status(new = false)
    if new && !@purchaser_contact.nil?
      if @property.type.to_s == "Commercial"
        if @property.deal_type.to_s == "Lease"
          description = "Date = #{Date.today}, Price = #{params[:tenancy][:rent]}"
        else
          description = "Date = "+params[:transaction][:date] +", Price = "+params[:transaction][:amount] +', Display Price = '+ (params[:transaction][:display_price] == '1' ? "Yes": "No")
        end
      else
        description = "Date = "+params[:property][:date] +", Price = "+params[:property][:amount] +', Display Price = '+ (params[:property][:display_price] == '1' ? "Yes": "No")
      end
      agent_contact= AgentContact.find_by_id(@purchaser_contact.id)
      unless agent_contact.blank?
        #note type = Purchase Property
        agent_contact.create_note(@property.id, description, 8, current_user.id)
        note_type_id = 9  #lease property
        note_type_id = 8  if (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
        #note type for property
        agent_contact.create_note_for_property(@property.id, description, note_type_id, "", current_user.id)
        @purchaser.update_attributes({:contact_note_id => @purchaser_contact.id})
      end
    end

    last_status = @property.status
    #@property.attributes = params[:property]
    if (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
      @property.sold_on = params[:property][:date]
      @property.sold_price = params[:property][:amount]
    else
      @property.leased_on = params[:property][:date]
      @property.leased_price = params[:property][:amount]
    end
    @property.show_price = params[:property][:display_price]
    @property.status = params[:property][:status]
    if current_user.allow_property_owner_access? and (@office.property_owner_access and @office.property_draft_for_updated)
      @property.status = 6
      @property.role_6_mode = true
    end

    (last_status != 1 && @property.status == 1) ? activation = true : activation= nil
    @property.activation = activation
    @property.deactivation = nil if @property.status != 6

    if (last_status == 2 || last_status == 3 || last_status == '7') && (@property.status == 1)
      @property.purchaser.destroy unless @property.purchaser.blank?
      @property.sold_on = @property.sold_price = @property.leased_on = @property.leased_price = @property.show_price = nil
    end

    if (params[:property][:status] == '2') || (params[:property][:status] == '3') || (params[:property][:status] == '4') || (params[:property][:status] == '7')
      Opentime.destroy_all(:property_id => params[:id])
    end

    if (params[:property][:status] == '4') || (params[:property][:status] == '6')
      if @property.extra.blank?
        @extra = Extra.new({:property_id => @property.id, :office_id => @office.id, :featured_property => false})
      else
        @extra = @property.extra
        @extra.update_attribute(:featured_property, false)
      end
    end

    @property.updated_at = Time.now
    property_changes = {"property" => @property.changes.delete_if{|k,v| k == "save_status" || k == "activation"}} if @property.changes.present?
    ChangesLog.create_log(@property, "", property_changes, current_user, @office, 2)
    @property.save(false)
  end

  def find_header
    get_header_footer(@office.id, "Brochure Header")
  end

  def normal_contact
    if current_office.property_owner_access && current_office.property_with_owner_details && current_user.allow_property_owner_access? #access_level == 4
      contacts = @office.agent_users.find(:all, :conditions => ["id =?", current_user.id], :order => "`users`.first_name ASC")
    else
      if current_office.property_owner_access && current_user.limited_client_access? #access_level == 3
        contacts = @office.agent_users.find(:all, :conditions => "roles.id = 4 OR (users.id = #{current_user.id} and roles.id != 7)", :include => "roles", :order => "`users`.first_name ASC")
      else
        if current_office.property_owner_access && current_user.allow_property_owner_access? # access_level == 4
          contacts = @office.agent_users.find(:all, :conditions => "roles.id = 4 OR roles.id = 5", :include => "roles", :order => "`users`.first_name ASC")
        else
          if current_office.property_owner_access
            contacts = @office.agent_users.find(:all, :conditions => "roles.id = 3 OR roles.id = 4 OR roles.id = 5", :include => "roles", :order => "`users`.first_name ASC")
          else
            contacts = @office.agent_users.find(:all, :conditions => "roles.id != 7", :include => "roles", :order => "`users`.first_name ASC")
          end
          if current_office.property_with_owner_details
            contacts = @office.agent_users.find(:all, :conditions => "roles.id = 6", :include => "roles", :order => "`users`.first_name ASC")
          end
        end
      end
    end
    if current_user.allow_property_owner_access?
      contacts = @office.agent_users.find(:all, :conditions => "roles.id = 4", :include => "roles", :order => "`users`.first_name ASC")
    end
    contacts = @office.agent_users.find(:all, :conditions => "roles.id != 7", :include => "roles", :order => "`users`.first_name ASC") if contacts.blank?
    return contacts
  end

  def check_level3
    if current_user.limited_client_access? && !@office.control_level3
      render_optional_error_file(401)
    end
  end

  def check_level4
    if current_user.allow_property_owner_access?  && @office.control_level4
      render_optional_error_file(401)
    end
  end

  def check_hide_preview
    if current_user.allow_property_owner_access?  && @office.hide_preview
      render_optional_error_file(401)
    end
  end

  def check_hide_exports
    if current_user.allow_property_owner_access?  && @office.hide_exports
      render_optional_error_file(401)
    end
  end

  def check_hide_brochure
    if current_user.allow_property_owner_access?  && @office.hide_brochure
      render_optional_error_file(401)
    end
  end

  def check_permission_create
    case params[:action]
    when "new_development"
      unless @office.new_development
        flash[:notice] = 'You are not have permission to create Residential Building, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "land_release"
      unless @office.land_release
        flash[:notice] = 'You are not have permission to create Land Release, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "commercial_building"
      unless @office.commercial_building
        flash[:notice] = 'You are not have permission to create Building, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "residential_sale"
      unless @office.residential_sale
        flash[:notice] = 'You are not have permission to create Residential Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "residential_lease"
      unless @office.residential_lease
        flash[:notice] = 'You are not have permission to create Residential Lease, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "commercial"
      unless @office.commercial
        flash[:notice] = 'You are not have permission to create Commercial, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "project_sale"
      unless @office.project_sale
        flash[:notice] = 'You are not have permission to create House & Land, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "holiday_lease"
      unless @office.holiday_lease
        flash[:notice] = 'You are not have permission to create Holiday Lease, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "business_sale"
      unless @office.business_sale
        flash[:notice] = 'You are not have permission to create Business Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "general_sale"
      unless @office.general_sale
        flash[:notice] = 'You are not have permission to create General Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "livestock_sale"
      unless @office.livestock_sale
        flash[:notice] = 'You are not have permission to create Livestock Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "clearing_sales_event"
      unless @office.clearing_sales_event
        flash[:notice] = 'You are not have permission to create Clearing Sales Event, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "clearing_sale"
      unless @office.clearing_sale
        flash[:notice] = 'You are not have permission to create Clearing Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    end
  end

  def check_permission_edit
    @property = Property.find(params[:id])
    case @property.class.to_s
    when "NewDevelopment"
      unless @office.new_development
        flash[:notice] = 'You are not have permission to edit Residential Building, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "LandRelease"
      unless @office.land_release
        flash[:notice] = 'You are not have permission to edit Land Release, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "CommercialBuilding"
      unless @office.commercial_building
        flash[:notice] = 'You are not have permission to edit Building, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "ResidentialSale"
      unless @office.residential_sale
        flash[:notice] = 'You are not have permission to edit Residential Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "ResidentialLease"
      unless @office.residential_lease
        flash[:notice] = 'You are not have permission to edit Residential Lease, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "Commercial"
      unless @office.commercial
        flash[:notice] = 'You are not have permission to edit Commercial, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "ProjectSale"
      unless @office.project_sale
        flash[:notice] = 'You are not have permission to edit House & Land, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "HolidayLease"
      unless @office.holiday_lease
        flash[:notice] = 'You are not have permission to edit Holiday Lease, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "BusinessSale"
      unless @office.business_sale
        flash[:notice] = 'You are not have permission to edit Business Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "GeneralSale"
      unless @office.general_sale
        flash[:notice] = 'You are not have permission to edit General Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "LivestockSale"
      unless @office.livestock_sale
        flash[:notice] = 'You are not have permission to edit Livestock Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "ClearingSalesEvent"
      unless @office.clearing_sales_event
        flash[:notice] = 'You are not have permission to edit Clearing Sales Event, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    when "ClearingSale"
      unless @office.clearing_sale
        flash[:notice] = 'You are not have permission to edit Clearing Sale, please change your Listing Type setting first'
        redirect_to agent_office_path(current_agent, current_office)
      end
    end
  end

  def update_property_export_settings
    @property = Property.find(params[:id])
    old_export = PropertyDomainExport.find(:all, :conditions => "property_id = #{params[:id]} and send_api IS TRUE")
    if @property.deactivation == true
      PropertyDomainExport.update_all({:send_api => false}, {:property_id => params[:id]})
      PropertyDomainExport.update_all({:send_api => true}, {:id => params[:domain_export]}) unless params[:domain_export].blank?
    else
      PropertyDomainExport.update_all({:send_api => true}, {:id => params[:domain_export]}) unless params[:domain_export].blank?
    end
    begin
      new_export = PropertyDomainExport.find(:all, :conditions => "property_id = #{params[:id]} and send_api IS TRUE")
      old = old_export.present? ? old_export.map{|a| a.domain_id} : []
      current = new_export.present? ? new_export.map{|a| a.domain_id} : []
      domain_changes = {"old_domain" => old, "current_domain" => current} if old.present? || current.present?
      old_portals = @property.property_portal_exports.reject{|a| a.uncheck == true}
      old_ids = old_portals.present? ? old_portals.map(&:portal_id) : []
    rescue
    end
    unless params[:property_portals].blank?
      (@property.deactivation == true && @property.status == 6) || @property.status == 7 ? @property.add_property_portal(params[:property_portals]) : @property.add_property_portal(params[:property_portals], true)
    else
      @property.add_property_portal([0]) if (@property.deactivation == true && @property.status == 6) || @property.status == 7
    end
    @property.updated_at = Time.now

    @property.save(false)

    property = Property.find(@property.id)
    current_portal_ids = property.property_portal_exports.reject{|a| a.uncheck == true}.map(&:portal_id) rescue []
    if (old_ids <=> current_portal_ids) != 0
      portal_changes = {"current_portal_ids" => current_portal_ids.sort, "old_portal_ids" => old_ids.sort}
    end
    if domain_changes.present? || portal_changes.present?
      updated_attributes = {"domain_changes" => domain_changes, "portal_changes" => portal_changes}
      ChangesLog.create_log(@property, nil, updated_attributes, current_user, @office, 2)
    end

    flash[:notice] = 'Export was successfully updated.'
  end
end
