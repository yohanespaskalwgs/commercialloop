class AgentUsersController < ApplicationController
  layout 'agent'

  before_filter :login_required, :except => [:new,:create_level3,:create_level4,:team_notification]
  before_filter :get_agent_and_office, :except => [:toggle_active_help, :close_button_per_page]
  before_filter :check_level3, :only =>[:edit]
  before_filter :allow_display_conjunction, :only => [:conjunctional]
  before_filter :assign_group, :only => [:new, :create, :edit, :update,:create_level3,:create_level4]
  skip_before_filter :verify_authenticity_token, :only => [:search,:create_level4,:load_photo]


  def team_notification
    @agent_user = AgentUser.find_by_sql("SELECT * FROM users WHERE type LIKE 'AgentUser' AND id = #{params[:id]}").first
    unless params[:processed].blank?
      GLOBAL_ATTR.delete_if{|k,v| v == params[:token]}
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE users SET processed = #{params[:processed].to_i} WHERE id = #{@agent_user.id}"
      sql.update "UPDATE users SET cron_checked = #{params[:processed].to_i} WHERE id = #{@agent_user.id}"
      sql.commit_db_transaction

      id_jobs = []
      jobs = Delayed::Job.all
      unless jobs.blank?
        jobs.each do |job|
          handler = job.handler.split
          id_jobs << job.id if handler[handler.size-2].include?("agent_user_id") && @agent_user.id.to_i == handler.last.to_i
        end
      end
      Delayed::Job.destroy(id_jobs) unless id_jobs.blank?
      return render(:text => "Agent user with id : #{@agent_user.id} is successfully updated")
    else
      return render(:text => "false")
    end
  end

  def index
    if current_user.limited_client_access? || current_user.allow_property_owner_access?
      @agent_users_paginate = AgentUser.paginate(:all, :conditions => ["`users`.id = ? And `users`.office_id='#{@office.id}' And roles.id != 7",current_user.id], :include => "roles", :order => "`users`.position ASC", :page =>params[:page], :per_page => 100) #, :per_page => 24)
      @agent_users = @agent_users_paginate.in_groups_of(3,false)
    else
      @agent_users_paginate = AgentUser.paginate(:all, :conditions => "`users`.office_id='#{@office.id}' And roles.id != 7", :include => "roles", :order => "`users`.position ASC", :page =>params[:page], :per_page => 100)  #, :per_page => 45)
      @agent_users = @agent_users_paginate.in_groups_of(3,false)
    end
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Team Management' AND user_type = 'AgentUser'")

    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # Modify access levels
  # LEVEL 1
  # LEVEL 2
  # LEVEL 3
  def preference
    if request.post?
      params[:levels].each do |id, level|
        @office.agent_users.find(id).access_level = level
      end
      flash.now[:notice] = "Preferences updated."
    end
    @agent_users= AgentUser.find(:all, :conditions => "`office_id`='#{@office.id}' And roles.id != 7", :include => "roles",:order => "users.first_name ASC")
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Update Team Preference' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
  end

  def conjunctional
    @agent_users = AgentUser.find(:all, :conditions => "`developer_id`='#{@agent.developer_id}' And roles.id = 7", :include => "roles",:order => "users.first_name ASC")
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Update Team Preference' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
  end

  def show
    @agent_user = @office.agent_users.find(params[:id])
    redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/edit"
    return
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def new
    if request.url().scan("level4").blank?
      find_office_agent
      if request.url().scan("level3").blank?
        unless logged_in?
          redirect_to find_user_home_path and return
        end
        @agent_user = AgentUser.new
        @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add a Team Member' AND user_type = 'AgentUser'")
        @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
        respond_to do |format|
          if params[:conjunctional]
            format.html { render :file => "/agent_users/create_conjunctional", :layout => "agent"}
          else
            format.html # new.html.erb
          end
        end
      else
        #add conjunctional for iframe
        @type = request.url().scan("conjunctional").blank? ? "" : "Conjunctional"
        @conjunctional_type = request.url().scan("conjunctional").blank? ? false : true
        @contact_type = params[:contact_type]
        @agent_user = AgentUser.new
        respond_to do |format|
         format.html { render :file => "/agent_users/create_level3", :layout => false}
        end
      end
    else
      if @office.property_owner_access
        @agent_user = AgentUser.new
        respond_to do |format|
          format.html { render :file => "/agent_users/create_level4", :layout => "new_level4"}
        end
      else
        flash[:notice] = "The Office does not allow property access"
        redirect_back_or_default('/')
      end
    end
  end

  def edit
    @agent_user = @office.agent_users.find(params[:id], :include =>[:testimonials] )
    @agent_user.description= Iconv.new('UTF-8//IGNORE', 'UTF-8').iconv(@agent_user.description)
    @errors = flash[:error_hash] unless flash[:error_hash].blank?
    if @agent_user.office.office_contact == @agent_user.id
     @can_delete = false
    else
     @can_delete = true
    end
    find_office_agent
    landscapes = @agent_user.landscape_image
    unless landscapes.blank?
      landscapes.each do |img|
        if img.position == 1
          @img_1 = img
        else
          @img_2 = img
        end
      end
    end
    portraits = @agent_user.portrait_image
    unless portraits.blank?
      portraits.each do |img_portrait|
        if img_portrait.position == 1
          @img_portrait_1 = img_portrait
        else
          @img_portrait_2 = img_portrait
        end
      end
    end

    @vcard = @agent_user.vcard
    @audio = @agent_user.mp3
    @conjunctional = params[:conjunctional]

    @group_ids = @list_groups = ""
    @agent_user.user_groups.each{|ugroup|
      group = ugroup.group
      @group_ids = @group_ids + '#' + group
      @list_groups = @list_groups + '<span id="'+group+'" style="margin-right:20px;">'+group+'<a href="javascript:remove_group(\''+group+'\')">x</a></span>'
    } unless @agent_user.user_groups.blank?
  end

  def edit_conjunctional
    @agent_user = @office.agent_users.find(params[:id])
    unless @agent_user.conjunctional.blank?
      @agent_user.conjunctional_office = @agent_user.conjunctional.conjunctional_office_name
      @agent_user.conjunctional_rea_agent_id = @agent_user.conjunctional.conjunctional_rea_agent_id
    end
    find_office_agent
  end

  def create
    @agent_user = @office.agent_users.build(params[:agent_user])
    if @office.property_owner_access?
      role_id = 6
    else
      role_id = 4
    end
    @agent_user.roles << Role.find(role_id)
    @agent_user.active_help_status = true
    @agent_user.group = params[:agent_user][:group]
    @agent_user.display_on_site = params[:agent_user][:display_on_site]
    respond_to do |format|
      if @agent_user.save
        @active_helps = ActiveHelpPage.find_by_sql("SELECT * FROM active_help_pages WHERE user_type = 'AgentUser'")
        @agent_user.active_help_pages = @active_helps
        ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id = #{@agent_user.id}")
        UserMailer.deliver_agent_user_creation(@agent_user, @office) unless params[:notification].blank?
        flash.now[:notice] = 'AgentUser was successfully created.'
        format.html { redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/edit" }
      else
        format.html { render :action => "new" }
      end
    end
  end

  def create_conjunctional
    params[:agent_user][:login] = params[:agent_user][:first_name].blank? ? "quickadduser" : (params[:agent_user][:first_name].downcase + "." + params[:agent_user][:last_name].downcase)
    @agent_user = @office.agent_users.build(params[:agent_user])
    @agent_user.roles << Role.find(7)
    @agent_user.active_help_status = true
    @agent_user.group = "Other"
    @agent_user.activated_at = Time.now
    @agent_user.display_on_site  = true
    find_office_agent
    if @agent_user.save
      flash[:notice] = "Conjunctional Agent successfully added"
      redirect_to new_agent_office_agent_user_path(@agent,@office)+"?conjunctional=true"
    else
      respond_to do |format|
        format.html
      end
    end
  end

  def create_level3
    params[:agent_user][:login] = params[:agent_user][:first_name].blank? ? "quickadduser" : (params[:agent_user][:first_name].downcase + "." + params[:agent_user][:last_name].downcase)
    role_id = params[:agent_user][:conjunctional_id].blank? ? 5 : 7

    @agent_user = @office.agent_users.build(params[:agent_user])
    @agent_user.roles << Role.find(role_id)
    @agent_user.active_help_status = true
    @agent_user.group = "Other"
    @agent_user.activated_at = Time.now
    @agent_user.display_on_site  = (params[:type_agent] == 'Conjunctional' ? true : false)
    @type = (params[:type_agent] == 'Conjunctional' ? "Conjunctional" : "")
    @conjunctional_type = (params[:type_agent] == 'Conjunctional' ? true : false)
    find_office_agent
    @contact_type = params[:contact_type]
    if @agent_user.save
      flash[:notice] = "#{params[:type_agent]} Agent successfully added"
      redirect_to new_agent_office_agent_user_path(@agent,@office)+"?level3&contact_type=#{@contact_type}#{(@conjunctional_type == true ? '&conjunctional=true' :'')}"
    else
      respond_to do |format|
        format.html { render :layout => false}
      end
    end
  end

  def create_level4
    @agent_user = @office.agent_users.build(params[:agent_user])
    @agent_user.roles << Role.find(6)
    @agent_user.active_help_status = true
    @agent_user.group = "Other"
    @agent_user.activated_at = Time.now
    if @agent_user.save
      @active_helps = ActiveHelpPage.find_by_sql("SELECT * FROM active_help_pages WHERE user_type = 'AgentUser'")
      @agent_user.active_help_pages = @active_helps
      ActiveRecord::Base.connection.execute("UPDATE active_help_pages_users SET status=1 WHERE user_id = #{@agent_user.id}")
      spawn(:kill => true) do
        UserMailer.deliver_agent_user_creation(@agent_user, @office)
        @office.agent_users.each do |u|
          UserMailer.deliver_agent_user_notification(u, @office, @agent_user) if u.full_access?
        end
      end
      user = User.authenticate(params[:agent_user][:login], params[:agent_user][:password])
      flash[:notice] = "Logged in successfully"
      self.current_user = user
      redirect_to continue_path
#      redirect_to agent_office_path(user.agent,user.office)
    else
      respond_to do |format|
        format.html { render :layout => "new_level4"}
      end
    end
  end

  def update
    @agent_user = @office.agent_users.find(params[:id])
    @conjunctional = params[:conjunctional]
    respond_to do |format|
      update_vcard(format) if params[:vcard]
      update_agent_user(format) if params[:agent_user]
    end
  end

  def destroy
    @agent_user = AgentUser.find(params[:id])
    user = @agent_user.office.agent_users.find(:first, :conditions => "roles.id = 3 AND type = 'AgentUser' AND users.id !='#{params[:id]}'", :include => "roles")
    unless user.blank?
     ActiveRecord::Base.connection.execute("UPDATE users SET deleted_at = '#{Time.now.strftime("%Y-%m-%d")}' WHERE id = #{@agent_user.id}")
     agent_user_id = @agent_user.id
     @agent_user.update_attribute(:updated_at, Time.now)
     spawn(:kill => true) do
       # wait 10 seconds before first trying send to third party
       sleep 10
       property_agent_users = Property.find(:all, :conditions => "agent_user_id = #{agent_user_id}")
       property_agent_users.each{|p|
         p.agent_user_id = user.id
         p.updated_at = Time.now
         p.save(false)
       } unless property_agent_users.blank?

       property_primary_contacts = Property.find(:all, :conditions => "primary_contact_id = #{agent_user_id}")
       property_primary_contacts.each{|p|
         p.primary_contact_id = user.id
         p.updated_at = Time.now
         p.save(false)
       } unless property_primary_contacts.blank?

       property_secondary_contacts = Property.find(:all, :conditions => "secondary_contact_id = #{agent_user_id}")
       property_secondary_contacts.each{|p|
         p.secondary_contact_id = user.id
         p.updated_at = Time.now
         p.save(false)
       } unless property_secondary_contacts.blank?

       property_third_contacts = Property.find(:all, :conditions => "third_contact_id = #{agent_user_id}")
       property_third_contacts.each{|p|
         p.third_contact_id = user.id
         p.updated_at = Time.now
         p.save(false)
       } unless property_third_contacts.blank?
     end
     if params[:to_index].present?
       flash[:notice] = "Team member successfully deleted."
       redirect_to agent_office_agent_users_path(@agent,@office)
     else
       return render(:json =>true)
     end
    else
     if params[:to_index].present?
      flash[:notice] = "Team member not found."
      redirect_to agent_office_agent_users_path(@agent,@office)
     else
      return render(:json =>false)
     end
    end
  end

  def delete_photo
    begin
      @agent_user = AgentUser.find(params[:agent_user_id])
      image = Kernel.const_get(params[:type].to_s.classify).find_by_attachable_id_and_position(@agent_user.id,params[:position])

      unless image.blank?
        image.old_images = nil
        if image.destroy
          @agent_user.update_attribute(:updated_at, Time.now)
          if params[:redirect] == 'true'
            redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/load_photo?type_image=#{params[:type]}&position=#{params[:position]}"
          else
            return (render(:json =>"1"))
          end
        else
          if params[:redirect] == 'true'
            redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/load_photo?type_image=#{params[:type]}&position=#{params[:position]}"
          else
            return (render(:json =>"0"))
          end
        end
      else
        if params[:redirect] == 'true'
          redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/load_photo?type_image=#{params[:type]}&position=#{params[:position]}"
        else
          return (render(:json =>"-1"))
        end
      end
    rescue Exception => ex
    end
  end

  def sort_users
    @agent_users = @office.agent_users.find(:all, :order => "`users`.position ASC")
    if @office.id == 1
      page = params[:page].present? ? params[:page].to_i : 1
      if page > 1
        card_idx = params[:agentuser].size * (page - 1)
        @counter = 1
        @agent_users.each_with_index do |user, idx|
          if params[:agentuser].index(user.id.to_s).present? && user.position >= card_idx && @counter <= params[:agentuser].size
            @counter = @counter + 1
            Log.create(:message => "params[:agentuser].size: #{params[:agentuser].size} user.position: #{user.position} @counter: #{@counter} INDEX param: #{params[:agentuser].index(user.id.to_s)} user.id.to_s : #{user.id.to_s}")
            user.update_attribute(:position, (params[:agentuser].index(user.id.to_s).blank?) ? params[:agentuser].size : card_idx + params[:agentuser].index(user.id.to_s) + 1)
          else
            user.update_attribute(:position, idx + 1)
          end
        end
      else
        @agent_users.each_with_index do |user, idx|
          if idx + 1 <= params[:agentuser].size
            user.update_attribute(:position, (params[:agentuser].index(user.id.to_s).blank?) ? params[:agentuser].size : params[:agentuser].index(user.id.to_s) + 1)
          else
            user.update_attribute(:position, idx + 1)
          end
        end
      end
    else
      @agent_users.each do |user|
        user.update_attribute(:position, (params[:agentuser].index(user.id.to_s).blank?) ? params[:agentuser].size : params[:agentuser].index(user.id.to_s) + 1)
      end
    end
  end

  def toggle_active_help
    render :update do |page|
      current_user.toggle_activate_help!
      page.replace_html :active_help_area, :partial => 'layouts/active_help'
      page.call :toggle_active_help, current_user.active_help_status
    end
  end

  def close_button_per_page
    render :update do |page|
      current_user.close_per_page(params[:id])
      page.call :toggle_active_help, false
    end
  end

  def check_user
    @agent_user = AgentUser.find(params[:id])
    if @agent_user.office.office_contact == @agent_user.id
     return render(:json => 0)
    else
     return render(:json => 1)
    end
  end

  def load_photo
    @agent_user = @office.agent_users.find(params[:id], :include =>[:testimonials] )
    if params[:formimage]
      type = params[:formimage][:type_image]
      if !type.blank?
        image = Kernel.const_get(type.to_s.classify).new(params[:formimage])
        image.old_images = @agent_user.send type
        image.attachable = @agent_user
        image.source = "team_member"
        page = "load_photo"
        if params[:no_crop].present?
          image.no_resize = true
        end
        if image.content_type.to_s == "image/jpeg" || image.content_type.to_s == "image/pjpeg"
          if image.save
            @agent_user.reload
            @agent_user.update_attribute(:updated_at, Time.now)
            page = "crop_photo"
          end
        else
          flash[:notice] = 'Please upload image for jpg format.'
        end
        if params[:no_crop].blank?
          redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/#{page}?type_image=#{params[:formimage][:type_image]}&position=#{params[:formimage][:position]}"
        else
          redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)
        end
      else
        redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)
      end
    else
      if !params[:type_image].blank?
        @type = params[:type_image]
        @position = params[:position]
      else
        redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)
      end
    end
  end

  def crop_photo
    @agent_user = @office.agent_users.find(params[:id], :include =>[:testimonials] )
    @type = params[:type_image]
    @position = params[:position]
    if @type == 'landscape_image'
      landscape = @agent_user.landscape_image.find_by_position(@position)
      unless landscape.blank?
        @image_file = landscape
        unless landscape.width.blank?
          @width = landscape.width < 600 ? landscape.width : 600
          @height = landscape.width < 600 ? landscape.height : (600*landscape.height)/landscape.width
        end
      end
      @dot_width = (@position == '1') ? 250 : 500
      @dot_height = (@position == '1') ? 125 : 250

      @prewidth = 200
      @preheight = 100
    else
      portrait = @agent_user.portrait_image.find_by_position(@position)
      unless portrait.blank?
        @image_file = portrait
        unless portrait.width.blank?
          @width = portrait.width < 600 ? portrait.width : 600
          @height = portrait.width < 600 ? portrait.height : (600*portrait.height)/portrait.width
        end
      end
      @dot_width = (@position == '1') ? 100 : 300
      @dot_height = (@position == '1') ? 125 : 375

      @prewidth = (@position == '1') ? 100 : 200
      @preheight = (@position == '1') ? 125 : 250
    end
  end

  def exec_crop
    @agent_user = @office.agent_users.find(params[:id], :include =>[:testimonials] )
    type = params[:type_image]
    position = params[:position]
    if type == 'landscape_image'
      landscape = @agent_user.landscape_image.find_by_position(position)
      image_file = landscape unless landscape.blank?
      new_width = (position == '1') ? 250 : 500
      new_height = (position == '1') ? 125 : 250
    else
      portrait = @agent_user.portrait_image.find_by_position(position)
      image_file = portrait unless portrait.blank?
      new_width = (position == '1') ? 100 : 300
      new_height = (position == '1') ? 125 : 375
    end

    unless image_file.blank?
      pic = Magick::ImageList.new(image_file.public_filename.to_s)
      cropped = pic.crop(params[:x_image].to_i,params[:y_image].to_i,params[:w_image].to_i,params[:h_image].to_i)
      resize = cropped.resize(new_width, new_height)
      image_file.filename = image_file.filename.gsub(".jpg","_crop.jpg")
      temporary = "#{RAILS_ROOT}/tmp/#{image_file.filename}"
      temporary.gsub!(".jpg","-#{image_file.id}.jpg")
      resize.write(temporary)
      image_file.moved_to_s3 = false
      uploaded = image_file.upload_to_s3(temporary)
      try = 0
      while try < 5 && !uploaded
        try = try + 1
        uploaded = image_file.upload_to_s3(temporary)
        sleep(5)
      end
      ActiveRecord::Base.connection.execute("UPDATE attachments SET filename='#{image_file.filename}' WHERE id = #{image_file.id}")
      if FileTest.exist?(temporary)
        File.delete(temporary) if uploaded
      end
    end

    @agent_user.reload
    @agent_user.update_attribute(:updated_at, Time.now)
    redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)
  end

  def search
    agent_users = AgentUser.find(:all, :conditions => "office_id = #{@office.id} and (first_name LIKE \"%#{params[:search_value]}%\" OR last_name LIKE \"%#{params[:search_value]}%\" OR email LIKE \"%#{params[:search_value]}%\")")
    jsons = []
    unless agent_users.blank?
      agent_users.each{|ac|
        unless ac.blank?
        jsons << {'id' => ac.id, 'first_name' => "#{ac.first_name}", 'last_name' =>"#{ac.last_name}",'email' =>"#{ac.email}", 'href' => edit_agent_office_agent_user_path(@agent,@office,ac.id)}
        end
      } unless agent_users.nil?
      text = {"results" => jsons, "total" => (agent_users ? agent_users.length : 0) }
    else
      text = ''
    end
    render :json => text
  end

  private

  def find_office_agent
    conjunctionals = Conjunctional.find(:all, :order => "`conjunctional_office_name` ASC")
    @offices_agent = conjunctionals.map{|u| [u.conjunctional_office_name, u.id]}.uniq.unshift(['Please select', nil])
  end

  def update_image(type, format)

    # don't use @agent_user.build_landscape_image here, it can't handle
    # empty upload

    image = Kernel.const_get(type.to_s.classify).new(params[type])
    image.old_images = @agent_user.send type
    image.attachable = @agent_user
    if image.save
      @agent_user.reload
      @agent_user.update_attribute(:updated_at, Time.now)
      flash[:notice] = 'Image was successfully saved.'
    else
      flash[:notice] = 'Failed to save image'
    end
    format.html { redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/edit"}
  end

  def update_agent_user(format)
    @agent_user.facebook_username = !params[:agent_user][:facebook_username].nil? ? params[:agent_user][:facebook_username] : @agent_user.facebook_username
    @agent_user.twitter_username = !params[:agent_user][:twitter_username].nil? ?  params[:agent_user][:twitter_username] : @agent_user.twitter_username
    @agent_user.linkedin_username = !params[:agent_user][:linkedin_username].nil? ? params[:agent_user][:linkedin_username] : @agent_user.linkedin_username
    @agent_user.group = !params[:agent_user][:group].blank? ? params[:agent_user][:group] : @agent_user.group
    @agent_user.video_url = !params[:agent_user][:video_url].nil? ? params[:agent_user][:video_url] : @agent_user.video_url
    @agent_user.video_code = !params[:agent_user][:video_code].nil? ? params[:agent_user][:video_code] : @agent_user.video_code
    @agent_user.display_on_site = !params[:agent_user][:display_on_site].nil? ? params[:agent_user][:display_on_site] : @agent_user.display_on_site
    @agent_user.display_on_team_page = !params[:agent_user][:display_on_team_page].nil? ? params[:agent_user][:display_on_team_page] : @agent_user.display_on_team_page
    @agent_user.website_url = !params[:agent_user][:website_url].nil? ? params[:agent_user][:website_url] : @agent_user.website_url
    if params[:agent_user][:conjunctional_id].blank?
      unless @agent_user.conjunctional_id.blank?
        new_conjunctional = Conjunctional.find_by_id(@agent_user.conjunctional_id)
        new_conjunctional.update_attributes({:conjunctional_rea_agent_id => params[:agent_user][:conjunctional_rea_agent_id], :conjunctional_office_name => params[:agent_user][:conjunctional_office]}) unless new_conjunctional.blank?
      else
        new_conjunctional = Conjunctional.create({:conjunctional_rea_agent_id => params[:agent_user][:conjunctional_rea_agent_id], :conjunctional_office_name => params[:agent_user][:conjunctional_office]}) unless params[:agent_user][:conjunctional_office].blank?
      end
      params[:agent_user][:conjunctional_id] = new_conjunctional.id if new_conjunctional
    end
    @agent_user.conjunctional_id = params[:agent_user][:conjunctional_id]

    begin
      @agent_user.generate_sequential_code if @agent_user.code.blank?
    rescue Exception => ex
      v = @office.agent_users.length
      @agent_user.code = ("%03d" % v)
    end

    @agent_user.description= Iconv.new('UTF-8//IGNORE', 'UTF-8').iconv(@agent_user.description)

    if @agent_user.update_attributes(params[:agent_user])
      update_audio if params[:agent_user][:uploaded_data]
      @properties = Property.find(:all,:conditions =>["agent_user_id = ? OR primary_contact_id = ? OR secondary_contact_id = ?",@agent_user.id,@agent_user.id,@agent_user.id])
      unless @properties.blank?
        @properties.each do |property|
          property.team_member_api = true
          ActiveRecord::Base.connection.execute("UPDATE properties SET updated_at = '#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}' WHERE id = #{property.id}") if property.status.to_i == 1 || property.status.to_i == 5
        end
      else
        flash[:notice] = 'User was successfully updated but Properties failed to update'
      end

      unless params[:group_ids].blank?
        group_ids = params[:group_ids].split("#").delete_if{|x| x.blank?}
        unless group_ids.blank?
          ActiveRecord::Base.connection.execute("DELETE FROM user_groups WHERE agent_user_id = '#{@agent_user.id}'")
          group_ids.each{|group| @agent_user.user_groups << UserGroup.create(:group => group) unless group.blank?}
        end
      else
        ActiveRecord::Base.connection.execute("DELETE FROM user_groups WHERE agent_user_id = '#{@agent_user.id}'")
      end
      flash[:notice] = 'User was successfully updated.'
    else
      flash[:error_hash] = {}
      @agent_user.errors.each do |x,y|
        x = "username" if x == "login"
        flash[:error_hash][x.humanize] = y.downcase
      end
    end

    format.html {
      if @conjunctional == 'true'
        redirect_to edit_conjunctional_agent_office_agent_user_path(@agent, @office, @agent_user)
      else
        redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/edit"
      end
    }
  end

  def assign_group
    @group = AgentUser::GROUP.collect{|group| group}.unshift(['Please select', nil])
  end

  def update_vcard(format)
    old_vcard = @agent_user.vcard
    vcard = Vcard.new(params[:vcard])
    vcard.attachable = @agent_user

    if vcard.save
      old_vcard.destroy unless old_vcard.blank?
      @agent_user.reload
      @agent_user.update_attribute(:updated_at, Time.now)
      flash[:notice] = 'Vcard was successfully saved.'
    else
      flash[:notice] = 'Failed to save vcard'
    end

    format.html { redirect_to agent_office_agent_user_path(@agent, @office, @agent_user)+"/edit" }
  end

  def update_audio
    old_audio = @agent_user.mp3
    audio = Mp3.new(params[:agent_user])
    audio.attachable = @agent_user

    if audio.save
      old_audio.destroy unless old_audio.blank?
      @agent_user.reload
      @agent_user.update_attribute(:updated_at, Time.now)
    end
  end

  def check_level3
    if current_user.limited_client_access? && current_user.id != params[:id].to_i && @office.control_level3
      render_optional_error_file(401)
    end
  end

  def allow_display_conjunction
    if !@agent.developer.show_conjunction
      render_optional_error_file(401)
    end
  end

end
