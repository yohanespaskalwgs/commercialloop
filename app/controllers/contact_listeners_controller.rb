class ContactListenersController < Api::ApiBaseController
  layout false
  before_filter :get_agent_and_office, :only => [:new, :new_heard_about_us, :new_group_contact]
  before_filter :find_agent_or_developer_by_access_key, :only => [:create, :add_heard_about_us, :heard_about_us_list, :add_group_contact, :group_contact_list]
  skip_before_filter :verify_authenticity_token

  def index
  end

  def show
  end

  def new
    #localhost:3000/agents/1/offices/1/contact_listeners/new
    if params[:id]
    au = AgentUser.find_by_id(params[:id])
    unless au.blank?
    landscapes = au.landscape_image
    unless landscapes.blank?
      landscapes.each do |img|
        if img.position == 1
          @img_1 = img.public_filename
        else
          @img_2 = img.public_filename
        end
      end
    end

    portraits = au.portrait_image
    unless portraits.blank?
      portraits.each do |img_portrait|
        if img_portrait.position == 1
          @img_portrait_1 = img_portrait.public_filename
        else
          @img_portrait_2 = img_portrait.public_filename
        end
      end
    end
    end
    end
  end

  def edit
  end

  def create
    @success = false
    @errors = @error_field = data = ""
    category_id = params[:category_id].blank? ? 8 : params[:category_id]
    @property = Property.find_by_id(params[:property_id]) unless params[:property_id].blank?
    country_id = nil
    unless params[:country].blank?
      country = Country.find_by_name(params[:country])
      country_id = country.id if country
    end

    if params[:already_have_account] == "true"
      if !@property.blank?
        if !params[:username].blank?
          @agent_contact = AgentContact.find(:first, :conditions => "username = '#{params[:username]}'")
          if !@agent_contact.blank?
            add_contact_to_vault
          else
            @errors = "Username is not found"
            @error_field = "username"
          end
        else
          @errors = "Username can't be blank"
          @error_field = "username"
        end
      else
        @errors = "Property can't be blank"
        @error_field = "property_id"
      end
    else
      require "base64"
      login_required = false
      login_required = true if (!params[:username].blank? and !params[:password].blank?)
      @contact = {:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id],
      :title => params[:title], :first_name => params[:first_name], :last_name => params[:last_name], :email => params[:email],
      :home_number => params[:home_number], :work_number => params[:work_number], :mobile_number => params[:mobile_number],
      :contact_number => params[:contact_number], :fax_number => params[:fax_number], :heard_from_id => params[:heard_about_us],
      :country_id => country_id, :suburb => params[:suburb], :state => params[:state], :city => params[:city], :street_name => params[:street], :post_code => params[:postcode], :email_format => params[:email_format], :company => params[:company],
      :username => params[:username], :login_required => login_required, :password => (Base64.encode64(params[:password]) unless params[:password].blank?)}

      if !params[:note_type].blank?
        assign_contact(@contact,category_id)
      else
        @errors = "Note type can't be blank"
        @error_field = "note_type"
      end
    end
    data = params.delete_if{|x,y| %w(action submit controller).include?(x)}

    contact_data = []
    contact_data << {:success => @success, :error_message => @errors, :error_fields => @error_field, :data => data}
    respond_to do |format|
      format.xml { render :xml => contact_data}
    end
  end

  def update
  end

  def destroy
  end

  def rails_services
  end

  def heard_about_us_list
    contact_data = HeardFrom.find(:all, :select => "id, name", :conditions => "status = 'heard_office'")
    respond_to do |format|
      format.xml { render :xml => contact_data}
    end
  end

  def new_heard_about_us
  end

  def add_heard_about_us
    #localhost:3000/agents/1/offices/1/contact_listeners/add_heard_about_us
    @success = false
    @errors = @error_field = data = ""
    if !params[:heard_about_us].blank?
      HeardFrom.create({:name => params[:heard_about_us], :status => "heard_office"})
      @success = true
    else
      @errors = "Heard About Us can't be blank"
      @error_field = "heard_about_us"
    end
    data = params.delete_if{|x,y| %w(action submit controller).include?(x)}

    heard_data = []
    heard_data << {:success => @success, :error_message => @errors, :error_fields => @error_field, :data => data}
    respond_to do |format|
      format.xml { render :xml => heard_data}
    end
  end

  def group_contact_list
    group_data = GroupContact.find(:all, :select => "id, name", :conditions => "office_id = '#{@office.id}'")
    respond_to do |format|
      format.xml { render :xml => group_data}
    end
  end

  def new_group_contact
  end

  def add_group_contact
    #localhost:3000/agents/1/offices/1/contact_listeners/add_group_contact
    @success = false
    @errors = @error_field = data = ""
    if !params[:group_contact].blank?
      GroupContact.create({:name => params[:group_contact], :office_id => @office.id})
      @success = true
    else
      @errors = "Group Contact can't be blank"
      @error_field = "group_contact"
    end
    data = params.delete_if{|x,y| %w(action submit controller).include?(x)}

    group_data = []
    group_data << {:success => @success, :error_message => @errors, :error_fields => @error_field, :data => data}
    respond_to do |format|
      format.xml { render :xml => group_data}
    end
  end

  def check_username
    #localhost:3000/agents/1/offices/1/contact_listeners/check_username
    @success = false
    @errors = @error_field = data = ""
    if !params[:username].blank?
      u = User.find(:first, :conditions => "login = '#{params[:username]}'")
      if !u.blank?
        @errors = "Username already exist"
        @error_field = "username"
      else
        ac = AgentContact.find(:first, :conditions => "username = '#{params[:username]}'")
        acm = AgentCompany.find(:first, :conditions => "username = '#{params[:username]}'")
        if !ac.blank?
          @errors = "Username already exist"
          @error_field = "username"
        elsif !acm.blank?
          @errors = "Username already exist"
          @error_field = "username"
        else
          @success = true
        end
      end
    else
      @errors = "Username can't be blank"
      @error_field = "username"
    end
    data = params.delete_if{|x,y| %w(action submit controller).include?(x)}

    check_username = []
    check_username << {:success => @success, :error_message => @errors, :error_fields => @error_field, :data => data}
    respond_to do |format|
      format.xml { render :xml => check_username}
    end
  end

  private

  def assign_contact(contact, category_id)
    detected = nil

    %w(email mobile_number contact_number work_number home_number).each{|w|
       unless params[w].blank?
          check = AgentContact.check_exist(contact, w)
          detected = check unless check.nil?
       end
    }

    if detected
      @agent_contact = AgentContact.find_by_id(detected.id)
      return unless @agent_contact.update_attributes(contact.delete_if{|x,y| y.blank?})
      create_related_data
      unless params[:group_id].blank?
        ids = params[:group_id].split('#')
        @agent_contact.add_groups(ids)
      end
      @success = true
      return
    end

    @agent_contact = AgentContact.new(contact)
    if @agent_contact.save
      @agent_contact.add_categories([category_id])

      create_related_data
      add_contact_to_vault if @office.activate_vault

      unless params[:group_id].blank?
        ids = params[:group_id].split('#')
        @agent_contact.add_groups(ids)
      end
      @agent_contact.use_spawn_for_boom
      @agent_contact.use_spawn_for_md
      @agent_contact.use_spawn_for_irealty
      @success = true
      return
    end

    err_first = @agent_contact.errors.on :first_name
    err_last = @agent_contact.errors.on :last_name

    @error_field = "first_name" unless err_first.blank?
    @error_field += ",last_name" unless err_last.blank?
    @errors = @agent_contact.errors
   end

  def create_related_data
    if params[:note_type] == '6' || params[:note_type] == '14'
      created_related_data_for_property_enquiry if @property
    else
      created_related_data_for_contact
    end
  end

  def add_contact_to_vault
    unless @property.blank?
      if (!@agent_contact.username.blank? and !@agent_contact.password.blank?)
        vault = @property.vault
        vault = Vault.create({:office_id =>	@property.office.id, :property_id => @property.id, :access_time => 0, :required_password => 0}) if @property.vault.blank?
        vc = VaultContact.find(:first, :conditions => "property_id = #{@property.id} And agent_contact_id = #{@agent_contact.id}")
        VaultContact.create({:vault_id => vault.id, :property_id => @property.id, :agent_contact_id => @agent_contact.id}) if vc.blank?
        ContactNote.create({:agent_contact_id => @agent_contact.id, :agent_user_id => @property.primary_contact_id, :note_type_id => 25, :description => "this user is granted access to property", :address => @property.address.to_s, :property_id => @property.id, :note_date => Time.now})
        spawn(:kill => true) do
          begin
            if @property.office.vault_template.blank?
              vt = VaultTemplate.find_by_office_id(1)
              VaultTemplate.create({:office_id=> @property.office_id, :headline => vt.headline, :content => vt.content, :enable_autoresponder => true})
            end
            unless @property.office.vault_template.blank?
              ContactMailer.deliver_send_vault(@property, @property.primary_contact, @property.office.vault_template, @agent_contact) if (@agent_contact.username != '' and @agent_contact.password != '') and @property.office.vault_template.enable_autoresponder == true
            end
          rescue Exception => ex
            Log.create({:message => "Failed vault send email for #{@agent_contact.id} -->  #{ex.inspect}"})
          end
        end
      end
    end
  end

  def created_related_data_for_contact
    description = params[:note]
    description += " + " if params[:contract].to_i == 1 && !params[:note].blank?
    description += "Take Contract/Application" if params[:contract].to_i == 1
    note_id = @agent_contact.create_note_for_contact(params[:note_type], description, nil, params[:address]) unless params[:note_type] == '4'
    @agent_contact.create_alert_for_contact(params, nil, "api") if params[:alert].to_i == 1
  end

  def created_related_data_for_property_enquiry
    #note type for property = Property Enquiry
    property_contact_enquiry = PropertyContactEnquiry.find(:first, :conditions => ["`property_id` = ? and `agent_contact_id` = ? ", @property.id, @agent_contact.id])
    PropertyContactEnquiry.create(:property_id => @property.id, :agent_contact_id => @agent_contact.id) unless property_contact_enquiry

    @agent_contact.create_note_for_property(@property.id, (params[:note].blank? ? "Property Enquiry" : params[:note]), 14, params[:heard_from_id], nil)
    @agent_contact.create_note(@property.id, (params[:note].blank? ? "Property Enquiry" : params[:note]), 6, nil)

    #note type for property = Property Enquiry and take contract
    @agent_contact.create_note_for_property(@property.id, "Take Contract/Application", 16, params[:heard_from_id], nil) if params[:contract].to_i == 1

    #alert Alerts Sent
    @agent_contact.create_alert(@property.id, nil, (params[:alert_type].blank? ? nil : params[:alert_type])) if params[:alert].to_i == 1
  end
end
