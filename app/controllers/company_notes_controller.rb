class CompanyNotesController < ApplicationController
  layout 'agent'

  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation

  def index
     @company_note = CompanyNote.new
  end

  def show
  end

  def new
  end

  def edit
    @company_note = CompanyNote.find(params[:id])
    @company_note.note_date = Time.now.strftime("%m/%d/%Y") if @company_note.note_date.nil?
  end

  def create
    params[:company_note][:note_date] = Time.now if params[:company_note][:note_date].blank?
    @company_note = CompanyNote.new(params[:company_note])

    respond_to do |format|
      if @company_note.save
        @company_note.create_offer
        flash[:notice] = 'Company Note was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
   @company_note = CompanyNote.find(params[:id])

    respond_to do |format|
      if @company_note.update_attributes(params[:company_note])
        flash[:notice] = 'Company Note was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @company_note = CompanyNote.find(params[:id])
    @company_note.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def prepare_creation
    @agent_company = AgentCompany.find(params[:agent_company_id])
    @notes_type = NoteType.find(:all, :conditions => ["`status` = 'company_note'"]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @team_members = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    @notes = @agent_company.company_notes.paginate(:all, :order => "`created_at` DESC", :page =>params[:page], :per_page => 30)
  end
end
