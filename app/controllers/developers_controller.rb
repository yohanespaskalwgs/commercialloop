class DevelopersController < ApplicationController
  layout 'developer'
  require_role 'DeveloperUser'
  require_role 'AdminUser', :only => %w(index new destroy)
  before_filter :get_developer, :only => %w(show edit update)
  before_filter :get_developer_req_by_ajax, :only => [:updates,:quicklinks,:account_statistic,:client_messages,:subscriptions,:commercial_loop_messages,:dev_summary, :clients]


  skip_before_filter :verify_authenticity_token, :only => [:search]
  # GET /developers
  # GET /developers.xml
  def index
    @active = Developer.count('id', :conditions => "status = 'active'")
    @inactive = Developer.count('id', :conditions => "status = 'inactive'")
    @overdue = Developer.count('id', :conditions => "status = 'suspended'")
    @developers = Developer.find_where(:all) do |dev|
      dev.country == params[:country] if params[:country]
      dev.id == params[:developer] if params[:developer]
      dev.status == params[:status] if params[:status]
      #dev.payments == params[:payments] if params[:payments]
    end

    respond_to do |format|
      format.html { render :layout => 'admin' }
    end
  end

  # GET /developers/1
  # GET /developers/1.xml
  def show
    @body_id = "developer_dashboard"

    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'My Dashboard'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /developers/new
  # GET /developers/new.xml
  def new
    @developer = Developer.new

    respond_to do |format|
      format.html { render :layout => false } # new.html.erb
    end
  end


  # GET /developers/1/edit
  def edit
  end

  # POST /developers
  # POST /developers.xml
  def create
    @developer = Developer.new(params[:developer])
    @user = DeveloperUser.new(params[:user])
    @developer.developer_users << @user
    @developer.contact_person = @user.id

    respond_to do |format|
      if @developer.save
        @user.toggle_admin # <= obsolete?
        @user.access_level = 1
        flash[:notice] = 'Developer was successfully created.'
        format.html { redirect_to(@developer) }
      else
        p @user.errors
        format.html { render :action => "new", :layout => false }
      end
    end
  end

  # PUT /developers/1
  # PUT /developers/1.xml
  def update

    respond_to do |format|
      if @developer.update_attributes(params[:developer])
        flash[:notice] = 'Developer was successfully updated.'
        format.html { redirect_to(@developer) }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  # DELETE /developers/1
  # DELETE /developers/1.xml
  def destroy
    @developer = Developer.find(params[:id])
    @developer.destroy

    respond_to do |format|
      format.html { redirect_to(developers_url) }
      format.xml  { head :ok }
    end
  end

  def search
    developers = Developer.search(params[:q])
    jsons = []
    developers.each{|d| jsons << {'id' => d.id, 'name' => d.name, 'href' => developers_path(:developer => d.id)} } unless developers.nil?
    text = {"results" => jsons, "total" => developers.length }
    render :json => text
  end
  def inactive_help
    current_developer.deactivate_help!
    render :action => 'show'
  end

  def api_details
    @developer = Developer.find_by_id(params[:id])
    if @developer.access_key.blank? && @developer.status == "active"
      @developer.update_attribute("access_key",Digest::SHA1.hexdigest("#{Time.now}-#{@developer.id}"))
      @developer.update_attribute("private_key" ,Digest::SHA1.hexdigest(@developer.access_key) )
    end
    @agents = @developer.agents
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'API'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end

  def update_developer_api
    @developer = Developer.find(params[:id])
    @developer.xml_api_enable = params[:developer][:xml_api_enable]
    unless params[:domain].blank?
      is_filled = false
      params[:domain].each do |domain|
        domain_att = Domain.find(:all, :conditions => ["name = ?",domain[:name]])
        is_filled = true if !domain[:name].blank? && !domain[:ipn].blank? && params[:developer][:http_api_enable] == "1"
        is_updated = false
        portal_durl = domain[:name].downcase.gsub('http://', '')
        portal_durl = portal_durl.gsub('www.', '')
        portal = Portal.find(:first, :conditions => "`ftp_url` LIKE '%#{portal_durl}%'")
        unless portal.blank?
          pext = {"portal_export" => portal.http_api_enable}
        else
          pext = {"portal_export" => nil}
        end

        domain[:enable_export] = 0 if domain[:enable_export].blank?
        enable_export = domain[:enable_export]
        domain.delete(:enable_export)

        domain = domain.merge pext
        if domain_att
          if !domain_att.map{|x| x.developer_id}.include?(params[:id].to_i)
            domain_att = Domain.new(domain)
            domain_att.developer = (@developer)
            domain_att.enable_export = enable_export
          else
            domain_ex = Domain.find(:first, :conditions => ["name = ? and developer_id = ?",domain[:name], @developer.id])
            domain_ex.update_attribute(:enable_export, enable_export)
            is_updated = true if !domain_att.map{|x| x.ipn}.include?(domain[:ipn])
          end
        else
          domain_att = Domain.new(domain)
          domain_att.developer = (@developer)
          domain_att.enable_export = enable_export
        end

        if is_updated
          domain_att.is_a?(Array) ? domain_att.each { |dom| dom.update_attributes(domain) } : domain_att.update_attributes(domain)
        else
          domain_att.is_a?(Array) ? domain_att.each { |dom| dom.save } : domain_att.save
        end
      end
      dev_domains = Domain.find(:all, :conditions => ["developer_id = ?", params[:id].to_i])
      dev_domains.each do |dom|
        Domain.destroy(dom.id) if !params[:domain].map{|x| x[:name]}.include?(dom.name)
      end

      unless dev_domains.blank?
        @developer.offices.each do |of|
          dev_domains.each do |dev_dom|
            if of.domains.blank?
              domain_new = Domain.new(dev_dom.attributes)
              domain_new.office_id = of.id
              domain_new.developer_id = nil
              domain_new.save!
            else
              exist = 0
              of.domains.each do |of_dom|
                portal_durl = dev_dom.name.downcase.gsub('http://', '')
                portal_durl = portal_durl.gsub('www.', '')
                exist = 1 if of_dom.name.include?(portal_durl)
              end

              if exist == 0
                domain_new = Domain.new(dev_dom.attributes)
                domain_new.office_id = of.id
                domain_new.developer_id = nil
                domain_new.save!
              end
            end
          end
          if dev_domains.size > 0
            sql = ActiveRecord::Base.connection()
            sql.update "UPDATE offices SET xml_api_enable = 1, http_api_enable = 1 WHERE id = #{of.id}"
            sql.commit_db_transaction
          end
        end
      end

      @developer.http_api_enable = is_filled
    else
      @developer.http_api_enable = false
    end
    respond_to do |format|
      if @developer.save
        flash[:notice] = 'Developer was successfully updated.'
      else
        flash[:notice] = 'Developer failed to be updated.'
      end
      format.html { redirect_to api_details_developer_path(@developer) }
    end
  end

  def combined_exports
    @developer = Developer.find_by_id(params[:id])
    if @developer.access_key.blank? && @developer.status == "active"
      @developer.update_attribute("access_key",Digest::SHA1.hexdigest("#{Time.now}-#{@developer.id}"))
      @developer.update_attribute("private_key" ,Digest::SHA1.hexdigest(@developer.access_key) )
    end
    @agents = @developer.agents
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Exports'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
    unless @agents.blank?
      begin
        country = Country.find_by_name(@developer.agents.last.offices.last.country)
        @portal_countries = country.portal_countries
      rescue
      end
    end
  end

  def update_combined_exports
    unless params[:id].blank?
      unless params[:portal_feeds].blank?
        PortalCombined.destroy_all(:developer_id => params[:id])
        params[:portal_feeds].each{|portal_id|
          auto = false
          params[:portal_autos].each{|portal_auto_id| auto = true if portal_auto_id == portal_id }
          PortalCombined.create({:developer_id => params[:id], :portal_id => portal_id, :active => true, :autosubcribe => auto})
        }
        developer = Developer.find_by_id(params[:id])
        unless developer.blank?
          developer.agents.each do |agent|
            next if agent.offices.blank?
            agent.offices.each do |office|
              developer.portal_combineds.each do |pc|
                next if pc.portal.blank?
                portal_account = PortalAccount.find_by_sql("SELECT * FROM portal_accounts WHERE portal_name = '#{pc.portal.portal_name}' AND office_id = #{office.id}")[0]
                unless portal_account.blank?
                  portal_account.update_attributes({:active_feeds => true})
                else
                  office.portal_accounts << PortalAccount.create(:portal_name => pc.portal.portal_name, :active_feeds => 1)
                end
                ck_auto = office.portal_autosubcribes.find(:all, :conditions => {:portal_id => pc.portal.id})
                (office.portal_autosubcribes.create(:portal_id => pc.portal.id) if pc.autosubcribe == true) if ck_auto.blank?
              end
            end
          end
          spawn do
            sleep 30
            developer.agents.each do |agent|
              next if agent.offices.blank?
              agent.offices.each{|office| office.update_property_exports }
            end
          end
        end
        flash[:notice] = 'Developer was successfully updated.'
      end
    end
    redirect_to "/developers/#{developer.id}/combined_exports"
  end

  def ecampaign
    @developer = Developer.find_by_id(params[:id])
  end

  def update_ecampaign
    @developer = Developer.find_by_id(params[:id])
    respond_to do |format|
      if @developer.update_attributes(params[:developer])
        flash[:notice] = 'Developer was successfully updated.'
        format.html { redirect_to "/developers/#{@developer.id}/ecampaign" }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def trigger_domain
    if !params[:domain_id].blank?
      Log.create({:message => "Triggered for domain id #{params[:domain_id]}"})
      Delayed::Job.enqueue(TriggerDomainApi.new(params[:domain_id], params[:id]), 3, 1.seconds.from_now.getutc)
      flash[:notice] = 'Website was successfully triggered.'
    else
      flash[:notice] = 'Process triggering failed.'
    end
    respond_to do |format|
      format.html { redirect_to "/developers/#{params[:id]}/api_details" }
    end
  end

  def delete_developer_domain
    domain = Domain.find_by_id(params[:domain_id])
    if !domain.blank?
      if domain.destroy
        return(render(:json => true))
      else
        return(render(:json => false))
      end
    else
      return(render(:json => true))
    end
  end

  def dev_summary
    render :layout => false
  end

  def client_messages
    @client_messages = (current_user.client_messages if current_user.attribute_present?(:client_messages)) unless current_user.blank?
    render :layout => false
  end

  def commercial_loop_messages
    @commercial_loop_messages = (current_user.admin_messages if current_user.attribute_present?(:admin_messages)) unless current_user.blank?
    render :layout => false
  end

  def subscriptions
    @subscriptions = current_developer.subscriptions.all(:order => 'created_at desc', :limit => 5)
    render :layout => false
  end

  def clients
    @clients = current_developer.agents.all(:order => 'created_at desc', :limit => 5)
    render :layout => false
  end

  def updates
    @quicklinks = DeveloperQuicklink.first.data
    render :layout => false
  end

  def quick_links
    @quicklinks = DeveloperQuicklink.first.data
    render :layout => false
  end

  def account_statistic
    render :layout => false
  end

  def active_offices
    @developer = Developer.find_by_id(params[:id])
    if params[:status] == "1"
      @developer.update_attribute(:active_offices, 1)
      @subscriptions = Subscription.find(:all, :include => [{:office => {:agent => :developer}}], :conditions => ["agents.developer_id = ? AND offices.status != ? OR subscriptions.status != ?", params[:id], "active", "ok"])
      @subscriptions.each do |subs|
        subs.update_attribute(:status, "ok") if !subs.blank?
        subs.office.update_attribute(:status, "active") if !subs.office.blank?
      end
    else
      @developer.update_attribute(:active_offices, 0)
    end
    redirect_to :back
  end

  def support
    @developer = Developer.find_by_id(params[:id])
    if params[:status] == "1"
      @developer.update_attribute(:support, 1)
    else
      @developer.update_attribute(:support, 0)
    end
    redirect_to :back
  end

  def add_conjunctional_office
    @developer = Developer.find_by_id(params[:id])
    @conjuctional = Conjunctional.new
  end

  def create_conjunctional_office
    developer = Developer.find_by_id(params[:id])
    conjunctional = Conjunctional.new(params[:conjuctional])
    if conjunctional.save
      flash[:notice] = 'Conjunctional office successfully created.'
      redirect_to developer_offices_path(developer)
    else
      flash[:notice] = conjunctional.errors.full_messages
      redirect_to add_conjunctional_office_developer_path(developer)
    end
  end

  private
  def get_developer
    @developer = current_user.class.name.include?("AdminUser") ? Developer.find_by_id(params[:id]) : current_user.developer
    unless params[:id] == @developer.id.to_s
      render_optional_error_file(401) and return false
    end
  end

  def get_developer_req_by_ajax
    @developer = Developer.find_by_id(params[:id])
  end

end
