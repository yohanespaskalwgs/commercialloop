class AutoResponderController < ApplicationController
  layout 'agent'
  before_filter :login_required
  before_filter :get_agent_and_office

  include PropertiesHelper

  def index
    @vault_template = @office.vault_template || VaultTemplate.new
    @vault_template.headline = "The Vault Contact Access" if @vault_template.headline.blank? && request.subdomains[0] != "knightfrank"
    @vault_template.headline = "Your access details to the Knight Frank Vault.\"Confidential Documentation – %address%\"" if request.subdomains[0] == "knightfrank" && @vault_template.headline.blank?
  end

  def update_auto_responder
    if @office.vault_template.blank?
      params[:vault_template] = params[:vault_template].merge(:office_id => @office.id)
      @vault_template = VaultTemplate.new(params[:vault_template])
    else
      @vault_template = @office.vault_template
    end

    if @office.vault_template.blank?
      sv = @vault_template.save
    else
      sv = @vault_template.update_attributes(params[:vault_template])
    end

    flash[:notice] = 'Auto Responder has successfully been updated.'
    redirect_to agent_office_auto_responder_index_path(@agent, @office)
  end

end