class LivestockTypeParentsController < ApplicationController
  layout false

  require_role ['AgentUser','DeveloperUser']

  before_filter :get_agent_and_office

  #winarti@kiranatama.com
  #Oct 7, 2013
  #GET parents template
  def index
    @parents = LivestockType.parent_types
    respond_to do |format|
      format.html
    end
  end

  #winarti@kiranatama.com
  #Oct 7, 2013
  #GET new parent template
  def new
    @livestock_type = LivestockType.new
    respond_to do |format|
      format.html
    end
  end

  #winarti@kiranatama.com
  #Oct 7, 2013
  #PUT update parent process
  def update
    @livestock_type = LivestockType.find_by_id params[:id]
    respond_to do |format|
      if @livestock_type.update_attributes(params[:livestock_type])
        flash[:notice] = 'The parent type has been successfully updated'
        format.html {redirect_to agent_office_livestock_type_parents_path(@agent, @office)}
      else
        format.html {render :action => "edit_parent"}
      end
    end
  end

  #winarti@kiranatama.com
  #Oct 7, 2013
  #GET edit parent template
  def edit
    @livestock_type = LivestockType.find_by_id params[:id]
    respond_to {|format| format.html }
  end

  #winarti@kiranatama.com
  #Oct 7, 2013
  #DELETE parent process
  def destroy
    @livestock_type = LivestockType.find_by_id params[:id]
    respond_to do |format|
      if @livestock_type.destroy
        flash[:notice] = 'The parent type has been successfully deleted'
      else
        flash[:notice] = 'The parent type cant be deleted'
      end
      format.html {redirect_to agent_office_livestock_type_parents_path(@agent, @office)}
    end
  end

  #winarti@kiranatama.com
  #Oct 7, 2013
  #POST create parent process
  def create
    @livestock_type = LivestockType.new(params[:livestock_type])
    respond_to do |format|
      if @livestock_type.save
        flash[:notice] = 'New livestock type has been successfully created'

        format.html { redirect_to agent_office_livestock_type_parents_path(@agent,@office) }
        format.xml  { render :xml => @livestock_type, :status => :created, :location => @livestock_type }
      else
        format.html { render :action => "new_parent" }
        format.xml  { render :xml => @livestock_type.errors, :status => :unprocessable_entity }
      end
    end
  end

end
