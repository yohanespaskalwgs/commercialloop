class FilesController < ApplicationController
  layout 'agent'
  before_filter :login_required
  before_filter :get_agent_and_office, :get_categories

  def index
    @files = @office.files
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Files' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    if params[:order].blank? and params[:category].blank?
      @disk_usage_percent = "%.2f\%" % (100.0 * current_user.disk_usage.bytes / User::DISK_QUOTA.gigabytes)
    else
      @files = @files.select{|f| f.category == params[:category]} unless params[:category].blank?

      # use case for 2 reason:
      # * hide db column name
      # * faster than use f.send params[:order]
      block = case params[:order]
              when 'atoz' then proc {|a,b| a.filename <=> b.filename}
              when 'createdby' then proc {|a,b| a.attachable_id <=> b.attachable_id}
              when 'datetime' then proc {|a,b| a.created_at <=> b.created_at}
              when 'filesize' then proc {|a,b| a.size <=> b.size}
              when 'privateshared'
                proc {|a,b|
                  if a.private && !b.private
                    1
                  elsif !a.private && b.private
                    -1
                  else
                    0
                  end
                }
              else proc {|a,b| a.created_at <=> b.created_at}
              end
      # sort_by won't work with true/false, thus with sort by 'privateshared'
      @files = @files.sort &block
      render :partial => 'file', :collection => @files
    end
  end

  def show
    @file = @office.files.find params[:id]
    begin
      @file.upload_to_s3
      redirect_to @file.public_filename
      #send_file File.join(RAILS_ROOT, 'public', @file.public_filename), :type => @file.content_type, :disposition => 'inline'
    rescue Exception => ex
      flash[:notice] = 'File Not Found.'
      redirect_to :action => 'index'
    end
  end

  def new
    @file = UserFile.new
  end

  def edit
    @file = @office.files.find params[:id]
  end

  def create
    if params[:file][:category].blank?
      flash[:notice] = 'Please select file category.'
      redirect_to :action => 'new'
      return
    end

    @file = current_user.files.build params[:file]
    if @file.save
      flash.now[:notice] = 'File uploaded.'
      redirect_to :action => 'index'
    else
      flash[:notice] = 'Failed to upload this file.'
      render :action => 'new'
    end
  end

  def update
    if params[:file][:category].blank?
      flash[:notice] = 'Please select file category.'
      redirect_to :action => 'new'
      return
    end

    @file = @office.files.find params[:id]
    @file.attachable = current_user
    if @file.update_attributes(params[:file])
      flash.now[:notice] = 'File uploaded.'
      redirect_to :action => 'index'
    else
      flash[:notice] = 'Failed to upload this file.'
      render :action => 'new'
    end
  end

  def destroy
    @file = @office.files.find params[:id]
    @file.destroy
    #@file = current_user.files.find params[:id]
    redirect_to :action => 'index'
  end

  private
  def get_categories
    @categories = UserFileCategory.find(:all, :conditions => "`office_id` IS NULL OR `office_id` = #{@office.id}", :order => "name").map{|c| c.name}
  end

end
