class BrochuresController < ApplicationController
  before_filter :login_required,:except =>[:generate_pdf]
  before_filter :get_agent_and_office, :get_property
  before_filter :property_data, :only => [:manage_brochure, :preview, :generate_pdf]
  before_filter :check_permission_page
  skip_before_filter :verify_authenticity_token, :except => [:index, :create, :destroy]
  layout "agent"

  def index
    @brochure = @property.brochure || Brochure.new
    respond_to do |format|
      format.html
      format.js { render :layout => false }
    end
  end

  def create
    @brochure = Brochure.new(params[:brochure])
    @brochure.attachable = @property
    @brochure.title = params[:title]
    @brochure.position = params[:position]
    respond_to do |format|
      if @brochure.validate_brochures && @brochure.save
        @property.update_attribute(:updated_at,Time.now)
        session[:update_listing] = "#{@property.type.underscore}_#{@property.id}-icon_brochure-1"
        flash[:notice] = 'Brochure was successfully created.'

        format.html { redirect_to agent_office_property_media_path(@agent,@office,@property) }
      else
        brochure_errors= []
        flash[:brochure_errors] = {}

        @brochure.errors.each_with_index do |k,index|
          brochure_errors.push("#{k[0].to_s.strip.downcase == "base" ? nil : k[0].humanize } #{k[1].humanize}")
        end

        unless brochure_errors.blank?
          flash[:brochure_position] = params[:position]
          flash[:brochure_errors][:message] =  "Error : #{brochure_errors.uniq.join(", ").to_s}".humanize
        end

        format.html { redirect_to agent_office_property_media_path(@agent,@office,@property)}
      end
    end
  end

  def destroy
    begin
      @brochure = Brochure.find(params[:id])
      @brochure.destroy
      @property.update_attribute(:updated_at,Time.now)
      session[:update_listing] = "#{@property.type.underscore}_#{@property.id}-icon_brochure-0"
      flash[:notice] = "Brochure deleted successfully."
    rescue
      flash[:notice] = "Delete brochure failed, please try again."
    end
    respond_to do |format|
      format.html { redirect_to agent_office_property_media_path(@agent,@office,@property) }
      format.xml { head :ok }
    end
  end

  def list
    if params[:submit]
      Brochure.update_all( "send_to_api = 0, pdf_layout = ''", "`attachable_id` = #{@property.id}" )
      ids = ""
      unless params[:add_to_media].blank?
        params[:add_to_media].each_with_index do |brochure, index|
          break if index > 9
          ids += "," unless index == 0
          ids += brochure
        end
        brochures = Brochure.find(:all, :conditions => "`id` IN (#{ids})", :order => "position")
        Brochure.update_all( "position = 15", "`attachable_id` = #{@property.id}" )
        brochures.each_with_index do |br,index|
          Brochure.update_all("send_to_api = 1, position = #{(index+1)}", "`id` = #{br.id}")
        end
        @property.update_attribute(:updated_at,Time.now)
      end
    end
    @brochures = @property.brochure.find(:all, :conditions => ["attachments.pdf_layout != ?", ''])
    session[:brochure_layout] = nil
    session[:brochure_image] = nil
    session[:brochure_sorted] = nil
    session[:brochure_data] = nil
    session[:brochure_style] = nil
    session[:brochure_option] = nil
  end

  def choose_layout
    if params[:layout]
      session[:brochure_layout] = params[:layout]
      redirect_to choose_images_agent_office_property_brochures_path(@agent, @office, @property)
    end
    @limit = Brochure.theme_count
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
    #    @header_image = find_header
    #    @footer_image = find_footer
  end

  def choose_images
    if params[:choose_images]
      session[:brochure_sorted] = nil
      session[:brochure_image] = params[:list_image]
      redirect_to crop_images_agent_office_property_brochures_path(@agent, @office, @property)
    end
    @limit = Brochure.theme_limit(session[:brochure_layout])
    @list_image = session[:brochure_image]

    unless session[:brochure_image].blank?
      images_id = session[:brochure_image]
      new_images_id= images_id.gsub("#",",")
      new_images_id[0] = ''
      @images = Attachment.find(:all, :conditions => "`id` IN(#{new_images_id})")
    end
  end

  def crop_images
    if params[:sort_images]
      session[:brochure_image] = params[:list_image]
      session[:brochure_sorted] = params[:sorted]
    end

    @sorted = session[:brochure_sorted]
    unless session[:brochure_image].blank?
      images_id = session[:brochure_image]
      @images = []
      arr_images= images_id.split("#")
      arr_images.map{|u| @images << Attachment.find_by_id(u.to_i) unless u.blank?}
      @list_image = session[:brochure_image]
    end
    @limit = Brochure.theme_limit(session[:brochure_layout])
    image_info
  end

  def crop
    image_info
    unless @image_file.blank?
      exist = Attachment.find(:first, :conditions => "`parent_id`= #{@image_file.id} AND `thumbnail` = 'crop'")
      exist.destroy unless exist.blank?
      path = @image_file.full_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
      pic = Magick::ImageList.new(@image_file.public_filename.to_s)
      cropped = pic.crop(params[:x_image].to_i,params[:y_image].to_i,params[:w_image].to_i,params[:h_image].to_i)
      resize = cropped.resize(@result_width, @result_height)
      @image_file.filename = @image_file.filename.gsub(".jpg","_crop.jpg")
      temporary = "#{RAILS_ROOT}/tmp/#{@image_file.filename}"
      temporary.gsub!(".jpg","-#{@image_file.id}.jpg")
      resize.write(temporary)
      new_file = Attachment.create(:size => @image_file.size, :content_type => @image_file.content_type, :filename => @image_file.filename,
        :height => @result_height, :width => @result_width, :parent_id => @image_file.id,
        :thumbnail => "crop", :attachable_id => @image_file.attachable_id, :attachable_type => @image_file.attachable_type)
      new_file.update_attribute(:type, @image_file.type)
      @image_file.moved_to_s3 = false
      uploaded = @image_file.upload_to_s3(temporary)
      try = 0
      while try < 5 && !uploaded
        try = try + 1
        uploaded = @image_file.upload_to_s3(temporary)
        sleep(3)
      end
      if FileTest.exist?(temporary)
        File.delete(temporary) if uploaded
      end
      new_file.update_attribute(:moved_to_s3, true) if uploaded
      session[:brochure_image] = session[:brochure_image].gsub(@image_file.id.to_s, new_file.id.to_s)
    end
    redirect_to crop_images_agent_office_property_brochures_path(@agent, @office, @property)
  end

  def manage_brochure
    if !params[:update].blank?
      session[:brochure_data] = params[:data]
      session[:brochure_style] = params[:style]
      session[:brochure_option] = params[:option]
      redirect_to brochure_back_agent_office_property_brochures_path(@agent, @office, @property)
    else
      case @property.display_price
      when 0
        price = ""
      when 1
        price = (@property.price.to_s.include? '$') ? @property.price.to_s : "$"+@property.price.to_s
      when 2
        price = @property.display_price_text
      else
        price = ""
      end
      desc = session[:brochure_data].present? && session[:brochure_data][:description].present? ? session[:brochure_data][:description] : @property.description

      session[:brochure_data] = {
        :title => (session[:brochure_layout] == "theme8") ? "<b>Location</b><br/>#{@property.address}" : @property.address,
        :headline => '<p><strong><span style="font-size: small;">'+@property.headline+'</span></strong></p>',
        :description => '<p>'+desc+'</p>',
        :price => (session[:brochure_layout] == "theme8")? '<span>'+price+'</span>' : '<p><span style="font-size: small;">'+price+'</span></p>',
        :contact1 => ('<p><span style="font-size: small;">'+format_agent(@property.primary_contact)+'</span></p>' unless @property.primary_contact.blank?),
        :contact2 => ('<p><span style="font-size: small;">'+format_agent(@property.secondary_contact)+'</span></p>' unless @property.secondary_contact.blank?) } #if session[:brochure_data].blank?
      session[:brochure_style] = { :font => 'font-family:arial;',
        :heading_color => 'background-color:#000000;',
        :background_color => 'background-color:#FFFFFF;',
        :sidebar_color => 'background-color:#FFFFFF;',
        :title_color => 'color:#FFFFFF'} if session[:brochure_style].blank?
      session[:brochure_option] = { :font => 'arial',
        :heading_color => '#000000',
        :background_color => '#FFFFFF',
        :sidebar_color => '#FFFFFF',
        :title_color => '#FFFFFF', :display_team_image => 0, :leave_blank_header => 0} if session[:brochure_option].blank?
    end
    @text_editor = Brochure.theme_editor(session[:brochure_layout])
    @editor_limit = Brochure.editor_limit(session[:brochure_layout])
    @opt_color = (session[:brochure_layout] == "theme8") ? "theme4_color_options" : "color_options"
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
#    @footer_image = find_footer
    @data = session[:brochure_data]
    @style = session[:brochure_style]
    @option = session[:brochure_option]

    @color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #999999 #CCCCCC #f0f0f0 #FFFFFF)
    @align_list = %w(left center right)
    @detail = @property.detail
  end

  def brochure_back
    if !params[:update].blank?
      session[:brochure_data][:display_brochure_back] = params[:display_brochure_back]
      unless params[:brochure_image].blank?
        unless params[:brochure_image][:uploaded_data].blank?
          old_bimage = @property.brochure_images.find_by_position(1)
          brochure_image = BrochureImage.new(params[:brochure_image])
          brochure_image.position = 1
          brochure_image.attachable = @property

          if brochure_image.save
            old_bimage.destroy unless old_bimage.blank?
          end
        end
      end

      unless params[:brochure_map].blank?
        unless params[:brochure_map][:uploaded_data].blank?
          old_bmap = @property.brochure_images.find_by_position(2)
          brochure_map = BrochureImage.new(params[:brochure_map])
          brochure_map.position = 2
          brochure_map.attachable = @property

          if brochure_map.save
            old_bmap.destroy unless old_bmap.blank?
          end
        end
      end
      redirect_to preview_agent_office_property_brochures_path(@agent,@office,@property)
    end
    @display_brochure_back = session[:brochure_data][:display_brochure_back] unless session[:brochure_data].blank?
    unless @property.brochure_images.blank?
      @brochure_image = @property.brochure_images.find_by_position(1)
      @brochure_map = @property.brochure_images.find_by_position(2)
    end
  end

  def preview
    redirect_to choose_layout_agent_office_property_brochures_path(@agent, @office, @property) if session[:brochure_layout].blank?
    unless session[:brochure_image].blank?
      images_id = session[:brochure_image]
      arr_images= images_id.split("#")
      info = Brochure.theme_info(session[:brochure_layout])
      @thumb_images = []
      unless info.blank?
        for i in 1..(arr_images.size-1)
          if i == 1
            @main_image  = Attachment.find_by_id(arr_images[i].to_i) unless arr_images[i].blank?
            @main_width = info[:main][:result_width]
            @main_height = info[:main][:result_height]
          else
            @thumb_images  << Attachment.find_by_id(arr_images[i].to_i) unless arr_images[i].blank?
            @thumb_width = info[:thumb][:result_width]
            @thumb_height = info[:thumb][:result_height]
          end
        end
      end
    end
    @editor_limit = Brochure.editor_limit(session[:brochure_layout])
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
    #    @header_image = find_header
    #    @footer_image = find_footer
    @editor_limit = Brochure.editor_limit(session[:brochure_layout])
    @data = session[:brochure_data]
    @style = session[:brochure_style]
    @option = session[:brochure_option]
    @detail = @property.detail
    unless @property.brochure_images.blank?
      @brochure_image = @property.brochure_images.find_by_position(1)
      @brochure_map = @property.brochure_images.find_by_position(2)
    end
  end

  def generate_pdf
    if params[:pdf_id]
      pdf_file  = Attachment.find_by_id(params[:pdf_id]) unless params[:pdf_id].blank?
      @html = pdf_file.pdf_layout
      render :layout => false
    else
      unless session[:brochure_image].blank?
        images_id = session[:brochure_image]
        arr_images= images_id.split("#")
        info = Brochure.theme_info(session[:brochure_layout])
        @thumb_images = []
        for i in 1..(arr_images.size-1)
          if i == 1
            @main_image  = Attachment.find_by_id(arr_images[i].to_i) unless arr_images[i].blank?
            @main_width = info[:main][:result_width]
            @main_height = info[:main][:result_height]
          else
            @thumb_images  << Attachment.find_by_id(arr_images[i].to_i) unless arr_images[i].blank?
            @thumb_width = info[:thumb][:result_width]
            @thumb_height = info[:thumb][:result_height]
          end
        end

        @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
        @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
        #        @header_image = find_header
        #        @footer_image = find_footer
        @editor_limit = Brochure.editor_limit(session[:brochure_layout])
        @data = session[:brochure_data]
        @style = session[:brochure_style]
        @option = session[:brochure_option]
        @detail = @property.detail
        unless @property.brochure_images.blank?
          @brochure_image = @property.brochure_images.find_by_position(1)
          @brochure_map = @property.brochure_images.find_by_position(2)
        end

        html = render_to_string(:layout => false)
        title = Time.zone.now.strftime("%d%m%y%H%M")
        filename = title + ".pdf"

        pdf_file = Attachment.create(:size => 1000, :content_type => "application/pdf", :filename => filename, :attachable_id => @property.id, :attachable_type => "Property", :title => title, :pdf_layout => html, :send_to_api => false, :position => 15)
        if  pdf_file
          pdf_file.update_attribute(:type, "Brochure")
          pdf = Attachment.find(pdf_file.id)
          path = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
          path_folder = path.gsub("/"+filename, "")
          url_path = generate_pdf_agent_office_property_brochures_url(@agent, @office, @property)+"?pdf_id="+pdf_file.id.to_s
          system("mkdir -p #{path_folder}")
          temporary = "#{RAILS_ROOT}/tmp/#{pdf.filename}"
          temporary.gsub!(".pdf","-#{pdf.id}.pdf")
          if session[:brochure_layout] == "theme9"
            width_height = "--page-height 219 --page-width 156.7"
          else
            width_height = "--page-size A4"
          end
          spawn(:kill => true) do
            system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{width_height} #{url_path} #{temporary}")
            begin
              if FileTest.exist?(temporary)
                uploaded = pdf.upload_to_s3(temporary)
              end
              if uploaded
                system("rm -rf #{temporary}") if FileTest.exist?(temporary)
              end
            rescue Exception => ex
              Log.create(:message => "ERROR brochures =#{ex.inspect}")
            end
          end
          #          Thread.new {system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{url_path} #{path}")}
        end
      end
      #      redirect_to preview_agent_office_property_brochures_path(@agent,@office,@property)
      redirect_to list_agent_office_property_brochures_path(@agent,@office,@property)
    end
  end

  def image_info
    unless params[:img_id].blank?
      @image_file = Attachment.find_by_id(params[:img_id])
      info = Brochure.theme_info(session[:brochure_layout])

      unless info.blank?
        @width = @image_file.width < 800 ? @image_file.width : 800
        @height = @image_file.width < 800 ? @image_file.height : (800*@image_file.height)/@image_file.width

        @result_width = params[:status] == 'main' ? info[:main][:result_width] : info[:thumb][:result_width]
        @result_height = params[:status] == 'main' ? info[:main][:result_height] : info[:thumb][:result_height]

        @dot_width = params[:status] == 'main' ? info[:main][:dot_width] : info[:thumb][:dot_width]
        @dot_height = params[:status] == 'main' ? info[:main][:dot_height] : info[:thumb][:dot_height]

        @prewidth = params[:status] == 'main' ? info[:main][:pre_width] : info[:thumb][:pre_width]
        @preheight = params[:status] == 'main' ? info[:main][:pre_height] : info[:thumb][:pre_height]
      end
    end
  end

  def download
    unless params[:id].blank?
      pdf  = Attachment.find_by_id(params[:id])
      if !pdf.blank?
        file = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
        if pdf.moved_to_s3 == false
          if FileTest.exist?(file)
            pdf.upload_to_s3(file)
            redirect_to pdf.public_filename
          else
            if pdf.created_at.strftime("%Y-%m-%d") < Time.zone.now.strftime("%Y-%m-%d")
              flash[:notice] = "Stock List not found."
            else
              flash[:notice] = "the pdf is still creating please try again in a few minutes."
            end
            redirect_to :action => "list"
          end
        else
          system("rm -rf #{file.gsub(pdf.filename, "")}") if FileTest.exist?(file)
          redirect_to pdf.public_filename
        end
      else
        flash[:notice] = "Brochure not found."
        redirect_to :action => "list"
      end
    end
  end

  def delete_brochure
    unless params[:id].blank?
      pdf  = Attachment.find_by_id(params[:id])
      pdf.destroy unless pdf.blank?
    end
    redirect_to list_agent_office_property_brochures_path(@agent,@office,@property)
  end

  def property_data
    @primary_image = (@property.primary_contact.portrait_image.find_by_position(1) unless @property.primary_contact.portrait_image.find_by_position(1).blank? ) unless @property.primary_contact.blank?
    @secondary_image = (@property.secondary_contact.portrait_image.find_by_position(1) unless @property.secondary_contact.portrait_image.find_by_position(1).blank? ) unless @property.secondary_contact.blank?

    if @property.type.to_s.underscore == 'commercial' || @property.type.to_s.underscore == 'business_sale'
      @baths = @beds = @garages = 0
    else
      @baths = (@property.detail.respond_to?('bathrooms')) ? @property.detail.bathrooms : 0
      @beds = (@property.detail.respond_to?('bedrooms')) ? @property.detail.bedrooms : 0
      @garages = (@property.detail.respond_to?('garage_spaces')) ? @property.detail.garage_spaces : 0
    end
  end

  def format_agent(agent)
    unless agent.blank?
      if session[:brochure_layout] == "theme8"
        contact = "<br/><b>Contact</b><br/>"+agent.full_name
        contact += "<br/>"+agent.mobile if agent.mobile
        contact += "<br/>"+agent.phone if agent.phone
      else
        contact = agent.full_name
        contact += "<br/>m: "+agent.mobile if agent.mobile
        contact += "<br/>t: "+agent.phone if agent.phone
        contact += "<br/>e: "+agent.email if agent.email
      end
    end
    return contact
  end

  def find_footer
    get_header_footer(@office.id, "Brochure Footer")
  end

  def find_header
    get_header_footer(@office.id, "Brochure Header")
  end
end
