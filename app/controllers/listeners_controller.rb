class ListenersController < ApplicationController
  layout false
  before_filter :get_agent_and_office, :except =>[:export_portal]
  before_filter :get_property
  skip_before_filter :verify_authenticity_token

  # ********************************************
  #      Property API for Opentime
  # ********************************************
  def opentime_form
  end

  def opentime
    success = false
    error_field = ''

    start_time = Time.parse(params[:start_hour]+":"+params[:start_minute]+":00") unless params[:start_hour].blank? && params[:start_minute].blank?
    end_time = Time.parse(params[:end_hour]+":"+params[:end_minute]+":00") unless params[:end_hour].blank? && params[:end_minute].blank?
    all_param = {:start_time => start_time, :end_time => end_time, :property_id => params[:property_id], :date => params[:date]}

    @opentime = @property.opentimes.new(all_param)
    if @opentime.save
      if params[:number_of_weeks].to_i > 1
        x = params[:number_of_weeks].to_i - 2
        for i in 0..x
         all_param[:date] = all_param[:date].to_date + 7.days
         @opentime = @property.opentimes.new(all_param)
         @property.update_attribute(:updated_at,Time.now) if @opentime.save
        end
      end
      success = true
    else
      @error_field = @opentime.errors
    end
    open_data = []
    open_data << {:success => success, :error_fields => error_field, :data => params}
    respond_to do |format|
      format.xml { render :xml => open_data}
    end
  end

  # ********************************************
  #      Property API for Status
  # ********************************************
  def status_form
  end

  def status
    @success = contact_exist = false
    @error_field = ""

    if (params[:status] == '2') || (params[:status] == '3')
      category_id = (@property.type == 'ResidentialLease')? 11 : 8
      @param_purchaser = {:agent_contact_id => nil, :property_id => @property.id, :date => params[:date], :amount => params[:amount], :display_price => params[:display_price]}
      @param_contact = {:category_contact_id => category_id, :agent_id => @agent.id, :office_id => @office.id, :first_name => params[:first_name], :last_name => params[:last_name], :email => params[:email], :home_number => params[:home_number], :work_number => params[:work_number], :mobile_number => params[:mobile_number], :contact_number => params[:contact_number], :fax_number => params[:fax_number]} if !params[:first_name].blank? && !params[:last_name].blank?

      new_purchaser = false
      @property.purchaser.nil? ? new_purchaser = true : @purchaser = @property.purchaser

      if new_purchaser
        @purchaser = Purchaser.new(@param_purchaser)
        @error_field = @purchaser.errors unless @purchaser.save
      else
        @error_field = @purchaser.errors unless @purchaser.update_attributes(@param_purchaser)
      end

      if @error_field == ''
        if @param_contact
          assign_purchaser_contact(@param_contact,category_id)
          description = "Date = "+params[:date] +", Price = "+params[:amount] +', Display Price = '+ (params[:display_price] == '1' ? "Yes": "No")
          agent_contact= AgentContact.find_by_id(@agent_contact.id)
          unless agent_contact.blank?
          agent_contact.create_note(@property.id, description, 8, current_user.id)
          note_type_id = 9  #lease property
          note_type_id = 8  if (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
          agent_contact.create_note_for_property(@property.id, description, note_type_id, "", nil)
          end
          @purchaser.update_attributes({:contact_note_id => @agent_contact.id})
        end
      end
    end
    update_property_status if @error_field == ''

    respond_to do |format|
     format.xml { render :xml => [{:success => @success, :error_fields => @error_field, :data => params.delete_if{|x,y| %w(action submit controller).include?(x)}}]}
    end
  end

  # ********************************************
  #      Property API for REA
  # ********************************************
  def form_export_portal
  end

  #localhost:3000/agents/1/offices/1/properties/7290/listeners/export_portal
  def export_portal
    success = true
    error_field = ""
    unless @property.blank?
      ActiveRecord::Base.connection.execute("UPDATE properties SET `cre_id`= '#{params[:cre_id]}' WHERE id=#{@property.id}") unless params[:cre_id].blank?
      ActiveRecord::Base.connection.execute("UPDATE properties SET `rca_id`= '#{params[:rca_id]}' WHERE id=#{@property.id}") unless params[:rca_id].blank?
      ActiveRecord::Base.connection.execute("UPDATE properties SET `domain_id`= '#{params[:domain_id]}' WHERE id=#{@property.id}") unless params[:domain_id].blank?
      unless params[:message_error].blank?
        ActiveRecord::Base.connection.execute("DELETE FROM `export_errors` WHERE `property_id`=#{@property.id} And `portal_name` LIKE '#{params[:portal_name]}' And `error_message` LIKE '#{params[:message_error].gsub("'", "")}'")
        ExportError.create({:office_id => @property.office.id, :property_id => @property.id, :portal_name => params[:portal_name], :error_message => params[:message_error]})
      end
    else
      success = false
      error_field = "Property can't be blank"
    end
    respond_to do |format|
     format.xml { render :xml => [{:success => success, :error_fields => error_field, :data => params.delete_if{|x,y| %w(action submit controller).include?(x)}}]}
    end
  end

  #localhost:3000/agents/:agent_id/offices/:office_id/properties/:property_id/listeners/export_report
  def export_report
    success = true
    error_field = ""
    if !@property.blank?
      ExportReport.create({:office_id => params[:office_id], :property_id => params[:property_id], :portal => params[:portal], :portal_property_id => params[:portal_property_id], :last_sent => params[:last_sent], :filename => params[:filename], :report_back => params[:report_back], :report_status => params[:report_status]})
    else
      success = false
      error_field = "Property can't be blank"
    end
    respond_to do |format|
     format.xml { render :xml => [{:success => success, :error_fields => error_field, :data => params.delete_if{|x,y| %w(action submit controller).include?(x)}}]}
    end
  end

  def update_property_status
    last_status = @property.status
    if (params[:status] == '2') || (params[:status] == '3')
      if (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
        @property.update_attribute(:sold_on, params[:date])
        @property.update_attribute(:sold_price, params[:amount])
      else
        @property.update_attribute(:leased_on, params[:date])
        @property.update_attribute(:leased_price, params[:amount])
      end
      @property.update_attribute(:show_price,params[:display_price])
    end

    @property.update_attribute(:status,params[:status])
    @property.update_attribute(:updated_at,Time.now)

    (last_status != 1 && @property.status == 1) ? @property.update_attribute(:activation,true) : @property.update_attribute(:activation,nil)
    @property.update_attribute(:deactivation,nil) if (last_status == 6 && @property.status != 6)

    if (params[:status] == '2') || (params[:status] == '3') || (params[:status] == '4')
      Opentime.destroy_all(:property_id => params[:id])
    end
    @success = true
  end

  def assign_purchaser_contact(contact, category_id)
    detected = nil
    %w(email mobile_number contact_number work_number home_number).each{|w|
       unless params[w].blank?
          check = AgentContact.check_exist(contact, w)
          detected = check unless check.nil?
       end
    }

    if detected
        @agent_contact = AgentContact.find_by_id(detected.id)
        return unless @agent_contact.update_attributes(contact.delete_if{|x,y| y.blank?})
        @purchaser.update_attribute(:agent_contact_id, @agent_contact.id)
        return
    end

    @agent_contact = AgentContact.new(contact)
    if @agent_contact.save
      team = []
      team << @property.primary_contact_id unless @property.primary_contact_id.nil?
      team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
      @agent_contact.add_categories([category_id])
      @agent_contact.add_assigns(team)
      @agent_contact.add_access(team)
      @purchaser.update_attribute(:agent_contact_id, @agent_contact.id)
      @agent_contact.use_spawn_for_boom
      @agent_contact.use_spawn_for_md
      @agent_contact.use_spawn_for_irealty
      return
    end
    @error_field = @agent_contact.errors
  end
end
