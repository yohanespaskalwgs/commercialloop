class MessageCommentsController < ApplicationController
  before_filter :login_required
  before_filter :get_message
  before_filter :get_comment, :only => %w(edit update destroy)

  # message archive methods placed in this controller for two reason:
  # 1, duplicated this methods to all messages controllers is ugly
  # 2, archive process is binding to comment (message sender could input a comment when approving a archive request)
  def request_archive
    # message sent from developer to agent user could be archived by sender
    pending = !(AgentMessage === @message)

    if @message.peding_archivable_by?(current_user)
      if pending
        @message.update_attribute(:archive_state, Message::ArchivePending)
        comment = MessageComment.new(:commenter_id => current_user.id, :comment => "The recipient of this message has completed this ticket and requested for it to be Archived.")
        @message.add_comment(comment)
        msg = "This message has been removed from your Inbox and placed in your Archives. The user who created this message will approve or disapprove your Archive request and provide feedback. If your request in not approved the message will appear back in your Inbox. "
      else
        @message.update_attribute(:archive_state, Message::ArchiveYes)
        msg = "This message has been removed from your Inbox and placed in your Archives."
      end
    end

    respond_to do |format|
      format.js {
        render :update do |page|
          page["request_archive_#{@message.id}_inner"].replace_html(:partial => 'thickbox_done', :locals => { :msg => msg })
          #page["archive_icon_#{@message.id}"].replace :partial => 'messages/archive_icon', :locals => { :msg => @message }
        end
      }
    end
    #redirect_to get_message_path
  end

  def approve_archive
    #just render the archive view
  end

  def archive
    if @message.archivable_by?(current_user)
      @message.archive(params[:rating].to_i, params[:remarks], current_user)
      if @message.archived?
        msg = "This message has been removed from your Inbox and placed in your Archives. A message has been sent to the Recipient informing them their request to Archive the message has been successful and your feedback will be provided."
      else
        msg = "The request by the Recipient of this message to Archive the message has been disapproved. A message containing your feedback has been sent to the Recipient."
      end
    end

    respond_to do |format|
      format.js {
        render :update do |page|
          page[:thickbox_content].replace_html(:partial => 'thickbox_done', :locals => { :msg => msg })
          #page["archive_icon_#{@message.id}"].replace :partial => 'messages/archive_icon', :locals => { :msg => @message }
        end
      }
      format.html {
        redirect_to get_message_path("new_comment_form")
      }
    end
    #redirect_to get_message_path
  end

  def update
    @comment.update_attributes(params[:message_comment])
    respond_to do |format|
      format.js do
        render :update do |page|
          page["message_comment_#{@comment.id}"].replace :partial => 'messages/comment', :object => @comment, :locals => { :status => @status }
          page.visual_effect :highlight, "message_comment_#{@comment.id}"
        end
      end
    end
  end

  def create
    @comment = MessageComment.new(params[:comment].merge(:commenter_id => current_user.id))
    if @comment.save_with_file
      flash[:post_result] = "Comment posted."
      if params[:comment][:internal_chat]
        @message.add_comment(@comment, "internal")
      else
        @message.add_comment(@comment)
      end

      if params[:unarchive]
        @message.update_attribute(:archive_state, Message::ArchiveNo)
      end
      if params[:archive]
        @message.update_attribute(:archive_state, Message::ArchivePending)
      end

      if params[:comment][:internal_chat]
        if params[:comment][:developer_internal] != ""
          redirect_to developer_internal_chat_path(current_user.developer.id, @message.id)
        else
          redirect_to internal_chat_path(@message.id)
        end
      else
        redirect_to get_message_path("message_comment_#{@comment.id}")
      end
    else
      flash[:post_reullt] = "Post fails: #{@comment.errors.full_messages.join('<br />')}"
      redirect_to get_message_path("new_comment_form")
    end
  end

  def destroy
    @comment.destroy if @comment.editable_by?(current_user)

    respond_to do |format|
      format.html {
        redirect_to get_message_path("comments")
      }
      format.js { render }
    end
  end

  private
  def get_message
    @message = Message.find(params[:message_id])
    redirect_to root_path and return false unless @message.viewable_by?(current_user)
  end

  def get_comment
    @comment = @message.message_comments.find(params[:id])
    redirect_to get_message_path and return false unless @comment.editable_by?(current_user)
  end

  def get_message_path(anchor = nil)
    case current_user
    when DeveloperUser
      path = developer_message_path(current_user.developer, @message, :anchor => anchor)
    when AgentUser
      path = agent_message_path(current_user.office.agent, @message, :anchor => anchor)
    else # AdminUser
      path = message_path(@message, :anchor => anchor)
    end
  end
end
