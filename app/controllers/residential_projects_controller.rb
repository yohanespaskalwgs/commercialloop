require 'will_paginate/array'
class ResidentialProjectsController < ApplicationController
  before_filter :prepare_agent_and_office
  skip_before_filter :login_required
  before_filter :get_session_listing
  before_filter :require_on_hold_status_changer, :only => [:status]
  skip_before_filter :require_accepted
  skip_before_filter :get_agent_and_office, :get_property
  skip_before_filter :verify_authenticity_token, :only => [:auto_complete]
  layout "vault"

  include ActionView::Helpers::TextHelper
  include PropertiesHelper

  def index
    get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def populate_combobox
    suburb,type,assigned_to,condition = [],[],[],[]
    condition = [" properties.office_id = ? "]
    condition << @agent_contact.office_id
    properties = Property.find(:all,:include =>[:primary_contact], :conditions => condition,:select => "properties.primary_contact_id, DISTINCT(properties.suburb),DISTINCT(properties.property_type),users.first_name,users.last_name")
    properties.map{|x|
      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText =>x.suburb.to_s
      } unless x.suburb.blank?;
      type << {
        :type => x.property_type.to_s,
        :typeText =>x.property_type.to_s
      } unless x.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?) ,
        :assigned_toText => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?)
      } unless x.primary_contact.blank?
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    return(render(:json =>{
          :results => properties.length,
          :suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,
          :types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,
          :team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{@agent_contact.office_id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << @agent_contact.office_id

        #        if current_user.limited_client_access?
        #          if !@agent_contact.office.control_level3
        #            condition[0] += " AND properties.primary_contact_id = ? "
        #            condition << current_user.id
        #          end
        #        elsif current_user.allow_property_owner_access?
        #          condition[0] += " AND properties.agent_user_id = ?"
        #          condition << current_user.id
        #        end

        if(!session[:type].blank? && Property::TYPES.map{|s| s[1]}.include?(session[:type]))
          condition[0] += " AND properties.type = ?"
          condition <<  session[:type]
        end


        #        if ((current_user.allow_property_owner_access? && session[:status] == 1) || (current_user.allow_property_owner_access?  && session[:status].blank?))|| current_user.allow_property_owner_access?
        #          session[:status] = "all"
        #        end

        unless session[:status] == "all"
          unless session[:status].blank?
            condition[0] += " AND properties.status = ?"
            condition <<  session[:status]
          end
        end

        unless session[:suburb] == "all" || session[:suburb].blank?
          condition[0] += " AND properties.suburb = ?"
          condition << session[:suburb]
        end

        unless session[:primary_contact_fullname] == "all" || session[:primary_contact_fullname].blank?
          name =  session[:primary_contact_fullname].split(' ')
          condition[0] += " AND users.first_name  = ?"
          condition << name[0]
          unless name[1].blank?
            condition[0] += " AND users.last_name  = ?"
            condition << name[1]
          end
        end

        unless session[:property_type] == "all" || session[:property_type].blank?
          condition[0] += " AND properties.property_type = ?"
          condition << session[:property_type]
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << @agent_contact.office_id

      #      if current_user.limited_client_access?
      #        if !@agent_contact.office.control_level3
      #          condition[0] += " AND properties.primary_contact_id = ? "
      #          condition << current_user.id
      #        end
      #      elsif current_user.allow_property_owner_access?
      #        condition[0] += " AND properties.agent_user_id = ?"
      #        condition << current_user.id
      #      end
    end
    condition[0] += " AND properties.deleted_at is NULL"

    condition[0] += " AND properties.type = ?"
    condition << "NewDevelopment"

    condition[0] += " AND properties.office_id = ?"
    condition << "#{@agent_contact.office_id}"

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = Property.build_sorting(params[:sort],params[:dir])
    @properties = Property.paginate(:all,:include =>[:primary_contact], :conditions => condition, :order => order,:select => "properties.id,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,users.first_name,users.last_name,properties.size_from,properties.size_to,properties.number_of_suites", :page =>params[:page], :per_page => params[:limit].to_i)
    if @properties.present?
      @properties.each do |property|
        check_and_update_icon(property)
      end
    end
    results = 0
    if params[:limit].to_i == 20
      #      results = 20 #Property.find(:all,:select => 'count(properties.id), properties.primary_contact_id',:include =>[:primary_contact],:conditions => condition,:limit => params[:limit].to_i)
      results = Property.count(:include =>[:primary_contact],:conditions => condition)
    else
      results = Property.count(:include =>[:primary_contact],:conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'listing'=>Property.abbrv_properties(x.type),
              'address' =>((x.street_number.blank? ? "" : x.street_number)+" "+(x.street.blank? ? "" : x.street.to_s)),
              'suburb' => x.suburb,
              'type' => x.property_type,
              'suite'=>(x.unit_number.blank? ? "" : x.unit_number),
              'price' => Property.check_price(x),
              'assigned_to'=>x.primary_contact.blank? ? '' : x.primary_contact.full_name.to_s.strip,
              'status' => Property.status_name(x),
              'available_children_listings'=>"<a href='/residential_projects/" + x.id.to_s + "/available_children_listings' style='color:#0E7FC7'>" + Property.find(:all, :conditions => ["parent_listing_id = ? AND status = ?", "#{x.id}", "1"]).count.to_s + "</a>",
              'sold_children_listings' =>"<a href='/residential_projects/" + x.id.to_s + "/sold_children_listings' style='color:#0E7FC7'>" + Property.find(:all, :conditions => ["parent_listing_id = ? AND status = ?", "#{x.id}", "2"]).count.to_s + "</a>",
              'on_hold_children_listings' =>"<a href='/residential_projects/" + x.id.to_s + "/on_hold_children_listings' style='color:#0E7FC7'>" + Property.find(:all, :conditions => ["parent_listing_id = ? AND status = ?", "#{x.id}", "7"]).count.to_s + "</a>",
              'total'=>"<a href='/residential_projects/" + x.id.to_s + "/children_listings' style='color:#0E7FC7'>" + Property.find(:all, :conditions => ["parent_listing_id = ?", "#{x.id}"]).count.to_s + "</a>",
              'actions'=>Property.check_color(x),
              'fb'=> [x.id,@agent_contact.office.url.blank? ? (x.detail.respond_to?(:deal_type) ? x.detail.property_url : "http://#{(@agent_contact.office.url.gsub('http://', '') unless @agent_contact.office.url.blank?)}" + "/#{x.id}") : "http://#{(@agent_contact.office.url.gsub('http://', '') unless @agent_contact.office.url.blank?)}" + "/#{x.id}", tweet(x)]} unless x.blank? }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def preview
    @property = @agent_contact.office.properties.find params[:id], :include => [:detail]
    @detail = @property.detail
    @feature_ids = @property.features
    @ptype = @property.type.to_s.underscore
    if @property.display_address == 0
      @property.street = ""
      @property.street_number = ""
    end
    begin
      @property.description= BlueCloth.new(decoded_and_converted_to(@property.description)).to_html
    rescue
    end
    #    self.send @ptype
    @header_image = find_header

    respond_to do |format|
      if @ptype == "commercial"
        format.html { render :file => "/residential_projects/preview_commercial", :layout => "iframe"}
      elsif @ptype == "commercialbuilding"
        format.html { render :file => "/residential_projects/preview_commercial_building", :layout => "iframe" }
      else
        format.html { render :layout => 'iframe' }
      end
    end
  end

  def check_and_update_icon(property)
    img_count = property.images.count rescue 0
    floorp_count = property.floorplans.count rescue 0
    if !property.image_exist && img_count > 0
      property.image_exist = true
      property.save(false)
    end
    if property.image_exist && img_count == 0
      property.image_exist = false
      property.save(false)
    end
    if !property.floorplan_exist && floorp_count > 0
      property.floorplan_exist = true
      property.save(false)
    end
    if property.floorplan_exist && floorp_count == 0
      property.floorplan_exist = false
      property.save(false)
    end
  end

  def find_header
    get_header_footer(@agent_contact.office_id, "Brochure Header")
  end

  def tweet(property)
    unless property.blank?
      case property.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{property.address.to_s.split(",").join(", ").to_s}, #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{property.address.to_s.split(",").join(", ").to_s}, #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      when 'commercial_building'
        tweet = "Building Listing: #{property.address.to_s.split(",").join(", ").to_s}, #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{property.address.to_s.split(",").join(", ").to_s}" + (property.detail.blank? ? "" : ", #{property.detail.bedrooms.blank? ? "0" : property.detail.bedrooms} Bed, #{property.detail.bathrooms.blank? ? "0" : property.detail.bathrooms} Bath, #{property.detail.carport_spaces.to_i + property.detail.garage_spaces.to_i + property.detail.off_street_spaces.to_i} Car") + " #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      when 'new_development'
        tweet = "Residential Building: #{property.address.to_s.split(",").join(", ").to_s}" + " #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      when 'land_release'
        tweet = "Land Release: #{property.address.to_s.split(",").join(", ").to_s}" + " #{@agent_contact.office.url.blank? ? translate_url("#{@agent_contact.office.url}") : translate_url("#{@agent_contact.office.url}/#{property.id}")}"
      end
    end
    return tweet
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
    session[:agent_contact_id] = @agent_contact.id
  end

  def nil_session_listing
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
    session[:property_id] = nil
  end

  def auto_complete
    result = []
    unless request.url().scan("auto_office").blank?
      offices = Office.find(:all, :conditions => ["`name` LIKE ? ", '%'+params[:q]+'%'])
      offices.each{|x| result << {'id' => "#{x.id.to_s}", 'name' => "#{x.name}" } }
      render :json => {"results" => result, "total" => (offices ? offices.length : 0) }
    else
      contacts = AgentContact.find(:all, :conditions => ["(`first_name` LIKE ? OR `last_name` LIKE ?) and `agent_id` = ? and `office_id` = ? ", '%' + params[:q].to_s + '%', '%' + params[:q].to_s + '%', @agent.id, @office.id])
      contacts.each{|x|
        full_name = x.first_name.to_s + " " + x.last_name.to_s
        result << {'id' => "#{x.id.to_s}", 'name' => "#{full_name}" } unless x.first_name.blank? }
      render :json => {"results" => result, "total" => (contacts ? contacts.length : 0) }
    end
  end

  def status
    session[:close_popup] = false
    @property = Property.find(params[:id])
    @office = @agent_contact.office
    @offers = @property.offers
    if @property.purchaser.nil?
      @purchaser = Purchaser.new
      @purchaser_contact = @purchaser.build_agent_contact
    else
      @purchaser = @property.purchaser
      @purchaser_contact = @purchaser.agent_contact_id.nil? ? @purchaser.build_agent_contact : @purchaser.agent_contact
    end

    unless @property.purchaser.nil?
      @contact_name = @property.purchaser.agent_contact.full_name unless @property.purchaser.agent_contact.nil?
    end
  end

  def update_status
    @property = @agent_contact.office.properties.find(params[:id], :include => [:detail])
    unless params[:property].blank?
      if params[:property][:status] == '7'
        @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil, :on_hold_by => session[:contact_login], :on_hold_by_user_type => "Agent Contact"})
        params[:purchaser] = params[:purchaser].merge(:property_id => @property.id, :date => nil, :amount => nil, :display_price => nil)

        # On Hold
        if @property.on_holder.blank?
          @on_holder = OnHolder.new({:property_id => @property.id, :name => (@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name), :user_type => "Agent User"})
          @on_holder.save
        else
          if @property.on_holder.name != "#{@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name}"
            @property.on_holder.update_attributes({:name => (@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name)})
          end
        end
        #End On Hold

        new_purchaser = create_note_for_contact = false
        @property.purchaser.nil? ? new_purchaser = true : @purchaser = @property.purchaser
        (previous_agent_contact_id = @property.purchaser.agent_contact_id unless @property.purchaser.agent_contact_id.nil?) unless @property.purchaser.nil?

        respond_to do |format|
          begin
            Purchaser.transaction do
              category_id = (@property.type == 'ResidentialLease')? 5 : 1
              params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => @agent.id, :office_id => @office.id)
              if new_purchaser
                @purchaser = Purchaser.new(params[:purchaser])
                save_purchaser = @purchaser.save
                @purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?
              else
                save_purchaser = @purchaser.update_attributes(params[:purchaser])
                @purchaser.agent_contact_id.nil? ? (@purchaser_contact = @purchaser.build_agent_contact(params[:agent_contact]) if params[:purchaser][:agent_contact_id].blank?) : (@purchaser_contact = @purchaser.agent_contact)
              end
              raise 'Failed to save Purchaser' unless save_purchaser

              if params[:add_new_contact] == "true" || !params[:purchaser][:agent_contact_id].blank?

                if new_purchaser || @purchaser.agent_contact_id.nil?
                  if params[:purchaser][:agent_contact_id].blank?
                    save_purchaser_contact = @purchaser_contact.save
                    @purchaser.update_attribute(:agent_contact_id, @purchaser_contact.id) unless @purchaser_contact.blank?
                  else
                    save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                    @purchaser_contact = @purchaser.agent_contact
                  end
                  create_note_for_contact = true
                else
                  if previous_agent_contact_id.to_s == params[:purchaser][:agent_contact_id].to_s
                    save_purchaser_contact = @purchaser.agent_contact.update_attributes(params[:agent_contact])
                  else
                    save_purchaser_contact = @purchaser.update_attribute(:agent_contact_id, params[:purchaser][:agent_contact_id]) unless params[:purchaser][:agent_contact_id].blank?
                    create_note_for_contact = true
                  end
                  @purchaser_contact = @purchaser.agent_contact
                end

                raise 'Failed to save Purchaser contact' unless save_purchaser_contact

                if params[:add_new_contact] == "true"
                  team = []
                  team << @property.primary_contact_id unless @property.primary_contact_id.nil?
                  team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
                  @purchaser_contact.add_categories([category_id])
                  @purchaser_contact.add_assigns(team)
                  @purchaser_contact.add_access(team)
                end
              end
            end
            update_property_status(create_note_for_contact)
            if @property.status == "7"
              if @property.on_holder.blank?
                @on_holder = OnHolder.new({:property_id => @property.id, :name => (@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name), :user_type => "Agent Contact"})
                @on_holder.save
              else
                if @property.on_holder.name != "#{@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name}"
                  @property.on_holder.update_attributes({:name => (@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name)})
                end
              end
            end
            users = @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
            users.each do |user|
              begin
                UserMailer.deliver_listing_notification_for_level1(user, current_user, @office, @property)
              rescue
              end
            end
            flash[:notice] = 'Property was successfully updated.'
            format.html { redirect_to residential_project_children_listings_path(@property.parent_listing_id) }
          rescue
            if @property
              @property.status = params[:property][:status]
              unless (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
                @property.leased_price = params[:property][:amount] unless params[:property][:amount].blank?
                @property.leased_on = params[:property][:date] unless params[:property][:date].blank?
              else
                @property.sold_price = params[:property][:amount] unless params[:property][:amount].blank?
                @property.sold_on = params[:property][:date] unless params[:property][:date].blank?
              end
            end
            @contact_name = params[:agent_name]
            @add_new_contact = params[:add_new_contact]
            format.html { render :action => "status" }
          end
        end
      else
        update_property_status
        if @property.status == "7"
          @property.update_attributes({:sold_on => nil, :sold_price => nil, :show_price => nil})
          unless @property.purchaser.blank?
            @property.purchaser.update_attributes({:date => nil, :amount => nil, :display_price => nil})
          end
          if @property.on_holder.blank?
            @on_holder = OnHolder.new({:property_id => @property.id, :name => (@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name), :user_type => "Agent User"})
            @on_holder.save
          else
            if @property.on_holder.name != "#{@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name}"
              @property.on_holder.update_attributes({:name => (@agent_contact.full_name.blank? ? @agent_contact.first_name : @agent_contact.full_name)})
            end
          end
        else
          @property.update_attributes({:on_hold_by => nil, :on_hold_by_user_type => nil})
          unless @property.purchaser.blank?
            @property.purchaser.destroy
          end
          unless @property.on_holder.blank?
            @on_holder = @property.on_holder
            @on_holder.destroy
          end
        end
        flash[:notice] = 'Property was successfully updated.'
        session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-statusChanged_#{Property::STATUS_NAME[@property.status - 1]}"
        redirect_to residential_project_children_listings_path(@property.parent_listing_id)
      end
    end
  end

  def update_property_status(new = false)
    if new && !@purchaser_contact.nil?
      description = "Date = "+params[:property][:date] +", Price = "+params[:property][:amount] +', Display Price = '+ (params[:property][:display_price] == '1' ? "Yes": "No")
      agent_contact= AgentContact.find_by_id(@purchaser_contact.id)
      unless agent_contact.blank?
        #note type = Purchase Property
        agent_contact.create_note(@property.id, description, 8, @agent_contact.id)
        note_type_id = 9  #lease property
        note_type_id = 8  if (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
        #note type for property
        agent_contact.create_note_for_property(@property.id, description, note_type_id, "", @agent_contact.id)
        @purchaser.update_attributes({:contact_note_id => @purchaser_contact.id})
      end
    end

    last_status = @property.status
    #@property.attributes = params[:property]
    if (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
      @property.sold_on = params[:property][:date]
      @property.sold_price = params[:property][:amount]
    else
      @property.leased_on = params[:property][:date]
      @property.leased_price = params[:property][:amount]
    end
    @property.show_price = params[:property][:display_price]
    @property.status = params[:property][:status]
    if @agent_contact.assigned.allow_property_owner_access? and (@office.property_owner_access and @office.property_draft_for_updated)
      @property.status = 6
      @property.role_6_mode = true
    end

    (last_status != 1 && @property.status == 1) ? activation = true : activation= nil
    @property.activation = activation
    @property.deactivation = nil if @property.status != 6

    if (last_status == 2 || last_status == 3 || last_status == '7') && (@property.status == 1)
      @property.purchaser.destroy unless @property.purchaser.blank?
      @property.sold_on = @property.sold_price = @property.leased_on = @property.leased_price = @property.show_price = nil
    end

    if (params[:property][:status] == '2') || (params[:property][:status] == '3') || (params[:property][:status] == '4') || (params[:property][:status] == '7')
      Opentime.destroy_all(:property_id => params[:id])
    end

    if (params[:property][:status] == '4') || (params[:property][:status] == '6')
      if @property.extra.blank?
        @extra = Extra.new({:property_id => @property.id, :office_id => @office.id, :featured_property => false})
      else
        @extra = @property.extra
        @extra.update_attribute(:featured_property, false)
      end
    end

    @property.updated_at = Time.now
    @property.save(false)
  end

  private

  def prepare_agent_and_office
    @agent_contact = AgentContact.find(session[:contact_login])
    @agent = @agent_contact.agent
    @office = @agent_contact.office
  end

  def require_on_hold_status_changer
    unless show_status?
      if @agent_contact.assigned_to.blank?
        flash[:notice] = "Please assign your agent contact first to allow change the property status"
      else
        flash[:notice] = "You don't have a permission to change the property status"
      end
      redirect_to "/residential_projects/#{session[:residential_project_id]}/children_listings"
    end
  end

  def display_status
    unless @agent_contact.assigned_to.blank?
      @status_property = Property.find_by_id(params[:id], :conditions => ["status = ? OR on_hold_by = ?", "1", "#{session[:contact_login]}"])
    end
  end

  def show_status?
    !!display_status
  end

end
