class PropertyTasksController < TasksController

  def populate_task_types
    @task_types = PropertyTaskType.find(:all, :conditions => ["office_id is NULL OR office_id = ?", @office.id]).map{|c| [c.name, c.id]}
    render :json => @task_types
  end

  protected

  def prepare_creation
    @property = Property.find(params[:property_id])
    @task_types = PropertyTaskType.all.map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @team_members = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    @tasks = @property.tasks.paginate(:all, :order => "`created_at` DESC", :page =>params[:page], :per_page => 30)
  end
end
