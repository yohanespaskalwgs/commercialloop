class OfficesAController < OfficesController
  layout 'agent'

  before_filter :get_agent
  before_filter :get_office, :only => [ :edit, :update]
  before_filter :check_office_ownership, :only => [:edit, :update]
  before_filter :check_agent_ownership
  before_filter :login_required

  append_before_filter :set_body_id
  append_before_filter :prepare_data, :only => [:new, :create, :edit, :update]
  before_filter :set_hostname

  make_resourceful do
    belongs_to :agent
    actions :new, :create
    #show, index, edit - inherited from offices_controller
  end

  def account
    @developer = @agent.developer
    @developer_contact = @agent.developer_contact
    @support_contact = @agent.support_contact
    @upgrade_contact = @agent.upgrade_contact
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Account' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
  end

  def create
    @office = @agent.offices.new(params[:office])
    @office.is_developer = false
    is_saved = @office.save
    @office.send_agent_user_api
    @agent_user = AgentUser.new
    @agent_user.developer = @office.agent.developer unless @office.agent.blank?
    @agent_user.email = params[:agent_user][:email]
    @agent_user.first_name = params[:agent_user][:first_name]
    @agent_user.last_name = params[:agent_user][:last_name]
    @agent_user.phone = @office.phone_number
    @agent_user.generate_sequential_code # force this as we don't validate
    if !params[:agent_user][:login].blank? && !params[:agent_user][:password].blank? && !params[:agent_user][:password_confirmation].blank?
      @agent_user.login = params[:agent_user][:login]
      @agent_user.password = params[:agent_user][:password]
      @agent_user.password_confirmation = params[:agent_user][:password_confirmation]
    end
    @agent_user.code = @office.generate_sequential_code
    @agent_user.office_id = @office.id
    @agent_user.group = "Sales Agent"
    respond_to do |format|
      if @agent_user.save && is_saved
        @agent_user.office.office_contact = @agent_user.id
        @agent_user.office.save
        @agent_user.roles << Role.find(3)
        @agent_user.save
        UserMailer.deliver_office_user_creation(@agent_user, @office) if params[:notification]
        format.html { redirect_to :action => 'index' }
      else
        @office_developer_accesses_ids = @office.office_developer_accesses.map(&:developer_user_id)
        format.html { render :action => 'new' }
      end
    end
  end

  def api_details
    @agent = Agent.find_by_id(params[:agent_id])
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'API'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end

  def agent_api_details
    @office = Office.find_by_id(params[:id])
    @superapi_offices =  SuperapiOffice.find(:all, :conditions => "office_id = #{@office.id}")
#    @superapi_offices = SuperapiOffice.find_by_sql("SELECT * from superapi_offices JOIN superapi_websites ON superapi_websites.id = superapi_offices.website_id WHERE office_id = #{@office.id}")
    @agent = @office.agent
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'API'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end


  def update_office_api
    @office = Office.find(params[:id])
    @office.xml_api_enable = params[:office][:xml_api_enable]
    unless params[:domain].blank?
      is_filled = false
      params[:domain].each do |domain|
        domain_att = Domain.find(:all, :conditions => ["name = ?",domain[:name]])
        is_filled = true if !domain[:name].blank? && !domain[:ipn].blank? && params[:office][:http_api_enable] == "1"
        is_updated = false

        portal_durl = domain[:name].downcase.gsub('http://', '')
        portal_durl = portal_durl.gsub('www.', '')
        portal = Portal.find(:first, :conditions => "`ftp_url` LIKE '%#{portal_durl}%'")
        unless portal.blank?
          pext = {"portal_export" => portal.http_api_enable}
        else
          pext = {"portal_export" => nil}
        end

        domain[:ipn] = domain[:ipn].downcase.gsub(/\s+/, "")
        domain[:ipn] = "http://#{domain[:ipn]}" unless domain[:ipn].include?("http://")
        domain[:enable_export] = 0 if domain[:enable_export].blank?

        enable_export = domain[:enable_export]
        domain.delete(:enable_export)

        domain = domain.merge pext
        if domain_att
          if !domain_att.map{|x| x.office_id}.include?(params[:id].to_i)
            domain_att = Domain.new(domain)
            domain_att.office = (@office)
            domain_att.enable_export = enable_export
          else
            domain_ex = Domain.find(:first, :conditions => ["name = ? and office_id = ?",domain[:name], @office.id])
            domain_ex.update_attribute(:enable_export, enable_export)
            is_updated = true
          end
        else
          domain_att = Domain.new(domain)
          domain_att.office = (@office)
          domain_att.enable_export = enable_export
        end

        if is_updated
          domain_att.is_a?(Array) ? domain_att.each { |dom| dom.update_attributes(domain) } : domain_att.update_attributes(domain)
        else
          domain_att.is_a?(Array) ? domain_att.each { |dom| dom.save } : domain_att.save
        end
      end
      #delete updated domains
      office_domains = Domain.find(:all, :conditions => ["office_id = ?", params[:id].to_i])
      office_domains.each do |dom|
        Domain.destroy(dom.id) if !params[:domain].map{|x| x[:name]}.include?(dom.name)
      end
      @office.http_api_enable = is_filled
    else
      @office.http_api_enable = false
    end

    #Adding websites -------------------------------------------------------------
    require 'net/http'

    if params[:website].present?
      params[:website].each do |web|
        @url_valid = false
        begin
          if web["url"].present?
            web_url = web["url"].gsub("http://","")
            web_url += "/" if web_url.last != "/"
            web_response = Net::HTTP.get_response(URI.parse("http://#{web_url}?zooapi=ping"))
            json_response = ActiveSupport::JSON.decode(web_response.body)
            if json_response["status"] == true
              @url_valid = true
              sw = SuperapiWebsite.find(:first, :conditions => "url LIKE '%#{web_url}%'")
              if sw.blank?
  #              ActiveRecord::Base.connection.execute("INSERT INTO `superapi_websites` (`server_id`, `url`, `version`, `wp_version`, `verified`, `next_ping`, `errors`, `first_seen_down`, `created`, `updated`) VALUES ('0', '#{"http://#{web_url}"}', '', '', '0', '0', '0', '0', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());")
                sql = ActiveRecord::Base.connection()
                sql.insert "INSERT INTO `superapi_websites` (`server_id`, `url`, `version`, `wp_version`, `verified`, `next_ping`, `errors`, `first_seen_down`, `created`, `updated`) VALUES ('0', '#{"http://#{web_url}"}', '', '', '0', '0', '0', '0', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());"
                sql.commit_db_transaction
                sw = SuperapiWebsite.find(:first, :conditions => "url LIKE '%#{web_url}%'")
              end
              so = SuperapiOffice.find(:first, :conditions => "office_id = #{@office.id} AND website_id = #{sw.id}")
              if so.blank?
                ActiveRecord::Base.connection.execute("INSERT INTO `superapi_offices` (`website_id`, `office_id`, `enabled`, `send_full_offices`, `next_listoffices`, `send_full_contacts`, `next_listcontacts`, `send_full_properties`, `next_listproperties`, `created`, `updated`) VALUES ('#{sw.id}', '#{@office.id}', '1', '0', '0', '0', '0', '0', '0', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());")
              else
                if web["enable"].blank? && so.enabled == 1
                  so.update_attributes(:enabled => 0)
                  so.update_attributes(:updated => Time.now.to_i)
                elsif web["enable"].present? && so.enabled == 0
                  so.update_attributes(:enabled => 1)
                  so.update_attributes(:updated => Time.now.to_i)
                end
              end
            end
          end
        rescue Exception => ex
          if !@url_valid
            flash[:url_invalid] = "Invalid URL: #{web_url} is not a valid URL for API."
          end
          Log.create(:message => "Superapiwebsite add error : #{ex.inspect}, URL: #{web["url"]}, Office: #{@office.id}")
        end
      end
    end

    respond_to do |format|
      if @office.save
        flash[:notice] = 'Office was successfully updated.'
        format.html { redirect_to agent_api_details_agent_office_path(@agent, @office) }
      else
        format.html { redirect_to agent_api_details_agent_office_path(@agent, @office) }
      end
    end
  end

  def send_properties
    so = SuperapiOffice.find(params[:so_id])
    if so.present?
      so.update_attributes(:next_listproperties => 0)
      so.update_attributes(:updated => Time.now.to_i)
    end
    redirect_to agent_api_details_agent_office_path(@agent, params[:id])
  end

  def send_contacts
    so = SuperapiOffice.find(params[:so_id])
    if so.present?
      so.update_attributes(:next_listcontacts => 0)
      so.update_attributes(:updated => Time.now.to_i)
    end
    redirect_to agent_api_details_agent_office_path(@agent, params[:id])
  end

  def send_office
    so = SuperapiOffice.find(params[:so_id])
    if so.present?
      so.update_attributes(:next_listoffices => 0)
      so.update_attributes(:updated => Time.now.to_i)
    end
    redirect_to agent_api_details_agent_office_path(@agent, params[:id])
  end

    def delete_domain
    domain = Domain.find_by_id(params[:domain_id])
    if domain
      ActiveRecord::Base.connection.execute("DELETE FROM property_domain_exports WHERE domain_id = #{params[:domain_id]}")
      if domain.destroy
        return(render(:json => true))
      else
        return(render(:json => false))
      end
    else
      return(render(:json => false))
    end
  end

  def delete_website
    so = SuperapiOffice.find(:first, :conditions => "office_id = #{params[:id]} AND website_id = #{params[:website_id]}")
    if so
      if so.destroy
        return(render(:json => true))
      else
        return(render(:json => false))
      end
    else
      return(render(:json => false))
    end
  end

  def trigger
    @office = Office.find(params[:id])
    unless params[:domain_id].blank?
      conditions = "and office_id = #{params[:id]} and (deleted_at IS NULL)"
      Delayed::Job.enqueue(TriggerApi.new("PropertyDomain", params[:domain_id], conditions), 3, 1.seconds.from_now.getutc)
      flash[:notice] = "Your domain has successfully triggered"
    end
    redirect_to agent_api_details_agent_office_path(@agent, @office)
  end

  private

  def current_model_name
    "Office"
  end

  def instance_variable_name
    "office"
  end


  def set_body_id
    @body_id = 'agent_dashboard'
  end

  def prepare_data
    if params[:action] == 'new'
      @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Add a New Office' AND user_type = 'AgentUser'")
      @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    end
    @countries = Country.find(:all, :conditions => "display_country = 1").map{|c| c.name}.unshift(['Please select', nil])
    user = User.find(current_user)

    if user.type == "DeveloperUser"
      @selected = current_office.country unless current_office.blank? && current_office.country.blank?
    else
      @selected = current_user.office.country unless current_user.office.blank? && current_user.office.country.blank?
    end
  end

  def get_agent
    @agent = Agent.find params[:agent_id]
  end

  def get_office
    @office = @agent.offices.find params[:id]
  end

end
