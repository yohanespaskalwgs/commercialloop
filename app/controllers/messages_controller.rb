#--------------------------------------------------
# for admin version messages centre
#--------------------------------------------------
class MessagesController < AdminController
  before_filter :get_sort_by, :only => %w(index unread archives assign_recipients)
  before_filter :show_message, :only => %w(show internal)
  before_filter :prepare_data

  def index
    @messages = AdminMessage.filter_by_category(params[:category]).inbox.order_by(params[:sortby], current_user).paginate(:page => params[:page])
  end

  def unread
    @messages = AdminMessage.filter_by_category(params[:category]).admin_unread.order_by(params[:sortby], current_user).paginate(:page => params[:page])
    render :action => "index"
  end

  def archives
    @messages = AdminMessage.filter_by_category(params[:category]).archives.order_by(params[:sortby], current_user).paginate(:page => params[:page])
    render :action => "index"
  end

  def show
    @status = "client"
  end

  def internal
    @status = "internal"
    render :action => "show"
  end

  def assign_recipients
    @message = AdminMessage.find(params[:id])
    @message.assign_recipients(params[:recipients])
    respond_to do |format|
      format.js {
        render :update do |page|
          page << "toggle_recipients_selector();"
        end
      }
    end
  end

  private
  def get_sort_by
    params[:sortby] ||= 'date'
  end

  def show_message
    @message = AdminMessage.find(params[:id])
    @message.mark_read_for!(current_user)
  end

  def prepare_data
    @priorities = [['Low', 'Low'],['Medium', 'Medium'],['High', 'High'],['Critical', 'Critical']]
  end
end
