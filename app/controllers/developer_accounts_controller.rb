class DeveloperAccountsController < ApplicationController
  layout 'developer'

  prepend_before_filter :login_required
  require_role "DeveloperUser"

  before_filter :get_developer
  before_filter :body_id
  before_filter :get_subscription_manager , :only => [:plans, :subscribe, :unsubscribe, :billing, :complete]
  # ssl_required :billing, :credit_card

  def show
    @users = User.find_all_by_developer_id_and_type(@developer.id, "DeveloperUser")
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Business Details'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
  end

  def update
    respond_to do |format|
      if params[:logo].blank? || params[:logo][:uploaded_data].blank?
        if @developer.update_attributes(params[:developer])
          flash[:notice] = 'Developer Business Details were successfully updated.'
          format.html { redirect_to :action => 'show' }
          format.xml  { head :ok }
        end
      else # upload logo
        @logo = Image.new params[:logo]
        old_logo = @developer.logo
        @logo.dimension = Image::LOGO2
        @logo.attachable = @developer
        if @logo.save
          old_logo.try(:destroy)
          flash[:notice] = 'Logo was successfully uploaded.'
          @developer.reload
        else
          flash[:notice] = 'Upload failed.' + @logo.errors.to_s
        end
        format.html {redirect_to :action => 'show'}
      end
    end
  end

  def plans
    @active_subscriptions = current_developer.subscriptions.active
    @inactive_subscriptions = current_developer.subscriptions.canceled
    @all_subscriptions = current_developer.subscriptions
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Current Plans'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
    respond_to do |format|
      format.html
    end
  end

  def billing
    @subscription_histories = current_developer.subscription_histories
    @active_subscriptions = current_developer.subscriptions.active
    @active_help = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Billing'")
    @page_status = current_user.check_user_page_status(@active_help.id) unless @active_help.blank?
    respond_to do |format|
      format.html
    end
  end

  def complete
    sub = Subscription.find(params[:subscription_id])
		if sub.status == 'ok'
			flash[:error] = 'The subscription has already been paid'
			redirect_to :action => "plans" and return
		end

    begin
      @sm.pay_for_subscription(sub.id, nil, {:description => description(sub.net_amount+sub.taxes_amount), :token => params[:token], :billing_amount => sub.billing_amount / 100.0 })
			#activate office
      office_sub = Office.find(sub.office_id)
      office_sub.update_attributes({:status => "active"})
      flash[:notice] = "Your PayPal account was successfully set up for the #{description(sub.net_amount+sub.taxes_amount)} payment plan."
    rescue Exception => e
       #Subscription.delete(sub.id)
      Log.create(:message => "PayPal ---> errors : #{e.inspect} ")
      flash[:error] = "There was a problem setting up your PayPal account for the #{description(sub.net_amount+sub.taxes_amount)} payment plan. #{e.message}"
    end
    redirect_to :action => "plans"
  end

  def cancel
    redirect_to :action => "plans"
  end

  def subscribe
    @subscription =  Subscription.find(params[:subscription_id])
    office = Office.find(@subscription.office_id)
    # concurrent process needs to check
    if office.status == "active" and @subscription.status == "ok"
      flash.now[:notice] = "Your subcription has already subscribed by admin. Please check again your subscription status."
      render :action => "plans"
      return
    end
    response = EXPRESS_GATEWAY.setup_agreement(:description => description, :return_url => complete_developer_developer_account_url({:developer_id => @developer.id, :tariff_plan_id => params[:tariff_plan_id], :subscription_id => params[:subscription_id]}), :cancel_return_url => cancel_developer_developer_account_url(:developer_id => @developer.id))
    redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
  end

  def unsubscribe
    @subscription = Subscription.find(params[:subscription_id])
    history = SubscriptionHistory.find(:last, :conditions => ["subscription_id = ?", @subscription.id], :order => "id ASC")
    office = Office.find(@subscription.office_id)
    # concurrent process needs to check
    if office.status == "inactive" and !@subscription.status == "ok"
      flash.now[:notice] = "Your subcription has already unsubscribed by admin. Please check again your subscription status."
      render :action => "plans"
      return
    end
    # condition if office created first time and activate by admin
    if history.blank?
      activate_office(office)
      return
    end

    # condition if subscription history is not empty and has subscribed by admin
    if history.txn_type == "recurring_payment_profile_cancel"
      activate_office(office)
      return
    end

    # normal condition
    begin
      if @subscription.next_payment_date.blank?
        flash[:notice] = "Your PayPal transaction is still pending. Please wait until your transaction completed."
        redirect_to :action => "plans"
        return
      end
      @sm.unsubscribe(@subscription.id)
      @subscription.update_attributes({:ends_on => @subscription.next_payment_date.yesterday, :canceled_at => Time.now, :next_payment_date => nil})
      flash[:notice] = "Your PayPal account was successfully unsubscribed for the #{description(@subscription.net_amount+@subscription.taxes_amount)} payment plan."
    rescue Exception => e
      flash[:error] = "There was a problem unsubscribe your PayPal account for the #{description(@subscription.net_amount)} payment plan. #{e.message}"
    end
    redirect_to :action => "plans"
  end


  # def credit_card
  #   if request.post?
  #
  #     credit_card = ActiveMerchant::Billing::CreditCard.new({
  #         :type => params[:card_type],
  #         :number => params[:card_number].to_i,
  #         :month => params[:card_expiration_month].to_i,
  #         :year => params[:card_expiration_year].to_i,
  #         :first_name => params[:card_first_name],
  #         :last_name => params[:card_last_name],
  #         :verification_value => params[:card_verification_value]
  #     })
  #     current_developer.subscriptions.each do |sub|
  #       @sm.update_subscription(sub.id, {:card => credit_card})
  #     end
  #   end
  # end

  def activate_office(office)
    office.update_attribute(:status, "inactive")
    sp = office.subscription_plan('one_monthly')
    unless office.subscription.next_payment_date.blank?
      office.subscription.update_attributes({:ends_on => office.subscription.next_payment_date.yesterday, :canceled_at => Time.now, :next_payment_date => nil, :status => "canceled"})
    else
      office.subscription.update_attributes({:ends_on => sp['trial_end_date'], :canceled_at => Time.now, :next_payment_date => nil, :status => "canceled"})
    end
    flash.now[:notice] = "Your PayPal account was successfully unsubscribed for the #{description(office.subscription.net_amount+office.subscription.taxes_amount)} payment plan."
    render :action => "plans"
  end

  def new_history(notify)
    sh = SubscriptionHistory.new
    profile = RecurringPaymentProfile.find_by_gateway_reference(notify.profile_id)

    subscription = profile.subscriptions.first #could optimise this

    sh.subscription_id = subscription.id
    sh.gateway_reference = notify.profile_id
    sh.txn_type = 'recurring_payment'
    sh.office_id = notify.office_id
    sh.profile_status = 'Active'
    sh.amount = notify.net_amount
    sh.currency_code = notify.currency
    sh.payer_email = current_user.email
    #%w(txn_type payer_email profile_status payer_id amount currency_code).each do |attribute|
    #  eval("sh.#{attribute} = notify.#{attribute}")   rescue false
    #end

    return sh
  end

  private

  def get_developer
    @developer = Developer.find params[:developer_id]
    unless check_developer_ownership
      raise NotAuthorized
    end
  end

  def body_id
    @body_id = "account_centre"
  end

  def get_subscription_manager
    @sm = SubscriptionManager
  end

  def tariff_plan(tariff_plan_id)
    @sm ||= SubscriptionManager
    @tariff_plan ||= @sm.all_tariff_plans[tariff_plan_id]
    return @tariff_plan
  end

  def description(amount = nil)
    the_amount = params[:amount] || amount
		tp = tariff_plan(params[:tariff_plan_id])
    taxes = params[:taxes].to_f / 100.0
    return "#{tp['currency']}#{sprintf("%.2f", (the_amount.to_f / 100.00) + taxes)}#{SubscriptionManagement.format_periodicity(tp['payment_term']['periodicity'])}"
  end
end
