class ExtraOptionsController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office, :get_property
  append_before_filter :prepare_creation

  def index
    @extra_option = ExtraOption.new
  end

  def show
  end

  def new
  end

  def edit
    @extra_option = ExtraOption.find(params[:id])
  end

  def create
   @extra_option = ExtraOption.new(params[:extra_option].merge(:property_id => params[:property_id], :office_id => params[:office_id], :extra_detail_name_id => params[:parent]))

    respond_to do |format|
      if @extra_option.save
        flash[:notice] = 'ExtraOption was successfully created.'
        format.html { redirect_to agent_office_property_extra_options_path + "?parent_id=#{@extra_option.extra_detail_name_id}" }
      else
        format.html { redirect_to agent_office_property_extra_options_path + "?parent_id=#{@extra_option.extra_detail_name_id}" }
      end
    end
  end

  def update
    @extra_option = ExtraOption.find(params[:id])
    respond_to do |format|
      if @extra_option.update_attributes(params[:extra_option])
        flash[:notice] = 'ExtraOption was successfully updated.'
        format.html { redirect_to agent_office_property_extra_options_path + "?parent_id=#{@extra_option.extra_detail_name_id}" }
      else
        format.html { render edit_agent_office_property_extra_option_path(@agent,@office,@property) + "parent_id=#{@extra_option.extra_detail_name_id}" }
      end
    end
  end

  def destroy
    @extra_option = ExtraOption.find_by_id(params[:id])
    @extra_option.destroy unless @extra_option.blank?

    respond_to do |format|
      format.html { redirect_to agent_office_property_extra_options_path + "?parent_id=#{params[:parent_id]}"}
    end
  end

  def populate
    @extra_options = ExtraOption.find(:all, :conditions => ["`office_id` = ? AND `extra_detail_name_id` = ?", params[:office_id], params[:parent_id]], :order => "`option_order` ASC").map{|c| [c.option_title, c.id]}
    return(render(:json => @extra_options))
  end

  def prepare_creation
    @extra_options = ExtraOption.paginate(:all, :conditions => ["`office_id` = ? AND `extra_detail_name_id` = ?", params[:office_id], params[:parent_id]], :order => "`option_order` ASC", :page =>params[:page], :per_page => 10)
  end
end
