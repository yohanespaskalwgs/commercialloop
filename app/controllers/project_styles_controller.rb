class ProjectStylesController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new, :refresh]
  before_filter :get_agent_and_office

  def index
    @project_styles = ProjectStyle.find(:all, :conditions => ["`office_id` = ?", @office.id])
  end

  def show
  end

  def new
  end

  def edit
    @project_style = ProjectStyle.find(params[:id])
  end

  def create
   @project_style = ProjectStyle.new(params[:project_style].merge(:office_id => @office.id))
    respond_to do |format|
      if @project_style.save
        flash[:notice] = 'ProjectStyle was successfully created.'
        format.html { redirect_to :action => 'new' }
      else
        format.html { render :action => "new" }
      end
    end
  end

  def update
    @project_style = ProjectStyle.find(params[:id])
    respond_to do |format|
      if @project_style.update_attributes(params[:project_style])
        flash[:notice] = 'ProjectStyle was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @project_style = ProjectStyle.find_by_id(params[:id])
    @project_style.destroy unless @project_style.blank?
    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def refresh
    @project_styles = ProjectStyle.find(:all, :conditions => ["`office_id` = ?", params[:office_id]]).map{|c| [c.name, c.id]}
    return(render(:json => @project_styles))
  end
end
