class ElistsController < ApplicationController
  before_filter :login_required, :except => [:forward, :send_forward, :send_elist, :get_image, :visit_link, :visit_property_link, :preview, :unsubcribe]
  before_filter :get_agent_and_office
  skip_before_filter :verify_authenticity_token, :except => [:index, :destroy,:forward, :sample, :send_forward, :send_elist, :save_elist]
  layout "agent"
  include EcampaignHelper

  def index
    session["elist_property_checkbox"] = session["elist_listing_type"] = session["elist_sale_lease"] = session["elist_property_type"] = session["elist_suburb"] = session["elist_properties_opt"] = session["elist_id"] = session["elist_id"] = session["elist_layout"] = session["elist_data"] = session["elist_colour"] = session["elist_recipient_data"] = session["elist_contact_name_checkbox"] =  nil

    sql = Elist.show_elist("e.office_id = #{@office.id} ")
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    @elists = @mysql_result.all_hashes
    @mysql_result.free
  end

  def stats
    sql = Elist.show_elist("e.id = #{params[:id]} ")
    elist_stats = Elist.find_by_sql(sql.to_s)
    @elist_stats = elist_stats[0]
    @ecampaign_link_clicks = EcampaignClick.find(:all, :conditions => "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='elist' And `property_id` IS NULL")

    unless params[:link_id].blank?
      @elist_click = EcampaignClick.find(params[:link_id])
      @destination = @elist_click.destination_name
      @ecampaign_detail_clicks = EcampaignClickDetail.find(:all, :select => "DISTINCT(`agent_contact_id`), ecampaign_click_id", :conditions=>"ecampaign_click_id = #{params[:link_id]}")
    end

    unless params[:type].blank?
      @stats_type = params[:type]
      conditions = "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='elist'"
      conditions +=" And email_bounced=1" if params[:type] == "bounced"
      conditions +=" And email_opened=1" if params[:type] == "views"
      conditions +=" And email_forwarded=1" if params[:type] == "forward"
      if params[:type] == "unsubcribe"
        @ecampaign_tracks = EcampaignUnsubcribe.find(:all, :select => "DISTINCT(`agent_contact_id`), type_ecampaign_id", :conditions=> "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='elist'")
      else
        @ecampaign_tracks = EcampaignTrack.find(:all, :select => "DISTINCT(`agent_contact_id`), type_ecampaign_id", :conditions=> conditions)
      end
    end
  end

  def edit_elist
    require 'json'
    ec = Elist.find_by_id(params[:id])
    if ec.stored_data.blank?
      flash[:notice] = 'Elist data doesn\'t found.'
      redirect_to agent_office_elists_path(@agent, @office)
    else
      set_data(JSON.parse(ec.stored_data))
      session["elist_id"] = params[:id]
      redirect_to choose_layout_agent_office_elists_path(@agent, @office)
    end
  end

  def view_and_send
    require 'json'
    ec = Elist.find_by_id(params[:id])
    if ec.stored_data.blank?
      flash[:notice] = 'Elist data doesn\'t found.'
      redirect_to agent_office_elists_path(@agent, @office)
    else
      set_data(JSON.parse(ec.stored_data))
      session["elist_id"] = params[:id]
      redirect_to view_sample_agent_office_elists_path(@agent, @office)
    end
  end

  def duplicate_elist
    ActiveRecord::Base.connection.execute("INSERT INTO elists (`office_id`, `title`, `stored_layout`, `stored_data`, `created_at`, `updated_at`) SELECT `office_id`, `title`, `stored_layout`, `stored_data`, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP() FROM elists WHERE id = #{params[:id]}") unless params[:id].blank?
    flash[:notice] = 'Elist has successfully been duplicated.'
    redirect_to agent_office_elists_path(@agent, @office)
  end

  def preview
    elist = Elist.find_by_id(params[:id])
    if !elist.blank?
      email = elist.stored_layout.gsub("{elist_track_property_url}", full_base_url+visit_property_link_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{elist_track_url}", full_base_url+visit_link_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{forward}", full_base_url+forward_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{preview}", full_base_url+preview_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_elist_path(elist.office.agent,elist.office,elist)+"?track_id=#{params[:track_id]}")
      @html = email.gsub("<img src='/images/click_here.gif'/>", "<img  style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_elists_path(elist.office.agent,elist.office)+"?track_id=#{params[:track_id]}&elist_id=#{elist.id}"}'/>")
    else
      @html = "<center><b>Elist Not Found!</b></center>"
    end
    render :layout => false
  end

  def unsubcribe
    track = EcampaignTrack.find_by_id(params[:track_id])
    unless track.blank?
      ac = track.agent_contact
      ac.inactive = true
      ac.save(false)
      ContactNote.create({:agent_contact_id => ac.id, :agent_user_id => ac.office.office_contact, :note_type_id => 22, :description => "User decided to unsubscribe through clicking option in elist", :note_date => Time.now})
      EcampaignUnsubcribe.create({:type_ecampaign_id => params[:id], :type_ecampaign => 'elist', :agent_contact_id => track.agent_contact_id})
    end
    @login_url = full_base_url
    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def visit_link
    #localhost:3000/agents/1/offices/1/elists/1/visit_link?track_id=1
    #track clicked link
    unless params[:link_url].blank?
      destination_link = params[:link_url].gsub("http://", "")
      ec  = EcampaignClick.find(:first, :conditions => "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='elist' And `destination_link` LIKE '%#{destination_link}%'")
      unless ec.blank?
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_clicks SET clicked = clicked + 1 WHERE `id`=#{ec.id}")
        unless params[:track_id].blank?
          track = EcampaignTrack.find(params[:track_id])
          EcampaignClickDetail.create({:ecampaign_click_id=> ec.id, :agent_contact_id => track.agent_contact_id})
        end
      end
      redirect_to params[:link_url]
    else
      redirect_to agent_office_elists_path(@agent, @office)
    end
  end

  def visit_property_link
    #track property clicked link
    unless params[:property_id].blank?
      ec  = EcampaignClick.find(:first, :conditions => "`type_ecampaign` LIKE 'elist' And	`type_ecampaign_id` = #{params[:id]} And `property_id` = #{params[:property_id]}")
      unless ec.blank?
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_clicks SET clicked = clicked + 1 WHERE `id`=#{ec.id}")
        unless params[:track_id].blank?
          track = EcampaignTrack.find(params[:track_id])
          EcampaignClickDetail.create({:ecampaign_click_id=> ec.id, :agent_contact_id => track.agent_contact_id})
        end
      end
      property = Property.find(params[:property_id])
      if property.detail.property_url.blank?
        if @office.url.present?
          office_url = "http://#{@office.url.gsub("http://", "")}"
          redirect_to "#{office_url}/#{params[:property_id]}"
        else
          redirect_to full_base_url+"/agents/#{property.agent_id}/offices/#{property.office_id}/properties/#{property.id}"
        end
      else
        redirect_to "http://#{property.detail.property_url.gsub("http://", "")}"
      end
    else
      redirect_to agent_office_elists_path(@agent, @office)
    end
  end

  def delete_elist
    unless params[:id].blank?
     ec  = Elist.find_by_id(params[:id])
     unless ec.blank?
       eclicks = EcampaignClick.find(:all, :conditions => "ecampaign_id = #{params[:id]}")
       unless eclicks.blank?
         eclicks.each{|eclick| ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_click_details WHERE ecampaign_click_id = #{eclick.id}")}
         ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_clicks WHERE type_ecampaign_id = #{ec.id} And type_ecampaign='elist'")
       end
       ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_tracks WHERE type_ecampaign_id = #{ec.id} And type_ecampaign='elist'")
       ec.destroy
     end
    end
    flash[:notice] = 'Elist already deleted.'
    redirect_to agent_office_elists_path(@agent, @office)
  end

  # ============================================
  #                    STEPS
  #=============================================
  def choose_layout
    if params[:layout]
      session["elist_layout"] = params[:layout]
      save_update_elist
      redirect_to choose_properties_agent_office_elists_path(@agent, @office)
    end
    @elist_layout=session["elist_layout"]
  end

  def choose_properties
    if !params[:update].blank?
      init_search_properties
      redirect_to choose_data_agent_office_elists_path(@agent, @office)
    else
      session["elist_properties_opt"] = { "status" =>"", "period" =>"", "min_price" =>"", "max_price" =>"", "min_rent" => "", "max_rent" => "", "min_size" => "", "max_size" => "", "building_min_size" => "", "building_max_size" => "", "baths" => "", "beds" => "", "cars" => ""} if session["elist_properties_opt"].blank?
    end
    property_opt = session["elist_properties_opt"]
    @status = property_opt["status"]
    @period = property_opt["period"]
    @min_price = property_opt["min_price"]
    @max_price = property_opt["max_price"]
    @min_rent = property_opt["min_rent"]
    @max_rent = property_opt["max_rent"]
    @min_size = property_opt["min_size"]
    @max_size = property_opt["max_size"]
    @building_min_size = property_opt["building_min_size"]
    @building_max_size = property_opt["building_max_size"]
    @baths = property_opt["baths"]
    @beds = property_opt["beds"]
    @cars = property_opt["cars"]
    @property_checkbox_ids = session["elist_property_checkbox"] unless session["elist_property_checkbox"].blank?
    @value_listing_type = @value_listing_types = ""
    session["elist_listing_type"].each{|listing|
      @value_listing_type = @value_listing_type + '#' + listing
      @value_listing_types = @value_listing_types + '<span id="'+listing+'" style="margin-right:20px;">'+listing+'<a href="javascript:remove_options(\'listing_type\',\''+listing+'\')">x</a></span>' unless listing == "All"
    } unless session["elist_listing_type"].blank?

    @value_sale_lease = @value_sale_leases = ""
    session["elist_sale_lease"].each{|sale_lease|
      @value_sale_lease = @value_sale_lease + '#' + sale_lease
      @value_sale_leases = @value_sale_leases + '<span id="'+sale_lease+'" style="margin-right:20px;">'+sale_lease+'<a href="javascript:remove_options(\'sale_lease\',\''+sale_lease+'\')">x</a></span>' unless sale_lease == "All"
    } unless session["elist_sale_lease"].blank?

    @value_property_type = @value_property_types = ""
    session["elist_property_type"].each{|property_type|
      @value_property_type = @value_property_type + '#' + property_type
      @value_property_types = @value_property_types + '<span id="'+property_type+'" style="margin-right:20px;">'+property_type+'<a href="javascript:remove_options(\'property_type\',\''+property_type+'\')">x</a></span>' unless property_type == "All"
    } unless session["elist_property_type"].blank?

    @value_suburb = @value_suburbs = ""
    session["elist_suburb"].each{|suburb|
      @value_suburb = @value_suburb + '#' + suburb
      @value_suburbs = @value_suburbs + '<span id="'+suburb+'" style="margin-right:20px;">'+suburb+'<a href="javascript:remove_options(\'suburb\',\''+suburb+'\')">x</a></span>' unless suburb == "All"
    } unless session["elist_suburb"].blank?
    prepare_data
  end

  def choose_data
    redirect_to choose_layout_agent_office_elists_path(@agent, @office) if session["elist_layout"].blank?
    if !params[:update].blank?
      session["elist_data"] = params[:data]
      save_update_elist
      redirect_to choose_colour_agent_office_elists_path(@agent, @office)
    else
      session["elist_data"] = { "sender_email" =>"", "sender_description" =>"", "sender_title" =>"", "office_id" =>"", "link_url" => ""} if session["elist_data"].blank?
    end

    @data = session["elist_data"]
    @contacts = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    @elist_layout=session["elist_layout"]
  end

  def choose_colour
    @header_image = find_image("Brochure Header")
    @footer_image = find_image("Brochure Footer")
    @find_side_image = find_image("Side Image")
    @color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #999999 #CCCCCC #f0f0f0 #FFFFFF)
    unless params[:choose_colours].blank?
      session["elist_colour"] = { "bgcolor" => params[:bgcolor], "textcolor" => params[:textcolor], "heading" =>params[:heading], "headingtextcolor" =>params[:headingtextcolor]}
      save_update_elist
      redirect_to select_recipient_agent_office_elists_path(@agent, @office)
    else
      session["elist_colour"] = { "bgcolor" =>"", "textcolor" =>"", "heading" =>"", "headingtextcolor" =>""} if session["elist_colour"].blank?
    end
    @bgcolor = session["elist_colour"]["bgcolor"].blank? ? "#FFFFFF": session["elist_colour"]["bgcolor"]
    @textcolor =  session["elist_colour"]["textcolor"].blank? ? "#000000" : session["elist_colour"]["textcolor"]
    @heading =  session["elist_colour"]["heading"].blank? ? "#f0f0f0" : session["elist_colour"]["heading"]
    @headingtextcolor =  session["elist_colour"]["headingtextcolor"].blank? ? "#E10A3C" : session["elist_colour"]["headingtextcolor"]
  end

  def select_recipient
    if !params[:update].blank?
      session["elist_contact_name_checkbox"] = params[:contact_name_checkbox]
      session["elist_recipient_data"] = params[:recipient_data]
      save_update_elist
      redirect_to view_sample_agent_office_elists_path(@agent, @office)
    else
      session["elist_recipient_data"] = {"search_contact_name" => ""} if session["elist_recipient_data"].blank?
    end

    @recipient_data = session["elist_recipient_data"]
    @contact_name_checkbox = session["elist_contact_name_checkbox"]
    @contact_name_checkbox_ids = ""
    @contact_name_checkbox.each{|cid|
      @contact_name_checkbox_ids = @contact_name_checkbox_ids + '#' + cid
    } unless @contact_name_checkbox.blank?
  end

  def view_sample
    redirect_to send_elist_agent_office_elists_path(@agent, @office) if params[:send_elist]
    elist_data
  end

  def send_elist
    elist_data
    html = render_to_string(:layout => false)
    title = session["elist_data"]["sender_title"]
    sender_id = session["elist_data"]["sender_email"]
    contact_names = session["elist_contact_name_checkbox"]
    property_checkbox_ids = session["elist_property_checkbox"] unless session["elist_property_checkbox"].blank?
    properties_checkbox = Property.find(:all, :conditions => "id IN (#{property_checkbox_ids})") unless property_checkbox_ids.blank?
    all_data = formated_data

    unless session["elist_id"].blank?
      elist = Elist.find(session["elist_id"])
      elist.title = title
      elist.stored_layout = html
      elist.stored_data = all_data.to_json
      elist.sent_at = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      elist.save!
    end

    properties_checkbox.each{|property|
      ec  = EcampaignClick.find(:first, :conditions => "`type_ecampaign` LIKE 'elist' And	`type_ecampaign_id` = #{elist.id} And `property_id` = #{property.id}")
      EcampaignClick.create(:type_ecampaign => 'elist', :type_ecampaign_id => elist.id, :property_id => property.id, :clicked => 0) if ec.blank?
    } unless properties_checkbox.blank?

    if elist
      spawn(:kill => true) do
        contact_names.each{|contact_name_id| send_mail_elist(contact_name_id, sender_id, elist) } unless contact_names.blank?
      end
    end
    flash[:notice] = "Elist has successfully been sent"
    redirect_to agent_office_elists_path(@agent, @office)
  end

  def send_mail_elist(contact_id, sender_id, elist)
    contact = AgentContact.find_by_id(contact_id)
    unless contact.blank?
    if contact.inactive != true
      ecampaign_track = EcampaignTrack.create(:type_ecampaign_id => elist.id, :type_ecampaign => 'elist', :agent_contact_id => contact_id)
      if ecampaign_track
        sender = AgentUser.find_by_id(sender_id)
        if !contact.email.blank?
          begin
            EcampaignMailer.deliver_send_elist(@base_url, sender, elist, ecampaign_track, contact)
          rescue
            ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
          end
        else
          ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
        end
        ContactNote.create({:agent_contact_id => contact.id, :agent_user_id => sender_id, :note_type_id => 23, :description => "An eCbrochure was sent to this user", :note_date => Time.now})
      end
    end
    end
  end

  def sample
    @elist = Elist.find(params[:id])
    respond_to do |format|
      format.html { render :layout => false}
    end
  end

  def send_sample
    unless params[:id].blank?
      unless params[:email].blank?
        elist_data
        html = render_to_string(:layout => false)
        @elist = Elist.find(params[:id])
        @elist.stored_layout = html
        @elist.save!

        sender = AgentUser.find_by_id(session["elist_data"]["sender_email"])
        params[:email].each do |email|
        unless email.blank?
          begin
            EcampaignMailer.deliver_send_sample(sender, @elist, email)
          rescue
            Log.create({:message => "Failed send sample email elist #{@elist.id}"})
          end
        end
        end
      end
      flash[:notice] = 'Sample Email already sent.'
    end
    redirect_to sample_agent_office_elist_path(@agent,@office,@elist)
  end

  def formated_data
    all_data = { "elist_layout" => session["elist_layout"],
    "elist_properties_opt" => session["elist_properties_opt"],
    "elist_data" => session["elist_data"],
    "elist_colour" => session["elist_colour"],
    "elist_contact_name_checkbox" => session["elist_contact_name_checkbox"],
    "elist_recipient_data" => session["elist_recipient_data"],
    "elist_suburb" => session["elist_suburb"],
    "elist_property_checkbox" => session["elist_property_checkbox"],
    "elist_listing_type" => session["elist_listing_type"],
    "elist_sale_lease" => session["elist_sale_lease"],
    "elist_property_type" => session["elist_property_type"]
    }
    return all_data
  end

  def save_update_elist
    all_data = formated_data
    unless session["elist_id"].blank?
      ec = Elist.find(session["elist_id"])
      unless ec.blank?
        ec.title = session["elist_data"]["sender_title"] unless session["elist_data"].blank?
        ec.stored_data = all_data.to_json
        ec.save!
      end
    else
      ec = Elist.create(:office_id => @office.id, :title => (session["elist_data"].blank? ? "": session["elist_data"]["sender_title"]), :stored_data => all_data.to_json)
      session["elist_id"] = ec.id
    end
  end

  def forward
    #forward_agent_office_elists_path
    #localhost:3000/agents/1/offices/1/elists/1/forward?track_id=1
    @agent_contact = AgentContact.new
    @track_id = params[:track_id]
    @elist = Elist.find(params[:id])

    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def send_forward
    @agent_contact = AgentContact.new(params[:agent_contact])
    @track_id = params[:track_id]
    respond_to do |format|
      @agent_contact.elist_contact = true
      if @agent_contact.save
        elist_track = EcampaignTrack.find_by_id(params[:track_id])
        if elist_track
          spawn(:kill => true) do
            elist = elist_track.elist
            sender = AgentContact.find_by_id(elist_track.agent_contact_id)
            begin
              ElistMailer.deliver_send_elist(full_base_url, sender, elist, elist_track, @agent_contact)
            rescue Net::SMTPServerBusy
              ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{elist_track.id}") unless elist_track.blank?
            end
          end
        end
        #track forwarded link
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_forwarded = 1 WHERE id=#{@track_id}") unless @track_id.blank?
        flash[:notice] = 'Elist has successfully been forwarded.'
        format.html { redirect_to :action => 'forward' }
      else
        format.html { render :action => "forward", :layout => "new_level4" }
      end
    end
  end

  def get_image
    #track opened email
    ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_opened =1 WHERE id=#{params[:track_id]}") unless params[:track_id].blank?
    image = RAILS_ROOT+"/public/images/transparant.png"
    render :text => open(image, "rb").read
  end

  def elist_data
    session["elist_data"] = { "sender_email" =>"", "sender_description" =>"", "sender_title" =>"", "office_id" =>""} if session["elist_data"].blank?

    @base_url = full_base_url
    @elist_id = session["elist_id"]
    @elist = Elist.find_by_id(@elist_id)
    @side_image = find_image("Side Image")
    @header_image = find_image("Brochure Header")
    @footer_image = find_image("Brochure Footer")
    @data = session["elist_data"]
    property_checkbox_ids = session["elist_property_checkbox"] unless session["elist_property_checkbox"].blank?
    @properties = Property.find(:all, :conditions => "id IN (#{property_checkbox_ids})") unless property_checkbox_ids.blank?
    unless session["elist_colour"].blank?
      @bgcolor = session["elist_colour"]["bgcolor"]
      @textcolor = session["elist_colour"]["textcolor"]
      @heading = session["elist_colour"]["heading"]
      @headingtextcolor = session["elist_colour"]["headingtextcolor"]
    end

    unless @data.blank?
      unless @data["sender_email"].blank?
        @sender = AgentUser.find_by_id(@data["sender_email"])
        unless @sender.blank?
          portraits = @sender.portrait_image
          unless portraits.blank?
            portraits.each do |img_portrait|
              if img_portrait.position == 1
                @img_portrait_1 = img_portrait
              else
                @img_portrait_2 = img_portrait
              end
            end
          end
        end
      end
    end

    @elist_layout=session["elist_layout"]
  end

  def set_data(db_data)
    session["elist_layout"] = db_data["elist_layout"]
    session["elist_properties_opt"] = db_data["elist_properties_opt"]
    session["elist_data"] = db_data["elist_data"]
    session["elist_colour"] = db_data["elist_colour"]
    session["elist_group_id"] = db_data["elist_group_id"]
    session["elist_contact_name_checkbox"] = db_data["elist_contact_name_checkbox"]
    session["elist_recipient_data"] = db_data["elist_recipient_data"]
    session["elist_suburb"] = db_data["elist_suburb"]
    session["elist_property_checkbox"] = db_data["elist_property_checkbox"]
    session["elist_listing_type"] = db_data["elist_listing_type"]
    session["elist_sale_lease"] = db_data["elist_sale_lease"]
    session["elist_property_type"] = db_data["elist_property_type"]
  end

  def search
    if params[:status] == "sold"
      condition = "`status` IN (2,3)"
    else
      condition = "`status` IN (1,5)"
    end

    text = ''
    unless params[:search_value].blank?
      if @agent.developer.allow_listing_office == true
        condition = "((`suburb` LIKE '%#{params[:search_value]}%' OR `street` LIKE '%#{params[:search_value]}%') OR `id` = '#{params[:search_value]}') AND `status` IN (1,5) And `office_id`=#{@office.id}"
      else
        condition = "((`suburb` LIKE '%#{params[:search_value]}%' OR `street` LIKE '%#{params[:search_value]}%') OR `id` = '#{params[:search_value]}') AND `status` IN (1,5)"
      end
    else
      unless params[:selection_office_id].blank?
        tmp_office_id = params[:selection_office_id].split("#").delete_if{|x| x.blank?}
        office_id = tmp_office_id.join(",")
        condition += " And `office_id` IN (#{office_id})"
      else
        condition += " And `office_id` = #{@office.id}" if @agent.developer.allow_listing_office == true
      end

      unless params[:selection_property_type].blank?
        tmp_property_type = params[:selection_property_type].split("#").delete_if{|x| x.blank?}
        condition += " And `property_type` IN ('#{tmp_property_type.join("','")}')"
      end

      unless params[:selection_listing_type].blank?
        tmp_listing_type = params[:selection_listing_type].split("#").delete_if{|x| x.blank?}
        ori_listings = tmp_listing_type.join("','")
        deal_types = ""
        if ori_listings.include?("CommercialSale")
          ori_listings = ori_listings.gsub("CommercialSale", "Commercial")
          deal_types += "'Sale'"
        end
        if ori_listings.include?("CommercialLease")
          ori_listings = ori_listings.gsub("CommercialLease", "Commercial")
          if deal_types.blank?
            deal_types += "'Lease'"
          else
            deal_types += ",'Lease', 'Both'"
          end
        end
        condition += " And `type` IN ('#{ori_listings}')"
        condition += " And (`deal_type` IN (#{deal_types}) OR `deal_type` IS NULL)" unless deal_types.blank?
      end
    end

    properties = Property.find(:all, :conditions => condition)
    jsons = []
    unless properties.blank?
      properties.each{|p|
        unless p.blank?
        ad = [ p.unit_number, p.street_number, p.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
        address = [ad, p.suburb ].join(", ")
        jsons << {'id' => p.id, 'address' => "#{address unless p.address.blank?}",'suburb' =>"#{p.suburb}",'type' =>"#{p.type}",'price' =>"$#{p.price.to_i}",'status' =>"#{p.status_name}", 'href' => agent_office_properties_path(@agent, @office)+"?ID=#{p.id}"}
        end
      } unless properties.nil?
      text = {"results" => jsons, "total" => (properties ? properties.length : 0) }
    end

    render :json => text
  end

  def search_property
    search_data
    property_ids = params[:property_ids]
    property_ids = property_ids.split(",") unless property_ids.blank?

    jsons = []
    @properties.each{|property|
      address = "#{property["unit_number"].blank? ? property["street_number"].try.downcase : property["unit_number"]+"/"+property["street_number"].try.downcase} #{property["street"].try.downcase}"
      if !property_ids.blank?
        checked = (property_ids.include?(property["id"]))? "checked" : ""
      else
        checked = "checked"
      end
      jsons << {'id' => property["id"], 'address' => address, 'checked' => checked}
    } unless @properties.nil?
    text = {"results" => jsons, "total" => @properties.length }
    return(render(:json => text))
  end

  def search_contact
    unless params[:selection_group_id].blank?
      group_ids = params[:selection_group_id].split("#").delete_if{|x| x.blank?}
      gc = GroupContactRelation.find(:all, :select => "DISTINCT(agent_contact_id)", :conditions => {:group_contact_id => group_ids})
    end

    jsons = []
    if !gc.blank?
      gc.each do |ac|
        unless ac.agent_contact.blank?
          jsons << {'id' => ac.agent_contact.id, 'name' => "#{ac.agent_contact.full_name}"} if ac.agent_contact.inactive != true
        end
      end
      text = {"results" => jsons, "total" => (gc ? gc.length : 0) }
    else
      text = ''
    end
    render :json => text
  end

  def find_image(type)
    get_header_footer(@office.id, type)
  end

  def search_data(check_checkbox = nil)
    init_search_properties
    property_opt = session["elist_properties_opt"]
    @status = property_opt["status"]
    @period = property_opt["period"]
    @min_price = property_opt["min_price"].gsub(/[^0-9.]/, '') unless property_opt["min_price"].blank?
    @max_price = property_opt["max_price"].gsub(/[^0-9.]/, '') unless property_opt["max_price"].blank?
    @min_rent = property_opt["min_rent"].gsub(/[^0-9.]/, '') unless property_opt["min_rent"].blank?
    @max_rent = property_opt["max_rent"].gsub(/[^0-9.]/, '') unless property_opt["max_rent"].blank?
    @min_size = property_opt["min_size"].gsub(/[^0-9.]/, '') unless property_opt["min_size"].blank?
    @max_size = property_opt["max_size"].gsub(/[^0-9.]/, '') unless property_opt["max_size"].blank?
    @building_min_size = property_opt["building_min_size"].gsub(/[^0-9.]/, '') unless property_opt["building_min_size"].blank?
    @building_max_size = property_opt["building_max_size"].gsub(/[^0-9.]/, '') unless property_opt["building_max_size"].blank?
    @baths = property_opt["stock_baths"]
    @beds = property_opt["stock_beds"]
    @cars = property_opt["stock_cars"]
    @listing_type = session["elist_listing_type"].blank? ? "All" : session["elist_listing_type"]
    @suburb = session["elist_suburb"].blank? ? "All" : session["elist_suburb"]
    @property_type = session["elist_property_type"].blank? ? "All" : session["elist_property_type"]
    @sale_lease = session["elist_sale_lease"].blank? ? "All" : session["elist_sale_lease"]
    @property_checkbox_ids = session["elist_property_checkbox"] unless session["elist_property_checkbox"].blank?
    days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', "Friday", "Saturday", "Sunday"]

    order_by = "`properties`.`id`"
    ids_sql = "properties.id IN (#{@property_checkbox_ids})" if check_checkbox == true && !@property_checkbox_ids.blank?

    if !ids_sql.blank?
      conditions = ["#{ids_sql} AND (`attachments`.position = (SELECT min(atc.`position`) as latest FROM `attachments` as atc WHERE `atc`.`attachable_id` = `properties`.id AND `atc`.`type` LIKE 'Image' AND `atc`.`attachable_type` LIKE 'Property' AND `atc`.thumbnail IS NULL))"]
      @properties = elist_sql(conditions, order_by)
    else

      lt_conditions = ""
      unless @listing_type.blank?
        @listing_type.each_with_index{|list,index|
          lt_conditions += "," if index > 0
          lt_conditions += "'#{list}'"
        }
        listing_conditions = @listing_type.include?("All") ? "" : "And (properties.type IN (#{lt_conditions}))"
      end

      sb_conditions = ""
      unless @suburb.blank?
        @suburb.each_with_index{|sb,index|
          sb_conditions += "," if index > 0
          sb_conditions += "'#{sb}'"
        }
        suburb_conditions = @suburb.include?("All") ? "" : "And (properties.suburb IN (#{sb_conditions}))"
      end

      pt_conditions = ""
      unless @property_type.blank?
        @property_type.each_with_index{|pt,index|
          pt_conditions += "," if index > 0
          pt_conditions += "'#{pt}'"
        }
        property_type_conditions = @property_type.include?("All") ? "" : "And (properties.property_type IN (#{pt_conditions}))"
      end

      sl_conditions = ""
      unless @sale_lease.blank?
        @sale_lease.each_with_index{|sl,index|
          sl_conditions += "," if index > 0
          sl_conditions += "'#{sl}'"
        }
        sale_lease_conditions = @sale_lease.include?("All") ? "" : "And (properties.deal_type IN (#{sl_conditions}))"
      end

      price_conditions = size_conditions = ""
      if !@min_price.blank? && !@max_price.blank?
        price_conditions = "And ((properties.price >= #{@min_price} And properties.price <= #{@max_price})"
        if !@min_rent.blank? && !@max_rent.blank?
          if @listing_type.include?("All") || @listing_type.include?("Commercial")
            price_conditions += " OR (commercial_details.current_rent >= #{@min_rent} And commercial_details.current_rent <= #{@max_rent})"
          end
        end
        price_conditions += ")"
      end

      if !@min_size.blank? && !@max_size.blank?
        if @listing_type.include?("All") || @listing_type.include?("Commercial")
          size_conditions = "And (commercial_details.land_area >= #{@min_size} And commercial_details.land_area <= #{@max_size})"
        end
      end

      if !@building_min_size.blank? && !@building_max_size.blank?
        if @listing_type.include?("All") || @listing_type.include?("Commercial")
          size_conditions = "And (commercial_details.floor_area >= #{@building_min_size} And commercial_details.floor_area <= #{@building_max_size})"
        end
      end

      if !@beds.blank?
        if @listing_type.include?("All") || @listing_type.include?("ResidentialSale") || @listing_type.include?("ResidentialLease")
          size_conditions = "And (residential_sale_details.bedrooms >= #{@beds} OR residential_lease_details.bedrooms >= #{@beds})"
        end
      end

      if !@baths.blank?
        if @listing_type.include?("All") || @listing_type.include?("ResidentialSale") || @listing_type.include?("ResidentialLease")
          size_conditions = "And (residential_sale_details.bathrooms >= #{@baths} OR residential_lease_details.bathrooms >= #{@baths})"
        end
      end

      if !@cars.blank?
        if @listing_type.include?("All") || @listing_type.include?("ResidentialSale") || @listing_type.include?("ResidentialLease")
          size_conditions = "And (residential_sale_details.carport_spaces >= #{@cars} OR residential_lease_details.carport_spaces >= #{@cars})"
        end
      end

      status_sql = ""
      if @status.blank?
        status_sql = " AND (properties.status = 1 OR properties.status = 5)"
      else
        sts = ''
        @status.each_with_index{|d,index| sts += (index==0)? "#{d}" : ",#{d}" }
        status_sql = " AND properties.status IN (#{sts}) "
        unless @period.blank?
          if @period == '7d' || @period == '14d'
            if sts.include?("2")
              status_sql += " AND ((properties.status != 2 And properties.updated_at > '#{1.year.ago.strftime("%Y-%m-%d %H:%M")}') OR (properties.status = 2 And properties.sold_on > '#{7.day.ago.strftime("%Y-%m-%d")}') OR (properties.status = 3 And properties.leased_on > '#{7.day.ago.strftime("%Y-%m-%d")}')) " if @period == '7d'
              status_sql += " AND ((properties.status != 2 And properties.updated_at > '#{1.year.ago.strftime("%Y-%m-%d %H:%M")}') OR (properties.status = 2 And properties.sold_on > '#{14.day.ago.strftime("%Y-%m-%d")}') OR (properties.status = 3 And properties.leased_on > '#{14.day.ago.strftime("%Y-%m-%d")}')) " if @period == '14d'
            end
          else
            ( status_sql += " AND ((properties.status NOT IN (2,3) And properties.updated_at > '#{1.year.ago.strftime("%Y-%m-%d %H:%M")}') OR (properties.status = 2 And properties.sold_on > '#{@period.to_i.month.ago.strftime("%Y-%m-%d")}') OR (properties.status = 3 And properties.leased_on > '#{@period.to_i.month.ago.strftime("%Y-%m-%d")}')) " if sts.include?("2") || sts.include?("3"))
          end
        end
      end

      conditions = ["properties.office_id = #{@office.id} #{status_sql} #{listing_conditions} #{suburb_conditions} #{property_type_conditions} #{sale_lease_conditions} #{price_conditions} #{size_conditions}"]
      @properties = elist_sql(conditions, order_by)
      @psize = @properties.size
    end
  end

  def elist_sql(conditions, order_by)
    sql = [" SELECT DISTINCT `properties`.id, `properties`.status, `properties`.type, `properties`.property_id, `properties`.headline, `properties`.description, `properties`.unit_number, `properties`.street_number, `properties`.street, `properties`.suburb, `properties`.display_price, `properties`.price, `properties`.display_price_text, `properties`.primary_contact_id, `properties`.deal_type "]
    sql[0] += ",`residential_sale_details`.`bedrooms` as residential_sale_bedrooms,`residential_sale_details`.`bathrooms` as residential_sale_bathrooms,`residential_sale_details`.`carport_spaces` as residential_sale_carport_spaces,`residential_sale_details`.`garage_spaces` as residential_sale_garage_spaces,`residential_sale_details`.`land_area` as residential_sale_land,`residential_sale_details`.`land_area_metric` as residential_sale_land_metric, `residential_lease_details`.`bedrooms` as residential_lease_bedrooms,`residential_lease_details`.`bathrooms` as residential_lease_bathrooms, `residential_lease_details`.`carport_spaces` as residential_lease_carport_spaces,`residential_lease_details`.`garage_spaces` as residential_lease_garage_spaces,`residential_lease_details`.`land_area` as residential_lease_land,`residential_lease_details`.`land_area_metric` as residential_lease_land_metric, `commercial_details`.`current_rent` AS `commercial_current_rent`, `commercial_details`.`land_area` AS `commercial_land`, `commercial_details`.`land_area_metric` AS `commercial_land_metric`, `business_sale_details`.`land_area` AS `business_sale_land`, `business_sale_details`.`land_area_metric` AS `business_sale_land_metric`, `project_sale_details`.`half_bedroom` as project_sale_half_bedroom, `project_sale_details`.`bedrooms` as project_sale_bedrooms, `project_sale_details`.`half_bathroom`  as project_sale_half_bathroom, `project_sale_details`.`bathrooms` as project_sale_bathrooms, `project_sale_details`.`carport_spaces` as project_sale_carport_spaces, `project_sale_details`.`garage_spaces` as project_sale_garage_spaces, `project_sale_details`.`land_area` as project_sale_land, `project_sale_details`.`land_area_metric` as project_sale_land_metric,`holiday_lease_details`.`half_bedroom` as holiday_lease_half_bedroom,`holiday_lease_details`.`bedrooms` as holiday_lease_bedrooms, `holiday_lease_details`.`bathrooms` as holiday_lease_bathrooms, `holiday_lease_details`.`carport_spaces` as holiday_lease_carport_spaces, `holiday_lease_details`.`garage_spaces` as holiday_lease_garage_spaces, `holiday_lease_details`.`land_area` as holiday_lease_land, `holiday_lease_details`.`land_area_metric` as holiday_lease_land_metric from `properties` "
    sql[0] += " LEFT JOIN `users` ON (`users`.`id` = `properties`.`primary_contact_id` ) LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "
    sql_part1 = [" WHERE ( #{conditions.to_s} ) "]
    sql_part2 = [" AND ( properties.deleted_at IS NULL ) "]
    sql_part3 = [" ORDER BY #{order_by} ASC"]
    sql[0] += sql_part1.to_s
    sql[0] += sql_part2.to_s
    sql[0] += sql_part3.to_s
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    properties = @mysql_result.all_hashes
    @mysql_result.free
    return properties
  end

  def prepare_data
    @listings = ['ResidentialSale', 'ResidentialLease', 'HolidayLease', 'Commercial', 'CommercialBuilding','ProjectSale', 'BusinessSale', 'NewDevelopment', 'LandRelease'].map{|m| [m,m]}.unshift(['Please select',nil])
    ptypes = Property.find_by_sql("SELECT DISTINCT(`property_type`) FROM `properties` WHERE office_id = #{@office.id} And `property_type` IS NOT NULL ORDER BY `property_type` ASC")
    @property_types = ptypes.map{|m| [m.property_type, m.property_type] }.delete_if{|x, y| x.blank?}.unshift(['Please select',nil])

    suburbs = Property.find_by_sql("SELECT DISTINCT(`suburb`) FROM `properties` WHERE office_id = #{@office.id} And `suburb` IS NOT NULL ORDER BY `suburb` ASC")
    @suburbs = suburbs.map{|m| [m.suburb, m.suburb] unless m.suburb.blank?}.delete_if{|x, y| x.blank?}.unshift(['Please select',nil])
    @sale_leases = [["Sale & Lease", "Both"], ["Sale", "Sale"], ["Lease", "Lease"]].map{|k, v| [k, v]}.unshift(['Please select',nil])
  end

  def init_search_properties
    session["elist_properties_opt"] = { "status" => params[:status], "period" => params[:period], "min_price" => params[:min_price], "max_price" => params[:max_price], "min_rent" => params[:min_rent], "max_rent" => params[:max_rent], "min_size" => params[:min_size], "max_size" => params[:max_size], "building_min_size" => params[:building_min_size], "building_max_size" => params[:building_max_size], "baths" => params[:baths], "beds" => params[:beds], "cars" => params[:cars]}
    session["elist_listing_type"] =  params[:listing_type].blank? ? "" : params[:listing_type].split("#").delete_if{|x| x.blank?}
    session["elist_suburb"] =  params[:suburb].blank? ? "" : params[:suburb].split("#").delete_if{|x| x.blank?}
    session["elist_property_type"] = params[:property_type].blank? ? "" : params[:property_type].split("#").delete_if{|x| x.blank?}
    session["elist_sale_lease"] = params[:sale_lease].blank? ? "" : params[:sale_lease].split("#").delete_if{|x| x.blank?}
    session["elist_property_checkbox"] = "#{params[:property_checkbox].join(",")}" unless params[:property_checkbox].blank?
  end
end