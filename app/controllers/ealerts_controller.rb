class EalertsController < ApplicationController
  before_filter :login_required, :except => [:forward, :send_forward, :send_ealert, :get_image, :visit_link, :visit_property_link, :preview, :unsubcribe]
  before_filter :get_agent_and_office
  skip_before_filter :verify_authenticity_token, :except => [:index, :destroy,:forward, :sample, :send_forward, :send_ealert, :save_ealert]
  layout "agent"
  include EcampaignHelper

  def index
    session["ealert_id"] = session["ealert_status"] = session["ealert_layout"] = session["ealert_data"] = session["link_order"] = session["list_order"] = session["info_links"] = session["web_links"] = session["general_links"] = session["social_links"] = session["ealert_colour"] = session["ealert_group_id"] = session["ealert_recipient_data"] = session["ealert_contact_checkbox"] = session["ealert_contact_name_checkbox"] = session["ealert_schedule_data"] = nil

    sql = Ealert.show_ealert("e.office_id = #{@office.id} ")
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    @ealerts = @mysql_result.all_hashes
    @mysql_result.free
  end

  def stats
    sql = Ealert.show_ealert("e.id = #{params[:id]} ")
    ealert_stats = Ealert.find_by_sql(sql.to_s)
    @ealert_stats = ealert_stats[0]
    @ecampaign_property_clicks = EcampaignClick.find(:all, :conditions => "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='ealert' And `property_id` IS NOT NULL")
    @ecampaign_link_clicks = EcampaignClick.find(:all, :conditions => "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='ealert' And `property_id` IS NULL")

    unless params[:link_id].blank?
      @ealert_click = EcampaignClick.find(params[:link_id])
      unless @ealert_click.property_id.blank?
        p = Property.find(@ealert_click.property_id)
        @destination = p.address
      else
        @destination = @ealert_click.destination_name
      end
      @ecampaign_detail_clicks = EcampaignClickDetail.find(:all, :select => "DISTINCT(`agent_contact_id`), ecampaign_click_id", :conditions=>"ecampaign_click_id = #{params[:link_id]}")
    end

    unless params[:type].blank?
      @stats_type = params[:type]
      conditions = "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='ealert'"
      conditions +=" And email_bounced=1" if params[:type] == "bounced"
      conditions +=" And email_opened=1" if params[:type] == "views"
      conditions +=" And email_forwarded=1" if params[:type] == "forward"
      if params[:type] == "unsubcribe"
        @ecampaign_tracks = EcampaignUnsubcribe.find(:all, :select => "DISTINCT(`agent_contact_id`), type_ecampaign_id", :conditions=> "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='ealert'")
      else
        @ecampaign_tracks = EcampaignTrack.find(:all, :select => "DISTINCT(`agent_contact_id`), type_ecampaign_id", :conditions=> conditions)
      end
    end
  end

  def edit_ealert
    require 'json'
    ec = Ealert.find_by_id(params[:id])
    if ec.stored_data.blank?
      flash[:notice] = 'Eproperty Alert data doesn\'t found.'
      redirect_to agent_office_ealerts_path(@agent, @office)
    else
      set_data(JSON.parse(ec.stored_data))
      session["ealert_id"] = params[:id]
      redirect_to choose_layout_agent_office_ealerts_path(@agent, @office)
    end
  end

  def view_and_send
    require 'json'
    ec = Ealert.find_by_id(params[:id])
    if ec.stored_data.blank?
      flash[:notice] = 'Eproperty Update data doesn\'t found.'
      redirect_to agent_office_ealerts_path(@agent, @office)
    else
      set_data(JSON.parse(ec.stored_data))
      session["ealert_id"] = params[:id]
      redirect_to view_sample_agent_office_ealerts_path(@agent, @office)
    end
  end

  def duplicate_ealert
    ActiveRecord::Base.connection.execute("INSERT INTO ealerts (`office_id`, `title`, `stored_layout`, `stored_data`, `created_at`, `updated_at`) SELECT `office_id`, `title`, `stored_layout`, `stored_data`, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP() FROM ealerts WHERE id = #{params[:id]}") unless params[:id].blank?
    flash[:notice] = 'Eproperty Update has successfully been duplicated.'
    redirect_to agent_office_ealerts_path(@agent, @office)
  end

  def preview
    ealert = Ealert.find_by_id(params[:id])
    if !ealert.blank?
      email = ealert.stored_layout.gsub("{ealert_track_property_url}", full_base_url+visit_property_link_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{ealert_track_url}", full_base_url+visit_link_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{forward}", full_base_url+forward_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{preview}", full_base_url+preview_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_ealert_path(ealert.office.agent,ealert.office,ealert)+"?track_id=#{params[:track_id]}")
      @html = email.gsub("<img src='/images/click_here.gif'/>", "<img  style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_ealerts_path(ealert.office.agent,ealert.office)+"?track_id=#{params[:track_id]}&ealert_id=#{ealert.id}"}'/>")
    else
      @html = "<center><b>Ebrochure Not Found!</b></center>"
    end
    render :layout => false
  end

  def unsubcribe
    track = EcampaignTrack.find_by_id(params[:track_id])
    unless track.blank?
      ac = track.agent_contact
      ac.inactive = true
      ac.save(false)
      ContactNote.create({:agent_contact_id => ac.id, :agent_user_id => ac.office.office_contact, :note_type_id => 22, :description => "User decided to unsubscribe through clicking option in ealert", :note_date => Time.now})
      EcampaignUnsubcribe.create({:type_ecampaign_id => params[:id], :type_ecampaign => 'ealert', :agent_contact_id => track.agent_contact_id})
    end
    @login_url = full_base_url
    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def visit_link
    #localhost:3000/agents/1/offices/1/ealerts/1/visit_link?track_id=1
    #track clicked link
    unless params[:link_url].blank?
      destination_link = params[:link_url].gsub("http://", "").gsub("www", "")
      ec  = EcampaignClick.find(:first, :conditions => "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='ealert' And `destination_link` LIKE '%#{destination_link}%'")
      unless ec.blank?
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_clicks SET clicked = clicked + 1 WHERE `id`=#{ec.id}")
        unless params[:track_id].blank?
          track = EcampaignTrack.find(params[:track_id])
          EcampaignClickDetail.create({:ecampaign_click_id=> ec.id, :agent_contact_id => track.agent_contact_id})
        end
      end
      redirect_to params[:link_url]
    else
      redirect_to agent_office_ealerts_path(@agent, @office)
    end
  end

  def visit_property_link
    #track property clicked link
    unless params[:property_id].blank?
      ec  = EcampaignClick.find(:first, :conditions => "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='ealert' And `property_id` = #{params[:property_id]}")
      unless ec.blank?
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_clicks SET clicked = clicked + 1 WHERE `id`=#{ec.id}")
        unless params[:track_id].blank?
          track = EcampaignTrack.find(params[:track_id])
          EcampaignClickDetail.create({:ecampaign_click_id=> ec.id, :agent_contact_id => track.agent_contact_id})
        end
      end
      property = Property.find(params[:property_id])
      if property.detail.property_url.blank?
        if @office.url.present?
          office_url = "http://#{@office.url.gsub("http://", "")}"
          redirect_to "#{office_url}/#{params[:property_id]}"
        else
          redirect_to full_base_url+"/agents/#{property.agent_id}/offices/#{property.office_id}/properties/#{property.id}"
        end
      else
        redirect_to "http://#{property.detail.property_url.gsub("http://", "")}"
      end
    else
      redirect_to agent_office_ealerts_path(@agent, @office)
    end
  end

  def delete_alert
    unless params[:id].blank?
     ec  = Ealert.find_by_id(params[:id])
     unless ec.blank?
       eclicks = EcampaignClick.find(:all, :conditions => "ecampaign_id = #{params[:id]}")
       unless eclicks.blank?
         eclicks.each{|eclick| ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_click_details WHERE ecampaign_click_id = #{eclick.id}")}
         ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_clicks WHERE type_ecampaign_id = #{ec.id} And type_ecampaign='ealert'")
       end
       ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_tracks WHERE type_ecampaign_id = #{ec.id} And type_ecampaign='ealert'")
       ec.destroy
     end
    end
    flash[:notice] = 'Email Already deleted.'
    redirect_to agent_office_ealerts_path(@agent, @office)
  end

  # ============================================
  #                    STEPS
  #=============================================
  def choose_layout
    if params[:layout]
      session["ealert_layout"] = params[:layout]
      save_update_ealert
      redirect_to campaign_copy_agent_office_ealerts_path(@agent, @office)
    end
  end

  def campaign_copy
    redirect_to choose_layout_agent_office_ealerts_path(@agent, @office) if session["ealert_layout"].blank?
    if !params[:update].blank?
      session["ealert_data"] = params[:data]
      session["link_order"] = params[:link_order]
      session["list_order"] = params[:list_order]
      session["info_links"] = params[:info_link]
      session["web_links"] = params[:web_link]
      session["general_links"] = params[:general_link]
      session["social_links"] = { "facebook_social_link" => params[:facebook_social_link], "twitter_social_link" => params[:twitter_social_link], "linkedin_social_link" => params[:linkedin_social_link], "youtube_social_link" => params[:youtube_social_link], "rss_social_link" => params[:rss_social_link]}
      save_update_ealert
      redirect_to choose_colour_agent_office_ealerts_path(@agent, @office)
    else
      session["ealert_data"] = { "sender_email" =>"", "sender_description" =>"", "sender_title" =>"","order" =>"", "status" =>"", "office_id" =>"",
      "current_sales" => "", "current_rentals" => "", "open_sales" => "", "open_rentals" => "", "sold" => "", "leased" => ""} if session["ealert_data"].blank?
      session["social_links"] = { "facebook_social_link" => "", "twitter_social_link" => "", "linkedin_social_link" => "", "youtube_social_link" => "", "rss_social_link" => ""} if session["social_links"].blank?
    end

    @data = session["ealert_data"]
    @contacts = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq

    @info_links = @web_links = @general_links = ""
    session["info_links"].each{|link| @info_links +='<div class="campaingn_link_wrapper"><div class="campaign-link-title">Title Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="info_link[][title]" class="single" style="width:200px;" value="'+link["title"]+'" /></div><div class="campaign-link-title">&nbsp;Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="info_link[][url]" class="single" style="width:200px;" value="'+link["url"]+'" /></div></div>'} unless session["info_links"].blank?
    session["web_links"].each{|link| @web_links +='<div class="campaingn_link_wrapper"><div class="campaign-link-title">Title Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="web_link[][title]" class="single" style="width:200px;" value="'+link["title"]+'" /></div><div class="campaign-link-title">&nbsp;Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="web_link[][url]" class="single" style="width:200px;" value="'+link["url"]+'" /></div></div>'} unless session["web_links"].blank?
    session["general_links"].each{|link| @general_links +='<div class="campaingn_link_wrapper marg"><div class="campaign-link-title">Headline:&nbsp;</div><div class="campaign-link-link"><input type="text" name="general_link[][title]" class="single" style="width:200px;" value="'+link["title"]+'" /></div><div class="campaign-link-title">&nbsp;Link:&nbsp;</div><div class="campaign-link-link"><input type="text" name="general_link[][url]" class="single" style="width:200px;" value="'+link["url"]+'" /></div><div style="clear:both;"></div></div><div class="campaingn_link_wrapper marg"><div class="campaign-link-title">Description:&nbsp;</div><div class="campaign-link-link"><textarea name="general_link[][desc]" style="width:495px;">'+link["desc"]+'</textarea></div><div style="clear:both;"></div></div>'} unless session["general_links"].blank?
    @facebook_social_link = session["social_links"]["facebook_social_link"]
    @twitter_social_link = session["social_links"]["twitter_social_link"]
    @linkedin_social_link = session["social_links"]["linkedin_social_link"]
    @youtube_social_link = session["social_links"]["youtube_social_link"]
    @rss_social_link = session["social_links"]["rss_social_link"]
    @link_order = session["link_order"]
    @list_order = session["list_order"]
  end

  def choose_colour
    @header_image = find_image("Brochure Header")
    @footer_image = find_image("Brochure Footer")
    @find_side_image = find_image("Side Image")
    @color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #999999 #CCCCCC #f0f0f0 #FFFFFF)
    unless params[:choose_colours].blank?
      session["ealert_colour"] = { "bgcolor" => params[:bgcolor], "textcolor" => params[:textcolor], "heading" =>params[:heading], "headingtextcolor" =>params[:headingtextcolor], "thinline" =>params[:thinline], "mainbg" =>params[:mainbg], "maintext" =>params[:maintext], "icon_colour" =>params[:icon_colour]}
      save_update_ealert
      redirect_to select_recipient_agent_office_ealerts_path(@agent, @office)
    else
      session["ealert_colour"] = { "bgcolor" =>"", "textcolor" =>"", "heading" =>"", "headingtextcolor" =>"", "thinline" =>"", "generaltext" =>"", "mainbg" =>"", "maintext" =>"", "icon_colour" => ""} if session["ealert_colour"].blank?
    end
    @bgcolor = session["ealert_colour"]["bgcolor"].blank? ? "#000000": session["ealert_colour"]["bgcolor"]
    @textcolor =  session["ealert_colour"]["textcolor"].blank? ? "#000000" : session["ealert_colour"]["textcolor"]
    @heading =  session["ealert_colour"]["heading"].blank? ? "#FFFFFF" : session["ealert_colour"]["heading"]
    @headingtextcolor =  session["ealert_colour"]["headingtextcolor"].blank? ? "#FFFFFF" : session["ealert_colour"]["headingtextcolor"]
    @thinline =  session["ealert_colour"]["thinline"].blank? ? "#000000" : session["ealert_colour"]["thinline"]
    @mainbg =  session["ealert_colour"]["mainbg"].blank? ? "#FFFFFF" : session["ealert_colour"]["mainbg"]
    @maintext =  session["ealert_colour"]["maintext"].blank? ? "#000000" : session["ealert_colour"]["maintext"]
    @icon_colour =  session["ealert_colour"]["icon_colour"].blank? ? "black" : session["ealert_colour"]["icon_colour"]
  end

  def select_recipient
    if @agent.developer.allow_ecampaign == true
      @group_contacts = GroupContact.find(:all)
    else
      @group_contacts = GroupContact.find(:all, :conditions => "office_id=#{@office.id}")
    end

    if !params[:update].blank?
      session["ealert_group_id"] = params[:group_ids].blank? ? "" : params[:group_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["ealert_contact_checkbox"] = params[:contact_checkbox]
      session["ealert_contact_name_checkbox"] = params[:contact_name_checkbox]
      session["ealert_recipient_data"] = params[:recipient_data]
      save_update_ealert
      redirect_to schedule_alert_agent_office_ealerts_path(@agent, @office)
    else
      session["ealert_recipient_data"] = {"search_contact_name" => "", "send_match_criteria" => ""} if session["ealert_recipient_data"].blank?
    end
    @recipient_data = session["ealert_recipient_data"]

    @value_group = session["ealert_group_id"].join("#") unless session["ealert_group_id"].blank?
    @value_groups = ""
    session["ealert_group_id"].each{|group|
      gc = GroupContact.find_by_id(group)
      @value_groups = @value_groups + '<span style="margin-right: 20px;" id="group'+group+'">'+gc.name+'<a href="javascript:remove_options(\'group\',\''+group+'\',\'group\')">x</a></span> '
    } unless session["ealert_group_id"].blank?

    @contact_checkbox = session["ealert_contact_checkbox"]
    @contact_checkbox_ids = ""
    @contact_checkbox.each{|cid|
      @contact_checkbox_ids = @contact_checkbox_ids + '#' + cid
    } unless @contact_checkbox.blank?

    @contact_name_checkbox = session["ealert_contact_name_checkbox"]
    @contact_name_checkbox_ids = ""
    @contact_name_checkbox.each{|cid|
      @contact_name_checkbox_ids = @contact_name_checkbox_ids + '#' + cid
    } unless @contact_name_checkbox.blank?

    @alert_type = [['Newly Listed Properties', 'Newly Listed Properties'], ['Updated Properties', 'Updated Properties'], ['Open Inspections', 'Open Inspections'], ['Sold/Leased Properties', 'Sold/Leased Properties'], ['Auctions', 'Auctions'], ['Latest News', 'Latest News']]
    @listing_type = [['Business Sale', 'BusinessSale'], ['Commercial Sale/Lease', 'Commercial'], ['Residential Lease', 'ResidentialLease'],['Residential Sale', 'ResidentialSale'], ['Holiday Lease', 'HolidayLease'], ['Project Sale', 'ProjectSale']]
    @range = (1..10).map{|x|[x,x]}
  end

  def schedule_alert
    if !params[:update].blank?
      session["ealert_schedule_data"] = params[:schedule_data]
      save_update_ealert
      redirect_to view_sample_agent_office_ealerts_path(@agent, @office)
    else
      session["ealert_schedule_data"] = {"frequency" => "", "start_date" => "", "time" => "", "alert_enable" => ""} if session["ealert_schedule_data"].blank?
    end
    @schedule_data = session["ealert_schedule_data"]
    @times = (0..23).map{|x| x.to_s.size < 2 ? ["0#{x}:00", "0#{x}:00"] : ["#{x}:00", "#{x}:00"] }
    @frequencies = [["Daily","daily"],["Weekly","weekly"],["Fortnightly","fortnightly"],["Monthly","monthly"]].unshift(["Please select", ''])
  end

  def view_sample
    redirect_to send_ealert_agent_office_ealerts_path(@agent, @office) if params[:send_ealert]
    ealert_data
  end

  def send_ealert
    ealert_data
    html = render_to_string(:layout => false)
    title = session["ealert_data"]["sender_title"]
    all_data = formated_data
    unless session["ealert_id"].blank?
      ealert = Ealert.find(session["ealert_id"])
      ealert.title = title
      ealert.stored_layout = html
      ealert.stored_data = all_data.to_json
      ealert.sent_at = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      ealert.save!
    else
      ealert = Ealert.create(:office_id => @office.id, :title => title, :stored_layout => html, :stored_data => all_data.to_json, :sent_at => Time.now.strftime("%Y-%m-%d %H:%M:%S"))
    end
    flash[:notice] = "Eproperty Alert has successfully been sent"
    redirect_to agent_office_ealerts_path(@agent, @office)
  end

  def sample
    @ealert = Ealert.find(params[:id])
    respond_to do |format|
      format.html { render :layout => false}
    end
  end

  def send_sample
    unless params[:id].blank?
      unless params[:email].blank?
        ealert_data
        html = render_to_string(:layout => false)
        @ealert = Ealert.find(params[:id])
        @ealert.stored_layout = html
        @ealert.save!

        sender = AgentUser.find_by_id(session["ealert_data"]["sender_email"])
        params[:email].each do |email|
        unless email.blank?
          begin
            EcampaignMailer.deliver_send_sample(sender, @ealert, email)
          rescue
            Log.create({:message => "Failed send sample email ealert #{@ealert.id}"})
          end
        end
        end
      end
      flash[:notice] = 'Sample Email already sent.'
    end
    redirect_to sample_agent_office_ealert_path(@agent,@office,@ealert)
  end

  def formated_data
    all_data = {"ealert_layout" => session["ealert_layout"],
    "ealert_data" => session["ealert_data"],
    "link_order" => session["link_order"],
    "list_order" => session["list_order"],
    "info_links" => session["info_links"],
    "web_links" => session["web_links"],
    "general_links" => session["general_links"],
    "social_links" => session["social_links"],
    "ealert_colour" => session["ealert_colour"],
    "ealert_group_id" => session["ealert_group_id"],
    "ealert_contact_checkbox" => session["ealert_contact_checkbox"],
    "ealert_contact_name_checkbox" => session["ealert_contact_name_checkbox"],
    "ealert_recipient_data" => session["ealert_recipient_data"],
    "ealert_schedule_data" => session["ealert_schedule_data"]
    }
    return all_data
  end

  def save_update_ealert
    all_data = formated_data
    unless session["ealert_id"].blank?
      ec = Ealert.find(session["ealert_id"])
      ec.title = session["ealert_data"]["sender_title"] unless session["ealert_data"].blank?
      unless session["ealert_schedule_data"].blank?
        ec.alert_enable = session["ealert_schedule_data"]["alert_enable"]
        ec.frequency = session["ealert_schedule_data"]["frequency"] unless session["ealert_schedule_data"]["frequency"].blank?
        ec.start_date = session["ealert_schedule_data"]["start_date"] unless session["ealert_schedule_data"]["start_date"].blank?
        ec.send_time = Time.parse(session["ealert_schedule_data"]["time"]+":00") if !session["ealert_schedule_data"]["time"].blank?
      end
      ec.stored_data = all_data.to_json
      ec.save!
    else
      ec = Ealert.create(:office_id => @office.id, :title => (session["ealert_data"].blank? ? "": session["ealert_data"]["sender_title"]), :stored_data => all_data.to_json)
      session["ealert_id"] = ec.id
    end
  end

  def forward
    #forward_agent_office_ealerts_path
    #localhost:3000/agents/1/offices/1/ealerts/1/forward?track_id=1
    @agent_contact = AgentContact.new
    @track_id = params[:track_id]
    @ealert = Ealert.find(params[:id])

    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def send_forward
    @agent_contact = AgentContact.new(params[:agent_contact])
    @track_id = params[:track_id]
    respond_to do |format|
      @agent_contact.ealert_contact = true
      if @agent_contact.save
        ealert_track = EcampaignTrack.find_by_id(params[:track_id])
        if ealert_track
          spawn(:kill => true) do
            ealert = ealert_track.ealert
            sender = AgentContact.find_by_id(ealert_track.agent_contact_id)
            begin
              EalertMailer.deliver_send_ealert(full_base_url, sender, ealert, ealert_track, @agent_contact)
            rescue Net::SMTPServerBusy
              ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ealert_track.id}") unless ealert_track.blank?
            end
          end
        end
        #track forwarded link
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_forwarded = 1 WHERE id=#{@track_id}") unless @track_id.blank?
        flash[:notice] = 'Eproperty Update has successfully been forwarded.'
        format.html { redirect_to :action => 'forward' }
      else
        format.html { render :action => "forward", :layout => "new_level4" }
      end
    end
  end

  def get_image
    #track opened email
    ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_opened =1 WHERE id=#{params[:track_id]}") unless params[:track_id].blank?
    image = RAILS_ROOT+"/public/images/click_here.gif"
    render :text => open(image, "rb").read
  end

  def ealert_data
    session["ealert_data"] = { "sender_email" =>"", "sender_description" =>"", "sender_title" =>"","order" =>"", "status" =>"", "featured_property_ids" =>"", "office_id" =>""} if session["ealert_data"].blank?
    session["social_links"] = { "facebook_social_link" => "", "twitter_social_link" => "", "linkedin_social_link" => "", "youtube_social_link" => "", "rss_social_link" => ""} if session["social_links"].blank?
    session["ealert_colour"] = { "bgcolor" =>"", "textcolor" =>"", "heading" =>"", "headingtextcolor" =>"", "thinline" =>"", "generaltext" =>"", "mainbg" =>"", "maintext" =>"", "icon_colour" => ""} if session["ealert_colour"].blank?

    @base_url = full_base_url
    @ealert_id = session["ealert_id"]
    @ealert = Ealert.find_by_id(@ealert_id)
    @side_image = find_image("Side Image")
    @header_image = find_image("Brochure Header")
    @footer_image = find_image("Brochure Footer")
    @info_links = session["info_links"]
    @web_links = session["web_links"]
    @general_links = session["general_links"]
    @data = session["ealert_data"]
    @img_nm = "p"
    @icon_colour = "#000000"
    unless session["ealert_colour"].blank?
      @bgcolor = session["ealert_colour"]["bgcolor"]
      @textcolor = session["ealert_colour"]["textcolor"]
      @heading = session["ealert_colour"]["heading"]
      @headingtextcolor = session["ealert_colour"]["headingtextcolor"]
      @thinline = session["ealert_colour"]["thinline"]
      @mainbg = session["ealert_colour"]["mainbg"].blank? ? "#FFFFFF" : session["ealert_colour"]["mainbg"]
      @maintext = session["ealert_colour"]["maintext"]
      @icon_colour = session["ealert_colour"]["icon_colour"] == "white" ? "#ffffff" : "#000000"
      @img_nm = "white_p" if session["ealert_colour"]["icon_colour"] == "white"
    end
    @ealert_status = session["ealert_status"]

    unless @data.blank?
      unless @data["sender_email"].blank?
        @sender = AgentUser.find_by_id(@data["sender_email"])
        unless @sender.blank?
          portraits = @sender.portrait_image
          unless portraits.blank?
            portraits.each do |img_portrait|
              if img_portrait.position == 1
                @img_portrait_1 = img_portrait
              else
                @img_portrait_2 = img_portrait
              end
            end
          end
        end
      end
    end

    current_sales_condition = "office_id = #{@office.id} And status = 1 And type IN('ResidentialSale','ProjectSale','BusinessSale','Commercial')"
    current_rental_conditon = "office_id = #{@office.id} And status = 1 And type IN('ResidentialLease','HolidayLease','Commercial')"
    opentime_sales_condition = "office_id = #{@office.id} And status = 1 And type IN('ResidentialSale','ProjectSale','BusinessSale','Commercial') And id IN (Select DISTINCT(property_id) from opentimes)"
    opentime_rentals_condition = "office_id = #{@office.id} And status = 1 And type IN('ResidentialLease','HolidayLease','Commercial') And id IN (Select DISTINCT(property_id) from opentimes)"
    sold_condition = "office_id = #{@office.id} And status = 2"
    lease_condition = "office_id = #{@office.id} And status = 3"

    ea = Ealert.find_by_id(session["ealert_id"])
    unless ea.blank?
      unless ea.sent_at.blank?
        current_sales_condition += " And updated_at > '#{ea.sent_at.strftime("%Y-%m-%d")}'" if @data["current_sales"] == "2"
        current_rental_conditon += " And updated_at > '#{ea.sent_at.strftime("%Y-%m-%d")}'" if @data["current_rentals"] == "2"
        opentime_sales_condition += " And updated_at > '#{ea.sent_at.strftime("%Y-%m-%d")}'" if @data["open_sales"] == "2"
        opentime_rentals_condition += " And updated_at > '#{ea.sent_at.strftime("%Y-%m-%d")}'" if @data["open_rentals"] == "2"
        sold_condition += " And updated_at > '#{ea.sent_at.strftime("%Y-%m-%d")}'" if @data["sold"] == "2"
        lease_condition += " And updated_at > '#{ea.sent_at.strftime("%Y-%m-%d")}'" if @data["leased"] == "2"
      end
    end

    @current_sales_ids = Property.find(:all, :conditions => current_sales_condition, :order => "id DESC", :limit => 5)
    @current_rentals_ids = Property.find(:all, :conditions => current_rental_conditon, :order => "id DESC", :limit => 5)
    @opentime_sales_ids = Property.find(:all, :conditions => opentime_sales_condition, :order => "id DESC", :limit => 5)
    @opentime_rentals_ids = Property.find(:all, :conditions => opentime_rentals_condition, :order => "id DESC", :limit => 5)
    @sold_ids = Property.find(:all, :conditions => sold_condition, :order => "id DESC", :limit => 5)
    @lease_ids = Property.find(:all, :conditions => lease_condition, :order => "id DESC", :limit => 5)

    @categories = []
    if session["list_order"].blank?
      @categories = [["lease", "Leased Listings"], ["opentime_sales", "Open Times Sales"], ["current_rentals", "Current Rentals"], ["current_sales", "Current Sales"], ["opentime_rentals", "Open Times Rentals"], ["sold", "Sold Listings"]]
    else
      session["list_order"].split("#").map do |order|
       @categories << ["lease", "Leased Listings"] if order == "leased"
       @categories << ["opentime_sales", "Open Times Sales"] if order == "opensales"
       @categories << ["current_rentals", "Current Rentals"] if order == "openrentals"
       @categories << ["current_sales", "Current Sales"] if order == "currentsales"
       @categories << ["opentime_rentals", "Open Times Rentals"] if order == "currentrentals"
       @categories << ["sold", "Sold Listings"] if order == "sold"
      end
    end

    @property_ids = {"current_sales" => @current_sales_ids, "current_rentals" => @current_rentals_ids, "opentime_sales" => @opentime_sales_ids, "opentime_rentals" => @opentime_rentals_ids, "sold" => @sold_ids, "lease" => @lease_ids}
    @socials = session["social_links"]
    @link_order = session["link_order"]
    @list_order = session["list_order"]
  end

  def set_data(db_data)
    session["ealert_layout"] = db_data["ealert_layout"]
    session["ealert_data"] = db_data["ealert_data"]
    session["link_order"] = db_data["link_order"]
    session["list_order"] = db_data["list_order"]
    session["info_links"] = db_data["info_links"]
    session["web_links"] = db_data["web_links"]
    session["general_links"] = db_data["general_links"]
    session["social_links"] = db_data["social_links"]
    session["ealert_colour"] = db_data["ealert_colour"]
    session["ealert_group_id"] = db_data["ealert_group_id"]
    session["ealert_contact_checkbox"] = db_data["ealert_contact_checkbox"]
    session["ealert_contact_name_checkbox"] = db_data["ealert_contact_name_checkbox"]
    session["ealert_recipient_data"] = db_data["ealert_recipient_data"]
    session["ealert_status"] = "edit"
  end

  def search
    if params[:status] == "sold"
      condition = "`status` IN (2,3)"
    else
      condition = "`status` IN (1,5)"
    end

    text = ''
    unless params[:search_value].blank?
      if @agent.developer.allow_listing_office == true
        condition = "((`suburb` LIKE '%#{params[:search_value]}%' OR `street` LIKE '%#{params[:search_value]}%') OR `id` = '#{params[:search_value]}') AND `status` IN (1,5) And `office_id`=#{@office.id}"
      else
        condition = "((`suburb` LIKE '%#{params[:search_value]}%' OR `street` LIKE '%#{params[:search_value]}%') OR `id` = '#{params[:search_value]}') AND `status` IN (1,5)"
      end
    else
      unless params[:selection_office_id].blank?
        tmp_office_id = params[:selection_office_id].split("#").delete_if{|x| x.blank?}
        office_id = tmp_office_id.join(",")
        condition += " And `office_id` IN (#{office_id})"
      else
        condition += " And `office_id` = #{@office.id}" if @agent.developer.allow_listing_office == true
      end

      unless params[:selection_property_type].blank?
        tmp_property_type = params[:selection_property_type].split("#").delete_if{|x| x.blank?}
        condition += " And `property_type` IN ('#{tmp_property_type.join("','")}')"
      end

      unless params[:selection_listing_type].blank?
        tmp_listing_type = params[:selection_listing_type].split("#").delete_if{|x| x.blank?}
        ori_listings = tmp_listing_type.join("','")
        deal_types = ""
        if ori_listings.include?("CommercialSale")
          ori_listings = ori_listings.gsub("CommercialSale", "Commercial")
          deal_types += "'Sale'"
        end
        if ori_listings.include?("CommercialLease")
          ori_listings = ori_listings.gsub("CommercialLease", "Commercial")
          if deal_types.blank?
            deal_types += "'Lease'"
          else
            deal_types += ",'Lease', 'Both'"
          end
        end
        condition += " And `type` IN ('#{ori_listings}')"
        condition += " And (`deal_type` IN (#{deal_types}) OR `deal_type` IS NULL)" unless deal_types.blank?
      end
    end

    properties = Property.find(:all, :conditions => condition)
    jsons = []
    unless properties.blank?
      properties.each{|p|
        unless p.blank?
        ad = [ p.unit_number, p.street_number, p.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
        address = [ad, p.suburb ].join(", ")
        jsons << {'id' => p.id, 'address' => "#{address unless p.address.blank?}",'suburb' =>"#{p.suburb}",'type' =>"#{p.type}",'price' =>"$#{p.price.to_i}",'status' =>"#{p.status_name}", 'href' => agent_office_properties_path(@agent, @office)+"?ID=#{p.id}"}
        end
      } unless properties.nil?
      text = {"results" => jsons, "total" => (properties ? properties.length : 0) }
    end

    render :json => text
  end

  def search_contact
    unless params[:selection_group_id].blank?
      group_ids = params[:selection_group_id].split("#").delete_if{|x| x.blank?}
      gc = GroupContactRelation.find(:all, :select => "DISTINCT(agent_contact_id)", :conditions => {:group_contact_id => group_ids})
    end

    jsons = []
    if !gc.blank?
      gc.each do |ac|
        unless ac.agent_contact.blank?
          jsons << {'id' => ac.agent_contact.id, 'name' => "#{ac.agent_contact.full_name}"} if ac.agent_contact.inactive != true
        end
      end
      text = {"results" => jsons, "total" => (gc ? gc.length : 0) }
    else
      text = ''
    end
    render :json => text
  end

  def find_image(type)
    get_header_footer(@office.id, type)
  end

  def ealert_note_for_property(contact_id, property, sender_id)
    ad = [ property.unit_number, property.street_number, property.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
    address = [ad, property.suburb ].join(", ")
    note = {:skip_enquiry => true, :property_id => property.id, :agent_contact_id => contact_id, :agent_user_id => sender_id,
            :note_type_id => 24,:address => address, :description => "Property Alert sent via ealert", :note_date => Time.now, :duplicate => 0}
    property_note = PropertyNote.new(note)
    property_note.save!

    ContactNote.create({:agent_contact_id => contact_id, :agent_user_id => sender_id, :note_type_id => 1, :description => "Property Alert sent via ealert", :note_date => Time.now, :address => address, :property_id => property.id})
  end
end