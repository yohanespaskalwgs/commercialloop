class ParentListingsController < ApplicationController
  layout "agent"
  before_filter :get_agent_and_office, :get_property

  def index
    get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def populate_combobox
    floor,suburb,type,assigned_to,condition = [],[],[],[],[]
    condition = [" properties.office_id = ? "]
    condition << current_office.id
    properties = Property.find(:all,:include =>[:primary_contact], :conditions => ["properties.id = ?", "#{@property.parent_listing_id}"],:select => "properties.id, properties.primary_contact_id, DISTINCT(properties.suburb),DISTINCT(properties.property_type),users.first_name,users.last_name,properties.detail.number_of_floors")
    properties.map{|x|
      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText =>x.suburb.to_s
      } unless x.suburb.blank?;
      type << {
        :type => x.property_type.to_s,
        :typeText => x.property_type.to_s
      } unless x.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?) ,
        :assigned_toText => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?)
      } unless x.primary_contact.blank?;
      floor << {
        :floor => x.detail.number_of_floors.to_s,
        :floorText => x.detail.number_of_floors.to_s
      } unless x.detail.number_of_floors.blank?
    }
    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    uniq_floor =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    floor.each{|x|uniq_floor << x if !uniq_floor.include?(x) and !x.blank?}

    uniq_floor.to_a.sort!{|x,y| x[:floorText].to_s <=> y[:floorText].to_s}
    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    return(render(:json =>{:results => properties.length,:suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,:floors=>[uniq_floor.unshift({:floor=>'all',:floorText=>"All Floors"})].flatten,:types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,:team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        if current_user.limited_client_access?
          if !@office.control_level3
            condition[0] += " AND properties.primary_contact_id = ? "
            condition << current_user.id
          end
        elsif current_user.allow_property_owner_access?
          condition[0] += " AND properties.agent_user_id = ?"
          condition << current_user.id
        end

        if(!session[:type].blank? && Property::TYPES.map{|s| s[1]}.include?(session[:type]))
          condition[0] += " AND properties.type = ?"
          condition <<  session[:type]
        end


        if ((current_user.allow_property_owner_access? && session[:status] == 1) || (current_user.allow_property_owner_access?  && session[:status].blank?))|| current_user.allow_property_owner_access?
          session[:status] = "all"
        end

        unless session[:status] == "all"
          unless session[:status].blank?
            condition[0] += " AND properties.status = ?"
            condition <<  session[:status]
          end
        end

        unless session[:suburb] == "all" || session[:suburb].blank?
          condition[0] += " AND properties.suburb = ?"
          condition <<  session[:suburb]
        end

        unless session[:floor] == "all" || session[:floor].blank?
          condition[0] += "AND properties.detail = ?"
          condition << session[:floor]
        end

        unless session[:primary_contact_fullname] == "all" || session[:primary_contact_fullname].blank?
          name =  session[:primary_contact_fullname].split(' ')
          condition[0] += " AND users.first_name  = ?"
          condition << name[0]
          unless name[1].blank?
            condition[0] += " AND users.last_name  = ?"
            condition << name[1]
          end
        end

        unless session[:property_type] == "all" || session[:property_type].blank?
          condition[0] += " AND properties.property_type = ?"
          condition << session[:property_type]
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      if current_user.limited_client_access?
        if !@office.control_level3
          condition[0] += " AND properties.primary_contact_id = ? "
          condition << current_user.id
        end
      elsif current_user.allow_property_owner_access?
        condition[0] += " AND properties.agent_user_id = ?"
        condition << current_user.id
      end
    end
    condition[0] += " AND properties.deleted_at is NULL"

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = Property.build_sorting(params[:sort],params[:dir])
    @properties = Property.paginate(:all, :include =>[:primary_contact], :conditions => ["properties.id = ?", "#{@property.parent_listing_id}"], :order => order,:select => "properties.id,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.detail.number_of_floors,properties.deleted_at,users.first_name,users.last_name", :page =>params[:page], :per_page => params[:limit].to_i)
    if @properties.present?
      @properties.each do |property|
        check_and_update_icon(property)
      end
    end
    results = 0
    if params[:limit].to_i == 20
      results = 20 #Property.find(:all,:select => 'count(properties.id), properties.primary_contact_id',:include =>[:primary_contact],:conditions => condition,:limit => params[:limit].to_i)
    else
      results = Property.count(:include =>[:primary_contact], :conditions => ["properties.id = ?", "#{@property.parent_listing_id}"])
    end
    return(render(:json =>{:results => results,:rows=> @properties.map{|x|{'id' => x.id,'listing'=>Property.abbrv_properties(x.type),'address' =>((x.street_number.blank? ? "" : x.street_number)+" "+(x.street.blank? ? "" : x.street.to_s)),'suite'=>((x.street_number.blank? ? "" : x.street_number)+(x.unit_number.blank? ? "" : "/")+(x.unit_number.blank? ? "" : x.unit_number)),'suburb' => x.suburb,'type' => x.property_type,'floor' => x.detail.number_of_floors.to_s,'price' => Property.check_price(x),'assigned_to'=>x.primary_contact.blank? ? '' : x.primary_contact.full_name.to_s.strip,'actions'=>Property.check_color(x),'status' => Property.status_name(x),'fb'=> [x.id,@office.url.blank? ? (x.detail.respond_to?(:deal_type) ? x.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}", tweet(x)]} unless x.blank? }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:floor] = params[:floor].blank? ? "all" : params[:floor]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
  end

  def nil_session_listing
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:floor] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
  end

  def check_and_update_icon(parent_listing)
    img_count = parent_listing.images.count rescue 0
    floorp_count = parent_listing.floorplans.count rescue 0
    if !parent_listing.image_exist && img_count > 0
      parent_listing.image_exist = true
      parent_listing.save(false)
    end
    if parent_listing.image_exist && img_count == 0
      parent_listing.image_exist = false
      parent_listing.save(false)
    end
    if !parent_listing.floorplan_exist && floorp_count > 0
      parent_listing.floorplan_exist = true
      parent_listing.save(false)
    end
    if parent_listing.floorplan_exist && floorp_count == 0
      parent_listing.floorplan_exist = false
      parent_listing.save(false)
    end
  end

  def tweet(parent_listing)
    unless parent_listing.blank?
      case parent_listing.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{parent_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{parent_listing.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{parent_listing.address.to_s.split(",").join(", ").to_s}" + (parent_listing.detail.blank? ? "" : ", #{parent_listing.detail.bedrooms.blank? ? "0" : parent_listing.detail.bedrooms} Bed, #{parent_listing.detail.bathrooms.blank? ? "0" : parent_listing.detail.bathrooms} Bath, #{parent_listing.detail.carport_spaces.to_i + parent_listing.detail.garage_spaces.to_i + parent_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{parent_listing.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{parent_listing.address.to_s.split(",").join(", ").to_s}" + (parent_listing.detail.blank? ? "" : ", #{parent_listing.detail.bedrooms.blank? ? "0" : parent_listing.detail.bedrooms} Bed, #{parent_listing.detail.bathrooms.blank? ? "0" : parent_listing.detail.bathrooms} Bath, #{parent_listing.detail.carport_spaces.to_i + parent_listing.detail.garage_spaces.to_i + parent_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{parent_listing.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{parent_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{parent_listing.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{parent_listing.address.to_s.split(",").join(", ").to_s}" + (parent_listing.detail.blank? ? "" : ", #{parent_listing.detail.bedrooms.blank? ? "0" : parent_listing.detail.bedrooms} Bed, #{parent_listing.detail.bathrooms.blank? ? "0" : parent_listing.detail.bathrooms} Bath, #{parent_listing.detail.carport_spaces.to_i + parent_listing.detail.garage_spaces.to_i + parent_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{parent_listing.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{parent_listing.address.to_s.split(",").join(", ").to_s}" + (parent_listing.detail.blank? ? "" : ", #{parent_listing.detail.bedrooms.blank? ? "0" : parent_listing.detail.bedrooms} Bed, #{parent_listing.detail.bathrooms.blank? ? "0" : parent_listing.detail.bathrooms} Bath, #{parent_listing.detail.carport_spaces.to_i + parent_listing.detail.garage_spaces.to_i + parent_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{parent_listing.id}")}"
      end
    end
    return tweet
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end
end
