class EmarketingsController < ApplicationController
  before_filter :login_required, :except => [:forward, :send_forward, :send_emarketing, :get_image, :visit_link, :visit_property_link, :preview, :unsubcribe]
  before_filter :get_agent_and_office
  skip_before_filter :verify_authenticity_token, :except => [:index, :destroy, :sample, :forward, :send_forward, :send_emarketing, :save_emarketing]
  layout "agent"
  include EcampaignHelper

  def index
    session["emarketing_id"] = session["emarketing_layout"] = session["emarketing_data"] = session["general_links"] = session["social_links"] = session["emarketing_colour"] = session["emarketing_group_id"] = session["emarketing_recipient_data"] = session["emarketing_contact_checkbox"] = session["emarketing_contact_name_checkbox"] = session["news_links"] = session["sidebar_links"] = session["bottom_left"] = session["bottom_right"] = session["user_links"] = session["news_urls"] =nil
    sql = Emarketing.show_emarketing("e.office_id = #{@office.id} ")
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    @emarketings = @mysql_result.all_hashes
    @mysql_result.free

  end

  def stats
    sql = Emarketing.show_emarketing("e.id = #{params[:id]} ")
    emarketing_stats = Emarketing.find_by_sql(sql.to_s)
    @emarketing_stats = emarketing_stats[0]
    @ecampaign_link_clicks = EcampaignClick.find(:all, :conditions => "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='emarketing' And `property_id` IS NULL")

    unless params[:link_id].blank?
      @emarketing_click = EcampaignClick.find(params[:link_id])
      @destination = @emarketing_click.destination_name
      @ecampaign_detail_clicks = EcampaignClickDetail.find(:all, :select => "DISTINCT(`agent_contact_id`), ecampaign_click_id", :conditions=>"ecampaign_click_id = #{params[:link_id]}")
    end

    unless params[:type].blank?
      @stats_type = params[:type]
      conditions = "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='emarketing'"
      conditions +=" And email_bounced=1" if params[:type] == "bounced"
      conditions +=" And email_opened=1" if params[:type] == "views"
      conditions +=" And email_forwarded=1" if params[:type] == "forward"
      if params[:type] == "unsubcribe"
        @ecampaign_tracks = EcampaignUnsubcribe.find(:all, :select => "DISTINCT(`agent_contact_id`), type_ecampaign_id", :conditions=> "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='emarketing'")
      else
        @ecampaign_tracks = EcampaignTrack.find(:all, :select => "DISTINCT(`agent_contact_id`), type_ecampaign_id", :conditions=> conditions)
      end
    end
  end

  def edit_emarketing
    require 'json'
    ec = Emarketing.find_by_id(params[:id])
    if ec.blank?
      flash[:notice] = 'Emarketing data doesn\'t found.'
      redirect_to agent_office_emarketings_path(@agent, @office)
    else
      if ec.stored_data.blank?
        flash[:notice] = 'Emarketing data doesn\'t found.'
        redirect_to agent_office_emarketings_path(@agent, @office)
      else
        set_data(JSON.parse(ec.stored_data))
        session["emarketing_id"] = params[:id]
        redirect_to choose_layout_agent_office_emarketings_path(@agent, @office)
      end
    end
  end

  def view_and_send
    require 'json'
    ec = Emarketing.find_by_id(params[:id])
    
    if ec.stored_data.blank?
      flash[:notice] = 'Emarketing data doesn\'t found.'
      redirect_to agent_office_emarketings_path(@agent, @office)
    else
      set_data(JSON.parse(ec.stored_data))
      session["emarketing_id"] = params[:id]
      redirect_to view_sample_agent_office_emarketings_path(@agent, @office)
    end
  end

  def duplicate_emarketing
    require 'json'
    ec = Emarketing.find_by_id(params[:id])
    if ec.blank?
      flash[:notice] = 'Emarketing data doesn\'t found.'
    else
      set_data(JSON.parse(ec.stored_data))
      emarketing_new = save_update_emarketing
      
      unless emarketing_new.blank?
        begin
          require 'open-uri'
           # ------------Copying images-----------------------
           unless session["news_links"].blank?
             tmp_news = session["news_links"]
             session["news_links"] = []
             tmp_news.each do |news_link|
               img = Attachment.find_by_id(news_link["image_id"])
               img2 = Attachment.find_by_id(news_link["image_id2"])
               image_new = emarketing_new.copy_image(img, "news_image") unless img.blank?
               image_new2 = emarketing_new.copy_image(img2, "news_image2") unless img.blank?
               session["news_links"] << { "id" => news_link["id"], "image_id" => (image_new.blank? ? "" : image_new.id), "image_id2" => (image_new2.blank? ? "" : image_new2.id) , "url" => ( news_link["url"].gsub(/\s+/, "") unless news_link["url"].blank?), "headline" => news_link["headline"], "description" => news_link["description"], "date" => news_link["date"]}
             end
           end

           unless session["general_links"].blank?
             tmp_general = session["general_links"]
             session["general_links"] = []
             tmp_general.each do |general_link|
               img = Attachment.find_by_id(general_link["id"])
               image_new = emarketing_new.copy_image(img, "") unless img.blank?
               session["general_links"] << { "id" => (image_new.blank? ? general_link["id"] : image_new.id), "url" => (general_link["url"].gsub(/\s+/, "") unless general_link["url"].blank?) }
             end
           end

           unless session["sidebar_links"].blank?
             tmp_sidebar = session["sidebar_links"]
             session["sidebar_links"] = []
             tmp_sidebar.each do |sidebar_link|
               img = Attachment.find_by_id(sidebar_link["image_id"])
               image_new = emarketing_new.copy_image(img, "sidebar_image") unless img.blank?
               session["sidebar_links"] << { "id" => sidebar_link["id"], "image_id" => (image_new.blank? ? "" : image_new.id), "url" => (sidebar_link["url"].gsub(/\s+/, "") unless sidebar_link["url"].blank?), "headline" => sidebar_link["headline"], "field_name1" => sidebar_link["field_name1"], "field_value1" => sidebar_link["field_value1"], "field_name2" => sidebar_link["field_name2"], "field_value2" => sidebar_link["field_value2"]}
             end
           end
           
           unless session["bottom_left"].blank?
             tmp_news = session["bottom_left"]
             session["bottom_left"] = []
             tmp_news.each do |news_link|
               img = Attachment.find_by_id(news_link["image_id"])
               image_new = emarketing_new.copy_image(img, "news_image") unless img.blank?
               session["bottom_left"] << { "id" => news_link["id"], "image_id" => (image_new.blank? ? "" : image_new.id), "url" => ( news_link["url"].gsub(/\s+/, "") unless news_link["url"].blank?), "headline" => news_link["headline"], "description" => news_link["description"]}
             end
           end
           
           unless session["bottom_right"].blank?
             tmp_news = session["bottom_right"]
             session["bottom_right"] = []
             tmp_news.each do |news_link|
               img = Attachment.find_by_id(news_link["image_id"])
               image_new = emarketing_new.copy_image(img, "news_image") unless img.blank?
               session["bottom_right"] << { "id" => news_link["id"], "image_id" => (image_new.blank? ? "" : image_new.id), "url" => ( news_link["url"].gsub(/\s+/, "") unless news_link["url"].blank?), "headline" => news_link["headline"], "description" => news_link["description"]}
             end
           end
           
           if session["emarketing_layout"] == "theme1" || session["emarketing_layout"] == "theme5"
             img = ec.emarketing_images.find_by_category("main_image")
             if img.present?
               image_new = emarketing_new.copy_image(img, "main_image")
             end               
           end
          
           # ------------End Copying images-----------------------
         rescue
           Log.create({:message => "Error when duplicate emarketing images id #{emarketing_new.id}"})
         end
         save_update_emarketing
      end
      flash[:notice] = 'Emarketing has successfully been duplicated.'
    end
    redirect_to agent_office_emarketings_path(@agent, @office)
  end

  def preview
    emarketing = Emarketing.find_by_id(params[:id])
    track = EcampaignTrack.find(params[:track_id]) if params[:track_id].present?
    contact = AgentContact.find_by_id(track.agent_contact_id) if track.present?
    if !emarketing.blank?
      email = emarketing.stored_layout.gsub("{emarketing_track_property_url}", full_base_url+visit_property_link_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{ecampaign_track_property_url}", full_base_url+visit_property_link_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{emarketing_track_url}", full_base_url+visit_link_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{ecampaign_track_url}", full_base_url+visit_link_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{forward}", full_base_url+forward_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{preview}", full_base_url+preview_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{params[:track_id]}")
      email = email.gsub("{unsubcribe}", full_base_url+unsubcribe_agent_office_emarketing_path(emarketing.office.agent,emarketing.office,emarketing)+"?track_id=#{params[:track_id]}")
      if contact.present?
        if contact.first_name.present?
          email = email.gsub("{recipient_name}", " #{contact.first_name}")
        else
          email = email.gsub("{recipient_name}", "")
        end
        email = email.gsub("{recipient_mail_address}", contact.email)
      end
      @html = email.gsub("<img src='/images/click_here.gif'/>", "<img  style='color: transparent ! important;' src='#{full_base_url+get_image_agent_office_emarketings_path(emarketing.office.agent,emarketing.office)+"?track_id=#{params[:track_id]}&emarketing_id=#{emarketing.id}"}'/>")
    else
      @html = "<center><b>Emarketing Not Found!</b></center>"
    end
    render :layout => false
  end

  def unsubcribe
    track = EcampaignTrack.find_by_id(params[:track_id])
    unless track.blank?
      ac = AgentContact.find_by_id(track.agent_contact_id)
      
#     Unsubscribe by email address
      contacts = AgentContact.find(:all, :conditions => "email like '#{ac.email}' and office_id = #{ac.office.id}") if ac.present? && ac.email.present? && ac.office.present?
      if contacts.present?
        contacts.each do |contact|
          contact.inactive = true
          contact.save(false)
          unsubscribe = EcampaignUnsubcribe.find(:first, :conditions => "type_ecampaign_id = #{params[:id]} and type_ecampaign LIKE 'emarketing' and agent_contact_id = #{contact.id}")
          if unsubscribe.blank?
            if contact.office.present?
              ContactNote.create({:agent_contact_id => contact.id, :agent_user_id => contact.office.office_contact, :note_type_id => 22, :description => "User with this email decided to unsubscribe through clicking option in emarketing", :note_date => Time.now})
            end
            EcampaignUnsubcribe.create({:type_ecampaign_id => params[:id], :type_ecampaign => 'emarketing', :agent_contact_id => contact.id})
          end
        end
      end
      
#      Unsubscribe by contact id only (disabled) --------------------------------
#      unless ac.blank?
#        ac.inactive = true
#        ac.save(false)
#        unsubscribe = EcampaignUnsubcribe.find(:first, :conditions => "type_ecampaign_id = #{params[:id]} and type_ecampaign LIKE 'emarketing' and agent_contact_id = #{track.agent_contact_id}")
#        if unsubscribe.blank?
#          ContactNote.create({:agent_contact_id => ac.id, :agent_user_id => ac.office.office_contact, :note_type_id => 22, :description => "User with this email decided to unsubscribe through clicking option in emarketing", :note_date => Time.now})
#          EcampaignUnsubcribe.create({:type_ecampaign_id => params[:id], :type_ecampaign => 'emarketing', :agent_contact_id => track.agent_contact_id})
#        end
#      end
      
    end
    @login_url = full_base_url
    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def visit_link
    #localhost:3000/agents/1/offices/1/emarketings/1/visit_link?track_id=1
    #track clicked link
    unless params[:link_url].blank?
      destination_link = params[:link_url].gsub("http://", "").gsub(/\s+/, "")
      ec  = EcampaignClick.find(:first, :conditions => "`type_ecampaign_id` = #{params[:id]} And type_ecampaign='emarketing' And `destination_link` LIKE '%#{destination_link}%'")
      unless ec.blank?
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_clicks SET clicked = clicked + 1 WHERE `id`=#{ec.id}")
        unless params[:track_id].blank?
          track = EcampaignTrack.find(params[:track_id])
          EcampaignClickDetail.create({:ecampaign_click_id=> ec.id, :agent_contact_id => track.agent_contact_id})
        end
      end
      redirect_to params[:link_url].gsub(/\s+/, "")
    else
      redirect_to agent_office_emarketings_path(@agent, @office)
    end
  end

  def delete_emarketing
    unless params[:id].blank?
     ec  = Emarketing.find_by_id(params[:id])
     unless ec.blank?
       eclicks = EcampaignClick.find(:all, :conditions => "ecampaign_id = #{params[:id]}")
       unless eclicks.blank?
         eclicks.each{|eclick| ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_click_details WHERE ecampaign_click_id = #{eclick.id}")}
         ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_clicks WHERE type_ecampaign_id = #{ec.id} And type_ecampaign='emarketing'")
       end
       ActiveRecord::Base.connection.execute("DELETE FROM ecampaign_tracks WHERE type_ecampaign_id = #{ec.id} And type_ecampaign='emarketing'")
       ec.emarketing_images.destroy_all unless ec.emarketing_images.blank?
       ec.destroy
     end
    end
    flash[:notice] = 'Emarketing Already deleted.'
    redirect_to agent_office_emarketings_path(@agent, @office)
  end

  def delete_image
   if params[:type] == "news_link"
     unless params[:id].blank?
       unless session["news_links"].blank?
         tmp_news = session["news_links"]
         session["news_links"] = []
         tmp_news.each{|news_link| session["news_links"] << { "id" => news_link["id"], "image_id" => news_link["image_id"], "url" => news_link["url"], "headline" => news_link["headline"], "description" => news_link["description"], "date" => news_link["date"]} if params[:id] != news_link["id"]}
         unless params[:image_id].blank?
           img = Attachment.find_by_id(params[:image_id])
           img.destroy unless img.blank?
         end
       end
     end
   end

   if params[:type] == "general_link"
    unless params[:image_id].blank?
     unless session["general_links"].blank?
       tmp_general = session["general_links"]
       session["general_links"] = []
       tmp_general.each{|general_link| session["general_links"] << { "id" => general_link["id"], "url" => general_link["url"]} if params[:image_id] != general_link["id"] }
       img = Attachment.find_by_id(params[:image_id])
       img.destroy unless img.blank?
     end
    end
   end

   if params[:type] == "sidebar_link"
     unless params[:id].blank?
       unless session["sidebar_links"].blank?
         tmp_sidebar = session["sidebar_links"]
         session["sidebar_links"] = []
         tmp_sidebar.each{|sidebar_link| session["sidebar_links"] << { "id" => sidebar_link["id"], "image_id" => sidebar_link["image_id"], "url" => sidebar_link["url"], "headline" => sidebar_link["headline"], "field_name1" => sidebar_link["field_name1"], "field_value1" => sidebar_link["field_value1"], "field_name2" => sidebar_link["field_name2"], "field_value2" => sidebar_link["field_value2"]} if params[:id] != sidebar_link["id"]}
         unless params[:image_id].blank?
           img = Attachment.find_by_id(params[:image_id])
           img.destroy unless img.blank?
         end
       end
     end
   end
#   redirect_to campaign_copy_agent_office_emarketings_path(@agent, @office)
   return render(:json =>true)
  end

  # ============================================
  #                    STEPS
  #=============================================
  def choose_layout
    if params[:layout]
      session["emarketing_layout"] = params[:layout]
      save_update_emarketing
      redirect_to campaign_copy_agent_office_emarketings_path(@agent, @office)
    end
    @emarketing_layout=session["emarketing_layout"]
  end

  def campaign_copy
    redirect_to choose_layout_agent_office_emarketings_path(@agent, @office) and return if session["emarketing_layout"].blank?
    @emarketing_layout =session["emarketing_layout"]
    if !params[:update].blank?
      session["emarketing_data"] = params[:data]
      session["social_links"] = { "facebook_social_link" => params[:facebook_social_link], "twitter_social_link" => params[:twitter_social_link]}
      session["social_share"] = params[:social_share]
      session["larger_photos"] = params[:larger_side_photos]
      session["no_dear"] = params[:no_dear]
      
      session["general_links"] = []
      session["news_links"] = []
      session["sidebar_links"] = []
      session["bottom_left"] = []
      session["bottom_right"] = []
      
      emarketing = Emarketing.find(session["emarketing_id"]) unless session["emarketing_id"].blank?
      if @emarketing_layout == "theme1" || @emarketing_layout == "theme5"
        unless params[:emarketing_image].blank?
          # Theme1 Main image
          unless params[:emarketing_image][:uploaded_data].blank?
            old_emarketing = emarketing.emarketing_images.find_by_category("main_image")
            emarketing_image = EmarketingImage.new(params[:emarketing_image])
            emarketing_image.category = "main_image"
            emarketing_image.attachable = emarketing

            if emarketing_image.save
              old_emarketing.destroy unless old_emarketing.blank?
            end
          end
        end
      end

      if @emarketing_layout == "theme2"
        unless params[:general_link].blank?
          # Theme2 Logo
          params[:general_link].each do  |general_link|
            unless general_link[:uploaded_data].blank?
              g_emarketing_image = EmarketingImage.new(general_link)
              g_emarketing_image.category = "logo_image"
              g_emarketing_image.attachable = emarketing
              g_emarketing_image.save

              unless general_link[:id].blank?
                old_emarketing = emarketing.emarketing_images.find_by_id(general_link[:id])
                old_emarketing.destroy unless old_emarketing.blank?
              end
            end
            id = g_emarketing_image.blank? ? general_link[:id] : g_emarketing_image.id
            session["general_links"] << { "id" => id, "url" => general_link[:url]} unless id.blank?
          end
        end
      end

      if @emarketing_layout == "theme3" || @emarketing_layout == "theme5"
        unless params[:news_link].blank?
          params[:news_link].each do  |news_link|
            unless news_link[:uploaded_data].blank?
              n_emarketing_image = EmarketingImage.new(news_link)
              n_emarketing_image.category = "news_image"
              n_emarketing_image.attachable = emarketing
              n_emarketing_image.save

              unless news_link[:id].blank?
                old_emarketing = emarketing.emarketing_images.find_by_id(news_link[:id])
                old_emarketing.destroy unless old_emarketing.blank?
              end
            end
             
            if news_link[:custom_button].present? && news_link[:custom_button][:uploaded_data].present?
              n_emarketing_image2 = EmarketingImage.new(news_link[:custom_button])
              n_emarketing_image2.category = "news_image2"
              n_emarketing_image2.attachable = emarketing
              n_emarketing_image2.save
            end

            id = news_link[:id].blank? ? "#{rand(30)}#{Time.now.strftime("%m%d%Y%H%I%S")}" : news_link[:id]
            image_id = n_emarketing_image.blank? ? news_link[:image_id] : n_emarketing_image.id
            image_id2 = n_emarketing_image2.blank? ? news_link[:image_id2] : n_emarketing_image2.id
            session["news_links"] << { "id" => id, "image_id" => image_id, "image_id2" => image_id2, "url" => news_link[:url], "headline" => news_link[:headline], "description" => news_link[:description], "date" => news_link[:date]} if news_link[:headline].present? || news_link[:description].present?
          end
        end
        
        unless params[:sidebar_link].blank?
          params[:sidebar_link].each do  |sidebar_link|
            unless sidebar_link[:uploaded_data].blank?
              s_emarketing_image = EmarketingImage.new(sidebar_link)
              s_emarketing_image.category = "sidebar_image"
              s_emarketing_image.attachable = emarketing
              s_emarketing_image.save

              unless sidebar_link[:id].blank?
                old_emarketing = emarketing.emarketing_images.find_by_id(sidebar_link[:id])
                old_emarketing.destroy unless old_emarketing.blank?
              end
            end
            id = sidebar_link[:id].blank? ? "#{rand(30)}#{Time.now.strftime("%m%d%Y%H%I%S")}" : sidebar_link[:id]
            image_id = s_emarketing_image.blank? ? sidebar_link[:image_id] : s_emarketing_image.id
            session["sidebar_links"] << { "id" => id, "image_id" => image_id, "url" => sidebar_link[:url], "headline" => sidebar_link[:headline], "field_name1" => sidebar_link[:field_name1], "field_value1" => sidebar_link[:field_value1], "field_name2" => sidebar_link[:field_name2], "field_value2" => sidebar_link[:field_value2], "description" => sidebar_link[:description]} unless sidebar_link[:headline].blank?
          end
        end
      end
      
      if @emarketing_layout == "theme4"
        unless params[:news_link].blank?
          params[:news_link].each do  |news_link|
            unless news_link[:uploaded_data].blank?
              n_emarketing_image = EmarketingImage.new(news_link)
              n_emarketing_image.category = "news_image"
              n_emarketing_image.attachable = emarketing
              n_emarketing_image.save

              unless news_link[:id].blank?
                old_emarketing = emarketing.emarketing_images.find_by_id(news_link[:id])
                old_emarketing.destroy unless old_emarketing.blank?
              end
            end
            id = news_link[:id].blank? ? "#{rand(30)}#{Time.now.strftime("%m%d%Y%H%I%S")}" : news_link[:id]
            image_id = n_emarketing_image.blank? ? news_link[:image_id] : n_emarketing_image.id
            session["news_links"] << { "id" => id, "image_id" => image_id, "url" => news_link[:url], "headline" => news_link[:headline], "description" => news_link[:description], "date" => news_link[:date]} unless news_link[:headline].blank?
          end
        end
        
        unless params[:sidebar_link].blank?
          params[:sidebar_link].each do  |sidebar_link|
            unless sidebar_link[:uploaded_data].blank?
              s_emarketing_image = EmarketingImage.new(sidebar_link)
              s_emarketing_image.category = "sidebar_image"
              s_emarketing_image.attachable = emarketing
              s_emarketing_image.save

              unless sidebar_link[:id].blank?
                old_emarketing = emarketing.emarketing_images.find_by_id(sidebar_link[:id])
                old_emarketing.destroy unless old_emarketing.blank?
              end
            end
            id = sidebar_link[:id].blank? ? "#{rand(30)}#{Time.now.strftime("%m%d%Y%H%I%S")}" : sidebar_link[:id]
            image_id = s_emarketing_image.blank? ? sidebar_link[:image_id] : s_emarketing_image.id
            session["sidebar_links"] << { "id" => id, "image_id" => image_id, "url" => sidebar_link[:url]} unless image_id.blank?
          end
        end
        bottom_left_to_session(params, emarketing)
        bottom_right_to_session(params, emarketing)        
      end
      
      if @emarketing_layout == "theme5"
        bottom_left_to_session(params, emarketing)
        bottom_right_to_session(params, emarketing)
      end
      
      save_update_emarketing
      redirect_to choose_colour_agent_office_emarketings_path(@agent, @office) and return
    else
      session["emarketing_data"] = { "sender_email" =>"", "sender_description" =>"", "sender_title" =>"", "office_id" =>"", "link_url" => ""} if session["emarketing_data"].blank?
    end
    
    if session["emarketing_data"].present? && session["emarketing_data"]["sender_description"].present?
      session["emarketing_data"]["sender_description"] = CGI.unescape(session["emarketing_data"]["sender_description"]).gsub("{ecampaign_track_url}&link_url=http://","").gsub("{ecampaign_track_url}&amp;link_url=","").gsub("http://","").gsub("http:/","")
    end
      
    if session["news_links"].present?
      session["news_links"].each do |news_link|
        if news_link["description"].present?
          news_link["description"] = CGI.unescape(news_link["description"]).gsub("{ecampaign_track_url}&link_url=http://","").gsub("{ecampaign_track_url}&amp;link_url=","").gsub("http://","").gsub("http:/","")
        end
      end
    end
    
    @data = session["emarketing_data"]
    @contacts = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq

    if @emarketing_layout == "theme1" || @emarketing_layout == "theme5"
      emarketing = Emarketing.find(session["emarketing_id"]) unless session["emarketing_id"].blank?
      @emarketing_image = emarketing.emarketing_images.find_by_category("main_image") unless emarketing.blank?
    end

    if @emarketing_layout == "theme2"
      unless session["social_links"].blank?
        @facebook_social_link = session["social_links"]["facebook_social_link"]
        @twitter_social_link = session["social_links"]["twitter_social_link"]
      end

      @general_links = ""
      unless session["general_links"].blank?
        @general_links = session["general_links"]
#        session["general_links"].each do |general_link|
#          unless general_link["id"].blank?
#            img = Attachment.find(general_link["id"])
#            @general_links += '<div class="logos"><table width="230"><tr><td colspan="2" style="text-align:center;height:150px;"><img src="'+img.public_filename+'" width="185"/></td></tr><tr><td>&nbsp;<b>Link:&nbsp;</b></td><td><input type="hidden" name="general_link[][id]" value="'+general_link["id"].to_s+'" /><input type="text" name="general_link[][url]" class="single" style="width:190px;" value="'+general_link["url"]+'" /></td></tr><tr><td>&nbsp;<b>Image Logo:&nbsp;</b></td><td><input type="file" name="general_link[][uploaded_data]" id="general_link__uploaded_data" /></td></tr><tr><td colspan="2">&nbsp;<a href="'+delete_image_agent_office_emarketings_path(@agent, @office)+'?type=general_link&image_id='+general_link["id"].to_s+'" name="delete_img">Delete Image</a></td></tr></table></div>'
#          end
#        end
      end
    end

    if @emarketing_layout == "theme3"
      unless session["social_links"].blank?
        @facebook_social_link = session["social_links"]["facebook_social_link"]
        @twitter_social_link = session["social_links"]["twitter_social_link"]
      end
      @news_links = session["news_links"]
      @sidebar_links = session["sidebar_links"]
      @social_share = session["social_share"]
      @larger_photos = session["larger_photos"]
    end
    
    if @emarketing_layout == "theme4"
      unless session["social_links"].blank?
        @facebook_social_link = session["social_links"]["facebook_social_link"]
        @twitter_social_link = session["social_links"]["twitter_social_link"]
      end
      @news_links = session["news_links"]
      @sidebar_links = session["sidebar_links"]
      @bottom_left = session["bottom_left"].first if session["bottom_left"].present?
      @bottom_right = session["bottom_right"].first if session["bottom_right"].present?
    end
    
    if @emarketing_layout == "theme5"
      unless session["social_links"].blank?
        @facebook_social_link = session["social_links"]["facebook_social_link"]
        @twitter_social_link = session["social_links"]["twitter_social_link"]
      end
      @news_links = session["news_links"]
      @bottom_left = session["bottom_left"].first if session["bottom_left"].present?
      @bottom_right = session["bottom_right"].first if session["bottom_right"].present?
    end
    
  end

  def choose_colour
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Footer"])
    @find_side_image = find_image("Side Image")
    @color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #999999 #CCCCCC #f0f0f0 #FFFFFF)
    if !params[:choose_colours].blank?
      session["emarketing_colour"] = { "bgcolor" => params[:bgcolor], "textcolor" => params[:textcolor], "heading" =>params[:heading], "headingtextcolor" =>params[:headingtextcolor], "maintextcolor" => params[:maintext]}
      save_update_emarketing
      redirect_to select_recipient_agent_office_emarketings_path(@agent, @office)
    else
      session["emarketing_colour"] = { "bgcolor" =>"", "textcolor" =>"", "heading" =>"", "headingtextcolor" =>"", "maintextcolor" => ""} if session["emarketing_colour"].blank?
    end
    @bgcolor = session["emarketing_colour"]["bgcolor"].blank? ? "#FFFFFF": session["emarketing_colour"]["bgcolor"]
    @textcolor =  session["emarketing_colour"]["textcolor"].blank? ? "#000000" : session["emarketing_colour"]["textcolor"]
    @heading =  session["emarketing_colour"]["heading"].blank? ? "#FFFFFF" : session["emarketing_colour"]["heading"]
    if session["emarketing_layout"] == "theme5"
      @maintext =  session["emarketing_colour"]["maintextcolor"].blank? ? "#EE4D2A" : session["emarketing_colour"]["maintextcolor"]
    end
    @headingtextcolor =  session["emarketing_colour"]["headingtextcolor"].blank? ? "#000000" : session["emarketing_colour"]["headingtextcolor"]    
  end

  def select_recipient
    @property = Property.first
    if @agent.developer.allow_ecampaign == true
      @group_contacts = GroupContact.find(:all)
    else
      @group_contacts = GroupContact.find(:all, :conditions => "office_id=#{@office.id}")
    end

    @category_contacts = CategoryContact.find(:all)
    all_contacts = AgentContact.find(:all, :conditions => "office_id = #{params[:office_id]} and inactive is not true", :select => "id")
    @all_contacts = all_contacts.present? ? all_contacts.map{|a| a.id.to_s}.join('#') : ""

    if !params[:update].blank?
      session["emarketing_group_id"] = params[:group_ids].blank? ? "" : params[:group_ids].gsub(/ /,'').split("#").delete_if{|x| x.blank?}
      session["emarketing_contact_checkbox"] = params[:contact_checkbox]
      if params[:selected_contacts].present?
        sc = params[:selected_contacts].split('#')
        sc.delete_if{|a| a.blank?}
        session["emarketing_contact_name_checkbox"] = sc
      else
        session["emarketing_contact_name_checkbox"] = []
      end
      session["emarketing_recipient_data"] = params[:recipient_data]
      save_update_emarketing
      redirect_to view_sample_agent_office_emarketings_path(@agent, @office)
    else
      session["emarketing_recipient_data"] = {"search_contact_name" => ""} if session["emarketing_recipient_data"].blank?
    end
    @recipient_data = session["emarketing_recipient_data"]

    @value_group = session["emarketing_group_id"].join("#") unless session["emarketing_group_id"].blank?
    @value_groups = ""
    session["emarketing_group_id"].each{|group|
      gc = GroupContact.find_by_id(group)
      @value_groups = @value_groups + '<span style="margin-right: 20px;" id="group'+group+'">'+gc.name+'<a href="javascript:remove_options(\'group\',\''+group+'\',\'group\')">x</a></span> '
    } unless session["emarketing_group_id"].blank?

    @contact_checkbox = session["emarketing_contact_checkbox"]
    @contact_checkbox_ids = ""
    @contact_checkbox.each{|cid|
      @contact_checkbox_ids = @contact_checkbox_ids + '#' + cid
    } unless @contact_checkbox.blank?

    @contact_name_checkbox = session["emarketing_contact_name_checkbox"]
    @contact_name_checkbox_ids = ''
    @contact_name_checkbox.each{|cid|
      @contact_name_checkbox_ids = @contact_name_checkbox_ids + '#' + cid
    } unless @contact_name_checkbox.blank?
    
    @alert_type = [['Newly Listed Properties', 'Newly Listed Properties'], ['Updated Properties', 'Updated Properties'], ['Open Inspections', 'Open Inspections'], ['Sold/Leased Properties', 'Sold/Leased Properties'], ['Auctions', 'Auctions'], ['Latest News', 'Latest News']]
    @listing_type = [['Business Sale', 'BusinessSale'], ['Commercial Sale/Lease', 'Commercial'], ['Residential Lease', 'ResidentialLease'],['Residential Sale', 'ResidentialSale'], ['Holiday Lease', 'HolidayLease'], ['Project Sale', 'ProjectSale']]
    @range = (1..10).map{|x|[x,x]}
  end

  def view_sample
    session["user_links"] = session["news_urls"] = nil
    session["user_links"] = params[:user_links] if params[:user_links].present?
    session["news_urls"] = params[:news_urls] if params[:news_urls].present?
    redirect_to send_emarketing_agent_office_emarketings_path(@agent, @office) if params[:send_emarketing]
    emarketing_data
  end

  def send_emarketing
    emarketing_data
    
    unless session["user_links"].blank?
      session["user_links"].each do |link|
        enc_link = CGI.escape(link.gsub("{ecampaign_track_url}&link_url=","").gsub("http://","").gsub("https://",""))
        tracked_desc = session["emarketing_data"]["sender_description"].gsub("href=\"#{CGI.escapeHTML(link)}\"","href=\"{ecampaign_track_url}&link_url=http://#{enc_link}\"")
        session["emarketing_data"]["sender_description"]  = tracked_desc
      end
    end  
    
    if session["news_urls"].present? && session["news_links"].present?
      session["news_urls"].each do |link|
        session["news_links"].each do |news_link|
          link = CGI.unescape(link)
          enc_link = CGI.escape(link.gsub("{ecampaign_track_url}&link_url=","").gsub("http://","").gsub("https://",""))
          tracked_desc = news_link["description"].gsub("href=\"#{link}\"","href=\"{ecampaign_track_url}&link_url=http://#{enc_link}\"").gsub("href=\"#{CGI.escapeHTML(link)}\"","href=\"{ecampaign_track_url}&link_url=http://#{enc_link}\"")
          news_link["description"] = tracked_desc
        end
      end
    end
    
    html = render_to_string(:layout => false)
    title = session["emarketing_data"]["sender_title"]
    sender_id = session["emarketing_data"]["sender_email"]
    link_url = session["emarketing_data"]["link_url"].gsub(/\s+/, "") unless session["emarketing_data"]["link_url"].blank?
    contact_groups = session["emarketing_contact_checkbox"]
    contact_names = session["emarketing_contact_name_checkbox"]
    all_data = formated_data
    
    unless session["emarketing_id"].blank?
      emarketing = Emarketing.find(session["emarketing_id"])
      emarketing.title = title
      emarketing.stored_layout = html
      emarketing.stored_data = all_data.to_json
      emarketing.sent_at = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      emarketing.save!
    end
    
    unless session["user_links"].blank?
      session["user_links"].each do |link|
        link = link.gsub("{ecampaign_track_url}&link_url=","")
        link = CGI.unescape(link)
        ec = EcampaignClick.find(:first, :conditions => "type_ecampaign='emarketing' And type_ecampaign_id=#{emarketing.id} And `destination_link` LIKE '%#{link}%'")
        EcampaignClick.create({:type_ecampaign => 'emarketing', :type_ecampaign_id => emarketing.id, :destination_name => link, :destination_link => link, :clicked => 0}) if ec.blank?
      end
    end
    
    unless session["news_urls"].blank?
      session["news_urls"].each do |link|
        link = link.gsub("{ecampaign_track_url}&link_url=","")
        link = CGI.unescape(link)
        ec = EcampaignClick.find(:first, :conditions => "type_ecampaign='emarketing' And type_ecampaign_id=#{emarketing.id} And `destination_link` LIKE '%#{link}%'")
        EcampaignClick.create({:type_ecampaign => 'emarketing', :type_ecampaign_id => emarketing.id, :destination_name => link, :destination_link => link, :clicked => 0}) if ec.blank?
      end
    end
    
    unless link_url.blank?
      ec = EcampaignClick.find(:first, :conditions => "type_ecampaign='emarketing' And type_ecampaign_id=#{emarketing.id} And `destination_link` LIKE '%#{link_url.gsub("http://", "")}%'")
      EcampaignClick.create({:type_ecampaign => 'emarketing', :type_ecampaign_id => emarketing.id, :destination_name => link_url, :destination_link => link_url.gsub("http://", ""), :clicked => 0}) if ec.blank?
    end    
    
    unless session["social_links"].blank?
      ["facebook_social_link", "twitter_social_link"].each do |link_type|
        unless session["social_links"][link_type].blank?
          link_url = session["social_links"][link_type].gsub("http://", "").gsub(/\s+/, "")
          ec = EcampaignClick.find(:first, :conditions => "type_ecampaign='emarketing' And type_ecampaign_id=#{emarketing.id} And `destination_link` LIKE '%#{link_url}%'")
          EcampaignClick.create({:type_ecampaign => 'emarketing', :type_ecampaign_id => emarketing.id, :destination_name => link_url, :destination_link => link_url, :clicked => 0}) if ec.blank?
        end
      end
    end

    unless session["general_links"].blank?
      session["general_links"].each do |general_link|
        unless general_link["url"].blank?
          link_url = general_link["url"].gsub("http://", "").gsub(/\s+/, "")
          ec = EcampaignClick.find(:first, :conditions => "type_ecampaign='emarketing' And type_ecampaign_id=#{emarketing.id} And `destination_link` LIKE '%#{link_url}%'")
          EcampaignClick.create({:type_ecampaign => 'emarketing', :type_ecampaign_id => emarketing.id, :destination_name => link_url, :destination_link => link_url, :clicked => 0}) if ec.blank?
        end
      end
    end

    unless session["news_links"].blank?
      session["news_links"].each do |news_link|
        unless news_link["url"].blank?
          link_url = news_link["url"].gsub("http://", "").gsub(/\s+/, "")
          ec = EcampaignClick.find(:first, :conditions => "type_ecampaign='emarketing' And type_ecampaign_id=#{emarketing.id} And `destination_link` LIKE '%#{link_url}%'")
          EcampaignClick.create({:type_ecampaign => 'emarketing', :type_ecampaign_id => emarketing.id, :destination_name => link_url, :destination_link => link_url, :clicked => 0}) if ec.blank?
        end
      end
    end

    unless session["sidebar_links"].blank?
      session["sidebar_links"].each do |sidebar_link|
        unless sidebar_link["url"].blank?
          link_url = sidebar_link["url"].gsub("http://", "").gsub(/\s+/, "")
          ec = EcampaignClick.find(:first, :conditions => "type_ecampaign='emarketing' And type_ecampaign_id=#{emarketing.id} And `destination_link` LIKE '%#{link_url}%'")
          EcampaignClick.create({:type_ecampaign => 'emarketing', :type_ecampaign_id => emarketing.id, :destination_name => link_url, :destination_link => link_url, :clicked => 0}) if ec.blank?
        end
      end
    end

    if emarketing
      spawn(:kill => true) do
        begin
        emarketing.send_emarketing(contact_groups, contact_names, sender_id, @agent, @office, @base_url)
        rescue Exception => ex
          Log.create(:message => "Emarketing send failed : #{ex.inspect}")
        end
      end
    end
    flash[:notice] = "Emarketing has successfully been sent"
    redirect_to agent_office_emarketings_path(@agent, @office)
  end

  def send_mail_emarketing(contact, sender, emarketing)
    contact_id = contact.id
    sender_id = sender.id if sender.present?
    unless contact.blank?
    if contact.inactive != true
      et = EcampaignTrack.find(:all, :conditions => "type_ecampaign_id = #{emarketing.id} and type_ecampaign = 'emarketing' and agent_contact_id = #{contact_id}")
      if et.blank?
        ecampaign_track = EcampaignTrack.create(:type_ecampaign_id => emarketing.id, :type_ecampaign => 'emarketing', :agent_contact_id => contact_id)
        if ecampaign_track
          if !contact.email.blank?
            begin
              EcampaignMailer.deliver_send_emarketing(@base_url, sender, emarketing, ecampaign_track, contact)
              ContactNote.create({:agent_contact_id => contact.id, :agent_user_id => sender_id, :note_type_id => 23, :description => "An eMarketing was sent to this user", :note_date => Time.now})
            rescue
              ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
            end
          else
            ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ecampaign_track.id}") unless ecampaign_track.blank?
          end
        end
      end
    end
    end
  end

  def formated_data
    all_data = { "emarketing_layout" => session["emarketing_layout"],
    "emarketing_data" => session["emarketing_data"],
    "emarketing_colour" => session["emarketing_colour"],
    "emarketing_group_id" => session["emarketing_group_id"],
    "emarketing_contact_checkbox" => session["emarketing_contact_checkbox"],
    "emarketing_contact_name_checkbox" => session["emarketing_contact_name_checkbox"],
    "emarketing_recipient_data" => session["emarketing_recipient_data"],
    "general_links" => session["general_links"], "social_links" => session["social_links"],
    "news_links" => session["news_links"], "sidebar_links" => session["sidebar_links"],
    "social_share" => session["social_share"],"bottom_left" => session["bottom_left"], "bottom_right" => session["bottom_right"], 
    "larger_photos" =>session["larger_photos"]
    }
    return all_data
  end

  def save_update_emarketing
    all_data = formated_data
    unless session["emarketing_id"].blank?
      ec = Emarketing.find(session["emarketing_id"])
      ec.title = session["emarketing_data"]["sender_title"] unless session["emarketing_data"].blank?
      ec.stored_data = all_data.to_json
      ec.save!
    else
      all_data.merge!({"emarketing_group_id"=>"","emarketing_contact_checkbox"=>"", "emarketing_contact_name_checkbox"=>""})
      session["emarketing_group_id"] = ""
      session["emarketing_contact_checkbox"] = ""
      session["emarketing_contact_name_checkbox"] = ''
      ec = Emarketing.create(:office_id => @office.id, :title => (session["emarketing_data"].blank? ? "": session["emarketing_data"]["sender_title"]), :stored_data => all_data.to_json)
      session["emarketing_id"] = ec.id
    end
    return ec
  end

  def forward
    #forward_agent_office_emarketings_path
    #localhost:3000/agents/1/offices/1/emarketings/1/forward?track_id=1
    @agent_contact = AgentContact.new
    @track_id = params[:track_id]
    @emarketing = Emarketing.find(params[:id])

    respond_to do |format|
      format.html { render :layout => "new_level4"}
    end
  end

  def send_forward
    @agent_contact = AgentContact.new(params[:agent_contact])
    @track_id = params[:track_id]
    respond_to do |format|
      @agent_contact.ecampaign_contact = true
      if @agent_contact.save
        emarketing_track = EcampaignTrack.find_by_id(params[:track_id])
        if emarketing_track
          spawn(:kill => true) do
            emarketing = Emarketing.find_by_id(params[:id])
            sender = AgentContact.find_by_id(emarketing_track.agent_contact_id)
            begin
              EcampaignMailer.deliver_send_emarketing(full_base_url, sender, emarketing, emarketing_track, @agent_contact)
            rescue Net::SMTPServerBusy
              ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{emarketing_track.id}") unless emarketing_track.blank?
            end
          end
        end
        #track forwarded link
        ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_forwarded = 1 WHERE id=#{@track_id}") unless @track_id.blank?
        flash[:notice] = 'Emarketings has successfully been forwarded.'
        format.html { redirect_to :action => 'forward' }
      else
        format.html { render :action => "forward", :layout => "new_level4" }
      end
    end
  end

  def sample
    @emarketing = Emarketing.find(params[:id])
    respond_to do |format|
      format.html { render :layout => false}
    end
  end

  def send_sample
    unless params[:id].blank?
      unless params[:email].blank?
        emarketing_data
        html = render_to_string(:layout => false)
        
        @emarketing = Emarketing.find(params[:id])
        @emarketing.stored_layout = html
        @emarketing.save!
        
        if session["news_links"].present?
          session["news_links"].each do |news|
            if news["url"].present?
              news["url"].gsub!("http://","")
              @emarketing.stored_layout.gsub!(CGI.escape(news["url"]),news["url"])
            end
          end
        end
        if session["bottom_left"].present?
          session["bottom_left"].each do |bottom_left|
            if bottom_left["url"].present?
              bottom_left["url"].gsub!("http://","")
              @emarketing.stored_layout.gsub!(CGI.escape(bottom_left["url"]), bottom_left["url"])
            end
          end
        end
        if session["bottom_right"].present?
          session["bottom_right"].each do |bottom_right|
            if bottom_right["url"].present?
              bottom_right["url"].gsub!("http://","")
              @emarketing.stored_layout.gsub!(CGI.escape(bottom_right["url"]), bottom_right["url"])
            end
          end
        end
        
        sender = AgentUser.find_by_id(session["emarketing_data"]["sender_email"])
        params[:email].each do |email|
        unless email.blank?
          begin
            @emarketing.stored_layout.gsub!("{recipient_mail_address}", email)
            EcampaignMailer.deliver_send_sample(sender, @emarketing, email)
          rescue Exception => ex
            Log.create({:message => "Failed send sample email emarketing #{@emarketing.id}, ERR : #{ex.inspect}"})
          end
        end
        end
      end
      flash[:notice] = 'Sample Email sent.'
    end
    redirect_to sample_agent_office_emarketing_path(@agent,@office,@emarketing)
  end

  def get_image
    #track opened email
    ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_opened =1 WHERE id=#{params[:track_id]}") unless params[:track_id].blank?
    image = RAILS_ROOT+"/public/images/transparant.png"
    render :text => open(image, "rb").read
  end

  def emarketing_data
    session["emarketing_data"] = { "sender_email" =>"", "sender_description" =>"", "sender_title" =>"", "office_id" =>""} if session["emarketing_data"].blank?
    
    if session["emarketing_data"].present? && session["emarketing_data"]["sender_description"].present?
      session["emarketing_data"]["sender_description"] = CGI.unescape(session["emarketing_data"]["sender_description"]).gsub("{ecampaign_track_url}&link_url=http://","").gsub("{ecampaign_track_url}&amp;link_url=","").gsub("http://","").gsub("http:/","")
    end
    
    if session["news_links"].present?
      session["news_links"].each do |news_link|
        if news_link["description"].present?
          news_link["description"] = CGI.unescape(news_link["description"]).gsub("{ecampaign_track_url}&link_url=http://","").gsub("{ecampaign_track_url}&amp;link_url=","").gsub("http://","").gsub("http:/","")
        end
      end
    end
    
    @base_url = full_base_url
    @emarketing_id = session["emarketing_id"]
    @emarketing = Emarketing.find(@emarketing_id)
    @side_image = find_image("Side Image")
    @header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Header"])
    @footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "Emarketing Brochure Footer"])
    @data = session["emarketing_data"]
    @emarketing_layout=session["emarketing_layout"]
    unless session["emarketing_colour"].blank?
      @bgcolor = session["emarketing_colour"]["bgcolor"]
      @textcolor = session["emarketing_colour"]["textcolor"]
      @heading = session["emarketing_colour"]["heading"]
      @headingtextcolor = session["emarketing_colour"]["headingtextcolor"]
      @maintext = session["emarketing_colour"]["maintextcolor"]
    end

    unless @data.blank?
      unless @data["sender_email"].blank?
        @sender = AgentUser.find_by_id(@data["sender_email"])
        unless @sender.blank?
          portraits = @sender.portrait_image
          unless portraits.blank?
            portraits.each do |img_portrait|
              if img_portrait.position == 1
                @img_portrait_1 = img_portrait
              else
                @img_portrait_2 = img_portrait
              end
            end
          end
        end
      end
    end

    emarketing = Emarketing.find(session["emarketing_id"]) unless session["emarketing_id"].blank?
    if @emarketing_layout == "theme1" || @emarketing_layout == "theme5"
      @emarketing_image = emarketing.emarketing_images.find_by_category("main_image") unless emarketing.blank?
    end

    if @emarketing_layout == "theme2"
      unless session["social_links"].blank?
        @facebook_social_link = session["social_links"]["facebook_social_link"]
        @twitter_social_link = session["social_links"]["twitter_social_link"]
      end
      @general_links = ""
      unless session["general_links"].blank?
        session["general_links"].each do |general_link|
          unless general_link["id"].blank?
            img = Attachment.find(general_link["id"])
#            @general_links += '<div style="margin-bottom:20px;"><a href="{ecampaign_track_url}&link_url=http://'+general_link["url"].gsub("http://", "")+'" name="img"><img src="'+img.public_filename+'" width="185"/></a></div>' unless general_link["url"].blank?
             if img.present?
               if general_link["url"].present?
                general_url = general_link["url"].gsub("http://", "").gsub('https://',"")
                general_url = CGI.escape(general_url)
                @general_links += '<tr><td style="padding:0 0 10px 0;text-align:center"><a href="{ecampaign_track_url}&link_url=http://'+general_url+'" name="img"><img src="'+img.public_filename+'" width="185"/></a></td></tr>'
               else
                @general_links += '<tr><td style="padding:0 0 10px 0;text-align:center"><img src="'+img.public_filename+'" width="185"/></td></tr>'
               end
             end
          end
        end
      end
    end

    if @emarketing_layout == "theme3"
      unless session["social_links"].blank?
        @facebook_social_link = session["social_links"]["facebook_social_link"]
        @twitter_social_link = session["social_links"]["twitter_social_link"]
      end
      @news_links = session["news_links"]
      @sidebar_links = session["sidebar_links"]
      @social_share = session["social_share"]
      @larger_photos = session["larger_photos"]
    end
    
    if @emarketing_layout == "theme4"
      unless session["social_links"].blank?
        @facebook_social_link = session["social_links"]["facebook_social_link"]
        @twitter_social_link = session["social_links"]["twitter_social_link"]
      end
      @news_links = session["news_links"]
      @sidebar_links = session["sidebar_links"]
      @bottom_left = session["bottom_left"].first if session["bottom_left"].present?
      @bottom_right = session["bottom_right"].first if session["bottom_right"].present?
    end
    
    if @emarketing_layout == "theme5"
      unless session["social_links"].blank?
        @facebook_social_link = session["social_links"]["facebook_social_link"]
        @twitter_social_link = session["social_links"]["twitter_social_link"]
      end
      @news_links = session["news_links"]
      @bottom_left = session["bottom_left"].first if session["bottom_left"].present?
      @bottom_right = session["bottom_right"].first if session["bottom_right"].present?
    end
    
  end

  def set_data(db_data)
    session["emarketing_layout"] = db_data["emarketing_layout"]
    session["emarketing_data"] = db_data["emarketing_data"]
    session["emarketing_colour"] = db_data["emarketing_colour"]
    session["emarketing_group_id"] = db_data["emarketing_group_id"]
    session["emarketing_contact_checkbox"] = db_data["emarketing_contact_checkbox"]
    session["emarketing_contact_name_checkbox"] = db_data["emarketing_contact_name_checkbox"]
    session["emarketing_recipient_data"] = db_data["emarketing_recipient_data"]
    session["general_links"] = db_data["general_links"]
    session["social_links"] = db_data["social_links"]
    session["news_links"] = db_data["news_links"]
    session["sidebar_links"] = db_data["sidebar_links"]
    session["bottom_left"] = db_data["bottom_left"]
    session["bottom_right"] = db_data["bottom_right"]
  end

  def search
    if params[:status] == "sold"
      condition = "`status` IN (2,3)"
    else
      condition = "`status` IN (1,5)"
    end

    text = ''
    unless params[:search_value].blank?
      if @agent.developer.allow_listing_office == true
        condition = "((`suburb` LIKE '%#{params[:search_value]}%' OR `street` LIKE '%#{params[:search_value]}%') OR `id` = '#{params[:search_value]}') AND `status` IN (1,5) And `office_id`=#{@office.id}"
      else
        condition = "((`suburb` LIKE '%#{params[:search_value]}%' OR `street` LIKE '%#{params[:search_value]}%') OR `id` = '#{params[:search_value]}') AND `status` IN (1,5)"
      end
    else
      unless params[:selection_office_id].blank?
        tmp_office_id = params[:selection_office_id].split("#").delete_if{|x| x.blank?}
        office_id = tmp_office_id.join(",")
        condition += " And `office_id` IN (#{office_id})"
      else
        condition += " And `office_id` = #{@office.id}" if @agent.developer.allow_listing_office == true
      end

      unless params[:selection_property_type].blank?
        tmp_property_type = params[:selection_property_type].split("#").delete_if{|x| x.blank?}
        condition += " And `property_type` IN ('#{tmp_property_type.join("','")}')"
      end

      unless params[:selection_listing_type].blank?
        tmp_listing_type = params[:selection_listing_type].split("#").delete_if{|x| x.blank?}
        ori_listings = tmp_listing_type.join("','")
        deal_types = ""
        if ori_listings.include?("CommercialSale")
          ori_listings = ori_listings.gsub("CommercialSale", "Commercial")
          deal_types += "'Sale'"
        end
        if ori_listings.include?("CommercialLease")
          ori_listings = ori_listings.gsub("CommercialLease", "Commercial")
          if deal_types.blank?
            deal_types += "'Lease'"
          else
            deal_types += ",'Lease', 'Both'"
          end
        end
        condition += " And `type` IN ('#{ori_listings}')"
        condition += " And (`deal_type` IN (#{deal_types}) OR `deal_type` IS NULL)" unless deal_types.blank?
      end
    end

    properties = Property.find(:all, :conditions => condition)
    jsons = []
    unless properties.blank?
      properties.each{|p|
        unless p.blank?
        ad = [ p.unit_number, p.street_number, p.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
        address = [ad, p.suburb ].join(", ")
        jsons << {'id' => p.id, 'address' => "#{address unless p.address.blank?}",'suburb' =>"#{p.suburb}",'type' =>"#{p.type}",'price' =>"$#{p.price.to_i}",'status' =>"#{p.status_name}", 'href' => agent_office_properties_path(@agent, @office)+"?ID=#{p.id}"}
        end
      } unless properties.nil?
      text = {"results" => jsons, "total" => (properties ? properties.length : 0) }
    end

    render :json => text
  end

  def search_contact
    text = selection_contact_json(params[:selection_group_id], params[:selection_category_id])
    render :json => text
  end

  def find_image(type)
    get_header_footer(@office.id, type)
  end
  
  def delete_emarketing_image
    if session["general_links"].present? && params[:image_id].present?
      session["general_links"].delete_if{|a| a["id"] == params[:image_id].to_i}
    end
    if params[:part].present?
      if params[:part] == "1"
        session["news_links"].each{|a| a["image_id"] = "" if a["image_id"] == params[:image_id]}
      elsif params[:part] == "2"
        session["news_links"].each{|a| a["image_id2"] = "" if a["image_id2"] == params[:image_id]}
      elsif params[:part] == "3"
        session["bottom_left"].first["image_id"] = "" if session["bottom_left"].present?
      elsif params[:part] == "4"
       session["bottom_right"].first["image_id"] = "" if session["bottom_right"].present?
      end
    end
    img = Attachment.find(:first, :conditions => "id = #{params[:image_id]} and attachable_id = #{params[:emarketing_id]} and attachable_type = 'Emarketing'")   
    img.destroy unless img.blank?    
    return render(:json =>true)
  end
  
  def create_bounce_track_and_contact_notes(invalid_emails, recipients, sender_id, emarketing)
    if invalid_emails.present? && invalid_emails != true
      begin
      invalid_emails = invalid_emails.flatten
      invalid_contacts = AgentContact.find(:all, :conditions => "email IN (\"#{invalid_emails.join("\",\"")}\") and office_id = #{@office.id}")
      invalid_ids = invalid_contacts.map{|a| a.id} if invalid_contacts.present?
      if invalid_contacts.present?
        invalid_contacts.each do |contact|
          ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE type_ecampaign_id = #{emarketing.id} and type_ecampaign like 'emarketing' and agent_contact_id = #{contact.id}")
          contact_bounced_times(contact)
        end
      end
      rescue
      end
    end
    valid_recipients = invalid_ids.present? ? recipients.reject{|r| invalid_ids.include?(r.id)} : recipients
    valid_recipients.each do |recipient|
      ContactNote.create({:agent_contact_id => recipient.id, :agent_user_id => sender_id, :note_type_id => 23, :description => "An eMarketing was sent to this user", :note_date => Time.now})  
    end
  end
  
  def bottom_left_to_session(params, emarketing)
    unless params[:bottom_left].blank?
      params[:bottom_left].each do |news_link|
        unless news_link[:uploaded_data].blank?
          n_emarketing_image = EmarketingImage.new(news_link)
          n_emarketing_image.category = "news_image"
          n_emarketing_image.attachable = emarketing
          n_emarketing_image.save

          unless news_link[:id].blank?
            old_emarketing = emarketing.emarketing_images.find_by_id(news_link[:id])
            old_emarketing.destroy unless old_emarketing.blank?
          end
        end
        id = news_link[:id].blank? ? "#{rand(30)}#{Time.now.strftime("%m%d%Y%H%I%S")}" : news_link[:id]
        image_id = n_emarketing_image.blank? ? news_link[:image_id] : n_emarketing_image.id
        session["bottom_left"] << { "id" => id, "image_id" => image_id, "url" => news_link[:url], "headline" => news_link[:headline], "description" => news_link[:description], "date" => news_link[:date]} unless news_link[:headline].blank?
      end
    end
  end
  
  def bottom_right_to_session(params, emarketing)
    unless params[:bottom_right].blank?
      params[:bottom_right].each do |news_link|
        unless news_link[:uploaded_data].blank?
          n_emarketing_image = EmarketingImage.new(news_link)
          n_emarketing_image.category = "news_image"
          n_emarketing_image.attachable = emarketing
          n_emarketing_image.save

          unless news_link[:id].blank?
            old_emarketing = emarketing.emarketing_images.find_by_id(news_link[:id])
            old_emarketing.destroy unless old_emarketing.blank?
          end
        end
        id = news_link[:id].blank? ? "#{rand(30)}#{Time.now.strftime("%m%d%Y%H%I%S")}" : news_link[:id]
        image_id = n_emarketing_image.blank? ? news_link[:image_id] : n_emarketing_image.id
        session["bottom_right"] << { "id" => id, "image_id" => image_id, "url" => news_link[:url], "headline" => news_link[:headline], "description" => news_link[:description], "date" => news_link[:date]} unless news_link[:headline].blank?
      end
    end
  end
  
  private


  def selection_group_json(param_ids)
    unless param_ids.blank?
      ids = param_ids.split("#").delete_if{|x| x.blank?}
      return GroupContactRelation.find(:all, :select => "DISTINCT(agent_contact_id)", :conditions => {:group_contact_id => ids})
    end
  end

  def selection_category_json(param_ids)
    unless param_ids.blank?
      ids = param_ids.split("#").delete_if{|x| x.blank?}
      return ContactCategoryRelation.find(:all, :select => "DISTINCT(agent_contact_id)", :conditions => {:category_contact_id => ids})
    end
  end

  def selection_contact_json(group_ids, category_ids)
    jsons = []
    group_contacts = selection_group_json(group_ids)
    category_contacts = selection_category_json(category_ids)

    unless group_contacts.blank?
      group_contacts.each do |contact|
        unless contact.agent_contact.blank?
          jsons << {'id' => contact.agent_contact.id, 'name' => "#{contact.agent_contact.full_name}"} if contact.agent_contact.inactive != true
        end
      end
    end


    unless category_contacts.blank?
      category_contacts.each do |contact|
        unless contact.agent_contact.blank?
          jsons << {'id' => contact.agent_contact.id, 'name' => "#{contact.agent_contact.full_name}"} if contact.agent_contact.inactive != true
        end
      end
    end

    unless jsons.empty?
      return ({"results" => jsons, "total" => (jsons.blank? ? 0 : jsons.length) })
    else
      return ''
    end
  end

end
