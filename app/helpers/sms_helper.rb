module SmsHelper

  def purchase_credit_url(base_uri, office_id, redirect_url)
    uri = URI(base_uri)

    uri = append_uri(uri, "office_id=#{office_id}")
    uri = append_uri(uri, "redirect=#{redirect_url}")

    return uri.to_s
  end

  def append_uri(uri, string)
    URI.escape(string)
    uri.query = uri.query.nil? ? string : [uri.query, string].join('&') unless string.empty?
    return uri
  end

  def total_credit(number_of_receivers, number_of_messages)
    return number_of_receivers * number_of_messages
  end

  def total_messages(message)
    number_of_messages = message.length / 160 + 1
    number_of_messages = message.length / 153 + 1 if number_of_messages > 1

    return number_of_messages
  end
end
