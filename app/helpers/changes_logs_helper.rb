module ChangesLogsHelper

  def property_status(property)
    if property.deleted_at.blank?
      case property.status
      when 1
        "Available"
      when 2
        "Sold"
      when 3
        "Leased"
      when 4
        "Withdrawn"
      when 5
        "Under Offer"
      when 6
        "Draft"
      end
    else # if property is deleted
      "Deleted at #{property.deleted_at.strftime("%D %M %Y")}"
    end
  end

  def total_logs(property, all_logs)
    all_logs.find_all{|a| a.property_id == property.id}.count
  end


  def logged_user(log)
    user = User.find(log.agent_user_id) if log.agent_user_id != 0
    if user.present?
      "#{user.full_name}"
    end
  end

  def old_new_value(logs, deleted_at)
    require 'json'
    str = ""
    logs.each do |log|
      updated_attributes = JSON.parse(log.updated_attributes) rescue ''
      # opentime changes
      if log.updated_child == "Opentime"
        str += ChangesLog.display_opentimes_log(log, updated_attributes, log_row(log))
      end

      if log.updated_child == "ContactPastopentimeRelation" 
        str += log_row(log)
        str += ChangesLog.display_pastopentime_log(updated_attributes)
      end

      # property media changes
      str += ChangesLog.display_media_log(log, updated_attributes, log_row(log))

      # property vendor detail changes
      str += ChangesLog.display_vendor_detail_log(log, updated_attributes, log_row(log))

      # property add purchaser
      str += ChangesLog.display_add_purchaser_log(log, updated_attributes, log_row(log))

      #property changes
      if updated_attributes.present?
        if deleted_at.present? && updated_attributes["property"].present?
          property_hash = updated_attributes["property"]
          property_hash.map{|k,v|
            str += log_row(log)
            str += "  <td width='64%' colspan='2'>
                        <span style='font-size:13px'><b>#{k.humanize.capitalize}:</b></span> #{v} <br/>
                      </td>
                    </tr>" rescue ""}
        elsif updated_attributes["property"].present?
          property_hash = updated_attributes["property"]
          property_hash.map{|k,v|
            str += log_row(log)
            str += "  <td width='32%'>
                        <span style='font-size:13px'><b>#{k.humanize.capitalize}:</b></span> #{v[0]} <br/>
                      </td>
                      <td width='32%'>
                        <span style='font-size:13px'><b>#{k.humanize.capitalize}:</b></span> #{v[1]} <br/>
                      </td>
                    </tr>" rescue ""}
        end

        # property features changes
        features_ids = updated_attributes["features"]
        if features_ids.present?
          if features_ids[0].present?
            old_features = Feature.find(:all, :conditions => "id IN (#{features_ids[0].join(',')})")
          end
          if features_ids[1].present?
            new_features = Feature.find(:all, :conditions => "id IN (#{features_ids[1].join(',')})")
          end
          str += log_row(log)
          str += "  <td width='32%'>
                      <span style='font-size:13px'><b>Old checked features:</b><br/></span> #{features_name(old_features)} <br/>
                    </td>
                    <td width='32%'>
                      <span style='font-size:13px'><b>Current checked features:</b><br/></span> #{features_name(new_features)} <br/>
                    </td>
                  </tr>"
        end

        # portal changes
        if updated_attributes["portal_changes"].present?
          current_portals = Portal.find(:all, :conditions => "id IN (#{updated_attributes["portal_changes"]["current_portal_ids"].join(",")})") if updated_attributes["portal_changes"]["current_portal_ids"].present?
          old_portals = Portal.find(:all, :conditions => "id IN (#{updated_attributes["portal_changes"]["old_portal_ids"].join(",")})") if updated_attributes["portal_changes"]["old_portal_ids"].present?
          str += log_row(log)
          str += "  <td width='32%'>
                      <span style='font-size:13px'><b>Old checked portals:</b><br/></span> #{portals_name(old_portals)} <br/>
                    </td>
                    <td width='32%'>
                      <span style='font-size:13px'><b>Current checked portals:</b><br/></span> #{portals_name(current_portals)} <br/>
                    </td>
                  </tr>"
        end

        # Property detail changes
        if updated_attributes["detail"].present?
          updated_attributes["detail"].map{|k,v|
            str += log_row(log)
            str += "  <td width='32%'>
                      <span style='font-size:13px'><b>#{k.humanize.capitalize}:</b></span> #{v[0]} <br/>
                    </td>
                    <td width='32%'>
                      <span style='font-size:13px'><b>#{k.humanize.capitalize}:</b></span> #{v[1]} <br/>
                    </td>
                  </tr>" rescue ""
          }
        end
      end
    end
    return str
  end

  def log_row(log)
    str = "<tr>
              <td width='15%'>
                #{logged_user(log)}
              </td>
              <td width='5%'>
                #{log.agent_user_id}
              </td>
              <td width='10%'>
                #{log.created_at.utc.strftime('%d/%m/%Y')} #{log.created_at.utc.strftime('%H:%M:%S')}
              </td>"
    return str
  end

  def portals_name(portals)
    str = ""
    if portals.present?
      portals.each do |portal|
        str += "#{portal.portal_name} <br/>"
      end
    end
    return str
  end

  def features_name(features)
    str = ""
    if features.present?
      features.each do |feature|
        str += "#{feature.name} <br/>"
      end
    end
    return str
  end

end


