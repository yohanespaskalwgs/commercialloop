module AgentsHelper
  def agent_developers_options(agent)
    agent.developer.developer_users.map{|u| [u.full_name, u.id]}.uniq.compact
  end

  def agent_recipient_options(agent)
    agents =[]
    developer_contact = [agent.developer_contact.full_name, agent.developer_contact_id]
    support_contact = [agent.support_contact.full_name, agent.developer_contact_id]
    upgrade_contact = [agent.upgrade_contact.full_name, agent.upgrade_contact_id]
    agents << developer_contact unless agents.include?(developer_contact)
    agents << support_contact unless agents.include?(support_contact)
    agents << upgrade_contact unless agents.include?(upgrade_contact)
    return agents
  end
end
