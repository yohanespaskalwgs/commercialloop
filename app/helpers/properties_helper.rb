module PropertiesHelper
  def options_for_latest(latest)
    options = ""
    %w( 10 25 50 75 100 ).each do |x|
      if x == latest
        options << "<option value=#{x} selected='selected'>Latest #{x}</option>"
      else
        options << "<option value=#{x}>Latest #{x}</option>"
      end
    end
    options
  end

  def feature_list(name, lists, style_class)
    output = ''
    unless lists.blank?
      output << "<li class=\"#{style_class}\">"
      output << "  <label><strong>#{name.capitalize}:</strong></label> "
      output << "  <ul> #{ render :partial => 'feature', :collection => lists } </ul></li>"
    end
    output
  end

  def change_date_format(date)
    unless date.blank?
      arr = date.split("/")
      return "#{arr[2]}-#{arr[1]}-#{arr[0]}"
    end
  end

 def change_time_format(time)
    unless time.blank?
      arr = time.split(":")
      return "#{arr[0]}:#{arr[1]}"
    end
  end

  def abbrv_properties(name)
    case name.to_s.downcase
    when "residentialsale"
      return "ResSale"
    when "residentiallease"
      return "ResLease"
    when "commercial"
      return "Commercial"
    when "commercialbuilding"
      return "Building"
    when "businesssale"
      return "BusSale"
    when "projectsale"
      return "HouseLand"
    when "holidaylease"
      return "HolLease"
    when "newdevelopment"
      return "ResBuilding"
    when "landrelease"
      return "LandRelease"
    end
  end

  def is_property_draft_for(office, property, current_user)
    st = nil
    if current_user.allow_property_owner_access?
      if property.status == 6 && office.property_owner_access && office.property_draft_for_new
        st = "disabled"
      else
        if office.property_owner_access && office.property_draft_for_updated
          st = "disabled"
        else
          if office.property_payment_required
           st = "disabled" unless  property.payment_status.to_s.downcase == "completed"
          else
           st = nil
          end
        end
      end
    end
    # if 'Control Level 3 User Access' enabled at office settings, all radio will be disabled
    if office.control_level3 && current_user.limited_client_access?
      st = "disabled"
    end
    return st
  end

  def decoded_and_converted_to(untrusted_string)
    return Iconv.new('UTF-8//IGNORE', 'UTF-8').iconv(untrusted_string)
  end

  def get_property_type(name)
    type = name.camelize
    render_optional_error_file(401) unless %w(ResidentialLease ResidentialSale Commercial CommercialBuilding HolidayLease ProjectSale BusinessSale NewDevelopment LandRelease GeneralSale LivestockSale ClearingSalesEvent ClearingSale).include?(type)
    type
  end

  def property_params(param)
    p = Property.find(:last)
    attr = p.attributes.delete_if { |x,y| ["deleted_at","created_at","updated_at","office_id","agent_id","agent_user_id", "processed", "api_is_sent", "count_api_sent", "cron_checked", "delta","id"].include?(x)  }
    pparams = {}
    attr.each{|arr,val| pparams = pparams.merge!({arr.to_sym => param[arr.to_sym]})}
    return pparams
  end

  def detail_params(param)
    unless param[:listing_type].blank?
      p = Property.find(:last, :conditions => "type = '#{param[:listing_type].camelize}'")
      attr = p.detail.attributes.delete_if { |x,y| ["residential_sale_id","residential_lease_id","business_sale_id","commercial_id","commercial_building_id","project_sale_id","holiday_lease_id", "new_development_id" "land_release_id","id"].include?(x)  }

      param[:number_of_floors] = (param[:number_of_floors].to_i == 0 ? 1 : param[:number_of_floors].to_i) unless param[:number_of_floors].blank?
      param[:bedrooms] = param[:bedrooms].to_i unless param[:bedrooms].blank?
      param[:bathrooms] = param[:bathrooms].to_i unless param[:bathrooms].blank?
      param[:price_include_tax] = param[:price_include_tax].to_i unless param[:price_include_tax].blank?
      param[:price_include_stock] = param[:price_include_stock].to_i unless param[:price_include_stock].blank?
      dparams = {}
      attr.each{|arr,val| dparams = dparams.merge!({arr.to_sym => param[arr.to_sym]})}
      return dparams
    end
  end

  def none_url(text)
    return text.gsub('http://', '')
  end

  def change_time_to_hour(time)
    unless time.blank?
      ar = time.split(":")
      return ar[0].to_s
    end
  end

  def change_time_to_min(time)
    unless time.blank?
      ar = time.split(":")
      return ar[1].to_s
    end
  end

  def property_rent_currency(price, rent_type = '', current_rent_include_tax = '')
    tax = ''
    unless current_rent_include_tax.blank?
      case current_rent_include_tax
        when 1
          tax = 'GST Not applicable'
        when 2
          tax = 'GST Exclusive'
        when 3
          tax = 'GST Inclusive'
      end
    end
    return "#{property_currency(price)} #{(rent_type.blank? ? '':rent_type)} #{tax}"
  end

  def property_currency(price)
    return (number_to_currency(price).blank? ? '': number_to_currency(price).gsub(".00",""))
  end

  def extra_detail_id(property,id)
    p = property.extra_details.find(:first, :conditions => "extra_detail_name_id =#{id}")
    p.id unless p.blank?
  end

  def extra_detail_name(property,id)
    p = property.extra_details.find(:first, :conditions => "extra_detail_name_id =#{id}")
    p.field_value unless p.blank?
  end

  def portals(id)
    portals = ""
    pro_e = PropertyPortalExport.find(:all, :include=>[:portal], :conditions=>"`property_id` =#{id}", :order => "portals.portal_name ASC")
    pro_e.each {|pe| portals += "#{pe.portal.portal_name}, " unless pe.portal.blank?} unless pro_e.blank?
    return portals
  end

  def report_floor(id)
    p = Property.find_by_id(id)
    floor = (p.detail.floor_area if p.detail.respond_to?(:floor_area)) unless p.blank?
    return floor
  end

  def report_eer(id)
    p = Property.find_by_id(id)
    eer = (p.detail.energy_efficiency_rating if p.detail.respond_to?(:energy_efficiency_rating)) unless p.blank?
    return eer
  end

  def report_office_name(id)
    p = Property.find_by_id(id)
    office_name = p.office.name unless p.blank?
    return office_name
  end

  def report_agent_names(id)
    p = Property.find_by_id(id)
    agent_names = ""
    unless p.blank?
      agent_names += p.primary_contact.full_name unless p.primary_contact.blank?
      agent_names += ",#{p.secondary_contact.full_name}" unless p.secondary_contact.blank?
      agent_names += ",#{p.third_contact.full_name}" unless p.third_contact.blank?
      agent_names += ",#{p.fourth_contact.full_name}" unless p.fourth_contact.blank?
    end
    return agent_names
  end

  def portal_description(portal_account, portals)
    selected = portals.detect{|a| a.portal_name == portal_account.portal_name}
    desc = selected.description if selected.present?
    return desc
  end

  def export_history(id,portal_account,portals, exports, export_properties)
    str = ""
    portal_id = portals.detect{|a| a.portal_name == portal_account.portal_name}.id
    histories = exports.find_all{|a| a.portal_id == portal_id}

    if histories.present?
      histories = histories.sort_by{|a| a.created_at}.reverse!
    end
    histories.each_with_index do |hist, idx|
      if idx < 3
        str += "<tr>"
        str +=     "<td>
                      <a href=\"#\">#{id}</a>
                    </td>
                    <td>
                      #{hist.created_at.strftime("%d/%m/%Y")}
                    </td>
                    <td>
                     <a href='http://xml.commercialloopcrm.com.au/exports/view/#{id}?file=#{portal_id}/#{hist.filename}' target='_blank'>#{hist.filename}</a>
                    </td>
                    <td>
                      #{export_report_back(portals,hist)}
                    </td>
                    <td>
                      #{export_status(portals,hist,export_properties)}
                    </td>
                    <td>
                      #{get_export_message(hist,export_properties)}
                    </td>
                </tr>"
      end
    end
    str
  end

  def get_export_message(export,export_properties)
    message = export_properties.detect{|a| a.export_id == export.id}.message rescue ''
    return message
  end

  def export_report_back(portals,export)
    this_portal = portals.detect{|p| p.id == export.portal_id}
    if !this_portal.has_reports
      return "This portal has no reports"
    else
      return export.report_received == 1 ? "Yes" : "No"
    end
  end

  def export_status(portals,export,export_properties)
    report_back = export_report_back(portals,export)
    if report_back == "This portal has no reports" || report_back == "No"
      status = "Not Available"
    else
      status_num = export_properties.detect{|a| a.export_id == export.id}.attributes_before_type_cast['status'] rescue ''
      case status_num
      when "1"
        status = "Success"
      when "2"
        status = "Error"
      when "3"
        status = "Warning"
      when "4"
        status = "Unchanged"
      end
    end
    return status
  end

  def portal_check_box_exports2(property,portal_autosubcribes,portal_account,portals_name_id)
    portal = portals_name_id.detect{|a| a.portal_name == portal_account.portal_name}
    portal_id = portal.id
    country = Country.find_by_name(@office.country)
    other_portals = country.id == 12 ? ["Realcommercial.com.au","Realbusinesses.com.au","Domainbusiness.com.au","Realestate1commercial.com.au","Commercialview.com.au","Businessview.com.au","Stayz.com.au","Holidayview.com.au"] : ["Stayz.com.au","Holidayview.com.au"]
    if other_portals.include?(portal_account.portal_name)
      if ["Realcommercial.com.au","Realbusinesses.com.au"].include?(portal_account.portal_name)
        portal = Portal.find(1)
      elsif ["Domainbusiness.com.au","Stayz.com.au"].include?(portal_account.portal_name)
        portal = Portal.find(2)
      elsif portal_account.portal_name == "Realestate1commercial.com.au"
        portal = Portal.find(22)
      elsif ["Commercialview.com.au","Businessview.com.au","Holidayview.com.au"].include?(portal_account.portal_name)
        portal = Portal.find(8)
      end
      portal_autosubcribe = PropertyPortalExport.find_by_property_id_and_portal_id_and_uncheck(property.id,portal.id,false)
      active_feed = PortalAccount.find_by_portal_name_and_office_id(portal.portal_name, @office.id)
      unless active_feed.blank?
        disabled = active_feed.active_feeds != "1" ? 'disabled' : ''
      else
        disabled = 'disabled'
      end
      checked = portal_autosubcribe.blank? ?  "" : "checked"
      disabled = '' if (checked == "checked" && property.status == 6)
      disabled = 'disabled' if (checked == "checked" && property.status != 6) || (checked == "checked" && property.deactivation != true && property.status == 6)
    else
      unless portal_account.blank?
        disabled = portal_account.active_feeds != "1" ? 'disabled' : ''
      else
        disabled = 'disabled'
      end
#      display_name = portal.display_name
      portal_autosubscribe = portal_autosubcribes.detect{|a| a.portal_id == portal_id}
      checked = portal_autosubscribe.blank? ?  "" : "checked"
      disabled = '' if (checked == "checked" && property.status == 6)
      disabled = 'disabled' if (checked == "checked" && property.status != 6) || (checked == "checked" && property.deactivation != true && property.status == 6)
    end
    return "<input type='checkbox' id='ck#{portal_id}' onchange='update_checkbox(this);' name='property_portals[]'value='#{portal_id}' #{checked} #{disabled} />&nbsp;#{portal_account.portal_name}"
  end

  def office_not_subscribe(ns,property_portal_exports)
    if property_portal_exports.present?
      portal_autosubscribe = property_portal_exports.detect{|a| a.portal_id == ns.id}
    end
    checked = portal_autosubscribe.present? ? "checked" : ""
    return "<input type='checkbox' id='ck#{ns.id}' onchange='update_checkbox(this);' name='property_portals[]'value='#{ns.id}' #{checked} disabled />&nbsp;#{ns.portal_name}"
  end

  def send_export_button(portal_account,property,office)
    "<a href='http://commercialloopcrm.com.au/xml_file/xmlexport/send_feed.php?portal=#{portal_account.portal_name}&office=#{office.id}&property=#{property.id}'>Send Export</a>"
  end

end
