module EcampaignHelper
  def ecampagin_links(info_links, web_links, general_links, socials, bgcolor, heading, headingtextcolor, link_order)
    data_info = data_web = data_social = data_general = order = ""
    unless info_links.blank?
      info_links.delete_if{|a| a["title"].blank?}
      info_links.each_with_index{ |info,idx| 
        if idx == 0 
          data_info = "<div style='padding:8px;font-size: 10pt;border-bottom:3px solid #FFFFFF;background-color:#{bgcolor}'><div style='font-size:11pt;font-weight:bold;margin-bottom:5px;color:#{heading}'>Info links</div><div style='margin:10px 0;'>"
        end
        infolink = info["url"].gsub("http://", "")
        infolink = infolink.gsub("https://", "")
        data_info += "<span style='color:#{heading};font-size:12pt;font-weight:bold;'>&gt;</span>&nbsp;<a href='{ecampaign_track_url}&link_url=http://#{infolink}' style='font-weight:bold;text-decoration:none;color:#{headingtextcolor}'>#{info["title"]}</a><br/>" 
      }
      if data_info.present?
        data_info += "</div></div>"
      end
    end
    unless web_links.blank?
      web_links.delete_if{|a| a["title"].blank?}
      web_links.each_with_index{|web,idx|
        if idx == 0 
          data_web = "<div style='padding:8px;font-size: 10pt;border-bottom:3px solid #FFFFFF;background-color:#{bgcolor}'><div style='font-size:11pt;font-weight:bold;margin-bottom:5px;color:#{heading}'>Web links</div><div style='margin:10px 0;'>"
        end
        weblink = web["url"].gsub("http://", "")
        weblink = weblink.gsub("https://", "")
        data_web += "<span style='color:#{heading};font-size:12pt;font-weight:bold;'>&gt;</span>&nbsp;<span style='font-weight:bold;text-decoration:none;color:#{headingtextcolor}'><a href='{ecampaign_track_url}&link_url=http://#{weblink}' style='font-weight:bold;text-decoration:none;color:#{headingtextcolor}'>#{web["title"]}</a></span><br/>"         
      }
      if data_web.present?
        data_web +="</div></div>"
      end
    end
    unless general_links.blank?
      general_links.each do |general|
      title = general['title'].gsub(/\s+/, "")
      link_url = general["url"].gsub(/\s+/, "")
        if !title.blank? && !link_url.blank?
          link_url = link_url.gsub("http://", "")
          link_url = link_url.gsub("https://", "")
          data_general+=" <div style='padding:8px;font-size: 10pt;border-bottom:3px solid #FFFFFF;background-color:#{bgcolor}'>
          <div class='wl-headline' style='color:#{heading};font-weight:bold;font-size: 11pt;'>#{title.capitalize}</div>
          <div class='wl-list' style='color:#{headingtextcolor}'><div>#{general["desc"]}</div>
          <div style='float:right;'><br/>
          <a href='{ecampaign_track_url}&link_url=http://#{link_url}'><img src='#{BASE_URL}/images/click_here.gif'/></a>
          </div><div style='clear:both;'></div></div></div>"
        end
      end
    end
    unless socials.blank?
      data_social="<div style='padding:8px;font-size: 10pt;border-bottom:3px solid #FFFFFF;background-color:#{bgcolor}'>
        <div class='wl-headline' style='color:#{heading};font-weight:bold;margin-bottom:5px;'>Follow us</div>
        <div class='wl-list' style='text-align:center;'>"
      unless socials["facebook_social_link"].blank?
        fblink = socials["facebook_social_link"].gsub("http://", "")
        fblink = fblink.gsub("https://", "")
      end
      unless socials["twitter_social_link"].blank?
        twitlink = socials["twitter_social_link"].gsub("http://", "")
        twitlink = twitlink.gsub("https://", "")
      end
      unless socials["linkedin_social_link"].blank?
        linkedin = socials["linkedin_social_link"].gsub("http://", "")
        linkedin = linkedin.gsub("https://", "")
      end
      unless socials["youtube_social_link"].blank?
        youtubelink = socials["youtube_social_link"].gsub("http://", "")
        youtubelink = youtubelink.gsub("https://", "")
      end
      data_social +="<a href='{ecampaign_track_url}&link_url=http://#{fblink}' style='padding:2px;'><img src='#{BASE_URL}/images/share/facebook.png' width='30' height='30' style='color:transparent !important;'/></a>" unless socials["facebook_social_link"].blank?
      data_social +="<a href='{ecampaign_track_url}&link_url=http://#{twitlink}' style='padding:2px;'><img src='#{BASE_URL}/images/share/twitter.png' width='30' height='30' style='color:transparent !important;' /></a>" unless socials["twitter_social_link"].blank?
      data_social +="<a href='{ecampaign_track_url}&link_url=http://#{linkedin}' style='padding:2px;'><img src='#{BASE_URL}/images/share/linkedin.png' width='30' height='30' style='color:transparent !important;' /></a>" unless socials["linkedin_social_link"].blank?
      data_social +="<a href='{ecampaign_track_url}&link_url=http://#{youtubelink}' style='padding:2px;'><img src='#{BASE_URL}/images/share/youtube.png' width='30' height='30' style='color:transparent !important;' /></a>" unless socials["youtube_social_link"].blank?
      data_social +="<a href='{ecampaign_track_url}&link_url=http://#{socials["rss_social_link"].gsub("http://", "")}' style='padding:2px;'><img src='#{BASE_URL}/images/share/rss.png' width='30' height='30'' style='color:transparent !important;' /></a>" unless socials["rss_social_link"].blank?
      data_social +="</div>
      </div>"
    end

    if link_order.blank?
      order = "#{data_info} #{data_web} #{data_general} #{data_social}"
    else
      link_order.split("#").each{|ord|
         order += data_info if ord == "info"
         order += data_web if ord == "web"
         order += data_general if ord == "general"
         order += data_social if ord == "social"
      }
    end
    return order
  end
  
  def ecampaign_links_table(info_links, web_links, general_links, socials, bgcolor, heading, headingtextcolor, link_order)
    data_info = data_web = data_social = data_general = order = ""
    unless info_links.blank?
      info_links.delete_if{|a| a["title"].blank?}
      info_links.each_with_index{ |info,idx| 
        if idx == 0 
          data_info = "<tr>
                        <td valign=\"top\" style=\"padding:5px 5px;\">
                          <p style=\"color:#{heading}; font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold;margin: 0;\">Info Links</p>
                        </td>
                      </tr>"
        end
        infolink = info["url"].gsub("http://", "")
        infolink = infolink.gsub("https://", "")
        data_info += "<tr>
                      <td valign=\"top\" style=\"padding:5px 5px; border-bottom:3px solid #ffffff;\">
                        <a href=\"{ecampaign_track_url}&link_url=http://#{infolink}\" style=\"text-decoration:none;color:#{headingtextcolor}; font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold\"> #{info["title"]}</a>
                      </td>
                    </tr>"
      }
    end
    unless web_links.blank?
      web_links.delete_if{|a| a["title"].blank?}
      web_links.each_with_index{|web,idx|
        if idx == 0 
         data_web = "<tr>
                      <td valign=\"top\" style=\"padding:5px 5px;\">
                        <p style=\"color:#{heading}; font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold;margin: 0;\">Web Links</p>
                      </td>
                    </tr>"
        end
        weblink = web["url"].gsub("http://", "")
        weblink = weblink.gsub("https://", "")
        data_web += "<tr>
                      <td valign=\"top\" style=\"padding:5px 5px; border-bottom:3px solid #ffffff;\">
                        <a href=\"{ecampaign_track_url}&link_url=http://#{weblink}\" style=\"text-decoration:none;color:#{headingtextcolor}; font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold\"> #{web["title"]}</a>
                      </td>
                    </tr>"
      }
    end
    unless general_links.blank?
      general_links.each do |general|
      if general["url"].present?
        @link = general["url"].gsub(/\s+/, "")
        @link = @link.gsub("http://", "")
        @link = @link.gsub("https://", "")
      else
        @link = ""
      end
      if general["image_id"].present?
        if @link.present?
          general_img = "<tr>
                        <td valign=\"top\" style=\"padding:5px 5px;text-align:center\">
                          <a href=\"{ecampaign_track_url}&link_url=http://#{@link}\">
                           <img src=\"#{free_attachment(general["image_id"])}\" width='190' style='width=190px'/>
                          </a>
                        </td>
                      </tr>"
        else
          general_img = "<tr>
                      <td valign=\"top\" style=\"padding:5px 5px;text-align:center\">
                         <img src=\"#{free_attachment(general["image_id"])}\" width='190' style='width=190px'/>
                      </td>
                    </tr>"
        end
      end
      title = general['title'] #.gsub(/\s+/, "")
      if @link.present?        
        link_url =  "<tr>
                      <td valign=\"top\" style=\"padding:5px 5px; border-bottom:3px solid #ffffff\">
                        <a href=\"{ecampaign_track_url}&link_url=http://#{@link}\" style=\"text-decoration:none;color:#{headingtextcolor}; font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold\">Read more</a>
                      </td>
                    </tr>"
      end
      if !title.blank? 
        data_general+= "<tr>
                        <td valign=\"top\" style=\"padding:5px 5px;\">
                          <p style=\"color:#{heading}; font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold;margin: 0;\">#{title}</p>
                        </td>
                      </tr>
                      #{general_img}
                      <tr>
                        <td valign=\"top\" style=\"padding:5px 5px;\">
                          <p style=\"text-decoration:none;color:#{headingtextcolor}; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:13px;\">
                             #{general["desc"]}
                          </p>
                        </td>
                      </tr>
                      #{link_url}
                      "       
      end
      end
    end
    unless socials.blank?
      filler = 0
      data_social="<tr>
                    <td valign=\"top\" style=\"padding:5px 5px;\">
                      <p style=\"color:#{heading}; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px;margin: 0;\">Follow Us</p>
                    </td>
                  </tr>"
      unless socials["facebook_social_link"].blank?
        fblink = socials["facebook_social_link"].gsub("http://", "")
        fblink = fblink.gsub("https://", "")
      end
      unless socials["twitter_social_link"].blank?
        twitlink = socials["twitter_social_link"].gsub("http://", "")
        twitlink = twitlink.gsub("https://", "")
      end
      unless socials["linkedin_social_link"].blank?
        linkedin = socials["linkedin_social_link"].gsub("http://", "")
        linkedin = linkedin.gsub("https://", "")
      end
      unless socials["youtube_social_link"].blank?
        youtubelink = socials["youtube_social_link"].gsub("http://", "")
        youtubelink = youtubelink.gsub("https://", "")
      end
      data_social +="<tr>
                      <td valign='top' style='padding:5px 5px; border-bottom:3px solid #ffffff;'>
                        <table width='100%' cellpadding='0' cellspacing='0' style='white-space:normal;text-align:left'>
                          <tbody>
                            <tr>
                              "
      unless socials["facebook_social_link"].blank?
      data_social +=          "<td style='text-align:left;width:35px'><a href='{ecampaign_track_url}&link_url=http://#{fblink}' style='padding: 0;display:block;'><img src='#{BASE_URL}/images/share/facebook.png' width='30' height='30' style='display:block'/></a></td>"
      else
        filler = filler + 1
      end
      unless socials["twitter_social_link"].blank?
      data_social +=          "<td style='text-align:left;width:35px'><a href='{ecampaign_track_url}&link_url=http://#{twitlink}' style='padding: 0;display:block;'><img src='#{BASE_URL}/images/share/twitter.png' width='30' height='30' style='display:block'/></a></td>"
      else
        filler = filler + 1
      end
      unless socials["linkedin_social_link"].blank?
      data_social +=          "<td style='text-align:left;width:35px'><a href='{ecampaign_track_url}&link_url=http://#{linkedin}' style='padding: 0;display:block;'><img src='#{BASE_URL}/images/share/linkedin.png' width='30' height='30' style='display:block'/></a></td>"
      else
        filler = filler + 1
      end
      unless socials["youtube_social_link"].blank?
      data_social +=          "<td style='text-align:left;width:35px'><a href='{ecampaign_track_url}&link_url=http://#{youtubelink}' style='padding: 0;display:block;'><img src='#{BASE_URL}/images/share/youtube.png' width='30' height='30' style='display:block'/></a></td>"
      else
        filler = filler + 1
      end
      unless socials["rss_social_link"].blank?
      data_social +=          "<td style='text-align:left;width:35px'><a href='{ecampaign_track_url}&link_url=http://#{socials["rss_social_link"].gsub("http://", "")}' style='padding: 0 ;display:block;'><img src='#{BASE_URL}/images/share/9238927349.png' width='30' height='30' style='display:block'/></a></td>"
      else
        filler = filler + 1
      end
      if filler > 0
        1.upto(filler) do
          data_social += '<td>&nbsp;</td>'
        end
      end
      data_social +=         "
                             </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>"
    end    
    if link_order.blank?
      order = "#{data_info} #{data_web} #{data_general} #{data_social}"
    else
      link_order.split("#").each{|ord|
         order += data_info if ord == "info"
         order += data_web if ord == "web"
         order += data_general if ord == "general"
         order += data_social if ord == "social"
      }
    end
    return order
  end
  

  def view_rates(opened, recipient)
    if !opened.blank? && !recipient.blank?
      if opened == "0" && recipient == "0"
        "0.00"
      else
        "%.2f" % ((opened.to_f/recipient.to_f)*100)
      end
    else
      "0.00"
    end
  end
  
  def ebrochure_sent(eb)
    ebrochure = Ebrochure.find_by_id(eb["id"]) rescue ""
    if ebrochure.present?
      all_sent = ebrochure.is_sent_to_all?
      if ebrochure["sent_at"].blank?
        return link_to "Send Ebrochure", "/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/ebrochures/#{ebrochure["id"]}/view_and_send", :style => "color:blue;"
      elsif ebrochure["sent_at"].present? && !all_sent && !ebrochure.send_process 
        return "#{link_to "Continue send Ebrochure", "/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/ebrochures/#{ebrochure["id"]}/view_and_send", :style => "color:blue;"} &nbsp;|&nbsp; #{link_to "View", "/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/ebrochures/#{ebrochure["id"]}/preview", :style => "color:blue;"}"
  #      return link_to "Continue send Ebrochure", "/agents/#{@agent.id}/offices/#{@office.id}/ebrochures/#{ebrochure["id"]}/continue_send", :style => "color:blue;"
      else
        return "Sent &nbsp;|&nbsp; #{link_to "View", "/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/ebrochures/#{ebrochure["id"]}/preview", :style => "color:blue;", :target => "_blank"}"
      end
    end
  end
  
  def top_headline(property)
    if property.type == 'Commercial'
      sale_lease = property.deal_type == "Both" ? "FOR SALE & LEASE" : "FOR #{property.deal_type.to_s.upcase}"
    elsif property.type == 'NewDevelopment'
      sale_lease = "New Development"
    else
      sale_lease = property.type.to_s.downcase.include?("sale") ? "FOR SALE" : "FOR LEASE" rescue ""
    end
    return sale_lease
  end
  
  def property_url1(url)
    prop_url = url.gsub("http://","")
    prop_url = prop_url.gsub("https://","")
    return prop_url
  end
  
  def carport_garage(property)
    carport = property.detail.respond_to?("carport_spaces") && property.detail.carport_spaces.present? ? property.detail.carport_spaces.to_i : 0
    garage = property.detail.respond_to?("garage_spaces") && property.detail.garage_spaces.present? ? property.detail.garage_spaces.to_i : 0
    return carport + garage
  end
  
  def deal_type_and_price(display_price, property)
    str = ''

    if property.present? && display_price == "true" 
      str += top_headline(property).present? ? "#{top_headline(property).capitalize} - " : ""
      if property.type.to_s == 'Commercial'
        if property.deal_type == 'Lease'
          str += property.display_price.to_i == 1 ? (property.detail.rental_price.blank? ? "" : "$#{property.detail.rental_price} - $#{property.detail.rental_price_to}") : property.display_price_text 
        else
          str += property.display_price.to_i == 1 ? (property.detail.sale_price_from.blank? ? "" : "$#{property.detail.sale_price_from} - $#{property.detail.sale_price_to}") : property.display_price_text
        end
      else
        str += property.display_price.to_i == 1 ? (property.price.blank? ? "" : "$#{property.price} - $#{property.to_price}") : property.display_price_text 
      end 
    end
    return str
  end
  
  def two_col_desc(data)
    #ecampaign theme6
    str = ''
    signature = "<br/><img width='210' src='#{@signature.public_filename}'/><br/> #{data["sign_caption"].gsub("\r\n","<br/>").gsub("\r","<br/>").gsub("\n","<br/>") if data["sign_caption"].present?}" if @signature.present?
    if data["sender_description"].present? && data["sender_description"].length > 300
      data["sender_description"] = data["sender_description"].gsub("\r\n","<br/>")
      data["sender_description"] = data["sender_description"].gsub("\r","<br/>")
      data["sender_description"] = data["sender_description"].gsub("\n","<br/>")
      center = data["sender_description"].length/2
      if data["sender_description"][center] != 32 && data["sender_description"][center+1] != 32
        while data["sender_description"][center] != 32
          center = center + 1
        end
      end
      desc_part1 = data["sender_description"][0..center]
      desc_part2 = data["sender_description"][center+1..data["sender_description"].length]
      
      str += "<tr>
                <td width=\"240\" style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;padding:10px 7px 10px 10px;text-align:justify;vertical-align: top;\">
                  <p style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:0;font-size:12pt;color:#6b6c6e;line-height:19px\">
                    #{desc_part1}
                  </p>
                </td>
                <td width=\"240\" style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;padding:10px 10px 10px 7px;text-align:justify;vertical-align: top;\">
                  <p style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:0;font-size:12pt;color:#6b6c6e;line-height:19px\">
                    #{desc_part2}
                    #{signature}
                  </p>
                </td>
              </tr>"
    elsif data["sender_description"].present? && data["sender_description"].length <= 300
      data["sender_description"] = data["sender_description"].gsub("\r\n","<br/>")
      data["sender_description"] = data["sender_description"].gsub("\r","<br/>")
      data["sender_description"] = data["sender_description"].gsub("\n","<br/>")
      str += "<tr>
                <td width=\"240\" style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;padding:10px 7px 10px 10px;text-align:justify;vertical-align: top;\">
                  <p style=\"margin:0;font-size:12pt;color:#6b6c6e;line-height:19px\">
                    #{data["sender_description"]}
                    #{signature}
                  </p>
                </td>
                <td width=\"240\" style=\"padding:10px 10px 10px 7px;text-align:justify;vertical-align: top;\">
                  <p style=\"margin:0;font-size:10pt;color:#6b6c6e\">
                    &nbsp;
                  </p>
                </td>
              </tr>"
    end
    return str
  end
  
  def did_you_know(data)
    #ecampaign theme6
    str = ''
    if data["you_know_desc"].present? 
      if data["you_know_desc"].length > 200
        data["you_know_desc"] = data["you_know_desc"].gsub("\r\n","<br/>")
        data["you_know_desc"] = data["you_know_desc"].gsub("\r","<br/>")
        data["you_know_desc"] = data["you_know_desc"].gsub("\n","<br/>")
        center = data["you_know_desc"].length/2
        if data["you_know_desc"][center] != 32 && data["you_know_desc"][center+1] != 32
          while data["you_know_desc"][center] != 32
            center = center + 1
          end
        end
        desc_part1 = data["you_know_desc"][0..center]
        desc_part2 = data["you_know_desc"][center+1..data["you_know_desc"].length]
      elsif data["you_know_desc"].length <= 200
        data["you_know_desc"] = data["you_know_desc"].gsub("\r\n","<br/>")
        data["you_know_desc"] = data["you_know_desc"].gsub("\r","<br/>")
        data["you_know_desc"] = data["you_know_desc"].gsub("\n","<br/>")
        desc_part1 = data["you_know_desc"][0..data["you_know_desc"].length]
        desc_part2 = "&nbsp;"
      end
      str ="<tr>
              <td colspan='3' style='padding:10px 15px;'>
                <table>
                  <tbody>
                    <tr>
                      <td valign='top' colspan='2' style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;\">
                        <p style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:0 0 10px 0;font-size:28pt;color:#6b6c6e\">#{data["you_know_title"].present? ? data["you_know_title"] : "Did You Know?"}</p>
                      </td>
                    </tr>
                    <tr>
                      <td valign='top' style=\"padding-right:20px;text-align:justify;vertical-align: top;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;\">
                        <p style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:0;font-size:12pt;color:#6b6c6e\">#{desc_part1}
                        </p>
                      </td>
                      <td valign='top' style=\"text-align:justify;vertical-align: top;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;\">
                        <p style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:0;font-size:12pt;color:#6b6c6e\">#{desc_part2}
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>"
    end
    return str
  end
  
  def this_month(data)
    #ecampaign theme6
    str = ''
    if data["this_month_desc"].present?
      if data["this_month_desc"].length > 200
        data["this_month_desc"] = data["this_month_desc"].gsub("\r\n","<br/>")
        data["this_month_desc"] = data["this_month_desc"].gsub("\r","<br/>")
        data["this_month_desc"] = data["this_month_desc"].gsub("\n","<br/>")
        center = data["this_month_desc"].length/2
        if data["this_month_desc"][center] != 32 && data["this_month_desc"][center+1] != 32
          while data["this_month_desc"][center] != 32
            center = center + 1
          end
        end
        desc_part1 = data["this_month_desc"][0..center]
        desc_part2 = data["this_month_desc"][center+1..data["this_month_desc"].length]
      elsif data["this_month_desc"].length <= 200
        data["this_month_desc"] = data["this_month_desc"].gsub("\r\n","<br/>")
        data["this_month_desc"] = data["this_month_desc"].gsub("\r","<br/>")
        data["this_month_desc"] = data["this_month_desc"].gsub("\n","<br/>")
        desc_part1 = data["this_month_desc"][0..data["this_month_desc"].length]
        desc_part2 = "&nbsp;"
      end
    str ="<tr>
            <td colspan='3' style='border-top:7px solid gray;padding:10px 15px;border-bottom:20px solid #FFE000'>
              <table>
                <tbody>
                  <tr>
                    <td valign='top' colspan='2' style=\"vertical-align: top;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif\">
                      <p style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:0 0 10px 0;font-size:28pt;color:#6b6c6e;line-height:30px;\">#{data["this_month_title"].present? ? data["this_month_title"] : "What's On This Month"}</p>
                    </td>
                  </tr>
                  <tr>
                    <td valign='top' style=\"padding-right:10px;vertical-align: top;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif\">
                      <p style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:0;font-size:12pt;color:#6b6c6e\">#{desc_part1}</p>
                    </td>
                    <td valign='top' style=\"vertical-align: top;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif\">
                      <p style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin:0;font-size:12pt;color:#6b6c6e\">#{desc_part2}</p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>"
    end
    return str
  end
  
  def custom_fb_share(featured_properties,property_checkbox_ids,data)
    #ecampaign theme6
    img = []
    if featured_properties.present?
      begin
        img << featured_properties.first.images.first.public_filename(:thumb)
        img << featured_properties.first.images[1].public_filename(:thumb)
        img << featured_properties.first.images[2].public_filename(:thumb)
      rescue
      end
    end
    if property_checkbox_ids.present?
      property_checkbox_ids.each do |prop|
        begin
          img << prop.images.first.public_filename(:thumb)
        rescue
        end
      end
    end
    str = ""
    if featured_properties.present? && featured_properties.first.images.first.present?
      url = featured_properties.first.images.first.public_filename(:thumb)
    elsif img.present?
      url = img.first
    else
      url = "http://agentpoint.agentaccount.com/images/no_image.jpg"
    end
    str = "&p[images][0]=#{url}"
    href = "http://www.facebook.com/sharer.php?s=100&p[url]={preview}&p[title]=#{CGI.escape(data["sender_title"])}#{str}&p[summary]=#{CGI.escape(truncate(data["sender_description"], 200, "..."))}"
    return href
  end
  
  def header_shortcut(featured_properties,current_listing,open_date_properties,property_soldlease,data,color)
    #ecampaign theme7
    str = ':: Latest update :: - &nbsp;|&nbsp; '
    str += featured_properties.present? ? "<a href='#feat' style='color:#{color};font-size:12px;font-weight:bold;text-decoration:none'>#{data["featured_listing_title"].blank? ? "Feature Property" : data["featured_listing_title"]}</a> &nbsp;|&nbsp; " : ""
    str += current_listing.present? ? "<a href='#current' style='color:#{color};font-size:12px;font-weight:bold;text-decoration:none'>#{data["current_listing_title"].blank? ? "Current Listings" : data["current_listing_title"]}</a> &nbsp;|&nbsp; " : ""
    str += open_date_properties.present? ? "<a href='#open' style='color:#{color};font-size:12px;font-weight:bold;text-decoration:none'>#{data["open_listing_title"].blank? ? "Open for Inspection" : data["open_listing_title"]}</a> &nbsp;|&nbsp; " : ""
    str += property_soldlease.present? ? "<a href='#sold' style='color:#{color};font-size:12px;font-weight:bold;text-decoration:none'>#{data["sold_listing_title"].blank? ? "Sold Listings" : data["sold_listing_title"]}" : ""
    return str
  end
  
  def opentime_date(date)
    #ecampaign theme7
    date = Date.parse(date) rescue ''
    if date.present?
      date = "#{date.strftime("%A, #{date.strftime('%-d').to_i.ordinalize} %B %Y")}"
    end
    return date
  end
  
  def property_url(property)
    prop_url = ""
    if property.detail.respond_to?("property_url")
      if property.detail.property_url.blank?
        if @office.url.present?
          office_url = "http://#{@office.url.gsub("http://", "")}"
          prop_url = "#{office_url}/#{property.id}"
        end
      else
        prop_url = "http://#{property.detail.property_url.gsub("http://", "")}"
      end
      return prop_url
    end
  end
  
  def process_link(url)
    link_url = ""
    if url.present?
      link_url = url.gsub("http://","").gsub("https://","")
      link_url = CGI.escape(link_url)
    end
    return link_url
  end
  
  def display_nearest_opentime(property_checkbox)
    # ecampaign theme7
    opentimes = Opentime.find(:all, :conditions => "property_id = #{property_checkbox.id}", :order => "date ASC")    
    end_week = Date.today.at_end_of_week
    start_week = Date.today.at_beginning_of_week
    opentimes.delete_if{|a| 
      if a.date.present?
        a.date > end_week || a.date < start_week
      end
     }
    str = ""    
    if opentimes.present?
      str += "Opentime: #{opentimes.first.start_time.strftime("%I:%M %P")} to #{opentimes.first.end_time.strftime("%I:%M %P")} #{opentimes.first.date.strftime("%a, %d %b %y")} <br/>"
    end
    return str
  end
  
  def ealert_sent_at_time(ealert_id)
    ealert = Ealert.find(ealert_id)
    sent_at_time(ealert)
  end
  
  def elist_sent_at_time(elist_id)
    elist = Elist.find(elist_id)
    sent_at_time(elist)
  end
  
  def emarketing_sent_at_time(emarketing_id)
    emarketing = Emarketing.find(emarketing_id)
    sent_at_time(emarketing)
  end
  
  def ecampaign_sent_at_time(ecampaign_id)
    ecampaign = Ecampaign.find(ecampaign_id)
    sent_at_time(ecampaign)
  end
  
  def ebrochure_sent_at_time(ecampaign_id)
    ecampaign = Ebrochure.find(ecampaign_id)
    sent_at_time(ecampaign)
  end
  
  def sent_at_time(ecampaign)
    if ecampaign.present? && ecampaign.sent_at.present?
      sent_at_date = ecampaign.sent_at.strftime("%d %B %Y")
      sent_at_time = ecampaign.sent_at.strftime("%H:%M:%S")
      return "#{sent_at_date}<br/>#{sent_at_time}"
    else
      return ""
    end
  end
  
end
