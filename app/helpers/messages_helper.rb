module MessagesHelper
  def unread_messages_icon
    image_tag 'icons/document_types/Message32X32.png', :alt => 'Unread messages'
  end

  def read_messages_icon
    image_tag 'icons/document_types/Message-yellow32X32.png', :alt => 'Read messages'
  end

  def read_n_replied_messages_icon
    image_tag 'icons/document_types/Message-grey32X32.png', :alt => 'Read messages'
  end

  def system_message_categories_select_options
    MessageCategory.system_categories.map{|c| [c.name, c.id]}.unshift(['Please select', nil])
  end

  def developer_message_categories_select_options(developer)
    developer.message_categories.map{|c| [c.name, c.id]}.unshift(['Please select', nil])
  end

  def message_ratings
    [
      [ Message::RatingIncomplete,  'Incomplete'  ],
      [ Message::RatingPoor,        'Poor'        ],
      [ Message::RatingReasonable,  'Reasonable'  ],
      [ Message::RatingGood,        'Good'        ],
      [ Message::RatingExcellent,   'Excellent'   ]
    ].freeze
  end

  def message_rating(rating) 
    message_ratings.find { |r| r[0] == rating }[1]
  end

  def time_ago_in_words_now(from_time, include_seconds = false)
    # for indonesia +7
    distance_of_time_in_words(from_time, Time.now.utc.to_time + 7.hours, include_seconds)
  end

  def time_ago_in_words_zone_now(from_time, include_seconds = false)
    distance_of_time_in_words(from_time, Time.zone.now.utc.to_time + 10.hours, include_seconds)
  end
end
