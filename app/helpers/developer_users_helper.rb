module DeveloperUsersHelper
  def developer_user_access_level_select
    [1, 2].map do |level|
      [DeveloperUser.access(level), level]
    end
  end
end
