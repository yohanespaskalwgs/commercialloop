module BrochuresHelper
  def top_headline(property)
    if property.type == 'Commercial'
      sale_lease = property.deal_type == "Both" ? "FOR SALE & LEASE" : "FOR #{property.deal_type.to_s.upcase}"
    elsif property.type == 'NewDevelopment'
      sale_lease = "New Development"
    else
      sale_lease = property.type.to_s.downcase.include?("sale") ? "FOR SALE" : "FOR LEASE" rescue ""
    end
    return sale_lease
  end
end
