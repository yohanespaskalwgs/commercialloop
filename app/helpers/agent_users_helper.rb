module AgentUsersHelper

  def user_photo(user, type)
    image = user.send type.name.underscore
    path = image.nil? ? image_path('default_avatar.jpg') : image.public_filename
    image_tag path, :width => type.width, :height => type.height, :alt => user.full_name, :class => 'team_photo'
  end

  def check_user_office(office, user)
    if office.agent_users.find_by_id(user.id)
      true
    else
      false
    end
  end
end
