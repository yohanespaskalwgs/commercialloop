# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  #winarti@kiranatama.com
  #Oct 18, 2013
  #active detail class in listing detail tabs in edit property page
  def active_detail_class flag
    if flag
      "current-active-listing-detail-link"
    else
      "current-listing-detail-link"
    end
  end

  #winarti@kiranatama.com
  #Oct 18, 2013
  #active other class in other tabs in edit property page
  def active_other_class flag
    if flag
      "current-active-other-link"
    else
      "current-other-link"
    end
  end

  #rio.dermawan@kiranatama.com
  #Nov 4, 2013
  #active marketing class in marketing tabs in edit property page
  def active_marketing_class flag
    if flag
      "current-active-marketing-link"
    else
      "current-marketing-link"
    end
  end

  def get_opentimes(id)
    @opentimes = Property.find_by_id(id).opentimes
    @opentimes
  end

  def dashboard_placeholder(left, text)
    "<div style='
    z-index: 999;
    height: 70px;
    width: 200px;
    background-color: white;
    border: solid 1px blue;
    color: #000000;
    text-align: center;
    left: #{left}px;
    position: absolute;
    !important;'>#{text}</div>"
  end

  def display_image_path(id = nil)
    unless id.blank?
      #image_holder = Attachment.find(:first, :conditions => "attachable_id=#{id} And type='Image'", :order => "position ASC")
      #image_holder.public_filename unless image_holder.blank?
      property = Property.find(id)
      unless property.blank?
        image_holder = property.images.first
        image_holder.public_filename unless image_holder.blank?
      end
    end
  end

  def nil_on_fail(option = nil)
    yield
  rescue
    option
  end

  def availiability_options
    [['Please Select', nil],
      ['Available', '1'],
      ['Sold', '2'],
      ['Leased', '3'],
      ['Withdrawn', '4'],
      ['Underoffer', '5'],
      ['Draft', '6'],
      ['On Hold', '7']
      ]
  end

  def status_property_residential
    [['Please Select', nil],
      ['For Sale', 'Sale'],
      ['For Lease','Lease']
    ]
  end

  def status_property_commercial
    [['Please Select', nil],
      ['For Sale', 'Sale'],
      ['For Lease','Lease'],
      ['For Sale & Lease','Both']
    ]
  end

  def commercial_type
    [['Please Select', nil],
      ['Building', 'CommercialBuilding'],
      ['Commercial Listing', 'Commercial']
    ]
  end

  def australian_states
    [['Please select', nil],
      ['NT', 'NT'],
      ['SA', 'SA'],
      ['NSW', 'NSW'],
      ['ACT', 'ACT'],
      ['VIC', 'VIC'],
      ['QLD', 'QLD'],
      ['WA', 'WA'],
      ['TAS', 'TAS']
    ]
  end

  def property_options
    [ ['Please select', nil],
      ['Parent :'],
      ['Residential Building', 'new_development'],
      ['Building', 'commercial_building'],
      ['Land Release', 'land_release'],
      ['Child :'],
      ['Residential Sale', 'residential_sale'],
      ['Residential Lease', 'residential_lease'],
      ['Commercial', 'commercial'],
      ['Holiday Lease', 'holiday_lease'],
      ['House & Land', 'project_sale'],
      ['Business Sale', 'business_sale']]
  end

  def property_options_classify
    [ ['Please select', nil],
      ['Parent :'],
      ['Residential Building', 'NewDevelopment'],
      ['Building', 'CommercialBuilding'],
      ['Land Release', 'LandRelease'],
      ['Child :'],
      ['Residential Sale', 'ResidentialSale'],
      ['Residential Lease', 'ResidentialLease'],
      ['Commercial', 'Commercial'],
      ['Holiday Lease', 'HolidayLease'],
      ['House & Land', 'ProjectSale'],
      ['Business Sale', 'BusinessSale']]
  end

  def parent_property_options
    [['Please select', nil],
     ['Residential Building', 'new_development'],
     ['Building', 'commercial_building'],
     ['Land Release', 'land_release']]
  end

  def parent_property_options_classify
    [['Please select', nil],
     ['Residential Building', 'NewDevelopment'],
     ['Building', 'CommercialBuilding'],
     ['Land Release', 'LandRelease']]
  end

  def thickbox_close_button
    link_to_function image_tag('delete.gif'), 'tb_remove();', :class => 'thickbox_close_btn'
  end

  def im_services
    %w( AOL MSN ICQ Yahoo Jabber Skype Google ).map{|s| [s, s]}
  end

  # overwriting rails
  def current_page?(options)
    url_string = CGI.escapeHTML(url_for(options))
    request = @controller.request

    if options.is_a? Hash
      opt_controller = options.delete(:controller)
      opt_action = options.delete(:action)
      opt_controller = [opt_controller] unless opt_controller.is_a? Array
      return true if opt_controller.include?(controller.controller_path) && opt_action.blank?
    end

    # We ignore any extra parameters in the request_uri if the
    # submitted url doesn't have any either.  This lets the function
    # work with things like ?order=asc
    if url_string.index("?")
      request_uri = request.request_uri
    else
      request_uri = request.request_uri.split('?').first
    end
    if url_string =~ /^\w+:\/\//
      url_string == "#{request.protocol}#{request.host_with_port}#{request_uri}"
    else
      url_string == request_uri
    end
  end

  def photo_icon(p)
    p.images.count > 0 ? image_tag("/images/icons/icon_photo.gif") : image_tag("/images/icons/icon_photo_grey.gif")
  end

  def plan_icon(p)
    p.floorplans.count > 0 ? image_tag("/images/icons/icon_plan.gif") : image_tag("/images/icons/icon_plan_grey.gif")
  end

  def mp3_icon(p)
    p.mp3.blank? ? image_tag("/images/icons/icon_mp3_grey.gif") : image_tag("/images/icons/icon_mp3.gif")
  end

  def brochure_icon(p)
    p.brochure.blank? ? image_tag("/images/icons/icon_brochure_grey.gif") : image_tag("/images/icons/icon_brochure.gif")
  end

  def opentime_icon(p)
    p.opentimes.blank? ? image_tag("/images/icons/icon_date_grey.gif") : image_tag("/images/icons/icon_date.gif")
  end

  def video_icon(p)
    p.videos.blank? ? image_tag("/images/icons/icon_video_grey.gif") : image_tag("/images/icons/icon_video.gif")
  end

  def map_icon(p)
    (p.latitude.blank? && p.longitude.blank?) ? image_tag("/images/icons/icon_map_grey.gif") : image_tag("/images/icons/icon_map.gif")
  end

  def status_icon(p)
    image_tag("/images/icons/icon_#{p.status_name.underscore}.gif")
  end

  def translate_url_for_agent_user(text)
    text.gsub("{subdomain}","#{request.subdomains[0]}").gsub("{agent_id}","#{current_user.office.agent_id}").gsub("{developer_id}","#{current_user.developer_id}").gsub("{office_id}","#{current_user.office_id}")
  end

  def translate_url_for_developer_user(text)
    text.gsub("{subdomain}","#{request.subdomains[0]}").gsub("{developer_id}","#{current_user.developer_id}").gsub("{agent_id}","").gsub("{office_id}","")
  end

  def translate_url_for_fb(text,id)
    return 'http://' + text.gsub('http://', '') + "/#{id}"
  end

  def translate_url(text)
    return 'http://' + text.gsub('http://', '')
  end

  def find_category(category)
    @property_types_by_category = PropertyType.find(:all,:conditions=>["category=?",category])
  end

  def user_fullname(id)
    user = User.find_by_id(id)
    unless user.blank?
      return "#{user.first_name} #{user.last_name}"
    else
      return "---"
    end
  end

  def price_per(id)
    case id
    when 0
      "Monthly"
    when 1
      "Weekly"
    when 2
      "Quarterly"
    when 3
      "Yearly"
    end
  end

  def set_status(property)
    return(
      case property
      when "Commercial"
        Property::STATUS[:sold]
      when "CommercialBuilding"
        Property::STATUS[:sold]
      when "ResidentialSale"
        Property::STATUS[:sold]
      when "ResidentialLease"
        Property::STATUS[:leased]
      when "HolidayLease"
        Property::STATUS[:leased]
      when "ProjectSale"
        Property::STATUS[:sold]
      when "BusinessSale"
        Property::STATUS[:sold]
      when "NewDevelopment"
        Property::STATUS[:sold]
      when "LandRelease"
        Property::STATUS[:sold]
      end)
  end

  def stock_image_path(id = nil, parent_id = nil, image_name = nil, type = nil)
    unless image_name.blank?
       at = Attachment.find_by_id(id)
       unless at.blank?
         if type == "true"
           img = at.public_filename
         else
           img = at.public_filename(:thumb)
         end
       end
    end
    return img
  end

  def stock_image_theme4_path(id = nil, parent_id = nil, image_name = nil, type = nil, status = nil, preview = nil)
    if !id.blank?
       at = Attachment.find_by_id(id)
       unless at.blank?
         min_top = 0
         if type == "true"
          img = at.public_filename
           if at.height > 600
             resize_height = ((at.height*158)/at.width).to_i
             min_top = ((resize_height-110)/2).to_i
           end
         else
           img = at.public_filename(:thumb)
           if at.height > 200
             #portrait
             resize_height = ((at.height*158)/at.width).to_i
             min_top = ((resize_height-110)/2).to_i
           end
         end

          add_style = add_style_bg = ""
          if min_top != 0
           add_style = "top:-#{min_top}px;"
           add_style_bg = ""
          end

         if status == "2"
           filename = (preview == true) ? "sold_bg" : "sold_bg_big"
           formated_img = "<div style='position:relative;height: 110px; width: 158px;'>"
           formated_img += "<img width='158' style='margin: 0pt; position: absolute;bottom:0px; left:0;#{add_style}' src='#{img}'>"
           formated_img += "<img width='158' style='position: absolute; left:0; margin: 0pt;#{add_style_bg}' src='#{BASE_URL}/images/#{filename}.png'></div>"
         elsif status == "5"
           filename = (preview == true) ? "under_offer_bg" : "under_offer_bg_big"
           formated_img = "<div style='position:relative;height: 110px; width: 158px;'>"
           formated_img += "<img width='158' style='margin: 0pt; position: absolute;bottom:0px; left:0;#{add_style}' src='#{img}'>"
           formated_img += "<img width='158' style='margin: 0pt; position: absolute; left:0;#{add_style_bg}' src='#{BASE_URL}/images/#{filename}.png'></div>"
         else
           formated_img = "<img src='#{img}' width='158' style='margin:0;position:relative;#{add_style}' />"
         end
       end
    else
      formated_img = "<img src='#{BASE_URL}/images/no_image.jpg' width='158' />"
    end
    return formated_img
  end

  def stock_desc(id = nil)
    p = Property.find_by_id(id)
    if p
      unless p.extra.blank?
        p.extra.description unless p.extra.description.blank?
      end
    end
  end

  def stock_land_metric(metric)
    case metric
    when "Acres"
      "a<br/>"
    when "Hectares"
      "h<br/>"
    when "Square Feet"
      "f2"
    when "Square Metres"
      "m2"
    when "Square Yards"
      "y2"
    else
      "m2"
    end
  end

  def check_opentime(property_id)
    opentime = Opentime.find(:last, :conditions=> "property_id = #{property_id}") unless property_id.blank?
    return opentime.blank? ? false : true
  end

  def display_opentime(property_id, selected_days, type, short = nil)
    day = ""
    unless property_id.blank?
    opentimes = Opentime.find(:all, :conditions=> "property_id = #{property_id}") unless property_id.blank?
    unless opentimes.blank?
      opentimes.each do |op|
        display = false
        if type == "all"
          display = true
        else
          begin_date = Date.today
          end_date = Date.today + type.to_i.day
          display = true if op.date >= begin_date and op.date <= end_date
        end
        if display
          unless selected_days.blank?
            if !short
              unless selected_days.include?("All")
               day += "#{op.date.strftime('%A, %B %d')} - #{op.start_time.strftime('%I:%M%p')} to #{op.end_time.strftime('%I:%M%p')} <br/>" if selected_days.include?(op.date.strftime("%A"))
              else
               day += "#{op.date.strftime('%A, %B %d')} - #{op.start_time.strftime('%I:%M%p')} to #{op.end_time.strftime('%I:%M%p')} <br/>"
              end
            else
              unless selected_days.include?("All")
               day += "#{op.date.strftime('%a, %d %b')}  #{op.start_time.strftime('%I:%M%P')} to #{op.end_time.strftime('%I:%M%P')} <br/>" if selected_days.include?(op.date.strftime("%A"))
              else
               day += "#{op.date.strftime('%a, %d %b')}  #{op.start_time.strftime('%I:%M%P')} to #{op.end_time.strftime('%I:%M%P')} <br/>"
              end
            end
          else
           if !short
             day += "#{op.date.strftime('%A, %B %d')} - #{op.start_time.strftime('%I:%M%p')} to #{op.end_time.strftime('%I:%M%p')} <br/>"
           else
             day += "#{op.date.strftime('%a, %d %b')} #{op.start_time.strftime('%I:%M%P')} to #{op.end_time.strftime('%I:%M%P')} <br/>"
           end
          end
        end
      end
    end
    end
    return day
  end

  def display_opentime_theme4(property_id, selected_days, type, startdate)
    day = ""
    opentimes = Opentime.find(:all, :conditions=> "property_id = #{property_id}")
    unless opentimes.blank?
      opentimes.each do |op|
        display = false
        if type == "all"
          display = true
        else
          start_date = Date.parse(startdate.gsub("/","-")) if startdate.present?
          begin_date = start_date.present? ? start_date: Date.today
          end_date = start_date.present? ? start_date + type.to_i.day : Date.today + type.to_i.day
          display = true if op.date >= begin_date and op.date <= end_date
        end
        if display
          if !op.end_time.blank?
            end_time = "#{op.end_time.strftime('%I').to_i}#{(op.end_time.strftime('%M') != '00' ? ".#{op.end_time.strftime('%M')}" : "")}#{op.end_time.strftime('%p').downcase}"
          else
            end_time = ""
          end

          if !op.start_time.blank?
            start_time = "#{op.start_time.strftime('%I').to_i}#{(op.start_time.strftime('%M') != '00' ? ".#{op.start_time.strftime('%M')}" : "")}"
          else
            start_time = ""
          end

          unless selected_days.blank?
           unless selected_days.include?("All")
             day += "#{op.date.strftime('%a').downcase} #{start_time}-#{end_time} <br/>" if selected_days.include?(op.date.strftime("%A"))
           else
             day += "#{op.date.strftime('%a').downcase} #{start_time}-#{end_time} <br/>"
           end
          else
           day += "#{op.date.strftime('%a').downcase} #{start_time}-#{end_time} <br/>"
          end
        end
      end
    end
    day = "by appointment" if day.blank?
    return day
  end

  def display_opentime_theme5(property_id, selected_days, type)
    day = ""
    opentimes = Opentime.find(:all, :conditions=> "property_id = #{property_id}")
    unless opentimes.blank?
      opentimes.each do |op|
        display = false
        if type == "all"
          display = true
        else
          begin_date = Date.today
          end_date = Date.today + type.to_i.day
          display = true if op.date >= begin_date and op.date <= end_date
        end
        if display
          if !op.end_time.blank?
            end_time = "#{op.end_time.strftime('%I').to_i}#{(op.end_time.strftime('%M') != '00' ? ".#{op.end_time.strftime('%M')}" : "")}#{op.end_time.strftime('%p').downcase}"
          else
            end_time = ""
          end

          if !op.start_time.blank?
            start_time = "#{op.start_time.strftime('%I').to_i}#{(op.start_time.strftime('%M') != '00' ? ".#{op.start_time.strftime('%M')}" : "")}"
          else
            start_time = ""
          end

          unless selected_days.blank?
           unless selected_days.include?("All")
             day += "#{op.date.strftime('%a').downcase} #{start_time}-#{end_time} <br/>" if selected_days.include?(op.date.strftime("%A"))
           else
             day += "#{op.date.strftime('%a').downcase} #{start_time}-#{end_time} <br/>"
           end
          else
           day += "#{op.date.strftime('%a').downcase} #{start_time}-#{end_time} <br/>"
          end
        end
      end
    end
    return day
  end

  def level5_contacts(property_id)
    property = Property.find(property_id)
    contacts = ""
    second = property.secondary_contact
    contacts += "<div class='s5-content-contact'>#{second.first_name.to_s.capitalize} #{second.last_name.to_s.capitalize} #{second.mobile.blank? ? second.mobile.to_s.downcase : second.mobile.to_s.downcase}</div>" unless second.blank?
    third = property.third_contact
    contacts += "<div class='s5-content-contact'>#{third.first_name.to_s.capitalize} #{third.last_name.to_s.capitalize} #{third.mobile.blank? ? third.mobile.to_s.downcase : third.mobile.to_s.downcase}</div>" unless third.blank?
    return contacts
  end

  def display_opentime_ebochures(property_id, selected_days, type)
    day = ""
    opentimes = Opentime.find(:all, :conditions=> "property_id = #{property_id}")
    unless opentimes.blank?
      opentimes.each do |op|
        display = false
        if type == "all"
          display = true
        else
          begin_date = Date.today
          end_date = Date.today + type.to_i.day
          display = true if op.date >= begin_date and op.date <= end_date
        end
        if display
          unless selected_days.blank?
            unless selected_days.include?("All")
             day += "#{op.date.strftime('%a %b %d')} #{op.start_time.strftime('%I:%M%p').downcase} - #{op.end_time.strftime('%I:%M%p').downcase} <br/>" if selected_days.include?(op.date.strftime("%A"))
            else
             day += "#{op.date.strftime('%a %b %d')} #{op.start_time.strftime('%I:%M%p').downcase} - #{op.end_time.strftime('%I:%M%p').downcase} <br/>"
            end
          else
           day += "#{op.date.strftime('%a %b %d')} #{op.start_time.strftime('%I:%M%p').downcase} - #{op.end_time.strftime('%I:%M%p').downcase} <br/>"
          end
        end
      end
    end
    return day
  end

  def pets_allowed(property_id)
    p = Property.find_by_id(property_id)
    if p
      pets = p.features.find(:first, :conditions => "name = 'Pets allowed'")
      return true unless pets.blank?
    end
  end

  def get_developer_subdomain
    if @developer.blank?
      developer_sub = Developer.find_by_entry(request.subdomains[0])
    else
      developer_sub = @developer
    end
    return developer_sub.blank? ? "Administration" : developer_sub.name
  end

  def get_current_subdomain
    return request.subdomains[0]
  end

  def check_layout_subdomain
    layout = ""
    layout = "knightfrank" if request.subdomains[0] == 'knightfrank'
    layout = "jll" if request.subdomains[0] == 'jll'
    layout = "cre" if request.subdomains[0] == 'cre'
    layout = "inno" if request.subdomains[0] == 'portalpushing'
    return layout
  end

  def rent_forsale_stocklist(property)
    if property['type'].to_s == 'ResidentialSale' && property['residential_sale_method_of_sale'] == "Expression of Interest" && !property['residential_sale_closing_date'].blank?
    "<div class='s4-opentime-detail'><div class='boldf'>Expressions of interest</div>
     <div>#{property['residential_sale_closing_date'].to_date.strftime('%d/%m/%Y').downcase}</div>
     </div>"
    else
      if property['type'].to_s == 'Commercial' || property['type'].to_s == 'ResidentialLease' || property['type'].to_s == 'HolidayLease'
        if property['commercial_current_rent']
          if property['deal_type'] == 'Lease'
            '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">rent</div>'
          else
            '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">for Sale</div>'
          end
        else
          '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">rent</div>'
        end
      else
        '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">for sale</div>'
      end
    end
  end

  def rent_forsale_stocklist_theme4(property)
    if property['status'] == "5"
       '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">under contract</div>'
    elsif property['status'] == "2"
       '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">sold</div>'
    else
      if property['type'].to_s == 'ResidentialSale' && property['residential_sale_method_of_sale'] == "Expression of Interest" && !property['residential_sale_closing_date'].blank?
      "<div class='s4-opentime-detail'><div class='boldf'>Expressions of interest</div>
       <div>#{property['residential_sale_closing_date'].to_date.strftime('%d/%m/%Y').downcase}</div>
       </div>"
      else
        if property['type'].to_s == 'Commercial' || property['type'].to_s == 'ResidentialLease' || property['type'].to_s == 'HolidayLease'
          if property['commercial_current_rent']
            if property['deal_type'] == 'Lease'
              '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">rent</div>'
            else
              '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">for Sale</div>'
            end
          else
            '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">rent</div>'
          end
        else
          '<div class="s4-opentime-detail boldf" style="margin-bottom:4px;">for sale</div>'
        end
      end
    end
  end

  def auction_theme4(a_time, a_date)
    if a_date != "0000-00-00"
      unless a_time.blank?
        n_date = a_date.to_date
        arr_t = a_time.split(":")
        if arr_t.blank?
          n_time = Time.mktime(n_date.year,n_date.month,n_date.day,0,0,0)
        else
          n_time = Time.mktime(n_date.year,n_date.month,n_date.day,(arr_t[0].blank? ? 0 : arr_t[0]),(arr_t[1].blank? ? 0 : arr_t[1]),(arr_t[2].blank? ? 0 : arr_t[2]))
        end
        if !n_time.blank?
          end_time = "#{n_time.strftime('%I').to_i}#{(n_time.strftime('%M') != '00' ? ".#{n_time.strftime('%M')}" : "")}#{n_time.strftime('%p').downcase}"
        else
          end_time = ""
        end
        return "<b>#{end_time} #{n_date.strftime('%a').downcase} #{n_date.strftime('%d').to_i} #{n_date.strftime('%b').downcase}</b>"
      end
    end
  end

  def secondary_contact(id)
    p = Property.find_by_id(id)
    sc = p.secondary_contact
    return sc.full_name.to_s.downcase + " " + (sc.mobile.blank? ? sc.phone.to_s.downcase : sc.mobile.to_s.downcase) unless sc.blank?
  end

  def contact_agent(note)
    note.agent_user.full_name
  end

  def get_priority_color(priority)
    color = "#333333"
    color = "#ff65ad" if priority.to_s == "High"
    color = "#ef0000" if priority.to_s == "Critical"

    return color
  end

  def get_estimation_left(message)
    day = hour = min = 0
    unless message.estimated_at.blank?
      total_et = ((message.estimation_day.to_i*1440) + (message.estimation_hour.to_i*60) + message.estimation_min.to_i)
      total_now = ((Time.zone.now.day.to_i*1440) + (Time.zone.now.hour.to_i*60) + Time.zone.now.min.to_i)
      total_estd = ((message.estimated_at.day.to_i*1440) + (message.estimated_at.hour.to_i*60) + message.estimated_at.min.to_i)

      time_used = total_now - total_estd
      total_left = total_et - time_used

      day = total_left/1440
      hour = (total_left%1440)/60
      min = (total_left%1440)%60
    end
    "#{day}d #{hour}h #{min}m"
  end

  def free_attachment(id)
    unless id.blank?
      img = Attachment.find_by_id(id)
      img.public_filename unless img.blank?
    end
  end

  def portal_display_name(portal)
    if portal.display_name.present?
      return portal.display_name
    else
      return portal.portal_name
    end
  end

  def breadcrumb(params, agent, office)
    # OFFICE > TOOLS > ECAMPAIGNS > EPROPERTY UPDATE > CHOOSE LAYOUT
    controllers =  ["ecampaigns","ealerts","ebrochures","elists","emarketings"]
    actions = ["choose_layout","campaign_copy","choose_colour","select_recipient","view_sample","schedule_alert",
               "choose_properties","choose_data","stats"]
    str = ""
    if controllers.include?(params[:controller]) && actions.include?(params[:action])
      if params[:controller] == "ecampaigns"
        controller = "<a href='#{eproperty_update_agent_office_ecampaigns_path(agent,office)}'>EPROPERTY UPDATE</a> >"
      elsif params[:controller] == "ealerts"
        controller = "<a href='#{agent_office_ealerts_path(agent,office)}'>EALERT</a> >"
      elsif params[:controller] == "emarketings"
        controller = "<a href='#{agent_office_emarketings_path(agent,office)}'>EMARKETING</a> >"
      elsif params[:controller] == "elists"
        controller = "<a href='#{agent_office_elists_path(agent,office)}'>ELISTS</a> >"
      elsif params[:controller] == "ebrochures"
        controller = "<a href='#{agent_office_ebrochures_path(agent,office)}'>EBROCHURES</a> >"
      end
      str += "<a href='#{agent_office_path(agent, office)}'>OFFICE</a> >
              <a href='#{agent_office_files_path(agent,office)}'>TOOLS</a> >
              <a href='#{agent_office_ecampaigns_path(agent, office)}'>ECAMPAIGNS</a> >
              #{controller} #{params[:action].humanize.upcase}"
    end
  end

  def search_path(params, agent, office)
    if params[:controller] == 'agent_contacts'
      return search_agent_office_agent_contacts_path(agent, office)
    elsif params[:controller] == 'agent_users'
      return search_agent_office_agent_users_path(agent, office)
    end
  end

  def carport_and_garage(carport,garage)
    str = ''
    carport = carport.present? ? carport.to_i : 0
    garage = garage.present? ? garage.to_i : 0
    parking = carport + garage
    if parking != 0
      str += "<div class='detail-option'><div class='detail-title'>Parking:</div>#{parking}</div>"
    end
    return str
  end

end
