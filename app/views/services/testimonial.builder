require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct!
xml.suburbs do
  xml.pagination("xmlns:xlink" => "http://www.w3.org/1999/xlink" ) do
    xml.tag!(:previous_page, @current_page.to_i - 1) unless @current_page.to_i == 1
    xml.tag!(:current_page, @current_page.to_i)
    xml.tag!(:next_page, @current_page.to_i + 1) unless @current_page.to_i == @total_pages.to_i
    xml.tag!(:per_page, {}, @per_page)
    xml.tag!(:total_entries, {}, @total_entries)
    xml.tag!(:total_pages, {},@total_pages )
  end
  unless @testimonials.blank?
    @testimonials.each{|x|  xml.tag!(:testimonial, "#{x["content"]} By #{x["name"]}")}
  else
    xml.tag!(:name, nil)
  end
end