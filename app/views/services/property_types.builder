require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct!

xml.property_types do
  @ptypes.each do |ptype|
    xml.tag!(:name, ptype)
  end
end