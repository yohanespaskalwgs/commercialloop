require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct!
begin
  xml.agents do
    unless @agents.blank?

      @agents.each do |agent_user|
        xml << agent_user.to_xml(:skip_types => true, :skip_instruct => true, :root => "agent", :only => [:email,:fax,:mobile,:phone], :procs => [Proc.new{|y|
              y[:builder].tag!("id_agent",agent_user.id);
              y[:builder].tag!("name",agent_user.full_name);
              y[:builder].tag!("role",agent_user.role);
              agent_user.description = Iconv.new('UTF-8//IGNORE', 'UTF-8').iconv(agent_user.description) unless agent_user.blank?
              y[:builder].tag!("description",agent_user.description);
              y[:builder].tag!("property_available",agent_user.properties.count(:conditions => " properties.status = 1 OR properties.status = 5 "));
              y[:builder].tag!("property_sold",agent_user.properties.count(:conditions => " properties.status = 2 "));
              y[:builder].tag!("count_testimonial",agent_user.testimonials.length);
              y[:builder].tag!("facebook_username",agent_user.facebook_username);
              y[:builder].tag!("twitter_username",agent_user.twitter_username);
              y[:builder].tag!("linkedin_username",agent_user.linkedin_username);
              y[:builder].tag!("photo",agent_user.portrait_image.blank? ? nil : agent_user.portrait_image.first.public_filename(:thumb));
            }])
      end

    else
      xml.agent do
        xml.tag!(:name, nil)
        xml.tag!(:role, nil)
        xml.tag!(:email, nil)
        xml.tag!(:fax, nil)
        xml.tag!(:mobile, nil)
        xml.tag!(:phone, nil)
        xml.tag!(:id_agent, nil)
      end
    end
  end
rescue
end