require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct!
begin
if @properties.blank?
  xml.properties do
  end
else

  xml.properties do
    xml.pagination("xmlns:xlink" => "http://www.w3.org/1999/xlink" ) do
      xml.tag!(:previous_page, @current_page.to_i - 1) unless @current_page.to_i == 1
      xml.tag!(:current_page, @current_page.to_i)
      xml.tag!(:next_page, @current_page.to_i + 1) unless @current_page.to_i == @total_pages.to_i
      xml.tag!(:per_page, {}, @per_page)
      xml.tag!(:total_entries, {}, @total_entries)
      xml.tag!(:total_pages, {},@total_pages )
    end
    @properties.each do |property|
      xml.property do
        xml.tag!(:id_property, property["id"])
        xml.tag!(:suburb, property["suburb"])
        xml.tag!(:description, property["description"])
        xml.tag!(:headline, property["headline"])
        xml.tag!(:latitude, property["latitude"])
        xml.tag!(:longitude, property["longitude"])
        xml.tag!(:price, property["price"])
        xml.tag!(:status, Property::STATUS_NAME[property["status"].to_i - 1])
        case property["type"].underscore
        when 'business_sale'
          xml.tag!(:auction_place, property["business_sale_auction_place"])
          xml.tag!(:auction_date, "#{property["business_sale_auction_date"]} : #{property["business_sale_auction_time"]}")
        when 'residential_sale'
          xml.tag!(:auction_place, property["residential_sale_auction_place"])
          xml.tag!(:auction_date, "#{property["residential_sale_auction_date"]} : #{property["residential_sale_auction_time"]}")
        when 'commercial'
          xml.tag!(:auction_place, property["commercial_auction_place"])
          xml.tag!(:auction_date, "#{property["commercial_auction_date"]} : #{property["commercial_auction_time"]}")
        end
        case property["type"].underscore
        when 'business_sale'
          xml.tag!(:bedrooms, nil)
          xml.tag!(:bathrooms, nil)
          xml.tag!(:carspaces, nil)
        when 'residential_sale'
          xml.tag!(:bedrooms, property["residential_sale_bedrooms"])
          xml.tag!(:bathrooms, property["residential_sale_bathrooms"])
          xml.tag!(:carspaces, property["residential_sale_carport_spaces"])
        when 'residential_lease'
          xml.tag!(:bedrooms, property["residential_lease_bedrooms"])
          xml.tag!(:bathrooms, property["residential_lease_bathrooms"])
          xml.tag!(:carspaces, property["residential_lease_carport_spaces"])
        when 'commercial'
          xml.tag!(:bedrooms, nil)
          xml.tag!(:bathrooms, nil)
          xml.tag!(:carspaces, nil)
        when 'project_sale'
          xml.tag!(:bedrooms, property["project_sale_bedrooms"])
          xml.tag!(:bathrooms, property["project_sale_bathrooms"])
          xml.tag!(:carspaces, property["project_sale_carport_spaces"])
        when 'holiday_lease'
          xml.tag!(:bedrooms, property["holiday_lease_bedrooms"])
          xml.tag!(:bathrooms, property["holiday_lease_bathrooms"])
          xml.tag!(:carspaces, property["holiday_lease_carport_spaces"])
        end
        xml.tag!(:property_type, property["property_type"])
        xml.tag!(:address, "#{property["unit_number"].blank? ? '' : property["unit_number"] + '/'}#{property["street_number"]} #{property["street"]}")
        xml.tag!(:type, property["type"])
        xml.tag!(:deal_type, property["deal_type"])
        xml.tag!(:sell_type, ["ResidentialLease", "HolidayLease", "Commercial"].include?(property["type"].to_s) ? "Lease" : "Sale")
        xml.tag!(:photo, display_image_path(property["id"]))
        xml.tag!(:updated_at, property["updated_at"])
      end
    end

  end
end
rescue
end