require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct!
if @property.status == 2 or @property.status == 3
  the_detail = @property.detail.attributes
  the_detail.delete_if {|x,y|['auction_date','auction_time'].include?(x)}
else
  the_detail = @property.detail
end
xml.property do
  xml.tag!(:id, @property.id)
  xml.tag!(:unique_id, @property.property_id)
  xml.tag!(:headline, @property.headline)
  xml.tag!(:description, @property.description)
  xml.tag!(:country, @property.country)
  xml.tag!(:deal_type, @property.deal_type)
  xml.tag!(:display_address, @property.display_address)
  xml.tag!(:display_price, @property.display_price)
  xml.tag!(:display_price_text, @property.display_price_text)
  xml.tag!(:latitude, @property.latitude)
  xml.tag!(:longitude, @property.longitude)
  xml.tag!(:price, @property.price)
  xml.tag!(:price_period, "Monthly") if %w( Commercial ResidentialLease ).include?(@property.type)
  xml.tag!(:property_type, @property.property_type)
  xml.tag!(:save_status, @property.save_status)
  xml.tag!(:state, @property.state)
  xml.tag!(:status, @property.status_name)
  xml.tag!(:street_number, @property.street_number)
  xml.tag!(:street, @property.street)
  xml.tag!(:suburb, @property.suburb)
  xml.tag!(:town_village, @property.town_village)
  xml.tag!(:unit_number, @property.unit_number)
  xml.tag!(:vendor_email, @property.vendor_email)
  xml.tag!(:vendor_first_name, @property.vendor_first_name)
  xml.tag!(:vendor_last_name, @property.vendor_last_name)
  xml.tag!(:vendor_phone, @property.vendor_phone)
  xml.tag!(:zipcode, @property.zipcode)
  xml.tag!(:sold_on, @property.sold_on)
  xml.tag!(:sold_price, @property.sold_price)
  xml.tag!(:leased_on, @property.leased_on)
  xml.tag!(:leased_price, @property.leased_price)
  xml.tag!(:agent_id, @property.agent_id)
  xml.tag!(:office_id, @property.office_id)
  xml.tag!(:type, @property.type)
  xml.tag!(:show_price, @property.show_price)
  unless the_detail.blank?
    case @property.type
    when "ResidentialSale"
      xml << the_detail.to_xml(:skip_types => true, :skip_instruct => true, :root => "detail", :procs => [@property.sale_detail_proc])
    when "ProjectSale"
      xml << the_detail.to_xml(:skip_types => true, :skip_instruct => true, :root => "detail", :procs => [@property.project_detail_proc])
    when "HolidayLease"
      xml << the_detail.to_xml(:skip_types => true, :skip_instruct => true, :root => "detail", :procs => [@property.holiday_detail_proc])
      xml << @property.rental_seasons.to_xml(:skip_types => true, :skip_instruct => true, :root => 'rental_seasons', :except => [:id, :holiday_lease_id])
    else
      xml << the_detail.to_xml(:skip_types => true, :skip_instruct => true, :root => "detail", :procs => [@property.lease_detail_proc])
    end
  end
  unless @property.features.blank?
    xml << @property.features.to_xml(:skip_types => true, :skip_instruct => true, :root => "features", :only => [:name])
  end
  unless @property.opentimes.blank?
    xml << @property.opentimes.to_xml(:skip_types => true, :skip_instruct => true, :root => "opentimes", :only => [:date], :methods => [:starttime, :endtime]) unless @property.status == 2 or @property.status == 3
  end
  unless @property.primary_contact.blank?
    xml << @property.primary_contact.to_xml(:skip_types => true, :skip_instruct => true, :root => "primary_contact", :only => [:id, :first_name, :last_name])
  end
  unless @property.secondary_contact.blank?
    xml << @property.secondary_contact.to_xml(:skip_types => true, :skip_instruct => true, :root => "secondary_contact", :only => [:id, :first_name, :last_name]) unless @property.secondary_contact_id.blank?
  end

  xml.photos do
    @property.images.each_with_index do |photo, i|
      xml.photo(:position => i) do
        xml.tag!(:large, photo.public_filename)
        xml.tag!(:medium, photo.public_filename(:medium)) rescue ''
        xml.tag!(:thumb, photo.public_filename(:thumb)) rescue ''
        xml.tag!(:updated_at, photo.updated_at.utc.strftime("%Y-%m-%d %H:%M"))
        xml.tag!(:created_at, photo.created_at.utc.strftime("%Y-%m-%d %H:%M"))
      end
    end
  end

  xml.floorplans do
    @property.floorplans.each_with_index do |floorplan, i|
      xml.floorplan(:position => i) do
        xml.tag!(:large, floorplan.public_filename)
        xml.tag!(:medium, floorplan.public_filename(:medium))
        xml.tag!(:thumb, floorplan.public_filename(:thumb))
        xml.tag!(:updated_at, floorplan.updated_at.utc.strftime("%Y-%m-%d %H:%M"))
        xml.tag!(:created_at, floorplan.created_at.utc.strftime("%Y-%m-%d %H:%M"))
      end
    end
  end

  xml.brochures do
    for i in 0..3 do
      unless @property.brochure[i].blank?
        brochure = {}
        brochure['title'] = @property.brochure[i].blank? ? '' : @property.brochure[i].title
        brochure['public_filename'] = @property.brochure[i].blank? ? '' : @property.brochure[i].public_filename
        xml.brochure(:position => i) do
          xml.tag!(:title, brochure['title'])
          xml.tag!(:large, brochure['public_filename'])
          xml.tag!(:updated_at, brochure['updated_at'].blank? ? nil : brochure['updated_at'].utc.strftime("%Y-%m-%d %H:%M"))
          xml.tag!(:created_at, brochure['created_at'].blank? ? nil : brochure['created_at'].utc.strftime("%Y-%m-%d %H:%M"))
        end
      end
    end
  end

  xml.tag!(:updated_at, @property.updated_at.utc.strftime("%Y-%m-%d %H:%M"))
  xml.tag!(:created_at, @property.created_at.utc.strftime("%Y-%m-%d %H:%M"))
end