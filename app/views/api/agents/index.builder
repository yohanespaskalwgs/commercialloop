require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct! 
xml.agents do
  unless @agents.blank?
    @agents.each do |agent_user|
      xml << agent_user.to_xml(:root =>"agent",:skip_instruct => true,:except => [:crypted_password, :salt, :deleted_at, :remember_token, :remember_token_expires_at, :activation_code])
      xml << agent_user.testimonials.to_xml(:skip_types => true, :skip_instruct => true, :root => "testimonial", :only => [:id, :name,:content])
      unless agent_user.landscape_image.blank?
        xml.landscape_images do
          agent_user.landscape_image.each_with_index do |landscape_image, i|
            xml.landscape_image(:position => i) do
              xml.tag!(:large, agent_user.landscape_image[i].public_filename)
            end
          end
        end
      else
        xml.landscape_images do
        end
      end
      unless agent_user.portrait_image.blank?
        xml.portrait_images do
          agent_user.portrait_image.each_with_index do |portrait_image, i|
            xml.portrait_image(:position => i) do
              xml.tag!(:large, agent_user.portrait_image[i].public_filename)
            end
          end
        end
      else
        xml.portrait_images do
        end
      end

      unless agent_user.vcard.blank?
        xml.vcard do
          xml.tag!(:title, agent_user.vcard.filename)
          xml.tag!(:path, translate_url(request.env["HTTP_HOST"])+agent_user.vcard.public_filename)
        end
      else
        xml.vcard do
        end
      end
    end
  end
end
