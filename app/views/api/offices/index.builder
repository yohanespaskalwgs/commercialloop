require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct!
xml.office do
  xml.tag!("agent-id", @office.agent_id)
  xml.tag!("code", @office.code)
  xml.tag!("country", @office.country)
  xml.tag!("created-at", @office.created_at)
  xml.tag!("deleted-at", @office.deleted_at)
  xml.tag!("developer-contact-id", @office.developer_contact_id)
  xml.tag!("email", @office.email)
  xml.tag!("fax-number", @office.fax_number)
  xml.tag!("head", @office.head)
  xml.tag!("id", @office.id)
  xml.tag!("latitude", @office.latitude)
  xml.tag!("longitude", @office.longitude)
  xml.tag!("name", @office.name)
  xml.tag!("office-contact", @office.office_contact)
  xml.tag!("office-paypal-account", @office.office_paypal_account)
  xml.tag!("phone-number", @office.phone_number)
  xml.tag!("property-draft-for-new", @office.property_draft_for_new)
  xml.tag!("property-draft-for-updated", @office.property_draft_for_updated)
  xml.tag!("property_listing_cost", @office.property_listing_cost)
  xml.tag!("property_owner_access", @office.property_owner_access)
  xml.tag!("property-payment-required", @office.property_payment_required)
  xml.tag!("property-url", @office.property_url)
  xml.tag!("property-with-owner-details", @office.property_with_owner_details)
  xml.tag!("state", @office.state)
  xml.tag!("status", @office.status)
  xml.tag!("street", @office.street)
  xml.tag!("street-number", @office.street_number)
  xml.tag!("street-type", @office.street_type)
  xml.tag!("suburb", @office.suburb)
  xml.tag!("support-contact-id", @office.support_contact_id)
  xml.tag!("unit-number", @office.unit_number)
  xml.tag!("updated-at", @office.updated_at)
  xml.tag!("upgrade-contact-id", @office.upgrade_contact_id)
  xml.tag!("url", @office.url)
  xml.tag!("zipcode", @office.zipcode)
  xml.tag!("logo1", @office.logos.first.blank? ? nil : @office.logos.first.public_filename)
  xml.tag!("logo2", @office.logos.second.blank? ? nil : @office.logos.second.public_filename)
  xml.tag!("notes_for_property", @office.notes_for_property)
  xml.domains do
    if !@office.domains.blank?
      @office.domains.each do |domain|
        xml.tag!(:name, domain.name)
      end
    else
      xml.tag!(:name, '')
    end
  end
end
