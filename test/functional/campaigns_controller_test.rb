require 'test_helper'

class CampaignsControllerTest < ActionController::TestCase
  test "should create campaigns" do
    login_as :alice
    assert_difference('Campaign.count') do
      post :create, :office_id => offices(:domain_office_2), :agent_id => agents(:domain), :campaigns => { }, :property_id => properties(:residential_lease)
    end

  end

  test "should destroy campaigns" do
    login_as :alice
    assert_difference('Campaign.count', -1) do
      delete :destroy, :id => campaigns(:one).id, :office_id => offices(:domain_office_2), :agent_id => agents(:domain)
    end

  end
end
