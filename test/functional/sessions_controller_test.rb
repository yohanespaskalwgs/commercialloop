require File.dirname(__FILE__) + '/../test_helper'
require 'sessions_controller'

# Re-raise errors caught by the controller.
class SessionsController; def rescue_action(e) raise e end; end

class SessionsControllerTest < ActionController::TestCase

  def test_should_login_and_redirect
    @request.host = "aline.whatever.com"
    post :create, :login => 'jan', :password => 'monkey'
    assert session[:user_id]
    assert_response :redirect
  end

  def test_should_fail_login_and_not_redirect
    @request.host = "aline.whatever.com"
    post :create, :login => 'jan', :password => 'bad password'
    assert_nil session[:user_id]
    assert_response :success
  end

  def test_should_logout
    login_as :jan
    get :destroy
    assert_nil session[:user_id]
    assert_response :redirect
  end

  def test_should_remember_me
    @request.host = "aline.whatever.com"
    @request.cookies["auth_token"] = nil
    post :create, :login => 'jan', :password => 'monkey', :remember_me => "1"
    assert_not_nil @response.cookies["auth_token"]
  end

  def test_should_not_remember_me
    @request.cookies["auth_token"] = nil
    post :create, :login => 'jan', :password => 'monkey', :remember_me => "0"
    assert @response.cookies["auth_token"].blank?
  end
  
  def test_should_delete_token_on_logout
    login_as :jan
    get :destroy
    assert @response.cookies["auth_token"].blank?
  end

  def test_should_login_with_cookie
    users(:jan).remember_me
    @request.cookies["auth_token"] = cookie_for(:jan)
    get :new
    assert @controller.send(:logged_in?)
  end

  def test_should_fail_expired_cookie_login
    users(:jan).remember_me
    users(:jan).update_attribute :remember_token_expires_at, 5.minutes.ago
    @request.cookies["auth_token"] = cookie_for(:jan)
    get :new
    assert !@controller.send(:logged_in?)
  end

  def test_should_fail_cookie_login
    users(:jan).remember_me
    @request.cookies["auth_token"] = auth_token('invalid_auth_token')
    get :new
    assert !@controller.send(:logged_in?)
  end

  test "agent user will see dashboard after login" do
    @request.host = "#{developers(:agentsquare).entry}.whatever.com"
    post :create, :login => 'devon', :password => 'monkey'
    assert_redirected_to agent_office_path(agents(:domain),offices(:domain_office_3))
  end

  test "developer user will see developer board after login" do
    @request.host = "#{users(:jan).developer.entry}.whatever.com"
    post :create, :login => 'jan', :password => 'monkey'
    assert_redirected_to :controller => 'developers', :action => 'show', :id => users(:jan).developer.id
  end

  test "admin user will see admin dashboard after login" do
    @request.host = "admin.whatever.com"
    post :create, :login => 'admin', :password => 'monkey'
    assert_redirected_to :controller => 'admin'
  end

  protected
    def auth_token(token)
      CGI::Cookie.new('name' => 'auth_token', 'value' => token)
    end
    
    def cookie_for(user)
      auth_token users(user).remember_token
    end
end
