require 'test_helper'

class AdminUsersControllerTest < ActionController::TestCase
  
  test "should get index" do
    login_as :admin
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_users)
  end

  test "should get new" do
    login_as :admin
    get :new
    assert_response :success
  end

  test "should create admin_user" do
    login_as :admin
    assert_difference('AdminUser.count') do
      post :create, :admin_user => valid_user
    end
    assert_redirected_to admin_user_path(assigns(:admin_user))
  end

  test "failed to create admin" do
    login_as :admin
    assert_no_difference('AdminUser.count') do
      post :create
    end
    assert_response :success
  end

  test "should show admin_user" do
    login_as :admin
    get :show, :id => users(:admin).id
    assert_response :success
  end

  test "should get edit" do
    login_as :admin
    get :edit, :id => users(:admin).id
    assert_response :success
  end

  test "should update admin_user" do
    login_as :admin
    put :update, :id => users(:admin).id, :admin_user => valid_user(:email => 'validagain@vaid.eample.com')
    assert_redirected_to admin_user_path(assigns(:admin_user))
  end

  test "failed to update admin" do
    login_as :admin
    put :update, :id => users(:admin).id, :admin_user => {:password => '123'}
    assert_response :success
  end

  test "should destroy admin_user" do
    login_as :admin
    assert_difference('AdminUser.count', -1) do
      delete :destroy, :id => users(:admin).id
    end

    assert_redirected_to admin_users_path
  end

  test "user can visit admin pages when there's no users in db" do
    login_as :admin
    User.delete_all
    get :index
    assert_response :success
  end

  test "user can't visit admin pages directly" do
    get :index
    assert_redirected_to new_session_url
  end

  test "user who is not AdminUser can't visit admin pages" do
    # jan is developer admin but not AdminUser
    login_as :jan
    get :index
    assert_response 401
  end

end
