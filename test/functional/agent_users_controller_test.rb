require 'test_helper'

class AgentUsersControllerTest < ActionController::TestCase

  test "should require login" do
    get :index
    assert_response :redirect
  end

  test "should get index" do
    login_as :alice
    get :index, :office_id => offices(:domain_office_2).id, :agent_id => agents(:domain).id
    assert_response :success
    assert_not_nil assigns(:agent_users)
  end

  test "should get new" do
    login_as :alice
    get :new, :office_id => offices(:domain_office_2).id, :agent_id => agents(:domain).id
    assert_response :success
  end

  test "should create agent_user" do
    login_as :alice
    assert_difference('AgentUser.count') do
      post :create, :agent_user => valid_user(:office_id => offices(:domain_office_2).id, :phone => '123456') , :office_id => offices(:domain_office_2).id, :agent_id => agents(:domain).id
    end
    assert_response 200
  end

  test "should failed to create agent user" do
    login_as :alice
    assert_no_difference('AgentUser.count') do
      post :create, :agent_user => valid_user(:office_id => offices(:domain_office_2).id, :phone => '') , :office_id => offices(:domain_office_2).id, :agent_id => agents(:domain).id
    end
    assert_response 200
  end

  test "should show agent_user" do
    login_as :alice
    get :show, :id => users(:alice).id, :office_id => offices(:domain_office_2).id, :agent_id => agents(:domain).id
    assert_response :success
  end

  test "should get edit" do
    login_as :alice
    get :edit, :id => users(:alice).id, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    assert_response :success
  end

  test "should update agent_user" do
    login_as :bob
    put :update, :id => users(:bob).id, :agent_user => { }, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    assert_response 200
  end

  test "should update user's images" do
    login_as :bob
    assert_nil users(:bob).landscape_image
    put :update, :id => users(:bob).id, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id, :landscape_image => {:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png')}
    assert_not_nil users(:bob).reload.landscape_image
    assert_response 200
  end

  test "should destroy agent_user" do
    login_as :bob
    assert_difference('AgentUser.count', -1) do
      delete :destroy, :id => users(:alice).id, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    end

    assert_redirected_to agent_office_agent_users_path(agents(:domain),offices(:domain_office_2))
  end

  test "should list users' preferences" do
    login_as :bob
    get :preference, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    assert(assigns(:agent_users).size > 0)
  end

  test "should update users' preferences" do
    login_as :bob
    post :preference, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id, :levels => {users(:alice).id => 1}
    u = assigns(:agent_users).find {|u| u.id == users(:alice).id}
    assert_equal 1, u.access_level
  end

end
