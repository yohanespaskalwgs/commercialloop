require 'test_helper'

class DevelopersControllerTest < ActionController::TestCase

  test "only developer or admin user can visit show" do
    get :show, :id => 1
    assert_response :redirect
    login_as :jan
    get :show, :id => 1
    assert_response :success
  end

#  test "developer user can only visit their own pages" do
#    login_as :berry
#    get :show, :id => 1
#    assert_response 401
#  end

  test "agent user can't visit show" do
    login_as :alice
    get :show, :id => 1
    assert_response 401
  end

  test "should get index" do
    login_as :admin
    get :index
    assert_response :success
    assert_not_nil assigns(:developers)
  end

  test "should get new" do
    login_as :admin
    get :new
    assert_response :success
  end

  test "should create developer" do
    login_as :admin
    assert_difference('Developer.count') do
      post :create, :developer => {:name => 'foo', :company => 'bar', :entry => 'foobar'}, :user => valid_user
    end

    assert_redirected_to developer_path(assigns(:developer))
  end

  test "should show developer" do
    login_as :jan
    get :show, :id => developers(:agentline).id
    assert_response :success
  end

  test "should get edit" do
    login_as :jan
    get :edit, :id => developers(:agentline).id
    assert_response :success
  end

  test "should update developer" do
    login_as :jan
    put :update, :id => developers(:agentline).id, :developer => {:name => 'agentline1' }
    developers(:agentline).reload
    assert_equal 'agentline1', developers(:agentline).name
    assert_equal 'aline', developers(:agentline).entry
    assert_redirected_to developer_path(assigns(:developer))
  end

  test "should destroy developer" do
    login_as :admin
    assert_difference('Developer.count', -1) do
      delete :destroy, :id => developers(:agentline).id
    end

    assert_redirected_to developers_path
  end
end
