require 'test_helper'

class OpentimesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:opentimes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create opentime" do
    assert_difference('Opentime.count') do
      post :create, :opentime => { }
    end

    assert_redirected_to opentime_path(assigns(:opentime))
  end

  test "should show opentime" do
    get :show, :id => opentimes(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => opentimes(:one).id
    assert_response :success
  end

  test "should update opentime" do
    put :update, :id => opentimes(:one).id, :opentime => { }
    assert_redirected_to opentime_path(assigns(:opentime))
  end

  test "should destroy opentime" do
    assert_difference('Opentime.count', -1) do
      delete :destroy, :id => opentimes(:one).id
    end

    assert_redirected_to opentimes_path
  end
end
