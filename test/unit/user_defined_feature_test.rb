# == Schema Info
# Schema version: 20090614051807
#
# Table name: features
#
#  id            :integer(4)      not null, primary key
#  office_id     :integer(4)
#  name          :string(255)
#  property_type :string(255)
#  type          :string(255)

require 'test_helper'

class UserDefinedFeatureTest < ActiveSupport::TestCase

  test "get an user defined feature" do
    assert_equal UserDefinedFeature, Feature.find(features(:multicar).id).class
  end

  test "user defined feature must belong to office" do
    f = features(:electric)
    assert_equal offices(:domain_office_2), f.office
    f.office_id = nil
    assert !f.valid?
  end

end
