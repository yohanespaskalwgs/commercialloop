# == Schema Info
# Schema version: 20090614051807
#
# Table name: project_sale_details
#
#  id                       :integer(4)      not null, primary key
#  project_sale_id          :integer(4)
#  bathrooms                :integer(4)      default(0)
#  bedrooms                 :integer(4)      default(0)
#  carport_spaces           :integer(4)      default(0)
#  category                 :string(255)
#  date_of_completion       :date
#  design_type              :string(255)
#  development_name         :string(255)
#  duplication              :integer(4)
#  energy_efficiency_rating :string(255)
#  energy_star_rating       :string(255)
#  estimate_rental_return   :decimal(16, 3)
#  floor_area               :decimal(16, 3)
#  garage_area              :decimal(16, 3)
#  garage_spaces            :integer(4)      default(0)
#  house_depth              :decimal(16, 3)
#  land_width              :decimal(16, 3)
#  land_area                :decimal(16, 3)
#  latitude                 :decimal(12, 7)
#  longitude                :decimal(12, 7)
#  off_street_spaces        :integer(4)      default(0)
#  porch_terrace_area       :decimal(16, 3)
#  style                    :string(255)
#  virtual_tour             :string(255)

require 'test_helper'

class ProjectSaleDetailTest < ActiveSupport::TestCase

  def setup
    @ps = properties(:project_sale)
    @psd = project_sale_details(:one)
  end

  test "project sale detail is valid" do
    assert @psd.valid?
  end

  test "project sale detail belongs to a project sale property" do
    assert_equal @ps, @psd.project_sale
  end

  test "user must provide valid floor area" do
    valid_area_assertions @psd, :floor_area=, true
  end

  test "user must provide valid land area" do
    valid_area_assertions @psd, :land_area=, true
  end

  test "porch/terrace area must be valid number" do
    valid_price_assertions @psd, :porch_terrace_area=, true
  end

  test "house width must be valid number" do
    valid_price_assertions @psd, :land_width=, true
  end

  test "house depth must be valid number" do
    valid_price_assertions @psd, :land_depth=, true
  end

  test "estimate rental return must be valid number" do
    valid_price_assertions @psd, :estimate_rental_return=, true
  end

  test "user must provide category" do
    @psd.category = ''
    assert !@psd.valid?
  end

  test "bedroom number must be present" do
    @psd.bedrooms = nil
    assert !@psd.valid?
  end

  test "bathroom number must be present" do
    @psd.bathrooms = nil
    assert !@psd.valid?
  end

  test "should formalize prices rate to nightly based" do
    @psd.estimate_rental_return = 100
    @psd.save
    assert_equal 100, @psd.estimate_rental_return.to_i
    @psd.estimate_rental_return_period = 'per week'
    @psd.save
    assert_in_delta @psd.estimate_rental_return.to_f, 100*4.0, 0.001
    @psd.estimate_rental_return = 100
    @psd.estimate_rental_return_period = 'per decade'
    @psd.save
    assert_in_delta @psd.estimate_rental_return.to_f, 100/120.0, 0.001
  end

  test "should validates area metrics only when area is provided" do
    @new = ProjectSaleDetail.new @psd.attributes
    @new.porch_terrace_area = nil
    @new.garage_area = nil
    @new.floor_area = nil
    @new.floor_area_metric = nil
    @new.land_area = nil
    @new.land_area_metric = nil
    assert @new.valid?

    @new.floor_area = 123
    assert !@new.valid?

    @new.floor_area_metric = 'Acres'
    assert @new.valid?
  end

  test "should save standard area in db" do
    @psd.land_area = 100
    @psd.land_area_metric = 'Square Meters'
    @psd.save
    assert_equal 100, @psd.land_area

    @psd.land_area = 100
    @psd.land_area_metric = 'Acres'
    @psd.save
    assert_in_delta 100*4046.85642, @psd.land_area.to_f, 0.0001

    @psd.land_area = 100
    @psd.land_area_metric = 'Hectares'
    @psd.save
    assert_equal 100*10000, @psd.land_area.to_i

    @psd.land_area = 100
    @psd.land_area_metric = 'Square Feets'
    @psd.save
    assert_in_delta 100*0.3048*0.3048, @psd.land_area.to_f, 0.0001

    @psd.land_area = 100
    @psd.land_area_metric = 'Square Yards'
    @psd.save
    assert_equal 100*0.9144*0.9144, @psd.land_area.to_f, 0.0001

    @psd.porch_terrace_area = 100
    @psd.porch_terrace_area_metric = 'Square Yards'
    @psd.save
    assert_equal 100*0.9144*0.9144, @psd.porch_terrace_area.to_f, 0.0001

    @psd.garage_area = 100
    @psd.garage_area_metric = 'Square Yards'
    @psd.save
    assert_equal 100*0.9144*0.9144, @psd.garage_area.to_f, 0.0001
  end

  test "should save standard length in db" do
    @psd.house_depth = 100
    @psd.house_depth_metric = 'Meters'
    @psd.save
    assert_equal 100, @psd.house_depth
    @psd.house_depth = 100
    @psd.house_depth_metric = 'Yards'
    @psd.save
    assert_in_delta 100*0.9144, @psd.house_depth.to_f, 0.0001
    @psd.land_width = 100
    @psd.land_width_metric = 'Feets'
    @psd.save
    assert_in_delta 100*0.3048, @psd.land_width.to_f, 0.0001
  end

  test "should calculate total area" do
    @psd.floor_area = 100
    @psd.floor_area_metric = 'Square Meters'
    @psd.porch_terrace_area = 200
    @psd.porch_terrace_area_metric = 'Square Feets'
    @psd.garage_area = 300
    @psd.garage_area_metric = 'Square Yards'
    total = (300*0.9144*0.9144) + (200*0.3048*0.3048) + 100
    assert_in_delta total, @psd.total_area.to_f, 0.0001
  end

  test "saved detail has default metric" do
    assert_equal 'Square Meters', @psd.land_area_metric
    assert_equal 'Square Meters', @psd.floor_area_metric
  end

end
