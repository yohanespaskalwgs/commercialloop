# == Schema Info
# Schema version: 20090614051807
#
# Table name: business_sale_details
#
#  id                            :integer(4)      not null, primary key
#  business_sale_id              :integer(4)
#  additional_notes              :text
#  annual_ebit                   :decimal(16, 3)
#  annual_gross_profit           :decimal(16, 3)
#  annual_turnover               :decimal(16, 3)
#  approximate_stock_value       :decimal(16, 3)
#  auction_datetime              :datetime
#  auction_place                 :string(255)
#  business_name                 :string(255)
#  category                      :string(255)
#  current_outgoings             :decimal(16, 3)
#  current_outgoings_include_tax :boolean(1)
#  current_rent                  :decimal(16, 3)
#  current_rent_include_tax      :boolean(1)
#  energy_efficiency_rating      :string(255)
#  energy_star_rating            :string(255)
#  floor_area                    :decimal(16, 3)
#  forthcoming_auction           :boolean(1)
#  franchise                     :string(255)
#  franchise_levies              :string(255)
#  franchise_royalties           :string(255)
#  land_area                     :decimal(16, 3)
#  latitude                      :decimal(12, 7)
#  lease_commencement            :date
#  lease_end                     :date
#  lease_option                  :string(255)
#  lease_plus_another            :string(255)
#  longitude                     :decimal(12, 7)
#  outlets                       :string(255)
#  parking_comments              :string(255)
#  parking_spaces                :integer(4)      default(0)
#  patron_capacity               :string(255)
#  premise                       :string(255)
#  price_include_stock           :boolean(1)
#  price_include_tax             :boolean(1)
#  staff_casual                  :integer(4)
#  staff_full_time               :integer(4)
#  staff_part_time               :integer(4)
#  virtual_tour                  :string(255)
#  year_built                    :integer(4)

require 'test_helper'

class BusinessSaleDetailTest < ActiveSupport::TestCase

  def setup
    @bs = properties(:business_sale)
    @bsd = business_sale_details(:one)
  end

  test "business sale detail is valid" do
    assert @bsd.valid?
  end

  test "business sale details belongs to a business sale property" do
    assert_equal @bs, @bsd.business_sale
  end

  test "user must provide category" do
    @bsd.category = ''
    assert !@bsd.valid?
  end

  test "user must provide valid floor area" do
    valid_area_assertions @bsd, :floor_area=, true
  end

  test "user must provide valid land area" do
    valid_area_assertions @bsd, :land_area=, true
  end

  test "user must provide valid build year" do
    valid_year_assertions @bsd, :year_built=, true
  end

  test "annual turnover must be valid number" do
    valid_price_assertions @bsd, :annual_turnover=, true
  end

  test "annual gross profit must be valid number" do
    valid_price_assertions @bsd, :annual_gross_profit=, true
  end

  test "annual EBIT must be valid number" do
    valid_price_assertions @bsd, :annual_ebit=, true
  end

  test "approximate stock value must be valid number" do
    valid_price_assertions @bsd, :approximate_stock_value=, true
  end

  test "current outgoings must be valid number" do
    valid_price_assertions @bsd, :current_outgoings=, true
  end

  test "current rent must be valid number" do
    valid_price_assertions @bsd, :current_rent=, true
  end

  test "casual staff must be number" do
    valid_price_assertions @bsd, :staff_casual=, true, true
  end

  test "part time staff number must be valid" do
    valid_price_assertions @bsd, :staff_part_time=, true, true
  end

  test "full time staff number must be valid" do
    valid_price_assertions @bsd, :staff_full_time=, true, true
  end

  test "should validates area metrics only when area is provided" do
    @new = BusinessSaleDetail.new @bsd.attributes
    @new.floor_area = nil
    @new.floor_area_metric = nil
    @new.land_area = nil
    @new.land_area_metric = nil
    assert @new.valid?

    @new.floor_area = 123
    assert !@new.valid?

    @new.floor_area_metric = 'Acres'
    assert @new.valid?
  end

  test "should save standard area in db" do
    @bsd.land_area = 100
    @bsd.land_area_metric = 'Square Meters'
    @bsd.save
    assert_equal 100, @bsd.land_area

    @bsd.land_area = 100
    @bsd.land_area_metric = 'Acres'
    @bsd.save
    assert_in_delta 100*4046.85642, @bsd.land_area.to_f, 0.0001

    @bsd.land_area = 100
    @bsd.land_area_metric = 'Hectares'
    @bsd.save
    assert_equal 100*10000, @bsd.land_area.to_i

    @bsd.land_area = 100
    @bsd.land_area_metric = 'Square Feets'
    @bsd.save
    assert_in_delta 100*0.3048*0.3048, @bsd.land_area.to_f, 0.0001

    @bsd.land_area = 100
    @bsd.land_area_metric = 'Square Yards'
    @bsd.save
    assert_equal 100*0.9144*0.9144, @bsd.land_area.to_f, 0.0001
  end

  test "saved detail has default metric" do
    assert_equal 'Square Meters', @bsd.land_area_metric
    assert_equal 'Square Meters', @bsd.floor_area_metric
  end

end
