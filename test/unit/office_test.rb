# == Schema Info
# Schema version: 20090614051807
#
# Table name: offices
#
#  id                   :integer(4)      not null, primary key
#  agent_id             :integer(4)
#  developer_contact_id :integer(4)
#  support_contact_id   :integer(4)
#  upgrade_contact_id   :integer(4)
#  code                 :string(255)
#  contact_email        :string(255)
#  contact_first_name   :string(255)
#  contact_last_name    :string(255)
#  country              :string(255)
#  email                :string(255)
#  fax_number           :string(255)
#  head                 :boolean(1)
#  latitude             :string(255)
#  longitude            :string(255)
#  name                 :string(255)
#  phone_number         :string(255)
#  state                :string(255)
#  status               :string(255)
#  street               :string(255)
#  street_number        :string(255)
#  street_type          :string(255)
#  suburb               :string(255)
#  unit_number          :string(255)
#  url                  :string(255)
#  zipcode              :string(255)
#  created_at           :datetime
#  updated_at           :datetime

require 'test_helper'

class OfficeTest < ActiveSupport::TestCase

  def setup
    @new_office = Office.new valid_office(:agent_id => agents(:domain).id, :name => "new office", :code => ("%04d" % agents(:domain).offices.count))
    @new_office2 = Office.new @new_office.attributes
  end

  test "new office is valid" do
    assert @new_office.valid?
  end

  test "office belongs to agent" do
    assert_equal agents(:domain), offices(:domain_office_1).agent
    assert_equal agents(:google), offices(:google_office).agent
  end

  test "office can have many logos" do
    assert offices(:domain_office_1).logos.empty?
    logo = offices(:domain_office_1).logos.build(:order => 1, :uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'))
    logo.dimension = Image::LOGO1
    assert logo.save
    assert_equal 2, offices(:domain_office_1).logos.count
  end

  test "office can have many files" do
    assert_equal 0, offices(:domain_office_2).files.count
    assert_equal 0, users(:alice).files.count
    file = users(:alice).files.build :uploaded_data => fixture_file_upload('shareonbox.png', 'image/png')
    file.category = 'Pictures'
    file.save
    file = users(:alice).files.build :uploaded_data => fixture_file_upload('users.yml', 'text/plain')
    file.category = 'Documents'
    file.save
    assert_equal 2, users(:alice).files.reload.count
    assert_equal 2, offices(:domain_office_2).files.count
  end

  test "office has status" do
    assert_equal 2, Office.active.count
    assert_equal 1, Office.pending.count
    assert_equal 0, Office.inactive.count
    assert_equal 0, Office.suspended.count
  end

  test "office has contact" do
    @new_office.phone_number = '123456'
    assert_equal '123456', @new_office.contact.phone_number
    assert_equal '223-009-9820', offices(:domain_office_1).contact.phone_number
  end

  test "office has a developer contact" do
    assert_equal users(:louies), offices(:domain_office_2).developer_contact
  end

  test "office has a support contact" do
    assert_equal users(:louies), offices(:domain_office_2).developer_contact
  end

  test "office has a upgrade contact" do
    assert_equal users(:louies), offices(:domain_office_2).developer_contact
  end

  test "office has full name" do
    assert_equal 'domain - new office', @new_office.full_name
  end

  test "office code must be 4 digits long" do
    @new_office.code = "123"
    assert (not @new_office.valid?)
    @new_office.code = "12345"
    assert (not @new_office.valid?)
    @new_office.code = "12a3"
    assert (not @new_office.valid?)
    @new_office.code = "1234"
    assert @new_office.valid?
  end

  test "office will auto generate the code" do
    @new_office.code = nil
    assert @new_office.valid?
  end

  test "office code must be unique in the same agent" do
    @new_office.save
    # will auto change office's conflict code when validate
    assert !@new_office2.valid?
    @new_office2.code = nil
    assert @new_office2.valid?
    assert_not_nil @new_office2.code
    assert_not_equal @new_office.code, @new_office2.code
  end

  test "office in different agent can have same code" do
    @o1 = agents(:domain).offices.create valid_office(:name => 'lala', :code => '1111')
    @o2 = agents(:google).offices.create valid_office(:name => 'lala', :code => '1111')
    assert @o1.valid?
    assert @o2.valid?
  end

  test "office code will sequential increase" do
    @new_office.code = nil
    @new_office2.code = nil
    @new_office.save
    @new_office2.save
    assert_equal 1, (@new_office2.code.to_i - @new_office.code.to_i)
  end

  test "office's full code will comprised of developer code, agent code and office code" do
    assert_equal [@new_office.agent.developer.code, @new_office.agent.code, @new_office.code].join('-'), @new_office.full_code
  end

  test "office can has many agent users" do
    assert(offices(:domain_office_2).agent_users.count > 1)
  end

  test "office has many properties" do
    assert(offices(:domain_office_2).properties.count > 1)
  end

  test "office can have many user defined features" do
    assert(offices(:domain_office_2).user_defined_features.count > 1)
  end

  test "should get office statistics" do
    assert_equal [0,0,0,0,0,1], offices(:domain_office_2).stats('ResidentialSale')
    assert_equal [0,0,0,0,0,3], offices(:domain_office_2).stats('Commercial')
  end

end
