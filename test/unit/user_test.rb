# == Schema Info
# Schema version: 20090614051807
#
# Table name: users
#
#  id                        :integer(4)      not null, primary key
#  developer_id              :integer(4)
#  office_id                 :integer(4)
#  activation_code           :string(40)
#  code                      :string(255)
#  crypted_password          :string(40)
#  description               :text
#  email                     :string(100)
#  fax                       :string(255)
#  first_name                :string(50)      default("")
#  im_service                :string(255)
#  im_username               :string(255)
#  last_name                 :string(50)      default("")
#  login                     :string(40)
#  mobile                    :string(255)
#  phone                     :string(255)
#  position                  :integer(4)
#  remember_token            :string(40)
#  salt                      :string(40)
#  type                      :string(255)
#  activated_at              :datetime
#  created_at                :datetime
#  deleted_at                :datetime
#  remember_token_expires_at :datetime
#  updated_at                :datetime

require 'test_helper'

class UserTest < ActiveSupport::TestCase

  # Be sure to include AuthenticatedTestHelper in test/test_helper.rb instead.
  # Then, you can remove it from this and the functional test.
  include AuthenticatedTestHelper

  def setup
    @new_user = User.new valid_user
    @new_user2 = User.new valid_user(:login => 'foo', :email => 'bar@xx.yy')
  end

  test "new user is valid" do
    assert @new_user.valid?
    assert @new_user2.valid?
    assert @new_user.save
    assert @new_user2.save
  end

  test "user must have login" do
    @new_user.login = nil
    assert (!@new_user.valid?)
  end

  test "user login must be unique" do
    @new_user.save
    @new_user2.login = @new_user.login
    assert (!@new_user2.valid?)
  end

  test "user must have email" do
    @new_user.email = nil
    assert (!@new_user.valid?)
  end

  test "user email must be unique" do
    @new_user.save
    @new_user2.email = @new_user.email
    assert (!@new_user2.valid?)
  end

  test "user must have password when create" do
    @new_user.password = nil
    assert !@new_user.valid?
    @new_user.password = @new_user.password_confirmation
    @new_user.password_confirmation = nil
    assert !@new_user.valid?
  end

  test 'email must be valid' do
    @new_user.email = ''
    assert (!@new_user.valid?)
    @new_user.email = 'aa'
    assert (!@new_user.valid?)
    @new_user.email = 'aa.com'
    assert (!@new_user.valid?)
    @new_user.email = 'aa@bb'
    assert (!@new_user.valid?)
    @new_user.email = 'aa~@bb.com'
    assert (!@new_user.valid?)
    @new_user.email = 'jan-xie@rorcraft.cn'
    assert @new_user.valid?
  end

  test 'password should match with its confirmation' do
    @new_user.password_confirmation = 'r0rcraft'
    assert (!@new_user.valid?)
    @new_user.password = 'r0rcraft'
    assert @new_user.valid?
  end

  test "user can have one avatar" do
    assert_nil users(:jan).avatar
    avatar = users(:jan).build_avatar(:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'))
    avatar.dimension = Image::AVATAR
    assert avatar.save
    assert_not_nil users(:jan).avatar
  end

  test "user can have many files" do
    assert_equal 0, users(:jan).files.count
    file = users(:jan).files.build(:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'), :category => 'Pictures')
    assert file.save
    assert_equal 1, users(:jan).files.count

    file = users(:jan).files.build(:uploaded_data => fixture_file_upload('users.yml', 'text/plain'), :category => 'Documents')
    assert file.save
    assert_equal 2, users(:jan).files.count
  end

  test "get disk usage of user files" do
    size1 = File.size File.join(RAILS_ROOT, 'test/fixtures/shareonbox.png')
    size2 = File.size File.join(RAILS_ROOT, 'test/fixtures/users.yml')

    file = users(:jan).files.build(:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'), :category => 'Pictures')
    file.save
    assert_equal size1, users(:jan).disk_usage

    file = users(:jan).files.build(:uploaded_data => fixture_file_upload('users.yml', 'text/plain'), :category => 'Documents')
    file.save
    assert_equal (size1+size2), users(:jan).disk_usage
  end

  test "user can have role" do
    assert users(:jan).has_role?('admin')
    assert users(:admin).has_role?('admin')
  end

  test "user have its class name as default role name" do
    assert users(:jan).has_role?('DeveloperUser')
    assert !users(:jan).has_role?('AdminUser')
    assert users(:admin).has_role?('AdminUser')
    assert users(:devon).has_role?('AgentUser')
  end

  test "AdminUser can pretend as any role" do
    assert users(:admin).has_role?('DeveloperUser')
    assert users(:admin).has_role?('EvenNotExist')
  end

  test "list all admins" do
    assert_equal 6, User.admin.count
  end

  test "toggler admin role" do
    assert users(:rex).has_role?('admin')
    users(:rex).toggle_admin
    assert !users(:rex).has_role?('admin')
    users(:rex).toggle_admin
    assert users(:rex).has_role?('admin')
    assert_not_nil Role.find_by_name('admin')
  end

  #--------------------------------------------------
  #   restful authentication tests
  #-------------------------------------------------- 

  def test_should_create_user
    assert_difference 'User.count' do
      user = create_user
      assert !user.new_record?, "#{user.errors.full_messages.to_sentence}"
    end
  end

  def test_should_initialize_activation_code_upon_creation
    user = create_user
    user.reload
    assert_not_nil user.activation_code
  end

  def test_should_require_login
    assert_no_difference 'User.count' do
      u = create_user(:login => nil)
      assert u.errors.on(:login)
    end
  end

  def test_should_require_password
    assert_no_difference 'User.count' do
      u = create_user(:password => nil)
      assert u.errors.on(:password)
    end
  end

  def test_should_require_password_confirmation
    assert_no_difference 'User.count' do
      u = create_user(:password_confirmation => nil)
      assert u.errors.on(:password_confirmation)
    end
  end

  def test_should_require_email
    assert_no_difference 'User.count' do
      u = create_user(:email => nil)
      assert u.errors.on(:email)
    end
  end

  def test_should_reset_password
    users(:peter).update_attributes(:password => 'new password', :password_confirmation => 'new password')
    assert_equal users(:peter), User.authenticate('peter', 'new password')
  end

  def test_should_not_rehash_password
    users(:peter).update_attributes(:login => 'quentin2')
    assert_equal users(:peter), User.authenticate('quentin2', 'monkey')
  end

  def test_should_authenticate_user
    assert_equal users(:jan), User.authenticate('jan', 'monkey')
    assert_equal users(:rex), User.authenticate('rex', 'monkey')
    assert_equal users(:alice), User.authenticate('alice', 'monkey')
    assert_equal users(:bob), User.authenticate('bob', 'monkey')
  end

  def test_should_set_remember_token
    users(:alice).remember_me
    assert_not_nil users(:alice).remember_token
    assert_not_nil users(:alice).remember_token_expires_at
  end

  def test_should_unset_remember_token
    users(:alice).remember_me
    assert_not_nil users(:alice).remember_token
    users(:alice).forget_me
    assert_nil users(:alice).remember_token
  end

  def test_should_remember_me_for_one_week
    before = 1.week.from_now.utc
    users(:alice).remember_me_for 1.week
    after = 1.week.from_now.utc
    assert_not_nil users(:alice).remember_token
    assert_not_nil users(:alice).remember_token_expires_at
    assert users(:alice).remember_token_expires_at.between?(before, after)
  end

  def test_should_remember_me_until_one_week
    time = 1.week.from_now.utc
    users(:alice).remember_me_until time
    assert_not_nil users(:alice).remember_token
    assert_not_nil users(:alice).remember_token_expires_at
    assert_equal users(:alice).remember_token_expires_at, time
  end

  def test_should_remember_me_default_two_weeks
    before = 2.weeks.from_now.utc
    users(:alice).remember_me
    after = 2.weeks.from_now.utc
    assert_not_nil users(:alice).remember_token
    assert_not_nil users(:alice).remember_token_expires_at
    assert users(:alice).remember_token_expires_at.between?(before, after)
  end

  protected

  def create_user(options={})
    record = User.new valid_user(options)
    record.save
    record
  end

end
