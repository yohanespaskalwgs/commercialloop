# == Schema Info
# Schema version: 20090614051807
#
# Table name: features
#
#  id            :integer(4)      not null, primary key
#  office_id     :integer(4)
#  name          :string(255)
#  property_type :string(255)
#  type          :string(255)

require 'test_helper'

class FeatureTest < ActiveSupport::TestCase

  test "feature must have a name" do
    assert !InternalFeature.new(:property_type => 'Commercial').valid?
  end

  test "feature must have a property type" do
    assert !InternalFeature.new(:name => 'BBQ').valid?
  end

  test "feature name is unique in a feature category" do
    same_name = features(:bath).name
    assert_equal false, InternalFeature.new(:name => same_name, :property_type => 'ResidentialSale').valid?
    assert_equal false, InternalFeature.new(:name => same_name.upcase, :property_type => 'ResidentialSale').valid?
    assert_equal true, InternalFeature.new(:name => same_name, :property_type => 'Commercial').valid?
    assert_equal true, ExternalFeature.new(:name => same_name, :property_type => 'ResidentialSale').valid?
  end

  test "feature belongs to many properties" do
    assert(features(:bath).properties.count > 1)
  end

  test "get external features" do
    assert_equal ExternalFeature.count, Feature.external.count
  end

  test "get internal features" do
    assert_equal InternalFeature.count, Feature.internal.count
  end

  test "get location features" do
    assert_equal LocationFeature.count, Feature.location.count
  end

  test "get security features" do
    assert_equal SecurityFeature.count, Feature.security.count
  end

  test "get lifestyle features" do
    assert_equal LifestyleFeature.count, Feature.lifestyle.count
  end

  test "get general features" do
    assert_equal GeneralFeature.count, Feature.general.count
  end

  test "get user defined features" do
    assert_equal UserDefinedFeature.count, Feature.user_defined.count
  end

  test "get residential lease features" do
    assert_equal 1, Feature.residential_lease.count
  end

  test "get residential sale features" do
    assert_equal 1, Feature.residential_sale.count
  end

  test "get holiday lease features" do
    assert_equal 1, Feature.holiday_lease.count
  end

  test "get project sale features" do
    assert_equal 1, Feature.project_sale.count
  end

  test "get business sale features" do
    assert_equal 1, Feature.business_sale.count
  end

  test "get commercial features" do
    assert(Feature.commercial.count > 1)
  end

  test "get commercial's security features" do
    assert_equal SecurityFeature.commercial.count, Feature.security.commercial.count
  end

end
