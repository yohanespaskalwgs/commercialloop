# == Schema Info
# Schema version: 20090614051807
#
# Table name: message_topics
#
#  id          :integer(4)      not null, primary key
#  category_id :integer(4)
#  name        :string(255)
#  created_at  :datetime
#  updated_at  :datetime

require 'test_helper'

class MessageTopicTest < ActiveSupport::TestCase

  test "message topic should have a name" do
    t = MessageTopic.new
    assert !t.valid?
  end
  
  test "message topic should have a unique name" do
    t = MessageTopic.new(:name => 'Iamunique')
    assert t.valid?
    t.name = message_topics(:systems).name
    assert !t.valid?
  end

end
