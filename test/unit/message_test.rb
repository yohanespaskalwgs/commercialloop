# == Schema Info
# Schema version: 20090614051807
#
# Table name: messages
#
#  id                       :integer(4)      not null, primary key
#  category_id              :integer(4)
#  office_id                :integer(4)
#  sender_id                :integer(4)
#  topic_id                 :integer(4)
#  admin_unread             :boolean(1)      default(TRUE)
#  archive_state            :integer(4)      default(0)
#  message                  :text
#  message_comments_count   :integer(4)      default(0)
#  message_recipients_count :integer(4)      default(0)
#  pending_archive          :boolean(1)
#  rating                   :integer(4)      default(0)
#  title                    :string(255)
#  type                     :string(255)
#  created_at               :datetime
#  updated_at               :datetime

require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  
  test "AdminMessage should have a topic" do
    msg = AdminMessage.new(:sender_id => users(:jan).id, :title => 'hello admin', :message => 'just say hi')
    assert !msg.valid?
    assert_not_nil msg.errors.on(:topic)
  end
  
end
