# == Schema Info
# Schema version: 20090614051807
#
# Table name: users
#
#  id                        :integer(4)      not null, primary key
#  developer_id              :integer(4)
#  office_id                 :integer(4)
#  activation_code           :string(40)
#  code                      :string(255)
#  crypted_password          :string(40)
#  description               :text
#  email                     :string(100)
#  fax                       :string(255)
#  first_name                :string(50)      default("")
#  im_service                :string(255)
#  im_username               :string(255)
#  last_name                 :string(50)      default("")
#  login                     :string(40)
#  mobile                    :string(255)
#  phone                     :string(255)
#  position                  :integer(4)
#  remember_token            :string(40)
#  salt                      :string(40)
#  type                      :string(255)
#  activated_at              :datetime
#  created_at                :datetime
#  deleted_at                :datetime
#  remember_token_expires_at :datetime
#  updated_at                :datetime

require 'test_helper'

class DeveloperUserTest < ActiveSupport::TestCase

  def setup
    @du = DeveloperUser.create valid_user
  end

  test "read developer from database" do
    @user = User.find_by_login @du.login
    assert_equal DeveloperUser, @user.class
  end

  test "developer must have first name" do
    @du.first_name = ''
    assert !@du.valid?
  end

  test "developer must have last name" do
    @du.last_name = ''
    assert !@du.valid?
  end

  test "developer must have phone number" do
    @du.phone = ''
    assert !@du.valid?
  end

end
