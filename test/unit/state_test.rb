# == Schema Info
# Schema version: 20090614051807
#
# Table name: states
#
#  id         :integer(4)      not null, primary key
#  country_id :integer(4)
#  name       :string(255)

require 'test_helper'

class StateTest < ActiveSupport::TestCase

  def setup
    @zj = states(:zj)
    @jx = states(:jx)
  end

  test "state is valid" do
    assert @zj.valid?
    assert @jx.valid?
  end

  test "state must belong to a country" do
    @zj.country_id = nil
    assert !@zj.valid?
    assert_equal countries(:cn), @jx.country
  end

  test "state must have a name" do
    @jx.name = ''
    assert !@jx.valid?
  end

  test "state may have states" do
    assert(@zj.suburbs.count > 1)
  end

end
