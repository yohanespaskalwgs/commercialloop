# == Schema Info
# Schema version: 20090614051807
#
# Table name: message_categories
#
#  id           :integer(4)      not null, primary key
#  developer_id :integer(4)
#  name         :string(255)
#  created_at   :datetime
#  updated_at   :datetime

require 'test_helper'

class MessageCategoryTest < ActiveSupport::TestCase
  test "message category should have a name" do
    cate = MessageCategory.new
    assert !cate.valid?
  end
  
  test "message categories should have uniq name in with the scope of develper" do
    cate = MessageCategory.new(:name => 'Systems')
    assert !cate.valid?
    cate.name = "AgentTwo"
    cate.developer_id = 2
    assert !cate.valid?
    cate.name = "AnyOther"
    assert cate.valid?
  end
  
  test "system category should not have a developer" do
    cate = message_categories(:systems)
    assert cate.system_category?
    assert_nil cate.developer
  end
  
  test "category belongs to a developer should not be a system category" do
    cate = developers(:agentline).message_categories.build(:name => "agent_category")
    assert !cate.system_category?
    
    cate = message_categories(:agent1)
    assert !MessageCategory.system_categories.include?(cate)
  end
  
  test "system categories should contain a category withoug a developer" do
    cate = message_categories(:systems)
    assert MessageCategory.system_categories.include?(cate)
  end
end
