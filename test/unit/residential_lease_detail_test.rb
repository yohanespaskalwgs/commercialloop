# == Schema Info
# Schema version: 20090614051807
#
# Table name: residential_lease_details
#
#  id                       :integer(4)      not null, primary key
#  residential_lease_id     :integer(4)
#  bathrooms                :integer(4)      default(0)
#  bedrooms                 :integer(4)      default(0)
#  bond                     :decimal(16, 3)
#  carport_spaces           :integer(4)      default(0)
#  date_available           :date
#  energy_efficiency_rating :integer(4)
#  energy_star_rating       :integer(4)
#  floor_area               :decimal(16, 3)
#  garage_spaces            :integer(4)      default(0)
#  land_area                :decimal(16, 3)
#  latitude                 :decimal(12, 7)
#  longitude                :decimal(12, 7)
#  number_of_floors         :integer(4)
#  off_street_spaces        :integer(4)      default(0)
#  virtual_tour             :string(255)
#  year_built               :integer(4)

require 'test_helper'

class ResidentialLeaseDetailTest < ActiveSupport::TestCase
  
  def setup
    @rl = properties(:residential_lease)
    @rld = residential_lease_details(:one)
    @rld.land_area_metric = 'Square Meters'
    @rld.floor_area_metric = 'Square Meters'
  end

  test "residential lease detail is valid" do
    assert @rld.valid?
  end

  test "resident lease details belongs to a resident lease property" do
    assert_equal @rl, @rld.residential_lease
  end

  test "user must provide number of floors" do
    valid_positive_integer_assertions @rld, :number_of_floors=, true
  end

  test "user must provide valid floor area" do
    valid_area_assertions @rld, :floor_area=, true
  end

  test "user must provide valid land area" do
    valid_area_assertions @rld, :land_area=, true
  end

  test "user must provide valid build year" do
    valid_year_assertions @rld, :year_built=, true
  end

  test "bond must be valid number" do
    valid_price_assertions @rld, :bond=, true
  end

  test "user must provide available date" do
    @rld.date_available = ''
    assert !@rld.valid?
  end

  test "user must provide number of rooms" do
    @rld.bedrooms = nil 
    assert !@rld.valid?
    @rld.bedrooms = 0
    @rld.bathrooms = nil
    assert !@rld.valid?
  end

  test "should validates area metrics only when area is provided" do
    @new = ResidentialLeaseDetail.new @rld.attributes
    @new.floor_area = nil
    @new.floor_area_metric = nil
    @new.land_area = nil
    @new.land_area_metric = nil
    assert @new.valid?

    @new.floor_area = 123
    assert !@new.valid?

    @new.floor_area_metric = 'Acres'
    assert @new.valid?
  end

  test "should save standard area in db" do
    @rld.land_area = 100
    @rld.land_area_metric = 'Square Meters'
    @rld.save
    assert_equal 100, @rld.land_area

    @rld.land_area = 100
    @rld.land_area_metric = 'Acres'
    @rld.save
    assert_in_delta 100*4046.85642, @rld.land_area.to_f, 0.0001

    @rld.land_area = 100
    @rld.land_area_metric = 'Hectares'
    @rld.save
    assert_equal 100*10000, @rld.land_area.to_i

    @rld.land_area = 100
    @rld.land_area_metric = 'Square Feets'
    @rld.save
    assert_in_delta 100*0.3048*0.3048, @rld.land_area.to_f, 0.0001

    @rld.land_area = 100
    @rld.land_area_metric = 'Square Yards'
    @rld.save
    assert_equal 100*0.9144*0.9144, @rld.land_area.to_f, 0.0001
  end

  test "saved detail has default metric" do
    assert_equal 'Square Meters', @rld.land_area_metric
    assert_equal 'Square Meters', @rld.floor_area_metric
  end

end
