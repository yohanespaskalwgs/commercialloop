# == Schema Info
# Schema version: 20090614051807
#
# Table name: user_file_categories
#
#  id        :integer(4)      not null, primary key
#  office_id :integer(4)
#  name      :string(255)

require 'test_helper'

class UserFileCategoryTest < ActiveSupport::TestCase

  test "name should be unique in a office" do
    category = offices(:domain_office_2).user_file_categories.build :name => user_file_categories(:one).name
    assert !category.valid?
  end

  test "name can be the same in different offices" do
    category = offices(:domain_office_1).user_file_categories.build :name => user_file_categories(:one).name
    assert category.valid?
  end

end
