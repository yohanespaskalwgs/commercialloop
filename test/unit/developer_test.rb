# == Schema Info
# Schema version: 20090614051807
#
# Table name: developers
#
#  id            :integer(4)      not null, primary key
#  code          :string(255)
#  company       :string(255)
#  country       :string(255)
#  description   :text
#  domain        :string(255)
#  email         :string(255)
#  entry         :string(255)
#  fax_number    :string(255)
#  name          :string(255)
#  phone_number  :string(255)
#  state         :string(255)
#  status        :string(255)     default("active")
#  street        :string(255)
#  street_number :string(255)
#  street_type   :string(255)
#  suburb        :string(255)
#  time_zone     :string(255)
#  unit_number   :string(255)
#  website       :string(255)
#  created_at    :datetime
#  deleted_at    :datetime
#  updated_at    :datetime

require 'test_helper'

class DeveloperTest < ActiveSupport::TestCase

  def setup
    @new_developer = Developer.new :name => 'realestate', :company => 'real estate company', :code => 'au57865', :entry => 'real'
  end

  test "the new developer is valid" do
    assert @new_developer.valid?
  end

  test "developer will auto generate a code before save" do
    @new_developer.code = nil
    assert @new_developer.valid?
    assert_match /au\d{5}/, @new_developer.code
  end

  test "developer's full code is its code self" do
    assert_equal @new_developer.code, @new_developer.full_code
  end

  test "developer will re-generate the code when the code generated conflict with other developer" do
    @new_developer.save
    @new_developer2 = Developer.new @new_developer.attributes
    @new_developer2.entry = @new_developer2.entry + "1"
    @new_developer2.name = "Nobody else"
    @new_developer2.code = nil
    $new_developer_code = @new_developer.code
    def @new_developer2.populate_code
      @@count ||= 0
      if @@count == 0
        # conflict at the first time
        @@count += 1
        self.code = $new_developer_code
      else
        super
      end
    end
    assert @new_developer2.valid?
  end

  test "developer code have to be unique" do
    @new_developer.save
    @new_developer2 = Developer.new @new_developer.attributes
    assert (not @new_developer2.valid?)
  end

  test "developer code must be in certain format" do
    @new_developer.code = "aa1243"
    assert (not @new_developer.valid?)
    @new_developer.code = "a12345"
    assert (not @new_developer.valid?)
  end

  test "entry string can only contains alphabeta, numbers and slash" do
    @new_developer.entry = "a.b"
    assert (not @new_developer.valid?)
    @new_developer.entry = "sldfkj_dsfb"
    assert (not @new_developer.valid?)
    @new_developer.entry = "ok"
    assert (not @new_developer.valid?)
    @new_developer.entry = "-ok"
    assert (not @new_developer.valid?)
    @new_developer.entry = "o-k"
    assert @new_developer.valid?
    @new_developer.entry = "asdf-bcmd"
    assert @new_developer.valid?
    @new_developer.entry = "dmd"
    assert @new_developer.valid?
    @new_developer.entry = "s3s"
    assert @new_developer.valid?
  end
  
  test "entry should not user reserved subdomains" do
    @new_developer.entry = "admin"
    assert (not @new_developer.valid?)
    @new_developer.entry = "www"
    assert (not @new_developer.valid?)
  end
  
  test "entry should be unique" do
    @new_developer.entry = "aline"
    assert (not @new_developer.valid?)
  end

  test "developer has many developer_users" do
    assert(developers(:agentline).developer_users.count > 1)
  end

  test "developer has a admin" do
    assert developers(:agentline).developer_users.admin.include?(users(:rex))
    assert developers(:agentsquare).developer_users.admin.include?(users(:louies))
    assert developers(:agentcircle).developer_users.admin.include?(users(:berry))
  end

  test "developer can have many agents" do
    assert_equal 2, developers(:agentsquare).agents.count
  end

end
