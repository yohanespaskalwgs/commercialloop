# == Schema Info
# Schema version: 20090614051807
#
# Table name: users
#
#  id                        :integer(4)      not null, primary key
#  developer_id              :integer(4)
#  office_id                 :integer(4)
#  activation_code           :string(40)
#  code                      :string(255)
#  crypted_password          :string(40)
#  description               :text
#  email                     :string(100)
#  fax                       :string(255)
#  first_name                :string(50)      default("")
#  im_service                :string(255)
#  im_username               :string(255)
#  last_name                 :string(50)      default("")
#  login                     :string(40)
#  mobile                    :string(255)
#  phone                     :string(255)
#  position                  :integer(4)
#  remember_token            :string(40)
#  salt                      :string(40)
#  type                      :string(255)
#  activated_at              :datetime
#  created_at                :datetime
#  deleted_at                :datetime
#  remember_token_expires_at :datetime
#  updated_at                :datetime

require 'test_helper'

class AgentUserTest < ActiveSupport::TestCase

  def setup
    @au = AgentUser.new valid_user(:phone => '123-456-789', :office_id => offices(:domain_office_2).id, :code => ("%03d" % offices(:domain_office_2).agent_users.count))
  end

  test "agent user is valid" do
    assert @au.valid?
  end
  
  test "read admin from database" do
    assert @au.save
    @user = User.find_by_login @au.login
    assert_equal AgentUser, @user.class
  end

  test "agent user will belong to an office" do
    assert_equal offices(:domain_office_2), @au.office
    assert_equal offices(:domain_office_2), users(:alice).office
    assert_equal offices(:domain_office_3), users(:devon).office
  end

  test "agent user belongs to agent" do
    assert_equal agents(:domain), users(:alice).agent
  end

  test "agent user login will have agent name as scope" do
    @au.save
    assert_equal 'jan1', @au.reload.pretty_login
  end

  # test "agent user save real login = agent name + login in db" do
  #   @au.login = "JAN"
  #   @au.save
  #   @au.reload
  #   assert_equal "#{@au.pretty_login}@@@#{@au.agent.name}".downcase, @au.login
  # end

  test "agent user can have a landscape image" do
    assert_nil users(:alice).landscape_image
    assert users(:alice).create_landscape_image(:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'))
    assert_not_nil users(:alice).landscape_image
  end

  test "agent user can have a portrait image" do
    assert_nil users(:alice).portrait_image
    assert users(:alice).create_portrait_image(:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'))
    assert_not_nil users(:alice).portrait_image
  end

  test "agent user must have phone number" do
    @au.phone = nil
    assert !@au.valid?
  end

  test "agent user must have first name" do
    @au.first_name = nil
    assert !@au.valid?
  end

  test "agent user must have last name" do
    @au.last_name = nil
    assert !@au.valid?
  end

  test "agent user code must be 3 digits long" do
    @au.code = "12"
    assert (not @au.valid?)
    @au.code = "1234"
    assert (not @au.valid?)
    @au.code = "1a3"
    assert (not @au.valid?)
    @au.code = "123"
    assert @au.valid?
  end

  test "agent user will auto generate the sequential code" do
    @au.code = nil
    @au.valid?
    assert @au.valid?
  end
  
  test "agent user auto generated code will sequential increase" do
    @au2 = AgentUser.new :login => 'au2', :email => 'au2@aa.xx.zz', :password => 'au2au2', :password_confirmation => 'au2au2', :office_id => @au.office_id, :phone => '123456', :first_name => 'aaa', :last_name => 'bbb'
    @au3 = AgentUser.new :login => 'au3', :email => 'au3@aa.xx.zz', :password => 'au3au3', :password_confirmation => 'au3au3', :office_id => @au.office_id, :phone => '123456', :first_name => 'aaa', :last_name => 'bbb'
    assert @au2.save
    assert @au3.save
    assert_equal 1, (@au3.code.to_i - @au2.code.to_i)
  end

  test "user in different office can have same code" do
    @au2 = offices(:domain_office_1).agent_users.create :login => 'au2', :email => 'au2@aa.xx.zz', :password => 'au2au2', :password_confirmation => 'au2au2', :code => '100', :phone => '123456', :first_name => 'aaa', :last_name => 'bbb'
    @au3 = offices(:domain_office_2).agent_users.create :login => 'au3', :email => 'au3@aa.xx.zz', :password => 'au3au3', :password_confirmation => 'au3au3', :code => '100', :phone => '123456', :first_name => 'aaa', :last_name => 'bbb'
    assert @au2.valid?
    assert_equal 'au2', @au2.pretty_login
    assert_equal 'au2', @au2.login
    assert @au3.valid?
    assert_equal 'au3', @au3.pretty_login
    assert_equal 'au3', @au3.login
  end

  test "agent user full code comprised of developer code, agent code, office code and agent user code" do
    assert_equal [@au.office.agent.developer.code, @au.office.agent.code, @au.office.code, @au.code].join('-'), @au.full_code
  end

  test "agent user can have many properties" do
    assert(users(:bob).properties.count > 1)
  end

  test "agent user have access level" do
    users(:alice).roles << Role.find_by_name('normal')
    assert_equal 3, users(:alice).access_level
    users(:alice).access_level = 1
    assert_equal 1, users(:alice).access_level
    users(:alice).access_level = 2
    assert_equal 2, users(:alice).access_level
    assert users(:alice).has_role?('normal')
  end

end
