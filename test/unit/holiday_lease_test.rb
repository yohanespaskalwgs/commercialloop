# == Schema Info
# Schema version: 20090614051807
#
# Table name: properties
#
#  id                   :integer(4)      not null, primary key
#  agent_id             :integer(4)
#  agent_user_id        :integer(4)
#  office_id            :integer(4)
#  primary_contact_id   :integer(4)
#  property_id          :string(255)
#  secondary_contact_id :integer(4)
#  country              :string(255)
#  deal_type            :string(255)
#  description          :text
#  display_address      :boolean(1)      default(TRUE)
#  display_price        :boolean(1)
#  display_price_text   :string(255)
#  headline             :string(255)
#  latitude             :string(50)
#  longitude            :string(50)
#  price                :decimal(16, 3)
#  price2               :decimal(16, 3)
#  price2_include_tax   :boolean(1)
#  property_type        :string(255)
#  save_status          :string(255)
#  state                :string(255)
#  status               :integer(4)
#  street               :string(255)
#  street_number        :string(255)
#  suburb               :string(255)
#  town_village         :string(255)
#  type                 :string(255)
#  unit_number          :string(255)
#  vendor_email         :string(255)
#  vendor_first_name    :string(255)
#  vendor_last_name     :string(255)
#  vendor_phone         :string(255)
#  zipcode              :string(30)
#  created_at           :datetime
#  deleted_at           :datetime
#  updated_at           :datetime

require 'test_helper'

class HolidayLeaseTest < ActiveSupport::TestCase

  test "holiday lease should have detail" do
    assert_equal holiday_lease_details(:one), properties(:holiday_lease).detail
  end

  test "resident sale should have valid detail" do
    p = properties(:holiday_lease)
    p.detail.bedrooms = nil
    assert !p.valid?
  end

  test "should formalize prices rate to nightly based" do
    p = properties(:holiday_lease)
    p.price = 100
    p.save
    assert_equal 100, p.price.to_i
    p.price_period = 'Nightly'
    p.save
    assert_equal 100, p.price.to_i
    p.price_period = 'Weekly'
    p.save
    assert_in_delta p.price.to_f, 100/7.0, 0.001
    p.price = 100
    p.price_period = 'Monthly'
    p.save
    assert_in_delta p.price, 100/30.0, 0.001
  end

end
