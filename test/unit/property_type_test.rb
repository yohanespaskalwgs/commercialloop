# == Schema Info
# Schema version: 20090614051807
#
# Table name: property_types
#
#  id       :integer(4)      not null, primary key
#  category :string(255)
#  name     :string(255)

require 'test_helper'

class PropertyTypeTest < ActiveSupport::TestCase

  def setup
    @pt = PropertyType.new :category => 'Residential', :name => 'Terrace'
  end

  test "property type is valid" do
    assert @pt.valid?
  end

  test "property type must have category" do
    @pt.category = ''
    assert !@pt.valid?
  end


  test "property type must have name" do
    @pt.name = ''
    assert !@pt.valid?
  end

  test "property type name must be unique in its category" do
    @pt.save
    @pt2 = PropertyType.new :category => @pt.category, :name => @pt.name
    assert !@pt2.valid?
    @pt2.category = 'Commercial'
    assert @pt2.valid?
  end

  test "should get different types" do
    assert_equal property_types(:residential), PropertyType.residential.first
    assert_equal property_types(:commercial), PropertyType.commercial.first
    assert_equal property_types(:business), PropertyType.business.first
  end

end
