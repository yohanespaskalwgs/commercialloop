# == Schema Info
# Schema version: 20090614051807
#
# Table name: roles
#
#  id   :integer(4)      not null, primary key
#  name :string(255)

require 'test_helper'

class RoleTest < ActiveSupport::TestCase

  test "role has many users" do
    assert(roles(:admin_role).users.count > 1)
  end

end
