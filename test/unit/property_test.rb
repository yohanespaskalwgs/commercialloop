# == Schema Info
# Schema version: 20090614051807
#
# Table name: properties
#
#  id                   :integer(4)      not null, primary key
#  agent_id             :integer(4)
#  agent_user_id        :integer(4)
#  office_id            :integer(4)
#  primary_contact_id   :integer(4)
#  property_id          :string(255)
#  secondary_contact_id :integer(4)
#  country              :string(255)
#  deal_type            :string(255)
#  description          :text
#  display_address      :boolean(1)      default(TRUE)
#  display_price        :boolean(1)
#  display_price_text   :string(255)
#  headline             :string(255)
#  latitude             :string(50)
#  longitude            :string(50)
#  price                :decimal(16, 3)
#  price2               :decimal(16, 3)
#  price2_include_tax   :boolean(1)
#  property_type        :string(255)
#  save_status          :string(255)
#  state                :string(255)
#  status               :integer(4)
#  street               :string(255)
#  street_number        :string(255)
#  suburb               :string(255)
#  town_village         :string(255)
#  type                 :string(255)
#  unit_number          :string(255)
#  vendor_email         :string(255)
#  vendor_first_name    :string(255)
#  vendor_last_name     :string(255)
#  vendor_phone         :string(255)
#  zipcode              :string(30)
#  created_at           :datetime
#  deleted_at           :datetime
#  updated_at           :datetime

require 'test_helper'

class PropertyTest < ActiveSupport::TestCase

  def setup
    @new_property = Property.new valid_property
  end

  test "new property is valid" do
    assert @new_property.valid?
    assert @new_property.save
  end

  test "property has many features" do
    assert(properties(:holiday_lease).features.count > 1)
    assert_equal 1, properties(:holiday_lease).features.external.count
    assert_equal 1, properties(:holiday_lease).features.general.count
    assert_equal 1, properties(:holiday_lease).features.internal.count
    assert_equal 1, properties(:holiday_lease).features.security.count
    assert_equal 1, properties(:holiday_lease).features.location.count
    assert_equal 1, properties(:holiday_lease).features.lifestyle.count
    assert_equal 1, properties(:holiday_lease).features.user_defined.count
  end

  test "add features to property" do
    p = properties(:project_sale)
    assert_equal 0, p.features.count
    assert_no_difference 'Feature.count' do
      p.feature_ids = [features(:golf).id, features(:bath).id]
      p.save
      assert_equal 2, p.features.count
    end
  end

  test "property belongs to a agent user" do
    assert_equal users(:bob), @new_property.agent_user
  end

  test "property belongs to a primary contact" do
    assert_equal users(:bob), @new_property.primary_contact
  end

  test "property must have a primary contact" do
    @new_property.primary_contact_id = nil
    assert !@new_property.valid?
  end

  test "property can have a secondary contact" do
    assert_nil @new_property.secondary_contact_id
    assert @new_property.valid?
    @new_property.secondary_contact = users(:alice)
    assert @new_property.save
    assert_equal users(:alice).id, @new_property.secondary_contact_id
  end

  test "property must have certain address informations" do
    assert !Property.new(valid_property(:country => nil)).valid?
    assert !Property.new(valid_property(:state => nil)).valid?
    assert !Property.new(valid_property(:street => nil)).valid?
    assert !Property.new(valid_property(:street_number => nil)).valid?
    assert !Property.new(valid_property(:zipcode => nil)).valid?
    assert !Property.new(valid_property(:suburb => nil)).valid?
    assert !Property.new(valid_property(:display_address => nil)).valid?
    assert Property.new(valid_property(:display_address => false)).valid?
    assert Property.new(valid_property(:display_price => false)).valid?
  end

  test "property must have certain price informations" do
    assert !Property.new(valid_property(:price => nil)).valid?
  end

  test "property must have valid price number" do
    valid_price_assertions @new_property, :price=
  end

  test "property don't need price if it's commercial sale" do
    @new_property.deal_type = 'Sale'
    @new_property.price = nil
    @new_property.price2 = 123
    @new_property.price2_include_tax = true
    assert @new_property.valid?
  end

  test "property must have property type" do
    @new_property.property_type = nil
    assert !@new_property.valid?
  end

  test "property price is monthly based" do
    @new_property.price_per = Property::METRIC_WEEKLY.to_s
    monthly_price = @new_property.price*4
    @new_property.save
    assert_equal monthly_price, @new_property.price
  end

  test "property has one or none current campaign" do
    assert_nil @new_property.current_campaign
  end

  test "default display price is true" do
    assert Property.new(valid_property(:display_price => nil)).display_price
  end

end
