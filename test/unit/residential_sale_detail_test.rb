# == Schema Info
# Schema version: 20090614051807
#
# Table name: residential_sale_details
#
#  id                       :integer(4)      not null, primary key
#  residential_sale_id      :integer(4)
#  auction_date             :date
#  auction_place            :string(255)
#  auction_price            :decimal(16, 3)
#  auction_time             :time
#  bathrooms                :integer(4)      default(0)
#  bedrooms                 :integer(4)      default(0)
#  carport_spaces           :integer(4)      default(0)
#  condo_strata_fee         :decimal(16, 3)
#  energy_efficiency_rating :string(255)
#  energy_star_rating       :string(255)
#  estimate_rental_return   :decimal(16, 3)
#  floor_area               :decimal(16, 3)
#  forthcoming_auction      :boolean(1)
#  garage_spaces            :integer(4)      default(0)
#  land_area                :decimal(16, 3)
#  latitude                 :decimal(12, 7)
#  longitude                :decimal(12, 7)
#  number_of_floors         :integer(4)
#  off_street_spaces        :integer(4)      default(0)
#  tax_rate                 :decimal(16, 3)
#  virtual_tour             :string(255)
#  year_built               :integer(4)

require 'test_helper'

class ResidentialSaleDetailTest < ActiveSupport::TestCase
  
  def setup
    @rs = properties(:residential_sale)
    @rsd = residential_sale_details(:one)
  end

  test "residential sale detail is valid" do
    assert @rsd.valid?
  end

  test "resident sale details belongs to a resident sale property" do
    assert_equal @rs, @rsd.residential_sale
  end

  test "user must provide number of floors" do
    valid_positive_integer_assertions @rsd, :number_of_floors=
  end

  test "user must provide valid floor area" do
    valid_area_assertions @rsd, :floor_area=, true
  end

  test "user must provide valid land area" do
    valid_area_assertions @rsd, :land_area=, true
  end

  test "user must provide valid build year" do
    valid_year_assertions @rsd, :year_built=, true
  end

  test "bedroom number must be present" do
    @rsd.bedrooms = nil
    assert !@rsd.valid?
  end

  test "bathroom number must be present" do
    @rsd.bathrooms = nil
    assert !@rsd.valid?
  end


  test "should formalize tax rate to weekly based" do
    @rsd.tax_rate = 100
    @rsd.save
    assert_equal 100, @rsd.tax_rate.to_i
    @rsd.tax_rate_period = 'per month'
    @rsd.save
    assert_equal 25, @rsd.tax_rate.to_i
    @rsd.condo_strata_fee = 100
    @rsd.condo_strata_fee_period = 'per year'
    @rsd.save
    assert_in_delta @rsd.condo_strata_fee.to_f, 100/48.0, 0.001
    @rsd.estimate_rental_return = 100
    @rsd.estimate_rental_return_period = 'per decade'
    @rsd.save
    assert_in_delta @rsd.estimate_rental_return, 100/480.0, 0.001
  end

  test "should validates area metrics only when area is provided" do
    @new = ResidentialSaleDetail.new @rsd.attributes
    @new.floor_area = nil
    @new.floor_area_metric = nil
    @new.land_area = nil
    @new.land_area_metric = nil
    @new.tax_rate = nil
    @new.condo_strata_fee = nil
    @new.estimate_rental_return = nil
    assert @new.valid?

    @new.floor_area = 123
    assert !@new.valid?

    @new.floor_area_metric = 'Acres'
    assert @new.valid?
  end

  test "should save standard area in db" do
    @rsd.land_area = 100
    @rsd.land_area_metric = 'Square Meters'
    @rsd.save
    assert_equal 100, @rsd.land_area

    @rsd.land_area = 100
    @rsd.land_area_metric = 'Acres'
    @rsd.save
    assert_in_delta 100*4046.85642, @rsd.land_area.to_f, 0.0001

    @rsd.land_area = 100
    @rsd.land_area_metric = 'Hectares'
    @rsd.save
    assert_equal 100*10000, @rsd.land_area.to_i

    @rsd.land_area = 100
    @rsd.land_area_metric = 'Square Feets'
    @rsd.save
    assert_in_delta 100*0.3048*0.3048, @rsd.land_area.to_f, 0.0001

    @rsd.land_area = 100
    @rsd.land_area_metric = 'Square Yards'
    @rsd.save
    assert_equal 100*0.9144*0.9144, @rsd.land_area.to_f, 0.0001
  end

  test "saved detail has default metric" do
    assert_equal 'Square Meters', @rsd.land_area_metric
    assert_equal 'Square Meters', @rsd.floor_area_metric
  end

end
