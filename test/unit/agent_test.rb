# == Schema Info
# Schema version: 20090614051807
#
# Table name: agents
#
#  id                   :integer(4)      not null, primary key
#  developer_contact_id :integer(4)
#  developer_id         :integer(4)
#  support_contact_id   :integer(4)
#  upgrade_contact_id   :integer(4)
#  access_key           :string(255)
#  afterhrs_phone       :string(255)
#  business_phone       :string(255)
#  code                 :string(255)
#  country              :string(255)
#  email                :string(255)
#  fax_number           :string(255)
#  holder_email         :string(255)
#  holder_fax           :string(255)
#  holder_first_name    :string(255)
#  holder_last_name     :string(255)
#  holder_mobile        :string(255)
#  holder_phone         :string(255)
#  mobile               :string(255)
#  name                 :string(255)
#  private_key          :string(255)
#  state                :string(255)
#  status               :string(255)
#  street               :string(255)
#  street_number        :string(255)
#  street_type          :string(255)
#  suburb               :string(255)
#  unit_number          :string(255)
#  website              :string(255)
#  zipcode              :string(10)
#  created_at           :datetime
#  deleted_at           :datetime
#  updated_at           :datetime

require 'test_helper'

class AgentTest < ActiveSupport::TestCase

  def setup
    @agent = Agent.new :developer_id => agents(:domain).developer_id, :code => '87654', :name => 'rorcraft'
  end

  test "agent belongs to a developer(reseller)" do
    assert_equal developers(:agentsquare), agents(:domain).developer
    assert_equal developers(:agentline), agents(:yahoo).developer
  end

  test "agent has a developer contact" do
    assert_equal users(:louies), agents(:domain).developer_contact
  end

  test "agent has a support contact" do
    assert_equal users(:louies), agents(:domain).support_contact
  end
  
  test "agent has a upgrade contact" do
    assert_equal users(:louies), agents(:domain).upgrade_contact
  end

  test "agent has many agent users" do
    assert(agents(:domain).agent_users.count > 1)
  end

  test "agent has many properties" do
    assert_equal 8, agents(:domain).properties.count
  end

  test "agent has contact details" do
    @agent.country = 'china'
    assert_equal 'china', @agent.contact.country
    assert_equal 'AU', agents(:domain).contact.country
  end

  test "agent has a holder" do
    @agent.holder_mobile = '12345'
    assert_equal '12345', @agent.holder.mobile
    assert_equal 'Jan X', agents(:domain).holder.full_name
  end

  test "agent has head office" do
    assert_equal offices(:domain_office_1), agents(:domain).head_office
  end

  test "agent is valid" do
    assert @agent.valid?
  end

  test "one developer's agents can't have same name" do
    @agent.name = agents(:domain).name
    assert (not @agent.valid?)
  end

  test "agent name can be same in different developer" do
    @agent.developer_id = 12345
    @agent.name = agents(:domain).name
    assert @agent.valid?
  end

  test "one developer's agents can't have same code" do
    @agent.code = agents(:domain).code
    assert (not @agent.valid?)
  end

  test "agent code can be same in different developer" do
    @agent.developer_id = 12345
    @agent.code = agents(:domain).code
    assert @agent.valid?
  end

  test "agent's full code comprised of developer code and agent code" do
    assert_equal((@agent.developer.full_code+"-"+@agent.code), @agent.full_code)
  end

  test "agent must belong to a developer" do
    @agent.developer_id = nil
    assert !@agent.valid?
  end

  test "agent must have a 5 digits code" do
    @agent.code = "1234"
    assert (not @agent.valid?)
    @agent.code = "123456"
    assert (not @agent.valid?)
    @agent.code = "12a45"
    assert (not @agent.valid?)
  end

  test "agent will auto generate a code when create" do
    @agent.code = nil
    assert @agent.valid?
    assert_match /\d{5}/, @agent.code
  end

  test "agent will re-generate the code when it conflict with other agent in the same developer" do
    @agent.save
    @agent2 = Agent.new @agent.attributes
    @agent2.name = "noonelikeme"
    @agent2.code = nil
    $agent_code = @agent.code
    def @agent2.populate_code
      @@count ||= 0
      if @@count == 0
        # conflict at the first time
        @@count += 1
        self.code = $agent_code
      else
        super
      end
    end
    assert @agent2.valid?
  end

  test "agent in diff developer can have same code and name" do
    @a1 = developers(:agentline).agents.create :name => 'foo', :code => '11111'
    @a2 = developers(:agentsquare).agents.create :name => 'foo', :code => '11111'
    assert @a1.valid?
    assert @a2.valid?
  end

  test "agent can have many offices" do
    assert(agents(:domain).offices.count > 1)
  end

end
