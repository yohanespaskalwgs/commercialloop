ENV["RAILS_ENV"] = "test"
require File.expand_path(File.dirname(__FILE__) + "/../config/environment")
require 'test_help'

class ActiveSupport::TestCase
  # Transactional fixtures accelerate your tests by wrapping each test method
  # in a transaction that's rolled back on completion.  This ensures that the
  # test database remains unchanged so your fixtures don't have to be reloaded
  # between every test method.  Fewer database queries means faster tests.
  #
  # Read Mike Clark's excellent walkthrough at
  #   http://clarkware.com/cgi/blosxom/2005/10/24#Rails10FastTesting
  #
  # Every Active Record database supports transactions except MyISAM tables
  # in MySQL.  Turn off transactional fixtures in this case; however, if you
  # don't care one way or the other, switching from MyISAM to InnoDB tables
  # is recommended.
  #
  # The only drawback to using transactional fixtures is when you actually 
  # need to test transactions.  Since your test is bracketed by a transaction,
  # any transactions started in your code will be automatically rolled back.
  self.use_transactional_fixtures = true

  # Instantiated fixtures are slow, but give you @david where otherwise you
  # would need people(:david).  If you don't want to migrate your existing
  # test cases which use the @david style and don't mind the speed hit (each
  # instantiated fixtures translates to a database query per test method),
  # then set this back to true.
  self.use_instantiated_fixtures  = false

  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  # Add more helper methods to be used by all tests here...
end

class ActionController::TestCase
  # Be sure to include AuthenticatedTestHelper in test/test_helper.rb instead
  # Then, you can remove it from this and the units test.
  include AuthenticatedTestHelper
end

def valid_user(attrs={})
  {:login => 'jan1', :email => 'jan1@aa.bb.zz', :password => 'rorcraft', :password_confirmation => 'rorcraft', :phone => '1234567', :first_name => 'jan', :last_name => 'x'}.merge!(attrs)
end

def valid_office(attrs={})
  {:name => 'somename', :contact_first_name => 'foo', :contact_last_name => 'bar', :contact_email => 'somesome@some.somesome.som', :phone_number => '123 123 123'}.merge!(attrs)
end

def valid_property(attrs={})
  {:agent_user_id => users(:bob).id, :primary_contact_id => users(:bob).id, :country => 'China', :state => 'Zhejiang', :street_number => '123', :street => 'fengqi donglu', :zipcode => '310000', :suburb => 'jianggan', :display_address => true, :display_price => true, :price => 12345.67, :property_type => 'House', :headline => 'aaa', :description => 'bbb'}.merge!(attrs)
end

def valid_price_assertions(obj, method, allow_nil=false, allow_zero=false)
  obj.send method, nil
  allow_nil ? assert(obj.valid?) : assert(!obj.valid?)
  obj.send method, 0
  allow_zero ? assert(obj.valid?) : assert(!obj.valid?)
  obj.send method, -0.1
  assert !obj.valid?
end
alias :valid_area_assertions :valid_price_assertions

def valid_year_assertions(obj, method, allow_nil=false)
  obj.send method, nil
  allow_nil ? assert(obj.valid?) : assert(!obj.valid?)
  obj.send method, 0
  assert !obj.valid?
  obj.send method, -12
  assert !obj.valid?
  obj.send method, 1999.9
  assert !obj.valid?
  obj.send method, Time.now.year + 1
  assert !obj.valid?
  obj.send method, 999
  assert !obj.valid?
  obj.send method, 1000
  assert obj.valid?
  obj.send method, Time.now.year
  assert obj.valid?
end

def valid_positive_integer_assertions(obj, method, allow_nil=false)
  obj.send method, nil
  allow_nil ? assert(obj.valid?) : assert(!obj.valid?)
  obj.send method, 0
  assert !obj.valid?
  obj.send method, -1
  assert !obj.valid?
  obj.send method, 1.1
  assert !obj.valid?
  obj.send method, 1
  assert obj.valid?
end

IMAGE_PREFIX = 'tmp/test/images' unless defined? IMAGE_PREFIX
Image.attachment_options[:path_prefix] = IMAGE_PREFIX

