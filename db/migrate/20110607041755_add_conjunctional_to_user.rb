class AddConjunctionalToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :conjunctional_office_name, :string
    add_column :users, :conjunction_rea_agent_id, :integer
  end

  def self.down
    remove_column(:users, :conjunctional_office_name)
    remove_column(:users, :conjunction_rea_agent_id)
  end
end
