class CreateSalesRecords < ActiveRecord::Migration
  def self.up
    create_table :sales_records do |t|
      t.integer :property_id
      t.string :company
      t.string :contact
      t.string :phone
      t.string :mobile
      t.string :email
      t.string :method_of_sale
      t.boolean :investment
      t.string :sale_price
      t.string :fee
      t.date :date_of_sale
      t.date :settlement_date
      t.text :office_note
      t.string :transaction_sales_person
      t.string :external_agent_company
      t.string :external_agent_sales_person
      t.string :conjunction_fee
      t.string :scale_of_fee
      t.timestamps
    end
  end

  def self.down
    drop_table :sales_records
  end
end
