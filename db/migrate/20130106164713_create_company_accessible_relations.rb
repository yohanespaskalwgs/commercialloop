class CreateCompanyAccessibleRelations < ActiveRecord::Migration
  def self.up
    create_table :company_accessible_relations do |t|
      t.integer :agent_company_id
      t.integer :accessible_by
      t.timestamps
    end
  end

  def self.down
    drop_table :company_accessible_relations
  end
end
