class AddFieldToCommercial < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :authority, :string
    add_column :commercial_details, :highlight1, :string
    add_column :commercial_details, :highlight2, :string
    add_column :commercial_details, :highlight3, :string
    add_column :commercial_details, :area_range, :integer
    add_column :commercial_details, :area_range_to, :integer
    add_column :commercial_details, :rental_price, :integer
    add_column :commercial_details, :rental_price_to, :integer

  end

  def self.down
    remove_column(:commercial_details, :authority)
    remove_column(:commercial_details, :highlight1)
    remove_column(:commercial_details, :highlight2)
    remove_column(:commercial_details, :highlight3)
    remove_column(:commercial_details, :area_range)
    remove_column(:commercial_details, :area_range_to)
    remove_column(:commercial_details, :rental_price)
    remove_column(:commercial_details, :rental_price_to)
  end
end
