class CreateUpdatesForAgents < ActiveRecord::Migration
  def self.up
    rename_table :news, :agent_updates
  end

  def self.down
    rename_table :agent_updates, :news
  end
end
