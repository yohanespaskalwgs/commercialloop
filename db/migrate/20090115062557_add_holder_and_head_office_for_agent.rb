class AddHolderAndHeadOfficeForAgent < ActiveRecord::Migration
  def self.up
    add_column :offices, :head, :boolean
    add_column :agents, :status, :string
    remove_column :agents, :head_office_id
    Role.create :name => 'holder'
  end

  def self.down
    remove_column :offices, :head
    remove_column :agents, :status
    add_column :agents, :head_office_id, :integer
    Role.find_by_name('holder').destroy
  end
end
