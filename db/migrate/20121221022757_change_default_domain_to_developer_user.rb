class ChangeDefaultDomainToDeveloperUser < ActiveRecord::Migration
  def self.up
    change_column :developers, :domain, :string, :default => 'commercialloopcrm.com.au'
  end

  def self.down
    change_column :developers, :domain, :string, :default => 'agentaccount.com'
  end
end
