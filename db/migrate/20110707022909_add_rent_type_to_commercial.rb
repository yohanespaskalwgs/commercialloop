class AddRentTypeToCommercial < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :rent_type, :string
  end

  def self.down
    remove_column(:commercial_details, :rent_type)
  end
end
