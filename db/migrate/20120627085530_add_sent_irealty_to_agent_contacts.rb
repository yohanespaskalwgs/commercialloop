class AddSentIrealtyToAgentContacts < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :sent_irealty, :boolean
  end

  def self.down
    remove_column :agent_contacts, :sent_irealty
  end
end
