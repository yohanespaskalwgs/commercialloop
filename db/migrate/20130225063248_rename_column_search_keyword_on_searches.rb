class RenameColumnSearchKeywordOnSearches < ActiveRecord::Migration
  def self.up
    rename_column :searches, :search_keyword, :keyword_name
  end

  def self.down
    rename_column :searches, :keyword_name, :search_keyword
  end
end
