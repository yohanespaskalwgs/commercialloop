class CreateSearches < ActiveRecord::Migration
  def self.up
    create_table :searches do |t|
      t.string :search_keyword
      t.integer :agent_id
      t.integer :office_id
      t.timestamps
    end
  end

  def self.down
    drop_table :searches
  end
end
