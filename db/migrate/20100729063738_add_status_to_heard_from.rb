class AddStatusToHeardFrom < ActiveRecord::Migration
  def self.up
    add_column :heard_froms, :status, :string
    HeardFrom.destroy_all
    HeardFrom.create(:name => "Newspaper", :status => "heard_office")
    HeardFrom.create(:name => "Referral", :status => "heard_office")
    HeardFrom.create(:name => "Web", :status => "heard_office")
    HeardFrom.create(:name => "Past Client", :status => "heard_office")
    HeardFrom.create(:name => "Sign board", :status => "heard_office")
    HeardFrom.create(:name => "Other", :status => "heard_office")

    HeardFrom.create(:name => "Office Website", :status => "heard_property")
    HeardFrom.create(:name => "Individual Property Website", :status => "heard_property")
    HeardFrom.create(:name => "Individual Agent Website", :status => "heard_property")
    HeardFrom.create(:name => "Mobile Phone Website", :status => "heard_property")
    HeardFrom.create(:name => "iPhone Application", :status => "heard_property")
    HeardFrom.create(:name => "Facebook Application", :status => "heard_property")
    HeardFrom.create(:name => "Telephone", :status => "heard_property")
    HeardFrom.create(:name => "Internet (Source 1)", :status => "heard_property")
    HeardFrom.create(:name => "Internet (Source 2)", :status => "heard_property")
    HeardFrom.create(:name => "Internet (Source 3)", :status => "heard_property")
    HeardFrom.create(:name => "Print (Source 1)", :status => "heard_property")
    HeardFrom.create(:name => "Print (Source 2)", :status => "heard_property")
    HeardFrom.create(:name => "Print (Source 3)", :status => "heard_property")
    HeardFrom.create(:name => "Email Alert", :status => "heard_property")
    HeardFrom.create(:name => "SMS Alert", :status => "heard_property")
  end

  def self.down
    remove_column(:heard_froms, :status)
  end
end
