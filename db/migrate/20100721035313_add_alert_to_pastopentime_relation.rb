class AddAlertToPastopentimeRelation < ActiveRecord::Migration
  def self.up
    add_column :contact_pastopentime_relations, :contact_alert_id, :integer
    add_column :contact_pastopentime_relations, :contact_alertnote_id, :integer
  end

  def self.down
    remove_column(:contact_pastopentime_relations, :contact_alert_id)
    remove_column(:contact_pastopentime_relations, :contact_alertnote_id)
  end
end
