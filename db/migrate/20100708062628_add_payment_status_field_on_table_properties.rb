class AddPaymentStatusFieldOnTableProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :payment_status, :string
  end

  def self.down
    remove_column(:properties, :payment_status)
  end
end
