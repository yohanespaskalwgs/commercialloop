class AddNewPorjectRea < ActiveRecord::Migration
  def self.up
    add_column :properties, :new_project_rea, :boolean
  end

  def self.down
    remove_column :properties, :new_project_rea
  end
end
