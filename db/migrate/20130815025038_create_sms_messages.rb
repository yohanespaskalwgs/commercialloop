class CreateSmsMessages < ActiveRecord::Migration
  def self.up
    create_table :sms_messages do |t|
      t.integer :sender_id
      t.integer :office_id
      t.string :receiver_number
      t.text :message
      t.string :status
      t.timestamps
    end

    add_index :sms_messages, :sender_id
    add_index :sms_messages, :office_id
  end

  def self.down
    drop_table :sms_messages
  end
end
