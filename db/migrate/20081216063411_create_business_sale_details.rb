class CreateBusinessSaleDetails < ActiveRecord::Migration
  def self.up
    create_table :business_sale_details do |t|
      t.string :category
      t.string :business_name
      t.text :additional_notes
      t.integer :year_built
      t.string :energy_efficiency_rating
      t.string :energy_star_rating
      t.decimal :longitude, :precision => 12, :scale => 7
      t.decimal :latitude, :precision => 12, :scale => 7
      t.string :premise
      t.string :lease_commencement
      t.string :lease_end
      t.string :lease_option
      t.string :lease_plus_another
      t.decimal :current_rent, :precision => 16, :scale => 3
      t.boolean :current_rent_include_tax
      t.decimal :current_outgoings, :precision => 16, :scale => 3
      t.boolean :current_outgoings_include_tax
      t.decimal :land_area, :precision => 16, :scale => 3
      t.decimal :floor_area, :precision => 16, :scale => 3
      t.string :patron_capacity
      t.integer :parking_spaces, :default => 0
      t.string :parking_comments
      t.string :franchise
      t.string :outlets
      t.string :franchise_royalties
      t.string :franchise_levies
      t.string :staff_full_time
      t.string :staff_part_time
      t.string :staff_casual
      t.decimal :annual_turnover, :precision => 16, :scale => 3
      t.decimal :annual_gross_profit, :precision => 16, :scale => 3
      t.decimal :annual_ebit, :precision => 16, :scale => 3
      t.string :virtual_tour
      t.boolean :forthcoming_auction
      t.string :auction_place
      t.datetime :auction_datetime
      t.boolean :price_include_tax
      t.boolean :price_include_stock
      t.decimal :approximate_stock_value, :precision => 16, :scale => 3
      t.integer :business_sale_id
    end
  end

  def self.down
    drop_table :business_sale_details
  end
end
