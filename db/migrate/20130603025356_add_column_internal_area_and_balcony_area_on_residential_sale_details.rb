class AddColumnInternalAreaAndBalconyAreaOnResidentialSaleDetails < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :internal_area, :integer
    add_column :residential_sale_details, :balcony_area, :integer
  end

  def self.down
    remove_column :residential_sale_details, :internal_area
    remove_column :residential_sale_details, :balcony_area
  end
end
