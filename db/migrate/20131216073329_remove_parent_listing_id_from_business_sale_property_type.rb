class RemoveParentListingIdFromBusinessSalePropertyType < ActiveRecord::Migration
  def self.up
  	business_sales = Property.business_sale
  	business_sales.each do |bus_sale|
  		bus_sale.parent_listing_id = nil
  		bus_sale.save
  	end
  end

  def self.down
  end
end
