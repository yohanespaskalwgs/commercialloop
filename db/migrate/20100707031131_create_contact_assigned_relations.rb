class CreateContactAssignedRelations < ActiveRecord::Migration
  def self.up
    create_table :contact_assigned_relations do |t|
      t.integer :agent_contact_id
      t.integer :assigned_to
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_assigned_relations
  end
end
