class AddColumnTownCountryIdOnSuburbs < ActiveRecord::Migration
  def self.up
    add_column :suburbs, :town_country_id, :integer
  end

  def self.down
    remove_column(:suburbs, :town_country_id)
  end
end
