class AddFeeTermsForTableResidentialLeaseDetails < ActiveRecord::Migration
   def self.up
    add_column :residential_lease_details, :fee_terms, :string
  end

  def self.down
    remove_column :residential_lease_details, :fee_terms
  end
end
