class CreateOffers < ActiveRecord::Migration
  def self.up
    create_table :offers do |t|
      t.integer :property_id
      t.integer :agent_contact_id
      t.date :date
      t.text :detail
      t.timestamps
    end
  end

  def self.down
    drop_table :offers
  end
end
