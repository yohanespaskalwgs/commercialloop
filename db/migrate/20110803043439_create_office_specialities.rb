class CreateOfficeSpecialities < ActiveRecord::Migration
  def self.up
    create_table :office_specialities do |t|
      t.string :name
      t.timestamps
    end
    OfficeSpeciality.destroy_all
    OfficeSpeciality.create(:name => "Residential Property Management")
    OfficeSpeciality.create(:name => "Residential Sales")
    OfficeSpeciality.create(:name => "Residential Project Marketing")
    OfficeSpeciality.create(:name => "Commercial Property Management")
    OfficeSpeciality.create(:name => "Commercial Sales")
    OfficeSpeciality.create(:name => "Commercial Project Marketing")
    OfficeSpeciality.create(:name => "Business Sales")
    OfficeSpeciality.create(:name => "Land Sales")
    OfficeSpeciality.create(:name => "Rural Sales")
    OfficeSpeciality.create(:name => "Acerage Sales")
  end

  def self.down
    drop_table :office_specialities
  end
end
