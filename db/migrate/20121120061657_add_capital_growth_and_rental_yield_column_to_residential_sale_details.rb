class AddCapitalGrowthAndRentalYieldColumnToResidentialSaleDetails < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :capital_growth, :string
    add_column :residential_sale_details, :rental_yield, :string
  end

  def self.down
    remove_column :residential_sale_details, :capital_growth
    remove_column :residential_sale_details, :rental_yield
  end
end
