class AddSuburbFieldOnAgentUser < ActiveRecord::Migration
  def self.up
    add_column :users, :suburb, :string
  end

  def self.down
    remove_column :users, :suburb
  end
end
