class ChangeFieldName < ActiveRecord::Migration
  def self.up
    add_column :extra_details, :extra_detail_name_id, :integer
    remove_column :extra_details, :field_name
  end

  def self.down
    remove_column :extra_details, :extra_detail_name_id
    add_column :extra_details, :field_name, :string
  end
end
