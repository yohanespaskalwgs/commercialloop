class CreateCountryPropertyTypeRelationships < ActiveRecord::Migration
  def self.up
    create_table :country_property_type_relationships do |t|
      t.integer :country_id
      t.integer :property_type_id
      t.timestamps
    end
  end

  def self.down
    drop_table :country_property_type_relationships
  end
end
