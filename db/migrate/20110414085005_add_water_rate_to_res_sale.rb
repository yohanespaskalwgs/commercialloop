class AddWaterRateToResSale < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :water_rate, :decimal, :precision => 16, :scale => 3
    add_column :residential_sale_details, :water_rate_period,:string
  end

  def self.down
    remove_column :residential_sale_details, :water_rate
    remove_column :residential_sale_details, :water_rate_period
  end
end
