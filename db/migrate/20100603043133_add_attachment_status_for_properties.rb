class AddAttachmentStatusForProperties < ActiveRecord::Migration
  def self.up
    add_column :properties,:image_exist,:boolean
    add_column :properties,:floorplan_exist,:boolean
  end

  def self.down
    remove_column :properties,:image_exist
    remove_column :properties,:floorplan_exist
  end
end
