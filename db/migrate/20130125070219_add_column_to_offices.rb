class AddColumnToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :new_development, :boolean
    add_column :offices, :commercial_building, :boolean
    add_column :offices, :land_release, :boolean
    add_column :offices, :residential_sale, :boolean
    add_column :offices, :residential_lease, :boolean
    add_column :offices, :holiday_lease, :boolean
    add_column :offices, :commercial, :boolean
    add_column :offices, :project_sale, :boolean
    add_column :offices, :business_sale, :boolean
  end

  def self.down
    remove_column :offices, :new_development
    remove_column :offices, :commercial_building
    remove_column :offices, :land_release
    remove_column :offices, :residential_sale
    remove_column :offices, :residential_lease
    remove_column :offices, :holiday_lease
    remove_column :offices, :commercial
    remove_column :offices, :project_sale
    remove_column :offices, :business_sale
  end
end
