class AddRequireImportToOffice < ActiveRecord::Migration
def self.up
    add_column :offices, :require_import, :boolean
  end

  def self.down
    remove_column :offices, :require_import
  end
end
