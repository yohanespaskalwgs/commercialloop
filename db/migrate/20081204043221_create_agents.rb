class CreateAgents < ActiveRecord::Migration
  def self.up
    create_table :agents do |t|
      t.string :name
      t.string :code
      t.string :unit_number
      t.string :street_number
      t.string :street
      t.string :street_type
      t.string :suburb
      t.string :state
      t.string :country
      t.string :email
      t.string :business_phone
      t.string :afterhrs_phone
      t.string :mobile
      t.string :fax_number
      t.string :website
      t.integer :head_office_id
      t.integer :developer_id
      t.datetime :deleted_at

      t.timestamps
    end
  end

  def self.down
    drop_table :agents
  end
end
