class AddSendToBoomForOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :send_contact, :boolean
  end

  def self.down
    remove_column :offices, :send_contact
  end
end
