class ChangeTypeField < ActiveRecord::Migration
def self.up
    change_table :commercial_details do |t|
      t.change :price_include_tax, :integer
      t.change :current_rent_include_tax, :integer
    end

    change_table :users do |t|
      t.change :conjunction_rea_agent_id, :string
    end
  end

  def self.down
    change_table :commercial_details do |t|
      t.change :price_include_tax, :boolean
      t.change :current_rent_include_tax, :boolean
    end
    change_table :users do |t|
      t.change :conjunction_rea_agent_id, :integer
    end
  end
end
