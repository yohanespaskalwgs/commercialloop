class AddCategoryToAttachment < ActiveRecord::Migration
  def self.up
    add_column :attachments, :category, :string
  end

  def self.down
    remove_column :attachments, :category
  end
end
