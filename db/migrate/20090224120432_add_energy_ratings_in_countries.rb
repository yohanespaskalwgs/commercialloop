class AddEnergyRatingsInCountries < ActiveRecord::Migration
  def self.up
    add_column :countries, :energy_efficiency_rating, :integer
    add_column :countries, :energy_star_rating, :integer
  end

  def self.down
    remove_column :countries, :energy_star_rating
    remove_column :countries, :energy_efficiency_rating
  end
end
