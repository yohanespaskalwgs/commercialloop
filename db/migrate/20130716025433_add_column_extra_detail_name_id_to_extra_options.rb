class AddColumnExtraDetailNameIdToExtraOptions < ActiveRecord::Migration
  def self.up
    add_column :extra_options, :extra_detail_name_id, :integer
    add_column :extra_detail_names, :input_type, :string
    add_column :extra_detail_names, :property_id, :integer
  end

  def self.down
    remove_column :extra_options, :extra_detail_name_id
    remove_column :extra_detail_names, :input_type
    remove_column :extra_detail_names, :property_id
  end
end
