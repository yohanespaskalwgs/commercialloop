class CreateMatchProperties < ActiveRecord::Migration
  def self.up
    create_table :match_properties do |t|
      t.integer :property_id
      t.integer :user_id
      t.integer :agent_id
      t.integer :office_id
      t.timestamps
    end
  end

  def self.down
    drop_table :match_properties
  end
end
