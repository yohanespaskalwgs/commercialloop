class Add3LevelRoles < ActiveRecord::Migration
  def self.up
    (1..3).each{|i| Role.create! :name => "LEVEL #{i}"}
  end

  def self.down
    (1..3).each{|i| Role.find_by_name("LEVEL #{i}").destroy}
  end
end
