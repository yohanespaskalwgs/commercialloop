class CreateCategoryIndustries < ActiveRecord::Migration
  def self.up
    create_table :category_industries do |t|
      t.string :name
      t.timestamps
    end
  end

  def self.down
    drop_table :category_industries
  end
end
