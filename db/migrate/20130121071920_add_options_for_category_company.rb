class AddOptionsForCategoryCompany < ActiveRecord::Migration
  def self.up
    CategoryCompany.destroy_all
    CategoryCompany.create(:name => "Prospect")
    CategoryCompany.create(:name => "Property Owner")
    CategoryCompany.create(:name => "Developer")
    CategoryCompany.create(:name => "Agent")
    CategoryCompany.create(:name => "Tenant")
    CategoryCompany.create(:name => "Service Provider")
  end

  def self.down
    CategoryCompany.destroy_all
  end
end
