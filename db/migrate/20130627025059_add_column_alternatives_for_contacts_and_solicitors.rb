class AddColumnAlternativesForContactsAndSolicitors < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :contact_data_alternative, :string
    add_column :agent_contacts, :solicitor_data_alternative, :string
  end

  def self.down
    remove_column :agent_contacts, :contact_data_alternative
    remove_column :agent_contacts, :solicitor_data_alternative
  end
end
