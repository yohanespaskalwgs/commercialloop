class AddNewFieldForTableOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :property_payment_required, :boolean
    add_column :offices, :office_paypal_account, :string
    add_column :offices, :property_listing_cost, :integer
  end

  def self.down
    remove_column(:offices, :property_payment_required)
    remove_column(:offices, :office_paypal_account)
    remove_column(:offices, :property_listing_cost)
  end
end
