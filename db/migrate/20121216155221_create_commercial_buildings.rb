class CreateCommercialBuildings < ActiveRecord::Migration
  def self.up
    create_table :commercial_building_details do |t|
      t.integer :commercial_building_id
      t.string :building_name
      t.string :building_website
      t.string :distance
      t.integer :year_built
      t.string :capital_growth
      t.string :rental_yield
      t.decimal :land_area, :precision => 16, :scale => 3
      t.string :land_area_metric
      t.decimal :floor_area, :precision => 16, :scale => 3
      t.string :floor_area_metric
      t.decimal :office_area, :precision => 16, :scale => 3
      t.string :office_area_metric
      t.decimal :retail_area, :precision => 16, :scale => 3
      t.string :retail_area_metric
      t.decimal :warehouse_area, :precision => 16, :scale => 3
      t.string :warehouse_area_metric
      t.decimal :other_area, :precision => 16, :scale => 3
      t.string :other_area_metric
      t.integer :size_from
      t.integer :size_to
      t.integer :number_of_suites
      t.integer :number_of_floors
      t.integer :number_of_carspaces
      t.string :parking_comments
      t.integer :carspace_from
      t.integer :carspace_to
      t.string :zoning
      t.timestamps
    end
  end

  def self.down
    drop_table :commercial_building_details
  end
end
