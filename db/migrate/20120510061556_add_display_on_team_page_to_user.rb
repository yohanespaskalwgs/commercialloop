class AddDisplayOnTeamPageToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :display_on_team_page, :boolean, :default => 0
  end

  def self.down
    remove_column(:users, :display_on_team_page)
  end
end
