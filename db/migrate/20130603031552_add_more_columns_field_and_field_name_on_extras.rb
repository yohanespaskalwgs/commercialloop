class AddMoreColumnsFieldAndFieldNameOnExtras < ActiveRecord::Migration
  def self.up
    add_column :extras, :field10, :string
    add_column :extras, :field11, :string
    add_column :extras, :field12, :string
    add_column :extras, :field13, :string
    add_column :extras, :field14, :string
    add_column :extras, :field_name10, :string
    add_column :extras, :field_name11, :string
    add_column :extras, :field_name12, :string
    add_column :extras, :field_name13, :string
    add_column :extras, :field_name14, :string
  end

  def self.down
    remove_column :extras, :field10
    remove_column :extras, :field11
    remove_column :extras, :field12
    remove_column :extras, :field13
    remove_column :extras, :field14
    remove_column :extras, :field_name10
    remove_column :extras, :field_name11
    remove_column :extras, :field_name12
    remove_column :extras, :field_name13
    remove_column :extras, :field_name14
  end
end
