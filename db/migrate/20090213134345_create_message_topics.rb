class CreateMessageTopics < ActiveRecord::Migration
  def self.up
    create_table :message_topics do |t|
      t.string :name
    
      t.timestamps
    end
    
    # default topics
    %w( Systems Exports Websites Updates ).each { |t| MessageTopic.create(:name => t) }
    # all admin messages should have topic associated
    AdminMessage.all.each { |m| m.update_attribute(:topic_id, MessageTopic.first.id) }
  end

  def self.down
    drop_table :message_topics
  end
end
