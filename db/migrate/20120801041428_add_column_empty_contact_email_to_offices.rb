class AddColumnEmptyContactEmailToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :empty_contact_email, :boolean
  end

  def self.down
    remove_column :offices, :empty_contact_email
  end
end
