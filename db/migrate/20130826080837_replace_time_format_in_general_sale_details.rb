class ReplaceTimeFormatInGeneralSaleDetails < ActiveRecord::Migration
  def self.up
    change_column :general_sale_details, :time, :string
  end

  def self.down
    change_column :general_sale_details, :time, :time
  end
end
