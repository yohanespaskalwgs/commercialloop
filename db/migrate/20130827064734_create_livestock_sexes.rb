class CreateLivestockSexes < ActiveRecord::Migration
  def self.up
    create_table :livestock_sexes do |t|
      t.string :name
      t.timestamps
    end

    LivestockSex.create({:name => "Bullocks"})
    LivestockSex.create({:name => "Steers"})
    LivestockSex.create({:name => "Heifers"})
    LivestockSex.create({:name => "Mixed Sex Steers & Heifers"})
    LivestockSex.create({:name => "Cows"})
    LivestockSex.create({:name => "Cows & Calves"})
    LivestockSex.create({:name => "Bulls"})
    LivestockSex.create({:name => "Mickey Bulls"})
  end

  def self.down
    drop_table :livestock_sexes
  end
end
