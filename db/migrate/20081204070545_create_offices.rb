class CreateOffices < ActiveRecord::Migration
  def self.up
    create_table :offices do |t|
      t.string :name
      t.string :code
      t.integer :agent_id
      t.string :unit_number
      t.string :street_number
      t.string :suburb
      t.string :state
      t.string :country
      t.string :phone_number
      t.string :fax_number
      t.string :email
      t.string :street
      t.string :street_type
      t.string :url
      t.string :latitude
      t.string :longitude

      t.timestamps
    end
  end

  def self.down
    drop_table :offices
  end
end
