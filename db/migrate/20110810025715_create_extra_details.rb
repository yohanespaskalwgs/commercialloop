class CreateExtraDetails < ActiveRecord::Migration
  def self.up
    create_table :extra_details do |t|
      t.integer :extra_id
      t.string  :field_name
      t.string  :field_value
      t.timestamps
    end
  end

  def self.down
    drop_table :extra_details
  end
end
