class AlterTableResidentialSaleDetailsChangeTypeDataOfInternalAndBalconyAreaIntoFloat < ActiveRecord::Migration
  def self.up
    change_column :residential_sale_details, :internal_area, :string
    change_column :residential_sale_details, :balcony_area, :string
  end

  def self.down
    change_column :residential_sale_details, :internal_area, :integer
    change_column :residential_sale_details, :balcony_area, :integer
  end
end
