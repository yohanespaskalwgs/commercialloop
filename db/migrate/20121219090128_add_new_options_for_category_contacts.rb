class AddNewOptionsForCategoryContacts < ActiveRecord::Migration
  def self.up
    CategoryContact.destroy_all
    CategoryContact.create(:name => "Prospect")
    CategoryContact.create(:name => "Property Owner")
    CategoryContact.create(:name => "Developer")
    CategoryContact.create(:name => "Agent")
    CategoryContact.create(:name => "Tenant")
    CategoryContact.create(:name => "Service Provider")
  end

  def self.down
    CategoryContact.destroy_all
    CategoryContact.create(:name => "Vendor - Past")
    CategoryContact.create(:name => "Vendor - Existing")
    CategoryContact.create(:name => "Vendor - Potential")
    CategoryContact.create(:name => "Landlord - Past")
    CategoryContact.create(:name => "Landlord - Existing")
    CategoryContact.create(:name => "Landlord - Potential")
    CategoryContact.create(:name => "Buyer - Past")
    CategoryContact.create(:name => "Buyer - Potential")
    CategoryContact.create(:name => "Tenant - Past")
    CategoryContact.create(:name => "Tenant - Existing")
    CategoryContact.create(:name => "Tenant - Potential")
    CategoryContact.create(:name => "Other")
  end
end
