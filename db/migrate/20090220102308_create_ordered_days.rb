class CreateOrderedDays < ActiveRecord::Migration
  def self.up
    create_table :ordered_days do |t|
      t.date :date
      t.integer :rental_season_id

    end
  end

  def self.down
    drop_table :ordered_days
  end
end
