class AddColumnAgentCompanyIdToAgentContacts < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :agent_company_id, :integer
  end

  def self.down
    add_column :agent_contacts, :agent_company_id
  end
end
