class AddPrice2ToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :price2, :decimal, :precision => 16, :scale => 3
    add_column :properties, :price2_include_tax, :boolean
  end

  def self.down
    remove_column :properties, :price2_include_tax
    remove_column :properties, :price2
  end
end
