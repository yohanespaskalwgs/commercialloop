class AddSendStatusToAttachment < ActiveRecord::Migration
  def self.up
    add_column :attachments, :send_to_api, :boolean
  end

  def self.down
    remove_column :attachments, :send_to_api
  end
end
