class AddNoteForPropertyToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :notes_for_property, :boolean
  end

  def self.down
    remove_column(:offices, :notes_for_property)
  end
end
