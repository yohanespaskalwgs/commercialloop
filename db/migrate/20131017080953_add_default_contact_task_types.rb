class AddDefaultContactTaskTypes < ActiveRecord::Migration
  def self.up
    ContactTaskType.create({:name => "Follow up on Appraisal", :type => "ContactTaskType"})
    ContactTaskType.create({:name => "Weekly catch-up", :type => "ContactTaskType"})
  end

  def self.down
    TaskType.find_by_name_and_type('Follow up on Appraisal', 'ContactTaskType').destroy
    TaskType.find_by_name_and_type('Weekly catch-up', 'ContactTaskType').destroy
  end
end
