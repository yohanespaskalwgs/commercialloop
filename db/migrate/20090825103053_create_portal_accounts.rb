class CreatePortalAccounts < ActiveRecord::Migration
  def self.up
    create_table :portal_accounts do |t|
      t.string  :portal_name
      t.integer :account_id
      t.string  :account_password
      t.string  :active_feeds
      t.integer :office_id
      t.timestamps
    end
  end

  def self.down
    drop_table :portal_accounts
  end
end
