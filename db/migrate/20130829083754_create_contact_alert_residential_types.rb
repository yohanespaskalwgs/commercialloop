class CreateContactAlertResidentialTypes < ActiveRecord::Migration
  def self.up
    create_table :contact_alert_residential_types do |t|
      t.integer :contact_alert_id
      t.string :residential_type
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_alert_residential_types
  end
end
