class AddStateToContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :state, :string
    add_column :agent_contacts, :city, :string
    add_column :agent_contacts, :email_format, :string
  end

  def self.down
    remove_column :agent_contacts, :state
    remove_column :agent_contacts, :city
    remove_column :agent_contacts, :email_format
  end
end
