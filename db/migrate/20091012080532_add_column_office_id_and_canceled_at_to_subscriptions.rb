class AddColumnOfficeIdAndCanceledAtToSubscriptions < ActiveRecord::Migration
  def self.up
    add_column :subscriptions, :office_id, :integer
    add_column :subscriptions, :canceled_at, :datetime
  end

  def self.down
    remove_column :subscriptions, :office_id
    remove_column :subscriptions, :canceled_at
  end
end
