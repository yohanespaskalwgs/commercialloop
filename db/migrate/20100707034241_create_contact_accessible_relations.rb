class CreateContactAccessibleRelations < ActiveRecord::Migration
  def self.up
    create_table :contact_accessible_relations do |t|
      t.integer :agent_contact_id
      t.integer :accessible_by
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_accessible_relations
  end
end
