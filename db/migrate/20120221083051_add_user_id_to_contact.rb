class AddUserIdToContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :contact_id, :integer
  end

  def self.down
    remove_column :agent_contacts, :contact_id
  end
end
