class AddFieldsToCommercialDetail < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :property_type2, :string
    add_column :commercial_details, :property_type3, :string
    add_column :commercial_details, :method_of_sale, :string
  end

  def self.down
    remove_column(:commercial_details, :property_type2)
    remove_column(:commercial_details, :property_type3)
    remove_column(:commercial_details, :method_of_sale)
  end
end
