class CreateConjunctionals < ActiveRecord::Migration
  def self.up
    create_table :conjunctionals do |t|
      t.string  :conjunctional_rea_agent_id
      t.string  :conjunctional_office_name
      t.timestamps
    end
  end

  def self.down
    drop_table :conjunctionals
  end
end
