class CreateLivestockTickStatuses < ActiveRecord::Migration
  def self.up
    create_table :livestock_tick_statuses do |t|
      t.string :name
      t.timestamps
    end

    LivestockTickStatus.create({:name => "Ticky"})
    LivestockTickStatus.create({:name => "Clean"})
    LivestockTickStatus.create({:name => "NTC Zone"})
    LivestockTickStatus.create({:name => "Clean but originally out of tick country"})
  end

  def self.down
    drop_table :livestock_tick_statuses
  end
end
