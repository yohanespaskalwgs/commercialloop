class AddColumnToCommercialDetails < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :land_frontage, :float
    add_column :commercial_details, :land_depth_left, :float
    add_column :commercial_details, :land_depth_right, :float
    add_column :commercial_details, :land_depth_rear, :float
    add_column :commercial_details, :land_crossover, :string
  end

  def self.down
    remove_column :commercial_details, :land_frontage
    remove_column :commercial_details, :land_depth_left
    remove_column :commercial_details, :land_depth_right
    remove_column :commercial_details, :land_depth_rear
    remove_column :commercial_details, :land_crossover
  end
end
