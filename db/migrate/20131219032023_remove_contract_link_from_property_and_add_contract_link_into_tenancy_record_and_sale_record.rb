class RemoveContractLinkFromPropertyAndAddContractLinkIntoTenancyRecordAndSaleRecord < ActiveRecord::Migration
  def self.up
  	remove_column :properties, :contract_link
  	add_column :sales_records, :contract_link, :string
  	add_column :tenancy_records, :contract_link, :string
  end

  def self.down
  	add_column :properties, :contract_link, :string
  	remove_column :sales_records, :contract_link
  	remove_column :tenancy_records, :contract_link
  end
end
