class AddTxnIdToTableSubscriptionHistory < ActiveRecord::Migration
  def self.up
    add_column :subscription_histories, :txn_id, :string
  end

  def self.down
    remove_column :subscription_histories, :txn_id
  end
end
