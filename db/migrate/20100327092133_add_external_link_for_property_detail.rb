class AddExternalLinkForPropertyDetail < ActiveRecord::Migration
  def self.up
    add_column(:residential_sale_details, :ext_link_1, :string)
    add_column(:residential_lease_details, :ext_link_1, :string)
    add_column(:holiday_lease_details, :ext_link_1, :string)
    add_column(:commercial_details, :ext_link_1, :string)
    add_column(:project_sale_details, :ext_link_1, :string)
    add_column(:business_sale_details, :ext_link_1, :string)
    add_column(:residential_sale_details, :ext_link_2, :string)
    add_column(:residential_lease_details, :ext_link_2, :string)
    add_column(:holiday_lease_details, :ext_link_2, :string)
    add_column(:commercial_details, :ext_link_2, :string)
    add_column(:project_sale_details, :ext_link_2, :string)
    add_column(:business_sale_details, :ext_link_2, :string)
  end

  def self.down
    remove_column(:residential_sale_details, :ext_link_1)
    remove_column(:residential_lease_details, :ext_link_1)
    remove_column(:holiday_lease_details, :ext_link_1)
    remove_column(:commercial_details, :ext_link_1)
    remove_column(:project_sale_details, :ext_link_1)
    remove_column(:business_sale_details, :ext_link_1)
    remove_column(:residential_sale_details, :ext_link_2)
    remove_column(:residential_lease_details, :ext_link_2)
    remove_column(:holiday_lease_details, :ext_link_2)
    remove_column(:commercial_details, :ext_link_2)
    remove_column(:project_sale_details, :ext_link_2)
    remove_column(:business_sale_details, :ext_link_2)
  end
end
