class AddPartialFeedOnPortalAccounts < ActiveRecord::Migration
  def self.up
    add_column :portal_accounts ,:partial_feed, :boolean
  end

  def self.down
    remove_column(:portal_accounts, :partial_feed)
  end
end
