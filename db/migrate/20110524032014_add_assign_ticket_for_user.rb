class AddAssignTicketForUser < ActiveRecord::Migration
  def self.up
    add_column :users, :assign_to_all_ticket, :boolean
  end

  def self.down
    remove_column(:users, :assign_to_all_ticket)
  end
end
