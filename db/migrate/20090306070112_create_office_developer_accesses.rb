class CreateOfficeDeveloperAccesses < ActiveRecord::Migration
  def self.up
    create_table :office_developer_accesses do |t|
      t.integer :office_id
      t.integer :developer_user_id
    end
    
    add_index :office_developer_accesses, [:office_id, :developer_user_id], :name => "index_office_developer_access_office_user"
  end

  def self.down
    drop_table :office_developer_accesses
  end
end
