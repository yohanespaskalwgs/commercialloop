class AddColumnAgentUserIdToSearches < ActiveRecord::Migration
  def self.up
    add_column :searches, :agent_user_id, :integer
  end

  def self.down
    remove_column :searches, :agent_user_id
  end
end
