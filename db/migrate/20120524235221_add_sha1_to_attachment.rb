class AddSha1ToAttachment < ActiveRecord::Migration
  def self.up
    add_column :attachments, :sha1, "char(40)"
  end

  def self.down
    remove_column(:attachments, :sha1)
  end
end
