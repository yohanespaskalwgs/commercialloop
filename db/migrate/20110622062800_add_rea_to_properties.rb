class AddReaToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :cre_id, :string
    add_column :properties, :rca_id, :string
  end

  def self.down
    remove_column :properties, :cre_id
    remove_column :properties, :rca_id
  end
end
