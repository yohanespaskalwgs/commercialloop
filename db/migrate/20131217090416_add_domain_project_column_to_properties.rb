class AddDomainProjectColumnToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :domain_project, :boolean
  end

  def self.down
    remove_column :properties, :domain_project, :boolean
  end
end
