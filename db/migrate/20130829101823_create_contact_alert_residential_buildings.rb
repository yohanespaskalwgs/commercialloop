class CreateContactAlertResidentialBuildings < ActiveRecord::Migration
  def self.up
    create_table :contact_alert_residential_buildings do |t|
      t.integer :contact_alert_id
      t.string :residential_building
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_alert_residential_buildings
  end
end
