class AddNewTaskTypeForReminderAlertLeaseProperty < ActiveRecord::Migration
  def self.up
  	task_type = TaskType.new({:name => "Reminder Alert for Lease Property", :office_id => nil, :status => nil, :type => "PropertyTaskType", :created_at => Time.now, :updated_at => Time.now})
  	task_type.save
  end

  def self.down
  	task_type = TaskType.find(:first, :conditions => ["name = ?", "Reminder Alert for Lease Property"])
  	task_type.destroy unless task_type.blank?
  end
end
