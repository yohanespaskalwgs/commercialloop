class CreateEmailConfigurations < ActiveRecord::Migration
  def self.up
    create_table :email_configurations do |t|
      t.integer :user_id
      t.integer :property_id
      t.string :sender_email
      t.string :sender_name
      t.string :send_bcc
      t.string :contact_name
      t.string :title
      t.text :comments
      t.timestamps
    end
  end

  def self.down
    drop_table :email_configurations
  end
end
