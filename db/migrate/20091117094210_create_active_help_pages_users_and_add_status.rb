class CreateActiveHelpPagesUsersAndAddStatus < ActiveRecord::Migration
  def self.up
#    create_table :active_help_pages_users, :id => false do |t|
#      t.integer :user_id
#      t.integer :active_help_page_id
#    end
    add_column :users, :active_help_status, :boolean
  end

  def self.down
    drop_table :active_help_pages_users
    remove_column :users, :active_help_status
  end
end
