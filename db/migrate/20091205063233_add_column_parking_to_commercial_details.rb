class AddColumnParkingToCommercialDetails < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :parking, :integer
  end

  def self.down
    remove_column :commercial_details, :parking
  end
end
