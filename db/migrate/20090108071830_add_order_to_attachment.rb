class AddOrderToAttachment < ActiveRecord::Migration
  def self.up
    add_column :attachments, :order, :integer
  end

  def self.down
    remove_column :attachments, :order
  end
end
