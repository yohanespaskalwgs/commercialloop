class AddWebUrlToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :website_url, :string
  end

  def self.down
    remove_column :users, :website_url
  end
end
