class AddFeaturePropertyType < ActiveRecord::Migration
  def self.up
    create_table :features, :force => true do |t|
      t.string :name
      t.string :type
      t.string :property_type
      t.integer :office_id
    end
    types = %w(Internal External Security Location Lifestyle General Inclusions)
    current = nil
    IO.readlines(File.join RAILS_ROOT, 'db/raw/Property_features.csv').each do |line|
      words = line.split(',')
      current = Kernel.const_get "#{words[0]}Feature" unless words[0].blank?
      next if current.blank?
      current.create :name => words[1], :property_type => 'ResidentialSale' unless words[1].blank?
      current.create :name => words[2], :property_type => 'ResidentialLease' unless words[2].blank?
      current.create :name => words[3], :property_type => 'Commercial' unless words[3].blank?
      current.create :name => words[4], :property_type => 'HolidayLease' unless words[4].blank?
      current.create :name => words[5], :property_type => 'ProjectSale' unless words[5].blank?
      current.create :name => words[6], :property_type => 'BusinessSale' unless words[6].blank?
    end
  end

  def self.down
    drop_table :features
  end
end
