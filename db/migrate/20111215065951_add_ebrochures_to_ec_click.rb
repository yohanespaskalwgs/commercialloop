class AddEbrochuresToEcClick < ActiveRecord::Migration
  def self.up
    add_column :ecampaign_clicks, :ebrochure_id, :integer
  end

  def self.down
    remove_column :ecampaign_clicks, :ebrochure_id
  end
end
