class AddDisplayWebsiteToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :display_on_website, :boolean
  end

  def self.down
    add_column :properties, :display_on_website
  end
end
