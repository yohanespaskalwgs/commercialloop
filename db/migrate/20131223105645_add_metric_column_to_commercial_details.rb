class AddMetricColumnToCommercialDetails < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :land_frontage_metric, :string
    add_column :commercial_details, :land_depth_left_metric, :string
    add_column :commercial_details, :land_depth_right_metric, :string
    add_column :commercial_details, :land_depth_rear_metric, :string
  end

  def self.down
    remove_column :commercial_details, :land_frontage_metric
    remove_column :commercial_details, :land_depth_left_metric
    remove_column :commercial_details, :land_depth_right_metric
    remove_column :commercial_details, :land_depth_rear_metric
  end
end
