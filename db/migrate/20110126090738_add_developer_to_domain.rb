class AddDeveloperToDomain < ActiveRecord::Migration
  def self.up
    add_column :domains, :developer_id, :integer
  end

  def self.down
    remove_column :domains, :developer_id
  end
end
