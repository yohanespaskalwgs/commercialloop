class AlterTableAgentContacts < ActiveRecord::Migration
  def self.up
  	add_column :agent_contacts, :type_contact, :string, :default => "Internal"
  end

  def self.down
  	remove_column :agent_contacts, :type_contact
  end
end
