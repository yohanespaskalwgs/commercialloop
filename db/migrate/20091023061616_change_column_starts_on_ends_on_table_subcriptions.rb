class ChangeColumnStartsOnEndsOnTableSubcriptions < ActiveRecord::Migration
  def self.up
    change_column :subscriptions, :starts_on, :datetime
    change_column :subscriptions, :ends_on, :datetime
  end

  def self.down
    change_column :subscriptions, :starts_on, :date
    change_column :subscriptions, :ends_on, :date
  end
end
