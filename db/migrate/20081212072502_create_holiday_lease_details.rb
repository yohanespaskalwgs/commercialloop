class CreateHolidayLeaseDetails < ActiveRecord::Migration
  def self.up
    create_table :holiday_lease_details do |t|
      t.text :additional_notes
      t.integer :year_built
      t.integer :carport_spaces, :default => 0
      t.integer :garage_spaces, :default => 0
      t.integer :off_street_spaces, :default => 0
      t.integer :bedrooms, :default => 0
      t.integer :bathrooms, :default => 0
      t.integer :max_persons
      t.string :energy_efficiency_rating
      t.string :energy_star_rating
      t.decimal :longitude, :precision => 12, :scale => 7
      t.decimal :latitude, :precision => 12, :scale => 7
      t.decimal :land_area, :precision => 16, :scale => 3
      t.decimal :floor_area, :precision => 16, :scale => 3
      t.integer :number_of_floors
      t.string :virtual_tour
      t.decimal :mid_season_price, :precision => 16, :scale => 3
      t.decimal :high_season_price, :precision => 16, :scale => 3
      t.decimal :peak_season_price, :precision => 16, :scale => 3
      t.decimal :bond, :precision => 16, :scale => 3
      t.decimal :cleaning_fee, :precision => 16, :scale => 3
      t.integer :holiday_lease_id
    end
  end

  def self.down
    drop_table :holiday_lease_details
  end
end
