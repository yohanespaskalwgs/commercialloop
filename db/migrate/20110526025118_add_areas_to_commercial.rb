class AddAreasToCommercial < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :office_area, :integer
    add_column :commercial_details, :warehouse_area, :integer
    add_column :commercial_details, :retail_area, :integer
    add_column :commercial_details, :other_area, :integer

    add_column :commercial_details, :office_area_metric, :string
    add_column :commercial_details, :warehouse_area_metric, :string
    add_column :commercial_details, :retail_area_metric, :string
    add_column :commercial_details, :other_area_metric, :string
  end

  def self.down
    remove_column(:commercial_details, :office_area)
    remove_column(:commercial_details, :warehouse_area)
    remove_column(:commercial_details, :retail_area)
    remove_column(:commercial_details, :other_area)

    remove_column(:commercial_details, :office_area_metric)
    remove_column(:commercial_details, :warehouse_area_metric)
    remove_column(:commercial_details, :retail_area_metric)
    remove_column(:commercial_details, :other_area_metric)
  end
end
