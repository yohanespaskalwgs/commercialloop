class AddDeveloperContactToAgent < ActiveRecord::Migration
  def self.up
    add_column :agents, :developer_contact_id, :integer
    add_column :agents, :support_contact_id, :integer
    add_column :agents, :upgrade_contact_id, :integer
  end

  def self.down
    remove_column :agents, :developer_contact_id
    remove_column :agents, :support_contact_id
    remove_column :agents, :upgrade_contact_id
  end
end
