class ChangeTaxRatesColumnToDecimalOnResidentialSaleDetails < ActiveRecord::Migration
  def self.up
    change_column :residential_sale_details,  :tax_rate, :decimal, :precision => 16, :scale => 3
  end

  def self.down
    change_column :residential_sale_details,  :tax_rate, :integer
  end
end
