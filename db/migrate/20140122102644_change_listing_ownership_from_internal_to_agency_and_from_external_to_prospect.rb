class ChangeListingOwnershipFromInternalToAgencyAndFromExternalToProspect < ActiveRecord::Migration
  def self.up
  	internal_listing = Property.find(:all, :conditions => ["ownership LIKE ?", "%Internal%"])
  	internal_listing.each do |internal|
  	  internal.update_attributes(:ownership => "Agency")
  	end

  	external_listing = Property.find(:all, :conditions => ["ownership LIKE ?", "%External%"])
  	external_listing.each do |external|
  	  external.update_attributes(:ownership => "Prospect")
  	end
  end

  def self.down
  	agency_listing = Property.find(:all, :conditions => ["ownership LIKE ?", "%Agency%"])
	agency_listing.each do |agency|
  	  agency.update_attributes(:ownership => "Internal")
  	end

  	prospect_listing = Property.find(:all, :conditions => ["ownership LIKE ?", "%Prospect%"])
	prospect_listing.each do |prospect|
  	  prospect.update_attributes(:ownership => "External")
  	end
  end
end
