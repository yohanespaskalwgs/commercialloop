class AddHideUnusedExportsColumnToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :hide_unused_exports, :boolean
  end

  def self.down
    remove_column :offices, :hide_unused_exports
  end
end
