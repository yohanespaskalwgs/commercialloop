class AddBoomCompanyname < ActiveRecord::Migration
  def self.up
    add_column :offices, :boom_company_name, :string
  end

  def self.down
    remove_column :offices, :boom_company_name
  end
end
