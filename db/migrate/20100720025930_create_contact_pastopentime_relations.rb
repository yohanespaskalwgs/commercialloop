class CreateContactPastopentimeRelations < ActiveRecord::Migration
  def self.up
    create_table :contact_pastopentime_relations do |t|      
      t.integer :past_opentime_id
      t.integer :agent_contact_id
      t.integer :contact_note_id
      t.boolean :alert
      t.boolean :contract
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_pastopentime_relations
  end
end
