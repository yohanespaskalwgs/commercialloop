class AddSendProcessColumnToEbrochures < ActiveRecord::Migration
  def self.up
    add_column :ebrochures, :send_process, :boolean
  end

  def self.down
    remove_column :ebrochures, :send_process
  end
end
