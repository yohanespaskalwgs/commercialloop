class AddColumnIndustrySubCategoryColumn < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :industry_sub_category_id, :integer
  end

  def self.down
    remove_column :agent_contacts, :industry_sub_category_id
  end
end
