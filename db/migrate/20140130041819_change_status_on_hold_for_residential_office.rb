class ChangeStatusOnHoldForResidentialOffice < ActiveRecord::Migration
  def self.up
  	properties = Property.find(:all, :conditions => ["office_id = ? AND status = ?", 244, 7])
  	unless properties.blank?
  	  properties.each do |property|
  	  	property.update_attributes!({:skip_validation => true, :status => 8})
  	  end
  	end
  end

  def self.down
  	properties = Property.find(:all, :conditions => ["office_id = ? AND status = ?", 244, 8])
  	unless properties.blank?
  	  properties.each do |property|
  	  	property.update_attributes!({:skip_validation => true, :status => 7})
  	  end
  	end
  end
end
