class AddLimitAccessToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :limit_access, :boolean
  end

  def self.down
    remove_column :offices, :limit_access
  end
end
