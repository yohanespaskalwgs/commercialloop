class AddFieldsToCommercial < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :listings_this_address, :boolean
    add_column :commercial_details, :virtual_tour2, :string
    add_column :commercial_details, :virtual_tour3, :string
    add_column :commercial_details, :ext_link_3, :string
    add_column :commercial_details, :video_url, :string
  end

  def self.down
    remove_column(:commercial_details, :listings_this_address)
    remove_column(:commercial_details, :virtual_tour2)
    remove_column(:commercial_details, :virtual_tour3)
    remove_column(:commercial_details, :ext_link_3)
    remove_column(:commercial_details, :video_url)
  end
end
