class AddImServiceAndImUsernameInUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :im_username, :string
    add_column :users, :im_service, :string
  end

  def self.down
    remove_column :users, :im_service
    remove_column :users, :im_username
  end
end
