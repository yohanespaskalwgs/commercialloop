class CreateCategoryContacts < ActiveRecord::Migration
  def self.up
    create_table :category_contacts do |t|
      t.string :name
      t.timestamps
    end
    CategoryContact.destroy_all
    CategoryContact.create(:name => "Vendor - Past")
    CategoryContact.create(:name => "Vendor - Existing")
    CategoryContact.create(:name => "Vendor - Potential")
    CategoryContact.create(:name => "Landlord - Past")
    CategoryContact.create(:name => "Landlord - Existing")
    CategoryContact.create(:name => "Landlord - Potential")
    CategoryContact.create(:name => "Buyer - Past")
    CategoryContact.create(:name => "Buyer - Potential")
    CategoryContact.create(:name => "Tenant - Past")
    CategoryContact.create(:name => "Tenant - Existing")
    CategoryContact.create(:name => "Tenant - Potential")
    CategoryContact.create(:name => "Other")
  end

  def self.down
    drop_table :category_contacts
  end
end
