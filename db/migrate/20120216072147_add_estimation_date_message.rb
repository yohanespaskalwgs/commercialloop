class AddEstimationDateMessage < ActiveRecord::Migration
  def self.up
    add_column :messages, :estimated_at, :datetime
  end

  def self.down
    remove_column :messages, :estimated_at
  end
end
