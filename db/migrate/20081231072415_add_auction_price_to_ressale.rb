class AddAuctionPriceToRessale < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :auction_price, :decimal, :precision => 16, :scale => 3
  end

  def self.down
    drop_column :residential_sale_details, :auction_price
  end
end
