class AddColumnsToClearingSaleDetails < ActiveRecord::Migration
  def self.up
    add_column :clearing_sale_details, :lot_number, :string
    add_column :clearing_sale_details, :condition, :text
  end

  def self.down
    remove_column :clearing_sale_details, :lot_number
    remove_column :clearing_sale_details, :condition
  end
end
