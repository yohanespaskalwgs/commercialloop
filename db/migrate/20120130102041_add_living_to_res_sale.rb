class AddLivingToResSale < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :living_area, :string
    add_column :residential_sale_details, :living_area_metric, :string
    add_column :residential_sale_details, :method_of_sale, :string
  end

  def self.down
    remove_column :residential_sale_details, :living_area
    remove_column :residential_sale_details, :living_area_metric
    remove_column :residential_sale_details, :method_of_sale
  end
end
