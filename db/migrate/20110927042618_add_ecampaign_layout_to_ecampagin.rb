class AddEcampaignLayoutToEcampagin < ActiveRecord::Migration
  def self.up
    add_column :ecampaigns, :stored_layout, :longblob
  end

  def self.down
    remove_column :ecampaigns, :stored_layout
  end
end
