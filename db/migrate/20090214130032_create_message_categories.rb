class CreateMessageCategories < ActiveRecord::Migration
  def self.up
    create_table :message_categories do |t|
      t.integer :developer_id
      t.string :name

      t.timestamps
    end
    add_index :message_categories, :developer_id
  end

  def self.down
    remove_index :message_categories, :developer_id
    drop_table :message_categories
  end
end
