class RenameColumnCategoryContactIdOnCompanyCategoryRelation < ActiveRecord::Migration
  def self.up
    rename_column :company_category_relations, :category_contact_id, :category_company_id
  end

  def self.down
    rename_column :company_category_relations, :category_company_id, :category_contact_id
  end
end
