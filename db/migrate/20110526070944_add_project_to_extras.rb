class AddProjectToExtras < ActiveRecord::Migration
  def self.up
    add_column :extras, :projects, :boolean
  end

  def self.down
    remove_column :extras, :projects
  end
end
