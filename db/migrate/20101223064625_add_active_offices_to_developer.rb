class AddActiveOfficesToDeveloper < ActiveRecord::Migration
  def self.up
    add_column :developers, :active_offices, :boolean
  end

  def self.down
    remove_column :developers, :active_offices
  end
end
