class AddColumnToCommercialBuildingDetails < ActiveRecord::Migration
  def self.up
    add_column :commercial_building_details, :energy_efficiency_rating, :string
    add_column :commercial_building_details, :area_range, :integer
    add_column :commercial_building_details, :area_range_to, :integer
    add_column :commercial_building_details, :rental_price, :integer
    add_column :commercial_building_details, :rental_price_to, :integer
    add_column :commercial_building_details, :sale_price_from, :integer
    add_column :commercial_building_details, :sale_price_to, :integer
    add_column :commercial_building_details, :carport_spaces, :integer
    add_column :commercial_building_details, :listings_this_address, :boolean
    add_column :commercial_building_details, :virtual_tour, :string
    add_column :commercial_building_details, :virtual_tour2, :string
    add_column :commercial_building_details, :virtual_tour3, :string
    add_column :commercial_building_details, :ext_link_3, :string
    add_column :commercial_building_details, :video_url, :string
    add_column :commercial_building_details, :lease_further_option, :string
    add_column :commercial_building_details, :rent_type, :string
    add_column :commercial_building_details, :outgoings_paid_by, :integer
    add_column :commercial_building_details, :tenancy_option, :integer
    add_column :commercial_building_details, :all_the_building, :integer
    add_column :commercial_building_details, :all_the_floor, :boolean
    add_column :commercial_building_details, :parking, :string
    add_column :commercial_building_details, :property_url, :string
    add_column :commercial_building_details, :ext_link_1, :string
    add_column :commercial_building_details, :ext_link_2, :string
    add_column :commercial_building_details, :method_of_sale, :string
    add_column :commercial_building_details, :authority, :string
    add_column :commercial_building_details, :highlight1, :string
    add_column :commercial_building_details, :highlight2, :string
    add_column :commercial_building_details, :highlight3, :string
    add_column :commercial_building_details, :current_leased, :integer
    add_column :commercial_building_details, :lease_end, :string
    add_column :commercial_building_details, :rent_review, :string
    add_column :commercial_building_details, :current_rent, :integer
    add_column :commercial_building_details, :current_rent_include_tax, :integer
    add_column :commercial_building_details, :outgoings_paid_by_tenant, :integer
    add_column :commercial_building_details, :return_percent, :integer
    add_column :commercial_building_details, :bond, :integer
    add_column :commercial_building_details, :annual_outgoings, :integer
    add_column :commercial_building_details, :annual_outgoings_include_tax, :integer
    add_column :commercial_building_details, :date_available, :datetime
    add_column :commercial_building_details, :forthcoming_auction, :boolean
    add_column :commercial_building_details, :auction_place, :string
    add_column :commercial_building_details, :price_include_tax, :integer
    add_column :commercial_building_details, :auction_date, :datetime
    add_column :commercial_building_details, :auction_time, :string

  end

  def self.down
    remove_column :commercial_building_details, :energy_efficiency_rating
    remove_column :commercial_building_details, :area_range
    remove_column :commercial_building_details, :area_range_to
    remove_column :commercial_building_details, :rental_price
    remove_column :commercial_building_details, :rental_price_to
    remove_column :commercial_building_details, :sale_price_from
    remove_column :commercial_building_details, :sale_price_to
    remove_column :commercial_building_details, :carport_spaces
    remove_column :commercial_building_details, :listings_this_address
    remove_column :commercial_building_details, :virtual_tour
    remove_column :commercial_building_details, :virtual_tour2
    remove_column :commercial_building_details, :virtual_tour3
    remove_column :commercial_building_details, :ext_link_3
    remove_column :commercial_building_details, :video_url
    remove_column :commercial_building_details, :lease_further_option
    remove_column :commercial_building_details, :rent_type
    remove_column :commercial_building_details, :outgoings_paid_by
    remove_column :commercial_building_details, :tenancy_option
    remove_column :commercial_building_details, :all_the_building
    remove_column :commercial_building_details, :all_the_floor
    remove_column :commercial_building_details, :parking
    remove_column :commercial_building_details, :property_url
    remove_column :commercial_building_details, :ext_link_1
    remove_column :commercial_building_details, :ext_link_2
    remove_column :commercial_building_details, :method_of_sale
    remove_column :commercial_building_details, :authority
    remove_column :commercial_building_details, :highlight1
    remove_column :commercial_building_details, :highlight2
    remove_column :commercial_building_details, :highlight3
    remove_column :commercial_building_details, :current_leased
    remove_column :commercial_building_details, :lease_end
    remove_column :commercial_building_details, :rent_review
    remove_column :commercial_building_details, :current_rent
    remove_column :commercial_building_details, :current_rent_include_tax
    remove_column :commercial_building_details, :outgoings_paid_by_tenant
    remove_column :commercial_building_details, :bond
    remove_column :commercial_building_details, :annual_outgoings
    remove_column :commercial_building_details, :annual_outgoings_include_tax
    remove_column :commercial_building_details, :date_available
    remove_column :commercial_building_details, :forthcoming_auction
    remove_column :commercial_building_details, :auction_place
    remove_column :commercial_building_details, :price_include_tax
    remove_column :commercial_building_details, :auction_date
    remove_column :commercial_building_details, :auction_time
  end
end
