class AddNewDevelopmentNameToExtras < ActiveRecord::Migration
  def self.up
    add_column :extras, :new_development_name, :string
  end

  def self.down
    remove_column :extras, :new_development_name
  end
end
