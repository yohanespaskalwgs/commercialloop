class AddFeedIdToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :feed_agent_id, :string, :limit => 50
  end

  def self.down
    remove_column :offices, :feed_agent_id
  end
end
