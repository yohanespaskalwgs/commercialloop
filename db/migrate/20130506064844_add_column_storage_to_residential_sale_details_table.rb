class AddColumnStorageToResidentialSaleDetailsTable < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :storage, :boolean
  end

  def self.down
    remove_column :residential_sale_details, :storage
  end
end
