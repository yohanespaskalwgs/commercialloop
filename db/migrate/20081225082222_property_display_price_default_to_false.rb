class PropertyDisplayPriceDefaultToFalse < ActiveRecord::Migration
  def self.up
    change_column_default :properties, :display_price, false
  end

  def self.down
    change_column_default :properties, :display_price, nil
  end
end
