class AddColumnContactTypeToAgentContacts < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :contact_type, :string
  end

  def self.down
    remove_column :agent_contacts, :contact_type
  end
end
