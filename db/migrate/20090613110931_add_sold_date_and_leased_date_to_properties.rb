class AddSoldDateAndLeasedDateToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :sold_on, :date
    add_column :properties, :sold_price, :integer
    add_column :properties, :leased_on, :date
    add_column :properties, :leased_price, :integer
  end

  def self.down
    remove_column :properties, :leased_price
    remove_column :properties, :leased_on
    remove_column :properites, :sold_price
    remove_column :properties, :sold_on
  end
end
