class AddColumnForSocialNetInfoOnTableUser < ActiveRecord::Migration
  def self.up
    add_column :users, :facebook_username, :string
    add_column :users, :twitter_username, :string
    add_column :users, :linkedin_username, :string
  end

  def self.down
    remove_column :users, :facebook_username
    remove_column :users, :twitter_username
    remove_column :users, :linkedin_username
  end
end
