class AddStatusToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :status, :string
  end

  def self.down
    remove_column :offices, :status
  end
end
