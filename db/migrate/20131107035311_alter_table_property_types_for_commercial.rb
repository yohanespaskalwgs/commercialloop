class AlterTablePropertyTypesForCommercial < ActiveRecord::Migration
  def self.up
  	unused_property_types = PropertyType.find(:all, :conditions => ["category = ? AND name IN (?)", 'Commercial', ['Institutional', 'Investment', 'Parking Space', 'Residential']])
  	unused_property_types.each do |commercial_property_type|
  	  commercial_property_type.destroy
  	end
  end

  def self.down
  	PropertyType.create(
  		[
  			{:id => 53, :category => "Commercial", :name => "Institutional"},
  			{:id => 54, :category => "Commercial", :name => "Investment"},
  			{:id => 58, :category => "Commercial", :name => "Parking Space"},
  			{:id => 59, :category => "Commercial", :name => "Residential"}
  		]
  	)
  end
end
