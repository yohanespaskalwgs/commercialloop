class AddStroreToEalert < ActiveRecord::Migration
  def self.up
    add_column :ealerts, :stored_layout, :longblob
    add_column :ealerts, :stored_data, :longblob
  end

  def self.down
    remove_column :ealerts, :stored_layout
    remove_column :ealerts, :stored_data
  end
end
