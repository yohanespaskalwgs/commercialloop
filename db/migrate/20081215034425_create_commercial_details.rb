class CreateCommercialDetails < ActiveRecord::Migration
  def self.up
    create_table :commercial_details do |t|
      t.string :building_name
      t.text :additional_notes
      t.integer :year_built
      t.string :energy_efficiency_rating
      t.string :energy_star_rating
      t.decimal :longitude, :precision => 12, :scale => 7
      t.decimal :latitude, :precision => 12, :scale => 7
      t.decimal :land_area, :precision => 16, :scale => 3
      t.decimal :floor_area, :precision => 16, :scale => 3
      t.integer :number_of_floors
      t.boolean :all_the_building, :default => false
      t.boolean :all_the_floor, :default => false
      t.string :zoning
      t.string :patron_capacity
      t.boolean :current_leased
      t.string :lease_commencement
      t.string :lease_end
      t.string :lease_option
      t.string :lease_plus_another
      t.string :rent_review
      t.decimal :current_rent, :precision => 16, :scale => 3
      t.boolean :current_rent_include_tax
      t.decimal :current_outgoings, :precision => 16, :scale => 3
      t.boolean :current_outgoings_include_tax
      t.string :outgoings_paid_by_tenant
      t.decimal :return_percent, :precision => 4, :scale => 2
      t.string :virtual_tour
      t.decimal :bond, :precision => 16, :scale => 3
      t.decimal :annual_outgoings, :precision => 16, :scale => 3
      t.boolean :annual_outgoings_include_tax
      t.date :date_available
      t.boolean :forthcoming_auction
      t.string :auction_place
      t.datetime :auction_datetime
      t.boolean :price_include_tax
      t.integer :commercial_id
    end
  end

  def self.down
    drop_table :commercial_details
  end
end
