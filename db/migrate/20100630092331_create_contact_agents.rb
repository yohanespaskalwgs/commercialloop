class CreateContactAgents < ActiveRecord::Migration
  def self.up
    create_table :agent_contacts do |t|
      t.string :title
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :home_number
      t.string :work_number
      t.string :mobile_number
      t.string :contact_number
      t.string :fax_number
      t.integer :country_id
      t.string :unit_number
      t.string :street_number
      t.string :street_name
      t.string :province
      t.string :suburb
      t.string :post_code
      t.integer :category_contact_id
      t.integer :assigned_to
      t.integer :accessible_by
      t.integer :heard_from_id
      t.timestamps
    end
  end

  def self.down
    drop_table :agent_contacts
  end
end
