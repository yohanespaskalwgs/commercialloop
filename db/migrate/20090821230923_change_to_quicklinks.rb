class ChangeToQuicklinks < ActiveRecord::Migration
  def self.up
    rename_table :agent_updates, :agent_quicklinks
    rename_table :developer_updates, :developer_quicklinks    
  end

  def self.down
    rename_table :agent_quicklinks, :agent_updates
    rename_table :developer_quicklinks, :developer_updates    
  end
end
