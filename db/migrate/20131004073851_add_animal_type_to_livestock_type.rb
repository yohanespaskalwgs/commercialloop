class AddAnimalTypeToLivestockType < ActiveRecord::Migration
  def self.up
    add_column :livestock_types, :animal_type, :string
  end

  def self.down
  end
end
