class AddDeltaFieldOnProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :delta, :boolean, :default => true, :null => false
  end

  def self.down
    remove_column(:properties, :delta)
  end
end
