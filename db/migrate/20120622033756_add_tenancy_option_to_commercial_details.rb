class AddTenancyOptionToCommercialDetails < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :tenancy_option, :integer
  end

  def self.down
    remove_column :commercial_details, :tenancy_option
  end
end
