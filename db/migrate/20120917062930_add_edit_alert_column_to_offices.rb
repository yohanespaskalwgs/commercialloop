class AddEditAlertColumnToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :edit_alert, :boolean
  end

  def self.down
    remove_column :offices, :edit_alert
  end
end
