class AddPositionToAttachment < ActiveRecord::Migration
  def self.up
    add_column :attachments, :position, :integer, :default => 0
  end

  def self.down
    remove_column :attachments, :position
  end
end
