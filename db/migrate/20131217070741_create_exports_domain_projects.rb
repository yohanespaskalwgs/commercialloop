class CreateExportsDomainProjects < ActiveRecord::Migration
  def self.up
    create_table :exports_domain_projects do |t|
      t.string :project_name
      t.integer :domain_id
      t.integer :developer_id
      t.boolean :enabled
      t.timestamps
    end
  end

  def self.down
    drop_table :exports_domain_projects
  end
end
