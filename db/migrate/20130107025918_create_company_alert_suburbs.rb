class CreateCompanyAlertSuburbs < ActiveRecord::Migration
  def self.up
    create_table :company_alert_suburbs do |t|
      t.integer :company_alert_id
      t.string :suburb
      t.timestamps
    end
  end

  def self.down
    drop_table :company_alert_suburbs
  end
end
