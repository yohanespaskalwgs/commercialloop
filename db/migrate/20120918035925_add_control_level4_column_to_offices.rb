class AddControlLevel4ColumnToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :control_level4, :boolean
  end

  def self.down
    remove_column :offices, :control_level4
  end
end
