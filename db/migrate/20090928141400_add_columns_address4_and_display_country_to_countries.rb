class AddColumnsAddress4AndDisplayCountryToCountries < ActiveRecord::Migration
  def self.up
    add_column :countries, :address4, :string
    add_column :countries, :display_country, :boolean
  end

  def self.down
    remove_column :countries, :address4
    remove_column :countries, :display_country
  end
end
