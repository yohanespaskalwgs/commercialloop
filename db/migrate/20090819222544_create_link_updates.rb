class CreateLinkUpdates < ActiveRecord::Migration
  def self.up
    add_column :news, :data, :text
    remove_column :news, :url
    remove_column :news, :heading
    remove_column :news, :type    
    add_column :quicklinks, :data, :text
    remove_column :quicklinks, :url
    remove_column :quicklinks, :key
    remove_column :quicklinks, :type        
  end

  def self.down
    remove_column :news, :data
    add_column :news, :url
    add_column :news, :heading
    add_column :news, :type
    remove_column :quicklinks, :data    
    add_column :quicklinks, :url
    add_column :quicklinks, :heading
    add_column :quicklinks, :type    
  end
end