class AddNextPaymentDateToSubscriptionAndErrorLogToSubscriptionHistory < ActiveRecord::Migration
  def self.up
    add_column :subscriptions, :next_payment_date, :date
    add_column :subscription_histories, :error_log, :string
  end

  def self.down
    remove_column :subscriptions, :next_payment_date
    remove_column :subscription_histories, :error_log
  end
end
