class UpdateNoteTypeForProperty < ActiveRecord::Migration
  def self.up
    NoteType.update_all( "status = 'system'", "id = 17" )
  end

  def self.down
    NoteType.update_all( "status = 'property_note'", "id = 17" )
  end
end
