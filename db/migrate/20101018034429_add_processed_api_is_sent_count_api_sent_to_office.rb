class AddProcessedApiIsSentCountApiSentToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :api_is_sent, :boolean
    add_column :offices, :count_api_sent, :integer
    add_column :offices, :processed, :boolean
  end

  def self.down
    remove_column :offices, :api_is_sent
    remove_column :offices, :count_api_sent
    remove_column :offices, :processed
  end
end
