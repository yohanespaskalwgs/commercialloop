class AddCarsSpaceCommercial < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :carport_spaces, :integer
  end

  def self.down
    remove_column(:commercial_details, :carport_spaces)
  end
end
