class AddRelationsBetweenTenancyRecordsAndTasks < ActiveRecord::Migration
  def self.up
  	add_column :tasks, :tenancy_record_id, :integer
  end

  def self.down
  	remove_column :tasks, :tenancy_record_id
  end
end
