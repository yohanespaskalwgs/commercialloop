class AddNewTableProviderAccounts < ActiveRecord::Migration
  def self.up
    create_table :provider_accounts do |t|
      t.string :provider , :limit => 255
      t.string :username , :limit => 255
      t.string :password , :limit => 255
      t.string :office , :limit => 255
      t.timestamps
    end

    add_index :provider_accounts, :provider
  end

  def self.down
    drop_table :provider_accounts
  end
end
