class CreateExportReports < ActiveRecord::Migration
  def self.up
    create_table :export_reports do |t|
      t.integer  :office_id
      t.integer  :property_id
      t.string   :portal
      t.integer  :portal_id
      t.datetime :last_sent
      t.string   :filename
      t.datetime :report_back
      t.text   :report_status
      t.timestamps
    end
  end

  def self.down
    drop_table :export_reports
  end
end
