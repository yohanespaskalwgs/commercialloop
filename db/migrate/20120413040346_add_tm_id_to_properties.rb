class AddTmIdToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :tm_id, :string
  end

  def self.down
    remove_column :properties, :tm_id
  end
end
