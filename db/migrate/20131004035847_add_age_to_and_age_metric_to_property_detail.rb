class AddAgeToAndAgeMetricToPropertyDetail < ActiveRecord::Migration
  def self.up
    add_column :livestock_sale_details, :age_to, :integer
    add_column :livestock_sale_details, :age_metric, :string
  end

  def self.down
    remove_column :livestock_sale_details, :age_to
    remove_column :livestock_sale_details, :age_metric
  end
end
