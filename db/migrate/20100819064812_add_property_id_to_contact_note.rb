class AddPropertyIdToContactNote < ActiveRecord::Migration
  def self.up
    add_column :contact_notes, :property_id, :integer
  end

  def self.down
    remove_column(:contact_notes, :property_id)
  end
end
