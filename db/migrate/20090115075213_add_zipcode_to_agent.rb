class AddZipcodeToAgent < ActiveRecord::Migration
  def self.up
    add_column :agents, :zipcode, :string, :limit => 10
  end

  def self.down
    remove_column :agents, :zipcode
  end
end
