class ChangeIndustryCategoryNameFromCommercialSaleLeaseToCommercial < ActiveRecord::Migration
  def self.up
   industry_category = IndustryCategory.find_by_name("Commercial Sale/Lease")
   industry_category.update_attributes(:name => "Commercial")
  end

  def self.down
    industry_category = IndustryCategory.find_by_name("Commercial")
    industry_category.update_attributes(:name => "Commercial Sale/Lease")
  end
end
