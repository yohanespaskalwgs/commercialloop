class AddAutosubcribeToPortalCombine < ActiveRecord::Migration
  def self.up
    add_column :portal_combineds, :autosubcribe, :boolean
  end

  def self.down
    remove_column :portal_combineds, :autosubcribe
  end
end
