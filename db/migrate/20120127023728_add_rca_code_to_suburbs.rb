class AddRcaCodeToSuburbs < ActiveRecord::Migration
  def self.up
    add_column :suburbs, :rca_code, :integer
  end

  def self.down
    remove_column :suburbs, :rca_code
  end
end
