class CreateMessageComments < ActiveRecord::Migration
  def self.up
    create_table :message_comments do |t|
      t.references :message
      t.text :comment
      t.integer :commenter_id

      t.timestamps
    end
    
    add_index :message_comments, :message_id
  end

  def self.down
    drop_table :message_comments
  end
end
