class CreateContactAlerts < ActiveRecord::Migration
  def self.up
    create_table :contact_alerts do |t|
      t.integer :agent_contact_id
      t.string :alert_type
      t.string :listing_type
      t.string :property_type
      t.integer :min_bedroom
      t.integer :min_bathroom
      t.integer :min_carspace
      t.integer :min_price
      t.integer :max_price
      t.string :suburb
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_alerts
  end
end
