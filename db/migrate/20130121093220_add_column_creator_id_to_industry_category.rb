class AddColumnCreatorIdToIndustryCategory < ActiveRecord::Migration
  def self.up
    add_column :industry_categories, :creator_id, :integer
  end

  def self.down
    remove_column :industry_categories, :creator_id
  end
end
