class CreateNewDevelopments < ActiveRecord::Migration
  def self.up
    create_table :new_development_details do |t|
      t.integer :new_development_id
      t.string  :development_name
      t.string  :development_website
      t.integer :year_built
      t.integer :number_of_floors
      t.integer :number_of_properties
      t.timestamps
    end
  end

  def self.down
    drop_table :new_development_details
  end
end
