class CreateFeaturesProperties < ActiveRecord::Migration
  def self.up
    create_table :features_properties do |t|
      t.integer :feature_id
      t.integer :property_id
    end
  end

  def self.down
    drop_table :features_properties
  end
end
