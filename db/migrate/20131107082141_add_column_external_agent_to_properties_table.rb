class AddColumnExternalAgentToPropertiesTable < ActiveRecord::Migration
  def self.up
  	add_column :properties, :external_agent_id, :integer
  	add_column :properties, :external_agent_first_name, :string
  	add_column :properties, :external_agent_last_name, :string
  	add_column :properties, :external_agent_email, :string
  	add_column :properties, :external_agent_phone, :string
  	add_column :properties, :external_agent_home_number, :string
  	add_column :properties, :external_agent_work_number, :string
  	add_column :properties, :external_agent_mobile_number, :string
  end

  def self.down
  	remove_column :properties, :external_agent_id
  	remove_column :properties, :external_agent_first_name
  	remove_column :properties, :external_agent_last_name
  	remove_column :properties, :external_agent_email
  	remove_column :properties, :external_agent_phone
  	remove_column :properties, :external_agent_home_number
  	remove_column :properties, :external_agent_work_number
  	remove_column :properties, :external_agent_mobile_number
  end
end
