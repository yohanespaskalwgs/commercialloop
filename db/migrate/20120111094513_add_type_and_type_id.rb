class AddTypeAndTypeId < ActiveRecord::Migration
  def self.up
    add_column :ecampaign_clicks, :type_ecampaign, :string
    add_column :ecampaign_tracks, :type_ecampaign, :string
    add_column :ecampaign_unsubcribes, :type_ecampaign, :string

    add_column :ecampaign_clicks, :type_ecampaign_id, :integer
    add_column :ecampaign_tracks, :type_ecampaign_id, :integer
    add_column :ecampaign_unsubcribes, :type_ecampaign_id, :integer
  end

  def self.down
    remove_column :ecampaign_clicks, :type_ecampaign
    remove_column :ecampaign_tracks, :type_ecampaign
    remove_column :ecampaign_unsubcribes, :type_ecampaign

    remove_column :ecampaign_clicks, :type_ecampaign_id
    remove_column :ecampaign_tracks, :type_ecampaign_id
    remove_column :ecampaign_unsubcribes, :type_ecampaign_id
  end
end
