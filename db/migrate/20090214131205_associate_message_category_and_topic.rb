class AssociateMessageCategoryAndTopic < ActiveRecord::Migration
  def self.up   
    
    %w( Systems Exports Websites Updates ).each { |c| MessageCategory.create(:name => c) }
    
    add_column :message_topics, :category_id, :integer
    add_index :message_topics, :category_id
    
    MessageTopic.reset_column_information
    
    MessageTopic.all.each { |t| t.update_attribute(:category_id, MessageCategory.first.id) }
  end

  def self.down
    remove_column :message_topics, :category_id
    MessageCategory.delete_all
  end
end
