class AddBoomUsernameAndPasswordToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :boom_username, :string
    add_column :offices, :boom_password, :string
  end

  def self.down
    remove_column :offices, :boom_username
    remove_column :offices, :boom_password
  end
end
