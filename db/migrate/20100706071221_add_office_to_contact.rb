class AddOfficeToContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :agent_id,:integer
    add_column :agent_contacts, :office_id,:integer    
  end

  def self.down
    remove_column(:agent_contacts, :agent_id)
    remove_column(:agent_contacts, :office_id)
  end
end
