class ManageIndex < ActiveRecord::Migration
  def self.up
    add_index :properties, :country
    add_index :properties, :suburb
    add_index :properties, :street
    add_index :properties, :street_number
    add_index :properties, :property_type
    add_index :properties, :type
    add_index :properties, :primary_contact_id
    add_index :properties, :latitude
    add_index :properties, :longitude
    add_index :properties, :agent_user_id
    add_index :properties, :save_status
    add_index :properties, :unit_number
    add_index :properties, :status
    add_index :properties, :office_id
    add_index :properties, :agent_id
    add_index :users, :first_name
    add_index :users, :last_name
  end

  def self.down
    remove_index :properties, :country
    remove_index :properties, :suburb
    remove_index :properties, :street
    remove_index :properties, :street_number
    remove_index :properties, :property_type
    remove_index :properties, :type
    remove_index :properties, :primary_contact_id
    remove_index :properties, :latitude
    remove_index :properties, :longitude
    remove_index :properties, :agent_user_id
    remove_index :properties, :save_status
    remove_index :properties, :unit_number
    remove_index :properties, :status
    remove_index :properties, :office_id
    remove_index :properties, :agent_id
    remove_index :users, :first_name
    remove_index :users, :last_name
  end
end
