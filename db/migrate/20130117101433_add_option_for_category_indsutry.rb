class AddOptionForCategoryIndsutry < ActiveRecord::Migration
  def self.up
    CategoryIndustry.destroy_all
    CategoryIndustry.create(:name => "Residential Sale")
    CategoryIndustry.create(:name => "Residential Lease")
    CategoryIndustry.create(:name => "Commercial Sale/Lease")
    CategoryIndustry.create(:name => "House & Land")
    CategoryIndustry.create(:name => "Business Sale")
    CategoryIndustry.create(:name => "Project Sale")
  end

  def self.down
    CategoryIndustry.destroy_all
  end
end
