class CreatePdfConfigurations < ActiveRecord::Migration
  def self.up
    create_table :pdf_configurations do |t|
      t.integer :office_id
      t.string :theme
      t.string :font_color
      t.string :font_family
      t.string :heading_color
      t.string :heading_font_color
      t.string :sidebar_color
      t.string :main_content_color
      t.timestamps
    end
  end

  def self.down
    drop_table :pdf_configurations
  end
end
