class CreateCompanyOffers < ActiveRecord::Migration
  def self.up
    create_table :company_offers do |t|
      t.integer :property_id
      t.string :agent_company_id
      t.date :date
      t.text :detail
      t.integer :company_note_id
      t.timestamps
    end
  end

  def self.down
    drop_table :company_offers
  end
end
