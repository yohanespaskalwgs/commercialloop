class AddPriceFromToCommercial < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :sale_price_from, :integer
    add_column :commercial_details, :sale_price_to, :integer
  end

  def self.down
    remove_column(:commercial_details, :sale_price_from)
    remove_column(:commercial_details, :sale_price_to)
  end
end
