class CreateEbrochures < ActiveRecord::Migration
  def self.up
    create_table :ebrochures do |t|
      t.integer  :property_id
      t.string   :title
      t.datetime :sent_at
      t.timestamps
    end
  end

  def self.down
    drop_table :ebrochures
  end
end
