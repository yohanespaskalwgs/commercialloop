class AddTypeToQuicklinks < ActiveRecord::Migration
  def self.up
    add_column :quicklinks, :type, :string
    add_column :news, :type, :string    
  end

  def self.down
    remove_column :quicklinks, :type
    remove_column :news, :type   
  end
end
