class AddStoredData < ActiveRecord::Migration
  def self.up
    add_column :ecampaigns, :stored_data, :longblob
  end

  def self.down
    remove_column :ecampaigns, :stored_data
  end
end
