class AddPropertyOwnerAccessToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices,:property_owner_access,:boolean
  end

  def self.down
    remove_column :offices,:property_owner_access
  end
end
