class AddPortalExportToDomain < ActiveRecord::Migration
  def self.up
    add_column :domains, :portal_export, :boolean
  end

  def self.down
    remove_column :domains, :portal_export
  end
end
