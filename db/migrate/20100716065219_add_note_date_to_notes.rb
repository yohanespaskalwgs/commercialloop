class AddNoteDateToNotes < ActiveRecord::Migration
  def self.up
    add_column :contact_notes, :note_date, :date
  end

  def self.down
    remove_column(:contact_notes, :note_date)
  end
end
