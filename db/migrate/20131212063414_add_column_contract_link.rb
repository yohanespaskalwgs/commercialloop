class AddColumnContractLink < ActiveRecord::Migration
  def self.up
  	add_column :properties, :contract_link, :string
  end

  def self.down
  	remove_column :properties, :contract_link
  end
end
