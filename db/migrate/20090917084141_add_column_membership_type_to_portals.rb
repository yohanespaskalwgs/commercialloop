class AddColumnMembershipTypeToPortals < ActiveRecord::Migration
  def self.up
    add_column :portals, :membership_type, :string
  end

  def self.down
    remove_column :portals, :membership_type
  end
end
