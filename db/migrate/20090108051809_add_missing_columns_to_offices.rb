class AddMissingColumnsToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :zipcode, :string
    add_column :offices, :contact_first_name, :string
    add_column :offices, :contact_last_name, :string
    add_column :offices, :contact_email, :string
  end

  def self.down
    remove_column :offices, :contact_email
    remove_column :offices, :contact_last_name
    remove_column :offices, :contact_first_name
    remove_column :offices, :zipcode
  end
end
