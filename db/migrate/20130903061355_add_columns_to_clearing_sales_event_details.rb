class AddColumnsToClearingSalesEventDetails < ActiveRecord::Migration
  def self.up
    add_column :clearing_sales_event_details, :date, :date
    add_column :clearing_sales_event_details, :time, :string
    add_column :clearing_sales_event_details, :buyers_premium, :text
    add_column :clearing_sales_event_details, :warranty, :text
    add_column :clearing_sales_event_details, :gst, :boolean
    add_column :clearing_sales_event_details, :sale_info, :text
    add_column :clearing_sales_event_details, :collection, :text
    add_column :clearing_sales_event_details, :ohs, :text
  end

  def self.down
    remove_column :clearing_sales_event_details, :date
    remove_column :clearing_sales_event_details, :time
    remove_column :clearing_sales_event_details, :buyers_premium
    remove_column :clearing_sales_event_details, :warranty
    remove_column :clearing_sales_event_details, :gst
    remove_column :clearing_sales_event_details, :sale_info
    remove_column :clearing_sales_event_details, :collection
    remove_column :clearing_sales_event_details, :ohs
  end
end
