class AddMessagePendingState < ActiveRecord::Migration
  def self.up
    add_column :messages, :archive_state, :integer, :default => 0
    add_index :messages, :archive_state
  end

  def self.down
    remove_index :messages, :archive_state
    remove_column :messages, :archive_state
  end
end
