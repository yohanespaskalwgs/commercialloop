class AddScheduleEalert < ActiveRecord::Migration
  def self.up
    add_column :ealerts, :frequency, :string
    add_column :ealerts, :start_date, :date
    add_column :ealerts, :send_time, :time
    add_column :ealerts, :alert_enable, :boolean
  end

  def self.down
    remove_column :ealerts, :frequency
    remove_column :ealerts, :start_date
    remove_column :ealerts, :send_time
    remove_column :ealerts, :alert_enable
  end
end
