class AddColumnOfficeIdToSubscriptionHistories < ActiveRecord::Migration
  def self.up
    add_column :subscription_histories, :office_id, :integer
  end

  def self.down
    remove_column :subscription_histories, :office_id
  end
end
