class AddResetCodeToUserAndChangeEntryToDomain < ActiveRecord::Migration
  def self.up
    add_column :users, :reset_code, :string, :default => nil    
    #rename_column :users, :entry, :name
  end

  def self.down
    add_column :users, :reset_code, :string    
    #change_column :developers, :domain, :string, :default => 'agentaccount.com'    
  end
end
