class AddColumnSupportToDevelopers < ActiveRecord::Migration
  def self.up
    add_column :developers, :support, :boolean
  end

  def self.down
    remove_column :developers, :support
  end
end
