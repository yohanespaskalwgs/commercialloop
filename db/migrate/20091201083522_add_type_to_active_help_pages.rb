class AddTypeToActiveHelpPages < ActiveRecord::Migration
  def self.up
    add_column :active_help_pages, :user_type, :string
  end

  def self.down
    remove_column :active_help_pages, :user_type
  end
end
