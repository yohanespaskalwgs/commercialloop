class AddEnableExportToDomain < ActiveRecord::Migration
  def self.up
    add_column :domains, :enable_export, :boolean, :default => false
  end

  def self.down
    remove_column :domains, :enable_export
  end
end
