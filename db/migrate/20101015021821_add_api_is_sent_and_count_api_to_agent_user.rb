class AddApiIsSentAndCountApiToAgentUser < ActiveRecord::Migration
  def self.up
    add_column :users, :api_is_sent, :boolean
    add_column :users, :count_api_sent, :integer
    add_column :users, :processed, :boolean
  end

  def self.down
    remove_column :users, :api_is_sent
    remove_column :users, :count_api_sent
    remove_column :users, :processed
  end
end
