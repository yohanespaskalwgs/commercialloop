class AddDisplayApiAgentSystemColumnInOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :display_api_agent_system, :boolean
  end

  def self.down
    remove_column :offices, :display_api_agent_system
  end
end
