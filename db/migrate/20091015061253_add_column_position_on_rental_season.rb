class AddColumnPositionOnRentalSeason < ActiveRecord::Migration
  def self.up
    add_column :rental_seasons, :position, :integer
  end

  def self.down
    remove_column :rental_seasons, :position
  end
end
