class AddColumnNumberOfSuitesAndSizeRangeToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :number_of_suites, :integer
    add_column :properties, :size_from, :integer
    add_column :properties, :size_to, :integer
  end

  def self.down
    remove_column :properties, :number_of_suites
    remove_column :properties, :size_from
    remove_column :properties, :size_to
  end
end
