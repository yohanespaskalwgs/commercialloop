class AddOptionsForIndustryCategory < ActiveRecord::Migration
  def self.up
    IndustryCategory.destroy_all
    IndustryCategory.create(:name => "Development")
    IndustryCategory.create(:name => "Farmland")
    IndustryCategory.create(:name => "Healthcare")
    IndustryCategory.create(:name => "Hotel")
    IndustryCategory.create(:name => "Industrial")
    IndustryCategory.create(:name => "Institutional")
    IndustryCategory.create(:name => "Investment")
    IndustryCategory.create(:name => "Land")
    IndustryCategory.create(:name => "Leisure")
    IndustryCategory.create(:name => "Offices")
    IndustryCategory.create(:name => "Parking Space")
    IndustryCategory.create(:name => "Residential")
    IndustryCategory.create(:name => "Retail")
    IndustryCategory.create(:name => "Showrooms/Bulky Goods")
    IndustryCategory.create(:name => "Warehouse")
  end

  def self.down
    IndustryCategory.destroy_all
  end
end
