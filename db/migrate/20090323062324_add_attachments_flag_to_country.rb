class AddAttachmentsFlagToCountry < ActiveRecord::Migration
  def self.up
    add_column :countries, :eer_upper, :integer
    add_column :countries, :eer_lower, :integer
    add_column :countries, :eer_deviation, :integer

    add_column :countries, :address1, :string
    add_column :countries, :address2, :string
    add_column :countries, :address3, :string

    add_column :countries, :flag_file_name, :string
    add_column :countries, :flag_content_type, :string
    add_column :countries, :flag_file_size, :integer
    add_column :countries, :flag_updated_at, :datetime
  end

  def self.down
    remove_column :countries, :flag_updated_at
    remove_column :countries, :flag_file_size
    remove_column :countries, :flag_content_type
    remove_column :countries, :flag_file_name

    remove_column :countries, :address3
    remove_column :countries, :address2
    remove_column :countries, :address1

    remove_column :countries, :eer_deviation
    remove_column :countries, :eer_lower
    remove_column :countries, :eer_upper
  end
end
