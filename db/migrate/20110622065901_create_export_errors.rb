class CreateExportErrors < ActiveRecord::Migration
  def self.up
    create_table :export_errors do |t|
      t.integer :office_id
      t.integer :property_id
      t.string  :portal_id
      t.string  :portal_name
      t.text    :error_message
      t.timestamps
    end
  end

  def self.down
    drop_table :export_errors
  end
end
