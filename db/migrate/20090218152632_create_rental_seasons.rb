class CreateRentalSeasons < ActiveRecord::Migration
  def self.up
    create_table :rental_seasons do |t|
      t.string :season, :limit => 10
      t.date :start_date
      t.date :end_date
      t.string :minimum_stay, :limit => 10
      t.integer :property_id
    end
  end

  def self.down
    drop_table :rental_seasons
  end
end
