class AddTypeToViewerType < ActiveRecord::Migration
  def self.up
    add_column :property_viewer_types, :status, :string
    PropertyViewerType.update_all( "`status` = 'general'", "`id` IN (7,8,9,10)" )
  end

  def self.down
    remove_column(:property_viewer_types, :status)
  end
end
