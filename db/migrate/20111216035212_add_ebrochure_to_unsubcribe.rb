class AddEbrochureToUnsubcribe < ActiveRecord::Migration
  def self.up
    add_column :ecampaign_unsubcribes, :ebrochure_id, :integer
  end

  def self.down
    remove_column :ecampaign_unsubcribes, :ebrochure_id
  end
end
