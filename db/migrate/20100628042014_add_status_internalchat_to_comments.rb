class AddStatusInternalchatToComments < ActiveRecord::Migration
  def self.up
    add_column :message_comments,:internal_chat,:boolean
  end

  def self.down
    remove_column :message_comments,:internal_chat
  end
end
