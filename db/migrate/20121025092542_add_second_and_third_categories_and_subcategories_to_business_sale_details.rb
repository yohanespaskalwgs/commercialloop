class AddSecondAndThirdCategoriesAndSubcategoriesToBusinessSaleDetails < ActiveRecord::Migration
  def self.up
    add_column :business_sale_details, :category2, :string
    add_column :business_sale_details, :sub_category2, :string
    add_column :business_sale_details, :category3, :string
    add_column :business_sale_details, :sub_category3, :string
  end

  def self.down
    remove_column :business_sale_details, :category2
    remove_column :business_sale_details, :sub_category2
    remove_column :business_sale_details, :category3
    remove_column :business_sale_details, :sub_category3
  end
end
