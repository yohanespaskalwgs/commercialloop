class CreatePropertyViewerTypes < ActiveRecord::Migration
  def self.up
    create_table :property_viewer_types do |t|
      t.string :name
      t.timestamps
    end
    PropertyViewerType.destroy_all
    PropertyViewerType.create(:name => "Office Website")
    PropertyViewerType.create(:name => "Individual Property Website")
    PropertyViewerType.create(:name => "Individual Agent Website")
    PropertyViewerType.create(:name => "Mobile Phone Website")    
    PropertyViewerType.create(:name => "Iphone Application")
    PropertyViewerType.create(:name => "Facebook Application")
    PropertyViewerType.create(:name => "Open Inspection Attendees")
    PropertyViewerType.create(:name => "Contracts Taken")
    PropertyViewerType.create(:name => "Offers Submitted")
    PropertyViewerType.create(:name => "Auction Attendees")
  end

  def self.down
    drop_table :property_viewer_types
  end
end
