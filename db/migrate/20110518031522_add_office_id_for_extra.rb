class AddOfficeIdForExtra < ActiveRecord::Migration
  def self.up
    add_column :extras, :office_id, :integer
    add_column :extra_options, :office_id, :integer
  end

  def self.down
    remove_column :extras, :office_id
    remove_column :extra_options, :office_id
  end
end
