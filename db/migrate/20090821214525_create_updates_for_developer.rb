class CreateUpdatesForDeveloper < ActiveRecord::Migration
  def self.up
    create_table :developer_updates do |t|
      t.text :data
      t.timestamps
    end
  end

  def self.down
    drop_table :developer_updates
  end
end
