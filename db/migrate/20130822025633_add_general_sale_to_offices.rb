class AddGeneralSaleToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :general_sale, :boolean
  end

  def self.down
    remove_column :offices, :general_sale
  end
end
