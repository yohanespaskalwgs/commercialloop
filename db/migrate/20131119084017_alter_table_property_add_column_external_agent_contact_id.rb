class AlterTablePropertyAddColumnExternalAgentContactId < ActiveRecord::Migration
  def self.up
  	add_column :properties, :external_agent_contact_id, :integer
  end

  def self.down
  	remove_column :properties, :external_agent_contact_id
  end
end
