class AddXmlUpdatedOnTableProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :xml_updated_at, :string
  end

  def self.down
    remove_column(:properties, :xml_updated_at)
  end
end
