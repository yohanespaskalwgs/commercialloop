class AddLivestockSaleToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :livestock_sale, :boolean
  end

  def self.down
    remove_column :offices, :livestock_sale
  end
end
