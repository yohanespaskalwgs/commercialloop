class AddConjuncIdToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :conjunctional_id, :integer
  end

  def self.down
    remove_column(:users, :conjunctional_id)
  end
end
