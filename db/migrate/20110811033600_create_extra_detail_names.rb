class CreateExtraDetailNames < ActiveRecord::Migration
  def self.up
    create_table :extra_detail_names do |t|
      t.integer :office_id
      t.string  :field_name
      t.timestamps
    end
  end

  def self.down
    drop_table :extra_detail_names
  end
end
