class CreateLandReleases < ActiveRecord::Migration
  def self.up
    create_table :land_release_details do |t|
      t.integer :land_release_id
      t.string :release_name
      t.string :release_website
      t.string :start_date
      t.string :completion_date
      t.integer :number_of_lots
      t.integer :size_from
      t.integer :size_to
      t.string :distance
      t.string :property_url
      t.string :virtual_tour
      t.string :ext_link_1
      t.string :ext_link_2
      t.integer :duplication
      t.timestamps
    end
  end

  def self.down
    drop_table :land_release_details
  end
end
