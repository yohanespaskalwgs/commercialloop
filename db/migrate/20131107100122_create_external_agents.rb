class CreateExternalAgents < ActiveRecord::Migration
  def self.up
    create_table :external_agents do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :home_number
      t.string :work_number
      t.string :mobile_number
      t.integer :agent_id
      t.integer :office_id
      t.integer :creator_id
      t.timestamps
    end
  end

  def self.down
    drop_table :external_agents
  end
end
