class CreateResidentialLeaseDetails < ActiveRecord::Migration
  def self.up
    create_table :residential_lease_details do |t|
      t.integer :year_built
      t.integer :carport_spaces, :default => 0
      t.integer :garage_spaces, :default => 0
      t.integer :off_street_spaces, :default => 0
      t.integer :bedrooms, :default => 0
      t.integer :bathrooms, :default => 0
      t.integer :energy_efficiency_rating
      t.integer :energy_star_rating
      t.decimal :longitude, :precision => 12, :scale => 7
      t.decimal :latitude, :precision => 12, :scale => 7
      t.decimal :land_area, :precision => 16, :scale => 3
      t.decimal :floor_area, :precision => 16, :scale => 3
      t.integer :number_of_floors
      t.string :virtual_tour
      t.decimal :bond, :precision => 16, :scale => 3
      t.date :date_available
      t.integer :residential_lease_id
    end
  end

  def self.down
    drop_table :residential_lease_details
  end
end
