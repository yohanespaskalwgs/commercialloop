class CreatePurchasers < ActiveRecord::Migration
  def self.up
    create_table :purchasers do |t|
      t.integer :property_id
      t.integer :agent_contact_id
      t.date :date
      t.integer :amount
      t.boolean :display_price
      t.timestamps
    end
  end

  def self.down
    drop_table :purchasers
  end
end
