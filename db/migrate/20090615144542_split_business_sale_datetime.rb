class SplitBusinessSaleDatetime < ActiveRecord::Migration
  def self.up
    remove_column :business_sale_details, :auction_datetime
    add_column :business_sale_details, :auction_date, :date
    add_column :business_sale_details, :auction_time, :time
  end

  def self.down
    add_column :business_sale_details, :auction_datetime, :datetime
    remove_column :business_sale_details, :auction_date
    remove_column :business_sale_details, :auction_time    
  end
end
