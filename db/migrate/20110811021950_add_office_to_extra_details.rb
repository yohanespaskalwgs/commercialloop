class AddOfficeToExtraDetails < ActiveRecord::Migration
  def self.up
    add_column :extra_details, :office_id, :integer
  end

  def self.down
    remove_column(:extra_details, :office_id)
  end
end
