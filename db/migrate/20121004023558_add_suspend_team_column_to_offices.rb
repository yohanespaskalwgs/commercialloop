class AddSuspendTeamColumnToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :suspend_team, :boolean
  end

  def self.down
    remove_column :offices, :suspend_team
  end
end
