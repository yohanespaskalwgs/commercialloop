class AddShowConjunctionColumnToDevelopers < ActiveRecord::Migration
  def self.up
    add_column :developers, :show_conjunction, :boolean
  end

  def self.down
    remove_column :developers, :show_conjunction
  end
end
