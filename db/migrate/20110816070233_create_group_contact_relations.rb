class CreateGroupContactRelations < ActiveRecord::Migration
  def self.up
    create_table :group_contact_relations do |t|
      t.integer :agent_contact_id
      t.integer :group_contact_id
      t.timestamps
    end
  end

  def self.down
    drop_table :group_contact_relations
  end
end
