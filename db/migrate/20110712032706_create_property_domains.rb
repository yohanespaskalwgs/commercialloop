class CreatePropertyDomains < ActiveRecord::Migration
  def self.up
    create_table :property_domains do |t|
      t.integer :property_id
      t.integer :domain_id
      t.integer :count_sent
      t.boolean :cron_checked
      t.integer :sender_uid
      t.timestamps
    end
  end

  def self.down
    drop_table :property_domains
  end
end
