class AddNameFieldsToExtras < ActiveRecord::Migration
  def self.up
    add_column :extras, :field_name1, :string
    add_column :extras, :field_name2, :string
    add_column :extras, :field_name3, :string
    add_column :extras, :field_name4, :string
    add_column :extras, :field_name5, :string
    add_column :extras, :field_name6, :string
    add_column :extras, :field_name7, :string
    add_column :extras, :field_name8, :string
    add_column :extras, :field_name9, :string
    add_column :extras, :dropdown_name, :string
    add_column :extras, :dropdown_value, :integer
  end

  def self.down
    remove_column :extras, :field_name1
    remove_column :extras, :field_name2
    remove_column :extras, :field_name3
    remove_column :extras, :field_name4
    remove_column :extras, :field_name5
    remove_column :extras, :field_name6
    remove_column :extras, :field_name7
    remove_column :extras, :field_name8
    remove_column :extras, :field_name9
    remove_column :extras, :dropdown_name
    remove_column :extras, :dropdown_value
  end
end
