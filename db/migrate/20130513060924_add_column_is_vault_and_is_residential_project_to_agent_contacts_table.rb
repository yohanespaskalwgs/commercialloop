class AddColumnIsVaultAndIsResidentialProjectToAgentContactsTable < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :is_vault, :boolean, :default => 1
    add_column :agent_contacts, :is_residential_project, :boolean
  end

  def self.down
    remove_column :agent_contacts, :is_vault
    remove_column :agent_contacts, :is_residential_project
  end
end
