class CreateEcampaignTracks < ActiveRecord::Migration
  def self.up
    create_table :ecampaign_tracks do |t|
      t.integer  :ecampaign_id
      t.integer  :agent_contact_id
      t.boolean  :email_bounced
      t.boolean  :email_opened
      t.boolean  :email_forwarded
      t.timestamps
    end
  end

  def self.down
    drop_table :ecampaign_tracks
  end
end
