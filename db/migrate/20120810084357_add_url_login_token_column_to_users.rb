class AddUrlLoginTokenColumnToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :url_login_token, :string
  end

  def self.down
    remove_column :users, :url_login_token
  end
end
