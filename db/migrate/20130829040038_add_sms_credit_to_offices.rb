class AddSmsCreditToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :sms_credit, :integer, :default => 0
  end

  def self.down
    remove_column :offices, :sms_credit
  end
end
