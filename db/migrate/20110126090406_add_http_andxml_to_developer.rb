class AddHttpAndxmlToDeveloper < ActiveRecord::Migration
  def self.up
    add_column :developers, :xml_api_enable, :boolean
    add_column :developers, :http_api_enable, :boolean
  end

  def self.down
    remove_column :developers, :xml_api_enable
    remove_column :developers, :http_api_enable
  end
end
