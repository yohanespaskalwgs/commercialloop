class ChangeColumnHgpInLivestockSaleDetails < ActiveRecord::Migration
  def self.up
    change_column :livestock_sale_details, :hgp, :boolean, :default => nil
  end

  def self.down
    change_column :livestock_sale_details, :hgp, :boolean, :default => false
  end
end
