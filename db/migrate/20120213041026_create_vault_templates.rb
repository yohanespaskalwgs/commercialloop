class CreateVaultTemplates < ActiveRecord::Migration
  def self.up
    create_table :vault_templates do |t|
      t.integer  :office_id
      t.string   :headline
      t.text     :content
      t.boolean  :enable_autoresponder
      t.timestamps
    end
  end

  def self.down
    drop_table :vault_templates
  end
end
