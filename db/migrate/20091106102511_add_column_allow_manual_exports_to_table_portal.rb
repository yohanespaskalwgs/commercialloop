class AddColumnAllowManualExportsToTablePortal < ActiveRecord::Migration
  def self.up
    add_column :portals,:allow_manual_exports,:boolean
  end

  def self.down
    remove_column :portals,:allow_manual_exports
  end
end
