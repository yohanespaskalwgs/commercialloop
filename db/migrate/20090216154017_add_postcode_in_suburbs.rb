class AddPostcodeInSuburbs < ActiveRecord::Migration
  def self.up
    add_column :suburbs, :postcode, :string, :limit => 10
  end

  def self.down
    remove_column :suburbs, :postcode
  end
end
