class CreateMailMerges < ActiveRecord::Migration
  def self.up
    create_table :mail_merges do |t|
      t.integer :attachment_id
      t.integer :office_id
      t.integer :property_id
      t.string :title
      t.boolean :deleted
      t.timestamps
    end
  end

  def self.down
    drop_table :mail_merges
  end
end
