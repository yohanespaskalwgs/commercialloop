class AddReaFieldToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :rea_unique_id, :string
  end

  def self.down
    remove_column :properties, :rea_unique_id
  end
end
