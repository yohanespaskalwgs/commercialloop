class AddColumnOnHoldByOnProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :on_hold_by, :integer
    add_column :properties, :on_hold_by_user_type, :string
  end

  def self.down
    remove_column :properties, :on_hold_by
    remove_column :properties, :on_hold_by_user_type
  end
end
