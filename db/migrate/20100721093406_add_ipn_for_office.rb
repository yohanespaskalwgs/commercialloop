class AddIpnForOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :ipn, :string
  end

  def self.down
    remove_column :offices, :ipn
  end
end
