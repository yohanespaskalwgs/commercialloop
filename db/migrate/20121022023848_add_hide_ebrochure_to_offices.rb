class AddHideEbrochureToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :hide_ebrochure, :boolean
  end

  def self.down
    remove_column :offices, :hide_ebrochure
  end
end
