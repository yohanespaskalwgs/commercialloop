class AddColumnReCodeNTmCodeOnSuburb < ActiveRecord::Migration
  def self.up
    add_column :suburbs,:re_code,:integer
    add_column :suburbs,:tm_code,:integer
  end

  def self.down
    remove_column :suburbs,:re_code
    remove_column :suburbs,:tm_code 
  end
end
