class ChangeFloorLandType < ActiveRecord::Migration
  def self.up
    change_column :residential_sale_details, :land_area, :string
    change_column :residential_sale_details, :floor_area, :string
    change_column :residential_lease_details, :land_area, :string
    change_column :residential_lease_details, :floor_area, :string
    change_column :project_sale_details, :land_area, :string
    change_column :project_sale_details, :floor_area, :string
    change_column :holiday_lease_details, :land_area, :string
    change_column :holiday_lease_details, :floor_area, :string
    change_column :commercial_details, :land_area, :string
    change_column :commercial_details, :floor_area, :string
    change_column :business_sale_details, :land_area, :string
    change_column :business_sale_details, :floor_area, :string
  end

  def self.down
    change_column :residential_sale_details, :land_area, :integer
    change_column :residential_sale_details, :floor_area, :integer
    change_column :residential_lease_details, :land_area, :integer
    change_column :residential_lease_details, :floor_area, :integer
    change_column :project_sale_details, :land_area, :integer
    change_column :project_sale_details, :floor_area, :integer
    change_column :holiday_lease_details, :land_area, :integer
    change_column :holiday_lease_details, :floor_area, :integer
    change_column :commercial_details, :land_area, :integer
    change_column :commercial_details, :floor_area, :integer
    change_column :business_sale_details, :land_area, :integer
    change_column :business_sale_details, :floor_area, :integer
  end
end
