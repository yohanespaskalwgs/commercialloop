class AddMydestopagentToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :mydesktop_agent_id, :string
  end

  def self.down
    remove_column :users, :mydesktop_agent_id
  end
end
