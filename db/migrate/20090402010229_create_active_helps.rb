class CreateActiveHelps < ActiveRecord::Migration
  def self.up
    create_table :active_helps do |t|
      t.text :data
      t.timestamps
    end
  end

  def self.down
    drop_table :active_helps
  end
end
