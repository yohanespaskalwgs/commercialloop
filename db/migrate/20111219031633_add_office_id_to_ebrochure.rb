class AddOfficeIdToEbrochure < ActiveRecord::Migration
  def self.up
    add_column :ebrochures, :office_id, :integer
  end

  def self.down
    remove_column :ebrochures, :office_id
  end
end
