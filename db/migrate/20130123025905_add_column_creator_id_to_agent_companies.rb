class AddColumnCreatorIdToAgentCompanies < ActiveRecord::Migration
  def self.up
    add_column :agent_companies, :creator_id, :integer
  end

  def self.down
    remove_column :agent_companies, :creator_id
  end
end
