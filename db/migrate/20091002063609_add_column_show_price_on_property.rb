class AddColumnShowPriceOnProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :show_price, :boolean
  end

  def self.down
    remove_column :properties, :show_price, :boolean
  end
end
