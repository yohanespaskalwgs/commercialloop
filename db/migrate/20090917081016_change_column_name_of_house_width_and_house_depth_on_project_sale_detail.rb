class ChangeColumnNameOfHouseWidthAndHouseDepthOnProjectSaleDetail < ActiveRecord::Migration
  def self.up
    rename_column(:project_sale_details, :house_width, :land_width)
    rename_column(:project_sale_details, :house_depth, :land_depth)
  end

  def self.down
    rename_column(:project_sale_details, :land_width, :house_width)
    rename_column(:project_sale_details, :land_depth, :house_depth)
  end
end
