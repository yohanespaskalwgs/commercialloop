class CreateOnHolders < ActiveRecord::Migration
  def self.up
    create_table :on_holders do |t|
      t.integer :property_id
      t.string :name
      t.string :user_type
      t.timestamps
    end
  end

  def self.down
    drop_table :on_holders
  end
end
