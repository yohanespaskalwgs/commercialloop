class AddClearingSaleToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :clearing_sale, :boolean
  end

  def self.down
    remove_column :offices, :clearing_sale
  end
end
