class AddStatusInDevelopers < ActiveRecord::Migration
  def self.up
    add_column :developers, :status, :string, :default => 'active'
  end

  def self.down
    remove_column :developers, :status
  end
end
