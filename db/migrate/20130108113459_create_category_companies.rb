class CreateCategoryCompanies < ActiveRecord::Migration
  def self.up
    create_table :category_companies do |t|
      t.string :name
      t.integer :creator_id
      t.timestamps
    end
  end

  def self.down
    drop_table :category_companies
  end
end
