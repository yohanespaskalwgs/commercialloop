class RemoveColumnEnergyStarRating < ActiveRecord::Migration
  def self.up
    remove_column(:countries, :energy_star_rating)
    remove_column(:residential_sale_details, :energy_star_rating)
    remove_column(:residential_lease_details, :energy_star_rating)
    remove_column(:holiday_lease_details, :energy_star_rating)
    remove_column(:commercial_details, :energy_star_rating)
    remove_column(:project_sale_details, :energy_star_rating)
    remove_column(:business_sale_details, :energy_star_rating)
  end

  def self.down
    add_column(:countries, :energy_star_rating, :integer)
    add_column(:residential_sale_details, :energy_star_rating, :integer)
    add_column(:residential_lease_details, :energy_star_rating, :integer)
    add_column(:holiday_lease_details, :energy_star_rating, :integer)
    add_column(:commercial_details, :energy_star_rating, :integer)
    add_column(:project_sale_details, :energy_star_rating, :integer)
    add_column(:business_sale_details, :energy_star_rating, :integer)
  end
end
