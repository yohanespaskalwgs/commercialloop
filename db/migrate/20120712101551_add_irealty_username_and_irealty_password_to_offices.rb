class AddIrealtyUsernameAndIrealtyPasswordToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :irealty_username, :string
    add_column :offices, :irealty_password, :string
  end

  def self.down
    remove_column :offices, :irealty_username
    remove_column :offices, :irealty_password
  end
end
