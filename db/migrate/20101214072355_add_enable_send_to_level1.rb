class AddEnableSendToLevel1 < ActiveRecord::Migration
  def self.up
    add_column :offices, :enable_send_email, :boolean
  end

  def self.down
    remove_column :offices, :enable_send_email
  end
end
