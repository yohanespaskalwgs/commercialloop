class AddWatermarkToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :include_watermark, :boolean
  end

  def self.down
    remove_column :offices, :include_watermark
  end
end
