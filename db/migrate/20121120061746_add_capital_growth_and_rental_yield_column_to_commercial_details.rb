class AddCapitalGrowthAndRentalYieldColumnToCommercialDetails < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :capital_growth, :string
    add_column :commercial_details, :rental_yield, :string
  end

  def self.down
    remove_column :commercial_details, :capital_growth
    remove_column :commercial_details, :rental_yield
  end
end
