class AddPriorityToMessage < ActiveRecord::Migration
  def self.up
    add_column :messages, :priority, :string
    add_column :messages, :quote, :boolean
  end

  def self.down
    remove_column :messages, :priority
    remove_column :messages, :quote
  end
end
