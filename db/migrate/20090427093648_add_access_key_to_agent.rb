class AddAccessKeyToAgent < ActiveRecord::Migration
  def self.up
    add_column :agents, :access_key, :string
    add_column :agents, :private_key, :string
  end

  def self.down
    remove_column :agents, :private_key
    remove_column :agents, :access_key
  end
end
