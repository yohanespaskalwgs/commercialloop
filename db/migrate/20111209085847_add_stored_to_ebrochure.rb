class AddStoredToEbrochure < ActiveRecord::Migration
  def self.up
    add_column :ebrochures, :stored_layout, :longblob
    add_column :ebrochures, :stored_data, :longblob
  end

  def self.down
    remove_column :ebrochures, :stored_layout
    remove_column :ebrochures, :stored_data
  end
end
