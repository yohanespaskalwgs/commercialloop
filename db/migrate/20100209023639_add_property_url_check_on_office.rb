class AddPropertyUrlCheckOnOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :property_url, :boolean
  end

  def self.down
    remove_column(:offices, :property_url)
  end
end
