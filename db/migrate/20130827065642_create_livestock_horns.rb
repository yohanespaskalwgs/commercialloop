class CreateLivestockHorns < ActiveRecord::Migration
  def self.up
    create_table :livestock_horns do |t|
      t.string :name
      t.timestamps
    end

    LivestockHorn.create({:name => "Poll"})
    LivestockHorn.create({:name => "Dehorned"})
    LivestockHorn.create({:name => "Tipped"})
  end

  def self.down
    drop_table :livestock_horns
  end
end
