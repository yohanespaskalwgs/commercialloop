class CreateAgentCompanies < ActiveRecord::Migration
  def self.up
    create_table :agent_companies do |t|
      t.string :company
      t.string :email
      t.string :website
      t.string :contact_number
      t.string :fax_number
      t.boolean :inactive
      t.integer :country_id
      t.string :unit_number
      t.string :street_number
      t.string :street_name
      t.string :town_village
      t.string :suburb
      t.string :state
      t.string :post_code
      t.integer :category_contact_id
      t.integer :assigned_to
      t.integer :accessible_by
      t.integer :heard_from_id
      t.boolean :login_required
      t.string :username
      t.string :password
      t.integer :industry_category_id
      t.integer :group_id
      t.integer :agent_id
      t.integer :office_id
      t.timestamps
    end
  end

  def self.down
    drop_table :agent_companies
  end
end
