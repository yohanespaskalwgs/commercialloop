class CreateActiveHelpPages < ActiveRecord::Migration
  def self.up
    create_table :active_help_pages do |t|
      t.string :key
      t.text :content
      t.timestamps
    end
  end

  def self.down
    drop_table :active_help_pages
  end
end
