class AddSaveStatusToProperty < ActiveRecord::Migration

  def self.up
    add_column :properties, :save_status, :string
  end

  def self.down
    remove_column :properties, :save_status
  end
end
