class AddActivateVault < ActiveRecord::Migration
  def self.up
    add_column :offices, :activate_vault, :boolean
  end

  def self.down
    remove_column :offices, :activate_vault
  end
end
