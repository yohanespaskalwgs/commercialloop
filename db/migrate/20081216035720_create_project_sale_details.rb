class CreateProjectSaleDetails < ActiveRecord::Migration
  def self.up
    create_table :project_sale_details do |t|
      t.string :category
      t.date :date_of_completion
      t.decimal :estimate_rental_return, :precision => 16, :scale => 3
      t.integer :carport_spaces, :default => 0
      t.integer :garage_spaces, :default => 0
      t.integer :off_street_spaces, :default => 0
      t.integer :bedrooms, :default => 0
      t.integer :bathrooms, :default => 0
      t.string :energy_efficiency_rating
      t.string :energy_star_rating
      t.decimal :longitude, :precision => 12, :scale => 7
      t.decimal :latitude, :precision => 12, :scale => 7
      t.decimal :floor_area, :precision => 16, :scale => 3
      t.decimal :land_area, :precision => 16, :scale => 3
      t.decimal :porch_terrace_area, :precision => 16, :scale => 3
      t.decimal :garage_area, :precision => 16, :scale => 3
      t.decimal :house_width, :precision => 16, :scale => 3
      t.decimal :house_depth, :precision => 16, :scale => 3
      t.string :development_name
      t.string :design_type
      t.string :style
      t.string :virtual_tour
      t.integer :duplication
      t.integer :project_sale_id
    end
  end

  def self.down
    drop_table :project_sale_details
  end
end
