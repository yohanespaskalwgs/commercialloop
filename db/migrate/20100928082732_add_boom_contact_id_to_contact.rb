class AddBoomContactIdToContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :boom_contact_id, :integer
  end

  def self.down
    remove_column :agent_contacts, :boom_contact_id
  end
end
