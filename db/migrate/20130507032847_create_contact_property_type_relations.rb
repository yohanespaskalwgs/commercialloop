class CreateContactPropertyTypeRelations < ActiveRecord::Migration
  def self.up
    create_table :contact_property_type_relations do |t|
      t.integer :agent_contact_id
      t.integer :property_type_id
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_property_type_relations
  end
end
