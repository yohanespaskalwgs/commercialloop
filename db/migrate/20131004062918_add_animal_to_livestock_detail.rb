class AddAnimalToLivestockDetail < ActiveRecord::Migration
  def self.up
    add_column :livestock_sale_details, :animal, :string
  end

  def self.down
  end
end
