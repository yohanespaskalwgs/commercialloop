class AddDataForInternetFeature < ActiveRecord::Migration
  def self.up
    InternalFeature.create({:name => "Wireless Internet", :property_type => "HolidayLease"})
    InternalFeature.create({:name => "Wireless Internet", :property_type => "BusinessSale"})
    InternalFeature.create({:name => "Wireless Internet", :property_type => "Commercial"})
  end

  def self.down
    Feature.find_by_name('Wireless Internet').destroy
  end
end
