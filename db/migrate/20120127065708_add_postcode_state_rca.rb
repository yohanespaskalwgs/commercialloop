class AddPostcodeStateRca < ActiveRecord::Migration
  def self.up
    add_column :properties, :re_state, :string
    add_column :properties, :rca_state, :string
    add_column :properties, :re_postcode, :string
    add_column :properties, :rca_postcode, :string
  end

  def self.down
    remove_column :properties, :re_state
    remove_column :properties, :rca_state
    remove_column :properties, :re_postcode
    remove_column :properties, :rca_postcode
  end
end
