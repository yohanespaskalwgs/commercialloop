class AddMoreExtrasColumn < ActiveRecord::Migration
  def self.up
    add_column :extras, :field15, :string
    add_column :extras, :field16, :string
    add_column :extras, :field17, :string
    add_column :extras, :field18, :string
    add_column :extras, :field19, :string
    add_column :extras, :field_name15, :string
    add_column :extras, :field_name16, :string
    add_column :extras, :field_name17, :string
    add_column :extras, :field_name18, :string
    add_column :extras, :field_name19, :string
  end

  def self.down
    remove_column :extras, :field15
    remove_column :extras, :field16
    remove_column :extras, :field17
    remove_column :extras, :field18
    remove_column :extras, :field19
    remove_column :extras, :field_name15
    remove_column :extras, :field_name16
    remove_column :extras, :field_name17
    remove_column :extras, :field_name18
    remove_column :extras, :field_name19
  end
end
