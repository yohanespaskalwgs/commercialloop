class AddCronCheckedToAgentUser < ActiveRecord::Migration
  def self.up
    add_column :users, :cron_checked, :boolean
  end

  def self.down
    add_column :users, :cron_checked
  end
end
