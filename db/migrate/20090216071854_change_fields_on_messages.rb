class ChangeFieldsOnMessages < ActiveRecord::Migration
  def self.up
    remove_column :messages, :sender_unread 
    remove_column :messages, :recipient_unread  
    add_column :messages, :office_id, :integer
    
    Message.reset_column_information
    
    Message.all.each do |msg|
      office_id = msg.sender.office_id
      unless office_id
        office = msg.sender.developer.offices.first 
        office_id = office.id if office
      end
      msg.update_attribute(:office_id, office_id)
    end
  end

  def self.down
    remove_column :messages, :office_id
    add_column :messages, :recipient_unread, :boolean,         :default => true
    add_column :messages, :sender_unread, :boolean,            :default => false
  end
end
