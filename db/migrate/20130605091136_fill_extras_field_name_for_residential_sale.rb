class FillExtrasFieldNameForResidentialSale < ActiveRecord::Migration
  def self.up
    residential_sales = Property.residential_sale
    residential_sales.each do |res_sale|
      if res_sale.extra.blank?
        extra = Extra.new({:property_id => res_sale.id,
            :office_id => res_sale.office_id,
            :field_name1 => "Floor Coverings",
            :field_name2 => "Aspect",
            :field_name3 => "Stamp Duty Payable",
            :field_name4 => "Estimated Stamp Duty Saving",
            :field_name5 => "Owners Corporation Cost",
            :field_name6 => "Notes",
            :field_name7 => "Deposit Paid",
            :field_name8 => "Finance",
            :field_name9 => "FIRB",
            :field_name10 => "Flooring Option",
            :field_name11 => "Settle Date",
            :field_name12 => "Trans Rec.",
            :field_name13 => "Dis Let",
            :field_name14 => "Sett"})
        extra.save
      end
    end
  end

  def self.down
  end
end
