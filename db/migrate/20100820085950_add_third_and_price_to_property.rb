class AddThirdAndPriceToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :third_contact_id, :integer
    add_column :properties, :to_price, :integer
  end

  def self.down
    remove_column(:properties, :third_contact)
    remove_column(:properties, :to_price)
  end
end
