class ChangePropertyIdInRentalSeasonsToHolidayLeaseId < ActiveRecord::Migration
  def self.up
    remove_column :rental_seasons, :property_id
    add_column :rental_seasons, :holiday_lease_id, :integer
  end

  def self.down
    remove_column :rental_seasons, :holiday_lease_id
    add_column :rental_seasons, :property_id, :integer
  end
end
