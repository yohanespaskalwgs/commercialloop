class CreateContactAlertCommercialTypes < ActiveRecord::Migration
  def self.up
    create_table :contact_alert_commercial_types do |t|
      t.integer :contact_alert_id
      t.string :commercial_type
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_alert_commercial_types
  end
end
