class AddSendToIrealtyToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :send_to_irealty, :boolean
  end

  def self.down
    remove_column :offices, :send_to_irealty
  end
end
