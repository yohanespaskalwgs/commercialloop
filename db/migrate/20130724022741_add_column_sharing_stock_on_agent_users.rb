class AddColumnSharingStockOnAgentUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :share_stock_on_admin_account, :boolean, :default => false
    add_column :users, :share_stock_with_all_offices, :boolean, :default => false
    add_column :users, :share_database_requirement_on_admin_account, :boolean, :default => false
    add_column :users, :share_database_requirement_with_all_offices, :boolean, :default => false
  end

  def self.down
    remove_column :users, :share_stock_on_admin_account
    remove_column :users, :share_stock_with_all_offices
    remove_column :users, :share_database_requirement_on_admin_account
    remove_column :users, :share_database_requirement_with_all_offices
  end
end
