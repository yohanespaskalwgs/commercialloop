class AddSuburbNzProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :re_suburb, :string
    add_column :properties, :rca_suburb, :string
  end

  def self.down
    remove_column :properties, :re_suburb
    remove_column :properties, :rca_suburb
  end
end
