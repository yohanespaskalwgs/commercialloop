class AddEcampaignToDev < ActiveRecord::Migration
  def self.up
    add_column :developers, :allow_ecampaign, :boolean
  end

  def self.down
    remove_column(:developers, :allow_ecampaign)
  end
end
