class AddStatusToActiveHelpPagesUsers < ActiveRecord::Migration
  def self.up
    add_column :active_help_pages_users, :status, :boolean
  end

  def self.down
    remove_column :active_help_pages_users, :status
  end
end
