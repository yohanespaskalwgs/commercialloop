class CreatePropertyContactEnquiries < ActiveRecord::Migration
  def self.up
    create_table :property_contact_enquiries do |t|
      t.integer :property_id
      t.string :agent_contact_id
      t.timestamps
    end
  end

  def self.down
    drop_table :property_contact_enquiries
  end
end
