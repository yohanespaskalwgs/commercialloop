class AddApiIsSentToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :api_is_sent, :boolean
    add_column :properties, :count_api_sent, :integer
  end

  def self.down
    remove_column :properties, :api_is_sent
    remove_column :properties, :count_api_sent
  end
end
