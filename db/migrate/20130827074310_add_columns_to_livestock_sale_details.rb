class AddColumnsToLivestockSaleDetails < ActiveRecord::Migration
  def self.up
    add_column :livestock_sale_details, :livestock_sale_type_id, :integer
    add_column :livestock_sale_details, :livestock_type_id, :integer
    add_column :livestock_sale_details, :livestock_sex_id, :integer
    add_column :livestock_sale_details, :livestock_pregnancy_id, :integer
    add_column :livestock_sale_details, :livestock_frame_id, :integer
    add_column :livestock_sale_details, :livestock_condition_id, :integer
    add_column :livestock_sale_details, :livestock_quality_id, :integer
    add_column :livestock_sale_details, :livestock_horn_id, :integer
    add_column :livestock_sale_details, :livestock_tick_status_id, :integer

    add_column :livestock_sale_details, :auction_date, :date
    add_column :livestock_sale_details, :livestock_number, :integer
    add_column :livestock_sale_details, :age, :integer
    add_column :livestock_sale_details, :weight_from, :integer
    add_column :livestock_sale_details, :weight_to, :integer
    add_column :livestock_sale_details, :weight, :integer
    add_column :livestock_sale_details, :horn_percentage, :decimal
    add_column :livestock_sale_details, :location, :string
    add_column :livestock_sale_details, :treatment, :string
    add_column :livestock_sale_details, :price, :decimal
  end

  def self.down
    remove_column :livestock_sale_details, :livestock_sale_type_id
    remove_column :livestock_sale_details, :livestock_type_id
    remove_column :livestock_sale_details, :livestock_sex_id
    remove_column :livestock_sale_details, :livestock_pregnancy_id
    remove_column :livestock_sale_details, :livestock_frame_id
    remove_column :livestock_sale_details, :livestock_condition_id
    remove_column :livestock_sale_details, :livestock_quality_id
    remove_column :livestock_sale_details, :livestock_horn_id
    remove_column :livestock_sale_details, :livestock_tick_status_id

    remove_column :livestock_sale_details, :auction_date
    remove_column :livestock_sale_details, :livestock_number
    remove_column :livestock_sale_details, :age
    remove_column :livestock_sale_details, :weight_from
    remove_column :livestock_sale_details, :weight_to
    remove_column :livestock_sale_details, :weight
    remove_column :livestock_sale_details, :horn_percentage
    remove_column :livestock_sale_details, :location
    remove_column :livestock_sale_details, :treatment
    remove_column :livestock_sale_details, :price
  end
end
