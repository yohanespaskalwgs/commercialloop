class CreateContactAlertRegions < ActiveRecord::Migration
  def self.up
    create_table :contact_alert_regions do |t|
      t.integer :contact_alert_id
      t.string :region
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_alert_regions
  end
end
