class AddColumnAgentIdAndOfficeIdToTenancyRecords < ActiveRecord::Migration
  def self.up
  	add_column :tenancy_records, :agent_id, :integer
  	add_column :tenancy_records, :office_id, :integer
  end

  def self.down
  	remove_column :tenancy_records, :agent_id
  	remove_column :tenancy_records, :office_id
  end
end
