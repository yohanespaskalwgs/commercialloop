class AddAgentIdOfficeIdToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :office_id, :integer
    add_column :properties, :agent_id, :integer
  end

  def self.down
    remove_column :properties, :agent_id
    remove_column :properties, :office_id
  end
end
