class CreateProjectDevelopments < ActiveRecord::Migration
  def self.up
    create_table :project_developments do |t|
      t.string   :name
      t.integer  :office_id
      t.timestamps
    end
  end

  def self.down
    drop_table :project_developments
  end
end
