class AddControlLevel3ColumnToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :control_level3, :boolean
  end

  def self.down
    remove_column :offices, :control_level3
  end
end
