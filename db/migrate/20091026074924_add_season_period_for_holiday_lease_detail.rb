class AddSeasonPeriodForHolidayLeaseDetail < ActiveRecord::Migration
  def self.up
    add_column :holiday_lease_details, :mid_season_period, :string
    add_column :holiday_lease_details, :high_season_period, :string
    add_column :holiday_lease_details, :peak_season_period, :string
  end

  def self.down
    remove_column :holiday_lease_details, :mid_season_period 
    remove_column :holiday_lease_details, :high_season_period
    remove_column :holiday_lease_details, :peak_season_period
  end
end
