class ChangePropertyPricePrecision < ActiveRecord::Migration
  
  def self.up
    change_column :properties, :price, :decimal, :precision => 16, :scale => 2
    change_column :properties, :price2, :decimal, :precision => 16, :scale => 2
  end

  def self.down
    # change_column :properties, :price, :string
  end
end
