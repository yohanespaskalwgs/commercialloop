class AddPropertyCheckBoxInTheOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :property_with_owner_details, :boolean
    add_column :offices, :property_draft_for_new, :boolean
    add_column :offices, :property_draft_for_updated, :boolean
  end

  def self.down
    remove_column(:offices, :property_with_owner_details)
    remove_column(:offices, :property_draft_for_new)
    remove_column(:offices, :property_draft_for_updated)
  end
end
