class AddColumnFloorAreaIntoOtherDetailColumn < ActiveRecord::Migration
  def self.up
    add_column :new_development_details, :floor_area, :string
    add_column :land_release_details, :floor_area, :string
    add_column :commercial_building_details, :bedrooms, :integer
    add_column :commercial_building_details, :bathrooms, :integer
    add_column :commercial_details, :bedrooms, :integer
    add_column :commercial_details, :bathrooms, :integer
    add_column :land_release_details, :bedrooms, :integer
    add_column :land_release_details, :bathrooms, :integer
    add_column :new_development_details, :bedrooms, :integer
    add_column :new_development_details, :bathrooms, :integer
    add_column :business_sale_details, :bedrooms, :integer
    add_column :business_sale_details, :bathrooms, :integer
    add_column :business_sale_details, :carport_spaces, :integer
    add_column :land_release_details, :carport_spaces, :integer
    add_column :new_development_details, :carport_spaces, :integer
  end

  def self.down
    remove_column :new_development_details, :floor_area
    remove_column :land_release_details, :floor_area
    remove_column :commercial_building_details, :bedrooms
    remove_column :commercial_building_details, :bathrooms
    remove_column :commercial_details, :bedrooms
    remove_column :commercial_details, :bathrooms
    remove_column :land_release_details, :bedrooms
    remove_column :land_release_details, :bathrooms
    remove_column :new_development_details, :bedrooms
    remove_column :new_development_details, :bathrooms
    remove_column :business_sale_details, :bedrooms
    remove_column :business_sale_details, :bathrooms
    remove_column :business_sale_details, :carport_spaces
    remove_column :land_release_details, :carport_spaces
    remove_column :new_development_details, :carport_spaces
  end
end
