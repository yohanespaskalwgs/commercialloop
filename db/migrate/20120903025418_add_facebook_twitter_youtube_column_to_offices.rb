class AddFacebookTwitterYoutubeColumnToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :facebook, :string
    add_column :offices, :twitter, :string
    add_column :offices, :youtube, :string
    add_column :offices, :use_social_links, :boolean
  end

  def self.down
    remove_column :offices, :facebook
    remove_column :offices, :twitter
    remove_column :offices, :youtube
    add_column :offices, :use_social_links
  end
end
