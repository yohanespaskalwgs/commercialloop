class AddDataForHeardFrom < ActiveRecord::Migration
  def self.up
    HeardFrom.create(:name => "Signboard", :status => "heard_property")
    HeardFrom.create(:name => "Referral", :status => "heard_property")
  end

  def self.down
  end
end
