class CreateExtras < ActiveRecord::Migration
  def self.up
    create_table :extras do |t|
      t.integer  :property_id
      t.boolean  :featured_property
      t.boolean  :property_of_week
      t.text     :description
      t.timestamps
    end
  end

  def self.down
    drop_table :extras
  end
end
