class ChangeValueForStatusOnHoldExceptResidentialSaleAndResidentialBuilding < ActiveRecord::Migration
  def self.up
    properties = Property.find(:all, :conditions => ["type NOT IN (?) AND status = ?", ["ResidentialSale", "NewDevelopment"], "7"])
    properties.each do |property|
      property.status = "1"
      property.save
      property.purchaser.destroy
    end
  end

  def self.down
  end
end
