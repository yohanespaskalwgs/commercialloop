class AddHgpToLivestockSaleDetails < ActiveRecord::Migration
  def self.up
    add_column :livestock_sale_details, :hgp, :boolean, :default => false
  end

  def self.down
    remove_column :livestock_sale_details, :hgp
  end
end
