class AddNormalSeasonPeriodForProperty < ActiveRecord::Migration
  def self.up
    add_column :properties,:normal_season_period,:string
  end

  def self.down
    remove_column :properties,:normal_season_period
  end
end
