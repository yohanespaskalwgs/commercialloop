class AddingContactType < ActiveRecord::Migration
  def self.up
    add_column :properties, :secondary_contact_type, :string
    add_column :properties, :third_contact_type, :string
    add_column :properties, :fourth_contact_type, :string
  end

  def self.down
    remove_column(:properties, :secondary_contact_type)
    remove_column(:properties, :third_contact_type)
    remove_column(:properties, :fourth_contact_type)
  end
end
