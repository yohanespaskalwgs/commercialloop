class AddLoginToContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :username, :string
    add_column :agent_contacts, :password, :string
  end

  def self.down
    remove_column :agent_contacts, :username
    remove_column :agent_contacts, :password
  end
end
