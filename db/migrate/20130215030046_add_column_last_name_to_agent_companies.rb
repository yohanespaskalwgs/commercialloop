class AddColumnLastNameToAgentCompanies < ActiveRecord::Migration
  def self.up
    add_column :agent_companies, :last_name, :string
  end

  def self.down
    remove_column :agent_companies, :last_name
  end
end
