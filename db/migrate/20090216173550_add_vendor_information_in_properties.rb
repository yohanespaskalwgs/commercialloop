class AddVendorInformationInProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :vendor_first_name, :string
    add_column :properties, :vendor_last_name, :string
    add_column :properties, :vendor_email, :string
    add_column :properties, :vendor_phone, :string
  end

  def self.down
    remove_column :properties, :vendor_phone
    remove_column :properties, :vendor_email
    remove_column :properties, :vendor_last_name
    remove_column :properties, :vendor_first_name
  end
end
