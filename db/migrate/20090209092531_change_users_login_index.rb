class ChangeUsersLoginIndex < ActiveRecord::Migration
  def self.up
    # remove the uniq index and create a new one
    remove_index :users, :login
    add_index :users, :login
    
    User.reset_column_information
    
    User.all.each do |u|
      # removed all @@@ suffix
      if u.login =~ /(.+)@@@(.*)/
        u.update_attribute(:login, $1)
      end
      
      # AgentUser also keeps track of developer
      if u[:type] == 'AgentUser' && u.office
        u.update_attribute(:developer_id, u.office.agent.developer_id)
      end
    end
  end

  def self.down   
    # do we need to go back
  end
end
