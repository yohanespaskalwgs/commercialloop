class CreateEcampaignClicks < ActiveRecord::Migration
  def self.up
    create_table :ecampaign_clicks do |t|
      t.integer  :ecampaign_id
      t.integer  :property_id
      t.string   :destination_name
      t.string   :destination_link
      t.integer  :clicked
      t.timestamps
    end
  end

  def self.down
    drop_table :ecampaign_clicks
  end
end
