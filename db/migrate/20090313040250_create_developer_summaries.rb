class CreateDeveloperSummaries < ActiveRecord::Migration
  def self.up
    create_table :developer_summaries do |t|
      t.integer :developer_id
      t.string :country
      t.integer :client_a
      t.integer :client_i
      t.integer :client_s
      t.integer :office_a
      t.integer :office_i
      t.integer :office_p
      t.integer :office_s
      t.integer :team
      t.integer :listing_a
      t.integer :listing_i

      t.timestamps
    end
    
    add_index :developer_summaries, :developer_id
    add_index :developer_summaries, :country
  end

  def self.down
    drop_table :developer_summaries
  end
end
