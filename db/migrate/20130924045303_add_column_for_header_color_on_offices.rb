class AddColumnForHeaderColorOnOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :header_color, :string
    add_column :offices, :ul_nav_color, :string
    add_column :offices, :ul_nav_li_active_color, :string
    add_column :offices, :ul_nav_li_current_color, :string
    add_column :offices, :navdiv_color, :string
  end

  def self.down
    remove_column :offices, :header_color
    remove_column :offices, :ul_nav_color
    remove_column :offices, :ul_nav_li_active_color
    remove_column :offices, :ul_nav_li_current_color
    remove_column :offices, :navdiv_color
  end
end
