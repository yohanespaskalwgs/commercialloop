class AddMetricsLengthInCountries < ActiveRecord::Migration
  def self.up
    add_column :countries, :metrics_length, :string
  end

  def self.down
    remove_column :countries, :metrics_length
  end
end
