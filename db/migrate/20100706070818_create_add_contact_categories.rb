class CreateAddContactCategories < ActiveRecord::Migration
  def self.up
    create_table :contact_category_relations do |t|
      t.integer :agent_contact_id
      t.integer :category_contact_id
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_category_relations
  end
end
