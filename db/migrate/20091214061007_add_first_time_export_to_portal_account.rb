class AddFirstTimeExportToPortalAccount < ActiveRecord::Migration
  def self.up
    add_column :portal_accounts, :first_time_export, :boolean
  end

  def self.down
    remove_column :portal_accounts, :first_time_export
  end
end
