class AddFourthContact < ActiveRecord::Migration
  def self.up
    add_column :properties, :fourth_contact_id, :integer
  end

  def self.down
    remove_column(:properties, :fourth_contact_id)
  end
end
