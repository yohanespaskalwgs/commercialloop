class AddCurrencyCodeForOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :office_currency_code, :string
  end

  def self.down
    remove_column :offices, :office_currency_code
  end
end
