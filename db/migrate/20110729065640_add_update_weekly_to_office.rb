class AddUpdateWeeklyToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :update_listing_weekly, :boolean
  end

  def self.down
    remove_column :offices, :update_listing_weekly
  end
end
