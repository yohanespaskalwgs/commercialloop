class AddSendAvailableAndUnderoffer < ActiveRecord::Migration
  def self.up
    add_column :domains, :send_available_and_underoffer, :boolean, :default => false
  end

  def self.down
    remove_column :domains, :send_available_and_underoffer
  end
end
