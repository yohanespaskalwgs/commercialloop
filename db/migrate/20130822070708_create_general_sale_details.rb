class CreateGeneralSaleDetails < ActiveRecord::Migration
  def self.up
    create_table :general_sale_details do |t|
      t.integer :year_built
      t.integer :carport_spaces, :default => 0
      t.integer :garage_spaces, :default => 0
      t.integer :off_street_spaces, :default => 0
      t.integer :bedrooms, :default => 0
      t.integer :bathrooms, :default => 0
      t.string :energy_efficiency_rating
      t.string :energy_star_rating
      t.decimal :longitude, :precision => 12, :scale => 7
      t.decimal :latitude, :precision => 12, :scale => 7
      t.decimal :land_area, :precision => 16, :scale => 3
      t.decimal :floor_area, :precision => 16, :scale => 3
      t.integer :number_of_floors
      t.string :virtual_tour
      t.integer :general_sale_id
      t.string :land_area_metric
      t.string :floor_area_metric
      t.string :property_url
      t.string :ext_link_1
      t.string :ext_link_2
      t.date :date
      t.time :time
    end
  end

  def self.down
    drop_table :general_sale_details
  end
end
