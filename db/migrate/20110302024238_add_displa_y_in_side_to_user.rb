class AddDisplaYInSideToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :display_on_site, :boolean
  end

  def self.down
    remove_column :users, :display_on_site
  end
end
