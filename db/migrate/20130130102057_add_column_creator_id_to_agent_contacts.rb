class AddColumnCreatorIdToAgentContacts < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :creator_id, :integer
  end

  def self.down
    add_column :agent_contacts, :creator_id
  end
end
