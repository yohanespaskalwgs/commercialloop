class AddProcessedStatusForProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :processed, :boolean
  end

  def self.down
    remove_column(:properties, :processed)
  end
end
