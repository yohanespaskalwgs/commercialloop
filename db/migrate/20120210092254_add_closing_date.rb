class AddClosingDate < ActiveRecord::Migration
def self.up
    add_column :residential_sale_details, :closing_date, :datetime
  end

  def self.down
    remove_column :residential_sale_details, :closing_date
  end
end
