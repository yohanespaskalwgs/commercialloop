class CreatePropertyPortalExports < ActiveRecord::Migration
  def self.up
    create_table :property_portal_exports do |t|
      t.integer :property_id
      t.integer :portal_id
      t.timestamps
    end
  end

  def self.down
    drop_table :property_portal_exports
  end
end
