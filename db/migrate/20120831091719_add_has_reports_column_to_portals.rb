class AddHasReportsColumnToPortals < ActiveRecord::Migration
  def self.up
    add_column :portals, :has_reports, :boolean
  end

  def self.down
    remove_column :portals, :has_reports
  end
end
