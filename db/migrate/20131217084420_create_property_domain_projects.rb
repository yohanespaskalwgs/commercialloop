class CreatePropertyDomainProjects < ActiveRecord::Migration
  def self.up
    create_table :property_domain_projects do |t|
      t.integer :property_id
      t.integer :domain_project_id
      t.timestamps
    end
  end

  def self.down
    drop_table :property_domain_projects
  end
end
