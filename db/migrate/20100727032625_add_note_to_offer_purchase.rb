class AddNoteToOfferPurchase < ActiveRecord::Migration
  def self.up
    add_column :offers, :contact_note_id, :integer
    add_column :purchasers, :contact_note_id, :integer
  end

  def self.down
    remove_column(:offers, :contact_note_id)
    remove_column(:purchasers, :contact_note_id)
  end
end
