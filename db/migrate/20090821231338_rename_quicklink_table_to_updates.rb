class RenameQuicklinkTableToUpdates < ActiveRecord::Migration
  def self.up
    rename_table :quicklinks, :updates    
  end

  def self.down
    rename_table :updates, :quicklinks  
  end
end
