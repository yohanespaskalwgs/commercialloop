class CreateOfficeSpecialitiesRelations < ActiveRecord::Migration
  def self.up
    create_table :office_specialities_relations do |t|
      t.integer :office_id
      t.integer :office_speciality_id
      t.timestamps
    end
  end

  def self.down
    drop_table :office_specialities_relations
  end
end
