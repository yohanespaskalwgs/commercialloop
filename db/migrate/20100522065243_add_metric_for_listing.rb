class AddMetricForListing < ActiveRecord::Migration
  def self.up
    #residential_sale
    add_column :residential_sale_details,:tax_rate_period,:string
    add_column :residential_sale_details,:condo_strata_fee_period,:string
    add_column :residential_sale_details,:estimate_rental_return_period,:string
    add_column :residential_sale_details,:land_area_metric,:string
    add_column :residential_sale_details,:floor_area_metric,:string
    #residential_lease
    add_column :residential_lease_details,:land_area_metric,:string
    add_column :residential_lease_details,:floor_area_metric,:string
    #holiday_lease_details
    add_column :holiday_lease_details,:price_period,:string
    add_column :holiday_lease_details,:land_area_metric,:string
    add_column :holiday_lease_details,:floor_area_metric,:string
    #commercial_details
    add_column :commercial_details,:land_area_metric,:string
    add_column :commercial_details,:floor_area_metric,:string
    #project_sale_details
    add_column :project_sale_details,:land_area_metric,:string
    add_column :project_sale_details,:floor_area_metric,:string
    add_column :project_sale_details,:porch_terrace_area_metric,:string
    add_column :project_sale_details,:garage_area_metric,:string
    add_column :project_sale_details,:land_width_metric,:string
    add_column :project_sale_details,:land_depth_metric,:string
    add_column :project_sale_details,:estimate_rental_return_period,:string
    #business_sale_details
    add_column :business_sale_details,:land_area_metric,:string
    add_column :business_sale_details,:floor_area_metric,:string
  end

  def self.down
    #residential_sale
    remove_column :residential_sale_details,:tax_rate_period
    remove_column :residential_sale_details,:condo_strata_fee_period
    remove_column :residential_sale_details,:estimate_rental_return_period
    remove_column :residential_sale_details,:land_area_metric
    remove_column :residential_sale_details,:floor_area_metric
    #residential_lease
    remove_column :residential_lease_details,:land_area_metric
    remove_column :residential_lease_details,:floor_area_metric
    #holiday_lease_details
    remove_column :holiday_lease_details,:price_period
    remove_column :holiday_lease_details,:land_area_metric
    remove_column :holiday_lease_details,:floor_area_metric
    #commercial_details
    remove_column :commercial_details,:land_area_metric
    remove_column :commercial_details,:floor_area_metric
    #project_sale_details
    remove_column :project_sale_details,:land_area_metric
    remove_column :project_sale_details,:floor_area_metric
    remove_column :project_sale_details,:porch_terrace_area_metric
    remove_column :project_sale_details,:garage_area_metric
    remove_column :project_sale_details,:land_width_metric
    remove_column :project_sale_details,:land_depth_metric
    remove_column :project_sale_details,:estimate_rental_return_period
    #business_sale_details
    remove_column :business_sale_details,:land_area_metric
    remove_column :business_sale_details,:floor_area_metric
  end
end
