class CreateCompanyCategoryRelations < ActiveRecord::Migration
  def self.up
    create_table :company_category_relations do |t|
      t.integer :agent_company_id
      t.integer :category_contact_id
      t.timestamps
    end
  end

  def self.down
    drop_table :company_category_relations
  end
end
