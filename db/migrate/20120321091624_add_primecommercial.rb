class AddPrimecommercial < ActiveRecord::Migration
  def self.up
    add_column :users, :primecommercial_id, :string, :limit => 50
  end

  def self.down
    remove_column(:users, :primecommercial_id)
  end
end
