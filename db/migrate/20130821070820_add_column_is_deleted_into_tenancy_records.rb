class AddColumnIsDeletedIntoTenancyRecords < ActiveRecord::Migration
  def self.up
    add_column :tenancy_records, :is_deleted, :boolean, :default => false
  end

  def self.down
    remove_column :tenancy_records, :is_deleted
  end
end
