class CreateCompanyIndustryCategoryRelations < ActiveRecord::Migration
  def self.up
    create_table :company_industry_category_relations do |t|
      t.integer :agent_company_id
      t.integer :industry_sub_category_id
      t.timestamps
    end
  end

  def self.down
    drop_table :company_industry_category_relations
  end
end
