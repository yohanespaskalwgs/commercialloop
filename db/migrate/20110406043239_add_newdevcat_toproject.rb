class AddNewdevcatToproject < ActiveRecord::Migration
  def self.up
    add_column :project_sale_details, :new_development_category_id, :integer
  end

  def self.down
    remove_column :project_sale_details, :new_development_category_id
  end
end
