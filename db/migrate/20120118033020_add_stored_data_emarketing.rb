class AddStoredDataEmarketing < ActiveRecord::Migration
  def self.up
    add_column :emarketings, :stored_layout, :longblob
    add_column :emarketings, :stored_data, :longblob
  end

  def self.down
    remove_column :emarketings, :stored_layout
    remove_column :emarketings, :stored_data
  end
end
