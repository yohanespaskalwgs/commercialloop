class CreateCompanyPropertyTypeRelations < ActiveRecord::Migration
  def self.up
    create_table :company_property_type_relations do |t|
      t.integer :agent_company_id
      t.integer :property_type_id
      t.timestamps
    end
  end

  def self.down
    drop_table :company_property_type_relations
  end
end
