class AddColumnForSolicitorOnAgentContactsAndCompanies < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :solicitor_name, :string
    add_column :agent_contacts, :solicitor_email, :string
    add_column :agent_contacts, :solicitor_phone, :string
    add_column :agent_companies, :solicitor_name, :string
    add_column :agent_companies, :solicitor_email, :string
    add_column :agent_companies, :solicitor_phone, :string
  end

  def self.down
    remove_column :agent_contacts, :solicitor_name
    remove_column :agent_contacts, :solicitor_email
    remove_column :agent_contacts, :solicitor_phone
    remove_column :agent_companies, :solicitor_name
    remove_column :agent_companies, :solicitor_email
    remove_column :agent_companies, :solicitor_phone
  end
end
