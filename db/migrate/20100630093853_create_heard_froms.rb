class CreateHeardFroms < ActiveRecord::Migration
  def self.up
    create_table :heard_froms do |t|
      t.string :name
      t.timestamps
    end
  end

  def self.down
    drop_table :heard_froms
  end
end
