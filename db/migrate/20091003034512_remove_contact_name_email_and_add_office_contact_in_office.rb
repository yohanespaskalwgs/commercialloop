class RemoveContactNameEmailAndAddOfficeContactInOffice < ActiveRecord::Migration
  def self.up
    remove_column :offices, :contact_first_name
    remove_column :offices, :contact_last_name
    remove_column :offices, :contact_email
    add_column :offices, :office_contact, :integer
  end

  def self.down
    remove_column :offices, :office_contact
    add_column :offices, :contact_first_name, :string
    add_column :offices, :contact_last_name, :string
    add_column :offices, :contact_email, :string
  end
end
