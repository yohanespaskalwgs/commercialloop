class FillNumberOfFloorsOnPropertyWithNumberOfFloorsFromPropertyDetails < ActiveRecord::Migration
  def self.up
    residential_sale_ids = ResidentialSaleDetail.find(:all, :conditions => ["number_of_floors IS NOT NULL"], :include => [:property]).collect(&:residential_sale_id)
    properties = Property.find(:all, :conditions => ["id IN (?)", residential_sale_ids])
    properties.each do |property|
      property.number_of_floors = property.detail.number_of_floors
      property.save
    end

    residential_lease_ids = ResidentialLeaseDetail.find(:all, :conditions => ["number_of_floors IS NOT NULL"], :include => [:property]).collect(&:residential_lease_id)
    properties = Property.find(:all, :conditions => ["id IN (?)", residential_lease_ids])
    properties.each do |property|
      property.number_of_floors = property.detail.number_of_floors
      property.save
    end

    holiday_lease_ids = HolidayLeaseDetail.find(:all, :conditions => ["number_of_floors IS NOT NULL"], :include => [:property]).collect(&:holiday_lease_id)
    properties = Property.find(:all, :conditions => ["id IN (?)", holiday_lease_ids])
    properties.each do |property|
      property.number_of_floors = property.detail.number_of_floors
      property.save
    end

    commercial_ids = CommercialDetail.find(:all, :conditions => ["number_of_floors IS NOT NULL"], :include => [:property]).collect(&:commercial_id)
    properties = Property.find(:all, :conditions => ["id IN (?)", commercial_ids])
    properties.each do |property|
      property.number_of_floors = property.detail.number_of_floors
      property.save
    end
  end

  def self.down
    children_listings = Property.find(:all, :conditions => ["number_of_floors IS NOT NULL"])
    children_listings.each do |children_listing|
      children_listing.number_of_floors = nil
      children_listing.save
    end  
  end
end
