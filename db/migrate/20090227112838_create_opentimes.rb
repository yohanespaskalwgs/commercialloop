class CreateOpentimes < ActiveRecord::Migration
  def self.up
    create_table :opentimes do |t|
      t.integer :property_id
      t.date :date
      t.time :start_time
      t.time :end_time

    end
  end

  def self.down
    drop_table :opentimes
  end
end
