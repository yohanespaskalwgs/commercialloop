class CreateUserFileCategories < ActiveRecord::Migration
  def self.up
    create_table :user_file_categories do |t|
      t.string :name
      t.integer :office_id
    end
  end

  def self.down
    drop_table :user_file_categories
  end
end
