class RemoveColumnIsVaultAndIsResidentialProjectAndAddNewColumnAccessTypeOnAgentContacts < ActiveRecord::Migration
  def self.up
    remove_column :agent_contacts, :is_vault
    remove_column :agent_contacts, :is_residential_project
    add_column :agent_contacts, :access_type, :string
  end

  def self.down
    add_column :agent_contacts, :is_vault, :boolean
    add_column :agent_contacts, :is_residential_project, :boolean
    remove_column :agent_contacts, :access_type
  end
end
