class CreateSuburbs < ActiveRecord::Migration
  def self.up
    create_table :suburbs do |t|
      t.string :name
      t.integer :area_id
      t.string :area_type
    end
  end

  def self.down
    drop_table :suburbs
  end
end
