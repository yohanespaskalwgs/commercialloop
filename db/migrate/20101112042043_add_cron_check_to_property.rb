class AddCronCheckToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :cron_checked, :boolean
  end

  def self.down
    add_column :properties, :cron_checked
  end
end
