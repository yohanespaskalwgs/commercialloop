class AddCreatorForCategoryNoteHeard < ActiveRecord::Migration
  def self.up
    add_column :category_contacts, :creator_id,:integer
    add_column :note_types, :creator_id,:integer
    add_column :heard_froms, :creator_id,:integer
  end

  def self.down
    remove_column(:category_contacts, :creator_id)
    remove_column(:note_types, :creator_id)
    remove_column(:heard_froms, :creator_id)
  end
end
