class AddDescriptionToAttachment < ActiveRecord::Migration
  def self.up
    add_column :attachments, :description, :string
    add_column :attachments, :private, :boolean
  end

  def self.down
    remove_column :attachments, :description
    remove_column :attachments, :private
  end
end
