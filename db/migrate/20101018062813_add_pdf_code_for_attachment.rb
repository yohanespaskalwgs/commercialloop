class AddPdfCodeForAttachment < ActiveRecord::Migration
  def self.up
    add_column :attachments, :pdf_layout, :text
  end

  def self.down
    remove_column :attachments, :pdf_layout
  end
end
