class CreateClearingSalesEventDetails < ActiveRecord::Migration
  def self.up
    create_table :clearing_sales_event_details do |t|
      t.decimal :longitude, :precision => 12, :scale => 7
      t.decimal :latitute, :precision => 12, :scale => 7
      t.decimal :land_area, :precision => 16, :scale => 3
      t.decimal :floor_area, :precision => 16, :scale => 3
      t.integer :number_of_floors
      t.string :virtual_tour
      t.integer :clearing_sales_event_id
      t.string :property_url
      t.string :ext_link_1
      t.string :ext_link_2
      t.timestamps
    end
  end

  def self.down
    drop_table :clearing_sales_event_details
  end
end
