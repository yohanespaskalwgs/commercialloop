Given /^the following testing_cucumbers:$/ do |testing_cucumbers|
  TestingCucumber.create!(testing_cucumbers.hashes)
end

When /^I delete the (\d+)(?:st|nd|rd|th) testing_cucumber$/ do |pos|
  visit testing_cucumbers_url
  within("table tr:nth-child(#{pos.to_i+1})") do
    click_link "Destroy"
  end
end

Then /^I should see the following testing_cucumbers:$/ do |expected_testing_cucumbers_table|
  expected_testing_cucumbers_table.diff!(tableish('table tr', 'td,th'))
end
